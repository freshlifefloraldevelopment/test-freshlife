<?php

// PO 2019-07-15

require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$shippingMethod = $_POST['shippingMethod'];
$userSessionID = $_SESSION["buyer"];

$date = date('Y-m-d');
$delDate = $date;
$dayOfMonth = date('d', strtotime($date));
$monthOfYear = date('m', strtotime($date));
$year = date('Y', strtotime($date));

$getBuyerShippingMethod = "select id             , buyer_id       , shipping_method_id, country     , own_shipping    , 
                                  cargo_agency_id, grower_box_name, choose_shipping   , first_name  , last_name       ,
                                  address        , state          , zipcode           , phone       , company         ,
                                  airport_id     , days           , drop_off_option   , address_type, default_shipping, 
                                  shipping_code  
                            from buyer_shipping_methods 
                           where shipping_method_id ='" . $shippingMethod . "'";  

$buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
$buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
$leavingFarmDate = '';

if (!empty($buyerShippingMethod['shipping_method_id'])) {
    
    $shipping_method_id = $buyerShippingMethod['shipping_method_id'];
    
    $getShippingMethod = "select connections , days as days_ship,
                                 connect_group
                            from shipping_method 
                           where id='" . $shipping_method_id . "'";
    
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    
    $temp_conn = explode(',', $shippingMethodDetail['connect_group']);
    
     $id_conn = $temp_conn[1];  // Escojer conexion Ejemplo:   $temp_conn[2];
     
     $var= date("w");
     
     $getConNew = mysqli_query($con, "select days ,trasit_time 
                                        from more_days_connection
                                       where id_conn='" . $id_conn . "' 
                                         and days >= '" . $var . "' LIMIT 0,1");  

    $trasit_time = 1;
    
     while ($conNew = mysqli_fetch_array($getConNew)) {
         $trasit_time = $conNew['trasit_time'];
     }
     
     
    $connections = unserialize($shippingMethodDetail['connections']);    
    $conCount = count($connections);
   // $trasit_time = 0;
    $days = array();
    
    
    /*
    if ($conCount == 1) {       // Si hay conecciones
        
        foreach ($connections as $connection) {
            $cost_un = unserialize($connection);

            foreach ($cost_un as $key => $value) {
                $value;
            }
        
            echo $value;
			
            $getConDetail = mysqli_query($con, "select (trasit_time) trasit_time , days ,(trasit_time+1) start_trasit  
                                                  from connections 
                                                 where id='" . $connection . "'");
            
            $conDetail = mysqli_fetch_assoc($getConDetail);
            
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $start_trasit = $trasit_time + $conDetail['start_trasit'];            
            
             $days = explode(',', $conDetail['days']);
            $daysOfWeekDisabled = $conDetail['days'];
        }


        if ($trasit_time > 1) {
            $prevDate = strtotime($delDate . ' +' . $trasit_time . ' days');
            $trasit_date = date("Y-m-d", $prevDate);
        } else {
            $prevDate = strtotime($delDate . '  +' . $trasit_time . ' day');
            $trasit_date = date("Y-m-d", $prevDate);
        }
        
        $trasitDay = date("w", strtotime($trasit_date));
        
        if (in_array($trasitDay, $days)) {
            $leavingFarmDate = $trasit_time;
        } else {
            $ttime = $trasit_time;
            switch (true) {
                case in_array($trasitDay + 1, $days):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay + 2, $days):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay + 3, $days):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay + 4, $days):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay + 5, $days):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay + 6, $days):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
            $leavingFarmDate = $ttime;
        }
        
    } */

    
    // $leavingFarmDate = $start_trasit;   
     
    // $startDate = date('Y-m-d', strtotime("+".$leavingFarmDate." days"));     
    //$startDate = date('Y-m-d', strtotime("+1 days"));
    //$startDate = date('Y-m-d'); 
    
    $startDate = date('Y-m-d', strtotime(' +' . $trasit_time . ' days') );
        
  
    $daysOfWeekDisabled = $shippingMethodDetail['days_ship'];
    
    $totalDays = array('0','1','2','3','4','5','6');
    $daysOfWeekDisabled = explode(',',$daysOfWeekDisabled);
    

    $longitud = count($daysOfWeekDisabled);
      
      for($i=0; $i<$longitud; $i++)  {
	   $daysOfWeekDisabled[$i] = substr($daysOfWeekDisabled[$i],-1) ;          
      }            
          
    $result=array_diff($totalDays,$daysOfWeekDisabled);
    $daysOfWeekDisabled = implode(',',$result);
    
    $val = '';

    
}else {
    
    $startDate = date('Y-m-d');
    $daysOfWeekDisabled = '';
    $val = $startDate;
}

 
?>
<input type="text" class="form-control datepicker cls_custom_date" id="deliveryDate_<?php echo $_REQUEST['product_id'].'_'.$_REQUEST['index'];?>" days-of-week-disabled="<?php echo $daysOfWeekDisabled; ?>"  data-todayHighlight='false' data-todayBtn="false"  value="<?php echo(!empty($val))? $startDate : ''; ?>" start-date="<?php echo $startDate; ?>" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" placeholder="Select Date" style="width: 400px!important;text-indent: 32px;border-radius: 5px;">
