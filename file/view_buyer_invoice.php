<?php
include("config/config_new.php");
if ($_SESSION["login"] != 1) {
	header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];

/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = 'profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$page_request = "buyer_invoices";
?>
<?php require_once 'includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "includes/left_sidebar_buyer.php"; ?>
<?php 
	$buyer_sql = "SELECT buyers.*, country.name as country_name, shipping_code FROM buyers LEFT JOIN country ON country.id = buyers.country LEFT JOIN buyer_shipping_methods ON buyer_shipping_methods.buyer_id = buyers.id WHERE buyers.id = '".$userSessionID."'";
	//echo $buyer_sql;
	$buyer_res = mysqli_query($con, $buyer_sql);
	$buyer_info = mysqli_fetch_assoc($buyer_res);
	//var_dump($buyer_info);
	
	$invoice_sql = "SELECT buyer_invoices.*, grower_offer_reply.product as product_name, grower_offer_reply.price as product_price, grower_offer_reply.boxtype, grower_offer_reply.boxqty, grower_offer_reply.product_subcategory, grower_offer_reply.box_weight, grower_offer_reply.accept_date, grower_offer_reply.size FROM buyer_invoices LEFT JOIN grower_offer_reply ON grower_offer_reply.id = buyer_invoices.bi_grower_offer_reply_id WHERE bi_buyer_id = '".$userSessionID."' AND bi_invoice_number = '".$_GET['id']."' GROUP BY bi_checkout_code";
	//echo $invoice_sql;
	$invoice_res = mysqli_query($con, $invoice_sql); // For calculating all amounts
	//var_dump($invoice_res);
	if(mysqli_num_rows($invoice_res) == 0){
		echo "<script type='text/javascript'>window.location.href = 'buyer_invoices.php';</script>";
	}
	$invoice_sql = "SELECT buyer_invoices.*, grower_offer_reply.product as product_name, grower_offer_reply.price as product_price, grower_offer_reply.boxtype, grower_offer_reply.boxqty, grower_offer_reply.product_subcategory, grower_offer_reply.box_weight, grower_offer_reply.accept_date, grower_offer_reply.size, grower_offer_reply.grower_id FROM buyer_invoices LEFT JOIN grower_offer_reply ON grower_offer_reply.id = buyer_invoices.bi_grower_offer_reply_id WHERE bi_buyer_id = '".$userSessionID."' AND bi_invoice_number = '".$_GET['id']."'";
	//echo $invoice_sql;
	$invoice_result = mysqli_query($con, $invoice_sql); // For shwoing all invoice items
	
	
	/*********** Code for getting Buyer Shipping details ************/

	$getShippingMethod = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "'";
	//echo $getShippingMethod;
	$shippingMethodRes = mysqli_query($con, $getShippingMethod);
	if (mysqli_num_rows($shippingMethodRes) > 0) {
		$total_freight = 0;
		$total_fuel_surcharge = 0;
		$total_security = 0;
		$total_due_agent = 0;
		$total_transit_days = 0;
		$total_shipping_rate = 0;
		$charges_per_kilo = 0;
		$charges_per_shipment = 0;
		$newChargesArr = array();
		while ($shippingMethodData = mysqli_fetch_assoc($shippingMethodRes)) {
			//echo '<pre>';print_r($shippingMethodData);
			if ($shippingMethodData['shipping_method_id'] != 0) {

				$getshipping_type = "select * from shipping_method where id =" . $shippingMethodData['shipping_method_id'];
				$shippingTypeQry = mysqli_query($con, $getshipping_type);   
				$shippingType = mysqli_fetch_assoc($shippingTypeQry);
				
				$connections_arr = unserialize($shippingType['connections']);
				//var_dump($connections_arr);
				
			} else {
				
				//echo $shippingMethodData['id'];
				if ($shippingMethodData['own_shipping'] == '1') {
						$getshipping_type = "select * from cargo_agency where id='" . $shippingMethodData['cargo_agency_id'] . "'";
						
						$shippingTypeQry = mysqli_query($con, $getshipping_type);
						$shippingType = mysqli_fetch_assoc($shippingTypeQry);
						//var_dump($shippingType);
						$shippingMethodDataname = $shippingType['name'];
				} else {
					$shipping_type = $shippingMethodData['choose_shipping'];
					

					if (!empty($shipping_type) && $shipping_type != '') {
						$getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
						$shippingTypeQry = mysqli_query($con, $getshipping_type);
						$shippingType = mysqli_fetch_assoc($shippingTypeQry);
						//echo "<pre>";print_r($shippingType);echo "</pre>";
						$connections_arr = unserialize($shippingType['connections']);
						//var_dump($connections_arr);
						
					} else {
						$shippingMethodDataname = $shippingMethodData['name'];
					}
				}
			}
			
			if($shippingMethodData['own_shipping'] == 0){
				
				foreach($connections_arr as $c){
					
					$c_sql = "SELECT * FROM connections WHERE id = '".$c."'";
					//echo $c_sql;
					$c_res = mysqli_query($con, $c_sql);
					$c_row = mysqli_fetch_assoc($c_res);
					//var_dump($c_row); echo '<br>';
					$charges_per_kilo_arr = unserialize($c_row['charges_per_kilo']);
					$charges_per_shipment_arr = unserialize($c_row['charges_per_shipment']);
					//var_dump($charges_arr);
					

					foreach($charges_per_kilo_arr as $k => $cpk){
						
					$charges_per_kilo += $cpk;	
						
					}
					
					foreach($charges_per_shipment_arr as $k => $cps){
						
						if(count($newChargesArr) > 0){
			
							if (array_key_exists($k, $newChargesArr)) {
									
									$newChargesArr[$k] += $cps;
								}
								else{
									
									$newChargesArr[$k] = $cps;
								}
						}
						else{
							$newChargesArr[$k] = $cps;
							
						}
						//echo $value;
					$charges_per_kilo += $cpk;	
						
					}
				
					$total_fuel_surcharge += $c_row['fuel_surcharge']; 
					$total_security += $c_row['security']; 
					$total_due_agent += $c_row['due_agent']; 
					$total_transit_days += $c_row['trasit_time'];
				}
				//echo $charges_per_kilo;
			}
			
			
		}
	} else {
		echo 'No Shipping Methods Found';
	} 
	
	
	/*********** Buyer shipping details code end here    ****************/
?>
<section id="middle">


				<!-- page title -->
				<header id="page-header">
					<h1>Invoice Page</h1>
					<ol class="breadcrumb">
						<li><a href="#">Pages</a></li>
						<li class="active">Invoice Page</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">

								<div class="col-md-6 col-sm-6 text-left">

									<h4><strong>Client</strong> Details</h4>
									<ul class="list-unstyled">
										<li><strong>First Name:</strong> <?php echo $buyer_info['first_name']; ?></li>
										<li><strong>Last Name:</strong> <?php echo $buyer_info['last_name']; ?></li>
										<li><strong>Company:</strong> <?php echo $buyer_info['company']; ?></li>
										<li><strong>Country:</strong> <?php echo $buyer_info['country_name']; ?></li>
										
									</ul>

								</div>

								<div class="col-md-6 col-sm-6 text-right">
									<div id="invoice_details">
									<h4><strong>Invoice</strong> Details</h4>
									<?php 
										$total_boxes = 0;
										$gross_weight = 0;
										$discount = 0;
										if($invoice_res){
											while($invoice = mysqli_fetch_array($invoice_res)){
												//var_dump($invoice); echo '<br>';
												//$sub_total += $invoice['bi_order_amount'];
												$total_boxes += $invoice['boxqty'];
												$gross_weight += $invoice['boxqty'] * $invoice['box_weight'];
												$delivery_date = $invoice['accept_date'];
												$shipping_method = substr($_GET['id'], 0, 4);
												$discount += $invoice['bi_order_discount'];
												//$total_freight_charges = $total_freight * $gross_weight;
											}
											 
										}
									?>
									<ul class="list-unstyled">
										<li><strong>Date:</strong> <?php echo date('d/m/Y'); ?></li>
										<li><strong>Invoice Number:</strong> <?php echo $_GET['id']; ?></li>
										<li><strong>Customer ID:</strong> <?php echo substr($_GET['id'], 0, 3); ?></li>
										<li><strong>Freight Type:</strong> Air</li>
                                        <li><strong>A.W.B:</strong> 139856247</li>
										<li><strong>Est Ship Date:</strong> <?php echo date("d/m/Y", strtotime($delivery_date. ' + '.$total_transit_days.' days')); ?></li>
										<li><strong>Est Gross Weight:</strong> <?php echo $gross_weight; ?>kg</li>
										<li><strong>Total Boxes:</strong> <?php echo $total_boxes; ?></li>
									</ul>
									</div>
								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Variety Description</th>
											<th>Bunch/Stem</th>
											<th>Quantity</th>
                                            <th>Grower Price</th>
                                            <?php 
												if($buyer_info['display_shipping'] == 0){
											?>	
												<th>Charges Per Kilo</th>
											<?php
												}
											?>
											<?php 
												if($buyer_info['display_handling'] == 0){
											?>	
												<th>Handling</th>
											<?php
												}
											?>
											<?php 
												if($buyer_info['display_tax'] == 0){
											?>	
												<th>Taxes</th>
											<?php
												}
											?>
                                            <th>Final Price</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
									<?php
										if($invoice_result){
											//echo 'hererer';
											$sub_total = 0;
											$grand_total = 0;
											while($invoice = mysqli_fetch_array($invoice_result)){
												//var_dump($invoice);
												$b_type = $invoice['boxtype'];
												if($b_type == 'HB'){
													$box_type_name = 'Half Box(es)';
												}
												else if($b_type == 'FB'){
													$box_type_name = 'Full Box(es)';
												}
												else if($b_type == 'QB'){
													$box_type_name = 'Quarter Box(es)';
												}
												else if($b_type == 'JB'){
													$box_type_name = 'Jumbo Box(es)';
												}
												else if($b_type == 'EB'){
													$box_type_name = 'Eighth Box(es)';
												}
												
												$g_sql = "SELECT growers_name FROM growers WHERE id= '".$invoice['grower_id']."'";
												$g_res = mysqli_query($con, $g_sql);
												$g_details = mysqli_fetch_assoc($g_res);
												
									?>
										<tr>
											<td>
												<div><strong><?php echo $invoice['product_subcategory'].' '.$invoice['product_name']. ' '.$invoice['size'];  ?></strong></div>
												<small><strong><?php echo $invoice['boxqty'].' '.$box_type_name;?></strong> <?php echo $g_details['growers_name']; ?></small>
											</td>
											<td><?php echo ($invoice['bi_is_bunch'] == 1)? 'Bunches' : 'Stems' ; ?></td>
											<td><?php echo $invoice['bi_bunch_qty']; ?></td>
                                            <td>$<?php echo $invoice['product_price']; ?></td>
                                            <?php 
											if($buyer_info['display_shipping'] == 0){
											?>	
												<td>$<?php $total_kilo_charges = $invoice['boxqty'] * $invoice['box_weight']*$charges_per_kilo; echo number_format($charges_per_kilo, 2); ?></td>
											<?php
												}else{$total_kilo_charges = 0;}
											?>
											<?php
												$handling_fee = 0;
												if($buyer_info['display_handling'] == 0){
													$handling_fee = $buyer_info['handling_fees'];
											?>	
												<td>$<?php echo $buyer_info['handling_fees']; ?></td>
											<?php
												}
											?>
											<?php 
												$tax = 0;
												if($buyer_info['display_tax'] == 0){
													$tax = $buyer_info['tax'];
											?>	
												<td>$<?php echo $buyer_info['tax']; ?></td>
											<?php
												}
											?>
                                            <td>$<?php $final_price = $invoice['product_price']+$handling_fee+$tax+$total_kilo_charges; echo number_format($final_price, 2); ?></td>
											<td>$<?php $total_amount = ($invoice['bi_bunch_qty']*$final_price); echo number_format($total_amount, 2); ?></td>
										</tr>
											
									<?php
											$sub_total += $total_amount;
											}
										$grand_total = $sub_total - $discount;
										
										$total_fuel_surcharge_charges = $total_fuel_surcharge * $gross_weight;
										$grand_total += $total_freight_charges; 
										$grand_total += $total_fuel_surcharge_charges; 
										$grand_total += $total_security; 
										$grand_total += $total_due_agent;
										$grand_total += $charges_per_shipment;
										}
										else{
									?>
										<tr>
											<td>No orders found</td>
										</tr>
									<?php
										}
									?>	
									
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>Contact</strong> Details</h4>

									<p class="nomargin nopadding">
										<strong>Terms of Sale:</strong> 
										All quality problems must be reported in writing and sending pictures within 48 hours after reciept of the product. No claims for weight will be accepted. We will not be responsible for shipping problems, ACCEPTANCE OF THIS SHIPMENT CONSTITUES AGREEMENT TO ALL OF THE ABOVE TERMS AND CONDITIONS. If this invoice is not cancelled by its due date, our company will charge you a legal fee as well as the cost. 
									</p><br><!-- no P margin for printing - use <br> instead -->

									<address>
										FRESH LIFE FLORAL <br>
										Av. Interoceanica OE6-73 y Gonzalez Suarez<br>
										Edificio Picadilly Center, Cuarto piso, oficina 407<br>
										Tumbaco. <br>
										Quito- Ecuador<br>
										EC170184<br>
										Phone: +593 602 2630<br>
										Fax: +593 602 2631<br>
										Email: info@freshlifefloral.com<br>
										P.O.Box 17-22-20577
									</address>

								</div>

								<div class="col-sm-6 text-right">

									<ul class="list-unstyled">
										<li><strong>Sub - Total Amount:</strong> $<?php echo number_format($sub_total, 2); ?></li>
										<li><strong>Discount:</strong> $<?php echo number_format($discount, 2); ?></li>
										<?php
											if($newChargesArr){
												$otherCharges = 0;
												foreach($newChargesArr as $k => $v){
													$otherCharges += $v;
										?>
											<li><strong><?php echo $k; ?>($<?php echo $v;?> per shipment):</strong> $ <?php echo number_format($v, 2); ?></li>
										<?php
												}
												//echo $otherCharges;
												$grand_total += $otherCharges;
											}
										?>
										<?php
											if($total_fuel_surcharge > 0){
										?>
											<li><strong>Fuel Surcharge($<?php echo $total_fuel_surcharge; ?> per kg):</strong> $<?php echo number_format($total_fuel_surcharge_charges, 2);?></li>
										<?php
											}
											if($total_security > 0){
										?>
											<li><strong>S.S.C(Security):</strong> $<?php echo number_format($total_security, 2); ?></li>
										<?php
											}
										?>
										
										<?php
											if($total_due_agent > 0){
										?>
											<li><strong>Due Agent & Docs:</strong> $<?php echo number_format($total_due_agent, 2); ?></li>
										<?php
											}
										?>
										
										<!--<li><strong>Form A Fee ($12 per form):</strong> $12.00</li>
										<li><strong>C.G(Per Air Way Bill):</strong> $1</li>
										<li><strong>Phyto Fee($2 per grower):</strong> $2</li>-->
										<li><strong>Grand Total:</strong> $<?php echo number_format($grand_total, 2); ?></li>
									</ul>     

									<button class="btn btn-default" id="send_email" onclick="send_email('<?php echo $buyer_info['email']; ?>');"> SEND TO CUSTOMER <button>
								</div>

							</div>

						</div>
					</div>

					<div class="panel panel-default text-right" id="invoice_buttons">
						<div class="panel-body">
							<a class="btn btn-warning" href="#"><i class="fa fa-pencil-square-o"></i> EDIT</a>
							<a class="btn btn-primary" href="#"><i class="fa fa-check"></i> SAVE</a>
							<a class="btn btn-success" href="javascript:;" onclick="print_invoice();"><i class="fa fa-print"></i> PRINT INVOICE</a>
						</div>
					</div>

				</div>
			</section>
			<!-- /MIDDLE -->
	<style>
		.panel-body p{margin-bottom: 3px;}
		.panel-body{font-size: 13px;}
	</style>
	<script>
		function print_invoice(){
			var divToPrint=document.getElementById('content');

			  var newWin=window.open('','Print-Window');

			  newWin.document.open();
			  var newHtml = '<html><head><link href="http://localhost/freshlifefloral-staging/includes/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><style type="text/css">#invoice_details{margin-top: -120px;} #send_email{visibility: hidden; display:none;} #invoice_buttons{display: none;}</style></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>';
			  newWin.document.write(newHtml);

			  newWin.document.close();

			  setTimeout(function(){newWin.close();},10);
		}
		
		function send_email(email){
			
			var divToPrint=document.getElementById('content');
			var emailData = {
				'send_invoice' : 'Submit',
				'buyer_email' : email,
				'email_body' : '<html><head><link href="http://localhost/freshlifefloral-staging/includes/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><style type="text/css">#invoice_details{margin-top: -120px;} #send_email{visibility: hidden; display:none;} #invoice_buttons{display: none;}</style></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>',
				
			};
			$.ajax({
				type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
				url         : 'functions.php', // the url where we want to POST
				data        : emailData, // our data object
				dataType    : 'text', // what type of data do we expect back from the server
				encode          : true
			})
			// using the done promise callback
			.done(function(data) {
				if(data  == 1){
					alert('Invoice has been sent successfully to your email address.');
				}
			});
		}
	</script>
</section>
