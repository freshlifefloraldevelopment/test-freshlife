<?php
    $menuoff=1;
    $page_id=1;
	  
    include("config/config.php");	
	
    if($_SESSION["login"]!=1 && $_SESSION["grower"]=="" )
	{
	   header("location:".$siteurl);
	}
	
	$sel_grower_info="select * from growers where id='".$_SESSION["grower"]."'";
	$rs_grower_info=mysql_query($sel_grower_info);
	$grower_info=mysql_fetch_array($rs_grower_info);
?>

<!DOCTYPE html>

<html lang="en">

<?php include("include/head.php"); ?>

 <style>
 .inner-content
 {
    margin-top:30px !important;
 }
 .brad
 {
    
	margin-bottom:0px;
	text-align:right;
	margin-right:269px;
  
 }
 .left-container .left-mid
 {
   margin-top:-25px;
 }
 
 .form-row
 {
    float:left;
	width:350px;
 }
 </style>
 
 <script>
 function validate_press() {
    error = false;
    error_message = 'Errors have occured during the process of your form.\n\nPlease make the following corrections:\n';
	var errmsg="";
	
	if(document.proposal.first_name.value=="" || document.proposal.first_name.value=="First Name (required)") 
	{
		errmsg  +=  "Please enter your first name.\n";
	}
	if(document.proposal.last_name.value=="" || document.proposal.last_name.value=="Last Name (required)") 
	{
		errmsg  +=  "Please enter your last name.\n";
	}
	if(document.proposal.email.value=="" || document.proposal.email.value=="Email id (required)")
	{
		errmsg  +=  "Please enter your email address.\n";
	}
	
	if(!IsEmail(document.proposal.email.value ))
	{
	    alert("Please enter your valid email address.\n");
		document.proposal.email.focus();
		return false;
	}
	
	    if(document.proposal.phone.value=="" || document.proposal.phone.value=="Phone No (required)")
	{
		errmsg  +=  "Please enter your phone no.\n";
	}
	
	
		if (errmsg!="") 
	{
	  alert(error_message+"\n"+errmsg);
	  return false;
	} 
	else
   	{
	 
	  return true;
	  
	}
}
function IsEmail(mail)
{
  var text  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return text.test(mail)
}
</script>  
<body>
<div class="wrapper">
 <?php include("include/header.php"); ?>
  <div class="cl"></div>

  <div class="cl"></div>

  <div class="content-container inner-content">
   
    <div class="brad"> <a href="<?=$siteurl?>vendor-account.php">My Account</a></div>

    <div class="cl"></div>

    
    <div class="left-container" style="width:780px;">

      <div class="top"></div>

      <div class="left-mid" style="width:850px; min-height:550px;">

        <div class="content_area" style="width:730px;">

          <h2>My Account</h2>
      
      </div>

       <div class="cl"></div>
       
       <div class="contact-form">
          <form action="" method="post" name="proposal" accept-charset="utf-8" onSubmit="return validate_press()" >
        <div class="form-row"> 
        <label style="width:180px;">Name : <b><?=$grower_info["growers_name"]?></b></label>
        </div>
        
        <div class="form-row" style="height:50px;">
        <label>Web Site<span>*</span></label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="website" value="<?=$grower_info["website"]?>" /></div></div><div class="cl"></div>
        </div>
        
        <div class="form-row" style="height:50px;">
        <label>Country <span>*</span> </label>
        <div class="right-form"><div class="textbox-bg2">
        <select name="country_id" id="country_id" class="styled" style="width:239px; height:35px; border:3px solid #e2e2e2; padding-top:5px;">
                                      <option value="">--Select--</option>
                                       <?php
										   $sel_user="select * from country order by name";
										   $rs_user=mysql_query($sel_user);
										   while($user=mysql_fetch_array($rs_user))
										   {
									  ?>
                                           <option value="<?=$user["id"]?>" <? if($grower_info["country_id"]==$user["id"]) { echo "selected"; } ?> ><?=$user["name"]?></option>
                                      <?php
									      }
									  ?>    
                                      </select>                              
        </div></div><div class="cl"></div>
        </div>
        
        
        <div class="form-row">
        <label>Address<span>*</span></label>
         <div class="right-form"><div class="textbox-bg">
        <input type="text" name="website" value="<?=$grower_info["address"]?>" /></div></div>
        <div class="cl"></div>
        </div>
        
        <div class="form-row">
        <label>Description <span>*</span></label>
        <div class="right-form"><div class="textarea-bg">
        <textarea name="page_desc"><?=$grower_info["page_desc"]?></textarea></div></div>
        <div class="cl"></div>
        </div>
        
        <div class="form-row">
        <label>Short Description <span>*</span></label>
        <div class="right-form"><div class="textarea-bg">
        <textarea name="short_desc"><?=$grower_info["short_desc"]?></textarea></div></div>
        <div class="cl"></div>
        </div>
        
        
        
          
        <div class="form-row">
        <label>&nbsp; </label><div class="cl"></div><input type="submit" name="submit" class="submit" value="" /><div class="cl"></div></div>
        </form>
          </div>

        </div>

      <div class="bot"></div>

      <div class="cl"></div>

    </div>

    
    <div class="right-container" style="width:210px;">
    
    <h2>Navigation</h2>
  <div class="testi-inner" style="width:200px;">
  <div style="color:#666; padding-left:10px; padding-bottom:5px;">
  <a href="<?=$siteurl?>buyer/my-account" style="color:#666; font-weight:bold; text-decoration:none;">My Account</a></div>  
  
  
  
  
  
  <div style="color:#666; padding-left:10px; padding-bottom:5px;">

    

  </div>
  </div>    
    </div>

    <div class="cl"></div>
    
    

  </div>

  <!-- inner-content ends -->

  <!-- footer starts -->

  <div class="footerwraper">

  <?php include("include/footer.php"); ?>

  </div>

  <!-- footer ends -->

   <?php include("include/copyright.php"); ?>

  <div class="cl"></div>

  <!-- body ends -->

</div>

<!-- main wrapper -->

<!-- Fixed-div starts -->

<?php include("include/fixeddiv.php"); ?>

<!-- Fixed-div ends -->

</body>

</html>
