<?php

include("config/config_new.php");
//print_r($_POST);
//die;
$pid = $_POST['pid'];
$vid = $_POST['vid'];

// sizecombo select box refill
$sizecombo = '<option value="">Select Size</option>';
$sel_sizecombo = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join sizes s on gpb.sizeid=s.id
			  left join growers g on gpb.growerid=g.id
			  where g.active='active' and s.name is not NULL ";
if ($pid != "") {
    $sel_sizecombo.=" and p.subcategoryid IN (" . $pid . ")";
}
if ($vid != "") {
    $sel_sizecombo.=" and  gpb.prodcutid IN (" . $vid . ")";
}
$sel_sizecombo .= " group by gpb.sizeid order by CONVERT(SUBSTRING(s.name,1), SIGNED INTEGER) ";

$rs_sizecombo = mysqli_query($con, $sel_sizecombo);
while ($sizecomboi = mysqli_fetch_array($rs_sizecombo)) {
    $sizecombo .='<option value="' . $sizecomboi["sid"] . '">' . trim($sizecomboi["sname"]) . 'cm</option>';
}


// specialcombo select box refill
$specialcombo = '<option value="">Select Special Feature</option>';

$sel_specialcombo = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join features s on gpb.feature=s.id
			  left join growers g on gpb.growerid=g.id
			  where g.active='active' and s.name is not NULL ";
if ($pid != "") {
    $sel_specialcombo.=" and p.subcategoryid IN (" . $pid . ")";
}
if ($vid != "") {
    $sel_specialcombo.=" and  gpb.prodcutid IN (" . $vid . ")";
}
$sel_specialcombo .= " group by gpb.feature order by s.name";
$rs_specialcombo = mysqli_query($con, $sel_specialcombo);
while ($specialcomboi = mysqli_fetch_array($rs_specialcombo)) {
    $specialcombo .='<option value="' . $specialcomboi["sid"] . '">' . trim($specialcomboi["sname"]) . '</option>';
}

$growercombo ='<option value="">Select Growers</option>';

$sel_growercombo = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join growers g on gpb.growerid=g.id		  
			  where g.active='active' and g.growers_name is not NULL ";
if ($pid != "") {
    $sel_growercombo.=" and p.subcategoryid IN (" . $pid . ")";
}
if ($vid != "") {
    $sel_growercombo.=" and  gpb.prodcutid IN (" . $vid . ")";
}
$sel_growercombo .= " group by g.id order by g.growers_name";
$rs_growercombo = mysqli_query($con, $sel_growercombo);
while ($growercomboi = mysqli_fetch_array($rs_growercombo)) {
    $growercombo .='<option value="' . $growercomboi["gid"] . '">' . trim($growercomboi["gname"]) . '</option>';
}


$output = array();
if (!empty($sizecombo)) {
    $output['sizecombo'] = utf8_encode($sizecombo);
} else {
    $output['sizecombo'] = '';
}
if (!empty($specialcombo)) {
    $output['specialcombo'] = utf8_encode($specialcombo);
} else {
    $output['specialcombo'] = '';
}
if (!empty($growercombo)) {
    $output['growercombo'] = utf8_encode($growercombo);
} else {
    $output['growercombo'] = '';
}
//echo '<pre>';
//print_r($output);die;
echo json_encode($output);
die;
?>
