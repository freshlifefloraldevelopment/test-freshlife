<footer>
      <nav>
        <ul class="last">
          <li><a href="<?=$siteurl?>cms/home">Home</a></li>
          <li><a href="<?=$siteurl?>cms/about-us">About us</a></li>
          <li><a href="<?=$siteurl?>cms/our-flowers">Our Flower</a></li>
          <li><a href="<?=$siteurl?>cms/services">Services</a></li>
          <li><a href="<?=$siteurl?>cms/buying-strategy">Buying Methods</a></li>
          <li><a href="<?=$siteurl?>cms/contact-us">Contact us</a></li>
        </ul>
        <ul style="float:left">
          <li><a href="<?=$siteurl?>cms/sitemap">SiteMap</a></li>
          <li><a href="<?=$siteurl?>cms/privacy-page">Privacy Policy</a></li>
          <li><a href="<?=$siteurl?>cms/terms-of-use">Refund Policy</a></li>
          <li><a href="<?=$siteurl?>cms/earning-disclaimer">Terms & Condition</a></li>
          <li><a href="<?=$siteurl?>cms/copyright-info">Copyright info</a></li>
        </ul>
        <div class="cl"></div>
      </nav>
      <aside class="social-network">
        <p>Newsletter</p>
        <div class="cl"></div>
        <span class="italic">Subscribe to our email newsletter for useful tips and <br>
        valuable resources</span>
    <form action="http://www.freshlifefloral.com/mcapi_listSubscribe.php" method="post">
        <div class="input_box">
          <input type="text" class="input_text" value="Your Email ID" name="text" id="text" onBlur="if (this.value == '') {this.value = 'Your Email ID';}" onFocus="if (this.value == 'Your Email ID') {this.value = '';}">
        </div>
        <div class="submit_btn">
          <input name="" type="image" src="images/submit.png">
        </div>
         <input type="hidden" name="submit" id="submit" />
   </form>

        <div class="cl"></div>
        <div class="social-icons">
          <p>Follow Us</p>
          <div class="social_icons_image"> <a href="https://www.facebook.com/pages/Fresh-Life-Floral/312264098886349"><img src="images/icon_facebook.png" width="37" height="36" alt="Facebook"></a> <a href="http://www.linkedin.com/pub/eduardo-polo/1b/93/306"><img src="images/icon_linkedin.png" width="37" height="36" alt="Facebook"></a> <a href="https://twitter.com/eduardopolo78"><img src="images/twitter.png" width="37" height="36" alt="Facebook"></a> </div>
        </div>
      </aside>
      <div class="cl"></div>
    </footer>
     
     <?php if($page!='grower') { ?>
<script type="text/javascript" src="<?=$siteurl?>js/jquery.colorbox.js"></script>
        
        <script type="text/javascript">
    $(document).ready(function(){
            jQuery(".example6").colorbox({iframe:true, innerWidth:335, innerHeight:500});
			jQuery(".example8").colorbox({iframe:true, innerWidth:335, innerHeight:550});
            jQuery(".example5").colorbox({iframe:true, innerWidth:335, innerHeight:450});
            jQuery(".example7").colorbox({iframe:true, innerWidth:335, innerHeight:230});
			jQuery(".example9").colorbox({iframe:true, innerWidth:335, innerHeight:400});
            jQuery(".example10").colorbox({iframe:true, innerWidth:335, innerHeight:315,onClosed:function(){ jQuery(location).attr('href', '<?=$siteurl?>buyer/my-account');} });
            //jQuery(".example8").colorbox({width:"30%", inline:true, href:"#inline_example1"});
	});
</script>

<?php } ?>
<script type="text/javascript">
  $(window).load(function(){
    jQuery("#all_id").removeClass('active');
  });
</script>
<!-- Start of HubSpot Embed Code --> <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/295496.js"></script> <!-- End of HubSpot Embed Code -->
