<?php
    $menuoff=1;
	$page_id=4;  
    include("config/config.php");	
	if($_SESSION["login"]!=1 && $_SESSION["grower"]=="" )
	{
	   header("location:".$siteurl);
	}
	if(isset($_POST["submit"]))
	{
		     $days="";
		     for($i=1;$i<=7;$i++)
			 {
				 if($_POST["day".$i]!="")
				 {
			     	$days.=$_POST["day".$i].",";
				 }
			 }
			 
			 $days=rtrim($days,',');
			 if($days!="")
			 {
				 $update_growers_days="update growers set growers_days='".$days."' where id='".$_SESSION["grower"]."'";
				 mysql_query($update_growers_days);
			 }
		     $today = date('mdyHis');
			 $tmp1 = $_FILES['stockfile']['name'];
			 $ext1 = explode('.',$tmp1);
			 $image=0;
			 $uploaddir = 'csvfiles/';
			 $uploadfile1 = $uploaddir.$today."big.".$ext1[1];
			 move_uploaded_file($_FILES['stockfile']['tmp_name'],$uploadfile1);
			 $csv_file=$uploadfile1;
			 if (($getfile = fopen($csv_file, "r")) !== FALSE) 
			 { 
				 $data = fgetcsv($getfile, 5000000, ",");
				 while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) 
				 {					 
				     $num = count($data); 
					 for ($c=0; $c < $num; $c++) 
					 {
						 $result = $data; 
						 $result = str_replace('""','|', $result);
						 $result = str_replace('"','|', $result);
						 $str = implode(',|', $result); 
						 $slice = explode(',|', $str);
						
					 }
					     $slice[8] = str_replace('|','', $slice[8]);
						 $slice[7] = str_replace('|','', $slice[7]);
						 $slice[6] = str_replace('|','', $slice[6]);
						if($slice[0]>1)
						{
							$slice[7]=str_replace(",",".",$slice[7]);
							$update="update grower_product_box_packing set stcok2='".$slice[8]."',nob='".$slice[9]."',price2='".$slice[7]."',comment='".$slice[6]."' where 
						    id='".$slice[0]."'";
						    mysql_query($update);
						}
				 }
				 
				header("location:http://staging.freshlifefloral.com/daily-stock-update.php?rf=1");
		     }
	}
		
?>

<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
 <style>
 .inner-content
 {
    margin-top:30px !important;
 }
 .brad
 {
	margin-bottom:0px;
	text-align:right;
	margin-right:269px;
  
 }
 .left-container .left-mid
 {
   margin-top:-25px;
 }
 </style>

<body>
<div class="wrapper">
<?php include("include/header.php"); ?>
<script type="text/javascript">
 function verify()
	{ 
		var arrTmp=new Array();
		arrTmp[5]=checkimage();
		var i;
		_blk=true;
		for(i=0;i<arrTmp.length;i++)
		{
			if(arrTmp[i]==false)
			{
			   _blk=false;
			}
		}

		if(_blk==true)
		{
			return true;
		}

		else
		{
			return false;
		}
 	}	

	function trim(str) 
	{    
		if (str != null) 
		{        
			var i;        
			for (i=0; i<str.length; i++) 
			{           

				if (str.charAt(i)!=" ") 
				{               
					str=str.substring(i,str.length);                 
					break;            
				}        

			}            

			for (i=str.length-1;i>=0;i--)
			{            
				if (str.charAt(i)!=" ") 
				{                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") 
			{            
				return "";         
			} 

			else 
			{            
				return str;         
			}    

		}

	}
	
	function checkimage()
	{

		if(trim(document.frmupload.stockfile.value) == "")
		{	 
			document.getElementById("lblstockfile").innerHTML="Please upload .csv file";
			return false;
		}
		else 
		{

			if(!validImageFile(document.frmupload.stockfile.value))
			{
				document.getElementById("lblstockfile").innerHTML="Please select valid file (.csv)";
				return false;
			}

			else
			{
				document.getElementById("lblstockfile").innerHTML="";
				return true;
			}
		}
	}
	
	function validImageFile(strfile)
	{

		var str = strfile;

		var pathLenth = strfile.length;

		var start = (str.lastIndexOf("."));

		var fileType = str.slice(start,pathLenth);

		fileType = fileType.toLowerCase();

		if (strfile.length > 0)
		{

		   if((fileType == ".csv")) 
		   {
				return true;

		   }

		   else 
		   {
				return false;
		   } 
		}
	}	
</script>	
  <div class="cl"></div>
  <div class="cl"></div>
  <div class="content-container inner-content">
       <div class="left-container" style="width:990px;">
      <div class="left-mid" style="width:990px; background:none; ">
        <div class="content_area" style="width:990px;">
          <h2>Update Stock</h2>
          <div style="clear:both"></div>
          <div class="contact-form">
        <form action="" method="post" name="frmupload" id="frmupload" onSubmit="return verify();" enctype="multipart/form-data" >
        <div class="form-row">
        <label style="width:205px;">Upload Your Inventory ( .csv file ) <span>*</span></label>
        <div class="right-form"><div class="">
        <input type="file" name="stockfile" id="stockfile" />
        <br/><span id="lblstockfile" style="color:red; font-size:12px; font-weight:bold;"></span>
        </div></div>
        <div class="cl"></div>
        </div>
        <div class="form-row">
        <label style="width:205px;">Days Avialable</label>
        <div class="right-form"><div class="">
        <b>Sunday</b> &nbsp;<input type="checkbox" name="day1" id="day1" value="0" /> &nbsp;&nbsp;
        <b>Monday</b> &nbsp;<input type="checkbox" name="day2" id="day2" value="1" /> &nbsp;&nbsp;
        <b>Tuesday</b> &nbsp;<input type="checkbox" name="day3" id="day3" value="2" /> &nbsp;&nbsp;
        <b>Wednesday</b> &nbsp;<input type="checkbox" name="day4" id="day4" value="3" /> &nbsp;&nbsp;
        <b>Thursday</b> &nbsp;<input type="checkbox" name="day5" id="day5" value="4" /> &nbsp;&nbsp;
        <b>Friday</b> &nbsp; <input type="checkbox" name="day6" id="day6" value="5" /> &nbsp;&nbsp;
        <b>Saturday</b> &nbsp; <input type="checkbox" name="day7" id="day7" value="6" /> &nbsp;&nbsp;
        </div></div>
        <div class="cl"></div>
        </div>
        <div class="form-row">
        <label>&nbsp; </label><div class="cl"></div><input type="submit" name="submit" class="submit" value="" /><div class="cl"></div></div>
        </form>
          </div>
      </div>
       <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <div class="cl"></div>
  </div>
  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
</div>
<?php include("include/fixeddiv.php"); ?>
</body>
</html>
<script tpe="text/javascript">
	function funPage(pageno)
	{
		document.frmfprd.startrow.value=pageno;
		document.frmfprd.submit();
	}
	function docolorchange()
	{
	   window.location.href='<?=$siteurl?>buy.php?id=<?=$_GET["id"]?>&categories=<?=$strdelete?>&l=<?=$_GET["l"]?>&c='+$('#color').val();
	}
	
	function frmsubmite()
	{
	  document.frmfilter.submit();
    }
	
	function doadd(id)
	{
	   var check = 0 ;
	   if($('#qty-'+id).val()=="" )
	   {
	       $('#ermsg-'+id).html("please enter box qty.")
		   check = 1 ;
	   }
	   if(check==0)
	   {
	     $('#ermsg-'+id).html("");
		 $('#gid6').val(id);
		 $('#frmrequest').submit();
	   }	 
	}
</script>
