<?php
require_once("config/config_new.php");
############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 15; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . 'includes/assets/css/essentials-flfv3.css', SITE_URL . 'includes/assets/css/layout-flfv3.css',
    SITE_URL . 'includes/assets/css/header-1-flfv3.css', SITE_URL . 'includes/assets/css/color_scheme/blue.css');
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

require_once 'includes/header.php';
?>
			<section class="page-header page-header-xs">
				<div class="container">

					<!-- breadcrumbs -->
					<ol class="breadcrumb breadcrumb-inverse">
						<li><a href="home-page.html">Home</a></li>
						<li><a href="#">Pages</a></li>
						<li class="active">Faq</li>
					</ol><!-- /breadcrumbs -->

				</div>
			</section>
			<!-- /PAGE HEADER -->



			<!-- -->
			<section>
				<div class="container">
					
					<div class="row">

						<!-- LEFT COLUMNS -->
						<div class="col-md-9">

							<p class="lead"><?php echo $pageData["para_1"]; ?></p>

							<!-- TOGGLES -->
							<div class="toggle toggle-transparent toggle-bordered-full margin-top-60">
							<?php 								
                            $box1_designation = explode(":", $pageData["box1_designation"]);
                            $box1_desc = explode(":", $pageData["box1_desc"]);
                            $box1_img = explode(":", $pageData["box1_img"]);
                            for ($i = 0; $i < count($box1_designation); $i++) {
                            if ($box1_designation[$i] != '') { 							
							?>
								<div class="toggle"><!-- toggle -->
									<label><?php echo ($i+1);?>. <?php echo $box1_designation[$i]; ?></label>
									<div class="toggle-content">					               
										<p class="clearfix">
										<?php echo $box1_desc[$i]; ?>
										</p>
										<?php  if ($box1_img[$i] != '') {?>
										<img class="hidden-sm  img-responsive" style="width:300px"src="<?php echo SITE_URL;?>user/<?php echo $box1_img[$i]; ?>" alt="Careers">
									
									 <?php }?>
									</div>
								</div><!-- /toggle -->
							<?php }}?>
							</div>
							<!-- /TOGGLES -->

							<!-- CALLOUT -->
							<div class="callout alert alert-border margin-top-60 margin-bottom-60">

								<div class="row">

									<div class="col-md-9 col-sm-12"><!-- left text -->
										<h4>Call now at <strong>+800-565-2390</strong> and get 15% discount!</h4>
										<p class="font-lato size-17">
											We truly care about our users and our product.
										</p>
									</div><!-- /left text -->

									
									<div class="col-md-3 col-sm-12 text-right"><!-- right btn -->
										<a href="#purchase" rel="nofollow" target="_blank" class="btn btn-default btn-lg">PURCHASE NOW</a>
									</div><!-- /right btn -->

								</div>

							</div>
							<!-- /CALLOUT -->

						</div>
						<!-- /LEFT COLUMNS -->

						<!-- RIGHT COLUMNS -->
						<div class="col-md-3">

							<!-- POPULAR QUESTIONS -->
							<h4><strong><?php echo $pageData["heading1"];?></h4>
							<p><em><?php echo $pageData["heading_desc1"]; ?></em></p>

							<hr>

							<ul class="list-unstyled"><!-- block 1 -->
								<?php
                    $para2 = explode(":", $pageData["para_2"]);
                    for ($i = 0; $i < count($para2); $i++) {
                        if ($para2[$i] != '') {
                            ?>
                            <li><a href="#">
                                <?php echo $para2[$i]; ?>
								</a>
                            </li>
                        <?php }
                    }
                    ?>	
							</ul><!-- /block 1 -->

							
							<hr>

							<!-- ASK A QUSTION -->
							<h4><strong>Ask</strong> a question</h4>
							<form action="#" method="post" class="sky-form clearfix">

								<label class="input">
									<i class="ico-prepend fa fa-user"></i>
									<input type="text" placeholder="Name">
								</label>

								<label class="input">
									<i class="ico-prepend fa fa-envelope"></i>
									<input type="text" placeholder="Email">
								</label>

								<label class="textarea">
									<i class="ico-prepend fa fa-comment"></i>
									<textarea rows="3" placeholder="Type your question..."></textarea>
								</label>

								<button class="btn btn-primary btn-sm pull-right">SUBMIT YOUR QUESTION</button>

							</form>

						</div>
						<!-- /RIGHT COLUMNS -->

					</div>
					
				</div>
			</section>
			<!-- / -->

<!-- /CLIENTS -->
<?php
require_once 'includes/footer.php';
?>
