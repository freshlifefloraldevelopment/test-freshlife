<?php
require_once("config/config_new.php");
//get current starting point of records
$items_per_group = 12;
$position = ($_REQUEST['group_no'] * $items_per_group);

if ($_REQUEST['cate_id'] != "") {
    $wh.="AND p.categoryid IN(" . $_REQUEST['cate_id'] . ")";
} elseif ($_REQUEST['selected_color'] != "") {
    $wh.="AND p.color_id IN(" . $_REQUEST['selected_color'] . ")";
} elseif ($_REQUEST['selected_sub_cat'] != "") {
    $wh.="AND p.subcategoryid IN(" . $_REQUEST['selected_sub_cat'] . ")";
}

//Limit our results within a specified range. 
$sel_products = "select p.* from product p 
			   left join grower_product gp on p.id=gp.product_id
			   left join growers g on gp.grower_id=g.id
			   where g.active='active' $wh GROUP BY p.id limit $position,$items_per_group";
//echo $sel_products;
//exit();	   
$rs_products = mysqli_query($con,$sel_products);
$number_row = mysqli_num_rows($rs_products);
if ($number_row > 0) {
    while ($products = mysqli_fetch_array($rs_products)) {
        //echo "<pre>";print_r($products);echo "</pre>";exit();

        $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

        $rs_catinfo = mysqli_query($con,$sel_catinfo);

        $catinfo = mysqli_fetch_array($rs_catinfo);

        $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));
        $absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/" . $products['image_path'];
       // if (file_exists($absolute_path)) {
            ?>
            <div class="col-md-4 col-sm-4 mix <?php echo $products['categoryid']; ?>"><!-- item -->

                <div class="item-box">
                    <figure>
                        <span class="item-hover">
                            <span class="overlay dark-5"></span>
                            <span class="inner">

                                <!-- lightbox -->
                                <a class="ico-rounded lightbox" href="<?php echo SITE_URL.$products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
                                    <span class="fa fa-plus size-20"></span>
                                </a>

                                <!-- details -->
                                <a class="ico-rounded" href="<?php echo SITE_URL.'variety-sub-page.php?id='.$products['id']; ?>">
                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                </a>

                            </span>
                        </span>
                        <div class="item-box-overlay-title">
                            <h3><?php echo $products["name"]; ?></h3>
                            <ul class="list-inline categories nomargin">
                                <li><a href="#"><?php echo $catinfo['name'] ?></a></li>
                            </ul>
                        </div>

                        <!-- carousel -->
                        <div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                            <div>
                                <img class="img-responsive" src="<?php echo SITE_URL.$products['image_path'] ?>" width="600" height="399" alt="">
                            </div>

                        </div>
                        <!-- /carousel -->

                    </figure>
                </div>

            </div><!-- /item -->
            <?php
       // }
    }
} else {
    ?>
    <div class="notfound" style="margin-left: 20px; clear: both;">No More Data!</div>
<?php
}
//}
?>
