<?php
require_once("../config/config_gcp.php");
//require_once("../functions/functions.php");

$qsel = "select gs.grower_id , gs.product_id , gs.sizes , gs.is_bunch , gs.is_bunch_value , gs.bunch_sizes , sh.name as sizename,
                p.id as pid             ,
                p.name as productname   ,
                p.box_type as p_box_type,
                bs.name as bunchname    ,
                gpb.boxes               ,
                gpb.box_value           ,
                b.name                  ,
                b.width , b.length , b.height , 
                b.type  ,
                GROUP_CONCAT(DISTINCT bt.name) as box_type_name,
                GROUP_CONCAT(DISTINCT bt.id) as box_type_id    ,
                sub_cat.name as sub_cat_name,fet.name as features_name
           from grower_product_bunch_sizes gs
          INNER JOIN grower_product_box as gpb on gpb.product_id = gs.product_id
           LEFT join boxes b    on gpb.boxes    = b.id
          INNER JOIN boxtype bt ON bt.id        = b.type
           LEFT join product p  on gs.product_id= p.id 
          INNER JOIN subcategory sub_cat ON sub_cat.id = p.subcategoryid 
           LEFT JOIN features as fet ON fet.id        = gs.feature
           LEFT join sizes sh        on gs.sizes      = sh.id
           LEFT join bunch_sizes bs  on gs.bunch_sizes= bs.id
          where gs.grower_id='" . $_REQUEST['id'] . "' 
            and p.name is not NULL 
          GROUP BY p.id,sh.name 
          ORDER by sub_cat.name , p.name , sh.name LIMIT 0,50";




$rs = mysqli_query($con, $qsel);
$pro_array = array();


while ($row_p = mysqli_fetch_array($rs)) {
            ?>
            <div class="col-md-4 col-sm-6 hidden-sm hidden-xs pricing-desc">
                <div class="pricing">
                    <ul class="list-unstyled">
                        <?php
                        $features_name = "";
                        if ($ava_p['features_name'] != "") {
                            $features_name = $ava_p['features_name'];
                        }
                        ?>       
                        <li>
                            <?php

                            $box_type_s = "";
                            if($row_p['p_box_type'] == "0")  {
                                $box_type_s = "Stems";
                            }else if($row_p['p_box_type'] == "1") {
                                $box_type_s = "Bunch";
                            }
                            
                            if ($row_p['is_bunch'] == 0) {
                                $total_kg_c = $row_p['width'] * $row_p['length'] * $row_p['height'] / 6000;
                                //echo $row_p['sub_cat_name'] . " " . $row_p['productname'] . " " . $row_p['sizename'] . "CM...." . " " . $features_name . " " . $row_p['bunchname'] . $box_type_s . $row_p['width'] . "X" . $row_p['length'] . "X" . $row_p['height'] . " (" . round($total_kg_c, 2) . " KG)";
                                echo $row_p['sub_cat_name'] . " " . $row_p['productname'] . " " . $row_p['sizename'] . "CM." . " " . $features_name . " " . $row_p['bunchname'] . $box_type_s ;                                
                            } else {
                                $total_kg_c = $row_p['width'] * $row_p['length'] * $row_p['height'] / 6000;
                                //echo $row_p['sub_cat_name'] . " " . $row_p['productname'] . " " . $row_p['sizename'] . "CM" . " " . $features_name . " " . $row_p['is_bunch_value'] . $box_type_s . $row_p['width'] . "X" . $row_p['length'] . "X" . $row_p['height'] . " (" . round($total_kg_c, 2) . " KG)";
                                echo $row_p['sub_cat_name'] . " " . $row_p['productname'] . " " . $row_p['sizename'] . "CM" . " " . $features_name . " " . $row_p['is_bunch_value'] . $box_type_s ;
                            }
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
            <?php
            $exp_boxtype_eb = explode(",", $row_p['box_type_id']);
            ?>
            <div class="col-md-2 col-sm-6 block">
                <div class="pricing">
                    <ul class="pricing-table list-unstyled">
                        <?php
                        
                        if (in_array("11", $exp_boxtype_eb)) {
                            $box_type_value = "SELECT gpb.boxes , b.type 
                                                 FROM grower_product_box as gpb 
                                                INNER JOIN boxes as b ON b.id=gpb.boxes
                                                WHERE gpb.grower_id  ='" . $_REQUEST['id'] . "' 
                                                  AND gpb.product_id ='" . $row_p["pid"] . "' 
                                                  AND b.type=11";    
                            
                            //exit();   error quitar              

                            $rs_box_val = mysqli_query($con, $box_type_value);
                            $row_box_val = mysqli_fetch_array($rs_box_val);

                            $sel_last_price = "select qty 
                                                 from grower_product_box_packing 
                                                where growerid  ='" . $_REQUEST['id'] . "' 
                                                  and prodcutid ='" . $row_p["pid"]   . "' 
                                                  and sizeid    ='" . $row_p["sizes"] . "' 
                                                  and box_id    ='" . $row_box_val['boxes'] . "' 
                                                order by id desc limit 0,1";                                                                                                                                                                                                                                                                       
                            
                            
                            //echo $sel_last_price;
                            //exit();
                            
                            $rs_last_price = mysqli_query($con, $sel_last_price);
                            $last_price = mysqli_fetch_array($rs_last_price);
                            
                            $qty_bunches = 0;
                            
                            if ($last_price['qty'] != "") {
                                $qty_bunches = $last_price['qty'];
                            } else {
                                $qty_bunches = 0;
                            }
                            ?>
                            <li><?php

                                if ($row_p['is_bunch'] == 0) {
                                    $total_stm = $row_p['bunchname'] * $qty_bunches;
                                    if ($total_stm == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                    } else {
                                        echo $total_stm . " Stems";
                                    }
                                } else {
                                    if ($qty_bunches == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                        } else {
                                            echo $qty_bunches . " Bunches";
                                        }
                                    }
                                    ?></li>
                            
                        <?php  } else { ?>   
                            <li>
                                <i class="fa fa-times"></i>
                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
<?php
$exp_boxtype_qb = explode(",", $row_p['box_type_id']);
?>
            <div class="col-md-2 col-sm-6 block">
                <div class="pricing">
                    <ul class="pricing-table list-unstyled">
                        <?php
                        
                        if (in_array("5", $exp_boxtype_qb)) {
                            $box_type_value = "SELECT gpb.boxes,b.type 
                                                 FROM grower_product_box  as gpb 
                                                INNER JOIN boxes as b ON b.id=gpb.boxes
                                                WHERE gpb.grower_id  ='" . $_REQUEST['id'] . "' 
                                                  AND gpb.product_id ='" . $row_p["pid"]   . "' 
                                                  AND b.type=5";    

                            $rs_box_val = mysqli_query($con, $box_type_value);
                            $row_box_val = mysqli_fetch_array($rs_box_val);

                            $sel_last_price = "select qty 
                                                 from grower_product_box_packing 
                                                where growerid ='" . $_REQUEST['id'] . "' 
                                                  and prodcutid='" . $row_p["pid"] . "' 
                                                  and sizeid   ='" . $row_p["sizes"] . "' 
                                                  and box_id   ='" . $row_box_val['boxes'] . "' 
                                                order by id desc limit 0,1";                                                     


                            //echo $sel_last_price;
                            //exit();
                            
                            
                            $rs_last_price = mysqli_query($con, $sel_last_price);
                            $last_price = mysqli_fetch_array($rs_last_price);
                            
                            $qty_bunches = 0;
                            
                            if ($last_price['qty'] != "") {
                                $qty_bunches = $last_price['qty'];
                            } else {
                                $qty_bunches = 0;
                            }
                            ?>
                            <li><?php

                                if ($row_p['is_bunch'] == 0) {
                                    $total_stm = $row_p['bunchname'] * $qty_bunches;
                                    if ($total_stm == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                    } else {
                                        echo $total_stm . " Stems";
                                    }
                                } else {
                                    if ($qty_bunches == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                        } else {
                                            echo $qty_bunches . " Bunches";
                                        }
                                    }
                                    ?></li>
                        <?php } else { ?>   
                            <li>
                                <i class="fa fa-times"></i>
                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
<?php
$exp_boxtype_hb = explode(",", $row_p['box_type_id']);
?>
            <div class="col-md-2 col-sm-6 block">
                <div class="pricing">
                    <ul class="pricing-table list-unstyled">
                        <?php
                        if (in_array("3", $exp_boxtype_hb)) {
                            $box_type_value = "SELECT gpb.boxes,b.type 
                                                 FROM grower_product_box  as gpb 
                                                INNER JOIN boxes as b ON b.id = gpb.boxes
                                                WHERE gpb.grower_id   ='" . $_REQUEST['id'] . "' 
                                                  AND gpb.product_id ='" . $row_p["pid"] . "' 
                                                  AND b.type=3";       

                            $rs_box_val = mysqli_query($con, $box_type_value);
                            $row_box_val = mysqli_fetch_array($rs_box_val);

                            $sel_last_price = "select qty 
                                                 from grower_product_box_packing 
                                                where growerid='" . $_REQUEST['id'] . "' 
                                                  and prodcutid='" . $row_p["pid"]  . "' 
                                                  and sizeid   ='" . $row_p["sizes"] . "' 
                                                  and box_id   ='" . $row_box_val['boxes'] . "' 
                                                order by id desc limit 0,1";                     
                            
                            
                            //echo $sel_last_price;
                            //exit();
                            $rs_last_price = mysqli_query($con, $sel_last_price);
                            $last_price = mysqli_fetch_array($rs_last_price);
                            $qty_bunches = 0;
                            if ($last_price['qty'] != "") {
                                $qty_bunches = $last_price['qty'];
                            } else {
                                $qty_bunches = 0;
                            }
                            ?>
                            <li><?php

                                if ($row_p['is_bunch'] == 0) {
                                    $total_stm = $row_p['bunchname'] * $qty_bunches;
                                    if ($total_stm == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                    } else {
                                        echo $total_stm . " Stems";
                                    }
                                } else {
                                    if ($qty_bunches == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                        } else {
                                            echo $qty_bunches . " Bunches";
                                        }
                                    }
                                    ?></li>
                        <?php } else { ?>   
                            <li>
                                <i class="fa fa-times"></i>
                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
<?php
$exp_boxtype_jumbo = explode(",", $row_p['box_type_id']);
?>
            <div class="col-md-2 col-sm-6 block">
                <div class="pricing">
                    <ul class="pricing-table list-unstyled">
                        <?php
                        if (in_array("10", $exp_boxtype_jumbo)) {
                            $box_type_value = "SELECT gpb.boxes,b.type 
                                                 FROM grower_product_box  as gpb 
                                                INNER JOIN boxes as b ON b.id=gpb.boxes
                                                WHERE gpb.grower_id  ='" . $_REQUEST['id'] . "' 
                                                  AND gpb.product_id ='" . $row_p["pid"]   . "' 
                                                  AND b.type=10";      

                            $rs_box_val = mysqli_query($con, $box_type_value);
                            $row_box_val = mysqli_fetch_array($rs_box_val);

                            $sel_last_price = "select qty 
                                                 from grower_product_box_packing 
                                                where growerid ='" . $_REQUEST['id'] . "' 
                                                  and prodcutid='" . $row_p["pid"] . "' 
                                                  and sizeid   ='" . $row_p["sizes"] . "' 
                                                  and box_id   ='" . $row_box_val['boxes'] . "' 
                                                order by id desc limit 0,1";                  
                            
                            //echo $sel_last_price;
                            //exit();
                            
                            $rs_last_price = mysqli_query($con, $sel_last_price);
                            $last_price = mysqli_fetch_array($rs_last_price);
                            
                            $qty_bunches = 0;
                            
                            if ($last_price['qty'] != "") {
                                $qty_bunches = $last_price['qty'];
                            } else {
                                $qty_bunches = 0;
                            }
                            ?>
                            <li><?php

                                if ($row_p['is_bunch'] == 0) {
                                    $total_stm = $row_p['bunchname'] * $qty_bunches;
                                    if ($total_stm == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                    } else {
                                        echo $total_stm . " Stems";
                                    }
                                } else {
                                    if ($qty_bunches == "0") {
                                        ?>
                                        <i class="fa fa-times"></i>
                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                        <?php
                                        } else {
                                            echo $qty_bunches . " Bunches";
                                        }
                                    }
                                    ?></li>
                        <?php } else { ?>   
                            <li>
                                <i class="fa fa-times"></i>
                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
<?php
    array_push($pro_array, $row_p['id']);
}
?> 
