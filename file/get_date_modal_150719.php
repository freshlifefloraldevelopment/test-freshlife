<?php

// PO 2018-07-02

require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$shippingMethod = $_POST['shippingMethod'];
$userSessionID = $_SESSION["buyer"];

$date = date('Y-m-d');
$delDate = $date;
$dayOfMonth = date('d', strtotime($date));
$monthOfYear = date('m', strtotime($date));
$year = date('Y', strtotime($date));

$getBuyerShippingMethod = "select * 
                            from buyer_shipping_methods 
                           where shipping_method_id ='" . $shippingMethod . "'";  // where shipping_method_id en vez de id

$buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
$buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
$leavingFarmDate = '';

if (!empty($buyerShippingMethod['shipping_method_id'])) {
    
    $shipping_method_id = $buyerShippingMethod['shipping_method_id'];
    $getShippingMethod = "select connections from shipping_method where id='" . $shipping_method_id . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);
    
    $conCount = count($connections);
    $trasit_time = 0;
    $days = array();
    
    if ($conCount == 1) {       // Si hay conecciones
        
        foreach ($connections as $connection) {
            $cost_un = unserialize($connection);

            foreach ($cost_un as $key => $value) {
                $value;
            }
        
            echo $value;
			
            $getConDetail = mysqli_query($con, "select (trasit_time) trasit_time , days ,(trasit_time+1) start_trasit  from connections where id='" . $connection . "'");    // IF (1)                    
            $conDetail = mysqli_fetch_assoc($getConDetail);
            
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $start_trasit = $trasit_time + $conDetail['start_trasit'];            
            
             $days = explode(',', $conDetail['days']);
            $daysOfWeekDisabled = $conDetail['days'];
        }


        if ($trasit_time > 1) {
            $prevDate = strtotime($delDate . ' +' . $trasit_time . ' days');
            $trasit_date = date("Y-m-d", $prevDate);
        } else {
            $prevDate = strtotime($delDate . '  +' . $trasit_time . ' day');
            $trasit_date = date("Y-m-d", $prevDate);
        }
        
        $trasitDay = date("w", strtotime($trasit_date));
        
        if (in_array($trasitDay, $days)) {
            $leavingFarmDate = $trasit_time;
        } else {
            $ttime = $trasit_time;
            switch (true) {
                case in_array($trasitDay + 1, $days):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay + 2, $days):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay + 3, $days):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay + 4, $days):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay + 5, $days):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay + 6, $days):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
            $leavingFarmDate = $ttime;
        }
        
    } /*
    else {

        $t = array();
        $dDate = $delDate;
        
        foreach ($connections as $connection) {
            
            $getConDetail = mysqli_query($con, "select trasit_time , days from connections where id='" . $value . "'");  // ELSE (2)
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            
            $t[] = $conDetail['trasit_time'];
            $days[] = $conDetail['days'];
            $day = explode(',', $conDetail['days']);
        }
        
        $daysOfWeekDisabled = $days[$conCount-1];
        $prevDates = strtotime($dDate . '  +' . $trasit_time . ' day');
        $trasitDay = date("w", strtotime($dDate));
        $ttime = 0;
        
        for ($j = 0; $j < $conCount; $j++) {
            $fday = explode(',', $days[$j]);
            $ttime = $ttime + $t[$j];
            
            if (in_array($trasitDay, $fday)) {
                $ttime = $ttime;
            } else {
                switch (true) {
                    case in_array($trasitDay + 1, $fday):
                        $ttime = $ttime + 1;
                        break;
                    case in_array($trasitDay + 2, $fday):
                        $ttime = $ttime + 2;
                        break;
                    case in_array($trasitDay + 3, $fday):
                        $ttime = $ttime + 3;
                        break;
                    case in_array($trasitDay + 4, $fday):
                        $ttime = $ttime + 4;
                        break;
                    case in_array($trasitDay + 5, $fday):
                        $ttime = $ttime + 5;
                        break;
                    case in_array($trasitDay + 6, $fday):
                        $ttime = $ttime + 6;
                        break;
                    default:
                        $ttime = $ttime + 1;
                }
            }
            $prevDates = strtotime($dDate . '  +' . $ttime . ' days');
            $dDates = date("Y-m-d", $prevDates);
            $trasitDay = date("w", strtotime($dDates));

        }   // End For

        $fday = explode(',', $days[0]);
        
        if (in_array($trasitDay, $fday)) {
            $ttime = $ttime;
        } else {
            switch (true) {
                case in_array($trasitDay + 1, $fday):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay + 2, $fday):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay + 3, $fday):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay + 4, $fday):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay + 5, $fday):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay + 6, $fday):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
        }
        $prevDatess = strtotime($dDate . '  +' . $ttime . ' days');
        $dDatess = date("Y-m-d", $prevDatess);
        $leavingFarmDate = $ttime;
    }
    */
    //$leavingFarmDate = 2; //General para todos star_transit
    
    $leavingFarmDate = $start_trasit;    
    $startDate = date('Y-m-d', strtotime("+".$leavingFarmDate." days"));
       
    //$startDate = date('Y-m-d');  -- po
    $totalDays = array('0','1','2','3','4','5','6');
    $daysOfWeekDisabled = explode(',',$daysOfWeekDisabled);
    
    //saco el numero de elementos
      $longitud = count($daysOfWeekDisabled);
      
      for($i=0; $i<$longitud; $i++)  {
	   $daysOfWeekDisabled[$i] = $daysOfWeekDisabled[$i] + $trasit_time;

                           $newWeek = 7;
                           
           if ($daysOfWeekDisabled[$i] > 7) { // validar si es que es mayor a 14 Tercera Semana
                // $daysOfWeekDisabled[$i] = $daysOfWeekDisabled[$i] - 7;
               
                $daysOfWeekDisabled[$i] = 1;
                $startDate = date('Y-m-d', strtotime("+".$newWeek." days"));
                 
           } else if (($daysOfWeekDisabled[$i] == 7)){   // Validacion dia Domingo Pasa al Lunes             
                 $daysOfWeekDisabled[$i] = 1;
                 $startDate = date('Y-m-d', strtotime("+".$newWeek." days"));
           }
      }            
          
    $result=array_diff($totalDays,$daysOfWeekDisabled);
    $daysOfWeekDisabled = implode(',',$result);
    
    $val = '';

}else if(!empty($buyerShippingMethod['choose_shipping'])){
    
    $getMethod = "select * from tax_relations  where shipping_method_name='".$buyerShippingMethod['choose_shipping']."'";
    $getMethodRes = mysqli_query($con, $getMethod);
    $methodDetail = mysqli_fetch_assoc($getMethodRes);
    $shipping_method_id = $methodDetail['shipping_option'];
    
    $getShippingMethod = "select connections from shipping_method where id='" . $shipping_method_id . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);

    $conCount = count($connections);
    $trasit_time = 0;
    $days = array();
    
    if ($conCount == 1) {
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "select trasit_time , days from connections where id='" . $value . "'");  // (3)
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $days = explode(',', $conDetail['days']);
            $daysOfWeekDisabled = $conDetail['days'];
            $trasit_time_sh=$conDetail['trasit_time'];
        }
        /* CH Code*/

        $timestamp = strtotime('next Sunday');
        $new_date_array=array();
        
        for ($i = 0; $i < 7; $i++) {
            $client_date=0;
            $timestamp = strtotime('+1 day', $timestamp);
            $date_number=date("N",$timestamp) - 1;
            
            if(in_array($date_number,$days))  {

                if($date_number == 6)     {
                    $date_number=0;
                    $client_date = $date_number + $trasit_time_sh;
                    array_push($new_date_array,$client_date);  
                }else{
                    $client_date = $date_number + $trasit_time_sh + 1;

                    if($client_date > 6) {
                          $client_date=$client_date - 6;
                        if($client_date == 0) {
                            $client_date=0;   
                        }
                        array_push($new_date_array,$client_date);   
                    }else{
                        array_push($new_date_array,$client_date);
                    }   
                }
                
            }
        }

        
        /* CH Code*/

        if ($trasit_time > 1) {
            $prevDate = strtotime($delDate . ' +' . $trasit_time . ' days');
            $trasit_date = date("Y-m-d", $prevDate);
        } else {
            $prevDate = strtotime($delDate . '  +' . $trasit_time . ' day');
            $trasit_date = date("Y-m-d", $prevDate);
        }
        
        $trasitDay = date("w", strtotime($trasit_date));
        
        if (in_array($trasitDay, $days)) {
            $leavingFarmDate = $trasit_time;
        } else {
            $ttime = $trasit_time;
            switch (true) {
                case in_array($trasitDay + 1, $days):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay + 2, $days):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay + 3, $days):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay + 4, $days):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay + 5, $days):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay - 6, $days):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
           $leavingFarmDate = $ttime;
        }
    } else {
        $t = array();
        $dDate = $delDate;
        
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "select  trasit_time , days from connections where id='" . $value . "'"); // (4)
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $t[] = $conDetail['trasit_time'];
            $days[] = $conDetail['days'];
            $day = explode(',', $conDetail['days']);
        }

        $daysOfWeekDisabled = $days[$conCount-1];
        $days_array=array();
        $days_array=explode(",",$daysOfWeekDisabled);
        
        /* CH Code*/
        $trasit_time_sh=$t[$conCount-1];
        $timestamp = strtotime('next Saturday');
        $new_date_array=array();
        
        for ($i = 0; $i < 7; $i++) {
            $client_date=0;
            $timestamp = strtotime('+1 day', $timestamp);
            $date_number=date("N",$timestamp) - 1;
            
            if(in_array($date_number,$days_array)){
                if($date_number == 6) {
                    $date_number=0;
                    $client_date = $date_number + $trasit_time_sh;
                    array_push($new_date_array,$client_date);  
                }else{
                    $client_date = $date_number + $trasit_time_sh + 1;
                    if($client_date > 6)  {
                        $client_date=$client_date - 7;
                        if($client_date == 0) {
                            $client_date=1;   
                        }
                        array_push($new_date_array,$client_date);   
                    }else{
                        array_push($new_date_array,$client_date);
                    }  
                }
                 
            }

        }
        
        
        $prevDates = strtotime($dDate . '  +' . $trasit_time . ' day');
        $trasitDay = date("w", strtotime($dDate));
        $ttime = 0;
        
        for ($j = 0; $j < $conCount; $j++) {
            
            $fday = explode(',', $days[$j]);
            $ttime = $ttime + $t[$j];
            
            if (in_array($trasitDay, $fday)) {
                $ttime = $ttime;
            } else {
                switch (true) {
                    case in_array($trasitDay + 1, $fday):
                        $ttime = $ttime + 1;
                        break;
                    case in_array($trasitDay + 2, $fday):
                        $ttime = $ttime + 2;
                        break;
                    case in_array($trasitDay + 3, $fday):
                        $ttime = $ttime + 3;
                        break;
                    case in_array($trasitDay + 4, $fday):
                        $ttime = $ttime + 4;
                        break;
                    case in_array($trasitDay + 5, $fday):
                        $ttime = $ttime + 5;
                        break;
                    case in_array($trasitDay + 6, $fday):
                        $ttime = $ttime + 6;
                        break;
                    default:
                        $ttime = $ttime + 1;
                }
            }
            $prevDates = strtotime($dDate . '  +' . $ttime . ' days');
            $dDates = date("Y-m-d", $prevDates);
            $trasitDay = date("w", strtotime($dDates));
        }

        $fday = explode(',', $days[0]);
        
        if (in_array($trasitDay, $fday)) {
            $ttime = $ttime;
        } else {
            switch (true) {
                case in_array($trasitDay + 1, $fday):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay + 2, $fday):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay + 3, $fday):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay + 4, $fday):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay + 5, $fday):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay + 6, $fday):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
        }
        $prevDatess = strtotime($dDate . '  +' . $ttime . ' days');
        $dDatess = date("Y-m-d", $prevDatess);
        $leavingFarmDate = $ttime;
    }
    
    $startDate = date('Y-m-d', strtotime("+".$leavingFarmDate." days"));
    $totalDays = array('0','1','2','3','4','5','6');
    
    $daysOfWeekDisabled = explode(',',$daysOfWeekDisabled);  
    $result=array_diff($totalDays,$new_date_array);         
    $daysOfWeekDisabled = implode(',',$result);
      
    $val = '';
    
}else {
    
    $startDate = date('Y-m-d');
    $daysOfWeekDisabled = '';
    $val = $startDate;
}

 
?>
<input type="text" class="form-control datepicker cls_custom_date" id="deliveryDate_<?php echo $_REQUEST['product_id'].'_'.$_REQUEST['index'];?>" days-of-week-disabled="<?php echo $daysOfWeekDisabled; ?>"  data-todayHighlight='false' value="<?php echo(!empty($val))? $startDate : ''; ?>" start-date="<?php echo $startDate; ?>" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" placeholder="Select Date" style="width: 400px!important;text-indent: 32px;border-radius: 5px;">
