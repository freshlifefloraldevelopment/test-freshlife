<?php 
require_once("../config/config_gcp.php");


if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login-type"] != "grower") {
    header("location:" . SITE_URL);
    die;
}
$userSessionID = $_SESSION["grower"];

//update profile data
if (isset($_POST['update_profile']) && (!empty($_POST['id']))) {

    $no_error = 1;
    $update_error = '';

    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];

    $country = $_POST['country'];
    $web_site = $_POST['web_site'];
    $page_desc = $_POST['page_desc'];
    $short_desc = $_POST['short_desc'];

    $is_public = $_POST['is_public'];
    $biographical_info = $_POST['biographical_info'];
    $profile_image = $_POST['profile_image_old'];

    $password = $_POST['password'];
    $repeat_password = $_POST['repeat_password'];

    if (!empty($password) && $password != $repeat_password) {
        $update_error = "Password did not match";
        $no_error = 0;
    }
    if (!empty($password) && strlen($password) <= 5) {
        $update_error = "Password should be greater then 5 character";
        $no_error = 0;
    }
    if (empty($first_name)) {
        $update_error = "Please fill the First Name";
        $no_error = 0;
    }
    if (empty($last_name)) {
        $update_error = "Please fill the Last Name";
        $no_error = 0;
    }
    if (empty($phone)) {
        $update_error = "Please fill the Phone";
        $no_error = 0;
    }

    if ($is_public) {
        $is_public = 1;
    } else {
        $is_public = 0;
    }

    if (!empty($_FILES["profile_image"]["name"])) {
        
		$target_path  = "images/profile_images/"; 
		$target_path1 = "images/profile_images/";
                
		$target_path = $target_path . basename( $_FILES['profile_image']['name']); 
		$imageFileType = pathinfo($target_path, PATHINFO_EXTENSION);   

                $file_name = $id . time() . "." . $imageFileType;
                $target_file = $target_path1 . $file_name;                

        $check = getimagesize($_FILES["profile_image"]["tmp_name"]);
        
        if ($check == false) {
            $update_error = "File is not an image.";
            $no_error = 0;
        }

        // Check file size
        if ($_FILES["profile_image"]["size"] > 1000000) {
            $update_error = "Sorry, your file is too large.";
            $no_error = 0;
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            $update_error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $no_error = 0;
        }
        if ($no_error == 1) {
            if (move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file)) {
                $profile_image = $file_name;
            }
        }
    }

    if (!empty($password) && $no_error == 1) {
        $stmt = $con->prepare("UPDATE admin SET pass=? WHERE grower_id=? AND type='Grower'");
        $stmt->bind_param("si", $password, $id);
        $stmt->execute();
        $stmt->close();
    }


    if ($no_error == 1) {
        $growers_name = $first_name . ' ' . $last_name;
        $stmt = $con->prepare("UPDATE growers SET growers_name=?, phone=?, address=?, country_id=?, website=?, page_desc=?, short_desc=?, is_public=?, biographical_info=?, profile_image=? WHERE id=?");
        $stmt->bind_param("ssssssssssi", $growers_name, $phone, $address, $country, $web_site, $page_desc, $short_desc, $is_public, $biographical_info, $profile_image, $id);
        $stmt->execute();
        $stmt->close();

        $update_msg = "Profile Update Successfully";

    }
}

//Delete Avatar
if (isset($_GET['delete_avatar']) && (!empty($_GET['id']))) {
    $user_id = $_GET['id'];
    if ($stmt = $con->prepare("SELECT id, profile_image FROM growers WHERE id =?")) {
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->bind_result($userID, $profile_image);
        $stmt->fetch();
        $stmt->close();

        unlink('images/profile_images/' . $profile_image);

        $stmt = $con->prepare("UPDATE growers SET profile_image='' WHERE id=?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $stmt->close();

        header("location:vendor-account.php?uam=1");
    }

}
if (isset($_GET['uam'])) {
    $update_msg = "Avatar image deleted successfully";
}

if ($stmt = $con->prepare("SELECT g.id as gid,g.growers_name,uname as email,g.phone,g.address,country_id,g.website,g.page_desc,g.short_desc, g.is_public, g.biographical_info, g.profile_image
 FROM growers g,admin a WHERE a.grower_id=g.id AND g.id =?")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $growers_name, $email, $phone, $address, $country, $website, $page_desc, $short_desc, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*********If not exist send to home page****************/
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*********If not statement send to home page****************/
    header("location:" . SITE_URL);
    die;
}
//manage labels for form area 
if ($_SESSION["lang"] == "ru") {
    $labels_arr['first_name'] = '????????????';
    $labels_arr['last_name'] = '???????';
    $labels_arr['email'] = 'Email';
    $labels_arr['phone'] = '???????';
    $labels_arr['address'] = '?????';
    $labels_arr['city'] = '?????';
    $labels_arr['state_text'] = '???????????';
    $labels_arr['zip'] = '???????? ??????';
    $labels_arr['country'] = 'Country';
    $labels_arr['web_site'] = '????';
    $labels_arr['company'] = '???????? ????????';
} else {
    $labels_arr['first_name'] = 'First Name';
    $labels_arr['last_name'] = 'Last Name';
    $labels_arr['email'] = 'Email';
    $labels_arr['phone'] = 'Phone';
    $labels_arr['address'] = 'Address';
    $labels_arr['city'] = 'City';
    $labels_arr['state_text'] = 'State';
    $labels_arr['zip'] = 'Zip';
    $labels_arr['country'] = 'Country';
    $labels_arr['web_site'] = 'Web Site';
    $labels_arr['company'] = 'Company';
}

$img_url = 'images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = 'images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<script>
    function validate_profile_form(theForm) {

        var error = 0;
        if (theForm.first_name.value == "" || theForm.first_name.value == "Full Name") {
            $('.error_first_name').html('Please fill the First Name');
            theForm.first_name.focus();
            $("#first_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.last_name.value == "" || theForm.last_name.value == "Last Name") {
            $('.error_last_name').html('Please fill the Last Name');
            theForm.last_name.focus();
            $("#last_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.phone.value == "" || theForm.phone.value == "Phone") {
            $('.error_phone').html('Please fill the Phone');
            theForm.phone.focus();
            $("#phone").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }

        if (error == 1) {
            return false;
        } else {
            return true;
        }
    }

    function remove_validate_text(id) {
        $('.error_' + id).html('');
        $("#" + id).removeAttr("style");
    }

    function check_password(current_element) {
        var pass_val = $('#password').val();
        var rep_pass_val = $('#repeat_password').val();

        console.log(pass_val.length);
        if (pass_val.length > 0) {
            if (current_element == 'password') {
                if (pass_val.length <= 5) {
                    $('.error_password').html("Password should be greater then 5 character");
                    $('#password').attr("style", "border:1px solid #FF0000;");
                } else {
                    $('.error_password').html("");
                    $('#password').removeAttr("style");
                }
            }

            if (current_element == 'repeat_password') {
                if (pass_val != rep_pass_val) {
                    $('.error_repeat_password').html("Password did not match!");
                    $('#repeat_password').attr("style", "border:1px solid #FF0000;");
                } else {
                    $('.error_repeat_password').html("");
                    $('#repeat_password').removeAttr("style");
                }
            }
        }
    }
</script>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_growers.php"; ?>
<!-- /ASIDE -->

<!--
            MIDDLE
        -->
<section id="middle">
    <div id="content" class="padding-20">

        <div class="page-profile">
            <?php
            if ($update_msg) {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_msg . '</div>';
            } elseif ($update_error) {
                echo '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_error . '</div>';
            }
            ?>
            <div class="row">

                <!-- COL 1 -->
                <div class="col-md-4 col-lg-3">
                    <section class="panel">
                        <div class="panel-body noradius padding-10">

                            <figure class="margin-bottom-10"><!-- image -->
                                <img class="img-responsive" src="<?php echo "/".$img_url ?>" alt="">
                            </figure><!-- /image -->

                            <!-- progress bar -->
                            <h6 class="progress-head">Profile Completion <span class="pull-right">60%</span></h6>
                            <div class="progress progress-xs">
                                <div class="progress-bar" role="progressbar" style="width: 60%;"></div>
                            </div><!-- /progress bar -->

                            <!-- updated -->
                            <ul class="list-unstyled size-13">
                                <li class="text-gray"><i class="fa fa-check"></i> Confirm Your Account</li>
                                <?php
                                if ($profile_image) {
                                    echo '<li class="text-gray"><i class="fa fa-check"></i> Update Profile Picture</li>';
                                } else {
                                    echo '<li><i class="fa fa-close"></i> Update Profile Picture</li>';
                                }
                                if ($address) {
                                    echo '<li class="text-gray"><i class="fa fa-check"></i> Update Your Address</li>';
                                } else {
                                    echo '<li><i class="fa fa-close"></i> Update Your Address</li>';
                                }
                                ?>
                            </ul><!-- /updated -->

                            <hr class="half-margins">

                            <!-- About -->
                            <h3 class="text-black">
                                Melisa Doe
                                <small class="text-gray size-14"> / CEO</small>
                            </h3>
                            <p class="size-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet dolore magna aliquam tincidunt erat
                                volutpat.</p>
                            <!-- /About -->

                            <hr class="half-margins">


                        </div>
                    </section>
                </div><!-- /COL 1 -->

                <!-- COL 2 -->
                <div class="col-md-8 col-lg-6">

                    <div class="tabs white nomargin-top">
                        <ul class="nav nav-tabs tabs-primary">

                            <li class="active">
                                <a href="#edit" data-toggle="tab">Edit</a>
                            </li>
                        </ul>

                        <div class="tab-content">


                            <?php $name = explode(" ", $growers_name);
                            $first = $name[0];
                            $last_name = $name[1]; ?>
                            <!-- Edit -->
                            <div id="edit" class="active tab-pane">

                                <form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" onsubmit="javascript: return validate_profile_form(profile_form);">
                                    <h4>Personal Information</h4>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_first_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="first_name"><?php echo $labels_arr['first_name']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $first; ?>" onkeypress="javascript:remove_validate_text('first_name');">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_last_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="last_name"><?php echo $labels_arr['last_name']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="last_name" value="<?php echo $last_name; ?>" name="last_name" onkeypress="javascript:remove_validate_text('last_name');">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="email"><?php echo $labels_arr['email']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" readonly="" name="email" class="form-control" id="profileEmail" name value="<?php echo $email; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_phone"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="phone"><?php echo $labels_arr['phone']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $phone; ?>" onkeypress="javascript:remove_validate_text('phone');">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="address"><?php echo $labels_arr['address']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="address" id="address" value="<?php echo $address; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="country"><?php echo $labels_arr['country']; ?></label>
                                            <div class="col-md-8">
                                                <select size="1" name="country" id="country" class="form-control">
                                                    <option value="">Country (required)</option>
                                                    <?
                                                    if ($stmt = $con->prepare("select id,name,flag from country order by name")) {
                                                        $stmt->bind_param('i', $userSessionID);
                                                        $stmt->execute();
                                                        $stmt->bind_result($cid, $cname, $cflag);
                                                        while ($stmt->fetch()) {
                                                            $select = '';
                                                            if ($country == $cid) {
                                                                $select = "selected='selected'";
                                                            }
                                                            echo '<option value="' . $cid . '" ' . $select . '>' . $cname . '</option>';
                                                        }
                                                        $stmt->close();

                                                    }

                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="web_site"><?php echo $labels_arr['web_site']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="web_site" name="web_site" value="<?php echo $website; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileBio">Description</label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" rows="3" id="profileBio" name="page_desc"><?php echo $page_desc; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileBio">Short Description</label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" rows="3" id="profileBio" name="short_desc"><?php echo $short_desc; ?></textarea>
                                            </div>
                                        </div>

                                    </fieldset>

                                    <hr>

                                    <h4>About</h4>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Public Profile</label>
                                            <div class="col-md-8">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="1" <?php if ($is_public){ ?>checked="checked" <?php } ?> id="profilePublic" name="is_public">
                                                    <i></i></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileBio">Biographical Info</label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" rows="3" id="profileBio" name="biographical_info"><?php echo $biographical_info; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="sky-form">
                                                <label class="col-xs-3 control-label">Profile Image</label>
                                                <div class="col-md-8">
                                                    <label for="file" class="input input-file">
                                                        <div class="button">
                                                            <input type="file" name="profile_image" id="file" onChange="document.getElementById('fake_button').value = this.value">
                                                            Browse
                                                        </div>
                                                        <input type="text" id="fake_button" readonly="">
                                                    </label>
                                                    <?php
                                                    if ($profile_image) {
                                                        echo '<a href="vendor-account.php?delete_avatar=1&id=' . $userID . '" class="btn btn-danger btn-xs nomargin"><i class="fa fa-times"></i> Remove Current Image</a>';
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($profile_image) {
                                                        ?>
                                                        <label class="col-xs-3 control-label">&nbsp;</label>
                                                        <div class="col-md-8">
                                                            <img class="img-responsive"
                                                                 style="width:100px;height:80px;margin-top:10px;" alt=""
                                                                 src="images/profile_images/<?php echo $profile_image; ?>">
                                                        </div>
                                                    <?php } ?>                                                    
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>

                                    <hr>

                                    <h4>Change Password</h4>
                                    <fieldset class="mb-xl">
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_password"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="profileNewPassword">New Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="password" onkeyup="javascript:check_password('password');" class="form-control" id="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_repeat_password"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="profileNewPasswordRepeat">Repeat New Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="repeat_password" id="repeat_password" onkeyup="javascript:check_password('repeat_password');" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                                            <input type="hidden" name="profile_image_old" value="<?php echo $profile_image; ?>">
                                            <input type="hidden" name="update_profile" value="1">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </div><!-- /COL 2 -->
                <!-- COL 3 -->
                <div class="col-md-12 col-lg-3">
                    <!-- projects -->
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <h2 class="panel-title elipsis">
                                <i class="fa fa-rss"></i> Customer's Requests
                            </h2>
                        </header>


                        <div class="panel-body noradius padding-10">

                            <ul class="comment list-unstyled">
                                <?php
                                $query = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,
                                            p.image_path,s.name as subs,sh.name as sizename,ff.name as featurename,rg.gprice from buyer_requests gpb
													            inner join request_growers rg on gpb.id=rg.rid
													            left join product p on gpb.product = p.id
													            left join subcategory s on p.subcategoryid=s.id  
													            left join features ff on gpb.feature=ff.id
													            left join grower_offer_reply gor on rg.rid=gor.offer_id and rg.gid=gor.grower_id
													            left join sizes sh on gpb.sizeid=sh.id where rg.gid='" . $userSessionID . "' and gor.id 
													            is null and gpb.lfd>='" . date("Y-m-d") . "'  group by gpb.id order by gpb.id desc";
                                $result = mysqli_query($con, $query);
                                $number_of_ven_request = mysqli_num_rows($result);
                                if ($number_of_ven_request > 0) {
                                    while ($row_customer_request = mysqli_fetch_array($result)) {
                                        $query_b = "select first_name,last_name,profile_image from buyers where id='" . $row_customer_request['buyer'] . "'";

                                        $result_b = mysqli_query($con, $query_b);
                                        $row_b = mysqli_fetch_array($result_b);

                                        $img_url = 'images/profile_images/noavatar.jpg';
                                        $profile_image = $row_b['profile_image'];
                                        if ($profile_image) {
                                            $img_url = 'images/profile_images/' . $profile_image;
                                        }
                                        ?>
                                        <li class="comment grower-comments">
                                            <!-- avatar -->
                                            <!--<img class="avatar_left" src="<?php echo $img_url ?>" width="50" height="50" alt="avatar">
-->
                                            <div class="avter_left">
                                                <img class="avatar-grower-grower" alt="avatar" src="<?="/".$img_url?>" width="100"  class="avatar">
                                            </div>



                                            <div class="comment-body comment-right">
                                                <a href="#" class="comment-author">
                                                    <span><?php echo $row_b['first_name'] . " " . $row_b['last_name']; ?></span>
                                                </a>
                                                <p>
                                                    <?= $row_customer_request["subs"] ?> <?= $row_customer_request["name"] ?> <?= $row_customer_request["featurename"] ?> <?= $row_customer_request["sizename"] ?>cm
                                                </p>
                                            </div>
                                        </li><!-- /options -->















                                        <?php
                                    }
                                } else {
                                    echo "<li class='comment grower-comments'> Requests Not Found</li>";
                                }


                                ?>
                            </ul>
                        </div>
                    </section>
                    <!-- /projects -->
                </div><!-- /COL 3 -->

            </div>

        </div>

    </div>
</section>
<?php require_once("../includes/profile-footer.php"); ?>
