<?php require("navigation.php") ?>
<style type="text/css">
#in-con #inner #inn p strong a {
	color: #F0F;
}
</style>

<div id="in-con">
  <table>
    <tr>
      <td style="padding-left:5px;"><img src="images/roomhire.jpg" alt="Seminar Facility Australia" title="Seminar Facility Australia" width="720" height="200" /></td>
    </tr>
  </table>
	<table>
		<tr>
			<td style="text-align:justify"><strong> Our Services >> Seminar Room Hire </strong><br>      
		</tr>    
	</table>  
  <div id="inner">
    <div id="inn">
      <p>Training Choice provide flexible Meeting Rooms designed to suit small gatherings as well as large conventions. Our conference room rentals are available at prime locations in major cities in Australia . Our conference rooms for hire are flexible and offer the perfect meeting venue, whether you are a travelling executive looking for a place to meet clients or a corporate leader looking to convene a large gathering of shareholders. </p>
      
      <p>
      It sounds too good to be true, but you don't have to have your own Seminar room to experience the benefits of having a designated space in which to discuss your business.Training Choice has offices in numerous locations in key business districts across Australia and the world that offer seminar room facilities at an affordable rate .</p>

<p>Hire quality seminar facilities for as short or as long as you need them, and forget having to fork out the expense of setting up your own.In fact, Training Choice has become the name in Australia that is synonymous with quality seminar rooms for a cheap rate.</p>

    
      <p>The seminar room is equipped with an overhead projector, PC, Internet access, VCR, DVD, TV, CD and radio, all presented via large format video projection.Training Choice Worldwide offers you, your  participants and your clients these fantastic features with all seminar rooms hire :<br>
      
              <p><br>
                ▪ Personalised meet & greet upon arrival
                ▪ Complimentary data projector<br/>
                ▪ Complimentary refreshments consisting of cookies & biscuits or the option of a catered gourmet morning & afternoon tea and lunch at a fee. <br/>
                ▪ A dedicated catered break out area <br/>
                ▪ Majority of our rooms provide natural lighting <br/>
                ▪ FREE Internet - WIFI<br>
                ▪ FREE Tea and coffee<br>
                ▪ FREE Dedicated on-site IT support<br>
                ▪ FREE Dedicated admin support</p>
                ▪ <strong style="color:#F749C2;">NEW</strong>: Virtual training solution<br>
                 ▪ <strong style="color:#F749C2;">NEW</strong>: <a href="catalogue/VIDEO_RECORDING_FACT_SHEET.pdf" target="_blank"><strong>Download Video Recording Product Sheet</strong></a><br>
                ▪ Comfortable, stylish and well-furnished  break-out areas<br>
                ▪ Enthusiastic and professional staff<br>
                ▪ High level of hospitality and customer  service<br>
                ▪ Logistical excellence<br>
                ▪ Premium catering service: <strong><a href="catalogue/CATERING_TC_2012.pdf" target="_blank">Download Catering Menu</a></strong></p>
                <p>▪  Flip-chart<br>
                 ▪ Data Projector<br>
                ▪ Fibre Optic Internet <br>
               
                ▪ AV Station (LCD TV, Digital Camera,  DVD player)<br>
                ▪ Other equipment available upon request<br>
                <br>
                For any equipment or supplies please download our <a href="catalogue/Training_Choice_Catalogue_RH.pdf" target="_blank"><strong>CATALOGUE</strong></a><br>
          </p>
          
          
           <p>Training Choice Australia belongs to one of the largest international networks of training facility providers, Training Choice Venues are located in prestigious areas across Australia's capital cities and central business districts.<br/>Locations include:</p>
               
               <p>
                Sydney – Level 6, 1 Elizabeth Plaza, North Sydney, 2060<br/>
                Melbourne - Level 3, 455 Bourke Street, Melbourne, 3000<br/>
                Brisbane – Mezzanine level, 88 Creek St, Brisbane, 4000<br/>
                Canberra - Level 4, 54 Marcus Clarke Street. ACT 2601<br/>
                Perth - Level 7, 105 St Georges Terrace, Perth, WA, 6000<br/>
                Adelaide - 19 Young Street , Adelaide, 5000<br/>
                *Rural locations are provided through partner organisations throughout<br/>
                Australia
               </p>
               
               
                  <p>
               For more information on Training Choice's services and continued commitment to world class training support, please contact us at our Australian headquarters on:<br/>
               Ph: (02) 9460 0666<br/>
               Email: info@training-choice.com<br/>
               Fax: (02) 9460 0622<br/>
               </p>
               
               
               
          </div>
</div>
  </div>
</div>
</div>
<?php require("footer.php") ?>
        </div>
</div>
