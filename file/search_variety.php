<?php
require_once("config/config_new.php");
$q = rtrim($_REQUEST['searchword'], ",");
$grower_checked = trim($_REQUEST['grower_checked'], ",");
$color_selected = trim($_REQUEST['selected_color'], ",");
$varieties_checked = trim($_REQUEST['varieties_checked'], ",");
$wh = "";
if ($q != "") {
    //$wh.=" AND p.name LIKE '".$q."%'";
    $wh.=" AND p.id IN(" . $q . ")";
}
if (trim($_REQUEST['subcategory_selected'], ",") != "") {
    if ($_REQUEST['selected_color'] != "") {
        $wh.=" AND gp.subcaegoryid IN (" . trim($_REQUEST["subcategory_selected"], ",") . ") AND p.color_id IN (" . $color_selected . ")";
    } else {
        $wh.=" AND gp.subcaegoryid IN (" . trim($_REQUEST["subcategory_selected"], ",") . ")";
    }
}
if ($q != "") {
    if ($_REQUEST["growers_id"] != "") {
        $wh.=" OR gp.grower_id='" . $_REQUEST["growers_id"] . "'";
    }
} else {
    if ($_REQUEST["growers_id"] != "") {
        $wh.=" AND gp.grower_id='" . $_REQUEST["growers_id"] . "'";
    }
}

if ($color_selected != "") {
    $wh.=" AND p.color_id IN (" . $color_selected . ")";
}

if ($varieties_checked != "") {
    $wh.=" AND p.id IN (" . $varieties_checked . ")";
}

if ($_REQUEST['availability_check'] != "") {
    $wh.=" AND g.active='" . $_REQUEST['availability_check'] . "'";
}

if ($wh == "") {
    $sel_products = "select p.* from product p 
		   left join grower_product gp on p.id=gp.product_id
		   left join growers g on gp.grower_id=g.id
		   where g.active='active' limit 0,15";

    $rs_products = mysqli_query($con,$sel_products);
    $number_row = mysqli_num_rows($rs_products);
    if ($number_row > 0) {
        while ($products = mysqli_fetch_array($rs_products)) {
            //echo "<pre>";print_r($products);echo "</pre>";exit();
            $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

            $rs_catinfo = mysqli_query($con,$sel_catinfo);

            $catinfo = mysqli_fetch_array($rs_catinfo);

            $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));
            $absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/" . $products['image_path'];
           // if (file_exists($absolute_path)) {
                ?>  
                <div class="col-md-4 col-sm-4 mix <?php echo $products['categoryid']; ?>"><!-- item -->

                    <div class="item-box">
                        <figure>
                            <span class="item-hover">
                                <span class="overlay dark-5"></span>
                                <span class="inner">
                                    <!-- lightbox -->
                                    <a class="ico-rounded lightbox" href="<?php echo SITE_URL.$products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
                                        <span class="fa fa-plus size-20"></span>
                                    </a>
                                    <!-- details -->
                                    <a class="ico-rounded" href="<?php echo SITE_URL.'variety-sub-page.php?id='.$products['id']; ?>">
                                        <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                    </a>
                                </span>
                            </span>
                            <div class="item-box-overlay-title">
                                <h3><?php echo $products["name"]; ?></h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#"><?php echo $catinfo['name'] ?></a></li>
                                </ul>
                            </div>

                            <!-- carousel OK-->
                            <div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                                <div>
                                    <img class="img-responsive" src="<?php echo SITE_URL.$products['image_path'] ?>" width="600" height="399" alt="">
                                </div>

                            </div>
                            <!-- /carousel -->

                        </figure>
                    </div>

                </div><!-- /item -->
            <?php //} ?>
        <?php } ?>
    <?php
    } else {
        ?>	
        <div class="notfound" style="margin-left: 20px;">No Item Found !</div>
    <?php
    }
} else {
    $main_cat_id = "";
    if ($_REQUEST['selected_main_cat'] != "") {
        $main_cat_id = " AND p.categoryid='" . $_REQUEST['selected_main_cat'] . "'";
    }
    //echo $wh;
    $sel_products = "select p.* from product p 
	   left join grower_product gp on p.id=gp.product_id
	   left join growers g on gp.grower_id=g.id
	   where 1=1 $main_cat_id $wh GROUP BY p.name limit 0,12";
    // exit;
    //echo $sel_products;
    $selected_color_array = array();
    $rs_products = mysqli_query($con,$sel_products);
    $number_row = mysqli_num_rows($rs_products);
    if ($number_row > 0) {
        while ($products = mysqli_fetch_array($rs_products)) {
            //echo "<pre>";print_r($products);echo "</pre>";exit();
            array_push($selected_color_array, $products['color_id']);
            $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

            $rs_catinfo = mysqli_query($con,$sel_catinfo);

            $catinfo = mysqli_fetch_array($rs_catinfo);

            $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));
            ?>  
            <div class="col-md-4 col-sm-4 mix <?php echo $products['categoryid']; ?>"><!-- item -->

                <div class="item-box">
                    <figure>
                        <span class="item-hover">
                            <span class="overlay dark-5"></span>
                            <span class="inner">

                                <!-- lightbox -->
                                <a class="ico-rounded lightbox" href="<?php echo SITE_URL.$products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
                                    <span class="fa fa-plus size-20"></span>
                                </a>

                                <!-- details -->
                                <a class="ico-rounded" href="<?php echo SITE_URL.'variety-sub-page.php?id='.$products['id']; ?>">
                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                </a>

                            </span>
                        </span>
                        <div class="item-box-overlay-title">
                            <h3><?php echo $products["name"]; ?></h3>
                            <ul class="list-inline categories nomargin">
                                <li><a href="#"><?php echo $catinfo['name'] ?></a></li>
                            </ul>
                        </div>

                        <!-- carousel yes-->
                        <div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                            <div>
                                <img class="img-responsive" src="<?php echo SITE_URL.$products['image_path'] ?>" width="600" height="399" alt="">
                            </div>

                        </div>
                        <!-- /carousel -->

                    </figure>
                </div>

            </div><!-- /item -->
        <?php } ?>
    <?php
    } else {
        ?>	
        <div class="notfound" style="margin-left: 20px;">No Item Found !</div>
    <?php
    }
}

if (count($selected_color_array) > 0) {
    $imp_color = implode(",", $selected_color_array);

    $sel_colors = "select * from colors where id IN($imp_color) GROUP BY id order by name";
    $rs_colors = mysqli_query($con,$sel_colors);
    ?>
    #$#$#$#$#$

    <?php
    $selected_color_str = "";
    while ($colors = mysqli_fetch_array($rs_colors)) {

        $selected_color_str.=$colors["name"] . ",";
    }
    ?>
    <strong><?php echo trim($selected_color_str, ","); ?></strong>	

<?php } ?>
