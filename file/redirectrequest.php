<?php

// PO 2018-04-01
 
require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
session_start();
$delDate = $_POST['delDate'];
$dateRange = $_POST['dateRange'];
$shippingMethod = $_POST['shippingMethod'];
$userSessionID = $_SESSION["buyer"];
$qucik_desc = $_POST['qucik_desc'];

$date = date('Y-m-d H:i:s');
$order_serial = 1;
$dayOfMonth = date('d', strtotime($date));
$monthOfYear = date('m', strtotime($date));
$year = date('y', strtotime($date));

$getBuyerShippingMethod = "select * from buyer_shipping_methods where shipping_method_id ='" . $shippingMethod . "'";
$buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
$buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
$current_date=time();

$order_number = $buyerShippingMethod['shipping_code'] . $dayOfMonth . $monthOfYear . $year;
//$qry = "select * from buyer_orders where order_number='" . $order_number . "' ORDER BY order_serial DESC LIMIT 0,1";
$qry = "select * from buyer_orders where order_number='" . $order_number . "' ORDER BY order_serial DESC";
$data = mysqli_query($con, $qry);
$numrows = mysqli_num_rows($data);

if ($numrows > 0) {
    while ($dt = mysqli_fetch_assoc($data)) {
        $order_serial = $dt['order_serial'] + $order_serial;
    }
}
$leavingFarmDate = 0;
if (!empty($buyerShippingMethod['shipping_method_id']) || $buyerShippingMethod['shipping_method_id']!=0) {
    $shipping_method_id = $buyerShippingMethod['shipping_method_id'];
    $getShippingMethod = "select connections from shipping_method where id='" . $shipping_method_id . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);
    $conCount = count($connections);

    $trasit_time = 0;
    $days = array();
    if ($conCount == 1) {
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "select (trasit_time+1) trasit_time,days from connections where id='" . $connection . "'");
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $days = explode(',', $conDetail['days']);
        }
        // Proceso para poner el PO el LFD
        
           $sqlids = "SELECT date_add('".$delDate."', INTERVAL -".$trasit_time." DAY) fecha";  
           $row_sqlids = mysqli_query($con, $sqlids);
                                                            
           while ($fila =mysqli_fetch_assoc($row_sqlids)) {
                $date_lfd= $fila["fecha"];
           }
    
           $fecha_tmp = $date_lfd;
           $dayofweek = date('D', strtotime($date_lfd));  
           if ($dayofweek == "Sun")  {
                $date_lfd = strtotime ('-2 day',strtotime ($fecha_tmp) );
                $date_lfd = date ( 'Y-m-j' , $date_lfd );                
            }else{
                $date_lfd=$fecha_tmp;                    
            }   
            
            $dayOfMonth = date('d', strtotime($date_lfd));
            $monthOfYear = date('m', strtotime($date_lfd));
            $year = date('y', strtotime($date_lfd));
        
            $order_number = $buyerShippingMethod['shipping_code'] . $dayOfMonth . $monthOfYear . $year;  
        
        // Fin PO
        
        if ($trasit_time > 1) {
            $prevDate = strtotime($delDate . ' -' . $trasit_time . ' days');
            $trasit_date = date("Y-m-d", $prevDate);
        } else {
            $prevDate = strtotime($delDate . '  -' . $trasit_time . ' day');
            $trasit_date = date("Y-m-d", $prevDate);
        }
        $trasitDay = date("w", strtotime($trasit_date));
        if (in_array($trasitDay, $days)) {
            $leavingFarmDate = $trasit_time;
        } else {
            $ttime = $trasit_time;
            switch (true) {
                case in_array($trasitDay - 1, $days):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay - 2, $days):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay - 3, $days):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay - 4, $days):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay - 5, $days):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay - 6, $days):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
            $leavingFarmDate = $ttime;
        }
    } else {
        $t = array();
        $dDate = $delDate;
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "select trasit_time,days from connections where id='" . $connection . "'");
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $t[] = $conDetail['trasit_time'];
            $days[] = $conDetail['days'];
            $day = explode(',', $conDetail['days']);
        }
        $prevDates = strtotime($dDate . '  -' . $trasit_time . ' day');
        $trasitDay = date("w", strtotime($dDate));
        $ttime = 0;
        for ($j = $conCount - 1; $j >= 0; $j--) {
            $fday = explode(',', $days[$j]);
            $ttime = $ttime + $t[$j];
            
            if (in_array($trasitDay, $fday)) {
                $ttime = $ttime;
            } else {
                switch (true) {
                    case in_array($trasitDay - 1, $fday):
                        $ttime = $ttime + 1;
                        break;
                    case in_array($trasitDay - 2, $fday):
                        $ttime = $ttime + 2;
                        break;
                    case in_array($trasitDay - 3, $fday):
                        $ttime = $ttime + 3;
                        break;
                    case in_array($trasitDay - 4, $fday):
                        $ttime = $ttime + 4;
                        break;
                    case in_array($trasitDay - 5, $fday):
                        $ttime = $ttime + 5;
                        break;
                    case in_array($trasitDay - 6, $fday):
                        $ttime = $ttime + 6;
                        break;
                    default:
                        $ttime = $ttime + 1;
                }
            }
            $prevDates = strtotime($dDate . '  -' . $ttime . ' days');
            $dDates = date("Y-m-d", $prevDates);
            $trasitDay = date("w", strtotime($dDates));
            //$leavingFarmDate = $ttime;
        }

        $fday = explode(',', $days[0]);
        if (in_array($trasitDay, $fday)) {
            $ttime = $ttime;
        } else {
            switch (true) {
                case in_array($trasitDay - 1, $fday):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay - 2, $fday):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay - 3, $fday):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay - 4, $fday):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay - 5, $fday):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay - 6, $fday):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
        }
        $prevDatess = strtotime($dDate . '  -' . $ttime . ' days');
        $dDatess = date("Y-m-d", $prevDatess);
        $leavingFarmDate = $ttime;
    }
}else if(!empty($buyerShippingMethod['choose_shipping'])){

    $getMethod = "select * from shipping_method where shipping_type='".$buyerShippingMethod['choose_shipping']."'";
    $getMethodRes = mysqli_query($con, $getMethod);
    $methodDetail = mysqli_fetch_assoc($getMethodRes);
    $shipping_method_id = $methodDetail['id'];
    $getShippingMethod = "select connections from shipping_method where id='" . $shipping_method_id . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);
    $conCount = count($connections);
    $trasit_time = 0;
    $days = array();
    
    if ($conCount == 1) {
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "select trasit_time,days from connections where id='" . $connection . "'");
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $days = explode(',', $conDetail['days']);
            $daysOfWeekDisabled = $conDetail['days'];
        }
        if ($trasit_time > 1) {
            $prevDate = strtotime($delDate . ' +' . $trasit_time . ' days');
            $trasit_date = date("Y-m-d", $prevDate);
        } else {
            $prevDate = strtotime($delDate . '  +' . $trasit_time . ' day');
            $trasit_date = date("Y-m-d", $prevDate);
        }
        $trasitDay = date("w", strtotime($trasit_date));

        if (in_array($trasitDay, $days)) {
            $leavingFarmDate = $trasit_time;
        } else {
            $ttime = $trasit_time;
            switch (true) {
                case in_array($trasitDay - 1, $days):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay - 2, $days):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay - 3, $days):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay - 4, $days):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay - 5, $days):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay - 6, $days):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
            $leavingFarmDate = $ttime;
        }
    } else {
        $t = array();
        $dDate = $delDate;
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "select trasit_time,days from connections where id='" . $connection . "'");
            $conDetail = mysqli_fetch_assoc($getConDetail);
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $t[] = $conDetail['trasit_time'];
            $days[] = $conDetail['days'];
            $day = explode(',', $conDetail['days']);
        }
        $daysOfWeekDisabled = $days[$conCount-1];
        $prevDates = strtotime($dDate . '  +' . $trasit_time . ' day');
        $trasitDay = date("w", strtotime($dDate));
        $ttime = 0;
        for ($j = 0; $j < $conCount; $j++) {          
            $fday = explode(',', $days[$j]);
            $ttime = $ttime + $t[$j];
            
            if (in_array($trasitDay, $fday)) {
                $ttime = $ttime;
            } else {
                switch (true) {
                    case in_array($trasitDay - 1, $fday):
                        $ttime = $ttime + 1;
                        break;
                    case in_array($trasitDay - 2, $fday):
                        $ttime = $ttime + 2;
                        break;
                    case in_array($trasitDay - 3, $fday):
                        $ttime = $ttime + 3;
                        break;
                    case in_array($trasitDay - 4, $fday):
                        $ttime = $ttime + 4;
                        break;
                    case in_array($trasitDay - 5, $fday):
                        $ttime = $ttime + 5;
                        break;
                    case in_array($trasitDay - 6, $fday):
                        $ttime = $ttime + 6;
                        break;
                    default:
                        $ttime = $ttime + 1;
                }
            }
            $prevDates = strtotime($dDate . '  -' . $ttime . ' days');
            $dDates = date("Y-m-d", $prevDates);
            $trasitDay = date("w", strtotime($dDates));
            //$leavingFarmDate = $ttime;
        }

        $fday = explode(',', $days[0]);
        if (in_array($trasitDay, $fday)) {
            $ttime = $ttime;
        } else {
            switch (true) {
                case in_array($trasitDay - 1, $fday):
                    $ttime = $ttime + 1;
                    break;
                case in_array($trasitDay - 2, $fday):
                    $ttime = $ttime + 2;
                    break;
                case in_array($trasitDay - 3, $fday):
                    $ttime = $ttime + 3;
                    break;
                case in_array($trasitDay - 4, $fday):
                    $ttime = $ttime + 4;
                    break;
                case in_array($trasitDay - 5, $fday):
                    $ttime = $ttime + 5;
                    break;
                case in_array($trasitDay - 6, $fday):
                    $ttime = $ttime + 6;
                    break;
                default:
                    $ttime = $ttime + 1;
            }
        }
        $prevDatess = strtotime($dDate . '  -' . $ttime . ' days');
        $dDatess = date("Y-m-d", $prevDatess);
        $leavingFarmDate = $ttime;
    }
}else {
    $startDate = date('Y-m-d');
    $daysOfWeekDisabled = '';
    $val = $startDate;
}

	$qryMax="select (max(id)+1) as id from buyer_orders";
	$dataMaximo = mysqli_query($con, $qryMax);
         
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
            $IdMaximo= $dt['id'];
		}
                
               
$sql = "INSERT INTO `buyer_orders`  (`id`,`buyer_id`,`order_number`,`order_date`,`shipping_method`,`qucik_desc`,`del_date`,`date_range`,`is_pending`,`order_serial`,`delivery_dates`,`lfd_grower`) 
    VALUES ('" . $IdMaximo . "','" . $userSessionID . "','" . $order_number . "','" . $date . "','" . $shippingMethod . "','".$qucik_desc."','" . $delDate . "','" . $dateRange . "','0','" . $order_serial . "','','" . $leavingFarmDate . "')";


mysqli_query($con, $sql);
