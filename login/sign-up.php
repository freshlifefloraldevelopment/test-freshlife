<?php 
require_once("config/config_new.php");
if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login-type"] == 'buyer') {
    header("location:" . SITE_URL . "buyer/my-account");
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor/my-account");
    die;
}
#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 18;//VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL.'includes/assets/css/essentials-flfv3.css', SITE_URL.'includes/assets/css/layout-flfv3.css', 
                            SITE_URL.'includes/assets/css/header-1.css', SITE_URL.'includes/assets/css/layout-shop.css', SITE_URL.'includes/assets/css/color_scheme/blue.css' );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################
require_once 'includes/header.php';
?>
<section class="page-header page-header-xs">
  <div class="container">
    <h1>Register</h1>
    <!-- breadcrumbs -->
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><a href="#">Pages</a></li>
      <li class="active">Register</li>
    </ol>
    <!-- /breadcrumbs -->
    <!-- page tabs -->
    <ul class="page-header-tabs list-inline">
      <li class="active"><a href="#regsiter_as_buyer" id="regsiter_as_buyer_1" data-toggle="tab" aria-expanded="true">BUYERS</a></li>
      <li class=""><a href="#regsiter_as_grower" id="regsiter_as_grower_1" data-toggle="tab" aria-expanded="false">GROWERS</a></li>
    </ul>
    <!-- /page tabs -->
  </div>
</section>
<section>
  <div class="container">
    <div class="tab-content">
      <!-- REGISTER -->
      <div class="tab-pane fade in active" id="regsiter_as_buyer">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
            <div class="box-static box-transparent box-bordered padding-30">
              <div class="box-title margin-bottom-30">
                <h2 class="size-20">Register as Buyers</h2>
              </div>
              <form class="nomargin sky-form" id="registerFrm" action="<?= SITE_URL ?>sign-up.php" method="post">
                <input type="hidden" name="frm_mode" value="buyer">
                <fieldset>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label>First Name *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-user"></i>
                      <input required="" type="text" name="first_name">
                      <b class="tooltip tooltip-bottom-right">Your First Name</b> </label>
                    </div>
                    <div class="col-md-12">
                      <label for="register:last_name">Last Name *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-user"></i>
                      <input required="" type="text" name="last_name">
                      <b class="tooltip tooltip-bottom-right">Your Last Name</b> </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label for="register:email">Email *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-envelope"></i>
                      <input required="" type="email" name="email">
                      <b class="tooltip tooltip-bottom-right">Your Email</b> </label>
                    </div>
                    <div class="col-md-12">
                      <label for="register:phone">Phone</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-phone"></i>
                      <input type="text" name="phone">
                      <b class="tooltip tooltip-bottom-right">Your Phone (optional)</b> </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label for="register:pass1">Password *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-lock"></i>
                      <input required="" type="password" class="err" name="password">
                      <b class="tooltip tooltip-bottom-right">Min. 6 characters</b> </label>
                    </div>
                    <div class="col-md-12">
                      <label for="register:pass2">Password Again *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-lock"></i>
                      <input required="" type="password" class="err" name="cpassword">
                      <b class="tooltip tooltip-bottom-right">Type the password again</b> </label>
                    </div>
                  </div>
                </div>
                <hr>
                <label class="checkbox nomargin">
                <input required="" class="checked-agree" type="checkbox" name="checkbox">
                <i></i>I agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms of Service</a></label>
                <label class="checkbox nomargin">
                <input required="" name="subscriber" type="checkbox" name="checkbox">
                <i></i>Subscribe to newsletter</label>
                </fieldset>
                <!-- ALERT -->
                <div class="alert alert-mini" id="checkLoginErr"></div>
                <!-- /ALERT -->
                <div class="row">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> REGISTER</button>
                  </div>
                </div>
                <input type="hidden" name="submit" value="_register">
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /REGISTER -->
      <!-- REGISTER -->
      <div class="tab-pane fade" id="regsiter_as_grower">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
            <div class="box-static box-transparent box-bordered padding-30">
              <div class="box-title margin-bottom-30">
                <h2 class="size-20">Register as Grower</h2>
              </div>
              <form class="nomargin sky-form" id="registerFrmGrower" action="<?= SITE_URL ?>sign-up.php" method="post">
                <input type="hidden" name="frm_mode" value="grower">
                <fieldset>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label>First Name *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-user"></i>
                      <input required="" type="text" name="first_name">
                      <b class="tooltip tooltip-bottom-right">Your First Name</b> </label>
                    </div>
                    <div class="col-md-12">
                      <label for="register:last_name">Last Name *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-user"></i>
                      <input required="" type="text" name="last_name">
                      <b class="tooltip tooltip-bottom-right">Your Last Name</b> </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label for="register:email">Email *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-envelope"></i>
                      <input required="" type="email" name="uname">
                      <b class="tooltip tooltip-bottom-right">Your Email</b> </label>
                    </div>
                    <div class="col-md-12">
                      <label for="register:phone">Phone</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-phone"></i>
                      <input type="text" name="phone">
                      <b class="tooltip tooltip-bottom-right">Your Phone (optional)</b> </label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label for="register:pass1">Password *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-lock"></i>
                      <input required="" type="password" class="err" name="password">
                      <b class="tooltip tooltip-bottom-right">Min. 6 characters</b> </label>
                    </div>
                    <div class="col-md-12">
                      <label for="register:pass2">Password Again *</label>
                      <label class="input margin-bottom-10"> <i class="ico-append fa fa-lock"></i>
                      <input required="" type="password" class="err" name="cpassword">
                      <b class="tooltip tooltip-bottom-right">Type the password again</b> </label>
                    </div>
                  </div>
                </div>
                <hr>
                <label class="checkbox nomargin">
                <input class="checked-agree" type="checkbox" name="checkbox">
                <i></i>I agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms of Service</a></label>
                </fieldset>
                <!-- ALERT -->
                <div class="alert alert-mini" id="checkGrowerLoginErr"></div>
                <!-- /ALERT -->
                <div class="row">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> REGISTER</button>
                  </div>
                </div>
                <input type="hidden" name="submit" value="_register">
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /REGISTER -->
    </div>
  </div>
</section>
<?php require_once("includes/footer.php"); ?>
<script type="text/javascript">
    $("#regsiter_as_buyer_1").click(function (event) {
        $("#registerFrmGrower").find('input:text, input:password, select, textarea').val('');
    });
    $("#regsiter_as_grower_1").click(function (event) {
        $("#registerFrm").find('input:text, input:password, select, textarea').val('');

    });
    $(function () {
        $("#registerFrm").submit(function (event) {
            $('#checkLoginErr').removeClass("alert-danger");
            $('#checkLoginErr').removeClass("alert-success");
            $('#checkLoginErr').html('');
            event.preventDefault();
            $.ajax({
                url: '<?php echo SITE_URL; ?>save-register.php',
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                    if (result == 1) {
                        $("#registerFrm").find('input:text, input:password, select, textarea').val('');
                        $('#checkLoginErr').html('Your registration has been done successfully. Please sign-in to continue!');
                        $('#checkLoginErr').addClass("alert-success");
                    } else {
                        $('#checkLoginErr').addClass("alert-danger");
                        $('#checkLoginErr').html(result);

                    }
                }
            });
        });
        $("#registerFrmGrower").submit(function (event) {
            $('#checkGrowerLoginErr').removeClass("alert-danger");
            $('#checkGrowerLoginErr').removeClass("alert-success");
            $('#checkGrowerLoginErr').html('');
            event.preventDefault();
            $.ajax({
                url: '<?php echo SITE_URL; ?>save-register.php',
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                    if (result == 1) {
                        $("#registerFrmGrower").find('input:text, input:password, select, textarea').val('');
                        $('#checkGrowerLoginErr').html('Your registration has been done successfully. Please sign-in to continue!');
                        $('#checkGrowerLoginErr').addClass("alert-success");
                    } else {
                        $('#checkGrowerLoginErr').addClass("alert-danger");
                        $('#checkGrowerLoginErr').html(result);

                    }
                }
            });
        });
    });


</script>
