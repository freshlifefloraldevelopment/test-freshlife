<?php
require_once("config/config_new.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login-type"] == 'buyer') {
    header("location:" . SITE_URL . "buyers-account.php");
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 17;//VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL.'includes/assets/css/essentials-flfv3.css', SITE_URL.'includes/assets/css/layout-flfv3.css', 
                            SITE_URL.'includes/assets/css/header-1.css', SITE_URL.'includes/assets/css/layout-shop.css', SITE_URL.'includes/assets/css/color_scheme/blue.css' );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################
require_once 'includes/header.php'; 
?>
<section class="page-header page-header-xs">
    <div class="container">

        <h1>FORGOT PASSWORD</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <?php if ($_SESSION["login"] != 1) { ?>
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">Forgot</li>
            <?php } else { ?>
                <li><a href="#">Home</a></li>
                <li><a href="<?= SITE_URL ?>sign-out.php">My Account</a></li>
                <li class="active"><a href="<?= SITE_URL ?>sign-out.php">Logout</a></li>
            <?php } ?>	
        </ol><!-- /breadcrumbs -->
    </div>
</section>



<section>
    <div class="container">


        <div class="tab-content">
            <?php if ($_SESSION["login"] != 1) { ?>
                <!-- LOGIN -->
                <div class="tab-pane fade in active">

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">

                            <!-- ALERT -->
                            <div class="alert alert-mini margin-bottom-30" id="checkLoginErr">
                                    <!--<strong>Oh snap!</strong> Login Incorrect!-->
                            </div><!-- /ALERT -->

                            <div class="box-static box-transparent box-bordered padding-30">
                                <div class="box-title margin-bottom-30">
                                    <h2 class="size-20">Forgot <span id="textlogin">Password</span></h2>
                                </div>
                                <form class="sky-form" id="loginFormID" method="post" autocomplete="off">
                                    <div class="clearfix">
                                        <?php if ($_SESSION["lang"] != "ru") { ?>
                                            <!-- Email -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <label class="input margin-bottom-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="true" name="email"  id="email" type="email" placeholder="Email">
                                                    <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                                                </label>
                                            </div>
                                        <?php } else { ?>
                                            <!-- Email -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <label class="input margin-bottom-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="true" name="email"  id="email" type="email" placeholder="Email">
                                                    <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <!-- Inform Tip -->                                        
                                            <div class="form-tip pt-20" style="display: none;">
                                                <a class="no-text-decoration size-13 margin-top-10 block" href="<?php echo SITE_URL ?>login.php">Login here</a>
                                            </div>

                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                            <button class="btn btn-primary"  id="btn_forgot_password"><i class="fa fa-check"></i> LOGIN</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /LOGIN -->
            <?php } ?>
        </div>
    </div>
</section>
<?php require_once("includes/footer.php"); ?>	
<script type="text/javascript">
    $("#login_as_growers").click(function (event) {
        $("#textlogin").html('Growers');
        $("#usrtypeID").val('grower');
    });
    $("#login_as_buyers").click(function (event) {
        $("#textlogin").html('Buyers');
        $("#usrtypeID").val('buyer');

    });
    $(function () {
        $("#btn_forgot_password").click(function (event) {
            event.preventDefault();
            var email = $("#email").val();
            
                $.ajax({
                    url: '<?php echo SITE_URL; ?>checkForgotPassword.php',
                    type: 'POST',
                    data: 'email='+email+'&usrtype=buyer',
                    success: function (result) {
                        if($.trim(result) == "1")
                        {
                            $("#checkLoginErr").html("Your mail has been sent, Please check.");
                            $("#checkLoginErr").removeClass("alert-danger");
                            $("#checkLoginErr").addClass("alert-success");
                        }
                        else
                        {
                            $("#checkLoginErr").html("Invalid Email Address");
                            $("#checkLoginErr").removeClass("alert-success");
                            $("#checkLoginErr").addClass("alert-danger");
                        }
                    }
                });   
            
        });
    });
</script>		
