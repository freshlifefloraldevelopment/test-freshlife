<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idfac         = $_GET['b'];
    $cliente_inv   = $_GET['id_cli'];
    
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select id,name
                      from sub_client  
                     where id = '" . $cliente_inv . "'     " ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select ios.id            , ios.id_fact          , ios.buyer_id    , ios.id_client       , ios.order_number  , 
                         ios.order_date    , ios.shipping_method  , ios.date_order  , ios.date_range      , ios.is_pending    , 
                         ios.order_serial  , ios.description      , ios.bill_number , ios.gross_weight    , ios.volume_weight , 
                         ios.freight_value , ios.sub_total_amount , ios.tax_rate    , ios.shipping_charge , ios.handling      , 
                         ios.grand_total   , ios.bill_state       , ios.date_added  , ios.user_added      , ios.tot_reg       ,
                         ios.tot_bunches   , cl.id                , cl.name as name_client 
                    from invoice_orders_subcli ios  
                   INNER JOIN sub_client cl ON cl.id = ios.id_client
                   where ios.buyer_id  = '" . $userSessionID . "'
                     and ios.id_fact   = '" . $idfac . "' 
                     and ios.id_client = '" . $cliente_inv . "'" ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select irs.id           , irs.id_fact      , irs.id_order        , order_number, order_serial , 
                        irs.offer_id     , irs.product      , irs.prod_name       , buyer       , grower_id    ,
                        irs.box_qty_pack , irs.qty_pack     , irs.qty_box_packing , box_type    , comment      , 
                        irs.date_added   , irs.box_name     , irs.size, steams    , price       , 
                        irs.cliente_id   , irs.ship_cost    , irs.cost_cad        , price_cad   , gorPrice     , 
                        irs.duties       , irs.handling_pro , irs.total_duties    , irs.price_quick
                  from invoice_requests_subcli irs
                 where irs.buyer      = '" . $userSessionID . "'
                   and irs.id_fact    = '" . $idfac . "' 
                   and irs.cliente_id = '" . $cliente_inv . "'    
                 order by irs.prod_name";

        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    //$pdf->AliasPages();   
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'INVOICE',0,0,'L'); 
   // $pdf->Image('logo.png',1,5,22); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'Name: '.$buy['name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');  
    
   
    
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,0,'L');        
    $pdf->Cell(0,6,'Gross Weight: '.$buyerOrderCab['gross_weight'],0,1,'R');        
    
    $pdf->Cell(70,6,'Delivery Date : '.$buyerOrderCab['date_added'],0,0,'L');  
    $pdf->Cell(0,6,'Volume Weight: '.$buyerOrderCab['volume_weight'],0,1,'R');  
    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Cost',0,0,'C');    
    $pdf->Cell(25,6,'Price',0,0,'C');        
    $pdf->Cell(25,6,'Qty',0,0,'C');
    $pdf->Cell(25,6,'Subtotal',0,1,'C'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {       
       
         // Verificacion Stems/Bunch
        
        $sel_bu_st = "select box_type , subcategoryid from product where name = '" . $row['prod_name'] . "' "; 
        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    //$Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $Subtotal= $row['qty_pack'] * $row['price_quick'];
                    $qtyFac  = $row['qty_pack'] *$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal= $row['qty_pack'] * $row['price_quick'];
                    $qtyFac  = $row['qty_pack']; 
                    $unitFac = "BUNCHES";                   
              }        
        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);
         
               $subtotalStems= $row['steams'] *$row['bunchqty'] ;         
         

         $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(70,4,$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu",0,0,'L');            
         $pdf->Cell(25,6,number_format($row['price_cad'], 2, '.', ','),0,0,'C'); 
         $pdf->Cell(25,6,number_format($row['price_quick'], 2, '.', ','),0,0,'C'); 
         

if ($bunch_stem['box_type'] == 0) {         
         //$subTotalGrower= $subTotalGrower + ($row['steams']*$row['bunchqty']*$row['salesPriceCli']) ; 
         $subTotalGrower= $subTotalGrower + ($row['qty_pack']*$row['price_quick']) ;             
         $pdf->Cell(25,6,$row['qty_pack'],0,0,'C');
}else{
         $subTotalGrower= $subTotalGrower + ($row['qty_pack']*$row['price_quick']) ;             
         $pdf->Cell(25,6,$row['qty_pack'],0,0,'C');                                           
}         

         $pdf->Cell(25,6,number_format($Subtotal, 2, '.', ','),0,1,'C');  

         $pdf->Cell(70,4,$subc['name'],0,1,'L');     
         

         $pdf->Cell(70,2,'_______________________________________________________________________________________________________________________',0,1,'L');  

            $totalCal = $totalCal + $Subtotal;
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['grower_id'];
            $subStemsGrower= $subStemsGrower + ($row['qty_pack']) ; 
            
    }
                      
    
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
                        
            $pdf->SetFont('Arial','B',10);            

            $pdf->Cell(70,6,'Address',0,0,'L');    
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');                                
            $pdf->Cell(70,6,'Vancouver, Canada',0,1,'L');               
            $pdf->Cell(70,6,'Phone:',0,1,'L'); 
            $pdf->Cell(70,6,'Email:',0,0,'L');
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');    
    
  $pdf->Output();
  ?>