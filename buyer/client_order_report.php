<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idfac = $_GET['fac_id'];
    $clienteid = $_GET['id_cli'];
    
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id              , buyer_id        , order_number    , order_date      , shipping_method ,
                         del_date        , date_range      , is_pending      , order_serial    , seen            ,
                         delivery_dates  , lfd_grower      , qucik_desc      , type_market     , delivery_day    ,
                         availability    
                    from buyer_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id       = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select ir.id             , ir.id_order       , ir.order_serial   , ir.cod_order      , ir.product        ,
                       ir.sizeid         , ir.feature        , ir.noofstems      , sum(ir.qty) qty   , ir.buyer          ,
                       ir.boxtype        , ir.date_added     , ir.type           , ir.bunches        , ir.box_name       ,
                       ir.lfd            , ir.lfd2           , ir.comment        , ir.box_id         , ir.shpping_method ,
                       ir.isy            , ir.mreject        , ir.bunch_size     , ir.unseen         , ir.req_qty        ,
                       ir.bunches2       , ir.discount       , ir.inventary      , ir.type_price     , ir.id_client      ,
                       ir.id_grower      , ir.special_order  , ir.price          , ir.num_box        , ir.date_ship      ,
                       ir.date_del       , ir.cost           , p.name as nameprod,p.subcategoryid    , p.categoryid      ,
                       p.color_id        , s.name as subcate_name , c.name as colorname              , sz.name as sizename
                  from reser_requests ir
                 INNER JOIN product p ON ir.product = p.id                  
                 INNER JOIN subcategory s ON p.subcategoryid = s.id and p.categoryid = s.cat_id
                 INNER JOIN colors c ON p.color_id = c.id                  
                 INNER JOIN sizes sz ON ir.sizeid = sz.id                  
                 where ir.buyer     = '0'
                   and ir.id_order  = '0' 
                   and ir.comment   like 'SubClient%'                                                
                 group by p.id    
union
select re.id             , re.id_order   , re.order_serial   , re.cod_order      , re.product        ,
                       re.sizeid         , re.feature        , re.noofstems      , sum(re.qty) qty   , re.buyer          ,
                       re.boxtype        , re.date_added     , re.type           , re.bunches        , re.box_name       ,
                       re.lfd            , re.lfd2           , re.comment        , re.box_id         , re.shpping_method ,
                       re.isy            , re.mreject        , re.bunch_size     , re.unseen         , re.req_qty        ,
                       re.bunches2       , re.discount       , re.inventary      , re.type_price     , re.id_client      ,
                       re.id_grower      , re.special_order  , re.price          , re.num_box        , re.date_ship      ,
                       re.date_del       , re.cost           , p.name as nameprod,p.subcategoryid    , p.categoryid      ,
                       p.color_id        , s.name as subcate_name , c.name as colorname              , sz.name as sizename
                  from buyer_requests re
                 INNER JOIN product p ON re.product = p.id                  
                 INNER JOIN subcategory s ON p.subcategoryid = s.id and p.categoryid = s.cat_id
                 INNER JOIN colors c ON p.color_id = c.id                  
                 INNER JOIN sizes sz ON re.sizeid = sz.id                  
                 where re.buyer     = '" . $userSessionID . "'
                   and re.id_order  = '" . $idfac . "' 
                   and re.comment   like 'SubClient%'                                                   
                 group by p.id    order by subcate_name ";

        $result   = mysqli_query($con, $sqlDetalis);    
        
        //                    and ir.id_client = '" . $clienteid . "'

    $pdf = new PDF();
    //$pdf->AliasPages();   
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);  
    

        $pdf->Cell(70,10,'ORDER',0,0,'L'); 

    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Buyer Details ',0,1,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Order #: '.$buyerOrderCab['id'],0,1,'R');             

    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');            
    $pdf->Cell(70,6,'Company Name : '.$buy['company'],0,1,'L');  

    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'St/Bu',0,0,'C');
    $pdf->Cell(25,6,'Size',0,0,'C');    
    $pdf->Cell(25,6,'Feature',0,0,'C'); 
    $pdf->Cell(25,6,'Qty',0,1,'C');    
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {                        

                 

  $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(70,4,$row['subcate_name']." ".$row['nameprod'],0,0,'L');            
         $pdf->Cell(25,6,$row['noofstems'],0,0,'C');                              
         $pdf->Cell(25,6,$row['sizename'],0,0,'C');                              

         $pdf->Cell(25,6,$row['bunchqty']*$row['steams'],0,0,'C');           

         $pdf->Cell(25,4,$row['qty'],0,1,'C');       
         

         //$pdf->Cell(70,2,'_______________________________________________________________________________________________________________________',0,1,'L');  

            $totalCal = $totalCal + $Subtotal;
            $totalStems = $totalStems + $subtotalStems;            
            
            $totalBunch = $totalBunch + $subtotalBunch;            
            
            $tmp_idorder = $row['grower_id'];
            $subStemsGrower= $subStemsGrower + ($row['steams'] *$row['bunchqty']) ; 
            
    }

    $handling = $totalCal*0.08 ;    // parametrizar    
    
                      
    
    $pdf->Ln(2);
            
            
            $pdf->SetFont('Arial','B',10);            
            
          //  $pdf->Cell(35,6,'Total :'.number_format($totalStems, 0, '.', ','),0,1,'R');        
            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,1,'L');            
            
            $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');               
            
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L'); 
            
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,1,'L');
        //    $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');                             
    
    
    
  $pdf->Output();
  ?>