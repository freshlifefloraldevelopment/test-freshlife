<!doctype html>

<?php

// PO 2018-08-24

require_once("../config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
$userSessionID = $_SESSION["buyer"];
$idfac = $_GET['idi'];
$cliente_inv = $_GET['id_cli'];

if (isset($_GET['sd']) && !empty($_GET['sd'])) {
    $id = $_GET['sd'];
    $qry = "DELETE FROM `buyer_shipping_methods` WHERE `id` = '$id'";


    echo '<script language="javascript">alert("' . $id . '");</script>';
    if (mysqli_query($con, $qry)) {
        header("location:" . SITE_URL . "buyer/buyers-account.php");
        die;
    }
}



/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}


//  Actualizacion en Grupo
if (isset($_REQUEST["total"])) {
    
    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
              
            $update = "update invoice_requests_subcli 
                          set price_quick   = '" . $_POST["price-". $_POST["pro-" . $i]] . "' ,
                              box_qty_pack  = '" . $_POST["boxp-". $_POST["pro-" . $i]] . "'
                       where id='" . $_POST["pro-" . $i] . "'  ";                               
    
            mysqli_query($con, $update);                                                    
        
    }
    
  }        
  
    
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php


   $buyerEntity = "select b.first_name,b.last_name,c.name from buyers  b , country c where b.id = '" . $userSessionID . "'  and c.id=b.country" ;

   $buyer = mysqli_query($con, $buyerEntity);
   $buy = mysqli_fetch_array($buyer);
   
   
   $buyerOrder = "select ios.id            , ios.id_fact          , ios.buyer_id    , ios.id_client       , ios.order_number  , 
                         ios.order_date    , ios.shipping_method  , ios.date_order  , ios.date_range      , ios.is_pending    , 
                         ios.order_serial  , ios.description      , ios.bill_number , ios.gross_weight    , ios.volume_weight , 
                         ios.freight_value , ios.sub_total_amount , ios.tax_rate    , ios.shipping_charge , ios.handling      , 
                         ios.grand_total   , ios.bill_state       , ios.date_added  , ios.user_added      , ios.tot_reg       ,
                         ios.tot_bunches   , cl.id                , cl.name as name_client 
                    from invoice_orders_subcli ios  
                   INNER JOIN sub_client cl ON cl.id = ios.id_client
                   where ios.buyer_id  = '" . $userSessionID . "'
                     and ios.id_fact   = '" . $idfac . "' 
                     and ios.id_client = '" . $cliente_inv . "'  " ;    

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab);
   

    $sqlDetalis="select irs.id  as gid   , irs.id_fact      , irs.id_order        , irs.order_number, irs.order_serial , 
                        irs.offer_id     , irs.product      , irs.prod_name       , irs.buyer       , irs.grower_id    ,
                        irs.box_qty_pack , irs.qty_pack     , irs.qty_box_packing , irs.box_type    , irs.comment      , 
                        irs.date_added   , irs.box_name     , irs.size            , irs.steams      , irs.price        , 
                        irs.cliente_id   , irs.ship_cost    , irs.cost_cad        , irs.price_cad   , irs.gorPrice     , 
                        irs.duties       , irs.handling_pro , irs.total_duties    , irs.price_quick ,
                        s.name as psubcatego , irs.product_subcategory,
                        IFNULL(ft.name,'') as feature_name
                  from invoice_requests_subcli irs
                 INNER JOIN product p on irs.product = p.id
                  left join subcategory s on p.subcategoryid = s.id  
                  left JOIN buyer_requests br ON irs.comment = br.id
                  left JOIN features ft ON br.feature = ft.id                  
                 where irs.buyer      = '" . $userSessionID . "'
                   and irs.id_fact    = '" . $idfac . "' 
                   and irs.cliente_id = '" . $cliente_inv . "'    
                 order by s.name,irs.prod_name  ";

    $result = mysqli_query($con, $sqlDetalis);     


 ?>

            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
			<section id="middle">

				<!-- page title -->
				<header id="page-header">
					<h1>Invoice</h1>
					<ol class="breadcrumb">
						<li><a href="#">Pages</a></li>
						<li class="active">Invoice Page</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">

						<div class="panel-body">
                                                    <input type="submit" id="submitu" class="btn btn-success btn-sm" name="submitu" value="Update Price">     

							<div class="row">

								<div class="col-md-6 col-sm-6 text-left">
                                                            <!--img src="<?php echo SITE_URL; ?>includes/assets/images/logo.png" alt="admin panel" height="35"/--> 
                                                                    <br>
									<h4><strong>Client</strong> Details.</h4>
									<ul class="list-unstyled">
										<li><strong>Name         :</strong><?php echo $buyerOrderCab['name_client']; ?></li>
										<li><strong>Country      :</strong><?php echo "Canada"; ?></li>
										<li><strong>Delivery Date:</strong> <?php echo $buyerOrderCab['date_added']; ?></li>                                                                                                                                                                                                                                             
									</ul>

								</div>

								<div class="col-md-6 col-sm-6 text-right">
                                                                        <br>                                                                    
                                                                        <br>                                                                                                                                            
									<h4><strong>Shipping</strong> Details</h4>

									<ul class="list-unstyled">
										<li><strong>P.O. NUMBER:</strong><?php echo $buyerOrderCab['order_number']; ?></li>

                                                                                <li>
                                                                                    <strong>Total Boxes:</strong><?php echo $buyerOrderCab['total_boxes']; ?>
                                                                                </li>
                                                                                <?php if ($buyerOrderCab['bill_state'] == "P") {  ?>                                                                              
                                                                                        <li><strong>Gross Weight:</strong> Pending... </li>                                                                                
                                                                                        <li><strong>Volume Weight:</strong> Pending... </li>
                                                                                <?php }else {  ?> 
                                                                                        <li><strong>Gross Weight:</strong><?php echo $buyerOrderCab['gross_weight']; ?></li>                                                                                
                                                                                        <li><strong>Volume Weight:</strong><?php echo $buyerOrderCab['volume_weight']; ?></li>
                                                                                <?php }  ?>        
                                                                                

                                                                                
                                                                                <li>                                                                                    
                                                                                    <div class="col-md-6 col-sm-6 text-right"></div>  
                                                                                </li>
									</ul>

								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Variety</th>
											<th>Cost</th>
                                                                                        <th>Price</th>
											<th>Qty</th>
											<th>Subtotal</th>
                                                                                        <th>Box Packing</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                                 $cn=1;
                                                                                 
                                                                              while($row = mysqli_fetch_assoc($result))  {
                                                                                  
                                                                                         
                                                                                          
                                                                            ?>
										<tr>
										    <td>
                                                                                        <?php
                                                                                                $res = "";
                                                                                            if ($row['boxtype'] == "HB") {
                                                                                                $res = "Half Boxes";
                                                                                            } else if ($row['boxtype'] == "EB") {
                                                                                                $res = "Eight Boxes";
                                                                                            } else if ($row['boxtype'] == "QB") {
                                                                                                $res = "Quarter Boxes";
                                                                                            } else if ($row['boxtype'] == "JB") {
                                                                                                $res = "Jumbo Boxes";
                                                                                            }
        

         // Verificacion Stems/Bunch
       // $sel_bu_st = "select box_type from product where id = '" . $row['product'] . "' ";
                                                                                            
        $sel_bu_st = "select box_type, subcategoryid from product where name = '" . $row['prod_name'] . "' ";                                                                                            
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                   // $Subtotal= $row['steams'] * $row['bunchqty'] * $row['price_cad'];
                    $Subtotal=  $row['qty_pack'] * $row['price_quick'] ;                  
                    $qtyFac = $row['qty_pack'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['qty_pack'] * $row['price_quick'] ;
                    $qtyFac = $row['qty_pack']; 
                    $unitFac = "BUNCHES";                   
              }        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);


                                                                                        ?>
                                                                                        <?php  if ($row['grower_id'] == $tmp_idorder) {?>                                                                                                
                                                                                                <p>     </p>
                                                                                                <strong><?php echo $row['name_grower']." (". $cajas." ".$res.")"  ?></strong> 
                                                                                                <p>     </p>
                                                                                        <?php  }?>
                                                                                                
												<!--div><?php echo $row['prod_name']." ".$subc['name']." ".$row['size']." cm"." ".$row['steams']." st/bu"; ?></div-->
                                                                                                <div><?php echo $row['product_subcategory']." ".$row['prod_name']." ".$row['size']." cm"." ".$row['steams']." st/bu ".$row['feature_name']; ?></div>

										    </td> 
                                                                                                                            
                                                                                    
										    <td>
                                                                                        <?php echo "$".number_format($row['price_cad'], 2, '.', ',') ; ?>
                                                                                    </td>                                                                                    
                                                                                    
										    <td>                                                                   
                                                                                        <input type="text" class="form-control" name="price-<?php echo $row['gid'] ?>" id="price-<?php echo $row['gid'] ?>"  style="margin-top:2px; height:35px; padding:3px; width:150px;border-radius: 5px;"   value="<?php echo $row['price_quick'] ?>"></td>
                                                                                        <!--input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/-->
                                                                                    </td>                                                                                                                                                                        

                                                                                    
										    <td>
                                                                                        <?php echo $qtyFac;?>
                                                                                    </td>
                                                                                    
										    <td>
                                                                                        <?php echo "$".number_format($Subtotal, 2, '.', ',');?>
                                                                                    </td>                                                                                        
                                                                                    
										    <td>                                                                                       
                                                                                        <input type="text" class="form-control" name="boxp-<?php echo $row['gid'] ?>" id="boxp-<?php echo $row['gid'] ?>"  style="margin-top:2px; height:35px; padding:3px; width:80px;border-radius: 5px;"   value="<?php echo $row['box_qty_pack'] ?>"></td>
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/>                                                                                        
                                                                                    </td>                                                                                    
                                                                                 
										</tr>																				
											<?php 
                                                                                                $totalCal = $totalCal + $Subtotal;   
                                                                                                $cn++;
                                                                                                                                                                      
                                                                             } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>Contact</strong> Details</h4>

									<p class="nomargin nopadding">
										<strong>Note:</strong> 
										<?php echo $buyerOrderCab['quick_desc']; ?>
									</p><br><!-- no P margin for printing - use <br> instead -->

									<!--address>
                                                                                Address<br>
                                                                                Vancouver, Canada<br>
                                                                                Phone: <br>
                                                                                Email:                                                                                
									</address-->

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<ul class="list-unstyled">
                                                                            <li>
                                                                                <strong>Sub - Total Amount: </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>
                                                                            </li>                                                     
                                                                            <?php if ($buyerOrderCab['bill_state'] == "P") {  ?>      
                                                                                    <!--li><strong>Freight ($ per kg):</strong> Pending... </li>                                                                            
                                                                                    <li><strong>Air Waybill AWC:</strong> <?php echo "$".number_format($buyerOrderCab['air_waybill'], 2, '.', ',');  ?>  </li>                                                                                
                                                                                    <li><strong>Charges Dues Agent AWA:</strong> <?php echo "$".number_format($buyerOrderCab['charges_due_agent'], 2, '.', ',');  ?></li>
                                                                                    <li><strong>FLF Handling:</strong> Pending... </li>                                                                            
                                                                                    <li><strong>Total:</strong> Pending... </li-->  
                                                                            <?php }else {  ?>  
                                                                                    <li><strong>Freight ($ per kg):         </strong> <?php echo "$".number_format($buyerOrderCab['freight_value'], 2, '.', ',');?> </li>                                                                            
                                                                                    <li><strong>Air Waybill AWC:            </strong> <?php echo "$".number_format($buyerOrderCab['air_waybill'], 2, '.', ',');  ?>  </li>                                                                                
                                                                                    <li><strong>Charges Dues Agent AWA:     </strong> <?php echo "$".number_format($buyerOrderCab['charges_due_agent'], 2, '.', ',');  ?></li>
                                                                                    
                                                                                    <li><strong>FLF Handling:               </strong> <?php echo "$".number_format($buyerOrderCab['handling'], 2, '.', ',');  ?> </li>                                                                            
                                                                                    <li><strong>Total:                      </strong> <?php echo "$".number_format($buyerOrderCab['grand_total'], 2, '.', ',');  ?> </li>                                                                                      
                                                                            <?php }  ?>                                                                                            
                                                                                

									</ul>  

								</div>
                                                            
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">
						<div class="panel-body">
							<a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_etiqueta.php?b=<?php echo $idfac."&id_cli=".$cliente_inv ?>" target="_blank"><i class="fa fa-print"></i> LABEL</a>                                                    
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
			</section>
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
        </form>
</html>
