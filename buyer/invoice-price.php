<?php
session_start();


require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');

$fact_number = $_GET['idi'];

$userSessionID = $_SESSION["buyer"];

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}

/* * *******get the data of session user*************** */
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);


if (isset($_REQUEST["total"])) {

    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {


        $update = "update invoice_requests
                      set price_Flf      = '" . $_POST["qty-"   . $_POST["pro-" . $i]] . "',
                          salesPriceCli  = '" . $_POST["price-" . $_POST["pro-" . $i]] . "'
                    where offer_id ='" . $_POST["pro-" . $i] . "'
                      and id_fact  = '" . $fact_number . "'  ";


        mysqli_query($con, $update);

    }
}

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 02 Jul 2021
Structure MarketPlace previous to buy
**/


// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

include('../back-end/inc/header_ini.php');
?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<?php

$sel_products = "select ir.id_fact            , ir.id_order       , ir.order_serial
                   from invoice_requests ir
                  where id_fact  = '" . $fact_number . "'";


$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;
$display = 900;
$XX = '<div class="notfound">No Item Found !</div>';


function initial($fuser, $init, $display){
    $query = "select ir.id_fact            , ir.id_order       , ir.order_serial  ,
                            ir.cod_order          , ir.product        , ir.sizeid        ,
                            ir.qty                , ir.buyer          , ir.date_added    ,
                            ir.bunches            , ir.box_name       , ir.lfd           ,
                            ir.comment            , ir.box_id         , ir.shpping_method,
                            ir.mreject            , ir.bunch_size     , ir.unseen        ,
                            ir.inventary          , ir.offer_id       , ir.prod_name     ,
                            ir.product_subcategory, ir.size           , ir.boxtype       ,
                            ir.bunchsize          , ir.boxqty         , ir.bunchqty      ,
                            ir.steams             , ir.gorPrice       , ir.box_weight    ,
                            ir.box_volumn         , ir.grower_box_name, ir.reject        ,
                            ir.reason             , ir.coordination   , ir.cargo         ,
                            ir.color_id           , ir.gprice         , ir.tax           ,
                            ir.cost_ship          , round(ir.handling,0) as handling     ,
                            ir.grower_id          , ir.offer_id_index,
                            substr(rg.growers_name,1,19) as name_grower ,ir.salesPriceCli,
                            ir.price_Flf
                       from invoice_requests ir
                      INNER JOIN growers rg ON ir.grower_id = rg.id
                      where id_fact  = '" . $fuser . "'
                      order by ir.grower_id,ir.prod_name  LIMIT " . $init . ",$display ";

    return $query;
}




if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = initial($fact_number, $_POST["startrow"], $display);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = initial($fact_number, 0, $display);
    $result2 = mysqli_query($con, $query2);
}


 $display;
?>
<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Invoice </strong>
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->



						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


  <div class="modal-body">
              <!-- panel content -->
              <form name="frmrequest" id="frmrequest" method="post" action="">
                  <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                  <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

                  <h5>Price Invoice</h5>
                  <input type="submit" id="submitu" class="btn btn-success" name="submitu" value="Update All">

                    <!-- fullscreen -->
                          <a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
                            <span class="group-icon">
                              <i class="fi fi-expand"></i>
                              <i class="fi fi-shrink"></i>
                            </span>
                          </a>

                          <div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">



                  </div>

                  <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table-datatable table table-bordered table-hover table-striped"
                                data-lng-empty="No data available in table"
                                data-responsive="true"
                                data-header-fixed="true"
                                data-select-onclick="true"
                                data-enable-paging="true"
                                data-enable-col-sorting="true"
                                data-autofill="false"
                                data-group="false"
                                data-items-per-page="10">
                                    <thead>
                                      <tr>
                                        <th>Product</th>
                                        <th>Stems</th>
                                        <th>Bunch</th>
                                        <th>Price</th>
                                        <th>Price FLF</th>
                                        <th>Marketplace</th>                                        

                                      </tr>
                              </thead>

                              <tbody>
                                <?php
                                $i = 1;

                                while ($producs = mysqli_fetch_array($result2)) {


                                    if ($producs["gprice"] > 0) {
                                        $kp = $producs["gprice"];
                                    } else {
                                        $kp = $price["price"];
                                    }
                                    ?>


                                    <tr>
                                        <td><?php echo $producs["name_grower"] ?> <?php echo $producs["prod_name"]." " ?><?php echo $producs["size"]." cm." ?> <?php echo $producs["product_subcategory"] ?><?php echo $producs["colorname"] ?><input type="hidden" name="subcategoryname-<?php echo $producs["gid"] ?>" value="<?php echo $producs["subs"] ?>"></td>
                                        <td><?php echo $producs["steams"]  ?></td>

                                        <td><?php echo $producs["bunchqty"] ?> </td>

                                        <td><input type="text" class="form-control" name="price-<?php echo $producs["offer_id"] ?>" id="price-<?php echo $producs["offer_id"] ?>" value="<?php echo $producs["salesPriceCli"] ?>"></td>
                                        <td>
                                            <input type="text" class="form-control" name="qty-<?php echo $producs["offer_id"] ?>" id="qty-<?php echo $producs["offer_id"] ?>" value="<?php echo $producs["price_Flf"] ?>">

                                            <input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $producs["offer_id"] ?>"/>
                                            <input type="hidden" class="form-control" name="grower-<?php echo $producs["offer_id"] ?>" value="<?php echo $producs["growerid"] ?>">
                                        </td>
                                    </tr>
                                    <?php $i++;
                                }
                                ?>
                              </tbody>
                              <tr>
                                <th>Product</th>
                                <th>Stems</th>
                                <th>Bunch</th>
                                <th>Price</th>
                                <th>Price FLF</th>
                              </tr>
                          </table>

                      </div>
                  </div>
                  <input type="hidden" name="totalrow" value="<?php echo $i ?>"/>
              </form>




              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
            <form method="post" name="frmfprd" action="">
                <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
            </form>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control" id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>
                                  <br>
                                 <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>
                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>




			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
      <script tpe="text/javascript">
          function funPage(pageno) {
              document.frmfprd.startrow.value = pageno;
              document.frmfprd.submit();
          }
          function docolorchange() {
              window.location.href = '/buy.php?id=<?php echo $_GET["id"] ?>&categories=<?php echo $strdelete ?>&l=<?php echo $_GET["l"] ?>&c=' + $('#color').val();
          }
          function frmsubmite() {
              document.frmfilter.submit();
          }
          function doadd(id) {
              var check = 0;
              if ($('#qty-' + id).val() == "") {
                  $('#ermsg-' + id).html("please enter box qty.")
                  check = 1;
              }

              if (check == 0) {
                  $('#ermsg-' + id).html("");
                  $('#gid6').val(id);
                  $('#frmrequest').submit();
              }
          }
      </script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
