<?php

include("../config/config_gcp.php");

//$buyer_order_id = $_SESSION['buyer_order_id'];

$buyer_order_id = $_REQUEST['order_val'];

$id_request = $_REQUEST['date_range'];

$id_qty = $_REQUEST['qtyRange'];  // cantidad Request Pending

$qtybun         = $_REQUEST['box_quantity'];  // cantidad Offer

//$box_quantity   = $_REQUEST['box_quantity'];
//$btype          = explode('_', $_REQUEST['box_quantity']);


$order_serial   = 1;

//$typereq      = $_REQUEST['type'];

  $typereq        = 1;   /////  Cambiar en Proceso Annie
  
//$boxtype      = $_REQUEST['type_box'];
  $boxtype        = 37;
  

$lote_control   = $_REQUEST['req_grow'];
$id_grower      = 0;

$id_client      = $_REQUEST['tag'];
$discount       = 0;

$conType        = 4;                      //$_REQUEST['conType'];
$buyer          = $_REQUEST['buyer'];
$today          = date('Y-m-d');
$productName    = $_REQUEST['productName'];

if (!empty($_REQUEST['productId'])) {
    $cId          = 'SubClient-Confirmed';
    $productId    = $_REQUEST['productId'];
    $texto        = $_REQUEST['productText'];
} 

if (!empty($_REQUEST['productId_requ'])) {
    $cId          = 'SubClient-Pending';
    $productId    = $_REQUEST['productId_requ'];    
    $texto        = $_REQUEST['productText_requ'];
}

$arraysize      = explode("-",$texto);    
    
$buyerPrice     = $_REQUEST['buyerPrice'];
$discount       = 0;
$date_range     = $_REQUEST['date_range'];
$d              = explode(' - ', $date_range);

$date_ship      = $_REQUEST['date_ship'];
$date_del       = $_REQUEST['date_del'];

if (!empty($_REQUEST['delivery_date'])) {
    $delivery_date = $_REQUEST['delivery_date'];
} else {
    $delivery_date = date('Y-m-d');
}

$growers = $_REQUEST['growers'];
$growers = $growers.",";
$order   = $_REQUEST['order_val'];
$taxes   = $_REQUEST['tax'];
$ship_p  = $_REQUEST['ship_p'];
$had_l   = $_REQUEST['hand_l'];


//------------  Size ----------------------
$sql_size    = "select id from sizes where name = '".$arraysize[2]."'";
$data_size   = mysqli_query($con, $sql_size);
$dt_id       = mysqli_fetch_assoc($data_size);
$sizeId      = $dt_id['id'];

//------------  Feature ----------------------

$sql_fetu    = "select id from features where name = '".$arraysize[5]."' limit 1  ";
$data_fetu   = mysqli_query($con, $sql_fetu);


    $fet_id      = mysqli_fetch_assoc($data_fetu);
    $featureMp   = $fet_id['id'];
    
if ($featureMp=="") {
    $featureMp = 0;   
}

//------------Tallos por Bunche----------------------

$sql_stem    = "select gpb.product_id ,gpb.sizes , gpb.bunch_sizes   ,
                       bs.name as bunchesv
                 from growcard_prod_bunch_sizes gpb
                 LEFT JOIN bunch_sizes AS bs  ON gpb.bunch_sizes = bs.id
                where gpb.product_id = '" . $productId . "'
                  and gpb.sizes    = '" . $sizeId . "'
                group by  gpb.product_id , gpb.sizes , gpb.bunch_sizes  ";

       $data_stem   = mysqli_query($con, $sql_stem);

    $bunche_id = mysqli_fetch_assoc($data_stem);
    $stems     = $bunche_id['bunchesv'];
    
if ($stems=="") {
    $stems = 0;   
}




//------------shippingMethod----------------------
$sq_shpp        = "select  shipping_method , del_date from  buyer_orders where  id='".$order."'";
$data_sp_sh     = mysqli_query($con, $sq_shpp);
$dt_sp_sh       = mysqli_fetch_assoc($data_sp_sh);

$shippingMethod = $dt_sp_sh['shipping_method'];
$delivery_date  = $dt_sp_sh['del_date'];
/*----------------------------------------------*/



//------------ BUYER
$sq_buyer        = "select first_name, last_name from  buyers where  id='".$buyer."'";
$data_buyer     = mysqli_query($con, $sq_buyer);

$dt_buyer       = mysqli_fetch_assoc($data_buyer);
$nameBuyer = $dt_buyer['first_name']." ".$dt_buyer['last_name'];

/*----------------------------------------------*/

//------------ Subcategory
$sq_subcate   = "select subcategoryid from product where id ='".$productId."'";
$data_subcate = mysqli_query($con, $sq_subcate);

$dt_subcate = mysqli_fetch_assoc($data_subcate);
$subcateId  = $dt_subcate['subcategoryid'];

/*----------------------------------------------*/

function precioProductSelected($con,$subcategory,$sizeIdp,$productp,$featureId){

  ////////////////////////////////////////////////////////
  // Precios por producto
  if($featureId!=""){
    $feat =  " and feature = '$featureId'";
  }else{
    $feat = '';
  }

        $sql_priceProd = "select id      ,  idsc      ,  size    ,  stem_bunch,  description,  type,  feature,  id_ficha,
                            relacion,  price_card,  id_param,  product_id
                       from card_param_product_price
                      where idsc       = '" . $subcategory . "'
                          $feat
                        and product_id = '" . $productp . "'
                        and size       = '" . $sizeIdp . "'"     ;

      $rs_priceProd  = mysqli_query($con, $sql_priceProd);
      $priceProd     = mysqli_num_rows($rs_priceProd);
      
      $priceProdDet  = mysqli_fetch_array($rs_priceProd);

   if ($priceProd < 1) {

      $sql_priceSub = "select id,  idsc,  subcategory,  size,  stem_bunch,  description,  type,
                feature,  factor,  id_ficha,  relacion,  price_card,  price_special,
                price_adm
             from grower_parameter
              where idsc = '" . $subcategory   . "'
              $feat
              and size = '" . $sizeIdp . "'  ";

     $rs_priceSub  = mysqli_query($con, $sql_priceSub);
     $priceSubca  = mysqli_fetch_array($rs_priceSub);

        $precioSubcliente = $priceSubca["price_card"];
     }else{
         $precioSubcliente = $priceProdDet["price_card"];
  }

  return $precioSubcliente;
}


function vector($gro, $price, $taxes, $ship_p, $handling){
    $grower = substr($gro, 0, -1);
    $prices = substr($price, 0, -1);
    $taxes  = substr($taxes, 0, -1);
    $ships  = substr($ship_p, 0, -1);
    $hand   = substr($handling, 0, -1);
    $grow   = explode(',', $grower);
    $pric   = explode(',', $prices);
    $tax    = explode(',', $taxes);
    $ship   = explode(',', $ships);
    $had    = explode(',', $hand);
    
    $array  = array();
    
    for ($i = 0; $i < count($grow); $i++) {
        array_push($array, array($grow[$i], $pric[$i], $tax[$i], $ship[$i], $had[$i]));
    }
    return $array;
}

$price  = "";
$taxx   = "";
$shipps = "";
$hand_s = "";

function menor_price($array_s){   
    
    $filas  = count($array_s);    
    $filtro = array();
    $fil    = array();

    for ($i = 0; $i < $filas; $i++) {
        $g_id   = $array_s[$i][0];
        $price  = $array_s[$i][1];
        $taxx   = $array_s[$i][2];
        $shipps = $array_s[$i][3];
        $hand_s = $array_s[$i][4];
        $j = $i + 1;
        
        for ($j; $j < $filas; $j++) {
            if ($array_s[$j][0] == $g_id) {
                if ($array_s[$j][1] < $price) {
                    $price  = $array_s[$j][1];
                    $taxx   = $array_s[$j][2];
                    $shipps = $array_s[$i][3];
                    $hand_s = $array_s[$i][4];
                }
            }
        }

        if (!in_array($g_id, $fil)) {
            array_push($fil, $g_id);
            array_push($filtro, array($g_id, $price, $taxx, $shipps, $hand_s));
        }

    }
    return $filtro;
}     

// Fin Funciones

$cod_prices = array();
$cod_prices = vector($growers, $buyerPrice, $taxes, $ship_p, $had_l);

/*-----------------------------------------*/

$menor_price = array();
$menor_price = menor_price($cod_prices);

/*menor price*/

$fil_prices  = array();
$fil_growers = array();
$fil_taxes   = array();
$fil_shipps  = array();
$fil_had     = array();

for ($i = 0; $i < count($menor_price); $i++) {
    $fil_prices[]  = $menor_price[$i][1];
    $fil_growers[] = $menor_price[$i][0];
    $fil_taxes[]   = $menor_price[$i][2];
    $fil_shipps[]  = $menor_price[$i][3];
    $fil_had[]     = $menor_price[$i][4];
}

    
    $qry     = "select id,id_order,order_serial from  buyer_requests where  id_order='" . $order . "'  order by  order_serial DESC LIMIT 0,1";
    $data    = mysqli_query($con, $qry);
    $numrows = mysqli_num_rows($data);
    
    if ($numrows > 0) {
        while ($dt = mysqli_fetch_assoc($data)) {
            $order_serial = $order_serial + $dt['order_serial'];
        }
    }
    $qrt        = "select  concat( order_number ,'-',order_serial,'-','" . $order_serial . "')  as cod   from  buyer_orders where  id='" . $order . "'";
    $datas      = mysqli_query($con, $qrt);
    $dts        = mysqli_fetch_assoc($datas);
    $cod_order  = $dts['cod'];
    
     // Proceso para obtener LFD           
    $getShippingMethod = "select connections , days as days_ship,
                                 connect_group
                           from shipping_method 
                          where id='" . $shippingMethod . "'";
    
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    
    $temp_conn = explode(',', $shippingMethodDetail['connect_group']);
    
     $id_conn = $temp_conn[1];  // Escojer conexion Ejemplo:   $temp_conn[2];
     
     $var= date("w");
     
     $getConNew = mysqli_query($con, "select days ,trasit_time 
                                        from more_days_connection
                                       where id_conn='" . $id_conn . "' 
                                         and days >= '" . $var . "' LIMIT 0,1");  

    $trasit_time = 1;
    
     while ($conNew = mysqli_fetch_array($getConNew)) {
         $trasit_time = $conNew['trasit_time'];
     }                
            
       
    $sqlids = "SELECT date_add('".$delivery_date."', INTERVAL -".$trasit_time." DAY) fecha";  
    $row_sqlids = mysqli_query($con, $sqlids);
                                                            
    while ($fila =mysqli_fetch_assoc($row_sqlids)) {
              $date_lfd= $fila["fecha"];
    }
    
    $fecha_tmp = $date_lfd;
    $dayofweek = date('D', strtotime($date_lfd));  
    if ($dayofweek == "Sun")  {
         $date_lfd = strtotime ('-2 day',strtotime ($fecha_tmp) );
         $date_lfd = date ( 'Y-m-j' , $date_lfd );
    }else {
         $date_lfd=$fecha_tmp;                    
     }                                                                            

     
        //request_product_ajax_mp  (1.0)                        
        // $activity = "(1.)".$IdMaximo." (2)".$order." (3)".$order_serial ." (4)". $productId ." (5)". $buyer ." (6) ".$texto ." otros ".$cod_order." (1x)".$sizeId." (2x)".$featureMp." (3x)".$stems." (4x)".$btype [0]." (5x)".$boxtype." (6x)".$today." (7x)". $typereq . " (8x)" . $date_lfd . " (9x)" . $cId . " (10x)" . $shippingMethod . " (11x)" . $id_client ." (12x)" . $id_grower." (13)" . $qtybun ." (14)" . $date_ship." (15)" . $date_del;
        // $insert_login = "insert into activity set grower='9900',ldate='" . date("Y-m-d") . "' , type='market' , activity='" . $activity . "',ltime='12',atype='DAGY'";
        // mysqli_query($con,$insert_login);
     
/////////////////////////////////////////////////////////////////////////////////////
// Price  Category     
     
          $sql_price = "select p.id , p.name , p.categoryid    , p.subcategoryid ,
                               sc.name as product_subcategory  , sc.price_client
                          from product p
                         INNER JOIN subcategory AS sc ON (p.subcategoryid = sc.id and p.categoryid = sc.cat_id)
                         where p.id = '" . $productId . "'  ";

            $rs_price    = mysqli_query($con, $sql_price);
            $dt_price    = mysqli_fetch_assoc($rs_price);
            $priceQuickB = $dt_price['price_client'];   
            
/////////////////////////////////////////////////////////////////////////////////////
// Last Price
        $ultimo= 0;
        
        $sel_ultprice = "select product, prod_name,cliente_id ,price_quick
                           from invoice_requests_subcli
                          where product    = '" . $productId . "'
                            and cliente_id = '" . $id_client . "'
                          order by id desc limit 0,1";                                                                                        
        
        $rs_ultprice = mysqli_query($con,$sel_ultprice);
        
        while($last_price=mysqli_fetch_array($rs_ultprice))	{
            $ultimo = $last_price['price_quick'];
        }

        
        
/////////////////////////////////////////////////////////////////////////////////////        
        
      //    if ( $ultimo == 0 )  {                                                                    
      //          $priceinv = $priceQuickB;                                                                 
      //    } else {
      //          $priceinv = $ultimo;                                                                 
      //    }            
          
$priceinv = precioProductSelected($con , $subcateId , $sizeId , $productId , $featureMp );                    
  ////////////////////////////////////////////////////////////////////////////////       
         
if (!empty($_REQUEST['productId'])) {
    
    
        $qryMax     = "select (max(id)+1) as id from reser_requests";
        $dataMaximo = mysqli_query($con, $qryMax);
        
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
            $IdMaximo= $dt['id'];
		}
                
                
        $qryLote     = "select (max(lote)+1) as lote , (max(lote)) as lotemod
                          from reser_requests
                         where id_order  = '" . $order .  "' 
                           and id_client = '" . $id_client ."' ";
        
        $dataLote = mysqli_query($con, $qryLote);
        
        while ($dtlote = mysqli_fetch_assoc($dataLote)) {
            $IdLote= $dtlote['lote'];
            $IdLotemod= $dtlote['lotemod'];
        }                
                
                
          if ( $lote_control == "ADD" )  {                                                                    
                $lote = $IdLote;                                                                 
          } else {
                $lote = $IdLotemod;                                                                 
          }                            

                    $query_assig = "INSERT INTO reser_requests
                                   (id              ,              id_order         ,      order_serial   ,          cod_order        ,
                                    product         ,              sizeid           ,      feature        ,          noofstems        ,
                                    qty             ,              buyer            ,      boxtype        ,          date_added       ,                
                                    type            ,              lfd              ,      comment        ,          isy              ,
                                    shpping_method  ,              inventary        ,      id_client      ,          id_grower        ,
                                    date_ship       ,              date_del         ,      id_reser       ,          price            ,
                                    num_box         ,              lote)
                            VALUES ('".$IdMaximo."'           , '" . $order .  "'   , '" . $order_serial . "' , '" . $cod_order . "'  ,
                                    '" . $productId . "'      , '" . $sizeId . "'   , '" . $featureMp . "'    , '" . $stems . "'      ,
                                    '" . $qtybun . "'         , '" . $buyer .  "'   , '" . $boxtype . "'      , '" . $today . "'      ,                
                                    '" . $typereq . "'        , '" . $date_lfd . "' , '" . $cId . "'          ,          0            ,
                                    '" . $shippingMethod . "' ,       0             , '" . $id_client ."'     , '" . $id_grower . "'  ,
                                    '" . $date_del       . "' , '" . $date_del . "' , '" . $arraysize[4] . "' , '" . $priceinv . "',
                                    '1'                       , '" . $id_request . "'    )";     
                    mysqli_query($con, $query_assig);
                    
                    
                    
                    
                                      $update_pck = "update grower_offer_reply 
                                                        set reserve = reserve + '" . $qtybun . "'  
                                                      where id      = '" . $arraysize[4] . "'      ";
                                      
                                         mysqli_query($con, $update_pck);  
                                         
                                         //////////////////////////////////////////////////////////
                                   if ( $qtybun == $id_qty )  {   
                                            $update_pre = "update buyer_requests 
                                                              set comment =  'Confirmed'  
                                                            where id      = '" . $id_request . "'   ";

                                               mysqli_query($con, $update_pre);                                                                                                                                                                 
                                   }else{
                                             $qtyReq = $id_qty-$qtybun;
                                             
                                                if ( $qtyReq < 0 )  {   
                                                            $qtyReq = 0;
                                                    $statusPartial = 'Confirmed';                                                            
                                                } else {
                                                    $statusPartial = 'SubClient-Partial';
                                                }                                       
                                            $update_partial = "update buyer_requests 
                                                                  set comment = '" . $statusPartial . "',
                                                                      qty     = '" . $qtyReq . "'
                                                                where id      = '" . $id_request . "'   ";

                                               mysqli_query($con, $update_partial);                                                                                                                                                                                                        
                                   }      
}         
         
         
if (!empty($_REQUEST['productId_requ'])) {
    
        $qryMax     = "select (max(id)+1) as id from buyer_requests";
        $dataMaximo = mysqli_query($con, $qryMax);
        
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
            $IdMaximo= $dt['id'];
		}
        
      
                $query = "INSERT INTO buyer_requests
                                   (id              ,              id_order         ,      order_serial   ,          cod_order        ,
                                    product         ,              sizeid           ,      feature        ,          noofstems        ,
                                    qty             ,              buyer            ,      boxtype        ,          date_added       ,                
                                    type            ,              lfd              ,      comment        ,          isy              ,
                                    shpping_method  ,              inventary        ,      id_client      ,          id_grower        ,
                                    date_ship       ,              date_del         ,      price          ,          num_box)
                            VALUES ('".$IdMaximo."'           , '" . $order .  "'   , '" . $order_serial . "' , '" . $cod_order . "'  ,
                                    '" . $productId . "'      , '" . $sizeId . "'   , '" . $featureMp . "'    , '" . $stems . "'      ,
                                    '" . $qtybun . "'         , '" . $buyer .  "'   , '" . $boxtype . "'      , '" . $today . "'      ,                
                                    '" . $typereq . "'        , '" . $date_lfd . "' , '" . $cId . "'          ,          0            ,
                                    '" . $shippingMethod . "' ,       0             , '" . $id_client ."'     , '" . $id_grower . "'  ,
                                    '" . $date_del       . "' , '" . $date_del . "' , '" . $priceinv . "'  , '1')";     
  
//} 



// Send Message
    $msg = urlencode('Requests... '.$nameBuyer.' '.$productName);
    $result = file_get_contents('https://slack.com/api/chat.postMessage?token=xoxp-2937059456-654311528562-670940963591-bda4efad85241858d654e3ad40197dcb&channel=Requests&text='.$msg.'&pretty=1',false);
// End



//  PROCESO 2

$growers = $fil_growers;
$prices = $fil_prices;

$cont_prices = 0;

if (mysqli_query($con, $query)) {
    
                                      $update_pck = "update grower_offer_reply 
                                                        set reserve = reserve + '" . $qtybun . "'  
                                                      where id      = '" . $arraysize[4] . "'      ";
                                
                                              mysqli_query($con, $update_pck);

    $id         = $IdMaximo;
    $xs         = $id;
    
      // Proceso Nuevo      
  
    $checkShipp = "select bo.id            , bo.buyer_id  , bo.order_number, bo.order_date  , bo.shipping_method, 
                          bo.del_date      , bo.date_range, bo.is_pending  , bo.order_serial, bo.seen           ,
                          bo.delivery_dates, bo.lfd_grower, bo.qucik_desc  , bo.type_market , bo.delivery_day   ,
                          sm.connect_group
                     from buyer_orders bo  
                    inner join shipping_method sm  on bo.shipping_method     = sm.id
                    where bo.id ='" . $order . "' ";
    
    $reqShipp   = mysqli_query($con, $checkShipp);
    $info = mysqli_fetch_assoc($reqShipp);
    
$temp=explode(",",$info["connect_group"]); 

$tot_conn = count($temp);
    




for ($i = 0; $i <= $tot_conn; $i++) {  
    
    $connecx = $temp[$i];
    
    $qryConn     = "select id,connection_name,days,trasit_time,destiny, inventary
                      from connections
                     where id ='" . $connecx . "' ";
    
    $dataConn = mysqli_query($con, $qryConn);
         
    while ($dtConn = mysqli_fetch_assoc($dataConn)) {
        
            $IdInven = $dtConn['inventary'];

            $qryGrow    = " select g.id , g.growers_name , gp.product_id ,inventory
                              from growers g
                             inner join grower_product gp on g.id=gp.grower_id
                             where g.active      = 'active'
                               and gp.product_id = '" . $productId . "' ";
                
            $dataGrow = mysqli_query($con, $qryGrow);
            $ig = 0;
            while ($dtGrow = mysqli_fetch_assoc($dataGrow)) {
                
                $temp = explode(',', $dtGrow['inventory']);
                
                                                
                                $insGrow = "insert into request_growers set 
                                                tax        = 0 , 
                                                gprice     = 0 , 
                                                shipping   = 0 ,
                                                handling   = 0 ,
                                                gid        = '" . $dtGrow['id'] . "'  ,
                                                rid        = '" . $xs . "'            ,
                                                bid        = '" . $buyer . "'         ,
                                                mailsend   = '21'    ,
                                                order_view = 0      "; 
                        
                                mysqli_query($con, $insGrow);  

                        $ig++;
            }
   }
}   

}

 }         
 
 // Validar Cabecera de Clientes

$query_cab = "select id, order_cli , client_id , comment1  
                from subclient_cab
               where order_cli = '" . $order   . "'  
                 and client_id = '" . $id_client . "'  ";

$cli_ord  = mysqli_query($con, $query_cab);
$controlf  = mysqli_num_rows($cli_ord);

if ($controlf < 1) {
      $insert_clientCab = "INSERT INTO subclient_cab
                                   (order_cli   ,  
                                    client_id   , 
                                    comment1  )
                            VALUES ('" . $order   ."'     ,  
                                    '" . $id_client . "'  , 
                                    '.') ";
      
       mysqli_query($con, $insert_clientCab);    
    
}


    echo 'true';
    
    

?>
