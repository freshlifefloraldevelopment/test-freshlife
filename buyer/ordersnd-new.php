<?php
session_start();

$userSessionID = $_SESSION["buyer"];

require_once("../config/config_gcp.php"); 
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini.php');


if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}


/* * *******get the data of session user*************** */
$sel_info = "select * from buyers where id='$userSessionID' ";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);


?>

<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<?php

$sel_products = "select id, qucik_desc,del_date
                  from buyer_orders 
                 where buyer_id= '$userSessionID' 
                     and del_date > date_add(curdate(), INTERVAL -180 DAY) order by id desc";


$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;
$display = 150;
$XX = '<div class="notfound">No Item Found !</div>';


function initial($fuser, $init, $display, $bid){
      $query = "select id, qucik_desc,del_date
                  from buyer_orders 
                 where buyer_id= '" . $bid . "' 
                  and del_date > date_add(curdate(), INTERVAL -180 DAY) order by id desc";

    return $query;
}


if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = initial($fact_number, $_POST["startrow"], $display,$userSessionID );
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = initial($fact_number, 0, $display,$userSessionID);
    $result2 = mysqli_query($con, $query2);
}


 $display;
?>
<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Orders </strong>
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
	<!-- Boton Create New Order -->
            <div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">

            <span style="float:left;width:90%;" data-toggle="modal" data-target=".buying_method_modal">
                 <a href="javascript:void(0)" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;width:100%;"><i class="fa fa-plus"></i>Create new order.</a>
            </span>

	    <!-- fullscreen -->
                    <a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
                            <span class="group-icon">
                                    <i class="fi fi-expand"></i>
                                    <i class="fi fi-shrink"></i>
                            </span>
                    </a>
            </div>
	<!-- Fin Boton -->

						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


  <div class="modal-body">
              <!-- panel content -->
              <form name="frmrequest" id="frmrequest" method="post" action="">
                  <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                  <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

                  <!--div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
                  <input type="submit" id="submitu" class="btn btn-success mb-3 mt-3 d-block-xs w-100-xs" name="submitu" value="Update All">
                          <a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
                            <span class="group-icon">
                              <i class="fi fi-expand"></i>
                              <i class="fi fi-shrink"></i>
                            </span>
                          </a>
                  </div-->

                  <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table-datatable table table-bordered table-hover table-striped"
                                data-lng-empty="No data available in table"
                                data-responsive="true"
                                data-header-fixed="true"
                                data-select-onclick="true"
                                data-enable-paging="true"
                                data-enable-col-sorting="true"
                                data-autofill="false"
                                data-group="false"
                                data-items-per-page="100">
                                    <thead>
                                      <tr>
                                        <th>Order</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Detail</th>                                        
                                      </tr>
                              </thead>

                              <tbody>
                              <?php
                              $i = 1;

                              while ($producs = mysqli_fetch_array($result2)) {


                                  if ($producs["gprice"] > 0) {
                                      $kp = $producs["gprice"];
                                  } else {
                                      $kp = $price["price"];
                                  }
                                  ?>


                                  <tr>
                                      <td><?php echo $producs["id"]; ?>  </td>
                                      <td><?php echo $producs["del_date"];  ?>  </td>
                                      <td><?php echo $producs["qucik_desc"]; ?> </td>
                                       <td><a href="ordersdetail.php?GETiD=<?php echo $producs["id"]; ?>" class="text-dark">Order <?php echo $id; ?> </a></td>
                                      <!--td><a href="/buyer/card_price_special_prod.php?idsub=<?php echo $producs["idsc"]."&idsize=".$producs["size"]."&id_gid=".$producs["gid"] ?>" class="btn btn-success btn-xs relative"> Product </a></td-->
                                  </tr>


                                  <?php $i++;
                              }
                              ?>
                              </tbody>
                              <tr>
                                        <th>Order</th>
                                        <th>Date</th>
                                        <th>Description</th>
                              </tr>
                          </table>

                      </div>
                  </div>
                  <input type="hidden" name="totalrow" value="<?php echo $i ?>"/>
              </form>




              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."' order by id desc ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>



                                                    <br>
                                                   <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>

<!--Order create modal open-->
<div class="modal fade buying_method_next" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="min-height: 300px;">
          <!-- header modal -->
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabelMd">Select Date</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="fi fi-close fs--18" aria-hidden="true"></span>
            </button>

          </div>
          <!-- body modal 3-->
            <div class="modal-body">
                <div class="col-md-12 margin-bottom-20">
                    <h4>Quick Description</h4>
                    <textarea value="2" class="form-control" id="qty_desc" name="qty_desc" ></textarea>
                </div>
                <br>
                <!-- date picker -->
                <div class="col-md-12 margin-bottom-20">
                <label>Select delivery date</label>
                <!--<?php //echo $products["prodcutid"]; ?>_<?php ///echo $i; ?>--->
                <label class="cls_date_start_date" id="cls_date">
                    <input value="" class="datepicker link-muted js-datepickified required start_date" placeholder="Select Date" type="text">
                </label>

                <br>
                </div>
                <div class="to_right">
                    <a href="" class="btn btn-3d btn-purple no_back" style="background-color:#923A8D!important;" data-dismiss="modal" aria-label="Close">Cancel</a>
                    <a href="#" class="btn btn-3d btn-purple" data-toggle="modal" data-target=".buying_method_modal" style="background-color:#923A8D!important;" data-dismiss="modal"><i class="fa fa-chevron-left"></i>Back</a>&nbsp;&nbsp;
                    <button style="background:#8a2b83!important;" onclick="send_request()" class="btn btn-primary" type="button">Create order</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!--Order create modal End-->
            
            
            
<!--Next Modal Open-->
<div class="modal fade buying_method_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="min-height: 570px;">

            <!-- header modal -->

            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabelMd">Select Shipping Option</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="fi fi-close fs--18" aria-hidden="true"></span>
              </button>

            </div>
            <!-- body modal 3-->
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover nomargin">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Company</th>
                            <th>Description</th>
                            <th>Code</th>
                            <th>Destiny</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $getShippingMethod = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "'";
                        $shippingMethodRes = mysqli_query($con, $getShippingMethod);
                        if (mysqli_num_rows($shippingMethodRes) > 0) {
                            while ($shippingMethodData = mysqli_fetch_assoc($shippingMethodRes)) {
                                //echo '<pre>';//print_r($shippingMethodData);
                                if ($shippingMethodData['shipping_method_id'] != 0) {
                                    $dt = mysqli_query($con, "select * from shipping_method where id =" . $shippingMethodData['shipping_method_id']);
                                    $sdata = mysqli_fetch_assoc($dt);
                                    ?>
                                    <tr>

                                        <td align="left">
                                            <?php
                                            if ($shippingMethodData['default_shipping'] == 1) {
                                                ?>
                                                <input type="radio" name="row_id" value="<?php echo $shippingMethodData['shipping_method_id']; ?>" checked="checked">
                                                <?php

                                            } else { ?>
                                                <input type="radio" name="row_id" value="<?php echo $shippingMethodData['shipping_method_id'] ?>">
                                            <?php }
                                            ?>
                                        </td>


                                       <!-- <td><?php echo $sdata['name']; ?></td>-->
                                        <td><?php echo $shippingMethodData['company'] ?></td>
                                        <td><?php echo $sdata['description']; ?></td>
                                        <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                        <td><?php
                                            if ($sdata['destination_country'] != 0) {
                                                $getCountry = "select *from country where id =" . $sdata['destination_country'];
                                                $countryRes = mysqli_query($con, $getCountry);
                                                $country_s = mysqli_fetch_assoc($countryRes);
                                                echo $country_s['name'];
                                            } else {
                                                echo "No defined";
                                            } ?>
                                        </td>
                                    </tr>
                                <?php } else {
                                    // echo $shippingMethodData['id'];
                                    if ($shippingMethodData['own_shipping'] == '1') {
                                        $getshipping_type = "select * from cargo_agency where id='" . $shippingMethodData['cargo_agency_id'] . "'";
                                        $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                        $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                        //echo '<pre>';print_r($shippingType);
                                        $shippingMethodDataname = $shippingType['name'];
                                    } else {
                                        $shipping_type = $shippingMethodData['choose_shipping'];
                                        if (!empty($shipping_type) && $shipping_type != '') {
                                            $getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
                                            //echo $getshipping_type;
                                            $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                            $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                            $shippingMethodDataname = $shippingType['name'];
                                        } else {
                                            $shippingMethodDataname = $shippingMethodData['name'];
                                        }
                                    }
                                    ?>
                                    <tr>

                                        <td align="center">
                                            <?php
                                            if ($shippingMethodData['default_shipping'] == 1) {
                                                ?>
                                                <input type="radio" name="row_id" value="<?php echo $shippingMethodData['id']; ?>" checked="checked">
                                                <?php

                                            } else { ?>
                                                <input type="radio" name="row_id" value="<?php echo $shippingMethodData['id'] ?>">
                                            <?php }
                                            ?>
                                        </td>
                                        <td><?php echo $shippingMethodDataname; ?></td>
                                        <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                        <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                        <td><?php
                                            if ($shippingMethodData['country'] != 0) {
                                                $getCountry = "select *from country where id =" . $shippingMethodData['country'];
                                                $countryRes = mysqli_query($con, $getCountry);
                                                $country_s = mysqli_fetch_assoc($countryRes);
                                                echo $country_s['name'];
                                            } else {
                                                echo "No defined";
                                            } ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="3">No Shipping Method Found.....</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="to_right">
                    <br>
                    <div id="erMsg" style="display:none;color:red;">Please select a shipping method.</div>
                    <a href="" class="btn btn-3d btn-purple no_back" style="background-color:#923A8D!important;" data-dismiss="modal" aria-label="Close">Cancel</a>
                    <a id="nextOpt" style="display:none" href="javascript:void(0);" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal">dfsd Jose </a>
                    <a href="javascript:void(0);" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;" onclick="checkOption();">Next <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
                </div>
            </div>
        </div>
    </div>
</div>



			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
<script tpe="text/javascript">
          function funPage(pageno) {
              document.frmfprd.startrow.value = pageno;
              document.frmfprd.submit();
          }
          function docolorchange() {
              window.location.href = '/buy.php?id=<?php echo $_GET["id"] ?>&categories=<?php echo $strdelete ?>&l=<?php echo $_GET["l"] ?>&c=' + $('#color').val();
          }
          function frmsubmite() {
              document.frmfilter.submit();
          }
          function doadd(id) {
              var check = 0;
              if ($('#qty-' + id).val() == "") {
                  $('#ermsg-' + id).html("please enter box qty.")
                  check = 1;
              }

              if (check == 0) {
                  $('#ermsg-' + id).html("");
                  $('#gid6').val(id);
                  $('#frmrequest').submit();
              }
          }
function checkOrderPrevious(){
      var orderP =  document.getElementById('selectPreviousOrder').value;
      if(orderP==0){
        document.getElementById('valueOrderId_MP').value = orderP;
        document.getElementById('orders_modal').disabled = true;
  }
      else{
        document.getElementById('valueOrderId_MP').value = orderP;
        document.getElementById('orders_modal').disabled = false;
        }
    }
          
function checkOption() {

    var myRadio = $('input[name=row_id]');
    var shippingMethod = myRadio.filter(':checked').val();
    if (myRadio.filter(':checked').length > 0) {
        $('#erMsg').hide();
        $.ajax({
            type: 'post',
            url: '/file/get_date_modal.php',
            data: 'shippingMethod=' + shippingMethod,
            success: function (data) {
                $('.cls_date_start_date').html(data);
                $('#nextOpt').click();
                _pickers();//show calendar
            }
        });

    } else {
        $('#erMsg').show();
    }
}
        function send_request() {
            var delDate = $('#cls_date').find("input").val();
            var qucik_desc = $('#qty_desc').val();
            var myRadio = $('input[name=row_id]');
            var shippingMethod = myRadio.filter(':checked').val();
            var dateRange = "";

            var flag_s = true;


            if (qucik_desc == "") {
                alert("Please enter quick description.");
                flag_s = false;
            }
            else if (delDate == "") {
                alert("Please select delivery date.");
                flag_s = false;
            }

            if (flag_s == true) {
                $.ajax({
                    type: 'post',
                    url: '/file/redirectrequest.php',
                    data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
                    success: function (data) {
                        alert("Your order was created successfully");
                        window.location.href = '/buyer/ordersnd.php';
                    },
                    error: function () {
                        alert(" ! Your order has not been created !");
                    }
                });
            }


        }          
          
</script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
        <script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
