<?php

session_start();

$userSessionID = $_SESSION["buyer"];

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini.php');

//update profile data
    if (isset($_POST['update_profile_1']) ) {

              $insert="insert into sub_client set
                                name       = '".$_POST["name"]."' ,
                                FullName   = '".$_POST["FullName"]."' ,
                                email      = '".$_POST["email"]."' ,
                                phone1     = '".$_POST["phone1"]."' ,
                                BillingAddress  = '".$_POST["BillingAddress"]."' ,
                                ShippingAddress = '".$_POST["ShippingAddress"]."' ,
                                type_order = '0' ,
                                active     = 'active' ,
                                buyer      = '318'   ";

               mysqli_query($con,$insert);

               //header("location:/buyer/customers.php");
    }

?>

<div class="d-flex flex-fill" id="wrapper_content">

<?php
      include('../back-end/inc/sidebar-menu.php');
?>


        <!-- MIDDLE -->
        <div id="middle" class="flex-fill">

            <!--PAGE TITLE 	-->
            <div class="page-title bg-transparent b-0">
                    <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                            Customers

                    </h1>
            </div>

            <!-- INVITATION -->
            <div class="row gutters-sm">

		<div class="col-12 mb-3">

                    <!-- portlet -->
                    <div class="portlet">

                            <!-- portlet : header -->
                            <div class="portlet-header border-bottom mb-3">
                                    <span class="d-block text-dark text-truncate font-weight-medium">
                                            Add Customer
                                    </span>
                            </div>
                            <!-- /portlet : header -->

                            <!-- portlet : body -->
                            <div class="portlet-body">

                                <form class="bs-validate row" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" >

                                <!--form novalidate method="post" action="/admin_user/admin_staff/" class="bs-validate row"  data-error-scroll-up="false"-->

                                        <!-- CSRF -->
                                        <input type="text" name="csrf_token" value="{: $csrf_token :}" tabindex="-1" class="form-control hide-force">
                                        <input type="hidden" name="action" value="process:invitation:add" tabindex="-1">

                                        <div class="col-12 col-md-6 mb-3">
                                            <div class="p-4 shadow-xs border rounded">
                                                <div class="row">

                                                    <h2 class="h5 mb-4 pl-4">
                                                            Customer Details
                                                    </h2>

                                                    <div class="col-12 col-sm-12 col-lg-12">

                                                            <div class="form-label-group mb-3">
                                                                    <input required="" placeholder="Short Name" id="name" name="name" type="text" class="form-control" value="<?php echo $_POST["name"] ?>">
                                                                    <label for="name">Short Name</label>
                                                            </div>

                                                            <div class="form-label-group mb-3">
                                                                    <input required="" placeholder="Full Name" id="FullName" name="FullName" type="text" class="form-control" value="<?php echo $_POST["FullName"] ?>">
                                                                    <label for="FullName">Full Name</label>
                                                            </div>

                                                    </div>
                                                    <div class="col-12 col-sm-12 col-lg-12 mb--20">

                                                            <div class="form-label-group mb-3">
                                                                    <input placeholder="Email" id="email" name="email" type="text" class="form-control" value="<?php echo $_POST["email"] ?>">
                                                                    <label for="email">Email</label>
                                                            </div>

                                                            <div class="form-label-group mb-3">
                                                                    <input placeholder="Phone" id="phone1" name="phone1" type="text" class="form-control" value="<?php echo $_POST["phone1"] ?>">
                                                                    <label for="phone1">Phone</label>
                                                            </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <!-- /email address -->
                                            <div class="p-4 shadow-xs border rounded mb-5">

                                                <div class="row">

                                                <h2 class="h5 mb-4 pl-4">
                                                        Address
                                                </h2>

                                                <div class="col-12 col-sm-12 col-lg-12 mb--20">

                                                        <div class="form-label-group mb-3">
                                                                <input  placeholder="Billing Address" id="BillingAddress" name="BillingAddress" type="text" class="form-control" value="<?php echo $_POST["BillingAddress"] ?>">
                                                                <label for="BillingAddress">Billing<small class="text-info">(optional)</small></label>
                                                        </div>

                                                        <div class="form-label-group mb-3">
                                                                <input placeholder="Shipping Address" id="ShippingAddress" name="ShippingAddress" type="text" class="form-control" value="<?php echo $_POST["ShippingAddress"] ?>">
                                                                <label for="ShippingAddress">Shipping<small class="text-info">(optional)</small></label>
                                                        </div>
                                                </div>

                                                <!--div class="col-12 col-sm-6 col-lg-6">

                                                        <div class="form-label-group mb-3">
                                                                <input placeholder="City/Town" id="shipping_city" name="shipping_city" type="text" class="form-control">
                                                                <label for="shipping_city">City/Town</label>
                                                        </div>
                                                </div-->

                                                <!--div class="col-12 col-sm-6 col-lg-6">

                                                        <div class="form-label-group mb-3">
                                                                <input placeholder="Zip / Postal Code" id="shipping_zipcode" name="shipping_zipcode" type="text" class="form-control">
                                                                <label for="shipping_zipcode">Zip / Postal Code</label>
                                                        </div>
                                                </div-->


                                        </div>
                                    </div>

                                            <input type="hidden" name="update_profile_1" value="1">
                                    <button type="submit" class="btn btn-warning mb-3 mt-3 d-block-xs w-100-xs">
                                            <i class="fi fi-plus"></i>
                                            Add Customer
                                    </button>
				</div>

				</form>

                            </div>
                            <!-- /portlet : body -->

                    </div>
                    <!-- /portlet -->

		</div>

            </div>
            <!-- /INVITATION -->

            <!-- ADMIN LIST -->
            <div class="row gutters-sm">
                    <div class="col-12">

                        <!-- portlet -->
                        <div class="portlet">

                            <!-- portlet : header -->
                            <div class="portlet-header border-bottom mb-3">
                                    <span class="d-block text-dark text-truncate font-weight-medium">
                                            Admin List <span class="font-weight-light">(3)</span>
                                    </span>
                            </div>
                            <!-- /portlet : header -->


                            <!-- portlet : body -->
                            <div class="portlet-body">

                            <!-- 	LIST START 	-->
                            <div class="table-responsive">

                                    <table class="table">

                                        <thead>
                                                <tr class="text-muted fs--13">
                                                        <th class="w--100">&nbsp;</th>
                                                        <th>ADMIN</th>
                                                        <th class="w--250">LAST LOGIN</th>
                                                        <th class="w--150">&nbsp;</th>
                                                          <th class="w--150">MarketPlace</th>
                                                </tr>
                                        </thead>

                                        <tbody id="group_list">

                            <?php
                               /*     $sql = "select sc.id, sc.name, sc.buyer ,company
                                              from sub_client sc
                                             inner join buyers b on sc.buyer = b.id
                                             where sc.buyer = '$userSessionID'
                                               and sc.id not in (0,1,47)
                                               and sc.active = 'active'
                                             order by sc.name ";*/


                                     $sql = "select sc.id, sc.name, sc.buyer ,company,
                                                   sc.FullName        ,
                                                   sc.email           ,
                                                   sc.phone1          ,
                                                   sc.BillingAddress  ,
                                                   sc.ShippingAddress
                                              from sub_client sc
                                             inner join buyers b on sc.buyer = b.id
                                             where sc.buyer = '318'
                                               and sc.id not in (0,1,47)
                                               and sc.active = 'active'
                                             order by sc.name ";



                                    $client_res = mysqli_query($con, $sql);

				while ($client = mysqli_fetch_assoc($client_res)) {


			    ?>
                                                <!-- ITEM -->
                                                <tr id="admin_user_1">
                                                        <td class="text-center">
                                                                <!-- no avatar image -->

                                                                <span data-initials="<?php  echo $client['name'];  ?>" data-assign-color="true" class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center"></span>

                                                                <!-- avatar image -->
                                                                <!--
                                                                <span class="w--50 h--50 rounded-circle d-inline-block bg-light bg-cover lazy" data-background-image="avatar.jpg"></span>
                                                                -->
                                                        </td>

                                                        <td>
                                                                <?php
                                                                echo $client['name'];
                                                                ?>




                                                                <span class="d-block text-muted fs--12">
                                                                        <a href="mailto:" class="text-muted"><?php  echo $client['email'];  ?></a>

                                                                        <!--span class="d-block text-muted">
                                                                                Joined: Sep 01, 2019
                                                                        </span-->
                                                                </span>
                                                        </td>

                                        <td><span data-toggle="modal" data-target=".orders_method_modal"><a href="javascript:selectID(<?php echo $client["id"]?>)" class="btn btn-warning mb-3 mt-3 d-block-xs w-100-xs"> <span class="fa-stack fa-sm">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-key" aria-hidden="true">
                                         </span></i> User / Pass </a></span></td>
                                        <td><a href="<?php echo SITE_URL; ?>buyer/customer_pre_invoice.php?id_cli=<?php echo $client["id"]; ?>" class="btn btn-warning mb-3 mt-3 d-block-xs w-100-xs"> Packing </a></td>
                                        <td><a target="_blank"
                                          href="<?php echo SITE_URL; ?>sub-cliente/front-end/my-orders_adm.php?idprif=<?php echo $client["id"]; ?>"
                                          class="btn btn-purple mb-1 mt-3 d-block-xs w-100-xs">Market  <i class="fi fi-cart-1"></i></a></td>
                                                                        <td></td>

                                                </tr>
                                                <!-- /ITEM -->

				<?php
                                    $i++;
                                }
                                ?>
                                </tbody>
                              </table>
                            </div>
                    </div>
                    <!-- /portlet : body -->
            </div>
            <!-- /portlet -->
        </div>
    </div>
    <!-- /ADMIN LIST -->

</div>
        <!-- /MIDDLE -->
</div><!-- FOOTER -->
<?php include('../back-end/inc/footer.php'); ?>

<script src="../back-end/assets/js/blockui.js"></script>

<script>

function cleanField(){
  document.getElementById('passId').value = '';
}
function selectID(a){
document.getElementById('valueClient_Id').value = a;

$.ajax({
    type: 'post',
    url: 'consultaCliente.php',
    data: 'CID=' + a,
    success: function (data) {
        document.getElementById('dataClientUserPass').innerHTML = data;
    },
    error: function () {
      swal("Error! Try again pleae!", {
        icon: "error",
      });
    }
});
}

function update_request(){
  var user = document.getElementById('userId').value;
  var pass = document.getElementById('passId').value;
  var clientID = document.getElementById('valueClient_Id').value;

  if (user == "") {
    swal("Error! Type your new email!", {
      icon: "error",
    });
  }else{

    if (pass == "") {
        swal("Error! Type your new password!", {
        icon: "error",
      });
    }else{

      $.ajax({
          type: 'post',
          url: 'updateDataCliente.php',
          data: 'CID=' + clientID + '&emailID=' + user + '&passID=' + pass,
          success: function (data) {

              if(data == 1){
                swal("Ok! Your data was updated successfully!", {
                icon: "success",
              });
              }

              $('#orders_method_modal').modal('hide');

          },
          error: function () {
            swal("Error! Tlease try egain!", {
            icon: "error",
          });
          }
      });
    }

  }





}
</script>

<!--Select Orders Modal Open-->
<div class="modal fade orders_method_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Update Email and Password</h4>
            </div>
            <!-- body modal 3-->
            <form action="#" method="post" id="payment-form">
            <div class="modal-body">
                <div class="table-responsive">

                  <font color="#000"><strong>Please, change your email or password.</strong></font><br><br>

                  <table id="dataClientUserPass" width="100%" border="0">

                  </table>
                      <input type="hidden" name="valueClient_Id" id="valueClient_Id" value="notvalue" />
                      <input type="hidden" name="pass_Id_hidden" id="valueClient_Id" value="notvalue" />
                      <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">The buyer can't not login to the site until you inform new password.</font></em>

                </div>



            </div>

            <div class="modal-footer request_product_modal_hide_footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                <button style="background:#8a2b83!important;" type="button" onclick="update_request();" class="btn btn-primary" class="btn btn-default btn-xs">Update</button>

            </div>
            </form>
        </div>
    </div>
</div>
<!--Select Orders Modal Open-->
<div class="modal fade orders_method_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Update Email and Password</h4>
            </div>
            <!-- body modal 3-->
            <form action="#" method="post" id="payment-form">
            <div class="modal-body">
                <div class="table-responsive">

                  <font color="#000"><strong>Please, change your email or password.</strong></font><br><br>

                  <table id="dataClientUserPass" width="100%" border="0">

                  </table>
                      <input type="hidden" name="valueClient_Id" id="valueClient_Id" value="notvalue" />
                      <input type="hidden" name="pass_Id_hidden" id="valueClient_Id" value="notvalue" />
                      <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">The buyer can't not login to the site until you inform new password.</font></em>

                </div>



            </div>

            <div class="modal-footer request_product_modal_hide_footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                <button style="background:#8a2b83!important;" type="button" onclick="update_request();" class="btn btn-primary" class="btn btn-default btn-xs">Update</button>

            </div>
            </form>
        </div>
    </div>
</div>
