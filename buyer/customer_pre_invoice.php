<?php

// PO 2018-08-24

$menuoff = 1;
$page_id = 421;
$message = 0;


$id_client  = $_GET['id_cli'];

//include("../config/config_new.php");

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$sel_cli = "select * from sub_client where id='" . $id_client . "'";
$rs_cli = mysqli_query($con, $sel_cli);
$cliente = mysqli_fetch_array($rs_cli);


$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Invoice List</h1>
        <ol class="breadcrumb">
            <li><a href="#">Customer</a></li>
            <li class="active">Pendings</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Customer Invoice </strong>                     
                </span>
                <?php echo " (".$cliente['name'].")"; ?><!-- panel title -->

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>Invoice</th>
                                <th>Subtotal</th>
                                <th>Reg.</th>                                 
                                <th>Bunches</th>                                 
                                <th>Date Invoice</th>                                                                 
                                <th>Shipping</th>                                                                                                 
                                <th>Price</th>
                                <th>St</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "select id            , id_fact          , buyer_id    , id_client       , order_number  , 
                                           order_date    , shipping_method  , date_order  , date_range      , is_pending    , 
                                           order_serial  , description      , bill_number , gross_weight    , volume_weight , 
                                           freight_value , sub_total_amount , tax_rate    , shipping_charge , handling      , 
                                           grand_total   , bill_state       , date_added  , user_added      , tot_reg       , 
                                           tot_bunches
                                      from invoice_orders_subcli 
                                     where buyer_id  = '".$userSessionID."' 
                                       and id_client = '".$id_client."'    ";
                            
							$invoices_res = mysqli_query($con, $sql);
                                                        
							while ($invoice = mysqli_fetch_assoc($invoices_res)) { 
                                                              $id_fact = $invoice['id_fact'];
                                                              $datefac = $invoice['order_date'];
                                                              $newDate = date("d-m-Y", strtotime($datefac));
							?>
							<tr>

                                                                <td><?php echo $invoice['id_fact']; ?></td>
								<td><?php echo number_format($invoice['sub_total_amount'], 2, '.', ','); ?></td>
                                                                <td><?php echo $invoice['tot_reg']; ?></td>                                                              
                                                                <td><?php echo $invoice['tot_bunches']; ?></td>                                                              
                                                                <td><?php echo $newDate; ?></td>                                                                                                                                
								<td><?php echo $invoice['gross_weight']; ?></td>                                                                
								<td><?php echo $invoice['del_date']; ?></td>
								<td><?php echo $invoice['bill_state']; ?></td> 
                                                           
                                                                <?php if ($invoice['bill_state'] == "F") { ?>
                                                                        <td>
                                                                            <a href="<?php echo SITE_URL; ?>buyer/print_invoice_client_packing.php?idi=<?php echo $id_fact."&id_cli=".$cliente["id"] ?>" class="btn btn-success btn-xs relative"> Detail </a>
                                                                        </td>                                                                
                                                                <?php }else{ ?> 
                                                                        <td>
                                                                            <a href="<?php echo SITE_URL; ?>buyer/print_invoice_client_packing.php?idi=<?php echo $id_fact."&id_cli=".$cliente["id"] ?>" class="btn btn-success btn-xs relative"> Detail </a>
                                                                        </td>  
                                                                        
                                                                        <td>
                                                                            <a href="<?php echo SITE_URL; ?>buyer/print_invoice_client_all.php?idi=<?php echo $id_fact."&id_cli=".$cliente["id"] ?>" class="btn btn-success btn-xs relative"> Print </a>
                                                                        </td>  
                                                                        <td>
                                                                            <a href="<?php echo SITE_URL; ?>buyer/upload_quick.php?idi=<?php echo $id_fact."&id_cli=".$cliente["id"] ?>" class="btn btn-success btn-xs relative"> Quickbooks     </a>
                                                                        </td>                                                                                                                                                                                                                
                                                                        <!--td> <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">Quickbooks</a></td-->                                                                                                                                        
                                                                <?php }?>
                                                                
							</tr>
							<?php
							$i++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
