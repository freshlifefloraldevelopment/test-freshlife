<?php
session_start();

$menuoff = 1;
$page_id = 421;
$message = 0;

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');

$userSessionID = $_SESSION["buyer"];

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}


/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$page_request = "buyer_invoices";

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 01 Jul 2021
Structure MarketPlace previous to buy
**/


// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

include('../back-end/inc/header_ini.php');
?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>

<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Orders List </strong>
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->



						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


  <div class="modal-body">
              <!-- panel content -->
              <form name="frmrequest" id="frmrequest" method="post" action="">
                  <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                  <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

                  <div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">

                    <span class="title elipsis">
                        <strong>Buyer Orders</strong> <!-- panel title -->
                    </span>

                    <!-- fullscreen -->
                          <a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
                            <span class="group-icon">
                              <i class="fi fi-expand"></i>
                              <i class="fi fi-shrink"></i>
                            </span>
                          </a>
                  </div>

                  <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table-datatable table table-bordered table-hover table-striped"
                                data-lng-empty="No data available in table"
                                data-responsive="true"
                                data-header-fixed="true"
                                data-select-onclick="true"
                                data-enable-paging="true"
                                data-enable-col-sorting="false"
                                data-autofill="false"
                                data-group="false"
                                data-items-per-page="10">
                                    <thead>
                                      <tr>
                                        <th>Invoice</th>
                                        <th>Order</th>
                                        <th>Buyer</th>                                        
                                        <th>Delivery</th>
                                        <th> </th>
                                      </tr>
                              </thead>

                              <tbody>
                                <?php
                                 $sql = "select bo.id as id_fact   , bo.buyer_id    , bo.order_number, bo.order_date     , bo.shipping_method, bo.del_date, 
                                                bo.date_range, bo.order_serial, bo.seen        , bo.delivery_dates , bo.lfd_grower     , 
                                                bo.qucik_desc as descrip      , b.first_name   , b.last_name       , bo.join_order
                                           from buyer_orders bo
                                          inner join buyers b  ON bo.buyer_id = b.id          
                                          where bo.buyer_id = '".$userSessionID."'
                                            and bo.del_date > date_add(curdate(), INTERVAL -60 DAY) 
                                          order by bo.id desc";

    					$invoices_res = mysqli_query($con, $sql);

		while ($invoice = mysqli_fetch_assoc($invoices_res)) {
                                            $id_fact = $invoice['id_fact'];
    							?>
			<tr>
                                    <td><?php echo $invoice['id_fact']; ?></td>
                                    <td><?php echo $invoice['descrip']; ?></td>                                    
                                    <td><?php echo $invoice['first_name']." ".$invoice['last_name']; ?></td>
                                    <td><?php echo $invoice['del_date']; ?></td>

                                    <!--td><a href="/buyer/grower_offer_check_gen.php?id_fact=<?php echo $id_fact."&id_buy=".$userSessionID; ?>" class="btn btn-success btn-xs relative"> Offers </a></td>                  
                                    <td><button style="background:#8a2b83;" onclick="reverasign('<?php echo $id_fact; ?>','<?php  echo $userSessionID; ?>')" class="btn btn-primary" type="button">DelOffer</button></td>                                     
                                    <td><a href="/buyer/list_document_embarque.php?id_fact=<?php echo $id_fact."&id_buy=".$userSessionID; ?>" class="btn btn-success btn-xs relative"> Documents </a></td-->                                                      
                                    
                                    <!--td><button style="background:#8a2b83;" onclick="closeReq('<?php echo $id_fact; ?>','<?php  echo $userSessionID; ?>')" class="btn btn-primary" type="button">End Request</button></td-->                                                                                                                                                    

                                                                <td>
                                                                        <div class="clearfix"
                                                                                    <!-- using .dropdown, autowidth not working -->
                                                                                    <a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-light js-stoppropag" data-toggle="dropdown" href="#"><span class="group-icon"><i class=
                                                                                    "fi fi-dots-vertical-full"></i> <i class="fi fi-close"></i></span> </a>
                                                                                    <div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-250">
                                                                                            <div class="scrollable-vertical max-h-50vh">
                                                                                                <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="/buyer/grower_offer_check_gen.php?id_fact=<?php echo $id_fact."&id_buy=".$userSessionID; ?>" > Offers </a>                                                                                                
                                                                                                <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" onclick="reverasign('<?php echo $id_fact; ?>','<?php  echo $userSessionID; ?>')"> DelOffer</a>   
                                                                                                <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="/buyer/list_document_embarque.php?id_fact=<?php echo $id_fact."&id_buy=".$userSessionID; ?>" > Documents </a>                                                                                                
                                                                                                
                                                                                                    <div class="dropdown-divider"></div>
                                                                                                    <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" onclick="cleartime()"> Clear</a>
                                                                                            </div>
                                                                                    </div>
                                                                        </div><!-- /SELECTED ITEMS -->                                                                    
                                                                </td>                                                                                    
                                    
			</tr>
                                            <?php
                                            $i++;
                        }
                                ?>
                              </tbody>
                              <tr>
                                        <th>Invoice</th>
                                        <th>Order</th>
                                        <th>Buyer</th>                                        
                                        <th>Delivery</th>
                                        <th> </th>
                              </tr>
                          </table>

                      </div>
                  </div>
                  <input type="hidden" name="totalrow" value="<?php echo $i ?>"/>
              </form>




              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>



                                                    <br>
                                                   <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>




			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>

			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>

<script>
/*Button send DELETE */
    function reverasign(fid,bid) {                                  
                       
        var flag_s = true; 
                     
        var factura      = fid;                     
        var buyer = bid;                                              
                                                     
    if (flag_s == true) {           
        
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/reversar_offer_gen.php',
            data: 'factura=' + factura +
                   '&buyer=' + buyer,       
            success: function (data_s) {
                
                if (data_s == 'true') {
                       alert('OK');
                } else {
                    //alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    }  
    
function cleartime(){
                   
    location.reload();
    
}    
</script>    