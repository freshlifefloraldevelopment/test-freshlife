
<?php
require_once("../config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}


if (isset($_GET["idOrder"]) && $_GET["idOrder"] != "") {
   $idOrden = $_GET["idOrder"];
}else{
	$idOrden=0;
}

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}

if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}

$userSessionID = $_SESSION["buyer"];

$shippingMethod = $_SESSION['shippingMethod'];

/*********get the data of session user****************/
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {

    $stmt->bind_param('i', $userSessionID);

    $stmt->execute();

    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);

    $stmt->fetch();

    $stmt->close();


    if (empty($userID)) {

        /*********If not exist send to home page****************/

        header("location:" . SITE_URL);
        die;

    }


} else {

    /*********If not statement send to home page****************/

    header("location:" . SITE_URL);

    die;


}

$img_url = '/images/profile_images/noavatar.jpg';

if ($profile_image) {


    $img_url = '/images/profile_images' . $profile_image;


}

$page_request = "inventory";
?>

<?php
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_buyer.php";
include '/pagination/pagination.php';
?>


<style>
    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__rendered, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px !important;
        line-height: 36px !important;
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    td {
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    .modal_k {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('images/ajax-loader.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

    body.loading .modal_k {
        display: block;
    }

    .pagination > li {
        float: left;
    }
</style>


<style>

    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/

    .select2-container--default .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered,
    .select2-container--default .select2-selection--single .select2-selection__arrow {

        height: 40px !important;

        line-height: 36px !important;

        font-size: 14px !important;

        font-family: 'Open Sans', Arial, sans-serif !important;

    }"

                                            Please select your order
                                            
                                            "

    body.loading {

        overflow: hidden;

    }

    .pagination > li {

        float: left;

    }


</style>


<style>

    #middle div.panel-heading {

        height: auto;

    }

</style>

<?php

$dates = date("jS F Y");

?>


<section id="middle">


    <!-- page title -->


    <header id="page-header">

        <h1>Bootstrap Tables</h1>

        <ol class="breadcrumb">

            <li><a href="#">Tables</a></li>

            <li class="active">Bootstrap Tables</li>

        </ol>

    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <!-- LEFT -->
                    <div class="col-md-12">

                        <div id="content" class="padding-20">

                            <div id="panel-2" class="panel panel-default">

                                <div class="panel-heading">
                                    <span class="title elipsis">Select Order </span>
                                    <br>
                                </div>

                                <!-- panel content -->

                                <div class="panel-body">
                                    <div class="row">



                                        <div class="form-group col-md-4 col-md-offset-3" >

                                            <form name="forma" id="forma" action="inventory.php" method="post" onsubmit="return verify();">

                                            Please select your order
                                            
                                            <br>
                                            <br>
											 <?php if ($idOrden==0) { ?>
                                            <select  onchange="orderChange()"style="width: 100%; display: none;" name="filter_order" id="filter_order" class="form-control select2 cls_filter" tabindex="-1">
                                                <option value="">Select Order</option>
                                                <?php
                                                $subcategory_sql = "select  id, qucik_desc  from  buyer_orders where  buyer_id='".$userSessionID."' and del_date >= '" . date('Y-m-d') . "' and is_pending=0";
												
												
                                                $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                    <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['qucik_desc']; ?></option>
                                                <?php }
                                                ?>
                                            </select>
											 <?php }
                                                ?>
												
												
												<?php if ($idOrden>0) { ?>
                                            <select  onchange="orderChange()"style="width: 100%; display: none;" name="filter_order" id="filter_order" class="form-control select2 cls_filter" tabindex="-1">
                                                <?php
                                                $subcategory_sql = "select  id, qucik_desc  from  buyer_orders where id=".$idOrden." and  buyer_id='".$userSessionID."' and del_date >= '" . date('Y-m-d') . "' and is_pending=0";
												
												echo $subcategory_sql;
                                                $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                    <option selected="selected"  value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['qucik_desc']; ?></option>
                                                <?php }
                                                ?>
                                            </select>
											 <?php }
                                                ?>
                                            <br><br>
                                            <input type="hidden" name="order" id="order" value="">
                                            <div class="col-md-10 col-md-offset-3"></div>
                                            <br>
                                            <br>

                                            <div class="col-md-2 col-md-offset-3">
                                                <button type="submit" class="btn btn-3d btn-purple" style="background-color:#06B120!important;">Inventory <i class="fa fa-chevron-right"></i></button>
                                            </div>


                                            </form>

                                        </div>
                                        <div class="col-md-4 col-md-offset-3">

                                        </div>
                                        <div class="col-md-3 col-md-offset-3">
                                            <br>
                                            <br>
                                           <small >Fresh life floral  Ecuador  2017</small>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">


    function  verify() {
        //alert ("hola");

        //var order_val = $('#filter_order :selected').val();
        $("#order").val($("#filter_order option:selected").html());

        var order_val = $('#filter_order :selected').val();

        //alert($('#filter_order :selected').val());
        if (order_val==""){

            alert ("Please choose an option to proceed");
            return false;
        }
        else{
            return true;
        }

    }

    function orderChange(product_id, sizename, i) {

        var order_val = $('#filter_order :selected').val();
        //verificar  la  order_val=="";  hay esta  vacio
        console.log(order_val);


    }

</script>


<?php include("../includes/footer_new.php"); ?>
