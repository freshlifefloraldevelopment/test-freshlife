<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idfac = $_GET['fac_id'];
    $clienteid = $_GET['id_cli'];
    
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id              , buyer_id        , order_number    , order_date      , shipping_method ,
                         del_date        , date_range      , is_pending      , order_serial    , seen            ,
                         delivery_dates  , lfd_grower      , qucik_desc      , type_market     , delivery_day    ,
                         availability    
                    from buyer_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id       = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select  sc.name as namecli , num_box as xcli
                   from reser_requests br
                  inner JOIN buyer_orders bo ON br.id_order = bo.id
                  inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
                  where br.buyer = '" . $userSessionID . "' 
                    and br.comment   like 'SubClient%'                                            
                    and bo.assigned = 1 
                  group by sc.name,br.num_box
                  order by sc.name,br.num_box      ";

        $result   = mysqli_query($con, $sqlDetalis);    
        
        //                    and ir.id_client = '" . $clienteid . "'
        
     $sqlcount=" select br.num_box        
                   from buyer_requests br
                  inner join grower_offer_reply gor on gor.offer_id = br.id
                  inner join buyer_orders bo ON br.id_order = bo.id
                  inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                  inner join growers g on gor.grower_id = g.id
                  inner join colors c on p.color_id = c.id
                  inner JOIN sub_client cli on br.id_client = cli.id 
                   left join features f on br.feature = f.id 
                   left join buyer_requests res ON gor.request_id = res.id
                  where g.active = 'active' 
                    and br.buyer = '" . $userSessionID . "' 
                    and bo.assigned = 1     
                    and (gor.bunchqty-gor.reserve) > 0   
                    and (res.id_client = 0 or res.id_client is null)  
                   group by num_box";                

        $num       = mysqli_query($con, $sqlcount);           
                
        $total_box = mysqli_num_rows($num);
        
        

    $pdf = new PDF();
    //$pdf->AliasPages();   
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);  
    

        $pdf->Cell(70,10,'DELIVERY ORDER',0,0,'L'); 

    
    $pdf->Ln(10);    
    
            
    $pdf->SetFont('Arial','B',10);
    
    $pdf->Cell(30,6,'Order #: '.$buyerOrderCab['id'],0,1,'R');             

    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');            
    $pdf->Cell(70,6,'Company Name : '.$buy['company'],0,1,'L');  

    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'CUSTOMER',0,0,'L');
    $pdf->Cell(25,6,'BOXES',0,1,'C');
    $pdf->Cell(70,6,'__________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = '0';
    $tmp_caja    = 0;
    
    while($row = mysqli_fetch_assoc($result))  {                        
            
               $contar = $contar + 1; 

               $pdf->SetFont('Arial','',8);
         
               $conbox = $conbox + 1;
         
          if ($row['namecli'] != $tmp_idorder) {

                       if ($tmp_idorder != '0') {                                                  
                           $pdf->Cell(70,4,$tmp_idorder,0,0,'L');            
                           $pdf->Cell(25,6,$conbox,0,1,'C');
                       }
                           $conbox = 0;                                              
          } 
  
           $tmp_idorder = $row['namecli'];
           $tmp_caja = $row['xcli'];
    }
    $conbox = $conbox + 1;
                           $pdf->Cell(70,4,$tmp_idorder,0,0,'L');            
                           $pdf->Cell(25,6,$conbox,0,1,'C');
                          
    $pdf->Ln(2);
            
            
            $pdf->SetFont('Arial','B',10);            
            
            $pdf->Cell(35,6,'Total Boxes :'.$contar,0,1,'R'); 

$pdf->Ln(5);    

            $pdf->Cell(70,4,'INVENTORY',0,0,'L');            
            $pdf->Cell(25,6,$total_box,0,1,'C');
            
    
    
    
  $pdf->Output();
  ?>