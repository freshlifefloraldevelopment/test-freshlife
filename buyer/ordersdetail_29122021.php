<?php
session_start();

$userSessionID = $_SESSION["buyer"];
if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 16 Jun 2021
Structure MarketPlace previous to buy
**/
// start a session

$idR = $_GET["GETiD"];




// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini.php');



  $selOrderNum= "select gpb.id as cartid,
p.image_path as img,
s.name as subs,
p.name as namep,
ff.name as featurename,
sh.name as sizename,
gpb.qty as st_request,
gpb.lfd as date_lfd,
gp.stem_bunch id_stbu,
bs.name stem_bunch_name,
gpb.type as order_type,
gpb.price_front,
gpb.id_grower,
gpb.unit,
gpb.id_status,
gpb.countdown,
p.id ,p.subcategoryid,p.color_id
from buyer_requests gpb
left join product p on gpb.product = p.id
left join subcategory s on p.subcategoryid=s.id
left join features ff on gpb.feature=ff.id
left join sizes sh on gpb.sizeid=sh.id
left join grower_parameter gp on (gp.idsc=s.id and gpb.sizeid=gp.size)
left join bunch_sizes  bs on gp.stem_bunch = bs.id
where gpb.buyer  = '$userSessionID'
and gpb.id_order = '$idR'
and gpb.id_status = '2'
group by gpb.id
order by gpb.id desc";
$numero_filas = mysqli_num_rows(mysqli_query($con,$selOrderNum));

?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->
<style>

.color-not-available {
  background: #b02a37 !important;
}
.color-not-available:hover, .color-not-available:active {
  background: #e35d6a !important;
}

.color-complete {
  background: #6dbb30 !important;
}
.color-complete:hover, .color-complete:active {
  background: #95d661 !important;
}

.color-approve{
  background: #6d44c7 !important;
}
.color-approve:hover, .color-approve:active {
  background: #4c2c92 !important;
}


.color-approve-change {
  background: #495057 !important;
}
.color-approve-change:hover, .color-approve-change:active {
  background: #6c757d !important;
}

.color-inprocess {
  background: #fd7e14 !important;
}
.color-inprocess:hover, .color-inprocess:active {
  background: #feb272 !important;
}

.countdown-label {
	color: #fff;
	text-align: center;
	text-transform: uppercase;
  margin-top: 1px
}
#countdown{
box-shadow: 0 1px 2px 0 rgba(1, 1, 1, 0.4);
	height: 30px;
	text-align: center;
background: #495057;

	border-radius: 5px;

	margin: auto;

}

#countdown #tiles{
  color: #fff;
	position: relative;
	z-index: 1;
text-shadow: 1px 1px 0px #ccc;
	display: inline-block;
  font-family: Arial, sans-serif;
	text-align: center;

  padding: 5px;
  border-radius: 5px 5px 0 0;
  font-size: 14px;
  font-weight: thin;
  display: block;

}

.color-full {
  background: #4c2c92;
}
.color-half {
  background: #4c2c92;
}
.color-empty {
  background: #e5554e;
}

#countdown #tiles > span{
	width: 60px;
	max-width: 70px;

	padding: 18px 0;
	position: relative;
}





#countdown .labels{
	width: 70%;
	height: 25px;
	text-align: center;
	position: absolute;
	bottom: 8px;
}

#countdown .labels li{
	width: 102px;
	font: bold 15px 'Droid Sans', Arial, sans-serif;
	color: #f47321;
	text-shadow: 1px 1px 0px #000;
	text-align: center;
	text-transform: uppercase;
	display: inline-block;
}

.tooltip-inner {
    max-width: 400px;
    /* If max-width does not work, try using width instead */
    width: 400px;
    font-size: 12px;
}
</style>

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Orders Detail  # <?php echo $idR; ?> </strong>

						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">

							Orders Detail # <?php echo $idR; ?>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


              <div class="modal-body">
<table class="table-datatable table table-bordered table-hover table-striped"
                        data-lng-empty="No data available in table"
                        data-responsive="true"
                        data-header-fixed="true"
                        data-select-onclick="true"
                        data-enable-paging="true"
                        data-enable-col-sorting="true"
                        data-autofill="false"
                        data-group="false"
                        data-defer-render="true"
                        data-state-save="true"
                        data-items-per-page="50">
                        <thead>
                          <tr>
                            <th>Img</th>
                            <th>Request</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Farm</th>
                            <th>Order Type</th>
                            <th>Option</th>


                          </tr>
                        </thead>
                        <tbody>

                          <?php

                         $sel_order= "select gpb.id as cartid,
                          p.image_path as img,
                          s.name as subs,
                          p.name as namep,
                          ff.name as featurename,
                          sh.name as sizename,
                          gpb.qty as st_request,
                          gpb.lfd as date_lfd,
                          gp.stem_bunch id_stbu,
                          bs.name stem_bunch_name,
                          gpb.type as order_type,
                          gpb.price_front,
                          gpb.id_grower,
                          gpb.unit,
                          gpb.id_status,
                          gpb.countdown,
                          p.id ,p.subcategoryid,p.color_id
                          from buyer_requests gpb
                          left join product p on gpb.product = p.id
                          left join subcategory s on p.subcategoryid=s.id
                          left join features ff on gpb.feature=ff.id
                          left join sizes sh on gpb.sizeid=sh.id
                          left join grower_parameter gp on (gp.idsc=s.id and gpb.sizeid=gp.size)
                          left join bunch_sizes  bs on gp.stem_bunch = bs.id
                          where gpb.buyer  = '$userSessionID'
                          and gpb.id_order = '$idR'
                          group by gpb.id
                          order by gpb.id asc";

                                /*   $sel_order= "select gpb.id as cartid,
                                  p.image_path as img,
                                  s.name as subs,
                                  p.name as namep,
                                  ff.name as featurename,
                                  sh.name as sizename,
                                  gpb.qty as st_request,
                                  gpb.lfd as date_lfd,
                                  cod_order as ponumber
                                  from buyer_requests gpb
                                  left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                  left join subcategory s on p.subcategoryid=s.id
                                  left join features ff on gpb.feature=ff.id
                                  left join sizes sh on gpb.sizeid=sh.id
                                  where gpb.buyer = '$userSessionID'
                                  and gpb.id_order = '$idR'
                                  and gpb.lfd >='" . date("Y-m-d") . "'
                                  order by gpb.id desc";
                                      */
                                /*  $sel_order= "select gpb.id as cartid,
                                    p.image_path as img,
                                    s.name as subs,
                                    p.name as namep,
                                    ff.name as featurename,
                                    sh.name as sizename,
                                    gpb.qty as st_request,
                                    gpb.lfd as date_lfd,
                                    cod_order as ponumber
                                    from buyer_requests gpb
                                    left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                    left join subcategory s on p.subcategoryid=s.id
                                    left join features ff on gpb.feature=ff.id
                                    left join sizes sh on gpb.sizeid=sh.id
                                    where gpb.buyer = '318'
                                    and gpb.id_order = '1295'
                                    order by gpb.id desc";*/
                                    $i = 0;
                                  $rs_order=mysqli_query($con,$sel_order);
                                  while($orderCab=mysqli_fetch_array($rs_order))
                                  {

                              //   $_SESSION[$cartid] = $orderCab['cartid'];
                                $cartid = $orderCab['cartid'];
                                $img = $orderCab['img'];
                                $subs = $orderCab['subs'];
                                $namep = $orderCab['namep'];
                                $featurename= $orderCab['featurename'];
                                $sizename = $orderCab['sizename'];
                                $st_request = $orderCab['st_request'];
                                $date_lfd = $orderCab['date_lfd'];
                                $cod_order = $orderCab['ponumber'];
                                $unit = $orderCab['unit'];
                                $status = $orderCab['id_status'];
                                $price_front = $orderCab['price_front'];
                                $id_grower  = $orderCab['id_grower'];
                                $order_type = $orderCab['order_type'];
                                $st_x_bunches = $orderCab['stem_bunch_name']."st/bu";
                                $timeEnd = $orderCab['countdown'];

                                $stillNeeded = 0;

                                /*$sel_orderCountPrevious = "select br.type tipoReq,
                                g.file_path5 as img ,
                                p.name as variedad,
                                sz.name as size_name_request,
                                br.price_front as priceRequest,
                                br.qty as cantRequest,
                                p.image_path as fotoRequest,
                                g.growers_name  as  growerRequest,
                                gor.type_market as tipoOfer,
                                gor.product_subcategory as subcat_offer,
                                gor.product as variedadOffer       ,
                                gor.size   as sizeOffer       ,
                                gor.steams as steamsOffer        ,
                                gor.price as preciOfer  ,
                                gor.bunchqty*gor.steams as Stems_offered   ,
                                gor.bunchqty  as cantofer     ,
                                gor.grower_id as growerOffer  ,
                                gor.countdown   ,
                                br.id_status as status_request,
                                (select image_path from product where name = gor.product and subcate_name = gor.product_subcategory group by image_path) as fotoOffer,
                                f.name as featurename,
                                (select bs.name from grower_parameter gp inner JOIN bunch_sizes bs ON bs.id = gp.stem_bunch where gp.idsc = p.subcategoryid and gp.size = sz.id group by bs.name) as stemsRequest,
                                gor.request_id
                                from buyer_requests br
                                left JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                left JOIN product p ON br.product = p.id
                                left JOIN buyers b ON br.buyer = b.id
                                left JOIN buyer_orders bo ON br.id_order = bo.id
                                left JOIN sizes sz ON br.sizeid = sz.id
                                left JOIN growers g ON br.id_grower = g.id
                                left JOIN features f ON br.feature = f.id
                                where br.buyer  = '$userSessionID'
                                and gor.request_id = '$cartid'
                                ";

                              $valorTotalOfertPrevious  = mysqli_num_rows(mysqli_query($con,$sel_orderCountPrevious));
                              */



                                if($unit=='0' OR $unit==39)
                                {
                                  $unit = 'Stems';
                                }else {
                                  $unit = 'Bunches';
                                }

                                $rs_status   = @mysqli_query($con,"Select * from status where id='$status'");
                                $orderStatus = @mysqli_fetch_array($rs_status);

                                 $id_Status =   $orderStatus['0'];
                                // $name_Status =   $orderStatus['1'];
                                $name_Status = "Offers";
                              /*   if($id_Status==1)
                                 {
                                   $icon = '<i class="align-button float-left fa fa-info-circle" aria-hidden="true"></i>';
                                   $botonR = 'color-inprocess bg-gradient-warning text-white';
                                   $infomodal ='title="Request In process" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Please wait, we are processing your request."';
                                   $titleModel = '';
                                 }
                                 */
                              /*   if($id_Status==2)
                                 {
                                    $i = $i+1;

                                  $minutesadd = 0;
                                  date_default_timezone_set("America/Vancouver");
                                   $new_time = date("Y-m-d H:i:s", strtotime("+$minutesadd minute"));

                                   $icon = '<i class="fas fa-random m-0-md" aria-hidden="true"></i>';
                                   $botonR = 'color-approve-change bg-gradient-purple text-white';
                                   $botonRM = 'color-approve-change text-white';
                                   $titleModel = 'Approve change';
                                   $modalTitle = 'badge badge-purple';
                                   $botonProcess = "<button type='button' onclick='sendApprove($valorTotalOfertPrevious)' class='btn btn-success' data-dismiss='modal'><i class='fi fi-close'></i> Process Request</button>";

                                      $dateR = date('Y-m-d H:i:s');
                                    // $new_time = date("Y-m-d H:i:s", strtotime('+5 minute'));
                                    $timeEnd;
                                     $to_time = strtotime($dateR);
                                     $from_time = strtotime($timeEnd);

                                      $valueMinutes = round(($from_time - $to_time) / 60,0);
                                      $porciones = explode(".", $valueMinutes);

                                    //  echo $porciones[0];
                                    $infomodal = "data-toggle='modal' data-target='#billing_modal-$cartid-$st_request'";

                                 }*/

                              //   if($id_Status==3)
                              //   {
                                   $icon = '<i class="align-button float-left fa fa-pagelines "></i>';
                                   $botonR = 'color-approve bg-gradient-purple text-white';
                                   $botonRM = 'color-approve text-white';
                                   $infomodal = "data-toggle='modal' data-target='#billing_modal-$cartid-$st_request'";
                                   $titleModel = 'Offers';
                                   $modalTitle = 'badge badge-purple';
                              //   }

                              /*   if($id_Status==4)
                                 {
                                   $icon = '<i class="align-button float-left fa fa-ban" aria-hidden="true"></i>';
                                   $botonR = 'color-not-available bg-gradient-danger text-white';
                                   $infomodal ='title="Request Not available" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Unfortunately we could not found a fit offert."';
                                   $titleModel = '';
                                 }
                                 */


                                 if($id_grower!=100000 && $id_grower!=200000 && $id_grower!=0){
                                   $rs_Grower   = @mysqli_query($con,"Select * from growers where id='$id_grower'");
                                   $orderGrower = @mysqli_fetch_array($rs_Grower);

                                     $idGrowerR =   $orderGrower['0'];
                                    $nameGrowerR =   $orderGrower['1'];

                                    $nameGrowerR = "<span class='text-primary fs--14'><span class='text-primary'>*** </span><strong>$nameGrowerR</strong></span>";

                                  }else {

                                     $id_grower;

                                              if($id_grower==100000){
                                                  $nameGrowerR = '<span class="text-primary fs--14"><span class="text-danger">*** </span><strong>Best deal</strong></span>';
                                              }

                                              if($id_grower==200000){
                                                  $nameGrowerR = '<span class="text-primary fs--14"><span class="text-danger">*** </span><strong>Best deal</strong></span>';
                                              }

                                              if($id_grower==0){
                                                  $nameGrowerR = '<span class="text-danger fs--12"><strong>No Grower</strong></span>';
                                              }
                                  }




                                  if($order_type==0){
                                    $orderName = 'Standing Order';
                                  }else
                                  {
                                    $orderName = 'Open Market';
                                  }


                                $sel_order1="select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                                from grower_offer_reply gr
                                left join growers g on gr.grower_id=g.id
                                left join country cr on g.country_id=cr.id
                                where gr.request_id='$cartid'
                                and gr.buyer_id='$userSessionID'
                                order by gr.date_added desc";


                                /*
                                $sel_order1="select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                              from grower_offer_reply gr
                              left join growers g on gr.grower_id=g.id
                              left join country cr on g.country_id=cr.id
                              where gr.request_id='50195'
                              and gr.buyer_id='318'
                              order by gr.date_added desc";
                              */

                              $rs_order1=mysqli_query($con,$sel_order1);
                              $rs_order1NumR=mysqli_num_rows($rs_order1);
                              while($orderCab1=mysqli_fetch_array($rs_order1))  {
                                $stillNeeded += $orderCab1['Stems_offered'];
                              }


                          ?>

                          <tr>
                            <td>

                              <span class="fs--15">
                              <figure style="border-radius: 50%; height: 53px; width: 53px;" class="d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
        												<img style="border-radius: 50%; height: 50px; width: 50px;"  src="/<?php echo $img; ?>" alt="<?php echo $namep; ?>">
        	                     </figure>

                            </span></td>
                            <td><span class="fs--15"><?php echo $subs." <br> ".$namep." <br> ".$sizename." [cm]"." ".$st_x_bunches; ?></span>
                              <br>
                              <?php if($featurename!=''){?>
                                <sup>
                                <span class="text-primary fs--13">
                               <span class="text-danger">*** </span><strong>Feature: <?php echo $featurename; ?></strong>
                              </span>

                              </sup>
                              <?php }?>
                            </td>
                            <td align="left"><span class="fs--15">
                              <?php echo $st_request." [".$unit."]";  ?></span>
                            </td>
                            <td align="left">
                              <span class="fs--15">
                                <span class="d-block fs--15">Price: <span class="text-primary  fs--15"><?php echo $price_front; ?></span> <strong> <sup class="text-primary fs--10">USD</sup></strong></span>

                            </span></td>
                            <td><span class="fs--15"><?php echo $nameGrowerR; ?></span></td>
                            <td><span class="fs--15"><?php echo $orderName; ?></span></td>
                            <td>


                              <button <?php
                              $cadenaItemsData = '';
                          $sel_orderCount = "select DISTINCT
                                                    gor.id_group_cab, cof.date as date_cab_offer, cof.time as time, cof.status as statusCab
                                                    from buyer_requests br
                                                    inner JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                                    inner JOIN grower_offer_reply_cab cof ON (gor.offer_id = cof.request_id and cof.id=gor.id_group_cab)
                                               where gor.buyer_id  = '$userSessionID'
                                                 and gor.request_id = '$cartid'   ";
                                $arrayCab = mysqli_query($con,$sel_orderCount);
                                while($orderCabeceraDataItems=mysqli_fetch_array($arrayCab))
                                {
                                  $cadenaItemsData .= ','.$orderCabeceraDataItems['id_group_cab'];
                                }

                               $valorTotalOfert  = mysqli_num_rows(mysqli_query($con,$sel_orderCount));

                               if($cadenaItemsData!=''){
                               $delete1st = substr($cadenaItemsData, 1);
                               $contadorOffers = explode(',',$delete1st);
                               }else{
                                 $contadorOffers = 0;
                               }

                              echo $infomodal; ?> onclick="checkCloclk('<?php echo $valorTotalOfert; ?>','<?php echo $cadenaItemsData; ?>');"  class="btn btn-sm btn-block b-0 w--200 <?php echo $botonR; ?> b-0">
                                                    <span class="p-0-xs">

                                                      <?php  echo $icon;?>

                                                      <span class="text-uppercase fs--14">

                                                        <?php echo $name_Status;
                                                        if($contadorOffers!=0){
                                                        ?>
                                                        <span class="badge badge-success fs--16 float-right"><?php echo count($contadorOffers); ?></span>
                                                        <!-- <sup class="badge badge-danger position-absolute end-0 mt--n5 text-warning fs--14"><?php echo $rs_order1NumR; ?></sup> -->
                                                      <?php }else{ ?>
                                                        <span class="badge badge-warning fs--12 float-right"><?php echo $contadorOffers; ?></span>
                                                        <?php } ?>
                                                      </span>
                                                    </span>
                                                  </button>

                                <?php
                              //echo  $sel_orderCount;
                              //  if($id_Status==2)
                              //  {
                                ?>
                              <!--  <p>
                                <input type="hidden" id="set-time<?php echo $i; ?>" value="<?php echo $porciones[0];?>"/>
                                <div id="countdown" class="b-0 w--200 float-left">

                                <div id='tiles' class="color-full b-0 w--200"></div>
                                <div class="countdown-label b-0 w--200 ">Time Remaining</div>
                              </div> -->
                                <?php
                              //  }
                                ?>


                                                                                                          <!-- Billing Modal -->
                                                                                                          <div class="modal fade" name="billing_modal-<?php echo $cartid;?>-<?php echo $st_request; ?>"  id="billing_modal-<?php echo $cartid;?>-<?php echo $st_request; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                                                                                                          		<div class="modal-dialog modal-md modal-xl" role="document">
                                                                                                          				<div class="modal-content">

                                                                                                          						<!-- Header -->
                                                                                                          						<div class="modal-header <?php echo $botonRM; ?> ">
                                                                                                          								<h5 class="modal-title" id="exampleModalLabelMd"><?php echo $titleModel; ?>
                                                                                                                            </h5>

                                                                                                          						</div>

                                                                                                          						<!-- Content -->


                                                                                                          						<div class="modal-body">
                                                                                                                          <div class="container">
                                                                                                                            <h1 class="display-6">  &nbsp; Offers <span class="badge badge-purple fs--28 float-left"> <?php echo count($contadorOffers); ?> </span>

                                                                                                                              <button type="button" class="btn btn-danger float-right" onclick="destro('<?php echo $cartid;?>','<?php echo $st_request; ?>')">
                                                                                                                                  Close X
                                                                                                                              </button>
                                                                                                                              </h1>
                                                                                                                              <p class="lead">Grower's offers.

                                                                                                                            </p>
                                                                                                                          </div>


                                                                                                                        <!--<div class="container">
                                                                                                                            <div class="row">
                                                                                                                              <div class="col-5">
                                                                                                                                <h5>Request: <span class="<?php echo $modalTitle; ?>"><?php echo $st_request." Boxes ".$subs." ".$namep." ".$featurename." ".$sizename." [cm]" ?></span></h5>
                                                                                                                              </div>
                                                                                                                              <div class="col-7">
                                                                                                                                <img class="float-left" alt="<?php echo $st_request." Boxes ".$subs." ".$namep." ".$featurename." ".$sizename." [cm]" ?>" src="/product-image/big/031921185209_crop.jpg" width="55">

                                                                                                                              </div>
                                                                                                                            </div>
                                                                                                                          </div> -->



                                                                                                                        <hr class="my-4">

                                                                                                                        <?php
                                                                                                                        if($contadorOffers==0){
                                                                                                                        ?>
                                                                                                                        <div class="jumbotron">
                                                                                                                          <h1 class="display-4">Waiting offers!</h1>
                                                                                                                          <p class="lead"><i class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></i> Please, come back later on. We are finding the best offer.</p>


                                                                                                                        </div>
                                                                                                                        <?php
                                                                                                                        }
                                                                                                                        ?>

                                                                                                                          <input type="hidden" value="<?php echo $valorTotalOfert; ?>" id="totalOffert" name="totalOffert" />
                    <?php
                      $selDataCab = "select DISTINCT
                                            gor.id_group_cab, cof.date as date_cab_offer, cof.time as time, cof.status as statusCab
                                       from buyer_requests br
                                            inner JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                            inner JOIN grower_offer_reply_cab cof ON (cof.id=gor.id_group_cab and gor.offer_id = cof.request_id )
                                      where gor.buyer_id   = '$userSessionID'
                                        and gor.request_id = '$cartid'";

                        $rs_orderCabecera=mysqli_query($con,$selDataCab);
                        $iii = 0;
                        $$flagCab=0;
                        while($orderCabeceraData=mysqli_fetch_array($rs_orderCabecera))
                        {

                                                                                                                               $timeEndCab = $orderCabeceraData['date_cab_offer'];
                                                                                                                               $timeAddCab = $orderCabeceraData['time'];
                                                                                                                               $statusCab = $orderCabeceraData['statusCab'];
                                                                                                                               $id_group_cab = $orderCabeceraData['id_group_cab'];

                                                                                                                                 if($statusCab==1)
                                                                                                                                 {
                                                                                                                                   $icon_c = '<i class="align-button float-left fa fa-info-circle" aria-hidden="true"></i>';
                                                                                                                                   $botonR_c = 'color-inprocess bg-gradient-warning text-white';
                                                                                                                                   $infomodal_c ='title="Request In process" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Please wait, we are processing your request."';
                                                                                                                                   $titleModel = '';
                                                                                                                                   $titleButton = 'In process';
                                                                                                                                   $iconDetails = "<i class='fa fa-check-circle fa-2x' aria-hidden='true'></i>";
                                                                                                                                 }

                                                                                                                               if($statusCab==2)
                                                                                                                               {
                                                                                                                                   $iii++;
                                                                                                                                   $flagCab++;

                                                                                                                                 //$minutesadd = $timeAdd;
                                                                                                                                date_default_timezone_set("America/Vancouver");
                                                                                                                                // $new_time = date("Y-m-d H:i:s", strtotime("+$minutesadd minute"));

                                                                                                                                 $icon_c = '<i class="fas fa-random m-0-md" aria-hidden="true"></i>';
                                                                                                                                 $botonR_c = 'color-approve-change bg-gradient-purple text-white';
                                                                                                                                 $botonRM = 'color-approve-change text-white';
                                                                                                                                 $titleModel = 'Approve change';
                                                                                                                                 $modalTitle = 'badge badge-purple';
                                                                                                                                 $titleButton = '';



                                                                                                                                   $dateR = date('Y-m-d H:i:s');
                                                                                                                                   $to_time = strtotime($dateR);
                                                                                                                                   $from_time = strtotime($timeEndCab);

                                                                                                                                   $valueMinutes = round(($from_time - $to_time) / 60,0);
                                                                                                                                   $porcionesi = explode(".", $valueMinutes);

                                                                                                                                  //  echo $porciones[0];
                                                                                                                                  $infomodal_c = "data-toggle='modal' data-target='#billing_modal-$cartid-$st_request'";
                                                                                                                                  $titleButton = 'Pending approval';
                                                                                                                                  $iconDetails = "<i class='fa fa-clock-o fa-1x' aria-hidden='true'></i>";
                                                                                                                               }

                                                                                                                               if($statusCab==3)
                                                                                                                               {
                                                                                                                                 $icon_c = '<i class="align-button fa fa-check-circle float-left"></i>';
                                                                                                                                 $botonR_c = 'color-complete bg-gradient-success text-white';
                                                                                                                                 $botonRM = 'color-complete text-white';
                                                                                                                                 $infomodal_c = "data-toggle='modal' data-target='#billing_modal-$cartid-$st_request'";
                                                                                                                                 $titleModel = 'Offers';
                                                                                                                                 $modalTitle = 'badge badge-success';
                                                                                                                                 $titleButton = 'Complete';
                                                                                                                                 $iconDetails = "<i class='fa fa-check-circle fa-2x' aria-hidden='true'></i>";

                                                                                                                               }

                                                                                                                               if($statusCab==4)
                                                                                                                               {
                                                                                                                                 $icon_c = '<i class="align-button float-left fa fa-ban" aria-hidden="true"></i>';
                                                                                                                                 $botonR_c = 'color-not-available bg-gradient-danger text-white';
                                                                                                                                 $infomodal_c ='title="Request Not available" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Unfortunately we could not found a fit offert."';
                                                                                                                                 $titleModel = '';
                                                                                                                                 $titleButton = 'Decline';
                                                                                                                                  $iconDetails = "<i class='fa fa-exclamation-circle fa-2x' aria-hidden='true'></i>";
                                                                                                                               }


                                                                                                                          ?>




                                                                                                                            <div class="card-header text-success" >
                                                                                                                              <strong><sup>*</sup><sup>*</sup>Offer details:   <span class="text-secondary float-left"><?php echo $iconDetails; ?>&nbsp&nbsp;</strong>


                                                                                                                              <?php
                                                                                                                              if($statusCab==2)
                                                                                                                              {
                                                                                                                              ?>

                                                                                                                              <input type="hidden" id="set-time<?php echo $id_group_cab; ?>" value="<?php echo $porcionesi[0];?>"/>

                                                                                                                              <div id="countdown" class="b-0 w--200 float-right">

                                                                                                                              <div id='tiles<?php echo $id_group_cab; ?>' class="color-full b-0 w--200" style="color: #fff;
                                                                                                                              position: relative;
                                                                                                                              z-index: 1;
                                                                                                                              text-shadow: 1px 1px 0px #ccc;
                                                                                                                              display: inline-block;
                                                                                                                              font-family: Arial, sans-serif;
                                                                                                                              text-align: center;

                                                                                                                              padding: 5px;
                                                                                                                              border-radius: 5px 5px 0 0;
                                                                                                                              font-size: 18px;
                                                                                                                              font-weight: thin;
                                                                                                                              display: block;">



                                                                                                                            </div>

                                                                                                                              <br>
                                                                                                                              <?php




                                                                                                                              //echo $botonProcess;
                                                                                                                              ?>
                                                                                                                              </div>
                                                                                                                              <?php
                                                                                                                              }
                                                                                                                              ?>


                                                                                                                            </span>

                                                                                                                              <footer class="blockquote-footer text-primary"><strong><?php echo 'Offer : '; ?> <cite title="Source Title"><?php echo $titleButton; ?> &nbsp&nbsp  </cite></strong></footer>

                                                                                                                            </div>

                                                                                                                        <table class="table table-framed border">
                                                                                                          											<thead>
                                                                                                          												<tr style="background-color:#f6f6f6;">
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      REQUEST DETAILS
                                                                                                                                    </th>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      IMG
                                                                                                                                    </th>
                                                                                                          												  <th class="text-gray-500 font-weight-normal fs--14 w--500">
                                                                                                                                      PRODUCT NAME
                                                                                                                                    </th>
                                                                                                          													 <th class="text-gray-500 font-weight-normal fs--14 w--500">
                                                                                                                                      PRICE
                                                                                                                                    </th>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--400">
                                                                                                                                      FARM
                                                                                                                                    </th>
                                                                                                                                     <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      STATUS
                                                                                                                                    </th>
                                                                                                                                    <th class="text-green-500 w--50 floar-right">

                                                                                                                                    <input type="hidden" value="<?php echo $id_group_cab; ?>" id="requestIDID<?php echo $id_group_cab; ?>"/>
                                                                                                                                    <input type="hidden" value="<?php echo $flagCab; ?>" id="posicionCab" name="posicionCab" />


                                                                                                                                    <?php
                                                                                                                                    if($statusCab==2)
                                                                                                                                    {
                                                                                                                                    ?>


                                                                                                                                    <div class="clearfix"><!-- using .dropdown, autowidth not working -->

                                                                                                                            						<a href="#" class="btn btn-sm btn-light float-right rounded-circle" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                                                                                                            							<span class="group-icon text-secondary">
                                                                                                                            								<i class="fi fi-dots-vertical-full"></i>
                                                                                                                            								<i class="fi fi-close"></i>
                                                                                                                            							</span>
                                                                                                                            						</a>

                                                                                                                            						<div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-220">

                                                                                                                            							<div class="scrollable-vertical max-h-100vh">




                                                                                                                                              <div class="dropdown-item text-truncate js-ajax-confirm pt-2 pb-2 pl--8 pr--8">


                                                                                                                                                  <div class="btn-group">

                                                                                                                                                            <button  class="btn btn-light buttons-collection dropdown-toggle btn-sm ">

                                                                                                                                                              <i class="fa fa-plus"></i>
                                                                                                                                                            </button>
                                                                                                                                                            <button onclick=sendApprove('<?php echo $id_group_cab ?>','3') data-dismiss='modal' class="btn btn-success buttons-collection dropdown-toggle btn-sm pt--6 pb--6" style="color: white; width: 160px; height:42px;">
                                                                                                                                                                   Approve</button>
                                                                                                                                                    </div>

                                                                                                                                            <!--    <input checked style="height:20px; width:20px; vertical-align: middle;"  value="3" id="approve_decline_all<?php echo $id_group_cab; ?>" type="radio" name="approve_decline_all<?php echo $id_group_cab; ?>"> -->

                                                                                                                                             </div>



                                                                                                                                              <div class="dropdown-item text-truncate js-ajax-confirm pt-2 pb-2 pl--8 pr--8">

                                                                                                                                                  <div class="btn-group">

                                                                                                                                                            <button  class="btn btn-light buttons-collection dropdown-toggle btn-sm ">

                                                                                                                                                              <i class="fi fi-close"></i>
                                                                                                                                                            </button>
                                                                                                                                                            <button onclick=sendApprove('<?php echo $id_group_cab ?>','4') data-dismiss='modal'  class="btn btn-danger buttons-collection dropdown-toggle btn-sm pt--6 pb--6" style="color: white; width: 160px; height:42px;">
                                                                                                                                                                   Decline</button>
                                                                                                                                                    </div>

                                                                                                                                                  <!-- <input style="height:20px; width:20px; vertical-align: middle;"  value="4" id="approve_decline_all<?php echo $id_group_cab; ?>" type="radio" name="approve_decline_all<?php echo $id_group_cab; ?>"> -->

                                                                                                                                               </div>



                                                                                                                            							</div>

                                                                                                                            						</div>

                                                                                                                            					</div>



                                                                                                                                    <?php
                                                                                                                                      }
                                                                                                                                    ?>
                                                                                                                                  </th>




                                                                                                          												</tr>
                                                                                                          											</thead>
                                                                                                                                <tbody id="item_approve">
                                                                                                                                  <!-- product -->
                                                                                                                                  <?php






                             $sel_orderInt = " select br.type tipoReq, g.file_path5 as img , p.name as variedad, sz.name as size_name_request, br.price_front as priceRequest,
                                                      br.qty as cantRequest, p.image_path as fotoRequest, g.growers_name as growerRequest, gor.type_market as tipoOfer,
                                                      gor.product_subcategory as subcat_offer, gor.product as variedadOffer , gor.size as sizeOffer , gor.steams as steamsOffer ,
                                                      gor.price as preciOfer , gor.bunchqty*gor.steams as Stems_offered , gor.bunchqty as cantofer , gor.grower_id as growerOffer ,
                                                      gor.countdown , br.id_status as status_request,gor.steams, 
                                                      (select image_path from product where name = gor.product   and subcate_name = gor.product_subcategory group by image_path) as fotoOffer, 
                                                      f.name as featurename,
                                                       (select bs.name  from grower_parameter gp   inner JOIN bunch_sizes bs ON bs.id = gp.stem_bunch   where gp.idsc = p.subcategoryid   and gp.size = sz.id   and gp.stem_bunch = gor.steams  group by bs.name) as stemsRequest,
                                                       gor.request_id, gor.id as idgor, gor.id_group_cab
                                                from buyer_requests br
                                                left JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                                left JOIN product p ON br.product = p.id
                                                left JOIN buyers b  ON br.buyer   = b.id
                                                left JOIN buyer_orders bo ON br.id_order = bo.id
                                                left JOIN sizes sz   ON br.sizeid    = sz.id
                                                left JOIN growers g  ON br.id_grower = g.id
                                                left JOIN features f ON br.feature   = f.id
                                               where gor.buyer_id     = '$userSessionID'
                                                 and gor.id_group_cab = '$id_group_cab'";

                                                $rs_order1=mysqli_query($con,$sel_orderInt);
                                                    $rs_order1NumR=mysqli_num_rows($rs_order1);
                                                    $j = 0;
                                                    $hy = 0;
                                                    $ii = 0;
                                                                while($orderCab1=mysqli_fetch_array($rs_order1))
                                                                {
                                                                                                                                                      $hy++;
                                                                                                                                                      $ii++;
                                                                                                                                                      $idOferta = $orderCab1['idgor'];
                                                                                                                                                      $fotoOferta = $orderCab1['fotoOffer'];
                                                                                                                                                      $fotoRequest = $orderCab1['fotoRequest'];
                                                                                                                                                      $varidadRequest = $orderCab1['variedad'];
                                                                                                                                                      $varidadOferta = $orderCab1['variedadOffer'];
                                                                                                                                                      $productSubcategory = $orderCab1['subcat_offer'];
                                                                                                                                                      $sizeRequest = $orderCab1['size_name_request'];
                                                                                                                                                      $sizeOferta = $orderCab1['sizeOffer'];
                                                                                                                                                      $TallosBuncheRequest = 25; //$orderCab1['variedad'];
                                                                                                                                                      $TallosBuncheOferta =  25; //$orderCab1['variedad'];
                                                                                                                                                      $featureRequest = ''; //$orderCab1['variedad'];
                                                                                                                                                      $featureOferta =  ''; //$orderCab1['variedad'];
                                                                                                                                                      $qtyRequest = $orderCab1['cantRequest'];
                                                                                                                                                      $qtyOferta =  $orderCab1['cantofer'];
                                                                                                                                                      $priceRequest = $orderCab1['priceRequest'];
                                                                                                                                                      $priceOferta =  $orderCab1['preciOfer'];
                                                                                                                                                      $idRequestTT = $orderCab1['request_id'];


                                                                                                                                                      $farmRequest = $orderCab1['growerRequest'];
                                                                                                                                                      if($farmRequest==''){
                                                                                                                                                        $farmRequestLabel = '<span class="text-primary fs--14"><span class="text-danger">*** </span><strong>Best deal</strong></span>';
                                                                                                                                                      }


                                                                                                                                                      $farmOferta =  $orderCab1['growerOffer'];
                                                                                                                                                      $farmOfertaArray = @mysqli_query($con,"SELECT * FROM growers where id='$farmOferta'");
                                                                                                                                                      $farmOfertaLabel = @mysqli_fetch_array($farmOfertaArray);

                                                                                                                                                      $tipoRequest = $orderCab1['tipoReq'];
                                                                                                                                                      $tipoOferta =  $orderCab1['tipoOfer'];

                                                                                                                                                      if($tipoRequest==0){
                                                                                                                                                        $tipoRequest = 'Standing Order';
                                                                                                                                                      }else
                                                                                                                                                      {
                                                                                                                                                        $tipoRequest = 'Open Market';
                                                                                                                                                      }

                                                                                                                                                      if($tipoOferta==0){
                                                                                                                                                        $tipoOferta = 'Standing Order';
                                                                                                                                                      }else
                                                                                                                                                      {
                                                                                                                                                        $tipoOferta = 'Open Market';
                                                                                                                                                      }

                                                                                                                                                       $status_request = 4;

                                                                                                                                                       if($statusCab==2){
                                                                                                                                                        $titleStatus = '<a href="#" class="text-secondary btn btn-sm p-0">Pending</a>';
                                                                                                                                                       }

                                                                                                                                                       if($statusCab==3){
                                                                                                                                                         $titleStatus = '<a href="#" class="text-success btn btn-sm p-0">Confirmed</a>';
                                                                                                                                                       }

                                                                                                                                                       if($statusCab==4){
                                                                                                                                                          $titleStatus = '<a href="#" class="text-danger btn btn-sm p-0">Rejected</a>';
                                                                                                                                                       }


                                                                                                                                                        if($status_request==1)
                                                                                                                                                        {
                                                                                                                                                          $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-sucess btn btn-sm p-0 fs--15' href='#'>Complete</a></div>";
                                                                                                                                                        }

                                                                                                                                                        if($status_request==2){

                                                                                                                                                        $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-purple btn btn-sm p-0 fs--15' href='#'>Waiting approve...</a></div>";
                                                                                                                                                      }

                                                                                                                                                      if($status_request==3){
                                                                                                                                                        $titleStatus = '<a href="#" class="text-success btn btn-sm p-0">Confirmed</a>';
                                                                                                                                                      $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-success btn btn-sm p-0 fs--15' href='#'>Complete</a></div>";
                                                                                                                                                    }

                                                                                                                                                    if($status_request==4){

                                                                                                                                                    $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-danger btn btn-sm p-0 fs--15' href='#'>Not Approved</a></div>";
                                                                                                                                                  }

                                                                                                                                   ?>
                                                                                                                                  <tr>

                                                                                                                                    <td>

                                                                                                                                        <button type="button" class="btn btn-black btn-soft" data-html="true" data-toggle="tooltip" data-placement="bottom" title="
                                                                                                                                        <?php
                                                                                                                                        echo "<div>";
                                                                                                                                        echo "<span class='d-block text-white fs--15'>".$productSubcategory." ".$varidadRequest." ".$sizeRequest." [cm] ".$TallosBuncheRequest." st/bu".$featureRequest." / $</strong>".$priceRequest."</span>";
                                                                                                                                        echo "<span class='d-block text-white fs--15'><strong>Farmer:</strong>  ".$farmOfertaLabel[1]."</span>";
                                                                                                                                        echo "<span class='d-block text-white fs--13'><strong>Quantity:</strong> ".$qtyRequest."</span>";
                                                                                                                                        echo "</div>";


                                                                                                                                         ?>
                                                                                                                                        ">
                                                                                                                                        <a href="#" class="btn btn-secondary rounded-circle">
                                                                                                                                        	  <span class="text-white"><i class="fi fi-eye" aria-hidden="true"></i></span>
                                                                                                                                        </a>
                                                                                                                                      </button>
                                                                                                                                    </td>

                                                                                                                                    <td>

                                                                                                                                      <img style="border-radius: 50%; height: 65px; width: 65px;" alt="<?php echo $varidadOferta; ?>" src="/<?php echo $fotoOferta; ?>" width="75">
                                                                                                                                        <input type="hidden" value="<?php echo $idOferta;  ?>" id="valRBID<?php echo $hy; ?>" />


                                                                                                                                    </td><!-- product name -->
                                                                                                                                    <td>
                                                                                                                                      <h6 class="modal-title text-primary" id="exampleModalLabelMd"><?php echo $productSubcategory." ".$varidadOferta." ".$sizeOferta." [cm]"." ".$TallosBuncheOferta." st/bu"." ".$featureOferta; ?></h5>
                                                                                                                                      <span class="d-block text-muted fs--13">Quantity: <?php echo $qtyOferta; ?></span>
                                                                                                                                      <!--  <div class="input-group input-group">
                                                                                                                                          <a class="btn btn-xxxx text-white btn btn-block b-0 p-2 w--0" data-toggle="collapse" href="#xxx<?php echo $id_group_cab; ?>"      role="button" aria-expanded="false" aria-controls="multiCollapseExample1<?php echo $id_group_cab; ?>">  </a>
                                                                                                                                          <a class="btn btn-secondary text-white btn-sm btn-block b-0 p-2 w--150 fs--15" data-toggle="collapse" href="#multiCollapseExample1<?php echo $id_group_cab; ?>"      role="button" aria-expanded="false" aria-controls="multiCollapseExample1<?php echo $id_group_cab; ?>">     <i class="fa fa-list" aria-hidden="true"></i> Request details </a>
                                                                                                                                        </div> -->
                                                                                                                                      <p>






                                                                                                                                      <!-- Modal -->
                                                                                                                                      <div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
                                                                                                                                        <div class="modal-dialog" role="document">
                                                                                                                                          <div class="modal-content">
                                                                                                                                            <!-- Header -->
                                                                                                                                            <div class="modal-header">
                                                                                                                                              <h5 class="modal-title" id="exampleModalLabelMd">
                                                                                                                                                Standard Rose Freedom 50 25st/bu
                                                                                                                                              </h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
                                                                                                                                            </div><!-- Content -->
                                                                                                                                            <div class="modal-body">
                                                                                                                                              <img alt="..."  src="/images/product-image/big/062013073839_crop.jpg" width="100%">
                                                                                                                                            </div>
                                                                                                                                          </div>
                                                                                                                                        </div>
                                                                                                                                      </div><!-- /Modal -->

                                                                                                                                    </td><!-- price -->
                                                                                                                                    <td>
                                                                                                                            <span class="d-block text-danger fs--14"><sup>*</sup>Price <?php echo $priceOferta; ?> <sup class="text-muted fs--10">USD</sup></span>
                                                                                                                                      <a class="d-block text-success fs--15 js-ajax-modal"><?php echo $farmRequestLabel; ?></a>
                                                                                                                                      <span class="d-block text-muted fs--13"><?php echo $tipoRequest; ?></span>

                                                                                                                                    </td><!-- brand -->
                                                                                                                                    <td class="text-muted text-left">
                                                                                                                                      <span class="fs--15"><?php  echo $farmOfertaLabel[1]; ?></span>
                                                                                                                                    </td><!-- status -->

                                                                                                                                    <td class="text-left">
                                                                                                                                      <span class="fs--15"><?php  echo $titleStatus; ?></span>
                                                                                                                                    </td><!-- status -->

                                                                                                                                    <td colspan="2" class="text-left custom_td">

                                                                                                                                      <?php  if($ii<=1)
                                                                                                                                        { ?>



                                                                                                                                            <?php
                                                                                                                                            if($statusCab==2)
                                                                                                                                            {
                                                                                                                                            ?>


                                                                                                                                            <?php

                                                                                                                                            if($contadorOffers!=0){
                                                                                                                                              echo "<p><p>";
                                                                                                                                            //
                                                                                                                                            //$botonProcess = "<button type='button' onclick=sendApprove('$cadenaItemsData') class='btn btn-success b-0 p-1 w--200' data-dismiss='modal'>Process Request</button>";
                                                                                                                                          //  $botonProcess = "<button type='button' onclick=sendApprove('$id_group_cab') class='btn btn-success b-0 p-1 w--200' data-dismiss='modal'>Process Request</button>";
                                                                                                                                            }


                                                                                                                                            echo $botonProcess;
                                                                                                                                            ?>
                                                                                                                                          </div>
                                                                                                                                            <?php
                                                                                                                                          }else{
                                                                                                                                            ?>
                                                                                                                                          <!--  <button <?php echo $infomodal_c; ?>  class="btn btn-sm btn-block b-0 w--200 <?php echo $botonR_c; ?> b-0">
                                                                                                                                                                  <span class="p-0-xs">

                                                                                                                                                                    <?php  echo $icon_c;?>

                                                                                                                                                                    <span class="text-uppercase fs--14">

                                                                                                                                                                      <?php echo $titleButton; ?>
                                                                                                                                                                    </span>
                                                                                                                                                                  </span>
                                                                                                                                                                </button> -->

                                                                                                                                                              <?php } ?>
                                                                                                                                        <?php } ?>
                                                                                                                                      </td>

                                                                                                                                    </tr>



                                                                                                                                  <?php } ?>
                                                                                                                                  <!-- product -->
                                                                                                                                </tbody>
                                                                                                          										</table>


                                                                                                                            <hr class="mt-1 mb-1 border-gray">
                                                                                                                            <?php } ?>

                                                                                                          						</div>


                                                                                                                          



                                                                                                          						<!-- Footer -->
                                                                                                          						<div class="modal-footer">

                                                                                                                        <?php
                                                                                                                        //echo $botonProcess;
                                                                                                                       ?>

                                                                                                                          <button type="button" class="btn btn-danger " onclick="destro('<?php echo $cartid;?>','<?php echo $st_request; ?>')">

                                                                                                                                Close X
                                                                                                                          </button>
                                                                                                          						</div>

                                                                                                          				</div>
                                                                                                          		</div>
                                                                                                          </div>
                                                                                                          <!-- Billing Modal -->




                            </td>


                          </tr>




                          <?php





                              }
                             ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Img</th>
                            <th>Request</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Farm</th>
                            <th>Order Type</th>
                            <th>Option</th>


                          </tr>
                        </tfoot>
                      </table>
              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->

      <!--Select Orders Modal Open-->
      <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
          <div class="modal-dialog modal-md modal-md" role="document">
              <div class="modal-content">

                  <!-- header modal -->
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fi fi-close fs--18" aria-hidden="true"></span>
                    </button>

                  </div>
                  <!-- body modal 3-->
                  <form action="../en/florMP.php" method="post" id="payment-form">
                  <div class="modal-body">
                      <div class="table-responsive">

                        <font color="#000">Please, before to continue select an order.</font><br><br>

                        <div class="form-label-group mb-3">
                        <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                    <option value='0'>Select Previous Order</option>
                                    <?php
                                            $sel_order="select id , order_number ,del_date , qucik_desc
                                                          from buyer_orders
                                                         where del_date >= '" . date("Y-m-d") . "'
                                                           and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                            $rs_order=mysqli_query($con,$sel_order);

                                        while($orderCab=mysqli_fetch_array($rs_order))  {
                                    ?>
                                            <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                    <?php
                                        }
                                       ?>
                        </select>

                        <label for="select_options">Select Previous Order</label>
                         <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                      </div>



                                              <br>
                                             <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                      </div>



                  </div>

                  <div class="modal-footer request_product_modal_hide_footer">
                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                  <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                      <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                  </div>
                  </form>
              </div>
          </div>
      </div>



			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
			<script>

      function destro(a,b){

        location.reload();
      //  $("#billing_modal-"+a+"-"+b).removeClass('fade').modal('dispose');
      //  $("#myModal").modal("dispose");
      //  $("#billing_modal-"+a+"-"+b).modal('toggle');
      //  document.getElementById('billing_modal-'+a+'-'+b).modal('dispose');



      }
			function checkOrderPrevious(){
			      var orderP =  document.getElementById('selectPreviousOrder').value;
			      if(orderP==0){
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = true;
			  }
			      else{
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = false;
			        }
			    }


			        function checkOption() {


			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            if (myRadio.filter(':checked').length > 0) {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shippingMethod,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        $('#nextOpt').click();
			                        _pickers();//show calendar
			                    }
			                });




			            } else {
			                $('#erMsg').show();
			            }
			        }

			        //esta  es  la  nueva  opcion  by  Jose Portilla
			        function shippingChange(product_id, sizename, i) {
			            var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
			            if (shipping_val != "") {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        _pickers();//show calendar
			                    }
			                });

			            } else {
			                $('#erMsg').show();
			            }
			        }

			        function send_request() {
			            var delDate = $('#cls_date').find("input").val();
			            var qucik_desc = $('#qty_desc').val();
			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            var dateRange = "";

			            var flag_s = true;


			            if (qucik_desc == "") {
			                alert("Please enter quick description.");
			                flag_s = false;
			            }
			            else if (delDate == "") {
			                alert("Please select delivery date.");
			                flag_s = false;
			            }

			            if (flag_s == true) {
			                $.ajax({
			                    type: 'post',
			                    url: '/file/redirectrequest.php',
			                    data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
			                    success: function (data) {
			                        alert("Your order was created successfully");
			                        window.location.href = '/buyer/ordersnd.php';
			                    },
			                    error: function () {
			                        alert(" ! Your order has not been created !");
			                    }
			                });
			            }


			        }
			</script>



      <script type="text/javascript">

      function checkCloclk(a,b)
      {
      var subst = '';
      var subst = b.substring(1);
      var divisiones = subst.split(",");
      var tamanioCab = divisiones.length;

      var sec=0;

      var offersId=[];
      var timesSetId=[];


      for(var i=0; i<tamanioCab;i++)
      {

        sec = divisiones[i];
      //  alert(sec);
      //  document.getElementById("tiles"+sec).value = '';

        var minutes = $('#set-time'+sec).val();
        var setTimeOut = $('#set-time'+sec).val();
        var idRBID = $('#requestIDID'+sec).val();

        offersId[i]=idRBID;
        timesSetId[i]=setTimeOut;

        var datosRBID = 's_offersId=' + offersId + '&s_times=' + timesSetId+ '&s_total=' + tamanioCab;

        var target_date = new Date().getTime() + ((minutes * 60 ) * 1000);
        // set the countdown date
        var time_limit = ((minutes * 60 ) * 1000);
        //set actual timer

        if(time_limit<0){
        setTimeout(
          function()
          {
            $.ajax({
                 type: "POST",
                 url: "UpdateOfferComplete.php",
                 data: datosRBID,
                 cache: false,
                 success: function(r){
                    //  alert(r);
                    //  return false;
                     //if(idRBID!=3)
                     //{
                        swal("Request Completed!", "Your request was completed.","warning");
                       var x = setInterval(function() {
                         location.reload();

                         /*********************************/
                         // Actualizar Oferta             //
                         /*********************************/




                         swal.close();
                       }, 1000);
                    // }
                 }
             });



          }, time_limit );

        }

        //  countdown.innerHTML = '';

          getCountdown(sec,target_date);
          setInterval(getCountdown,1000,sec,target_date);

        } // cierro el for





        function getCountdown(sec,target_date)
        {


          //alert(sec);


          //; // get tag element

              var days, hours, minutes, seconds; // variables for time units
                    // find the amount of "seconds" between now and target
                    var current_date = new Date().getTime();
                    var seconds_left = (target_date - current_date) / 1000;


                    if ( seconds_left >= 0 )
                    {

                    days =  parseInt(seconds_left / 86400) ;
                    seconds_left = seconds_left % 86400;

                    hours = parseInt(seconds_left / 3600) ;
                    seconds_left = seconds_left % 3600;

                    minutes = parseInt(seconds_left / 60) ;
                    seconds = parseInt( seconds_left % 60 ) ;

                    // format countdown string + set tag value
                    //document.getElementById("tiles"+sec).innerHTML = "loading";
                  //  document.getElementById("tiles"+sec).innerHTML = "<span>" + 00 + "</span>";
                  //  document.getElementById("tiles"+sec).innerHTML = "<span>" + minutes + "</span>";
                    document.getElementById("tiles"+sec).innerHTML = "<span>" + hours + ":</span><span>" + minutes + ":</span><span>" + seconds + "</span>";


                    }

        }



        function pad(n) {
          return (n < 10 ? '0' : '') + n;
        }


      }



      function approveAll(h)
      {
        //var totalOffert = document.getElementById('totalOffert').value;

        for(var i=1; i<=h; i++){


        document.getElementById('approve'+i).value = 1;
        document.getElementById('approve'+i).checked = true;

        document.getElementById('decline'+i).value = 4;
        document.getElementById('decline'+i).checked = false;

        }

      }

      function declineAll(k)
      {
      //  var totalOffert = document.getElementById('totalOffert').value;

        for(var i=1; i<=k; i++){

        document.getElementById('decline'+i).value = 4;
        document.getElementById('decline'+i).checked = true;

        document.getElementById('approve'+i).value = 1;
        document.getElementById('approve'+i).checked = false;

        }

      }

      function approveOne(a){

        document.getElementById('decline'+a).value = 4;
        document.getElementById('decline'+a).checked = false;

        document.getElementById('approve'+a).value = 1;
        document.getElementById('approve'+a).checked = true;
      }

      function declineOne(a){

        document.getElementById('decline'+a).value = 4;
        document.getElementById('decline'+a).checked = true;

        document.getElementById('approve'+a).value = 1;
        document.getElementById('approve'+a).checked = false;
      }

      function verificaOld(l){

        //var totalOffert = $('#totalOffert').val();
       //alert(l);
       var subst = '';
       var subst = l.substring(1);
        var divisiones = subst.split(",");
        var tamanioCab = divisiones.length;

        var total = 0;
        var totalFlag = 0;
        var sec=0;

          for(var i=0; i<tamanioCab;i++)
          {

          sec = divisiones[i];
          //alert(sec);
            if(document.getElementById('approve_decline_all'+sec) !== null){
              totalFlag = totalFlag + 1;
              var valorAAA = document.getElementById('approve_decline_all'+sec).value;
              //if(document.getElementById('approve_decline_all'+i).checked){
                total  = total + 1;
              //  alert(total);
              //  }
              }
            }



        if(totalFlag==total)
        {
          // alert('completo');
          return true;
        }else{
          // alert('no-completo');
          return false;
        }
    }

    function verifica(l){

      var total = 0;
      var totalFlag = 0;
      var sec=0;

      //  alert(l);
          if(document.getElementById('approve_decline_all'+l) !== null){
            totalFlag = totalFlag + 1;
            var valorAAA = document.getElementById('approve_decline_all'+l).value;
            //if(document.getElementById('approve_decline_all'+i).checked){
              total  = total + 1;
            //  alert(total);
            //  }
            }




      if(totalFlag==total)
      {
        // alert('completo');
        return true;
      }else{
        // alert('no-completo');
        return false;
      }
  }

    function sendApprove(l,k)
    {
    //  alert();
      if(verifica(l))
      {

        //alert(l);
        swal({
               title: "Are you sure?",
               text: "Once Approved, you will not be able change the action!",
               icon: "warning",
               buttons: true,
               dangerMode: true,
             })
             .then((willDelete) =>
             {
               if (willDelete)
               {

                 var idRBID = $('#requestIDID'+l).val();

                /* if(document.getElementById('approve_decline_all'+l) !== null){
                   if(document.getElementById('approve_decline_all'+l).checked){
                      value = document.getElementById('approve_decline_all'+l).value;
                    }else{
                      value = 4;
                    }
                   }else{
                     value = 4;
                   }*/





             var datosRBID = "IDGRWO="+idRBID+'&s_offersId=' + idRBID + '&s_values=' + k;
             // alert(datosRBID);
            //  return false;

              $.ajax({
                   type: "POST",
                   url: "UpdateOfferCompleteApproved.php",
                   data: datosRBID,
                   cache: false,
                   success: function(r){
                      //  alert(r);
                      //  return false;
                      if(r==1)
                       {
                         swal("Request Completed!", "Your request was completed.","warning");
                         var x = setInterval(function() {
                           location.reload();

                           /*********************************/
                           // Actualizar Oferta             //
                           /*********************************/

                           swal.close();
                         }, 2000);
                       }
                   }
               });



               } else {
                 //Nothing to do!

               }
             });



      }
      else{
        swal("Incorrect value!", "All options are required","warning");
      }

    }


      function sendApproveOld(l)
      {
      //  alert(l);
        if(verifica(l))
        {
          swal({
                 title: "Are you sure?",
                 text: "Once Approved, you will not be able change the action!",
                 icon: "warning",
                 buttons: true,
                 dangerMode: true,
               })
               .then((willDelete) =>
               {
                 if (willDelete) {

                  var subst = '';
                  var subst = l.substring(1);
                  var divisiones = subst.split(",");
                  var tamanioCab = divisiones.length;
                  var sec=0;

                // alert(tamanioCab);
                // return false;

                var offersId=[];
                var values=[];
                var idRBIDId=[];

                for (var i=0; i<tamanioCab; i++) {
                  sec = divisiones[i];

                    var idRBID = $('#requestIDID'+sec).val();
                    var value;

                    if(document.getElementById('approve_decline_all'+sec) !== null){
                      if(document.getElementById('approve_decline_all'+sec).checked){
                         value = document.getElementById('approve_decline_all'+sec).value;

                       }else{
                         value = 4;
                       }
                      }else{
                        value = 4;
                      }

                    offersId[i]=idRBID;
                    values[i]=value;
                    idRBIDId[i] = idRBID;
                }

                var datosRBID = "IDGRWO="+idRBIDId+'&s_offersId=' + idRBIDId + '&s_values=' + values+ '&s_total=' + tamanioCab;
              //  alert(datosRBID);
             //    return false;

                $.ajax({
                     type: "POST",
                     url: "UpdateOfferCompleteApproved.php",
                     data: datosRBID,
                     cache: false,
                     success: function(r){
                        //  alert(r);
                        //  return false;
                         //if(r==11)
                         //{
                           swal("Request Completed!", "Your request was completed.","warning");
                           var x = setInterval(function() {
                             location.reload();

                             /*********************************/
                             // Actualizar Oferta             //
                             /*********************************/

                             swal.close();
                           }, 2000);
                        // }
                     }
                 });



                 } else {
                   //Nothing to do!

                 }
               });

        }
        else{
          swal("Incorrect value!", "All options are required","warning");
        }

      }

      </script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">

			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
