<?php
session_start();


require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');

$menuoff = 1;
$page_id = 421;
$message = 0;


$userSessionID = $_SESSION["buyer"];

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}


$cliente_inv = $_GET['id_cli'];
$swcab = $_GET['sw'];
$idfac = $_GET['fac_id'];



/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}

$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 16 Jun 2021
Structure MarketPlace previous to buy
**/


// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

include('../back-end/inc/header_ini.php');
?>
			<script src="../back-end/assets/js/blockui.js"></script>
<script>

function selectscategory()	{


  removeAllOptions(document.frmrequest.var_add);
	addOption(document.frmrequest.var_add,"","-- Select Variety --");


	  <?php
    $sel_subcategory="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id ,
                                         gor. product as  productname ,
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path , res.id_client as control,qucik_desc
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name
                                   inner join buyer_orders bo  on br.id_order = bo.id
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id
                                    left JOIN buyer_requests res ON gor.request_id = res.id
                                   where g.active = 'active'
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and bo.availability in (1,2) 
                                     and br.buyer = '" . $userSessionID . "'
                                     and res.id_client = 0 order by br.id_order";

		/*$sel_subcategory="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id ,
                                         gor. product as  productname ,
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path , res.id_client as control,qucik_desc
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name
                                   inner join buyer_orders bo  on br.id_order = bo.id
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id
                                    left JOIN buyer_requests res ON gor.request_id = res.id
                                   where g.active = 'active'
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and bo.availability in (1,2) 
                                     and br.buyer = '" . $userSessionID . "'
                                     and res.id_client = 0 order by br.id_order";*/

		$res_subcategory=mysqli_query($con,$sel_subcategory);

		while($rw_subcategory=mysqli_fetch_array($res_subcategory))	{

	  ?>

                var ordenid = $('#order_add' + ' :selected').val();

                if(ordenid=="<?php echo $rw_subcategory["id_order"]?>")	{

                          addOption(document.frmrequest.var_add,"<?php echo $rw_subcategory["codvar"];?>","<?php echo $rw_subcategory["bunchqty"]."  ".$rw_subcategory["productname"]."-*-".$rw_subcategory["size_name"]."-cm ".$rw_subcategory["growers_name"]."-".$rw_subcategory["idgor"]."-".$rw_subcategory["featurename"]."-".$rw_subcategory["product_subcategory"]."-".$rw_subcategory["qucik_desc"] ;?>");

			 $('#var_add').val('<?php echo $_POST["var_add"]?>');

                         //var xx = document.
	  	}

	  <?php	}   ?>

	}


	function removeAllOptions(selectbox)	{
		var i;

		for(i=selectbox.options.length-1;i>=0;i--){
			selectbox.remove(i);
		}
	}

	function addOption(selectbox,value,text){

		var optn=document.createElement("OPTION");
		optn.text=text;
		optn.value=value;
		selectbox.options.add(optn);
	}

function imagen(){

	  <?php

		$sel_img="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id ,
                                         gor. product as  productname ,
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path , res.id_client as control
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name
                                   inner join buyer_orders bo on br.id_order = bo.id
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id
                                    left JOIN buyer_requests res ON gor.request_id = res.id
                                   where g.active = 'active'
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and bo.availability in (1,2) 
                                     and br.buyer = '" . $userSessionID . "'
                                     and res.id_client = 0 ";

		$res_img=mysqli_query($con,$sel_img);

		while($rw_imagen=mysqli_fetch_array($res_img))	{

	  ?>

                var idprod = $('#var_add' + ' :selected').val();


               // alert(ordenid);

                if(idprod=="<?php echo $rw_imagen["codvar"]?>")	{

                        $('#errormsg-').html("<?php echo $rw_imagen["image_path"]?>")

                        var span_Text = document.getElementById("errormsg-").innerText;
                        //alert (span_Text);

                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;

                        $('input[name=idimg]').val(span_Text);

                        $('#my_image').attr('src',url_url);
	  	}

	  <?php	}   ?>

	}

function imagen_req(){

        var productText_requ = $('#productvar' + ' :selected').text();

        var path = productText_requ.split("+", 2);
        $('input[name=imgpath]').val(path[1]);

       var pimage = $('#imgpath').val();

        //alert(pimage);

                        $('#errormsg-').html(pimage)

                        var span_Text = document.getElementById("errormsg-").innerText;

                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;

                        $('input[name=idimg]').val(span_Text);

                        $('#my_image').attr('src',url_url);

	}
</script>


<?php

/*
    $sqlDetalis="select irs.id  as gid      ,  irs.id_week   , irs.id_client , irs.product,
                        irs.prod_name , irs.buyer   , irs.grower_id , irs.qty_pack  , irs.size   ,
                        irs.steams    , irs.comment,
                        s.name as psubcatego , p.name as name_product , sz.name as size_name,
                        cli.name as subclient,
                        irs.id_state , irs.fact_id,
                        irs.date_ship  , irs.date_del
                  from order_subclient irs
                 INNER JOIN product p on irs.product = p.id
                 INNER JOIN sizes sz  on irs.size = sz.id
                 INNER join subcategory s on p.subcategoryid = s.id
                 INNER JOIN sub_client cli on irs.id_client = cli.id
                 where irs.fact_id   = '".$idfac."'
                 order by s.name,p.name ";

    $result = mysqli_query($con, $sqlDetalis);
*/

 ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->



				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Customer Order </strong>
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">

            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
              <a href="/buyer/client_calendar_special.php" type="submit" class="btn btn-success mb-3 mt-3 d-block-xs w-100-xs">
  												<i class="fa fa-long-arrow-left" aria-hidden="true"></i>
  												Back
  											</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>





								<div class="row row-grid b-0">

									<div class="col-8 col-sm-8 col-md-8 col-lg-8">

										<div class="px-2 pb-2">

											<h6 class="mt-2 text-primary">
												CHOOSE AN OPTION
											</h6>

                      <table class="table table-align-middle table-sm">
                      		<thead>
                      			<tr>
                      				<th class="w--90">Head</th>
                      				<th>Option</th>

                      			</tr>
                      		</thead>

                      		<tbody>

                      			<tr>
                      				<td>
                                Client
                            	</td>
                      				<!-- SKU -->
                      				<td>
                                <?php   $sql_cli = "select id,name 
                                                        from sub_client 
                                                       where id != 1 
                                                         and buyer = '" . $userSessionID . "'
                                                         and active = 'active'    
                                                       order by name";
                                                    $result_cli = mysqli_query($con, $sql_cli);
                                ?>
                      			      <select class="form-control form-control-sm bs-select js-bselectified" name="cli_add" id="cli_add" onclick="checkOptionship();"  title="--Select Client--" >
                                    <option class="bs-title-option" value="">--Select Client--</option>
                                    <?php
                                        while ($row_client = mysqli_fetch_array($result_cli)) {
                                    ?>
                      						<option class="bs-title-option" value="<?php echo $row_client['id']; ?>"><?php echo $row_client['name']; ?></option>
                      					<?php    }	?>
                      					</select>
                      				</td>
                              		</tr>
                                  		<tr>

                              <td>
                                Delivery day
                              </td>
                              <!-- SKU -->
                              <td>

                              <label class="field cls_date_start_dated" id="cls_date_del">
                                            <input  class="datepicker form-control start_date" disabled="disabled" placeholder="Select Date Delivery">
                                </label>
                                        <input type="hidden" name="sname" id="sname" value="<?php echo $_POST["sname"];?>" />
                                        <input type="hidden" name="dname" id="dname" value="<?php echo $_POST["dname"];?>" />
                                        <input type="hidden" name="sizename" id="sizename" value="<?php echo $_POST["sizename"];?>" />

                              </td>



                      			</tr>
                            <tr>
                              <td>
                                Order
                              </td>
                              <td>
                                <?php   $sql_order = "select id,qucik_desc
                                                      from buyer_orders
                                                     where buyer_id = '" . $userSessionID . "'
                                                       and assigned=1";

                                                    $result_order = mysqli_query($con, $sql_order);
                                ?>

                                <select name="order_add" id="order_add"  onclick="grabaFecha();" class="form-control form-control-sm bs-select js-bselectified" >
                                            <option value="">--Select Order--</option>
                                    <?php
                                        while ($row_order = mysqli_fetch_assoc($result_order)) {
                                    ?>
                                             <option value="<?php echo $row_order['id']; ?>">
                                                            <?= $row_order['id']." ".$row_order['qucik_desc']; ?></option>
                                    <?php    }	?>
                                </select>
                              </td>
                            </tr>

                            <tr>
                                <td><b>
                                Availability</b>
                              </td>
                              <td>

                                <select name="var_add" id="var_add"  class="form-control form-control-sm bs-select js-bselectified" >
                                        <option value="">-- Select Variety --</option>
                                </select>

                                <button onclick="selectscategory()" class="btn btn-warning w--200 float-right" type="button">View Varieties</button>

                              </td>
                            </tr>

                            <tr>
                                <td><b>
                                Pre-order</b>
                              </td>
                              <td>

                                <select name="productvar"  id="productvar" class="form-control form-control-lg">
                                        <option value="">-- Select Variety --</option>
                                </select>

                              </td>
                            </tr>

                            <tr>
                              <td>
                                Bunch
                              </td>
                              <td>
                              <input type="number" class="form-control form-control-sm" type="number" name="qty_pack" id="qty_pack" value="<?php echo $_POST["qty_pack"];?>" />
                                </div>
                              </td>
                            </tr>

                            <input type="hidden" name="qty_ped" id="qty_ped"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_ped"];?>" />


                      		</tbody>
                      	</table>




									</div>
                  </div>

									<div class="col-4 col-sm-4 col-md-4 col-lg-4">

										<div class="px-2 pb-2">

											<h6 class="mb-4 mt-2 text-primary">
												FRESH LIFE FLORAL
											</h6>


                      <div class="error-box">
                                                                                                             <span id="errormsg-" style="color: white;"></span>
                                                                                                      </div>


                                                                                                 <img id="my_image" src="https://app.freshlifefloral.com//includes/assets/images/logo.png" width="200">

                                                                                                   <br>
                                                                                                  <!--
                                                                                                 <button  onclick="imagen()" class="btn btn-success btn-xs relative" type="button">View Asign</button>

                                                                                                 <button onclick="imagen_req()" class="btn btn-success btn-xs relative" type="button">View Request</button>
                                                                                                 <br> -->
                                                                                                 <input type="hidden" name="imgpath" id="imgpath" value="">

                                                                                                 <div class="error-box">
                                                                                                         <span id="errormsg-1" style="color:#FF0000; font-size:14px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                                                 </div>


										</div>

									</div>





								</div>


								<!-- customer detail -->

							</div>

              <div class="table-responsive">
                <table class="table table-condensed nomargin">
                  <thead>
                    <tr>
                      <th>Delivery</th>
                      <th>Variety</th>
                                                                                        <th>Bunches</th>
                                                                                        <th>Client</th>
                    </tr>
                  </thead>

                  <tbody>

                  <?php
                                                                                 $tmp_idorder = 0;

                                                                                 $cn=1;
                                                                                $ir = 1;

                                                            while($row = mysqli_fetch_assoc($result))  {

                                                                            ?>

                <?php
                                                                                $totalCal = $totalCal + $Subtotal;
                                                                                $cn++;
                                                            } ?>
                  </tbody>
                </table>
              </div>




              <div class="panel panel-default text-right">
                <div class="panel-body">
                                                            <button id="assig" class="btn btn-purple btn-xs relative"  onclick="requestProduct('<?php echo $ir ?>')" class="btn btn-primary" type="button">Assigned Customer</button>
                </div>
              </div>



								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
          </form>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>



                                                    <br>
                                                   <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>




			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
      <script type='text/javascript'>
          $(window).load(function () {
              $('#loading').css("display", "none");
          });
      </script>
      <script type="text/javascript">
          $(document).ready(function () {

                $("#assig").click(function(){
                    $("#assig").prop("disabled", true);
                });

                //   $('select').select2();

                      $('#productvar').select2({
                          ajax: {
                              url: "/buyer/search_variety_customer.php",
                              dataType: 'json',
                              delay: 10,
                              data: function (params) {

                                  return {
                                      q: params.term,
                                      s:'cities',
                                  };
                              },
                              results: function (data, page) {

                                  return {results: data.results};
                                  console.log("prod  "  + data);
                              },
                              cache: true
                          },
                          minimumInputLength: 1,
                      });



              ////////////////////////////////////////

                      $('#var_add_xx').select({
                          ajax: {
                              url: "search_date_filter.php",
                              dataType: 'json',
                              delay: 250,
                              data: function (params) {
                                  return {
                                      q: params.term,
                                      s:'cities',
                                  };
                              },
                              results: function (data, page) {
                                  return {results: data.results};
                                  console.log("prod  "  + data);
                              },
                              cache: true
                          },
                      });

          });

      </script>
			<script>

      window.onload=function() {
        //checkOptionship();
      }

      function grabaFecha() {

          var shipDate = $('#cls_date_ship').find("input").val();

           $('input[name=sname]').val(shipDate);

           console.log(shipDate);

          var delDate = $('#cls_date_del').find("input").val();

           $('input[name=dname]').val(delDate);

      }

      function bunQuantity() {

          var productText = $('#var_add' + ' :selected').text();

          var divisiones = productText.split("-", 1);

          var sizet = productText.split("-", 3);

          $('input[name=sizename]').val(sizet[2]);

          $('input[name=qty_pack]').val(divisiones);

           grabaFecha();
       }


      function boxQuantityChange(id, cartid, main_tr, with_a) {

          var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
          var selected_val  = $("#request_qty_box_" + main_tr + " option:selected").val();

  	var nbox = selected_val.split('-');
          var tbox = $.trim(selected_text).split(" ");

  	$('input[name=boxcant]').val(nbox[0]);

          $('input[name=boxtypen]').val(tbox[1]);

          $('input[name=product_su]').val(selected_val);
          $(".cls_hidden_selected_qty").val(selected_val);

      }


      // opcion que funciona del calendario pot

      function checkOptionship() {

          var shippingMethod = '132';

          var fecha_fin = $('#cls_date_del').find("input").val();


              $.ajax({
                  type: 'post',
                  url: '/file/get_date_modal_cliship.php',
                  data: 'shippingMethod=' + shippingMethod + '&fecha_fin=' + fecha_fin,
                  success: function (data) {

                      $('.cls_date_start_dates').html(data);
                      $('#nextOpt').click();
                      _pickers();//show calendar
                  }
              });

              checkOptiondel();
             // selectscategory();   // cambio final
      }

      function checkOptiondel() {

        //  var shippingMethod = '132';

          var fecha_ini = $('#cls_date_ship').find("input").val();


              $.ajax({
                  type: 'post',
                  url: '/file/get_date_modal_clidel.php',
                  data: 'fecha_ini=' + fecha_ini,
                  success: function (data) {

                      $('.cls_date_start_dated').html(data);
                      $('#nextOpt').click();
                      _pickers();//show calendar
                  }
              });

             //  send_date();     // Graba la fecha


      }
       function send_date() {

          var delDate = $('#cls_date_ship').find("input").val();

           $('input[name=sname]').val(delDate);

           console.log(delDate);

                  $('#var_add').select2({

                      ajax: {
                          url: "search_date_filter.php",
                          dataType: 'json',
                          delay: 250,
                          data: function (params) {
                              return {
                                  q: params.term,
                                  s:'cities',
                              };
                          },
                          results: function (data, page) {
                              return {results: data.results};
                              console.log("prod  "  + data);
                          },
                          cache: true
                      },
                      minimumInputLength: 1,
                  });
       }

      /*Button send request */
      function requestProduct(i) {


          var buyer     = '<?php echo $_SESSION["buyer"]; ?>'; // codigo  del  buyer
          var flag_s = true;
          var dateRange = "";

          var order_val      = $('#order_add' + ' :selected').val();



          var productId    = $('#var_add' + ' :selected').val();
          var productText  = $('#var_add' + ' :selected').text();


          var productId_requ      = $('#productvar' + ' :selected').val();
          var productText_requ    = $('#productvar' + ' :selected').text();



             //alert (productId_assign)  ;
             //alert (productId_requ)  ;



          var box_quantity   = $('#qty_pack').val() ;

          var type_box       = $('#box_type_'      + i + ' :selected').val();
          var type           = $('#type_req_'      + i).val();
          var comment_pro    = $('#comment_'       + i).val();
          //var req_grow       = $('#grow_'          + i).val();
          var req_grow       = "ADD";

          var tag            = $('#cli_add').val();

          var date_ship      = $('#sname').val();
          var date_del       = $('#dname').val();

          if (order_val != "") {
              $('#erMsg').hide();
          }else {
              flag_s = false;
              swal("Error!", "Please select Order!", "error");
              $('#erMsg').show();
          }


          if ($('#qty_pack').val() < 1) {
              $('#errormsg-1').html("Quantity error");
              flag_s = false;
          }

          // Valida Cantidad
          var qytmax = $.trim(productText).split(" ");

          $('input[name=qty_ped]').val(qytmax[0]);

          var qty_x = 0;
          var qty_control = 0;

            qty_x = $('#qty_pack').val();
           qty_control = $('#qty_ped').val();


          if (parseInt(qty_x) > parseInt(qty_control)) {
              $('#errormsg-1').html("Error Qty");
              flag_s = false;
          }




      if (flag_s == true) {
          console.log("flag_s "  + flag_s);
          console.log("order_val "   + order_val);
          console.log("productId "   + productId);

          $.ajax({
              type: 'post',
              url: '/buyer/request_product_ajax_subcliente.php',
              data: 'date_range=' + dateRange +
                     '&order_val=' + order_val +
                     '&productId=' + productId +
                '&productId_requ=' + productId_requ +
                  '&box_quantity=' + box_quantity +
                         '&buyer=' + buyer +
                      '&type_box=' + type_box +
                          '&type=' + type +
                   '&comment_pro=' + comment_pro +
                   '&productText=' + productText +
              '&productText_requ=' + productText_requ +
                           '&tag=' + tag +
                     '&date_ship=' + date_ship +
                      '&date_del=' + date_del +
                      '&req_grow=' + req_grow,
              success: function (data_s) {
                  if (data_s == 'true') {

                    swal("Great! Process completed.")
                    .then((value) => {
                        window.location.href = '/buyer/client_calendar_special.php';
                    });

                  } else {

                    swal("Sorry!", "There is some error. Please try again 1", "error");

                  }
                           //location.reload();


              }
          });
      }

      }

			function checkOrderPrevious(){
			      var orderP =  document.getElementById('selectPreviousOrder').value;
			      if(orderP==0){
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = true;
			  }
			      else{
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = false;
			        }
			    }


			        function checkOption() {


			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            if (myRadio.filter(':checked').length > 0) {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shippingMethod,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        $('#nextOpt').click();
			                        _pickers();//show calendar
			                    }
			                });




			            } else {
			                $('#erMsg').show();
			            }
			        }

			        //esta  es  la  nueva  opcion  by  Jose Portilla
			        function shippingChange(product_id, sizename, i) {
			            var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
			            if (shipping_val != "") {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        _pickers();//show calendar
			                    }
			                });

			            } else {
			                $('#erMsg').show();
			            }
			        }

			        function send_request() {
			            var delDate = $('#cls_date').find("input").val();
			            var qucik_desc = $('#qty_desc').val();
			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            var dateRange = "";

			            var flag_s = true;


			            if (qucik_desc == "") {
			                alert("Please enter quick description.");
			                flag_s = false;
			            }
			            else if (delDate == "") {
			                alert("Please select delivery date.");
			                flag_s = false;
			            }

			            if (flag_s == true) {
			                $.ajax({
			                    type: 'post',
			                    url: '/file/redirectrequest.php',
			                    data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
			                    success: function (data) {
			                        alert("Your order was created successfully");
			                        window.location.href = '/buyer/ordersnd.php';
			                    },
			                    error: function () {
			                        alert(" ! Your order has not been created !");
			                    }
			                });
			            }


			        }
			</script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
