<?php
              session_start();


              require_once("../config/config_gcp.php");
              include('../back-end/GlobalFSyn.php');


              $userSessionID = $_SESSION["buyer"];

              if ($_SESSION["login"] != 1) {
                  header("location:  /login.php");
                  die;
              }




              /**
              #Count temporal register to market place cart
              Developer educristo@gmail.com
              Start 16 Jun 2021
              Structure MarketPlace previous to buy
              **/


              $i = 0;
              $offer_price = 0;
                  $sql_special_products = "select sp.id as id , p.id as productid, p.name as productname, sp.imagen as productimage, sh.name as productsize,
                c.name as productcolor, f.name as productfeature, sp.price as productprice, sp.date_activacion as productdate,
                s.name as productsubcat, sh.id as size, sp.idspecial as productspecial
                from special_product sp
                inner JOIN product p ON sp.productid = p.id
                inner JOIN subcategory s on p.subcategoryid=s.id
                inner JOIN colors c  ON sp.colorid = c.id
                inner JOIN sizes sh on sp.sizeid=sh.id
                  left JOIN features f  ON sp.featureid = f.id
                where sp.status = '1'";
              $sql_array_products = mysqli_query($con,$sql_special_products);

               $num_products = mysqli_num_rows($sql_array_products);

              if($num_products<7){
                $detailSubmit ="type='submit' class='btn btn-block transition-hover-top btn-sm btn-primary bg-gradient-primary'";
              }else{
                $detailSubmit ="type='button' data-toggle='tooltip' data-placement='top' title='Maximum 7 products allowed' class='btn btn-block text-white btn-sm btn-secondary-soft bg-gradient-secondary'";
              }


              $selectProductTotal = "

              select
                    p.id AS productid    ,
                    p.image_path AS productimage  ,
                    fe.id AS productfeatureid        ,
                    gs.sizes AS productsizeid    ,
                    p.color_id AS productcolorid,
                    gp.price_adm as productprecio,
                    now() as fechaaacc,
                    1 as productostatus,
                    sc.id as productosubcat      ,
                    p.name AS productname    ,
                    c.name AS productcolor    ,
                    sc.name AS productsubcat,
                    s.name AS productsize ,
                    gs.is_bunch_value AS is_bunch_value,
                    fe.name AS featurename,
                    gr.bunch_size_id AS bunch_size_id,
                    bs.name AS bunchesv,
                    gs.categoryid AS categoryid
              from growcard_prod_bunch_sizes gs
              inner join product p on gs.product_id = p.id
              inner join subcategory sc on gs.sid = sc.id and gs.categoryid = sc.cat_id
              inner join growers g on gs.grower_id = g.id
              inner join growcard_prod_box_packing gr on gr.sizeid = gs.sizes and gr.bunch_size_id = gs.bunch_sizes and gr.growerid = gs.grower_id and gr.prodcutid = gs.product_id
              inner join sizes s on gs.sizes = s.id
              inner join colors c on p.color_id = c.id
              inner join boxes b on b.id = gr.box_id
              inner join boxtype bt on b.type = bt.id
              inner join bunch_sizes bs on gr.bunch_size_id = bs.id
              inner join grower_parameter gp on (gp.idsc = sc.id and gp.size = s.id )
              left join features fe on fe.id = gr.feature
              where g.active = 'active'
              and g.market_place = '1'
              group by p.name,p.id,p.color_id,gs.sizes,gs.is_bunch_value,s.name,fe.id,fe.name,gr.bunch_size_id,bs.name,gs.categoryid,gs.sid,sc.name
              order by p.name,sc.name,s.name";
              $sql_array_products_total = mysqli_query($con,$selectProductTotal);

              include('../back-end/inc/header_ini.php');
              ?>
              <div class="d-flex flex-fill" id="wrapper_content">
              <?php include('../back-end/inc/sidebar-menu.php'); ?>
              <!-- MIDDLE -->


              				<div class="flex-fill" id="middle">
              					<div class="page-title bg-transparent b-0">
              						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
              							<strong> Customer Webshop </strong>
              						</h1>
                          <nav class="mt-2 mb-0 px-3" aria-label="breadcrumb">
              						<ol class="breadcrumb fs--14">
              							<li class="breadcrumb-item active" aria-current="page">Webshop</li>
              						</ol>
              					</nav>
              					</div><!-- Primary-->
              					<section class="rounded mb-3 bg-white" id="section_1">

                          <form name="frmrequest" id="frmrequest" method="post" action="">
                                  <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                                  <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

              						<!-- graph header -->
              						<div class="clearfix fs--18 border-bottom" data-aos="fade-in" data-aos-delay="10" data-aos-offset="1">
                            <h2 class="h5 text-truncate w-100">
                										Frontend Products
                						</h2>
              							<!-- fullscreen -->

                            <div class="bg-white shadow-xs rounded fs--13">
              						  <div class="clearfix bg-light p-2 rounded d-flex align-items-center">
              							<span class="fs--13 btn row-pill btn-sm bg-gradient-secondary text-white b-0 py-1 mb-0 float-start">
              								<i class="fi fi-round-info-full"></i>
              								Note
              							</span>
              							<span class="d-block px-2 text-muted text-truncate">
              								Please choose and add <span class="badge badge-danger badge-soft fs--14 p-1 pl-2 pr-2 mt-2">
                            seven
                          </span>
                            products!
              							</span>
              						</div>
              					</div>

              						</div>

                          <div class="text-center-md text-center-xs" >

              							<div class=" bg-white shadow-primary-xs rounded p-4 p-4-xs text-align-start">

              								<h2 class="h5 mb-3">
              									<i class="fi fi-search"></i>
              									<span class="d-inline-block pl-1 pr-1">Product Search</span>
              								</h2>


              									<div class="form-label-group mb-4">
              										<select onchange="verifyselect();"  id="s_product" name="s_product" class="form-control bs-select" data-aos="fade-in" data-aos-delay="10" data-aos-offset="2" data-live-search="true" title="No Product Selected">
                                    <?php
                                    while($row_special_products_total = mysqli_fetch_array($sql_array_products_total))
                                    {

                                      $productname = $row_special_products_total["productname"];
                                      $productcolor = $row_special_products_total["productcolor"];
                                      $productsubcat = $row_special_products_total["productsubcat"];
                                      $productsize = $row_special_products_total["productsize"];

                                      $productid  = $row_special_products_total["productid"];
                                      $productimage = $row_special_products_total["productimage"];
                                      $productfeatureid = $row_special_products_total["productfeatureid"];
                                      $productsizeid = $row_special_products_total["productsizeid"];
                                      $productcolorid  = $row_special_products_total["productcolorid"];
                                      $productprecio = $row_special_products_total["productprecio"];
                                      $fechaaacc = $row_special_products_total["fechaaacc"];
                                      $productostatus = $row_special_products_total["productostatus"];
                                      $productosubcat = $row_special_products_total["productosubcat"];

                                     ?>
                                   <option value="<?php echo $productid."*".$productimage."*".$productfeatureid."*".$productsizeid."*".$productcolorid."*".$productprecio."*".$fechaaacc."*".$productostatus."*".$productosubcat; ?>">

                                     <?php echo $productname; ?> /
                                     <?php echo $productcolor; ?> /
                                     <?php echo $productsize. " [cm]"; ?> /
                                     <?php echo $productsubcat; ?>

                                   </option>

              			               <!--      <option data-content="<img class='img-fluid w--50' src='https://app.freshlifefloral.com/<?php echo $productimage; ?>'><span class='text-secondary font-weight-medium p-4'> <?php echo "   ".$productname; ?> / <?php echo $productcolor; ?> / <?php echo $productsubcat; ?></span>"s</option> -->
                                    <?php
                                    }
                                    ?>
              										</select>
              										<label for="s_location">Select a Product</label>
              									</div>

              									<button type="submit" onclick="verifyselect1();"  id="save_special_product" name="save_special_product" <?php echo $detailSubmit; ?>  >
              										Add Product
              									</button>

                                <input type="hidden" id="totalspecial" name="totalspecial" value="" />
                                <input type="hidden" id="idchecked" name="idchecked" value="0" />

              							</div>

              						</div>




              	<!--

              		IMPORTANT
              		The "action" hidden input is updated by javascript according to button params/action:
              			data-js-form-advanced-hidden-action-id="#action"
              			data-js-form-advanced-hidden-action-value="delete"

              		In your backend, should process data like this (PHP example):

              			if($_POST['action'] === 'delete') {

              				foreach($_POST['item_id'] as $item_id) {
              					// ... delete $item_id from database
              				}

              			}

              	-->
              	<input type="hidden" id="action" name="action" value=""><!-- value populated by js -->



              	<table class="table  bg-white shadow-primary-xs m-0" data-aos="fade-in" data-aos-delay="10" data-aos-offset="0">
              		<thead>
              			<tr>
                      <th class="font-weight-normal w--20"> </th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--50">IMG</th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--220 text-center">PRODUCT NAME</th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--150 text-center">PRICE</th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--150 text-center">SIZE</th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--150 text-center">STATUS</th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--60 text-align-end"> </th>
              			</tr>
              		</thead>

              		<!-- #item_list used by checkall: data-checkall-container="#item_list" -->
              		<tbody id="item_list">

                    <?php

                    while($row_special_products = mysqli_fetch_array($sql_array_products))
                    {
                       $i = $i+1;
                      $idspecialproduct  = $row_special_products["id"];
                      $productid = $row_special_products["productid"];
                      $productname = $row_special_products["productname"];
                      $productimage = $row_special_products["productimage"];
                      $productsize  = $row_special_products["productsize"];
                      $productcolor =  $row_special_products['productcolor'];
                      $productfeature = $row_special_products["productfeature"];
                      $productpriceprevio = $row_special_products["productprice"];
                      $productprice = number_format((float)$productpriceprevio,2);
                      $date_activacion = $row_special_products["date_activacion"];
                      $productsubcat = $row_special_products["productsubcat"];
                      $productspecial = $row_special_products["productspecial"];

                      ?>

              			<!-- product -->
              			<tr>

                      <!-- # -->
                      	<td class="text-left text-gray-500 fs--14">
                      <?php echo $i; ?>
                      </td>


              				<!-- image -->
              				<td>
              					<img src="https://app.freshlifefloral.com/<?php echo $productimage; ?>" class="img-fluid w--50 transition-hover-top" alt="<?php echo $productname; ?>">
              				</td>

              				<!-- product name -->
              				<td class="text-center">
              					<a href="#!" class="text-decoration-none">
              						<?php echo $productname; ?>
              					</a>
              					<span class="d-block text-muted fs--13"><?php echo $productsubcat; ?></span>

              				</td>

              				<!-- price -->
              			<td class="text-center">

              					<span class="d-block text-danger fs--15">
              						Price: <?php echo $productprice; ?>
              					</span>

              				</td>

              				<!-- brand -->
              				<td class="text-muted text-center">
              				<?php echo $productsize. " [cm]"; ?>
              				</td>

              				<!-- status -->
              				<td class="text-success text-center">
                        <label class="form-radio form-radio-success">
                        <input <?php if($productspecial==1){echo "checked";} ?> onclick="validarSpecial(<?php echo $i; ?>);" value="<?php echo $idspecialproduct ?>" type="radio" name="special<?php echo $i ?>" id="special<?php echo $i ?>">
                        <i></i>
                        </label>
              				</td>

              				<!-- options -->


              				<td class="text-align-end">
                        <a  href="!#" onclick="deleteItem(<?php echo $idspecialproduct; ?>)" class="js-ajax-confirm fs--13 btn btn-sm btn-soft btn-danger transition-hover-top row-pill mb-3 d-block-xs">
              										Delete
              									</a>


              			</tr>
              			<!-- /product -->

                  <?php } ?>

              		</tbody>

              		<tfoot>
              			<tr>
                      <th class="text-gray-500 font-weight-normal fs--14 w--80"> </th>
                      <th class="text-gray-500 font-weight-normal fs--14 w--80">IMG</th>
              				<th class="text-gray-500 font-weight-normal fs--14 w--240 text-center">PRODUCT NAME</th>
              				<th class="text-gray-500 font-weight-normal fs--14 w--100 text-center">PRICE</th>
              				<th class="text-gray-500 font-weight-normal fs--14 w--120 text-center">SIZE</th>
              				<th class="text-gray-500 font-weight-normal fs--14 w--100 text-center">STATUS</th>
              				<th class="text-gray-500 font-weight-normal fs--14 w--60 text-align-end"> </th>
              			</tr>
              		</tfoot>

              	</table>





              						</div>
                        </form>
              					</section><!-- /Primary -->




              				</div><!-- /MIDDLE -->


              			</div><!-- FOOTER -->



                          <!--Select Orders Modal Open-->
                          <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                              <div class="modal-dialog modal-md modal-md" role="document">
                                  <div class="modal-content">

                                      <!-- header modal -->
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span class="fi fi-close fs--18" aria-hidden="true"></span>
                                        </button>

                                      </div>
                                      <!-- body modal 3-->
                                      <form action="../en/florMP.php" method="post" id="payment-form">
                                      <div class="modal-body">
                                          <div class="table-responsive">

                                            <font color="#000">Please, before to continue select an order.</font><br><br>

                                            <div class="form-label-group mb-3">
                                            <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                                        <option value='0'>Select Previous Order</option>
                                                        <?php
                                                                $sel_order="select id , order_number ,del_date , qucik_desc
                                                                              from buyer_orders
                                                                             where del_date >= '" . date("Y-m-d") . "'
                                                                               and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                                $rs_order=mysqli_query($con,$sel_order);

                                                            while($orderCab=mysqli_fetch_array($rs_order))  {
                                                        ?>
                                                                <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                                        <?php
                                                            }
                                                           ?>
                                            </select>

                                            <label for="select_options">Select Previous Order</label>
                                             <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                                          </div>



                                                                  <br>
                                                                 <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                                          </div>



                                      </div>

                                      <div class="modal-footer request_product_modal_hide_footer">
                                          <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                      <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                                          <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                                      </div>
                                      </form>
                                  </div>
                              </div>
                          </div>


                          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
                          <script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>

              			<?php include('../back-end/inc/footer.php'); ?>


              			<script>

                    window.onload=function() {
                      contarproductos();
                    }

                    function verifyselect(){
                      var data = document.getElementById('s_product').value;
                      if(data==''){
                        document.getElementById('save_special_product').disabled = true;
                      }else{
                        contarproductos();
                      }

                    }

                    function validarSpecial(a)
                    {
                      var totalP = document.getElementById('totalspecial').value;
                      document.getElementById('idchecked').value = document.getElementById('special'+a).value;

                      for(var z=1; z<=totalP; z++){
                          if(z!=a){
                            document.getElementById('special'+z).checked = false;
                          }else{
                            document.getElementById('special'+z).checked = true;
                          }
                      }

                      var idcheck = document.getElementById('idchecked').value;
                      datos = "&idCHECK="+idcheck;

                      $.ajax({
                           type: "POST",
                           url: "updateSpecialProduct.php",
                           data: datos,
                           cache: false,
                           success: function(r){
                            // alert(r);
                             window.location.href = "customerwebshop.php";

                           }
                       });

                    }



                    function contarproductos(){

                      $.ajax({
                           type: "POST",
                           url: "count_special_product.php",
                           cache: false,
                           success: function(r)
                           {
                            // alert(r);
                            document.getElementById('totalspecial').value = r;
                             if(r>=7){
                               document.getElementById('save_special_product').disabled = true;
                             }else{
                               document.getElementById('save_special_product').disabled = false;
                             }
                           }
                       });
                    }

                    function checkOrderPrevious(){
              			      var orderP =  document.getElementById('selectPreviousOrder').value;
              			      if(orderP==0){
              			        document.getElementById('valueOrderId_MP').value = orderP;
              			        document.getElementById('orders_modal').disabled = true;
              			  }
              			      else{
              			        document.getElementById('valueOrderId_MP').value = orderP;
              			        document.getElementById('orders_modal').disabled = false;
              			        }
              			    }


        

                    function verifyselect1()
                    {
                      contarproductos();
                      var data = document.getElementById('s_product').value;
                      if(data==''){
                        document.getElementById('save_special_product').disabled = true;
                      }
                      if(data!=''){
                        var idcheck = document.getElementById('idchecked').value;
                        var valores = document.getElementById('s_product').value;
                        var valoresArray = valores.split("*");
                        var productid  = valoresArray[0];
                        var productimagen  = valoresArray[1];
                        var productfeature  = valoresArray[2];
                        var productsize  = valoresArray[3];
                        var productcolor  = valoresArray[4];
                        var productprice  = valoresArray[5];
                        var productacc  = valoresArray[6];
                        var productstatus  = valoresArray[7];
                        var productsubcat  = valoresArray[8];
                        datos = "productID="+productid+"&productIMG="+productimagen+"&productFEA="+productfeature+"&productSIZE="+productsize+"&productCOLOR="+productcolor+"&productPRICE="+productprice+"&productACC="+productacc+"&productSTATUS="+productstatus+"&productSUBCAT="+productsubcat+"&idCHECK="+idcheck;
                        //alert(datos);
                        //return false;
                        $.ajax({
                             type: "POST",
                             url: "saveNewSpecialProduct.php",
                             data: datos,
                             cache: false,
                             success: function(r){
                               window.location.href = "customerwebshop.php";

                             }
                         });
                      }
                    }

                    function deleteItem(a){
                      datos = "specialID="+a;
                      swal({
                             title: "Are you sure?",
                             text: "Once deleted, you will need to select again some product!",
                             icon: "warning",
                             buttons: true,
                             dangerMode: true,
                           })
                           .then((willDelete) =>
                           {
                             if (willDelete) {
                                 $.ajax({
                                      type: "POST",
                                      url: "del_special_product.php",
                                      data: datos,
                                      cache: false,
                                      success: function(r){

                                          window.location.href = "customerwebshop.php";

                                      }
                                  });
                             } else {
                               //Nothing to do!
                             }
                           });
                    }



              			</script>
