<?php
    // PO #1  2-jul-2018
	require_once("../config/config_gcp.php");

$menuoff = 1;
$page_id = 33;
$sub_id = 1;
$message = 0;


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}


$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,state,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image,handling_fees FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $state, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image, $handling_fees);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
//echo '<pre>';print_r($info);die;
$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
$today1 = explode(" ", $today);
$today_date = $today1["0"];
$time = $today1["1"];
$hours_array = explode(":", $time);

$hm = $hours_array[0] . ":" . $hours_array[1];
if (!isset($_POST["cartsubmit"]) && !isset($_GET["delete"]) && !isset($_POST["tp"])) {
    $sel_login_check = "select * from activity where buyer='" . $userID . "' and ldate='" . date("Y-m-d") . "' and ltime='" . $hm . "' and type='buyer' and atype='np'";
    $rs_login_check = mysqli_query($con, $sel_login_check);
    $login_check = mysqli_num_rows($rs_login_check);

    if ($login_check >= 1) {

    } else {
        $name = $first_name . " " . $last_name;
        $activity = "Name your pirce at " . $hm;
        $insert_login = "insert into activity set buyer='" . $userID . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='np'";
        mysqli_query($con, $insert_login);
    }
}
if (isset($_GET["delete"])) {
    $insert = "delete from offer_cart where id='" . $_GET["delete"] . "' and buyer='" . $_SESSION["buyer"] . "'";
    mysqli_query($con, $insert);
    $k = mysqli_affected_rows();
    if ($k == 1) {
        $message = 1;
        $name = $first_name . " " . $last_name;
        $activity = "Deleted offer from Name your pirce at " . $hm;
        $insert_login = "insert into activity set buyer='" . $userID . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='np'";
        mysqli_query($con, $insert_login);
    }
}

if (isset($_GET["idCategory"])) {
	$idCategory=$_GET["idCategory"];
}

?>

<?php
## CODE FROM OLD FILE AS REFERENCE ##
$sel_products = "select g.id from (SELECT  id,prodcutid,growerid,sizeid,feature FROM grower_product_box_packing) gb
        left join   product p on  p.id=gb.prodcutid 
        left join (select id,active from  growers ) g on  g.id =gb.growerid
        where g.active='active'  group by p.id,gb.sizeid,gb.feature ";
$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
    $from = (50 * $page) - 50;
    $display = 50;
} else {
    $page = 1;
    $from = 0;
    $display = 50;
}
$XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

/*
$query2 = "select gpb.prodcutid,gpb.id as gid,gpb.qty,gpb.sizeid,gpb.feature,gpb.growerid,p.id,p.subcategoryid,
                  p.name,p.color_id,p.image_path,s.name as subs,s.cat_id,g.file_path5,g.growers_name,sh.name as sizename,
                  ff.name as featurename,b.width,b.length,b.height,b.id as boxid,b.name as boxname,bs.id as bunchsizeid,
                  bs.name as bname,c.name as colorname 
             from grower_product_box_packing gpb
             left join product p      on gpb.prodcutid     = p.id
             left join subcategory s  on p.subcategoryid   = s.id  
             left join colors c       on p.color_id        = c.id 
             left join features ff    on gpb.feature       = ff.id
             left join sizes sh       on gpb.sizeid        = sh.id 
             left join boxes b        on gpb.box_id        = b.id
             left join growers g      on gpb.growerid      = g.id
             left join bunch_sizes bs on gpb.bunch_size_id = bs.id
            where g.active='active' 
              and p.name is not null 
              and sh.name is not null 
	      and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')";

					if (isset($idCategory)) {
						$query2 =$query2." and cat_id=".$idCategory;
					}
					
*/										
                
//$query2 = "select g.*,a.uname as person from growers g left join admin a on a.id=g.person_of_charge where active = 'active' order by g.growers_name";
$query2 = "select g.* from growers g where active = 'active' order by g.growers_name";
									
//mysqli_set_charset($con,"utf8");
$result2 = mysqli_query($con, $query2);


$getOrder = "select * from buyer_orders where id = '" . $orderId . "'";
$getOrderRes = mysqli_query($con, $getOrder);
$orderData = mysqli_fetch_assoc($getOrderRes);

$delDate = $orderData['del_date'];
$order_number = $orderData['order_number'];
$order_serial = $orderData['order_serial'];
$len = strlen($order_serial) + 1;
$order_serial = str_pad($order_serial, $len, '0', STR_PAD_LEFT);

$shippingMethod = $orderData['shipping_method'];
$getBuyerShippingMethod = "select * from buyer_shipping_methods where id ='" . $shippingMethod . "' ";
$buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
$buyerShippingMethodData = mysqli_fetch_assoc($buyerShippingMethodRes);

$shippingMethodDesc = '';
//print_r($buyerShippingMethodData);
if ($buyerShippingMethodData['shipping_method_id'] != 0) {
    $shippingMethodid = $buyerShippingMethodData['shipping_method_id'];
    $getShippingMethod = "select * from shipping_method where id = '" . $shippingMethodid . "'";
    $shippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);
    $shippingMethodDesc = $shippingMethodData['description'];
    $shippingMethodArray = unserialize($shippingMethodData['connections']);
    $firstConnection = $shippingMethodArray['connection_1'];
    $getConnectionType = "select type from connections where id='" . $firstConnection . "'";
    $conType = mysqli_query($con, $getConnectionType);
    $connectionsType = mysqli_fetch_assoc($conType);
    $connectionType = $connectionsType['type'];
    foreach ($shippingMethodArray as $connections) {
        $getConnections = "select charges_per_kilo,charges_per_shipment from connections where id='" . $connections . "'";
	
        $conDatas = mysqli_query($con, $getConnections);
        $connectionDatas = mysqli_fetch_assoc($conDatas);
        $cpk = unserialize($connectionDatas['charges_per_kilo']);
        foreach ($cpk as $perkilo) {
			
            $chargesPerKilo = $chargesPerKilo + $perkilo;
        }

        $cps = unserialize($connectionDatas['charges_per_shipment']);
        foreach ($cps as $pership) {
            $chargesPerShip = $chargesPerShip + $pership;
        }
    }
} else if (!empty($buyerShippingMethodData['choose_shipping'])) {
    $getMethod = "select * from shipping_method where shipping_type='" . $buyerShippingMethodData['choose_shipping'] . "'";
    $getMethodRes = mysqli_query($con, $getMethod);
    $methodDetail = mysqli_fetch_assoc($getMethodRes);
    $shipping_method_id = $methodDetail['id'];
    $getShippingMethod = "select connections from shipping_method where id='" . $shipping_method_id . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);
    $conCount = count($connections);
    //echo "<pre>";print_r($connections);echo "</pre>";
    foreach ($connections as $connection) {
        $getConnections = "select charges_per_kilo,charges_per_shipment from connections where id='" . $connection . "'";
        $conDatas = mysqli_query($con, $getConnections);
        $connectionDatas = mysqli_fetch_assoc($conDatas);
        $cpk = unserialize($connectionDatas['charges_per_kilo']);
        foreach ($cpk as $perkilo) {
            $chargesPerKilo = $chargesPerKilo + $perkilo;
        }

        $cps = unserialize($connectionDatas['charges_per_shipment']);
        foreach ($cps as $pership) {
            $chargesPerShip = $chargesPerShip + $pership;
        }
    }
} else {
    $chargesPerKilo = 0;
    $chargesPerShip = '';
}

if(strlen($chargesPerKilo) <= 0){
	$chargesPerKilo = 0;
}

function category()
{
    global $con;
    $category_sql = "SELECT id,name from category order by  name";
    $result_category = mysqli_query($con, $category_sql);
    $category_ret = "";
    while ($row_category = mysqli_fetch_assoc($result_category)) {
       // $category_ret .= "<option value='" . $row_category['id'] . "'>" . utf8_encode($row_category['name']) . "</option>";
        $category_ret .= "<option value='" . $row_category['id'] . "'>" . $row_category['name'] . "</option>";        
    }
    return $category_ret;
}

function product()
{
    global $con;
    $query = "SELECT id,name from product order  by  name";
    $result_product = mysqli_query($con, $query);
    $produc_result = "";
    while ($row_product = mysqli_fetch_assoc($result_product)) {
        $produc_result .= "<option value = '" . $row_product['id'] . "'>" . $row_product['name'] . "</option>";
    }
    return $produc_result;
}
function get_bunchs($product_id, $product_size, $buyer_id)
{
    global $con;
    $getbunches = mysqli_query($con, "SELECT gs.sizes,bs.name AS bunchname  
                                        FROM grower_product_bunch_sizes AS gs 
                                        LEFT JOIN bunch_sizes bs ON gs.bunch_sizes=bs.id 
                                       WHERE gs.grower_id = '" . $buyer_id . "' AND gs.product_id = '" . $product_id . "' 
                                         AND gs.sizes = '" . $product_size . "'");
    
    $rowbunches = mysqli_fetch_assoc($getbunches);
    return $rowbunches;
}

function colors()
{
    global $con;
    $colors_sql = "SELECT * from colors order by NAME ";
    $result_colors = mysqli_query($con, $colors_sql);
    return $result_colors;
}

$category = category();
$product = product();


?>
<?php include("../includes/profile-header.php"); ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/left_sidebar_buyer.php"); ?>
<style type="text/css">
    .not_set_border td {
        border: none !important;
    }

</style>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Report Growes</h1>
        <ol class="breadcrumb">
            <li><a href="#">Fresh Life Floral</a></li>
            <li class="active">Send your request to different growers</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">
                            <div id="panel-2" class="panel panel-default">
                                <!-- panel content -->
                                <div class="panel-body">

                                    <!-- date picker -->
                                    <input type="hidden"
                                           class="form-control datepicker picker1" <?php if (!empty($_SESSION['delDate'])) { ?> disabled="" value="<?php echo $_SESSION['delDate']; ?>" <?php } ?>
                                           start-date="<?php echo date('Y-m-d'); ?>" name="picker1" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false">
                                    <!-- range picker -->
                                    <input type="hidden" class="form-control rangepicker picker2"
                                           <?php if (!empty($_SESSION['dateRange'])) { ?>value="<?php echo $_SESSION['dateRange']; ?>"
                                           disabled=""<?php } ?> name="picker2" data-format="yyyy-mm-dd" data-from="2015-01-01" data-to="2016-12-31">
                                    <input type="hidden" id="shippingMethod"
                                           value="<?php echo (!empty($_SESSION['shippingMethod'])) ? $_SESSION['shippingMethod'] : '0'; ?>"/>
                                    <br>
                                    <!--barra  de  busqueda-->
                                    <form class="clearfix well well-sm search-big nomargin" action="/buyer/name-your-price.php" method="get">
                                        <div class="autosuggest fancy-form input-group" data-minLength="1" data-queryURL="<?php echo SITE_URL ?>/includes/autosuggest.php?limit=10&search=">
                                            <div class="input-group-btn">
                                                <button data-toggle="dropdown" class="btn btn-default input-lg dropdown-toggle noborder-right" type="button" aria-expanded="false">Filter <span class="caret"></span></button>
                                                <?php
                                                $sql_category = "select g.id   , g.growers_name from growers g left join admin a on a.id=g.person_of_charge order by g.growers_name";
                                                $result_cat = mysqli_query($con, $sql_category);
                                                ?>
                                                <ul class="dropdown-menu">
                                                    <li class="active"><a href="#"><i class="fa fa-check"></i> Grower</a></li>
                                                    <li class="divider"></li>
                                                    <?php
                                                    while ($options = mysqli_fetch_array($result_cat)) { ?>
                                                        <li><a href="<?php echo "name-your-price.php?idCategory=".$options['id'] ?>"><?php echo $options['growers_name'] ?></a></li>
                                                    <?php }

                                                    ?>


                                                </ul>
                                            </div>

                                            <input type="text" name="src" id="typeSearch" placeholder="Search From Here..." class="form-control typeahead"/>
                                            <!-- <i class="fa fa-arrow-down" id="on_click"></i> -->
                                            <div class="input-group-btn">
                                                <button class="btn btn-default input-lg noborder-left" type="submit">
                                                    <i class="fa fa-search fa-lg nopadding"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                    <!--filtros-->
                                    <div class="dataRequest">
                                        <?php
                                        $ii = 1;
                                        $tp = mysqli_num_rows($result2);
                                        ?>
                                        <input type="hidden" name="tp" id="tp" value="<?php echo $tp ?>">
                                        <input type="hidden" name="current" id="current" value="">
                                        <table class="table table-hover table-vertical-middle" id="example" style="margin-top:20px;">
                                            <thead>
                                            <tr>
                                                <!--<th>Photography</th>-->
                                                <th> </th>    
                                                <th>Farm</th>                                                
                                                <th> </th>                                                                                                
                                                <th>Pending Requests</th>                                             
                                                <th>Offer Status</th>                                                
                                                <!--<th>Confirmed</th> -->                                                                                               
                                            </tr>
                                            </thead>
                                            <tbody id="listing_product_s">
                                            <?php
 $sr=1;
                                            while ($products = mysqli_fetch_array($result2)) {
                                                
                                                   $query1 = "SELECT gpb.id AS cartid,id_order,order_serial,cod_order,gpb.product,sizeid,feature,noofstems,qty,buyer,
                                                                     gpb.boxtype,gpb.date_added,gpb.type,gpb.bunches,gpb.box_name,lfd,comment, box_id,shpping_method,isy,
                                                                     mreject,bunch_size,unseen,req_qty,bunches2 ,gpb.date_added ,p.name,
                                                                     p.color_id,p.image_path,s.name AS subs,sh.name AS sizename,
                                                                     ff.name AS featurename,rg.gprice AS price,rg.tax AS tax ,rg.shipping AS  cost_ship,rg.handling ,gor.offer_id ,gpb.inventary
                                                                FROM buyer_requests gpb
                                                               INNER JOIN request_growers rg     ON gpb.id=rg.rid
                                                                LEFT JOIN product p              ON gpb.product = p.id
                                                                LEFT JOIN subcategory s          ON p.subcategoryid=s.id  
                                                                LEFT JOIN features ff            ON gpb.feature=ff.id
                                                                LEFT JOIN grower_offer_reply gor ON rg.rid=gor.offer_id AND rg.gid=gor.grower_id
                                                                LEFT JOIN sizes sh               ON gpb.sizeid=sh.id                                                                                              
                                                               WHERE rg.gid='" . $products["id"] . "' AND gpb.lfd >='" . date("Y-m-d") . "'  and   IFNULL(gor.reject,'-1')<>'1'  
                                                                GROUP BY gpb.id ORDER BY gpb.id DESC";
                                                                                            
                                                   $result1 = mysqli_query($con,$query1);
                                                   //$result3 = mysqli_query($con,$query1);                                                   
                                                   $total_records = mysqli_num_rows($result1);   

                                                   $query4 = "select gor.boxqty            ,
                                                                     gor.boxtype           ,
                                                                     s.name as subs        ,
                                                                     p.name as product_name,
                                                                     sh.name as sizename   ,     
                                                                     gpb.comment           , 
                                                                     gpb.lfd               ,
                                                                     gor.cargo             , 
                                                                     gpb.buyer             ,
                                                                     bo.shipping_method    ,
                                                                     p.color_id            ,
                                                                     gpb.id as cartid
                                                                from buyer_requests gpb
                                                                left join product            p   on gpb.product     = p.id
                                                               inner join request_growers    rg  on gpb.id          = rg.rid
                                                               inner join buyer_orders       bo  on gpb.id_order    = bo.id          
                                                                left join subcategory        s   on p.subcategoryid = s.id  
                                                                left join features           ff  on gpb.feature     = ff.id
                                                                left join grower_offer_reply gor on gpb.id          = gor.offer_id 
                                                                left join sizes              sh  on gpb.sizeid      = sh.id 
                                                               where gor.grower_id='" . $products["id"] . "' 
                                                                 and gor.reject=0 
                                                                 and gor.status in (0,1) 
                                                                 and gor.id is not null 
                                                               group by gpb.id 
                                                               order by gpb.id desc";

                                                   $result4 = mysqli_query($con,$query4);
                                                   $result5 = mysqli_query($con,$query4);                                                   
                                                   $total_offer = mysqli_num_rows($result4);                                                      
                                                
                                                 $g = 1;                                                
                                                 $grower_cnt = 0;
                                                 $k = explode("/", $products["file_path5"]);
                                                ?>
                                                
                                                <!-- avatar -->
                                                <tr>
                                                   <td>
                                                    <div class="avter_left">
                                                    <img class="avatar-grower-buyer" alt="avatar"
                                                         src="<?php echo SITE_URL; ?>user/logo/<?php echo $k[1]; ?>"
                                                         class="avatar">
                                                    </div>
                                                    </td>

                                                    <td><span class="glyphicon glyphicon-grain" aria-hidden="true"></span><?php echo $products["growers_name"]; ?> </td>
                                                    <!--Variety-->
                                                    <td>

                                                    <?php 
							$select_farm_names = "select name from farms where id= '".$products["farms"]."'";
                                                        $rs_farm_names=mysqli_query($con,$select_farm_names);
							//while($farm_names=mysqli_fetch_array($rs_farm_names))	{
							//		echo $farm_names["name"];
							//}
                                                        
							$select_confirmed="select id,offer_id,product,boxqty ,date_added from grower_offer_reply where grower_id='" . $products["id"] . "' and status = '1'  ";
                                                        $total_confir = mysqli_num_rows($select_confirmed);                                                          
                                                     ?>
                                                    </td>

                                    <td><a data-toggle="modal" data-target="#open_offer_modal<?php echo $ii ?>" class="btn btn-success btn-xs relative">Pending<span
                                                    class="badge badge-dark badge-corner radius-0"><?php echo $total_records ; ?></span></a>

                                        <!--Requests Modal Start -->
                                        <div class="modal fade" id="open_offer_modal<?php echo $ii ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-full">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel"><?php echo "Buyer Requests  (".$products["growers_name"].")"; ?></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Product</th>
                                                                    <th>Comment</th>
                                                                    <th>Destiny</th>
                                                                    <th>LFD</th>                                                                    
                                                                    <th>Boxes</th>
                                                                    <th>Price</th>                                                                                                                                        
                                                                    <th>Action</th>                                                                     
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                    
                                                                <form>
                                                                    <?php
                                                                                       // $sr=1;
                                                                                                                                                        
                                                                             //while($prod=mysqli_fetch_assoc($result3))  {  
                                                                             while($prod=mysqli_fetch_array($result1))  {                                                                                          
                                                                                            
                                                                                        $sel_box_type = "SELECT * FROM boxtype WHERE id='" .$prod["boxtype"] . "'";
                                                                                        $rs_box_type = mysqli_query($con,$sel_box_type);
                                                                                        $box_type = mysqli_fetch_array($rs_box_type);
                                                                                        
                                                                                        $buyre_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE shipping_method_id='" . $prod['shpping_method'] . "'";
                                                                                        $rs_buyre_detail = mysqli_query($con,$buyre_detail_sql);
                                                                                        $row_buyre = mysqli_fetch_array($rs_buyre_detail);

                                                                                        $country_sql = "SELECT * FROM country WHERE id='" . $row_buyre['country'] . "'";
                                                                                        $rs_country_detail = mysqli_query($con,$country_sql);
                                                                                        $row_country = mysqli_fetch_array($rs_country_detail);                                                                                              
                                                                                        ?>
                                                                    
                                                                                <tr class="gradeU">
                                                                                            <td class="text" align="left"><?php  echo $prod["subs"]."  ".$prod["name"]."  ".$prod["sizename"]."  cm." ?></td>

                                                                                            <td class="text" align="left"><?php  echo $prod["comment"] ?></td>
                                                                                            
                                                                                            <td class="text" align="left">
                                                                                                <img src="<?php echo SITE_URL; ?>includes/assets/images/flags/<?php echo $row_country['flag'] ?>" width="25"/>
                                                                                            </td>                                                                                            

                                                                                            <td class="text" align="left"><?php  echo $prod["lfd"] ?></td>
                                                                                                                                                                                       
                                                                                            <td class="text" align="left"><?php  echo $prod["qty"]."  ".$box_type["name"] ?></td>
                                                                                            
                                                                                            <td>
														
                                                                                                    <?php if( $prod['price']>0){
                                                                                                                echo $prod['price'];
                                                                                                          }else{
                                                                                                                echo "Open bid";
                                                                                                          }
														
							?></td>
                                                                                            
                                                                                            
							    <!--Action Inicio Po-->  
                                                            
                                                            
							    <!--Action-->
							<td>							
							    <?php 
								if ($prod['price'] > 0 && $prod['inventary'] >0) { ?>
                                                                        <button type="button" class="btn btn-xs btn-success" onclick="modifyOfferxConfir(<?php echo $prod["cartid"] ?>)" >Confirm</button>

                                                            <?php }?>
													
                                                            <?php   if ($prod['price'] > 0 && $prod['inventary'] < 1 ) { ?>
                                                                        <a data-toggle="modal" data-target="#modify_modal_x<?php echo $sr ?>" class="btn btn-xs btn-success">Send Offer</a>
                                                            <?php   } else { if ($prod['inventary'] <1) {?>
                                                                        <a data-toggle="modal" data-target="#modify_modal_<?php echo $sr ?>" class="btn btn-xs btn-success">Send Offer</a>
                                                            <?php } } ?>
                                                                
                                                            <!-- Modify Modal >-->

                                                            <!-- Modify Modal >-->
                                                            
                                                            <div class="modal fade" id="modify_modal_<?php echo $sr; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <?php
                                                                        $rowbunches = get_bunchs($prod['product'], $prod['sizeid'], $prod['buyer']);
                                                                        ?>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X----</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">
                                                                                <?php echo $prod["qty"] . " " . $box_type["name"]; ?>
                                                                                <?php echo $prod["subs"] ?> <?php echo $prod["name"] ?> <?php echo $prod["featurename"] ?> <?php echo $prod["sizename"] ?>cm <?php echo $rowbunches['bunchname'] . "st/bu"; ?></h4>
                                                                        </div>                                                                        
<!-- body modal -->
                                                                        <div class="modal-body"><?php //echo '<pre>'; print_r($prod);     ?>
                                                                            <div class="row">
                                                                                <ul class="nav nav-tabs nav-clean">
                                                                                    <li class="dropdown active">
                                                                                        <a data-toggle="dropdown" id="offer_dropdown<?php echo $s; ?>" class="dropdown-toggle" href="#" aria-expanded="false">Offers<span class="caret"></span></a>
                                                                                        <ul class="dropdown-menu" id="offer_ul_s<?php echo $s; ?>">
                                                                                            <?php
                                                                                            $cnt_offer = 1;
                                                                                            if ($prod['offer_id'] != "") {
                                                                                                $offer_sql = "SELECT * FROM grower_offer_reply WHERE offer_id='" . $prod['offer_id'] . "' GROUP BY offer_id_index";
                                                                                                $rs_offer = mysqli_query($con, $offer_sql);
                                                                                                $rs_number_of_row = mysqli_num_rows($rs_offer);

                                                                                                if ($rs_number_of_row > 0) {
                                                                                                    while ($offer_info = mysqli_fetch_array($rs_offer)) {
                                                                                                        ?>
                                                                                                        <li>
                                                                                                            <a data-toggle="tab"
                                                                                                               onclick="getOfferDetailsPopup(<?php echo $offer_info['offer_id']; ?>,<?php echo $offer_info['offer_id_index']; ?>,<?php echo $sr; ?>,<?php echo $prod['product']; ?>,<?php echo $prod['feature']; ?>,<?php echo $prod['sizename']; ?>,<?php echo $prod['qty']; ?>,<?php echo $prod['boxtype']; ?>,<?php echo $prod['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                               href="#" aria-expanded="false">
                                                                                                                <?php echo "Offer" . $offer_info['offer_id_index']; ?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <?php
                                                                                                        $cnt_offer++;
                                                                                                    }
                                                                                                }
                                                                                            } ?>
                                                                                            <li>
                                                                                                <a data-toggle="tab" tabindex="-1" href="#"
                                                                                                   onclick="defaultOffer(<?php echo $prod['cartid']; ?>,<?php echo $cnt_offer; ?>,<?php echo $sr; ?>,<?php echo $prod['product']; ?>,<?php echo $prod['feature']; ?>,<?php echo $prod['sizename']; ?>,<?php echo $prod['qty']; ?>,<?php echo $prod['boxtype']; ?>,<?php echo $prod['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                   aria-expanded="false">
                                                                                                    <i class="fa fa-plus"></i> Add Offer
                                                                                                </a>
                                                                                            </li>

                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class=""><a data-toggle="tab" href="#tab_b_<?php echo $sr; ?>" aria-expanded="false">Order Details</a></li>

                                                                                </ul>    
                                                                                <!-- tabs content -->
                                                                                <div class="col-md-12 col-sm-12 nopadding">
                                                                                    <div class="tab-content tab-stacked">
                                                                                        <div id="tab_a_<?php echo $s; ?>" class="tab-pane active">
                                                                                            <!-- classic select2 -->
                                                                                            <?php
                                                                                            $sql_boxes = "SELECT  boxes FROM  growers WHERE  id ='" . $products["id"] . "'";
                                                                                            $rs_boxes = mysqli_query($con, $sql_boxes);
                                                                                            $boxes = mysqli_fetch_array($rs_boxes);
                                                                                            $a = substr($boxes['boxes'], 0, -1);
                                                                                            $a = substr($a, 1);

                                                                                            $sel_boxes = " SELECT b.id,b.name,b.width,b.length,b.height ,bo.name AS  name_box FROM boxes b                                                                                                                                                                                        
                                                                                                             LEFT JOIN  boxtype bo ON  b.type=bo.id 
                                                                                                            WHERE  b.id IN(" . $a . ") AND   bo.name='" . $box_type["name"] . "'";

                                                                                            $rs_boxes = mysqli_query($con, $sel_boxes);
                                                                                            
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="request_qty_box" id="request_qty_box_<?php echo $sr; ?>"
                                                                                                    onchange="boxQuantityChange(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>)" style="width:300px;">
                                                                                                <option value="">Select Box Quantity</option>
                                                                                                <?php
                                                                                                $request_qty = $prod["qty"];
                                                                                                while ($box_sel = mysqli_fetch_array($rs_boxes)) {
                                                                                                    for ($i1 = 1; $i1 <= $request_qty; $i1++) {
                                                                                                        ?>
                                                                                                        <option value="<?php echo $i1 . "-" . $box_sel['width'] * $box_sel['length'] * $box_sel['height']; ?>">
                                                                                                            <?php echo $i1 . " " . $box_sel['name_box'] . " " . $box_sel['width'] . "x" . $box_sel['length'] . "x" . $box_sel['height']; ?>
                                                                                                        </option>
                                                                                                    <?php }
                                                                                                } ?>

                                                                                            </select>
                                                                                            <input type="hidden" class="cls_hidden_selected_qty" name="cls_hidden_selected_qty" value=""/>
                                                                                            <form name="sendoffer" id="sendoffer<?php echo $prod["cartid"] ?>" method="post">
                                                                                                <input type="hidden" name="mainprice" id="mainprice" value=" <?php
                                                                                                if ($prod["price"] != "0.00") {
                                                                                                    echo $prod["price"];
                                                                                                }
                                                                                                ?> "/>
                                                                                                <!--Inside Table-->
                                                                                                <input type="hidden" name="offer" id="offer" value="<?php echo $prod["cartid"] ?>">
                                                                                                <input type="hidden" name="buyer" id="buyer" value="<?php echo $prod["buyer"] ?>">
                                                                                                <input type="hidden" name="product" id="product" value="<?php echo $prod["name"] ?> <?php echo $prod["featurename"] ?>">
                                                                                                <input type="hidden" name="product_subcategory" id="product_subcategory" value="<?php echo $prod["subs"] ?>">
                                                                                                <input type="hidden" name="product_ship" id="product_ship" value="1">
                                                                                                <input type="hidden" name="shipp" id="shipp" value="<?php echo $prod["shpping_method"] ?>">
                                                                                                <input type="hidden" name="product_su" id="product_su" value="1">
                                                                                                <input type="hidden" name="volume" id="volume" value="1">

                                                                                                <?php
                                                                                                $prod["boxtype"];
                                                                                                $temp = explode("-", $prod["boxtype"]);
                                                                                                $box_type_id = $temp[0];
                                                                                                $sel_box_ids = "SELECT bt.id AS boxtypeid,bt.name AS boxtype,bo.width,bo.length,bo.height,bo.name AS box_value,
                                                                                                                       go.grower_id,go.boxes FROM grower_product_box go 
                                                                                                             LEFT JOIN boxes bo   ON bo.id=go.boxes
                                                                                                             LEFT JOIN boxtype bt ON bo.type=bt.id
                                                                                                            WHERE go.grower_id='" . $products["id"] . "' AND bt.id ='" . $box_type_id . "' GROUP BY bo.name";
                                                                                                
                                                                                                $rs_box_ids = mysqli_query($con, $sel_box_ids);
                                                                                                $totalboxtype = mysqli_num_rows($rs_box_ids);
                                                                                                
                                                                                                $i2 = 1;
                                                                                                while ($box_ids = mysqli_fetch_array($rs_box_ids)) {
                                                                                                    $sel_weight = "SELECT weight FROM grower_product_box_weight WHERE growerid='" . $products["id"] . "' AND  box_id='" . $box_ids["boxes"] . "'";
                                                                                                    $rs_weight = mysqli_query($con, $sel_weight);
                                                                                                    $weight = mysqli_fetch_array($rs_weight);
                                                                                                    $optionbb[$i2] .= $box_ids["boxtype"] . "-" . $box_ids["box_value"] . "-" . ($box_ids["length"] * $box_ids["height"] * $box_ids["width"]) . "-" . $weight["weight"];
                                                                                                    $i2++;
                                                                                                }

                                                                                                ?>
                                                                                                <input type="hidden" name="boxtype-<?php echo $prod["cartid"] ?>" id="boxtype-<?php echo $prod["cartid"] ?>" value="<?php echo $optionbb[1]; ?>">
                                                                                                <div class="table-responsive" style="margin-top:20px;">
                                                                                                    <!--<table class="table table-bordered table-vertical-middle" id="main_table_offer_<?php echo $sr; ?>">
                                                                                                    </table>-->
                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_<?php echo $sr; ?>">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Product</th>
                                                                                                            <th>Bunch Quantity</th>
                                                                                                            <th>Price</th>
                                                                                                            <th>Column name</th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <?php $a = 0; ?>
                                                                                                        <tr id="product_tr_0">
                                                                                                            <?php $a = $a + 1; ?>
                                                                                                            <!--Product-->
                                                                                                            <td>
                                                                                                                <?php
                                                                                                                $sql_box_ids3 = "SELECT  gr.id AS id,gr.box_id ,b.type,b.name,gs.sizes,gs.bunch_value,gs.is_bunch,gs.is_bunch_value,s.name AS size_name FROM grower_product_bunch_sizes gs
                                                                                                                    LEFT JOIN grower_product_box_packing  gr
                                                                                                                    ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
                                                                                                                    LEFT JOIN sizes AS s ON gs.sizes=s.id
                                                                                                                    LEFT JOIN boxes AS b ON b.id=gr.box_id
                                                                                                                    WHERE  gs.grower_id='" . $products["id"] . "'  AND gs.product_id='" . $prod['product'] . "'AND b.type='" . $prod['boxtype'] . "'
                                                                                                                    ORDER BY s.name";

                                                                                                                $sel_box_ids3 = mysqli_query($con, $sql_box_ids3);
                                                                                                                ?>
                                                                                                                <select class="form-controll disabled-css offer-select" name="productsize[]" disabled
                                                                                                                        onchange="changeCMSize(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>,<?php echo $prod['boxtype']; ?>,
                                                                                                                        <?php echo $userSessionID; ?>,<?php echo $prod["product"]; ?>,<?php echo $prod["sizeid"]; ?>)"
                                                                                                                        id="productsize-<?php echo $prod["cartid"] ?>-0">
                                                                                                                    <option value="">Select Product</option>
                                                                                                                    <?php

                                                                                                                    while ($box_ids3 = mysqli_fetch_array($sel_box_ids3)) {
                                                                                                                        $bunch_value_s = "";
                                                                                                                        if ($box_ids3['is_bunch'] == "0") {
                                                                                                                            $bunch_value_s = $box_ids3['bunch_value'];
                                                                                                                        } else {
                                                                                                                            $bunch_value_s = $box_ids3['is_bunch_value'];
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        <!--aqui  estoy  enviando  el  id-->
                                                                                                                        <option value="<?php echo $box_ids3["id"] ?>" size_id="<?php echo $box_ids3["sizes"]; ?>"
                                                                                                                                cod_id="<?php echo $box_ids3["id"]; ?>"> <?php echo $box_type["name"] ?> <?php echo $prod["subs"] ?> <?php echo $prod["name"] ?> <?php echo $prod["featurename"] ?><?php echo $box_ids3["size_name"] ?>
                                                                                                                            cm <?php echo $bunch_value_s . "st/bu"; ?></option>
                                                                                                                        <?php
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                            </td>
                                                                                                            <!--Bunch Quantity-->
                                                                                                            <td>
                                                                                                                <select class="form-controll disabled-css offer-select" disabled
                                                                                                                        onchange="changeBunchQty(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>)" name="bunchqty[]"
                                                                                                                        id="bunchqty-<?php echo $prod["cartid"] ?>-0">
                                                                                                                    <option value="">Select Bunch QTY</option>
                                                                                                                    <?php
                                                                                                                    for ($i3 = 1; $i3 <= 100; $i3++) {
                                                                                                                        ?>
                                                                                                                        <option value="<?php echo $i3 ?>"><?php echo $i3 ?></option>
                                                                                                                    <?php } ?>
                                                                                                                </select>

                                                                                                            </td>
                                                                                                            <!--Price-->
                                                                                                            <td>
                                                                                                                <label id="price_label_<?php echo $prod["cartid"] ?>-0" style="display: none;"><?php echo substr($real_product_price, 0, 4); ?></label>
                                                                                                                <select style="display: block;" name="price[]" disabled id="price-<?php echo $prod["cartid"] ?>-0"
                                                                                                                        class="form-controll modal_option disabled-css offer-select">
                                                                                                                    <option value="">Select Price</option>
                                                                                                                    <?php
                                                                                                                    for ($i4 = 10; $i4 <= 90; $i4++) { ?>
                                                                                                                        <option value="0.<?php echo $i4; ?>">$0.<?php echo $i4; ?></option>
                                                                                                                    <?php }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                            </td>
                                                                                                            <!--Column name-->
                                                                                                            <td>
                                                                                                                <input type="hidden" class="boxqty<?php echo $s; ?>" name="boxqty[]" id="boxqty_0_<?php echo $sr; ?>_<?php echo $prod["cartid"]; ?>" value=""/>
                                                                                                                <input type="hidden" name="box[]" id="box" value="$sr"/>
                                                                                                                <input type="hidden" name="total_qty_bunches[]" id="total_qty_bunches_0_<?php echo $sr; ?>_<?php echo $prod["cartid"]; ?>" value="0"/>
                                                                                                                <a class="add_new_row" id="add_variety_0_<?php echo $prod["cartid"]; ?>_<?php echo $sr; ?>"
                                                                                                                   onclick="addNewVariety(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>,<?php echo $prod['boxtype']; ?>,<?php echo $userSessionID; ?>,<?php echo $products["product"]; ?>,<?php echo $products["sizeid"]; ?>)"
                                                                                                                   href="javascript:void(0);"><span class="label label-success">Add Variety </span> </a>&nbsp;
                                                                                                                <a href='javascript:void(0)' id='edit_variety_0_<?php echo $prod["cartid"]; ?>_<?php echo $sr; ?>'
                                                                                                                   onclick='editVariety(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-edit white"></i> Edit </a>
                                                                                                                <a href='javascript:void(0)' id='delete_variety_0_<?php echo $prod["cartid"]; ?>_<?php echo $sr; ?>'
                                                                                                                   onclick='deleteVariety(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-times white"></i> Delete </a>


                                                                                                            </td>
                                                                                                        </tr>


                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <input type="hidden" name="total_a" id="total_a" value="1"/>
                                                                                                    <input type="hidden" name="total_availabel_product" id="total_availabel_product_<?php echo $sr; ?>" value=""/>
                                                                                                    <div class="col-lg-4">
                                                                                                        <div class="error-box">
                                                                                                            <span id="errormsg-<?php echo $prod["cartid"] ?>" style="color:#FF0000; font-size:12px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                                                        </div>
                                                                                                        <div class="heading-title heading-border-bottom">
                                                                                                            <h3 style="background-color:transparent!important;">Box Competition</h3>
                                                                                                        </div>

                                                                                                        <div class="progress progress-lg"><!-- progress bar -->
                                                                                                            <div class="progress-bar progress-bar-warning progress-bar-striped active text-left" role="progressbar" id="progressbar_id_<?php echo $sr; ?>"
                                                                                                                 aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                                                                                <span>Completed 0%</span>
                                                                                                            </div>
                                                                                                        </div><!-- /progress bar -->
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Inside Table-->
                                                                                                <input type="hidden" name="hdn_offer_id_index" id="hdn_offer_id_index" value="<?php echo $cnt_offer; ?>"/>
                                                                                            </form>
                                                                                        </div>

                                                                                        <div id="tab_b_<?php echo $sr; ?>" class="tab-pane">

                                                                                            <!--Inside Table-->
                                                                                            <div class="table-responsive" id="order_details">
                                                                                                <?php
                                                                                                $buyre_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE id='" . $prod['shpping_method'] . "'";
                                                                                                $rs_buyre_detail = mysqli_query($con, $buyre_detail_sql);
                                                                                                $row_buyre = mysqli_fetch_array($rs_buyre_detail);

                                                                                                $country_sql = "SELECT * FROM country WHERE id='" . $row_buyre['country'] . "'";
                                                                                                $rs_country_detail = mysqli_query($con, $country_sql);
                                                                                                $row_country = mysqli_fetch_array($rs_country_detail);

                                                                                                ?>

                                                                                                <ul class="list-unstyled">
                                                                                                    <div style="float:left;width:300px;"><i class="fa fa-map-marker abs"></i>
                                                                                                        <li class="footer-sprite address">
                                                                                                            <?php echo $row_buyre['first_name'] . " " . $row_buyre['last_name'] ?>,<br><?php echo $row_buyre['address']; ?>,
                                                                                                            <?php
                                                                                                            if ($row_buyre['address2'] != "") {
                                                                                                                echo $row_buyre['address2'] . ",";
                                                                                                            }
                                                                                                            ?>
                                                                                                            <?php
                                                                                                            if ($row_buyre['state'] != "") {
                                                                                                                ?>
                                                                                                                <br><?php echo $row_buyre['state']; ?>,
                                                                                                            <?php }
                                                                                                            if ($row_country['name']) {
                                                                                                                echo $row_country['name'] . "<br>";
                                                                                                            } ?>


                                                                                                        </li>
                                                                                                    </div>
                                                                                                    <?php
                                                                                                    $shipping_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE id='" . $prod['shpping_method'] . "'";
                                                                                                    $rs_shipping_detail = mysqli_query($con, $shipping_detail_sql);
                                                                                                    $row_shipping = mysqli_fetch_array($rs_shipping_detail);

                                                                                                    $cargo_agency_sql = "SELECT * FROM cargo_agency WHERE id='" . $row_shipping['cargo_agency_id'] . "'";
                                                                                                    $rs_cargo_agency = mysqli_query($con, $cargo_agency_sql);
                                                                                                    $row_cargo_agency = mysqli_fetch_array($rs_cargo_agency);
                                                                                                    ?>
                                                                                                    <div style="float:left;width:1200px;">
                                                                                                        <i class="fa fa-user  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Client: <?php echo $row_buyre['first_name'] . " " . $row_buyre['last_name'] ?>
                                                                                                        </li>
                                                                                                        <i class="fa fa-plane abs"></i>
                                                                                                        <li class="footer-sprite email">
                                                                                                            Cargo Agency: <?php
                                                                                                            if ($row_cargo_agency['name'] != "") {
                                                                                                                echo $row_cargo_agency['name'];
                                                                                                            } else {
                                                                                                                echo "-";
                                                                                                            }
                                                                                                            ?>
                                                                                                        </li>
                                                                                                        <i class="fa fa-phone  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Coordination : Flora and Design
                                                                                                        </li>
                                                                                                        <i class="fa fa-pencil-square-o  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Marks: Fresh Life Floral - <?php echo $row_shipping['shipping_code']; ?>
                                                                                                        </li>

                                                                                                    </div>

                                                                                                </ul>
                                                                                                <a href="#" class="btn btn-3d btn-green"><i class="fa fa-comments-o"></i>Comments: <?php echo $product_comment; ?></a>
                                                                                            </div>
                                                                                            <!--Inside Table-->

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                 <!-- tabs content end-->
                                                                                
                                                                                
                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <?php //echo "Total Bunches : ".$total_qty_bunches; ?>
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer noborder offer_model_popup">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <a href="javascript:void(0);" onclick="modifyOffer(<?php echo $prod["cartid"] ?>)" class="btn btn-primary">Send Offer</a>
                                                                        </div>                                                                        
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>                                                                        
                                                            <!---->
                                                            <!--po2 -->
                                                            <div class="modal fade" id="modify_modal_x<?php echo $sr; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-medium" style="width: 1000px">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <?php


                                                                        $query_bon = "SELECT gs.sizes,bp.qty,bs.name AS bunchname  
                                                                                        FROM grower_product_bunch_sizes AS gs 
                                                                                   LEFT JOIN bunch_sizes bs                ON gs.bunch_sizes=bs.id 
                                                                                   LEFT JOIN grower_product_box_packing bp ON bp.growerid=gs.grower_id AND bp.prodcutid=gs.product_id AND bp.sizeid=gs.sizes AND   bs.id= bp.bunch_size_id
                                                                                   LEFT JOIN  boxes  bo                    ON bp.box_id =bo.id
                                                                                  WHERE gs.grower_id = '" . $products["id"] . "' AND gs.product_id = '" . $prod['product'] . "' 
                                                                                    AND gs.sizes = '" . $prod['sizeid'] . "'
                                                                                    AND bo.type='" . $prod['boxtype'] . "' ";
                                                                        
                                                                        $getbunches = mysqli_query($con, $query_bon);
                                                                        $rowbunches = mysqli_fetch_assoc($getbunches);

                                                                        ?>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X(------)</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">
                                                                                <?php echo $prod["qty"] . " " . $box_type["name"]; ?>
                                                                                <?php echo $prod["subs"] ?> <?php echo $prod["name"] ?> <?php echo $prod["featurename"] ?> <?php echo $prod["sizename"] . " cm" ?> <?php echo $rowbunches['qty'] . "Bu x"; ?><?php echo $rowbunches['bunchname'] . "st"; ?></h4>
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body"><?php //echo '<pre>'; print_r($prod);     ?>
                                                                            <div class="row">
                                                                                <ul class="nav nav-tabs nav-clean">
                                                                                    <li class="dropdown active">
                                                                                        <a data-toggle="dropdown" id="offer_dropdown<?php echo $sr; ?>" class="dropdown-toggle" href="#" aria-expanded="false">Offers<span class="caret"></span></a>
                                                                                        <ul class="dropdown-menu" id="offer_ul_s<?php echo $sr; ?>">
                                                                                            <?php
                                                                                            $cnt_offer = 1;
                                                                                            if ($prod['offer_id'] != "") {
                                                                                                $offer_sql = "SELECT * FROM grower_offer_reply WHERE offer_id='" . $prod['offer_id'] . "' GROUP BY offer_id_index";
                                                                                                $rs_offer = mysqli_query($con, $offer_sql);
                                                                                                $rs_number_of_row = mysqli_num_rows($rs_offer);

                                                                                                if ($rs_number_of_row > 0) {
                                                                                                    while ($offer_info = mysqli_fetch_array($rs_offer)) {
                                                                                                        ?>
                                                                                                        <li>
                                                                                                            <a data-toggle="tab"
                                                                                                               onclick="getOfferDetailsPopup(<?php echo $offer_info['offer_id']; ?>,<?php echo $offer_info['offer_id_index']; ?>,<?php echo $sr; ?>,<?php echo $prod['product']; ?>,<?php echo $prod['feature']; ?>,<?php echo $prod['sizename']; ?>,<?php echo $prod['qty']; ?>,<?php echo $prod['boxtype']; ?>,<?php echo $prod['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                               href="#" aria-expanded="false">
                                                                                                                <?php echo "Offer" . $offer_info['offer_id_index']; ?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <?php
                                                                                                        $cnt_offer++;
                                                                                                    }
                                                                                                }
                                                                                            } ?>
                                                                                            <li>
                                                                                                <a data-toggle="tab" tabindex="-1" href="#"
                                                                                                   onclick="defaultOffer(<?php echo $prod['cartid']; ?>,<?php echo $cnt_offer; ?>,<?php echo $sr; ?>,<?php echo $prod['product']; ?>,<?php echo $prod['feature']; ?>,<?php echo $prod['sizename']; ?>,<?php echo $prod['qty']; ?>,<?php echo $prod['boxtype']; ?>,<?php echo $prod['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                   aria-expanded="false"><i class="fa fa-plus"></i> Add Offer
                                                                                                </a>
                                                                                            </li>

                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class=""><a data-toggle="tab" href="#tab_b_<?php echo $sr; ?>" aria-expanded="false">Order Details</a></li>

                                                                                </ul>
                                                                                <!-- tabs content -->
                                                                                <div class="col-md-12 col-sm-12 nopadding">
                                                                                    <div class="tab-content tab-stacked">
                                                                                        <div id="tab_a_x_<?php echo $sr; ?>" class="tab-pane active">
                                                                                            <!-- classic select2 -->
                                                                                            <?php
                                                                                            $sql_boxes = "SELECT  boxes FROM  growers WHERE  id ='" . $products["id"] . "'";
                                                                                            $rs_boxes = mysqli_query($con, $sql_boxes);
                                                                                            $boxes = mysqli_fetch_array($rs_boxes);
                                                                                            $a = substr($boxes['boxes'], 0, -1);
                                                                                            $a = substr($a, 1);
                                                                                            $sel_boxes = " SELECT b.id,b.name,b.width,b.length,b.height ,bo.name AS  name_box FROM boxes b 
                                                                                                             LEFT JOIN  boxtype bo ON  b.type=bo.id 
                                                                                                            WHERE  b.id IN(" . $a . ") AND   bo.name='" . $box_type["name"] . "'";
                                                                                            
                                                                                            $rs_boxes = mysqli_query($con, $sel_boxes);
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="request_qty_box_x" id="request_qty_box_x_<?php echo $sr; ?>"
                                                                                                    onchange="boxQuantityChanges(0,<?php echo $prod["cartid"]; ?>,<?php echo $sr; ?>,<?php echo $rowbunches['qty'] ?>)" style="width:300px;">
                                                                                                <option value="">Select Box Quantity</option>
                                                                                                <?php
                                                                                                $request_qty = $prod["qty"];
                                                                                                while ($box_sel = mysqli_fetch_array($rs_boxes)) {
                                                                                                    for ($i5 = 1; $i5 <= $request_qty; $i5++) {
                                                                                                        ?>
                                                                                                        <option value="<?php echo $i5 . "-" . $box_sel['width'] * $box_sel['length'] * $box_sel['height'] . "-" . $box_sel['name']; ?>">
                                                                                                            <?php echo $i5 . " " . $box_sel['name_box'] . " " . $box_sel['width'] . "x" . $box_sel['length'] . "x" . $box_sel['height']; ?>
                                                                                                        </option>
                                                                                                    <?php }
                                                                                                } ?>
                                                                                            </select>
                                                                                            <input type="hidden" name="cls_hidden_selected_qty_x" value=""/>
                                                                                            <form name="sendof" id="sendof<?php echo $prod["cartid"] ?>">
                                                                                                <input type="hidden" name="offer_x" id="offer_x" value="<?php echo $prod["cartid"] ?>">
                                                                                                <input type="hidden" name="buyer_x" id="buyer_x" value="<?php echo $prod["buyer"] ?>">
                                                                                                <input type="hidden" name="product_x" id="product_x" value="<?php echo $prod["name"] ?> <?php echo $prod["featurename"] ?>">
                                                                                                <input type="hidden" name="product_subcategory_x" id="product_subcategory_x" value="<?php echo $prod["subs"] ?>">
                                                                                                <input type="hidden" name="product_ship_x" id="product_ship_x" value="1">
                                                                                                <input type="hidden" name="shipp_x" id="shipp_x" value="<?php echo $prod["shpping_method"] ?>">
                                                                                                <input type="hidden" name="product_su_x" id="product_su_x" value="1">
                                                                                                <input type="hidden" name="price_x" id="price_x" value="<?php echo $prod["price"] ?>">
                                                                                                <input type="hidden" name="volume_x" id="volume_x" value="1">
                                                                                                <input type="hidden" name="tot_bu_x" id="tot_bu_x" value="0">
                                                                                                <input type="hidden" name="stems_x" id="stems_x" value="<?php echo $rowbunches['bunchname'] ?>">
                                                                                                <input type="hidden" name="tax_x" id="tax_x" value="<?php echo $prod['tax'] ?>">
												<input type="hidden" name="inventary" id="inventary" value="<?php echo $prod['inventary'] ?>">
                                                                                                <input type="hidden" name="ship_cost_x" id="ship_cost_x" value="<?php echo $prod['cost_ship'] ?>">
                                                                                                <input type="hidden" name="handling_x" id="handling_x" value="<?php echo $prod['handling'] ?>">
                                                                                                <input type="hidden" name="box_type_x" id="box_type_x" value="<?php echo $box_type["name"] ?>">
                                                                                                <input type="hidden" name="sizename_x" id="sizename_x" value="<?php echo $prod["sizename"] ?>">
                                                                                                <input type="hidden" name="box_volumn_x" id="box_volumn_x" value="0">
                                                                                                <input type="hidden" name="box_name" id="box_name" value="">
                                                                                                <input type="hidden" class="boxqty<?php echo $sr;?>" name="boxqty[]" id="boxqty_0_<?php echo $sr;?>_<?php echo $prod["cartid"];?>" value=""/>
                                                                                                <input type="hidden" name="code_x" id="code_x" value="<?php echo substr($prod["cod_order"], 0, 4) ?>">   
                                                                                                <?php
                                                                                                $jj = 1;
                                                                                                ?>
                                                                                                <div class="table-responsive" style="margin-top:20px;">
                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_offer_x_<?php echo $sr; ?>">
                                                                                                    </table>
                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_x_<?php echo $sr; ?>">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Product</th>
                                                                                                            <th>St/Bu</th>
                                                                                                            <th>Total Bunch Quantity</th>
                                                                                                            <th>Price</th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <?php $a = 0; ?>
                                                                                                        <tr id="product_tr_0_0">
                                                                                                            <td><?php echo $prod["subs"] ?> <?php echo $prod["name"] ?> <?php echo $prod["featurename"] ?> <?php echo $prod["sizename"] . " cm " ?></td>
                                                                                                            <td><?php echo $rowbunches['bunchname'] . "st/bu"; ?></td>
                                                                                                            <td><input type="text" name="setear" id="setear-<?php echo $sr?>" disabled></td>
                                                                                                            <td><label id="price_label_<?php echo $prod["cartid"] ?>-0" style="display: none;"><?php echo substr($real_product_price, 0, 4); ?>
                                                                                                                </label><?php echo $prod["price"]; ?></td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer noborder offer_model_popup">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <a href="javascript:void(0);" onclick="modifyOfferx(<?php echo $prod["cartid"] ?>)" class="btn btn-success">Send Offer</a>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--po2-->

                                                            
                                                            <a href="javascript:void(0);" onclick="rejectOffer('<?php echo $prod["cartid"]; ?>', '<?php echo $prod["buyer"]; ?>','<?php echo $products["id"]; ?>')" class="btn-xs btn btn-danger">Reject</a>                                                                                                                       
                                                        </td>                                                            
                                                                                                                
							    <!--Action Fin Po-->                                                            
                                                                                </tr>
                                                                                        <?php 

                                                                                        $sr++;

                                                                                        }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                         </td>                                         
                                        <!--Requests Modal End-->
                                                            
                                        <td><a data-toggle="modal" data-target="#open_status_modal<?php echo $ii ?>" class="btn btn-success btn-xs relative">Offer<span
                                                    class="badge badge-dark badge-corner radius-0"><?php echo $total_offer ; ?></span></a>                                                    
                                        <!--Offer Modal Start-->
                                        <div class="modal fade" id="open_status_modal<?php echo $ii ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel">SEND OFFERS</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Boxes</th>
                                                                    <th>Product</th>
                                                                    <th>Comment</th>
                                                                    <th>LFD</th>                                                                    
                                                                    <th>Cargo Agency</th>
                                                                    <th>Status</th>                                                                                                                                        
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <form action="" method="post" id="add_to_cart_form" class="add_to_cart_form">
                                                                    <?php
                                                                                        $sro=1;
                                                                                                                                                        
                                                                             while($stat=mysqli_fetch_assoc($result5))  {  
                                                                                 
					  //it will give all the data from the table grower_offer_reply.
                                            $sel_check = "select * from  grower_offer_reply where offer_id='" . $stat["cartid"] . "' and grower_id='" . $products["id"] . "' ";
                                            $rs_check = mysqli_query($con, $sel_check);
                                            $check = mysqli_num_rows($rs_check);
                                            $grower_offer = mysqli_fetch_array($rs_check);                                       
									   
					  //it will give help to compare the current time with the lfd-*date*
                                            $tdate2 = date("Y-m-d");
                                            $date12 = date_create($grower_offer["date_added"]);
                                            $date22 = date_create($tdate2);
                                            $interval = $date12->diff($date22);
                                            $checka1 = $interval->format('%R%a');
                                            $expired = 0;
																						
					if ($grower_offer["status"] == 1 ) {
                                            $cls = 'success';
                                            $orderstatus = "In progres";
                                        } else if ($grower_offer["reject2"] == 1) {
                                            $cls = 'primary';
                                            $orderstatus = "Bid Closed";
                                        } else {
                                            
                                            if ($checka1 < 2) {
                                                $sel_buyer_info = "select live_days from buyers where id='" . $growers["buyer_id"] . "'";
                                                $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                $buyer_info = mysqli_fetch_array($rs_buyer_info);
                                                
                                                if ($buyer_info["live_days"] > 0) {
                                                    $tdate = date("Y-m-d");
                                                    $date1 = date_create($producs['lfd']);
                                                    $date2 = date_create($tdate);
                                                    $interval = $date2->diff($date1);
                                                    $checka2 = $interval->format('%R%a');
                                                    if ($checka2 == 0) {
                                                        $expired = 1;
                                                    } else if ($checka2 > $buyer_info["live_days"] ) {
                                                        $expired = 0;
                                                    } else {
                                                        $expired = 1;
                                                    }
                                                }
                                                
                                            } else {
                                                $expired = 1;
                                            }
                                            
                                            if ($expired == 0) {
                                                if($remaning_qty > 0){
						    $cls = 'warning';
                                                    $orderstatus = "In Process";                                                   												       
                                                }else{
                                                    $cls = 'primary';												
			                            $orderstatus = "Confirm";     
                                                }                                                
                                            } else {
                                                $cls = 'primary';
                                                $orderstatus = "Bid Closed";
                                            }
											
                                        }                                                                                  
                                                                                            
                                                                                        ?>
                                                                    
                                                                                <tr class="offer">

                                                                                            <td class="text" align="left"><?php  echo $stat["boxqty"]." ".$stat["boxtype"] ?></td>
                                                                                            
                                                                                            <td class="text" align="left"><?php  echo $stat["subs"]."  ".$stat["product_name"]."  ".$stat["sizename"]."  cm." ?></td>

                                                                                            <td class="text" align="left"><?php  echo $stat["comment"] ?></td>
                                                                                            
                                                                                            <td class="text" align="left"><?php  echo $stat["lfd"] ?></td>                                                                                                                                                                                       
                                                                                            
                                                                                            <td class="text" align="left"><?php  echo $stat["cargo"] ?></td>
                                                                                            
                                                                                            <td class="text" align="left"><?php  echo $orderstatus ?></td>                                                                                            
                                                                                </tr>
                                                                                        <?php 

                                                                                        $sro++;

                                                                                        }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                         </td>   
                                         <td class="text" align="center"><?php echo $total_confir ; ?></td>
                                         
                                        <!--Offer Modal End-->  
                                               
                                        </tr>  
                                        
                                                <?php


                                                $ii++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /LEFT -->
                </div>
            </div>
        </div>
    </div>
</section>

<script type='text/javascript'> 
 $(document).ready(function () {
								
        $('#loading').css("display", "none");
    });
		 

    $(".load").click(function (event) {
        $('#loading').css("display", "block");
    });									  
</script>
<script> 
    function rejectOffer(offer_id, buyer_id, grower_id) {
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>/growers/reject_offer_grower.php',
            data: 'offer_id=' + offer_id + '&buyer_id=' + buyer_id + '&grower_id=' + grower_id,
            success: function (data) {
                data = $.trim(data);
			   if (data == 'true') {
                    alert('Offer has been rejected.');
                    location.reload();
                } else {
                    alert('There is some error. Please try again');
                    location.reload();
                }
            }
        });
    }    
 
    function modifyOffer(id) {
        var check = 0;
        <?php
        $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
        $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
        $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>
        if ($('#productsize-' + id).val() == "") {
            $('#errormsg-' + id).html("please select product size");
            check = 1;
        }
        if ($('#price-' + id).val() == "") {
            $('#errormsg-' + id).html("please enter your price");
            check = 1;
        }
        else if ($('#price-' + id).val() != "") {
            if ($('#price-' + id).val() < <?php echo $minimum_price["mprice"] ?>) {
                $('#errormsg-' + id).html("price should be greater than <?php echo $minimum_price["mprice"] ?> ")
                check = 1;
            }
        }
        if ($('#boxtype-' + id).val() == "") {
            $('#errormsg-' + id).html("please select box type");
            check = 1;
        }

        else if ($('#bunchsize-' + id).val() == "") {
            $('#errormsg-' + id).html("please select bunch size");
            check = 1;
        }

        else if ($('#bunchqty-' + id).val() == "") {
            $('#errormsg-' + id).html("please enter bunch qty / box ");
            check = 1;
        }

        else if ($('#boxqty-' + id).val() == "") {
            $('#errormsg-' + id).html("please enter box qty");
            check = 1;
        }


        if (check == 0) {
            $('#addanomo').val("1")
            $('#errormsg-' + id).html("");

            console.log("Este  es  el  mensaje  de  a:" + "<br>");
            console.log($("#total_a").val());
            var k = confirm('Are you sure you want send offer');
            if (k == true) {
                $(".disabled-css").removeAttr("disabled");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>growers/growers-send-offers-ajax.php',
                    data: $('form#sendoffer' + id).serialize(),
                    success: function (data) {
                        //alert("esto es  data:" + data);
                        var fichas = data.split('######');
						 jQuery(".progress-bar").animate({width: "100%"}, 6000);
                        $('.count').each(function () {
                            $(this).prop('Counter', 0).animate(
                                {Counter: $(this).text()},
                                {
                                    duration: 7000, easing: 'swing', step: function (now) {
                                    $(this).text(Math.ceil(now));
                                }
                                });
                        });
                        console.log("este  es el  mensaje de  cuanto  esta  enviando:" + fichas[0]);
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
            }
        }

        //growers-send-offers-ajax.php

    } 
function modifyOfferxConfir(id) {


        var check = 0;
        <?php
        $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
        $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
        $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>


        var check = 0;

        if (check == 0) {
            var k = confirm('Are you sure you want send offer');
            if (k == true) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>/growers/growers-send-offers-price-conffir.php',
                    data: $('form#sendof' + id).serialize(),
                    success: function (data) {
						    console.log(data);
                        var fichas = data.split('######');
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
            }
        }
    }
    
function getOfferDetailsPopup(offer_id, offer_index_number, popup_index, product, feature, sizename, qty, boxtype, sizeid, box_type_name) {
        jQuery("#tab_a_" + popup_index).addClass('active');
        jQuery("#tab_b_" + popup_index).removeClass("active");
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>/growers/getOfferDetailsPopup.php',
            data: 'offer_id=' + offer_id + '&offer_index_number=' + offer_index_number + '&action_type=get_details_offer&product=' + product + '&feature=' + feature + '&sizename=' + sizename + '&qty=' + qty + '&boxtype=' + boxtype + '&sizeid=' + sizeid + '&box_type_name=' + box_type_name,
            success: function (data) {
                $("#main_table_offer_" + popup_index).html(data);
                $("#main_table_" + popup_index).hide();
                $(".offer_model_popup").hide();
            }
        });
    } 
    
    function defaultOffer(offer_id, offer_index_number, popup_index, product, feature, sizename, qty, boxtype, sizeid, box_type_name) {
        $("#main_table_offer_" + popup_index).html("");
        $("#main_table_" + popup_index).show();
        $("#tab_a_" + popup_index).addClass("active");
        $("#tab_b_" + popup_index).removeClass("active");
    }   
    
    function boxQuantityChange(id, cartid, main_tr, with_a) {
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val = $("#request_qty_box_" + main_tr + " option:selected").val();

        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);
        $("#productsize-" + cartid + "-" + id).removeAttr("disabled");
        $("#bunchqty-" + cartid + "-" + id).removeAttr("disabled");
        $("#price-" + cartid + "-" + id).show();
        $("#price_label_" + cartid + "-" + id).hide();
        $("#price-" + cartid + "-" + id).removeAttr("disabled");
        $("#productsize-" + cartid + "-" + id).removeClass("disabled-css");
        $("#bunchqty-" + cartid + "-" + id).removeClass("disabled-css");
        $("#price-" + cartid + "-" + id).removeClass("disabled-css");


    }
    function boxQuantityChanges(id, cartid, main_tr, with_a) {
        var selected_text = $("#request_qty_box_x_" + main_tr + " option:selected").text();
        var selected_val = $("#request_qty_box_x_" + main_tr + " option:selected").val();
        var numberbox = selected_val.split("-");
        $('input[name=product_su_x]').val(numberbox[0]);


        $("#setear-" + main_tr).val(with_a * numberbox[0]);

        $('input[name=tot_bu_x]').val(with_a * numberbox[0]);
        $('input[name=box_volumn_x]').val(numberbox[1] / 6000);
        $('input[name=box_name]').val(numberbox[2]);

    }  
    
    function modifyOfferx(id) {


        var check = 0;
        <?php
        $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
        $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
        $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>


        var check = 0;

        if (check == 0) {

            var k = confirm('Are you sure you want send offer');
            if (k == true) {
                //$(".disabled-css").removeAttr("disabled");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>/growers/growers-send-offers-price.php',
                    data: $('form#sendof' + id).serialize(),
                    success: function (data) {
						    console.log(data);
                        var fichas = data.split('######');
                        //console.log("este  es el  mensaje de  cuanto  esta  enviando:" + fichas[0]);
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
            }
        }

    }

    function send_offer_box() {

        alert(send_offer_box);

    }
    function send_offer(cartid, buyer, product, product_subcategory, sizename, price, qty) {
        console.log("Esta  es la oferta  se  esta  enviando");
        var boxtype = $("#boxtype-" + cartid).val();
        var k = confirm('Are you sure you want send offer');
        if (k == true) {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/growers/growers-send-offers-ajax-direct.php',
                data: 'offer=' + cartid + '&buyer=' + buyer + '&product=' + product + '&product_subcategory=' + product_subcategory + '&boxtype=' + boxtype + '&sizename' + sizename + '&price=' + price + '&qty=' + qty,
                success: function (data) {
                    alert('Offer has been sent.');
                    location.reload();
                }
            });
        }
    }
    

</script>
<?php include("../includes/footer_new.php"); ?>
