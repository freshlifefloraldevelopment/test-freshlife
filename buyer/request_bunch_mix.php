<?php

require_once("../config/config_gcp.php");
   
$menuoff = 1;
$page_id = 33;
$sub_id = 1;
$message = 0;

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}


function pagination($num_record, $per_page, $page){
    $total = $num_record;
    $adjacents = "2";
    $page = ($page == 0 ? 1 : $page);

    $start = ($page - 1) * $per_page;
    $counter = 1;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total / $per_page);
    $lpm1 = $lastpage - 1;
    $pagination = "";

    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination'>";
        if ($page > $counter) {
            $pagination .= "<li><a href='?page=1'>First</a></li>";
            $pagination .= "<li><a href='?page=$prev'>Pervious</a></li>";
        } else {
            $pagination .= "<li class=''><a href='javascript:void(0);' >First</a></li>";
            $pagination .= "<li class=''><a href='javascript:void(0);' >Pervious</a></li>";
        }
        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination .= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                else
                    $pagination .= "<li><a href='?page=$counter'>$counter</a></li>";
            }
        } elseif ($lastpage > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?page=$counter'>$counter</a></li>";
                }
                $pagination .= "<li class='dot'>...</li>";
                $pagination .= "<li><a href='?page=$lpm1'>$lpm1</a></li>";
                $pagination .= "<li><a href='?page=$lastpage'>$lastpage</a></li>";
            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination .= "<li><a href='?page=1'>1</a></li>";
                $pagination .= "<li><a href='?page=2'>2</a></li>";
                $pagination .= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?page=$counter'>$counter</a></li>";
                }
                $pagination .= "<li class='dot'>..</li>";
                $pagination .= "<li><a href='?page=$lpm1'>$lpm1</a></li>";
                $pagination .= "<li><a href='?page=$lastpage'>$lastpage</a></li>";
            } else {
                $pagination .= "<li><a href='?page=1'>1</a></li>";
                $pagination .= "<li><a href='?page=2'>2</a></li>";
                $pagination .= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?page=$counter'>$counter</a></li>";
                }
            }
        }
        if ($page < $counter - 1) {
            $pagination .= "<li><a href='?page=$next'>Next</a></li>";
            $pagination .= "<li><a href='?page=$lastpage'>Last</a></li>";
        } else {
            $pagination .= "<li class=''><a href='javascript:void(0);'>Next</a></li>";
            $pagination .= "<li class=''><a href='javascript:void(0);'>Last</a></li>";
        }
        $pagination .= "</ul>\n";
    }
    return $pagination;
}

$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,state,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image,handling_fees FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $state, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image, $handling_fees);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}

$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
$today1 = explode(" ", $today);
$today_date = $today1["0"];
$time = $today1["1"];
$hours_array = explode(":", $time);
$hm = $hours_array[0] . ":" . $hours_array[1];

if (!isset($_POST["cartsubmit"]) && !isset($_GET["delete"]) && !isset($_POST["tp"])) {
    $sel_login_check = "select * from activity where buyer='" . $userID . "' and ldate='" . date("Y-m-d") . "' and ltime='" . $hm . "' and type='buyer' and atype='np'";
    $rs_login_check = mysqli_query($con, $sel_login_check);
    $login_check = mysqli_num_rows($rs_login_check);

    if ($login_check >= 1) {

    } else {
        $name = $first_name . " " . $last_name;
        $activity = "Name your pirce at " . $hm;
        $insert_login = "insert into activity set buyer='" . $userID . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='np'";
        mysqli_query($con, $insert_login);
    }
}
if (isset($_GET["delete"])) {
    $insert = "delete from offer_cart where id='" . $_GET["delete"] . "' and buyer='" . $_SESSION["buyer"] . "'";
    mysqli_query($con, $insert);
    $k = mysqli_affected_rows();
    if ($k == 1) {
        $message = 1;
        $name = $first_name . " " . $last_name;
        $activity = "Deleted offer from Name your pirce at " . $hm;
        $insert_login = "insert into activity set buyer='" . $userID . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='np'";
        mysqli_query($con, $insert_login);
    }
}

?>

<?php
## CODE FROM OLD FILE AS REFERENCE ##

// Paginacion
$sel_pagina = "select gpb.prodcutid,
                  gpb.id as gid,
                  gpb.qty,
                  gpb.sizeid,
                  gpb.feature,
                  gpb.growerid,
                  p.id,
                  p.subcategoryid,
                  p.name,
                  p.color_id,
                  p.image_path as img_url,
                  s.name as subs,
                  s.cat_id,
                  g.file_path5,
                  g.growers_name,
                  sh.name as sizename,
                  ff.name as featurename,
                  b.width , b.length , b.height,
                  b.name as boxname,
                  bs.id as bunchsizeid,
                  bs.name as bname,
                  c.name as colorname 
             from grower_product_box_packing gpb
             left join product p      on gpb.prodcutid     = p.id
             left join subcategory s  on p.subcategoryid   = s.id  
             left join colors c       on p.color_id        = c.id 
             left join features ff    on gpb.feature       = ff.id
             left join sizes sh       on gpb.sizeid        = sh.id 
             left join boxes b        on gpb.box_id        = b.id
             left join boxtype bt     on b.type            = bt.id             
             left join growers g      on gpb.growerid      = g.id
             left join bunch_sizes bs on gpb.bunch_size_id = bs.id
            where g.active='active'  ";

$rs_pagina    = mysqli_query($con, $sel_pagina);
$total_pagina = mysqli_num_rows($rs_pagina);
$num_record   = $total_pagina;

$display = 50;

$XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

function get_bunchs($product_id, $product_size, $buyer_id) {
    global $con;
    $getbunches = mysqli_query($con, "SELECT gs.sizes,bs.name AS bunchname  
                                        FROM grower_product_bunch_sizes AS gs 
                                        LEFT JOIN bunch_sizes bs ON gs.bunch_sizes=bs.id 
                                       WHERE gs.grower_id  = '" . $buyer_id . "' 
                                         AND gs.product_id = '" . $product_id . "' 
                                         AND gs.sizes      = '" . $product_size . "'");
    
    $rowbunches = mysqli_fetch_assoc($getbunches);
    return $rowbunches;
}

function category()
{
    global $con;
    $category_sql = "SELECT id,name from category order by  name";
    $result_category = mysqli_query($con, $category_sql);
    $category_ret = "";
    while ($row_category = mysqli_fetch_assoc($result_category)) {
        $category_ret .= "<option value='" . $row_category['id'] . "'>" . $row_category['name'] . "</option>";
    }
    return $category_ret;
}

function product()
{
    global $con;
    $query = "SELECT id,name from product order  by  name";
    $result_product = mysqli_query($con, $query);
    $produc_result = "";
    while ($row_product = mysqli_fetch_assoc($result_product)) {
        $produc_result .= "<option value = '" . $row_product['id'] . "'>" . $row_product['name'] . "</option>";
    }
    return $produc_result;
}

function colors()
{
    global $con;
    $colors_sql = "SELECT * from colors order by NAME ";
    $result_colors = mysqli_query($con, $colors_sql);
    return $result_colors;
}

$category = category();
$product = product();


function query_main($user, $init, $display,$idsubcate){ 

    $query = "select gpb.prodcutid,
                  gpb.id as gid,
                  gpb.qty,
                  gpb.sizeid,
                  gpb.feature,
                  gpb.growerid,
                  p.id,
                  p.subcategoryid,
                  p.name,
                  p.color_id,
                  p.image_path as img_url,
                  s.name as subs,
                  s.cat_id,
                  g.file_path5,
                  g.growers_name,
                  sh.name as sizename,
                  ff.name as featurename,
                  b.width , b.length , b.height,
                  b.name as boxname, b.type ,
                  bt.id as typebox,                  
                  bt.name as boxt,
                  bs.id as bunchsizeid,
                  bs.name as bname,
                  c.name as colorname ,
                  (bs.name*gpb.qty) totbox
             from grower_product_box_packing gpb
             left join product p      on gpb.prodcutid     = p.id
             left join subcategory s  on p.subcategoryid   = s.id  
             left join colors c       on p.color_id        = c.id 
             left join features ff    on gpb.feature       = ff.id
             left join sizes sh       on gpb.sizeid        = sh.id 
             left join boxes b        on gpb.box_id        = b.id
             left join boxtype bt     on b.type            = bt.id
             left join growers g      on gpb.growerid      = g.id
             left join bunch_sizes bs on gpb.bunch_size_id = bs.id
            where g.active='active' ";
    
    		if (isset($idsubcate)) {
                    $query =$query." and s.id=".$idsubcate;
		}
            
           $query =$query. "  order by g.growers_name , p.name ,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER) LIMIT $init,$display";
         
    
    return $query;    
}


if (isset($_GET["idCategory"])) {
	$idCategory=$_GET["idCategory"];
}

if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = query_main($userSessionID, $_POST["startrow"], $display,$idCategory);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = query_main($userSessionID, 0, $display,$idCategory);
    $result2 = mysqli_query($con, $query2);
}


?>
<?php
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_buyer.php";
?>
<link rel="stylesheet" type="text/css" href="../assets/css/loading.css">

<!--comienza el  estilo-->
<style>

    .cssload-spin-box {
        position: absolute;
        margin: auto;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        width: 23px;
        height: 23px;
        border-radius: 100%;
        box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -o-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -ms-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -webkit-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -moz-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        animation: cssload-spin ease infinite 7s;
        -o-animation: cssload-spin ease infinite 7s;
        -ms-animation: cssload-spin ease infinite 7s;
        -webkit-animation: cssload-spin ease infinite 7s;
        -moz-animation: cssload-spin ease infinite 7s;
    }
 
 

				  
    @keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }
						  
 

				 
    @-o-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

    @-ms-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

    @-webkit-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

    @-moz-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

</style>
<!--termina  el  estilo  -->						
<style>
    /*This css is fir fixing select dropdown size just only on request_bunch_mix.php page*/
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__rendered, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px !important;
        line-height: 36px !important;
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    td {
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    .modal_k {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('../images/ajax-loader.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

    body.loading .modal_k {
        display: block;
    }

    .pagination > li {
        float: left;
    }

    #middle div.panel-heading {
        height: auto;
    }

    .text-center {
        width: 30px;
    }

    .price_field {
        overflow: inherit !important;
        width: 405px;
    }

    .request_product_modal_hide {
        height: 400px;
        overflow-y: auto;
    }

		</style>
<!--stilo  del  loading-->
<style type='text/css'>
    #loading {
        width: 100%;
        height: 100%;
        background-color: white;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 9999;
        opacity: 0.5;
        filter: alpha(opacity=70);
    }
</style>
<!--/stilo  del  loading-->
<style>				  
    .noRow {
        display: none;
    }
</style>	



<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Request Box Mix</h1>
        <ol class="breadcrumb">
            <li><a href="#">Fresh Life Floral</a></li>
            <li class="active">Send your request Mix </li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">
                            <div id="panel-2" class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="title elipsis"><strong> Requests per Box</strong> <!-- panel title --></span>
                                    <!-- right options -->
                                    <ul class="options pull-right list-inline">
                                        <!-- 20-10-2016 -->
                                        <div class="tags_selected" id="tab_for_filter" style="float: left; margin-top: 4px; margin-right: 4px; display: none;"></div>
                                        
                                        <!-- 30-05-2019 -->
                                        <!--li><a href="<?php echo SITE_URL; ?>buyer/upload_doh_csv.php" class="btn btn-success btn-xs relative"> Upload file (CSV) </a></li-->
                                        <li><a data-target=".search_modal_open" data-toggle="modal" class="btn btn-primary btn-xs white" href="#"><i class="fa fa-filter"></i>Filter</a></li>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fade search_modal_open"
                                             style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">X</span></button>
                                                        <h4 id="myModalLabel" class="modal-title">FILTER</h4>
                                                    </div>
                                                    <!-- Modal Body -->
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            
                                                                                                                       
                                                            <label>Size</label>
                                                            <div class="form-group">
                                                                <select style="width: 100%; display: none;" name="filter_size" id="filter_size" class="form-control select2 cls_filter" tabindex="-1">
                                                                    <option value="">Select Size</option>
                                                                    <option value="3">Half Box</option>
                                                                    <option value="5">Quarter Box</option>
                                                                    <option value="11">Eighth Box</option>
                                                                    <option value="10">Jumbo Box</option>
                                                                </select>
                                                            </div>
                                                            

                                                        </div>
                                                    </div>
                                                    <!-- Modal Footer -->
                                                    <div class="modal-footer">
                                                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                                        <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                        <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>../images/ajax-loader.gif"/>
                                                        <!--<div class="cssload-spin-box"></div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                    <!-- /right options -->
                                </div>
                                <!-- panel content -->
                                <div class="panel-body">

                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                            <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif">

                                        </div>
                                    </div>
                                    <!--                                    <div class="pickert1"><h5>Please select your delivery date</h5></div>
                                                                        <div class="pickert2"><h5>Please select a Date Range if your order can be delivered in different days</h5></div>-->
                                    <!-- date picker -->
                                    <input type="hidden"
                                           class="form-control datepicker picker1" <?php if (!empty($_SESSION['delDate'])) { ?> disabled="" value="<?php echo $_SESSION['delDate']; ?>" <?php } ?>
                                           start-date="<?php echo date('Y-m-d'); ?>" name="picker1" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false">
                                    <!-- range picker -->
                                    <input type="hidden" class="form-control rangepicker picker2"
                                           <?php if (!empty($_SESSION['dateRange'])) { ?>value="<?php echo $_SESSION['dateRange']; ?>"
                                           disabled=""<?php } ?> name="picker2" data-format="yyyy-mm-dd" data-from="2015-01-01" data-to="2016-12-31">
                                    <input type="hidden" id="shippingMethod"
                                           value="<?php echo (!empty($_SESSION['shippingMethod'])) ? $_SESSION['shippingMethod'] : '0'; ?>"/>
                                    <br>
                                    <!--barra  de  busqueda-->
                                    <form class="clearfix well well-sm search-big nomargin" action="/buyer/request_bunch_mix.php" method="get">
                                        <div class="autosuggest fancy-form input-group" data-minLength="1" data-queryURL="<?php echo SITE_URL ?>/includes/autosuggest.php?limit=10&search=">
                                            <div class="input-group-btn">
                                                <button data-toggle="dropdown" class="btn btn-default input-lg dropdown-toggle noborder-right" type="button" aria-expanded="false">Filter <span class="caret"></span></button>
                                                <?php
                                                $sql_category = "select id, name from subcategory order by name";
                                                $result_cat = mysqli_query($con, $sql_category);
                                                ?>
                                                <ul class="dropdown-menu">
                                                    <li class="active"><a href="#"><i class="fa fa-check"></i> Everyting</a></li>
                                                    <li class="divider"></li>
                                                    <?php
                                                    while ($options = mysqli_fetch_array($result_cat)) { ?>
                                                        <li><a href="<?= "/buyer/request_bunch_mix.php?idCategory=".$options['id'] ?>"><?= $options['name'] ?></a></li>
                                                    <?php }
                                                    ?>
                                                </ul>
                                            </div>

                                            <input type="text" name="src" id="typeSearch" placeholder="Search From Here..." class="form-control typeahead"/>
                                            <!-- <i class="fa fa-arrow-down" id="on_click"></i> -->
                                            <div class="input-group-btn">
                                                <button class="btn btn-default input-lg noborder-left" type="submit">
                                                    <i class="fa fa-search fa-lg nopadding"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                    <!--filtros-->
                                    <div class="dataRequest">
                                        <?php
                                        $i = 1;
                                        $s = 1;                                        
                                        $tp = mysqli_num_rows($result2);
                                        ?>
                                        <input type="hidden" name="tp" id="tp" value="<?= $tp ?>">
                                        <input type="hidden" name="current" id="current" value="">
                                        <table class="table table-hover table-vertical-middle" style="margin-top:20px;">
                                            <thead>
                                            <tr>
                                                <th>Grower</th>                                                
                                                <th>Variety</th>
                                                <th>Price</th>
                                                <th>Packing</th>                                                
                                                <th>Send Request</th>
                                            </tr>
                                            </thead>
                                            <tbody id="listing_product_s">
                                            <?php
                                            //echo '<pre>';
                                            
                                            
                                            
                                while ($products = mysqli_fetch_array($result2)) {
                                    
                                                //echo '<pre>';print_r($products);die;
                                                $g = 1;
                                                $grower_cnt = 0;
                                                $price_var = 'N/A';    
                                                
                                                
                                                
					    $sel_packing="select id          , price       , prodcutid   , prodcutname ,
                                                                 sizeid      , sizename    , date_added  , feature     , stems     
                                                            from grower_product_price
                                                           where growerid  = '".$products["growerid"]."'
                                                             and prodcutid = '".$products["prodcutid"]."'
                                                             and sizename  = '".$products["sizename"]."'  ";
								 
					    $rs_packing    = mysqli_query($con,$sel_packing);
					    $price_packing = mysqli_fetch_array($rs_packing);                                                                                                               
                                                                                                                                                                                                                
                                                ?>
                                                <tr>
                                                    <!--td><img src="https://app.freshlifefloral.com/<?= $products["img_url"]; ?>" width="70"></td-->
                                                    <td>
                                                        <?= $products["growers_name"]; ?>
                                                    </td>
                                                    
                                                    <!--Variety-->
                                                    <td>
                                                        <!--<td class="text-center" WIDTH="100" >-->
                                                        
                                                        <a href="" data-toggle="modal" data-target="#single_product_modal<?php echo $i; ?>"> <?= $products["subs"]." ".$products["name"]." ".$products["sizename"] . " cm "." ".$products["bname"]." st/bu "." ".$products["colorname"]." ".$products["featurename"] ?></a>
                                                        <!--Modal image for single product-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title"
                                                                            style="font-size: 14px;"><?= $products["subs"] ?>
                                                                            <br>
                                                                            <center><?= $products["name"] ?> <?= $products["sizename"] . "cm" ?></center>
                                                                        </h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12" style="color:midnightblue">
                                                                                <img src="<?php echo SITE_URL . $products["img_url"] ?>" width="100%">
                                                                                <h7 id="myLargeModalLabel3" class="modal-header" style="font-size: 14px;">Fresh Life Floral</h7>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!---->
                                                        <input type="hidden" name="sizename" id="sizename_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" value="<?php echo $products["sizename"]; ?>"/>
                                                        <input type="hidden" name="conType" id="conType_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" value="<?php echo $connectionType; ?>"/>
                                                        <input type="hidden" name="shippingFee" id="shippingFee_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" value="<?php echo $handling_fees; ?>"/>
                                                        <input type="hidden" name="sizeId" id="sizeId_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" value="<?php echo $products['sizeid'] ?>"/>
                                                        <input type="hidden" name="productId" id="productId_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" value="<?php echo $products['prodcutid'] ?>"/>
                                                        <input type="hidden" name="featureId" id="featureId_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" value="<?php echo $products['feature'] ?>"/>
                                                        <textarea style="display:none" name="productComment" id="productComment_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>"></textarea>
                                                        <input type="hidden" name="productName_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" id="productName_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>"
                                                               value="<?= $products["subs"] ?> - <?= $products["name"] ?> <?= $products["featurename"] ?> - <?= $products["sizename"] ?> cm"/>
                                                        <input id="buyerPrice_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>" type="hidden" name="buyerPrice_<?php echo $products['prodcutid'] ?>_<?php echo $i ?>"/>
                                                        <input type="hidden" name="order_csh" id="order_csh" value="">

                                                    </td>
                                                    
                                                    
                                                    <!--td><?= $products["colorname"] ?></td-->                                                    
                                                    
                                                    <!--td style="color: #cc0066"><?= $products["sizename"] . " cm" ?></td-->
                                                    
                                                    <!--td><?= $products["featurename"] ?></td-->
                                                    
                                                    <td><?= $price_packing["price"] ?></td>                                                    
                                                    <!--td><?= $products["boxt"]." ".$products["totbox"] ?></td-->  
                                                    <td>
                                                        
                                                        
                                                        
                                                    <a href="" data-toggle="modal" data-target="#single_size_modal<?php echo $i; ?>"> <?= $products["boxt"]." ".$products["totbox"] ?></a>
                                                        <div class="modal fade bs-example-modal-sm" id="single_size_modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title"
                                                                            style="font-size: 14px;"><?= $products["subs"] ?>
                                                                            <br>
                                                                            <center><?= $products["name"] ?> <?= $products["sizename"] . "cm" ?></center>
                                                                        </h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">      
                                                                            <div class="col-md-12" style="color:midnightblue">
                                                                                <center><?php echo "Sizes :".$products["width"]."x".$products["length"]."x".$products["length"] ?></center>
                                                                                <!--h7 id="myLargeModalLabel3" class="modal-header" style="font-size: 14px;">Fresh Life Floral</h7-->

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                    </td>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    

                                                    <!--Send  Request-->
                                                    <td>
                                                        <div class="btn-group">
                                                            <!--button type="button" onclick="modelClick(<?php echo $products['prodcutid'] ?>,<?php echo $i ?>,<?php echo $products['bname'] ?>,<?php echo $products['sizeid'] ?>,<?php echo $products['feature'] ?>,<?php echo $products['bunchsizeid'] ?>,<?php echo $chargesPerKilo; ?>,<?php echo $products["sizename"]; ?>,<?php echo $products['cat_id']; ?>,<?php echo $i; ?>)"
                                                                    id="price_modal_<?php echo $i; ?>"
                                                                    class="btn btn-default btn-xs" style="background:#34495E;color:white" data-toggle="modal" data-target=".price_modal_<?php echo $i; ?>"><i class="fa fa-send"></i></i>Request
                                                            </button-->
                                                             <button type="button" class="btn-xs btn btn-success" data-toggle="modal" id="modify_modal_<?php echo $s; ?>" data-target=".modify_modal_<?php echo $s; ?>">Request Mix</button>                                                        
                                                        </div>
                                                        
                                                            
                                                        <?php
                                                        $getbunches = mysqli_query($con, "SELECT gs.sizes,bs.name as bunchname  
                                                                                            FROM grower_product_bunch_sizes as gs 
                                                                                            left join bunch_sizes bs on gs.bunch_sizes=bs.id 
                                                                                            WHERE gs.product_id = '" . $products['prodcutid'] . "' 
                                                                                              AND gs.sizes      = '" . $products['sizeid'] . "'");   
                                                        $rowbunches = mysqli_fetch_assoc($getbunches);
                                                        ?>
                                                        
                                                        
                                                        
                                                        <!--Price Modal Start-->
                                                        
                                                        <!--Price Modal End-->
                                                        
                                                        
                                                        
                                                        
                                                        
                                                            <!-- Modify Modal 1.1.1>-->
                                                            <div class="modal fade modify_modal_<?php echo $s; ?>"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-medium" style="width: 1000px">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <?php
                                                                                $rowbunches = get_bunchs($products['product'], $products['sizeid'], $products["growerid"]);                                                                                                                                                                                        
                                                                                                                                                                                                                              
                                                                        ?>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">
                                                                                <?php echo $products["qty"] . " " . $box_type["name"]; ?>
                                                                                <?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] ?> cm <?php echo $rowbunches['bunchname'] . " st/bu" ; ?></h4>
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body"><?php //echo '<pre>'; print_r($products);     ?>
                                                                            <div class="row">

                                                                                <!-- tabs content -->
                                                                                <div class="col-md-12 col-sm-12 nopadding">
                                                                                    <div class="tab-content tab-stacked">
                                                                                        <div id="tab_a_<?php echo $s; ?>" class="tab-pane active">
                                                                                            <!-- classic select21 -->
                                                                                            <?php
                                                                                            
                                                                                            $sql_boxes = "SELECT boxes 
                                                                                                            FROM growers
                                                                                                           WHERE  id ='" . $products["growerid"] . "'";
                                                                                            
                                                                                            $rs_boxes = mysqli_query($con, $sql_boxes);
                                                                                            $boxes = mysqli_fetch_array($rs_boxes);
                                                                                            
                                                                                            $a = substr($boxes['boxes'], 0, -1);
                                                                                            $a = substr($a, 1);


                                                                                            $sel_boxes = " SELECT b.id   , b.name , b.width , b.length , b.height , bo.name AS name_box , b.type,
                                                                                                                  un.code, unt.name as name_unit
                                                                                                             FROM boxes b 
                                                                                                             LEFT JOIN  boxtype bo    ON  b.type=bo.id 
                                                                                                             LEFT JOIN  units un       ON  b.type=un.id 
                                                                                                             LEFT JOIN  units_type unt ON  un.type=unt.id 
                                                                                                            WHERE  b.id IN(" . $a . ") AND bo.name in ('HB','QB','EB')  ";
                                                                                                                                                                                       
                                                                                            $rs_boxes = mysqli_query($con, $sel_boxes);

                                                                                            $sel_typeu = " select un.id , un.code, un.type , unt.id ,unt.name as name_unit 
                                                                                                             from units un
                                                                                                             left join units_type unt on un.type=unt.id 
                                                                                                            where un.id = '" . $products["typebox"] . "'; ";
                                                                                                                                                                                      
                                                                                            $rs_typeu = mysqli_query($con, $sel_typeu);  
                                                                                            $type_sel = mysqli_fetch_array($rs_typeu)                                                                                            
                                                                                            ?>
                                                                                            
                                                                                            
                                                                                            <select class="form-control select2 " name="request_qty_box" id="request_qty_box_<?php echo $s; ?>"
                                                                                                    onchange="boxQuantityChange(0,<?php echo $products["gid"]; ?>,<?php echo $s; ?>)" style="width:300px;">
                                                                                                <option value="">Select Box Quantity</option>
                                                                                                
                                                                                                <?php
                                                                                                
                                                                                                $request_qty = round($products["qty"]/100);   
                                                                                                     
                                                                                                if ($request_qty <= 3) {
                                                                                                     $request_qty = 10;                                                                                                        
                                                                                                }
                                                                                                
                                                                                                while ($box_sel = mysqli_fetch_array($rs_boxes)) {
                                                                                                    if ($type_sel['name_unit'] == "Box") {
                                                                                                                $request_qty = $products["qty"];
                                                                                                    }                                                                                                                                                                                                        
                                                                                                    for ($i = 1; $i <= $request_qty; $i++) {
                                                                                                        ?>
                                                                                                        <option value="<?php echo $i . "-" . $box_sel['width'] * $box_sel['length'] * $box_sel['height']; ?>">
                                                                                                            <?= $i . " " . $box_sel['name_box'] . " " . $box_sel['width'] . "x" . $box_sel['length'] . "x" . $box_sel['height']; ?>
                                                                                                        </option>
                                                                                                    <?php }
                                                                                                } ?>
                                                                                            </select> 
                                                                                            

                                                                                            
                                                                                            
                                                                                            
                                                                                        <select style="width:300px;"
                                                                                                class="form-control select2 "                                                                                                
                                                                                                name="filter_order" 
                                                                                                id="box_order_<?php echo $products["prodcutid"] . '_' . $i; ?>"                                                                                                 
                                                                                            <option value="">Select order</option>
                                                                                                <?php
                                                                                                $category_sql = "select  id,qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' and  del_date >= '" . date("Y-m-d") . "' and is_pending=0";
                                                                                                $result_category = mysqli_query($con, $category_sql);
                                                                                                while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                                                    <option value="<?php echo $row_category['id']; ?>"><?= $row_category['qucik_desc']; ?></option>
                                                                                                <?php }
                                                                                                ?>
                                                                                        </select>
                                                                                            
                                                                                                                                                                                

                                                             
                                                                                            <input type="hidden" class="cls_hidden_selected_qty" name="cls_hidden_selected_qty" value=""/>
                                                                                            <form name="sendoffer" id="sendoffer<?= $products["cartid"] ?>" method="post">
                                                                                                <input type="hidden" name="mainprice" id="mainprice" value=" <?php
                                                                                                if ($products["price"] != "0.00") {
                                                                                                    echo $products["price"];
                                                                                                }

                                                                                                ?> "/>
                                                                                                <!--Inside Table-->

                                                                                                <input type="hidden" name="offer" id="offer" value="<?= $products["cartid"] ?>">
                                                                                                <input type="hidden" name="buyer" id="buyer" value="<?= $products["buyer"] ?>">
                                                                                                <input type="hidden" name="id_client" id="id_client" value="<?= $products["id_client"] ?>">                                                                                                
                                                                                                
                                                                                                <input type="hidden" name="product" id="product" value="<?= $products["name"] ?> <?= $products["featurename"] ?>">                                                                                                                                                                                                
                                                                                                <input type="hidden" name="product_subcategory" id="product_subcategory" value="<?= $products["subs"] ?>">
                                                                                                <input type="hidden" name="product_ship" id="product_ship" value="1">
                                                                                                <input type="hidden" name="shipp" id="shipp" value="<?= $products["shpping_method"] ?>">
                                                                                                <input type="hidden" name="product_su" id="product_su" value="1">
                                                                                                <input type="hidden" name="volume" id="volume" value="1">
                                                                                                <input type="hidden" name="code_order" id="code_order" value="<?= substr($products["cod_order"], 0, 4) ?>">
                                                                                                <input type="hidden" name="size_name" id="size_name" value="<?= $products["sizename"] ?>"> 
                                                                                                <input type="hidden" name="stems" id="stems" value="<?= $rowbunches['bunchname'] ?>">   
                                                                                                <input type="hidden" name="boxcant" id="boxcant" value="1"> 
                                                                                                <input type="hidden" name="boxtype" id="boxtype" value="<?= $box_type["name"] ?>">                                                                                                 
                                                                                                <input type="hidden" name="boxtypen" id="boxtypen" value="HB">

                                                                                                
                                                                                                <?php

                                                                                                $products["typebox"];
                                                                                                $temp = explode("-", $products["typebox"]);
                                                                                                $box_type_id = $temp[0];
                                                                                                $sel_box_ids = "SELECT bt.id AS boxtypeid , 
                                                                                                                       bt.name AS boxtype , 
                                                                                                                       bo.width , 
                                                                                                                       bo.length , 
                                                                                                                       bo.height , 
                                                                                                                       bo.name  AS box_value ,
                                                                                                                       go.grower_id , 
                                                                                                                       go.boxes 
                                                                                                                  FROM grower_product_box go 
                                                                                                                  LEFT JOIN boxes bo   ON bo.id=go.boxes
                                                                                                                  LEFT JOIN boxtype bt ON bo.type=bt.id
                                                                                                                 WHERE go.grower_id='" . $products["growerid"] . "' 
                                                                                                                   AND bt.id       ='" . $box_type_id . "' 
                                                                                                                 GROUP BY bo.name,                                                                                                                       bt.id ,
                                                                                                                       bt.name  , 
                                                                                                                       bo.width , 
                                                                                                                       bo.length , 
                                                                                                                       bo.height , 
                                                                                                                       go.grower_id , 
                                                                                                                       go.boxes  ";    
                                                                                                $rs_box_ids = mysqli_query($con, $sel_box_ids);
                                                                                                
                                                                                                $totalboxtype = mysqli_num_rows($rs_box_ids);
                                                                                                $i = 1;
                                                                                                
                                                                                                while ($box_ids = mysqli_fetch_array($rs_box_ids)) {
                                                                                                    $sel_weight = "SELECT weight 
                                                                                                                     FROM grower_product_box_weight 
                                                                                                                    WHERE growerid='" . $products["growerid"] . "' 
                                                                                                                      AND box_id  ='" . $box_ids["boxes"] . "'     ";
                                                                                                    
                                                                                                    $rs_weight = mysqli_query($con, $sel_weight);
                                                                                                    $weight = mysqli_fetch_array($rs_weight);
                                                                                                    $optionbb[$i] .= $box_ids["boxtype"] . "-" . $box_ids["box_value"] . "-" . ($box_ids["length"] * $box_ids["height"] * $box_ids["width"]) . "-" . $weight["weight"];
                                                                                                    $i++;
                                                                                                }
                                                                                                                                                                                                
                                                                                                
                                                                                                ?>
                                                                                                <input type="hidden" name="boxtype-<?= $products["cartid"] ?>" id="boxtype-<?= $products["cartid"] ?>" value="<?php echo $optionbb[1]; ?>">
                                                                                                <div class="table-responsive" style="margin-top:20px;">

                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_<?php echo $s; ?>">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Product</th>
                                                                                                            <th>Bunch Quantity</th>

                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <?php $a = 0; ?>
                                                                                                        <tr id="product_tr_0">
                                                                                                            <?php $a = $a + 1; ?>
                                                                                                            <!--Product-->
                                                                                                            <td>
                                                                                                                <select class="listmenu" style="margin-top:10px; height:35px; padding:3px; width:450px;border-radius: 5px;"
                                                                                                                        onchange="changeCMSize(0,<?php echo $products["gid"]; ?>,<?php echo $s; ?>,<?php echo $products['typebox']; ?>, <?php echo $products["growerid"]; ?>,<?php echo $products["prodcutid"]; ?>,<?php echo $products["sizeid"]; ?>)"                                                                                                                        
                                                                                                                        name="productsize[]"
                                                                                                                          id="productsize_<?php echo $products["gid"] ?>-0">
                                                                                                                    <option value="">Select Product ---</option>
                                                                                                                </select>
                                                                                                            </td>
                                                                                                            <!--Bunch Quantity-->
                                                                                                            <td>
                                                                                                                <select class="form-controll offer-select" 
                                                                                                                        onchange="changeBunchQty(0,<?php echo $products["gid"]; ?>,<?php echo $s; ?>)" 
                                                                                                                        name="bunchqty[]"
                                                                                                                        id="bunchqty-<?= $products["gid"] ?>-0">
                                                                                                                    <option value="">Select Bunch QTY</option>
                                                                                                                    <?php
                                                                                                                    for ($ij = 1; $ij <= 50; $ij++) {
                                                                                                                        ?>
                                                                                                                        <option value="<?= $ij ?>"><?= $ij ?></option>
                                                                                                                    <?php } ?>
                                                                                                                </select>

                                                                                                            </td>
                                                                                                            
                                                                                                            
                                                                                                            <!--Price-->                                                                                                                                                                                                                                                                                                                                    
                                                                                                            
                                                                                                            <!--Request-->                                                                                                                                                                                                                                                                                                                                    

                                                                                                            <!--Type Marquet-->                                                                                                            
                                                                                                            
                                                                                                            <!--Column name-->
                                                                                                            <td>
                                                                                                                <input type="hidden" class="boxqty<?php echo $s; ?>" name="boxqty[]" id="boxqty_0_<?php echo $s; ?>_<?php echo $products["gid"]; ?>" value=""/>
                                                                                                                <input type="hidden" name="box[]" id="box" value="$i"/>
                                                                                                                <input type="hidden" name="total_qty_bunches[]" id="total_qty_bunches_0_<?php echo $s; ?>_<?php echo $products["gid"]; ?>" value="0"/>
                                                                                                                
                                                                                                                
                                                                                                                
                                                                                                                <a class="add_new_row" id="add_variety_0_<?php echo $products["gid"]; ?>_<?php echo $s; ?>"
                                                                                                                   onclick="addNewVariety(0,<?php echo $products["gid"]; ?>,<?php echo $s; ?>,<?php echo $products['typebox']; ?>,<?php echo $products["growerid"]; ?>,<?php echo $products["prodcutid"]; ?>,<?php echo $products["sizeid"]; ?>)"
                                                                                                                   href="javascript:void(0);"><span class="label label-success">Add Variety </span> </a>&nbsp;
                                                                                                                   
                                                                                                                <a href='javascript:void(0)' id='edit_variety_0_<?php echo $products["gid"]; ?>_<?php echo $s; ?>'
                                                                                                                   onclick='editVariety(0,<?php echo $products["gid"]; ?>,<?php echo $s; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-edit white"></i> Edit </a>
                                                                                                                            
                                                                                                                <a href='javascript:void(0)' id='delete_variety_0_<?php echo $products["gid"]; ?>_<?php echo $s; ?>'
                                                                                                                   onclick='deleteVariety(0,<?php echo $products["gid"]; ?>,<?php echo $s; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-times white"></i> Delete </a>


                                                                                                            </td>
                                                                                                        </tr>


                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <input type="hidden" name="total_a" id="total_a" value="1"/>
                                                                                                    <input type="hidden" name="total_availabel_product" id="total_availabel_product_<?php echo $s; ?>" value=""/>
                                                                                                    <div class="col-lg-4">
                                                                                                        <div class="error-box">
                                                                                                            <span id="errormsg-<?= $products["gid"] ?>" style="color:#FF0000; font-size:12px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                                                        </div>
                                                                                                        <div class="heading-title heading-border-bottom">
                                                                                                            <h3 style="background-color:transparent!important;">Box Competition</h3>
                                                                                                        </div>

                                                                                                        <div class="progress progress-lg"><!-- progress bar -->
                                                                                                            <div class="progress-bar progress-bar-warning progress-bar-striped active text-left" role="progressbar" id="progressbar_id_<?php echo $s; ?>"
                                                                                                                 aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                                                                                <span>Completed 0%</span>
                                                                                                            </div>
                                                                                                        </div><!-- /progress bar -->
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Inside Table-->
                                                                                                <input type="hidden" name="hdn_offer_id_index" id="hdn_offer_id_index" value="<?php echo $cnt_offer; ?>"/>
                                                                                            </form>
                                                                                        </div>


                                                                                        
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $b=0;   ?>
                                                                        <?php $ss=10; ?>                                                                        
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer noborder offer_model_popup">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                                                            
                                                                            <a href="javascript:void(0);" onclick="requestProduct(<?php echo $products["prodcutid"] ?>,<?php echo $i ?>,<?php echo $products["gid"] ?>,<?= $ss+1 ?> )" class="btn btn-primary">Send Request Mix</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        <!-- End Modals 1.1-->
                                                    </td>
                                                    
                                                    
                                                </tr>
                                                <input type="hidden" id="totalRes_<?php echo $i; ?>" name="totalRes_<?php echo $i; ?>" value=""/>
                                                <input type="hidden" id="grower_all_send_number<?php echo $i; ?>" name="grower_all_send_number<?php echo $i; ?>" value=""/>
                                                <?php
                                                $i++;
                                                $s++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        
                        
                            <!--nav id="pagination_nav">
                                <?php
                                $page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
                                echo pagination($num_record, 50, $page);
                                ?>
                            </nav-->
                        
                        

                        </div>
                    </div>
                    <!-- /LEFT -->
                </div>
            </div>
        </div>
    
    
            <nav>
                <ul class="pagination">
                    <?php
                    if ($_POST["startrow"] != 0) {

                        $prevrow = $_POST["startrow"] - $display;

                        print("<li><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a></li>");
                    }
                    $pages = intval($num_record / $display);

                    if ($num_record % $display) {

                        $pages++;
                    }
                    $numofpages = $pages;
                    $cur_page = $_POST["startrow"] / $display;
                    $range = 5;
                    $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                    $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                    $page_min = $cur_page - $range_min;
                    $page_max = $cur_page + $range_max;
                    $page_min = ($page_min < 1) ? 1 : $page_min;
                    $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                    if ($page_max > $numofpages) {
                        $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                        $page_max = $numofpages;
                    }
                    if ($pages > 1) {
                        print("&nbsp;");
                        for ($i = $page_min; $i <= $page_max; $i++) {
                            if ($cur_page + 1 == $i) {
                                $nextrow = $display * ($i - 1);
                                print("<li class='active'><a href='javascript:void();'>$i</a></li>");
                            } else {

                                $nextrow = $display * ($i - 1);
                                print("<li><a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a></li>");
                            }
                        }
                        print("&nbsp;");
                    }
                    if ($pages > 1) {

                        if (!(($_POST["startrow"] / $display) == $pages - 1) && $pages != 1) {

                            $nextrow = $_POST["startrow"] + $display;

                            print("<li><a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a></li> ");
                        }
                    }

                    if ($num_record < 1) {
                        print("<span class='text'>" . $XX . "</span>");
                    }
                    ?></ul>
            </nav>        
            
    
    
    
    </div>
    
    
    
    
        <form method="post" name="frmfprd" action="">
            <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
        </form>        
    
    
</section>

<div id="v1" class="tab-pane fade in <?php echo $tab_active; ?>"></div>


<?php
$tab_active = "";
if ($_REQUEST['subcate_id'] != "") {
    $tab_active = "active";
} elseif ($_REQUEST['v_page'] == "yes") {
    $tab_active = "active";
} ?>
<input type="hidden" name="hdn_dele_date" id="hdn_dele_date" value=""/>
<!--Request Modal-->
<div class="modal fade request_modal_1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <span class="modal-title"
                      id="myLargeModalLabel">We are now sending your request to different growers</span>
                <div class="pull-right" style="width:150px;margin-right:100px;">
                    <div class="progress progress-striped active">
                        <!--Here all number asign as 54 is variable which need to be dynamic-->
                        <div class="progress-bar progress-bar-success" style="width:0%"><span class="count">85</span>&nbsp;Growers
                        </div>
                    </div>
                </div>
            </div>
            <!-- body modal -->
            <div class="modal-body">
                <div>
                    <div class="alert alert-warning notify_1">
                        Your order has been sent to <span id="msg_count">54</span> growers
                    </div>
                    <div class="text-right" style="border-top: rgba(0,0,0,0.02) 1px solid;">
                        <ul class="pagination">
                            <a class="btn btn-purple pull-right btn-sm nomargin-top nomargin-bottom"
                               href="<?php echo SITE_URL . "buyer/my-offers.php" ?>" style="background-color:#8a2b83;">Go to
                                grower's offers</a>
                            <a class="btn btn-success pull-right btn-sm nomargin-top nomargin-bottom"
                               data-dismiss="modal" href="javascript:void(0);">Continue buying</a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal_k"></div>
<!--Request Modal-->
<!-- JAVASCRIPT FILES -->
<script>
    var cant = 0;
    var porcentaje = [];    
</script>
    
<script tpe="text/javascript">
    $(document).ready(function () {
                                                        
        
        
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/request_page_name_mix.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();

                }
            });
        });
        /* 20-10-2016 Chetan*/
        $body = $("body");
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {

                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" && jQuery("#filter_variety").val() == "" && jQuery("#filter_color").val() == "" && jQuery("#filter_grower").val() == "" && jQuery("#filter_size").val() == "" && jQuery("#filter_pack").val() == "") {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option.");
                }
                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_variety = jQuery("#filter_variety").val();
                    var filter_color = jQuery("#filter_color").val();
                    var filter_grower = jQuery("#filter_grower").val();
                    var filter_size = jQuery("#filter_size").val();
                    //var filter_pack=jQuery("#filter_pack").val();

                    var filter_category_text = jQuery("#filter_category option:selected").text();
                    var filter_variety_text = jQuery("#filter_variety option:selected").text();
                    var filter_color_text = jQuery("#filter_color option:selected").text();
                    var filter_grower_text = jQuery("#filter_grower option:selected").text();
                    var filter_size_text = jQuery("#filter_size option:selected").text();
                    //var filter_pack_text=jQuery("#filter_pack option:selected").text();
                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterResults.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size,
                        success: function (data) {

                            jQuery("#tab_for_filter").html("");
                            $("#pagination_fresh").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#listing_product_s").html(data);
                            var pass_delete_f = "";
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }
                            if (filter_color == "") {
                                var filter_color_temp = "0";
                            }
                            else {
                                var filter_color_temp = filter_color;
                            }
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;
                            }
                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp;
                            //alert(pass_delete_f);

                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                            }
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                            }
                            if (filter_color != "") {
                                var pass_click_cate = "'" + filter_color_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);
                            }
                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_grower_filter" cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" value="' + filter_grower + '" />' + filter_grower_text);
                            }
                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_size_filter" cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" value="' + filter_size + '" />' + filter_size_text);
                            }
                            jQuery("#tab_for_filter").show();

                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterPagination.php',
                                data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();
        /* 21-10-2016 */
    });
    function click_Ajax(page_number) {
        var check_value = 0;
        var data_ajax = "";
        if (jQuery("#filter_category").val() == "" && jQuery("#filter_variety").val() == "" && jQuery("#filter_color").val() == "" && jQuery("#filter_grower").val() == "" && jQuery("#filter_size").val() == "" && jQuery("#filter_pack").val() == "") {
            check_value = 1;
        }
        if (check_value == 1) {
            alert("Please select any one option.");
        }
        else {
            var filter_category = "";

            var filter_category = jQuery("#hdn_selected_category_filter").val();
            if (typeof filter_category === "undefined") {
                filter_category = "";
            }
            var filter_variety = jQuery("#hdn_selected_variety_filter").val();
            if (typeof filter_variety === "undefined") {
                filter_variety = "";
            }
            var filter_color = jQuery("#hdn_selected_color_filter").val();
            if (typeof filter_color === "undefined") {
                filter_color = "";
            }
            var filter_grower = jQuery("#hdn_selected_grower_filter").val();
            if (typeof filter_grower === "undefined") {
                filter_grower = "";
            }
            var filter_size = jQuery("#hdn_selected_size_filter").val();
            if (typeof filter_size === "undefined") {
                filter_size = "";
            }
            $body.addClass("loading");
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getFilterResults.php',
                data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size + '&page_number=' + page_number,
                success: function (data) {

                    //jQuery("#tab_for_filter").html("");
                    //$("#pagination_fresh").hide();
                    jQuery('.search_modal_open').modal('hide');
                    jQuery("#listing_product_s").html(data);
                    jQuery("#tab_for_filter").show(); 
                    jQuery.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterPagination.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size + '&page_number=' + page_number,
                        success: function (data) {
                            jQuery("#pagination_nav").html(data);
                            $body.removeClass("loading");
                        }
                    });
                }
            });
        }
    }
	
    function deleteFilter(pass_click_cate, filter_category_text, filter_category, filter_variety_text, filter_variety, filter_color_text, filter_color, filter_grower_text, filter_grower, filter_size_text, filter_size) {
        var a_tag_html = $("#tab_for_filter").find("a").length
        if (a_tag_html == "1") {
            location.reload();
        }
        else {
            if (pass_click_cate == filter_category_text) {
                filter_category = "";
            }
            if (pass_click_cate == filter_variety_text) {
                filter_variety = "";
            }
            if (pass_click_cate == filter_color_text) {
                filter_color = "";
            }
            if (pass_click_cate == filter_grower_text) {
                filter_grower = "";
            }
            if (pass_click_cate == filter_size_text) {
                filter_size = "";
            }

            var check_value = 0;
            var data_ajax = "";
            if (filter_category == "" && filter_variety == "" && filter_color == "" && filter_grower == "" && filter_size == "") {
                check_value = 1;
            }
            if (check_value == 1) {
                alert("Please select any one option.");
            }
            else {
                if (filter_category == "0") {
                    filter_category = "";
                }
                if (filter_variety == "0") {
                    filter_variety = "";
                }
                if (filter_color == "0") {
                    filter_color = "";
                }
                if (filter_grower == "0") {
                    filter_grower = "";
                }
                if (filter_size == "0") {
                    filter_size = "";
                }
                $body.addClass("loading");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>buyer/getFilterResults.php',
                    data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size + '&page_number=1',
                    success: function (data) {
                        $body.removeClass("loading");
                        jQuery("#tab_for_filter").html("");
                        $("#pagination_fresh").hide();
                        jQuery('.search_modal_open').modal('hide');
                        jQuery("#listing_product_s").html(data);

                        var pass_delete_f = "";
                        if (filter_category == "") {
                            var filter_category_temp = "0";
                        }
                        else {
                            var filter_category_temp = filter_category;
                        }
                        if (filter_variety == "") {
                            var filter_variety_temp = "0";
                        }
                        else {
                            var filter_variety_temp = filter_variety;
                        }
                        if (filter_color == "") {
                            var filter_color_temp = "0";
                        }
                        else {
                            var filter_color_temp = filter_color;
                        }
                        if (filter_grower == "") {
                            var filter_grower_temp = "0";
                        }
                        else {
                            var filter_grower_temp = filter_grower;
                        }
                        if (filter_size == "") {
                            var filter_size_temp = "0";
                        }
                        else {
                            var filter_size_temp = filter_size;
                        }
                        pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                        pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                        pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                        pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                        pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp;

                        if (filter_category != "") {
                            var pass_click_cate = "'" + filter_category_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                        }
                        if (filter_variety != "") {
                            var pass_click_cate = "'" + filter_variety_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                        }
                        if (filter_color != "") {
                            var pass_click_cate = "'" + filter_color_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);
                        }
                        if (filter_grower != "") {
                            var pass_click_cate = "'" + filter_grower_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_grower_filter" cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" value="' + filter_grower + '" />' + filter_grower_text);
                        }
                        if (filter_size != "") {
                            var pass_click_cate = "'" + filter_size_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_size_filter" cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" value="' + filter_size + '" />' + filter_size_text);
                        }
                        jQuery("#tab_for_filter").show();
                        jQuery.ajax({
                            type: 'post',
                            url: '<?php echo SITE_URL; ?>buyer/getFilterPagination.php',
                            data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size + '&page_number=1',
                            success: function (data) {
                                jQuery("#pagination_nav").html(data);
                                jQuery(".ajax_loader_s").hide();
                            }
                        });
                    }
                });
            }
        }


    }
    $(function () {

        $('input:radio[name=order_id]').change(function () {
            var val = this.value;
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/redirectrequest_pending.php',
                data: 'id=' + val,
                success: function (data) {
                    if (data == 'true') {
                        window.location.href = '<?php echo SITE_URL; ?>buyer/request_bunch_mix.php';
                    }

                }
            });

        });
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    $('.imgDiv').hide();
                    $("#show_hide").toggle("slow");
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
    /*Button send request */
    function requestProduct(productId, i,gid,ss) {
         alert(productId);
         alert(i);
         
        var shippingFee = $('#shippingFee_' + productId + '_' + i).val();
        var conType = $('#conType_' + productId + '_' + i).val();
        //var sizeId = $('#sizeId_' + productId + '_' + i).val();
        var sizeId = '3';
        var featureId = $('#featureId_' + productId + '_' + i).val();
        var box_quantity = qty_boxtype;
        // productId
        var idProduct = '4326';
        var cId = $('#productComment_' + productId + '_' + i).val();
        var buyer = '<?= $_SESSION["buyer"]; ?>'; 
        var delDate = "";        
        var dateRange = "";        
        var growers = $("#grower_all_send_number" + i).val();  
        var productName = $('#productName_' + productId + '_' + i).val();
        var shippingMethod = "";
        var buyerPrice = $('#buyer_' + productId + '_' + i).val();        
        var comment_pro = 'Box Mix';            
        var order_val = $('#box_order_' + productId + '_' + i + ' :selected').val();
        var type_box = $('#box_type_' + productId + '_' + i + ' :selected').val();        
        var tax = $('#tax_' + productId + '_' + i).val();        
        var ship_p = $('#shipping_' + productId + '_' + i).val();    
        var hand_l = $('#handling_' + productId + '_' + i).val();            
        var noofstems = '25';            
        var type = $('#type_req_' + productId + '_' + i).val();        
        var tag = $('#tag_' + productId + '_' + i).val();                        
        var req_grow = $('#grow_' + productId + '_' + i).val();          
        
        ///////////////////////////////////////////////////////                        

        var totalRes = $('#totalRes_' + i).val();            
        var qty_boxtype = $('#box_quantity_' + productId + '_' + i).val() + "_" + $('#box_type_' + productId + '_' + i).val();        

        jQuery('.notify_1').hide();
        jQuery(".price_modal_" + i).modal('toggle');
        jQuery(".request_modal_1").modal('show');
        var flag_s = true;                             
        
        if (order_val != "") {
            $('#erMsg').hide();
        }else {
            flag_s = false;
            alert("Please select Order");            
            $('#erMsg').show();
        }
        
    if (flag_s == true) { 
        
        
        alert("dos");
                
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/request_product_ajax.php',
            data: 'shippingFee=' + shippingFee + 
                  '&conType=' + conType + 
                  '&sizeId=' + sizeId + 
                  '&featureId=' + featureId +
                  '&box_quantity=' + box_quantity + 
                  '&productId=' + idProduct + 
                  '&cId=' + cId + 
                  '&buyer=' + buyer + 
                  '&delivery_date=' + delDate + 
                  '&date_range=' + dateRange + 
                  '&growers=' + growers + 
                  '&productName=' + productName + 
                  '&shippingMethod=' + shippingMethod + 
                  '&buyerPrice=' + buyerPrice + 
                  '&comment_pro=' + comment_pro + 
                  '&order_val=' + order_val + 
                  '&type_box=' + type_box +
                  '&tax=' + tax + 
                  '&ship_p=' + ship_p + 
                  '&hand_l=' + hand_l + 
                  '&noofstems=' + noofstems + 
                  '&type=' + type + 
                  '&tag=' + tag + 
                  '&req_grow=' + req_grow,       
            success: function (data_s) {
  
                console.log(data_s);
                if (data_s == 'true') {
                    jQuery("#msg_count").html("0");
                    $(".count").html(totalRes);
                    setTimeout(function () {
                        jQuery("#msg_count").html(totalRes);
                        jQuery('.notify_1').show();
                    }, 7000);
                    jQuery(".progress-bar").animate({width: "100%"}, 6000);
                    $('.count').each(function () {
                        $(this).prop('Counter', 0).animate(
                            {Counter: $(this).text()},
                            {
                                duration: 7000, easing: 'swing', step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                            });
                    });
                    
                    modifyOffer(gid,ss);
                    
                    location.reload();
                } else {
                    alert('There is some error. Please try again 1');
                }
        

            }
        });
    }

    }
    
    
    function getGrowerPrice(id, ship, tax, val, i) {
        if (!$.isNumeric(val)) {
            $('#err_msg_' + i).html('Please enter only numeric value.').show();
        } else {
            $('#err_msg_' + i).hide();
            val = val * 1;
            var prc = val.toFixed(2);
            var lgt = $('#totalRes_' + id).val();
            for (var j = 1; j <= lgt; j++) {
                var totalPrice = $('#tot_' + id + '_' + j).html();
                var growerPrice = $('#grP_' + id + '_' + j).html();
                var shippingPrice = 0;
                var taxPrice = 0;
                var handlingPrice = $('#handling_' + id + '_' + j).html();
                if (ship == 'yes') {
                    shippingPrice = $('#ship_' + id + '_' + j).html();
                }
                if (tax == 'yes') {
                    taxPrice = $('#tax_' + id + '_' + j).html();
                }
                var totPrc = (prc * 1) + (shippingPrice * 1) + (taxPrice * 1) + (handlingPrice * 1);
                $('#tot_' + id + '_' + j).html(totPrc.toFixed(2));
                $('#grP_' + id + '_' + j).html(prc);
            }
        }
    }
    function addcomment(pId, i) {
        console.log("addcomment");
        var buyer_price = $('#buyer_price_' + pId + '_' + i).val();
        if (buyer_price == '') {
            $('#err_msg_' + i).html('Please enter your price.').show();
        } else if (!$.isNumeric(buyer_price)) {
            $('#err_msg_' + i).html('Please enter only numeric value.').show();
        } else {
            $('#err_msg_' + i).hide();
            var text = $('#comment_' + pId + '_' + i).val();
            $('#productComment_' + pId + '_' + i).val(text);
            $('#buyerPrice_' + pId + '_' + i).val(buyer_price);
            $('#close_' + pId + '_' + i).click();
        }

    }
    function boxQuantityChange(id, cartid, main_tr) {
        
        
            //alert(cartid);
              
            //alert(id-1);
            //var idant = id-1;
                        
                $('#productsize_' + cartid + "-" + id ).select2({
                    ajax: {
                        url: "search_variety_boxmix.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s: cartid,
                                //s:$('#productsize_' + cartid + "-" + id + ' option:selected').val(),
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });           
                
                //console.log($('#productsize_' + cartid + "-" + id ));                                  
        
        
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val = $("#request_qty_box_" + main_tr + " option:selected").val();
        
        	//console.log(selected_val);
                //console.log($.trim(selected_text));  
                
		var nbox = selected_val.split('-');
                var tbox = $.trim(selected_text).split(" ");
                
		$('input[name=boxcant]').val(nbox[0]);
                
                $('input[name=boxtypen]').val(tbox[1]);
                
          //                      
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);
        
        $("#productsize_" + cartid + "-" + id).removeAttr("disabled");
        $("#bunchqty-" + cartid + "-" + id).removeAttr("disabled");
        $("#price-" + cartid + "-" + id).show();
        $("#requestb-" + cartid + "-" + id).removeAttr("disabled");        
        $("#tmarquet-" + cartid + "-" + id).removeAttr("disabled");        
        
        $("#price_label_" + cartid + "-" + id).hide();
        
        $("#price-" + cartid + "-" + id).removeAttr("disabled");
        $("#productsize_" + cartid + "-" + id).removeClass("disabled-css");
           $("#bunchqty-" + cartid + "-" + id).removeClass("disabled-css");
        $("#requestb-" + cartid + "-" + id).removeClass("disabled-css");        
        $("#tmarquet-" + cartid + "-" + id).removeClass("disabled-css");        
        
        $("#price-" + cartid + "-" + id).removeClass("disabled-css");
                         
    }
    function modelClick(product_id, i, bname, sizeid, feature, bunchsizeid, chargesPerKilo, sizename, cate_id, loop_number) {
        var box_val = jQuery("#change_page_size_" + product_id + "_" + i).val();
        var box_val = '1';
        if (box_val == "") {
            alert("Please select box qty.");
        }
        else {
            var total_shipping = "";
            var split_box_val = box_val.split("_");
            var select_box_type = split_box_val[1] + "_" + split_box_val[2];
            var select_box_type = "";
            if (split_box_val[1] + "_" + split_box_val[2] == "eighth_box") {
                select_box_type = "EB";
            }
            else if (split_box_val[1] + "_" + split_box_val[2] == "quarter_box") {
                select_box_type = "QB";
            }
            else if (split_box_val[1] + "_" + split_box_val[2] == "half_box") {
                select_box_type = "HB";
            }
            else if (split_box_val[1] + "_" + split_box_val[2] == "jumbo_box") {
                select_box_type = "JB";
            }
            var shippingMethod = $('#shipping_id_' + product_id + '_' + i).val();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getProductGrowers.php',
                data: 'select_box_type=' + select_box_type + '&product_id=' + product_id + '&sizeid=' + sizeid +
                '&bname=' + bname + '&feature=' + feature + '&bunchsizeid=' + bunchsizeid + '&chargesPerKilo=' +
                chargesPerKilo + '&sizename=' + sizename + '&cate_id=' + cate_id + '&shippingMethod=' + shippingMethod,
                dataType: "html",
                success: function (data) {
                    var split_data = data.split("######");
                    jQuery("#price_model_" + loop_number + " tbody").html(split_data[0]);
                    jQuery(".select_price_cls").val(split_data[1]);
                    jQuery("#totalRes_" + i).val(split_data[2]);
                    jQuery("#grower_all_send_number" + i).val(split_data[3]);

                }
            });
        }
    }
    function orderChange(product_id, sizename, i) {
        var order_val = $('#box_order_' + product_id + '_' + i + ' :selected').val();
        var shipcost;
        if (order_val != "") {
            $.ajax({
                async: false,
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getshipcost.php', 
                data: 'order_val=' + order_val,
                success: function (data) {
                    shipcost = data;
                }
            });

        }
               
        if (shipcost == 0) {
            $('.elmn').addClass('noRow');
        }
        else {
            $('.elmn').removeClass('noRow');
        }
    }
    // Select  Shippinp option
    function shippingChange(product_id, sizename, i) {
        var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
        if (shipping_val != "") {
            $('#erMsg').hide();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/get_date_modal.php',
                data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
                success: function (data) {
                    $('.cls_date_start_date').html(data);
                    _pickers();//show calendar
                }
            });

        } else {
            $('#erMsg').show();
        }
    }
    //Modified  final Price
   function modelClick_New(product_id, i, bname, sizeid, feature, bunchsizeid, chargesPerKilo, sizename, cate_id, loop_number) {
        $('.request_product_modal_hide').hide();
        $('.request_product_modal_hide_footer').hide();
        $('.final_price_section').css("display", "inline-block");
        var price_id = "#buyer_price_" + product_id + "_" + i;
        var price = $(price_id).val();
        if (price > 0 && price != "") {
            var box_val = jQuery("#box_type_" + product_id + "_" + i).val();
            var order_val = $('#box_order_' + product_id + '_' + i + ' :selected').val();
            /*var shippingMethod = $('#shipping_id_' + product_id + '_' + i).val();
             console.log(shippingMethod);*/

            var select_box_type = $('#box_type_' + product_id + '_' + i + ' :selected').val();
            var box_val = '1';
            if (box_val == "") {
                alert("Please select box qty.");
            }
            else {
                var total_shipping = "";
                var shippingMethod = $('#shipping_id_' + product_id + '_' + i).val();
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>buyer/getProductGrowers.php',
                    data: 'select_box_type=' + select_box_type + '&product_id=' + product_id + '&sizeid=' + sizeid + '&bname=' + bname + '&feature=' + feature +
                    '&bunchsizeid=' + bunchsizeid + '&chargesPerKilo=' + chargesPerKilo + '&sizename=' + sizename + '&cate_id=' + cate_id +
                    '&shippingMethod=' + shippingMethod + '&order_val=' + order_val + '&price=' + price,
                    dataType: "html",
                    success: function (data) {
                        var split_data = data.split("######");
                        jQuery("#price_model_" + loop_number + " tbody").html(split_data[0]);
                        jQuery(".select_price_cls").val(split_data[1]);
                        jQuery("#totalRes_" + i).val(split_data[2]);
                        jQuery("#grower_all_send_number" + i).val(split_data[3]);
                        $("#buyer_" + product_id + "_" + i).val(split_data[8]);
                        $("#grower_" + product_id + "_" + i).val(split_data[9]);
                        $("#tax_" + product_id + "_" + i).val(split_data[10]);
                        $("#shipping_" + product_id + "_" + i).val(split_data[11]);
                        $("#handling_" + product_id + "_" + i).val(split_data[12]);

                    }
                });

            }


        }
        else {

            alert("PLEASE FILL  THE GROWER PRICE");
        }
    }
     function backPricePopup() {
        $(".request_product_modal_hide").show();
        $(".request_product_modal_hide_footer").show();
        $(".final_price_section").hide();
    }
    function savePricePopup(prodcutid, index) {
        console.log(prodcutid);
        console.log(index);
        var price_section_txt = $("#price_section_" + prodcutid + "_" + index).val();
        if (price_section_txt != "") {
            $(".request_product_modal_hide").show();
            $(".request_product_modal_hide_footer").show();
            $(".final_price_section").hide();
            $("#buyer_price_" + prodcutid + "_" + index).val(price_section_txt);
        }
        else {
            alert("Please Enter Price!!!");
        }
    }
    
       function funPage(pageno) {
            document.frmfprd.startrow.value = pageno;
            document.frmfprd.submit();
        }
        
        
        
    function changeCMSize(id , cat_id, main_tr, boxtype, userSessionID, product, sizeid) {
                

        var qty = $("#request_qty_box_" + main_tr + " option:selected").val();
        if (qty == "") {
            qty = $(".cls_hidden_selected_qty").val();
        }

        if (qty != "") {
           
             var codid = $("#productsize_" + cat_id  + "-" + id + " option:selected").attr("cod_id");
             
            jQuery.ajax({
                url: '/growers/ajax_bunch_standard.php',
                type: 'post',
                data: {'boxtype': boxtype, 'userSessionID': userSessionID, 'product': product, 'sizeid': sizeid, 'codid': codid, 'qty': qty},
                success: function (data) {
					 console.log(data);
                    if (jQuery.trim(data) == "0") {
                        alert("Your quantity level is : 0");
                    }
                    else {                        
                        
                        $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cat_id).val(data);
                    }

                }
            });
        }else{
            alert("Please select box quantity.");
        }

    }

    function changeBunchQty(id, cartid, main_tr) {

        var selected_val = $("#bunchqty-" + cartid + "-" + id + " option:selected").val();//cantidad selecionada
        total_qty_bunches = $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cartid).val();//total de bunches q me permite la caja
        $("#boxqty_" + id + "_" + main_tr + "_" + cartid).val(selected_val);
        
        var total_selected_val = 0;
        var this_box = 0;	
        var total = $("#total_a").val();
		
		for (var i = 0; i < total; i++) {
                            this_box= parseInt($("#boxqty_"+i+"_"+main_tr+"_"+cartid).val());
                            total_selected_val = parseInt(total_selected_val) + this_box;
		}
                                               		
        var nfilas = $("#main_table_" + main_tr + " tbody tr").length;
        var total_qty = total_qty_bunches;
        
        if (parseInt(this_box) > parseInt(total_qty)) {
            alert("Your bunch quantity greater than requested bunch qty.");
            alert(this_box);
            alert(total_qty);
        }else{
            

            $("#total_availabel_product_" + main_tr).val(total_selected_val);
            var per_val = parseInt(this_box) * 100 / parseInt(total_qty);
            porcentaje[nfilas - 1] = per_val;

            var porcentajes = 0;
            for (var i = 0; i < nfilas; i++) {
                porcentajes = porcentajes + porcentaje[i];
            }

            $("#progressbar_id_"+main_tr).css("width",parseFloat(per_val).toFixed(2)+"%");
             $("#progressbar_id_"+main_tr).html("<span>Completed "+parseFloat(per_val).toFixed(2)+"%</span>");

            $("#progressbar_id_" + main_tr).css("width", parseFloat(porcentajes).toFixed(2) + "%");
            $("#progressbar_id_" + main_tr).html("<span>Completed " + parseFloat(porcentajes).toFixed(2) + "%</span>");
        }
    }   
    
    function addNewVariety(id, cat_id, main_tr, boxtype, userSessionID, product, sizeid) {
    
    
    
        var total_qty = "<?php echo $total_qty_bunches;?>";
        var total_selected_val = $("#total_availabel_product_" + main_tr).val();
        
        if (parseInt(total_selected_val) > parseInt(total_qty)) {
            alert("Your bunch quantity greater than requested bunch qty.");
        }else{
            var productsize = $("#productsize_" + cat_id + "-" + id).val();
                  var bunchqty = $("#bunchqty-" + cat_id + "-" + id).val();            
            
            //var price = $("#price-" + cat_id + "-" + id).val();
            //var requestb = $("#requestb-" + cat_id + "-" + id).val();
            //var tmarquet = $("#tmarquet-" + cat_id + "-" + id).val();
            
            if (productsize == "") {
                $("#productsize_" + cat_id + "-" + id).css("border", "2px solid red");
            }
            else if (bunchqty == "") {
                $("#bunchqty-" + cat_id + "-" + id).css("border", "2px solid red");
            }                                   
            else{
                $("#productsize_" + cat_id + "-" + id).attr("disabled", "disable");
                $("#bunchqty-" + cat_id + "-" + id).attr("disabled", "disable");

                $("#productsize_" + cat_id + "-" + id).addClass("disabled-css");
                $("#bunchqty-" + cat_id + "-" + id).addClass("disabled-css");

                var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
                var conta_fil = parseInt($("#total_a").val()) + 1;

                $("#total_a").val(conta_fil);                                

                var box_qty_cnt = $("#main_table_" + main_tr + " tbody tr").length;
                box_qty_cnt = parseInt(box_qty_cnt) - 1;
                $("#request_qty_box_" + main_tr).attr("onchange", "boxQuantityChange(" + total_tr + "," + cat_id + "," + main_tr + ")");

                var productsize_html = $("#productsize_" + cat_id + "-0").html();
                var first_td = "<td><select class='listmenu'  style='margin-top:10px; height:35px; padding:3px; width:450px;border-radius: 5px;'  name='productsize[]' onchange='changeCMSize(" + total_tr + "," + cat_id + "," + main_tr + "," + boxtype + "," + userSessionID + "," + product + "," + sizeid + ")' id='productsize_" + cat_id + "-" + total_tr +  "'>" + productsize_html + "</select></td>";
                
                var bunchqty_html = $("#bunchqty-" + cat_id + "-0").html();
                var second_td = "<td><select class='form-controll offer-select' onchange='changeBunchQty(" + total_tr + "," + cat_id + "," + main_tr + ")' name='bunchqty[]' id='bunchqty-" + cat_id + "-" + total_tr + "'>" + bunchqty_html + "</select></td>";                
                
                var fifth_td = "<td><a href='javascript:void(0)' id='add_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='addNewVariety(" + total_tr + "," + cat_id + "," + main_tr + "," + boxtype + "," + userSessionID + "," + product + "," + sizeid + ")' class='add_new_row'><span class='label label-success'>Add Variety </span></a>&nbsp;<a class='btn btn-default btn-xs' href='javascript:void(0)' id='edit_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='editVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-edit white'></i> Edit </a><a class='btn btn-default btn-xs' href='javascript:void(0)' id='delete_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='deleteVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-times white'></i> Delete </a><input type='hidden' name='boxqty[]' class='boxqty" + main_tr + "' id='boxqty_" + total_tr + "_" + main_tr + "_" + cat_id + "' value='0' /><input type='hidden' name='total_qty_bunches[]'  id='total_qty_bunches_" + total_tr + "_" + main_tr + "_" + cat_id + "' value='0' /></td>";
                $("#main_table_" + main_tr).append("<tr id='product_tr_" + total_tr + "'>" + first_td + second_td + fifth_td + "</tr>");
            }
            
        }
                
        
      //////////////////////////////////////////  
      

      
         boxQuantityChange(total_tr, cat_id, main_tr);
        
        
    }
    
    function modifyOffer(idof,main_tr) {
    
        var check = 0;
        
        <?php
            $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
            $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
            $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>
                
                      
        if ($('#productsize-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please select Product");
            check = 1;
        }
        
        if ($('#price-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please enter your Price");
            check = 1;
        }else if ($('#price-' + idof + '-0').val() != "") {
            if ($('#price-' + idof + '-0').val() < <?= $minimum_price["mprice"] ?>) {
                $('#errormsg-' + idof).html("price should be greater than <?= $minimum_price["mprice"] ?> ")
                check = 1;
            }
        }

        if ($('#requestb-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please enter Requests");
            check = 1;
        }
        
        if ($('#tmarquet-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please enter Type Market");
            check = 1;
        }        


        if ($('#boxtype-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please select Box type");
            check = 1;
        }else if ($('#bunchsize-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please select Bunch size");
            check = 1;
        }else if ($('#bunchqty-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please enter Bunch qty / box ");
            check = 1;
        }else if ($('#boxqty-' + idof + '-0').val() == "") {
            $('#errormsg-' + idof).html("please enter Box qty");
            check = 1;
        }
        
    
        var nombres=[];
        var medidas=[];   
        var cajas=[];
        var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
        
        for (var i=0; i<=main_tr; i++) {   

            var prod_name = $("#productsize-" + idof + "-" + i + " option:selected").attr("prod_name");  
            var s_name = $("#productsize-" + idof + "-" + i + " option:selected").attr("s_name");  
            var b_type = $("#productsize-" + idof + "-" + i + " option:selected").attr("b_type");  
            
            console.log("Prueba " + prod_name); 
            
            nombres[i]=prod_name;
            medidas[i]=s_name;            
            cajas[i]=b_type;  
            
            console.log("Variedad " + nombres + main_tr + total_tr); 
            console.log("prod_name " + prod_name); 
        }
        
       if (check == 0) {
            $('#addanomo').val("1")
            $('#errormsg-' + idof).html("");
         
            
            var k = confirm('Are you sure you want send offer 1');
            if (k == true) {
                $(".disabled-css").removeAttr("disabled");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>growers/buyers-send-offers-ajax-ver1.php',
                    data: [$('form#sendoffer' + idof).serialize()]+ '&prod_name=' + nombres + '&s_name=' + medidas + '&b_type=' + cajas,                    
                            success: function (data) {
                        var fichas = data.split('######');
						 jQuery(".progress-bar").animate({width: "100%"}, 6000);
                        $('.count').each(function () {
                            $(this).prop('Counter', 0).animate(
                                {Counter: $(this).text()},
                                {
                                    duration: 7000, easing: 'swing', step: function (now) {
                                    $(this).text(Math.ceil(now));
                                }
                                });
                        });
                        console.log("este  es el  mensaje de  cuanto  esta  enviando:" + fichas[0]);
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
           }
        }
    
    }    
</script>
<script type='text/javascript'> 
 $(document).ready(function () {
								
        $('#loading').css("display", "none");
    });
		 

    $(".load").click(function (event) {
        $('#loading').css("display", "block");
    });									  
</script>

<?php include("../includes/footer_new.php"); ?>