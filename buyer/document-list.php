<?php

$factId = $_GET["idi"];

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');


$userSessionID = $_SESSION["buyer"];

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}

/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$page_request = "buyer_invoices";
?>
<?php include('../back-end/inc/header_ini.php'); ?>
<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php');?>




<div class="flex-fill" id="middle">
  <div class="page-title bg-transparent b-0">
    <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
      <strong> Document List </strong>
    </h1>
  </div><!-- Primary-->
  <section class="rounded mb-3 bg-white" id="section_1">
    <!-- graph header -->
    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">


            <!-- panel content -->

                          <div class="panel-body">
                              <div class="table-responsive">
                                <table class="table-datatable table table-bordered table-hover table-striped"
                                        data-lng-empty="No data available in table"
                                        data-responsive="true"
                                        data-header-fixed="true"
                                        data-select-onclick="true"
                                        data-enable-paging="true"
                                        data-enable-col-sorting="true"
                                        data-autofill="false"
                                        data-group="false"
                                        data-items-per-page="10">
                                            <thead>
                                              <tr>
                                                <th>#</th>
                                                <th>Document</th>
                                                <th>Description </th>
                                                <th> </th>
                                                <th>View</th>
                                                <th>Delete</th>
                                              </tr>
                                      </thead>

                                      <tbody>
                            <?php
                            $sql = "select id , id_fact , document_path , description
                                      from invoice_document
                                     where id_fact = '".$factId."' ";

				$invoices_res = mysqli_query($con, $sql);

                                $ii=0;

				while ($invoice = mysqli_fetch_assoc($invoices_res)) {

                                    $filedoc = substr($invoice['document_path'],4,22);
                                    $ii++;

                                    if ($invoice['description'] == 1) {
                                        $typedesc = 'Farm Invoice';
                                    }elseif ($invoice['description'] == 2) {
                                        $typedesc = 'Commercial Invoice';
                                    }elseif ($invoice['description'] == 3) {
                                        $typedesc = 'Master Airwaybill';
                                    }elseif ($invoice['description'] == 4) {
                                        $typedesc = 'House Airwaybill';
                                    }

			    ?>
							<tr>
                                                                <td><?php echo $ii; ?></td>
                                                                <td><?php echo $factId; ?></td>
								                                                <td><?php echo $typedesc; ?></td>
                                                                <!-- <td><?php //echo $filedoc; ?></td> -->
      <td><embed src="/<?php echo $invoice['document_path']; ?>" type="application/pdf" width="100%" height="400px" /><td>
      <!--  <td><a title="Visualizar Archivo" href="/<?php //echo $invoice['document_path']; ?>" download="<?php //echo $filedoc; ?>" style="color: blue; font-size:18px;"> <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> </a></td> -->
      <!--  <td><a title="Descargar Archivo" href="/<?php //echo $invoice['document_path']; ?>" download="<?php //echo $filedoc; ?>" style="color: blue; font-size:18px;"> <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> </a></td> -->
      <td><a title="Eliminar Archivo" href="Eliminar.php?name=../<?php echo $invoice['document_path']; ?>&id=<?php echo $invoice['id'];?>" style="color: red; font-size:18px;" onclick="return confirm('Are you sure, you want to delete this Document ?');"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>

							</tr>
							<?php

                            }
                            ?>
                          </tbody>
                          <tr>
                            <th>#</th>
                            <th>Document</th>
                            <th>Description </th>
                            <th> </th>
                            <th>View</th>
                            <th>Delete</th>
                          </tr>
                      </table>

                  </div>
              </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
  </div><!-- /MIDDLE -->


</div><!-- FOOTER -->
</section>

<?php include('../back-end/inc/footer.php'); ?>
<script src="../back-end/assets/js/blockui.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
