<?php
session_start();


require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');

$idbuyer = $_GET['id_buy'];
$fac_id  = $_GET['id_fact'];

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}


/* * *******get the data of session user*************** */
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);


if (isset($_REQUEST["total"])) {

    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {


        $update = "update grower_offer_reply
                      set price    = '" . $_POST["price-" . $_POST["pro-" . $i]]   . "' ,
                          bunchqty = '" . $_POST["bunch-" . $_POST["pro-" . $i]]   . "' ,
                          steams   = '" . $_POST["stems-" . $_POST["pro-" . $i]]   . "' ,
                          size     = '" . $_POST["size-" . $_POST["pro-" . $i]]   . "'  ,
                          eliminar = '" . $_POST["elimina-" . $_POST["pro-" . $i]]. "'                                       
                    where id ='" . $_POST["pro-" . $i] . "'   ";


        mysqli_query($con, $update);

    }
}

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 01 Jul 2021
Structure MarketPlace previous to buy
**/


// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

include('../back-end/inc/header_ini.php');
?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<?php

$sel_products = "select gor.id  as gid , gor.offer_id , gor.offer_id_index , gor.grower_id  , gor.size , gor.price ,
                       gor.steams       , gor.product  , gor.boxqty         , gor.buyer_id   , gor.grower_id  ,
                       gor.bunchqty     , gor.boxtype  , gor.status         , gor.request_id , g.growers_name as gname,
                       gor.type_market  , gor.product_subcategory           , br.id_order    , gor.cliente_id ,
                       substr(cl.name,1,12) as namecli,p.color_id,c.name as colorname, gor.reserve , gor.numship ,
                       gor.invoice
                  from grower_offer_reply gor
                 inner join growers g on gor.grower_id = g.id
                 inner join buyer_requests br on gor.offer_id = br.id
                  left JOIN product p  ON gor.product = p.name and gor.product_subcategory = p.subcate_name
	          left join colors c on p.color_id=c.id      
                  left join sub_client cl ON cl.id = gor.cliente_id                 
                 where gor.buyer_id = '" . $idbuyer . "'
                   and br.id_order  = '" . $fac_id  . "'   ";


$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;
$display = 150;
$XX = '<div class="notfound">No Item Found !</div>';


function initial($fid, $init, $display,$bid ){
    $query = "select gor.id  as gid , gor.offer_id , gor.offer_id_index , gor.grower_id  , gor.size , gor.price ,
                       gor.steams       , gor.product  , gor.boxqty         , gor.buyer_id   , gor.grower_id  ,
                       gor.bunchqty     , gor.boxtype  , gor.status         , gor.request_id , g.growers_name as gname,
                       gor.type_market  , gor.product_subcategory           , br.id_order    , gor.cliente_id ,
                       substr(cl.name,1,12) as namecli,p.color_id,c.name as colorname, gor.reserve , gor.numship,
                       p.subcategoryid , eliminar, gor.invoice
                  from grower_offer_reply gor
                 inner join growers g on gor.grower_id = g.id
                 inner join buyer_requests br on gor.offer_id = br.id
                  left JOIN product p  ON gor.product = p.name and gor.product_subcategory = p.subcate_name
	          left join colors c on p.color_id=c.id      
                  left join sub_client cl ON cl.id = gor.cliente_id                 
                 where gor.buyer_id = $bid
                   and br.id_order  = '" . $fid  . "'
                 order by g.growers_name ";

    return $query;
}




if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = initial($fac_id, $_POST["startrow"], $display,$idbuyer);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = initial($fac_id, 0, $display , $idbuyer);
    $result2 = mysqli_query($con, $query2);
}


 $display;
?>
<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Offers </strong>
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->



						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


  <div class="modal-body">
              <!-- panel content -->
              <form name="frmrequest" id="frmrequest" method="post" action="">
                  <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                  <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

                  <div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
                  <input type="submit" id="submitu" class="btn btn-success mb-3 mt-3 d-block-xs w-100-xs" name="submitu" value="Update All">


                    <!-- fullscreen -->
                          <a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
                            <span class="group-icon">
                              <i class="fi fi-expand"></i>
                              <i class="fi fi-shrink"></i>
                            </span>
                          </a>
                  </div>

                  <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table-datatable table table-bordered table-hover table-striped"
                                data-lng-empty="No data available in table"
                                data-responsive="true"
                                data-header-fixed="true"
                                data-select-onclick="true"
                                data-enable-paging="true"
                                data-enable-col-sorting="true"
                                data-autofill="false"
                                data-group="false"
                                data-items-per-page="100">
                                    <thead>
                                      <tr>
                                        <th>Grower</th>
                                        <th>Variety</th>
                                        <th>Inv</th>
                                        <th>Bun</th>
                                        <th>Stems</th>
                                        <th>Price</th>
                                        <th>Size</th>
                                        <th></th>
                                      </tr>
                              </thead>

                              <tbody>
                              <?php
                              $i = 1;

                              while ($producs = mysqli_fetch_array($result2)) {


                                  ?>


                                  <tr>
                                      <td><?php echo $producs["gname"]; ?>  </td>
                                      <td><?php echo $producs["product"]." ".$producs["product_subcategory"]." ".$producs["colorname"]."(".$producs["reserve"].")";  ?>  </td>
<td><?php echo $producs["invoice"]; ?>  </td>                                      
                                      <td><input type="text" class="form-control" name="bunch-<?php echo $producs["gid"] ?>" id="bunch-<?php echo $producs["gid"] ?>" value="<?php echo $producs["bunchqty"] ?>" style="margin-top:5px; height:48px; padding:3px; width:50px;border-radius: 5px;"></td>                                      
                                      <td><input type="text" class="form-control" name="stems-<?php echo $producs["gid"] ?>" id="stems-<?php echo $producs["gid"] ?>" value="<?php echo $producs["steams"] ?>"></td>

                                      <td><input type="text" class="form-control" name="price-<?php echo $producs["gid"] ?>" id="price-<?php echo $producs["gid"] ?>" value="<?php echo $producs["price"] ?>"></td>

                                    <td>
                                         <select style="width: 100%; diplay: none;" name="size-<?php echo $producs["gid"] ; ?>" id="size-<?php echo $producs["gid"] ; ?>" class="form-control" >
                                                        <option value="">--Select--</option>

                                                <?php 	$sel_size="select pr.size , s.name as namesize , pr.stem_bunch, bs.name as namebunch,
                                                                      f.name as namefeature
                                                                     from grower_parameter pr
                                                                    inner join sizes s on pr.size = s.id
                                                                    inner join bunch_sizes bs on pr.stem_bunch = bs.id
                                                                     left JOIN features f  ON pr.feature = f.id   
                                                                    where pr.idsc = '" . $producs["subcategoryid"] . "'
                                                                      and bs.name = '" . $producs["steams"] . "'
                                                                    order by (-1*s.name) desc";

                                                        $rs_size=mysqli_query($con,$sel_size);

                                                        while($sizecat=mysqli_fetch_array($rs_size)){
                                                ?>
                                                            <option value="<?php echo $sizecat["namesize"]?>" <?php if($producs["size"]==$sizecat["namesize"]) { echo"selected"; } ?> ><?php echo $sizecat["namesize"]?></option>
                                                <?php
                                                         }
                                                ?> 
                                         </select>                                                                        
                                     </td> 
                                     
                                    <td>
                                         <select style="width: 100%; diplay: none;" name="elimina-<?php echo $producs["gid"] ; ?>" id="elimina-<?php echo $producs["gid"] ; ?>" class="form-control" >
                                                <option value="0" <?php  if($producs["eliminar"] == 0) echo "selected";?>>OK</option>
                                                <option value="1" <?php  if($producs["eliminar"] == 1) echo "selected";?>>Delete</option>
                                         </select>                                                                        
<input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $producs["gid"] ?>"/>                                        
                                     </td>                                                                           

                                      <!--td>
                                          
                                      </td-->

                                      
                                  </tr>


                                  <?php $i++;
                              }
                              ?>
                              </tbody>
                              <tr>
                                        <th>Grower</th>
                                        <th>Variety</th>
                                        <th>Inv</th>                                        
                                        <th>Bun</th>
                                        <th>Stems</th>
                                        <th>Price</th>
                                        <th>Size</th>
                                        <th></th>
                              </tr>
                          </table>

                      </div>
                  </div>
                  <input type="hidden" name="totalrow" value="<?php echo $i ?>"/>
              </form>




              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>



                                                    <br>
                                                   <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>




			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
      <script tpe="text/javascript">
          function funPage(pageno) {
              document.frmfprd.startrow.value = pageno;
              document.frmfprd.submit();
          }
          function docolorchange() {
              window.location.href = '/buy.php?id=<?php echo $_GET["id"] ?>&categories=<?php echo $strdelete ?>&l=<?php echo $_GET["l"] ?>&c=' + $('#color').val();
          }
          function frmsubmite() {
              document.frmfilter.submit();
          }
          function doadd(id) {
              var check = 0;
              if ($('#qty-' + id).val() == "") {
                  $('#ermsg-' + id).html("please enter box qty.")
                  check = 1;
              }

              if (check == 0) {
                  $('#ermsg-' + id).html("");
                  $('#gid6').val(id);
                  $('#frmrequest').submit();
              }
          }
      </script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
