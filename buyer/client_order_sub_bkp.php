<?php

// PO 2018-08-24

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["buyer"];


$cliente_inv = $_GET['id_cli'];

$swcab = $_GET['sw'];
$idfac = $_GET['fac_id'];





/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}


//  Actualizacion en Grupo
if (isset($_REQUEST["total"])) {
    
    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
              
            $update = "update order_subclient 
                          set qty_pack  = '" . $_POST["qty_pack-". $_POST["pro-" . $i]] . "'  ,
                              id_state  = '" . $_POST["id_state-". $_POST["pro-" . $i]] . "'  
                        where id='" . $_POST["pro-" . $i] . "'  ";                               
    
            mysqli_query($con, $update);            
                                                
    }
    
  }  
  
  if(isset($_POST["submiso"]) && $_POST["submiso"]=="Add Order")	{	
      
      if ($swcab = 1) {
             $insert_cab="insert into order_subclient_cab set
                            fact_id   = '0'   ,    
                            client_id = '".$_POST["cli_add"]."' ,
                            date_ship = '".$_POST["sname"]."' ,
                            date_del  = '".$_POST["dname"]."' ,                                                            
                            marks     = 'Order Subcli' , 
                            status    = '0'      ";
       
	           mysqli_query($con,$insert_cab);      
                   
                $qryMaxFac ="select (max(id)) as ido from order_subclient_cab";
                $dataMaxFac = mysqli_query($con, $qryMaxFac);
         
                while ($dto = mysqli_fetch_assoc($dataMaxFac)) {
                    $IdMaxFac= $dto['ido'];
		}                          
      }else{
          $IdMaxFac= $idfac;
      }
      

      
       $insert="insert into order_subclient set
                            id_week   = '1'   ,    
                            id_client = '".$_POST["cli_add"]."' ,
                            product   = '".$_POST["var_add"]."' ,
                            prod_name = 'N/A'   ,
                            qty_pack  = '".$_POST["qty_pack"]."',
                            size      = '3' , 
                            steams    = '25'  ,    
                            id_state  = '0'   ,
                            date_ship = '".$_POST["sname"]."' ,
                            date_del  = '".$_POST["dname"]."' ,                            
                            fact_id   = $IdMaxFac   ,    
                            comment = 'CALENDAR'    ";
       
	   mysqli_query($con,$insert);      
  }
      
  
    
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <!--script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script-->
    
<script>       
        
function selectscategory()	{

   removeAllOptions(document.frmrequest.var_add);

	addOption(document.frmrequest.var_add,"","-- Select Variety --");
        
        
	  <?php

		$sel_subcategory="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id , 
                                         gor. product as  productname , 
                                         gor.product_subcategory, gor.size as size_name  , gor.bunchqty       , gor.steams      
                                    from buyer_requests_standing_order br
                                   inner join grower_offer_reply_special gor on gor.offer_id = br.id
                                   inner join product p on gor. product = p.name and gor. product_subcategory = p.subcate_name       ";

		$res_subcategory=mysqli_query($con,$sel_subcategory);

		while($rw_subcategory=mysqli_fetch_array($res_subcategory))	{

	  ?>
                               
                var delDate = $('#cls_date_ship').find("input").val();
                
		//if(document.frmrequest.cls_date_ship.value=="<?php echo $rw_subcategory["lfd"]?>")	{
                
                if(delDate=="<?php echo $rw_subcategory["lfd"]?>")	{

			addOption(document.frmrequest.var_add,"<?php echo $rw_subcategory["codvar"];?>","<?php echo $rw_subcategory["bunchqty"]."-Bunch ".$rw_subcategory["productname"];?>");

			 $('#var_add').val('<?php echo $_POST["var_add"]?>');                                                  
		}
                                              
	  <?php	}   ?>	

	}
                        

	function removeAllOptions(selectbox)	{
		var i;

		for(i=selectbox.options.length-1;i>=0;i--){
			selectbox.remove(i);
		}
	}

	function addOption(selectbox,value,text){

		var optn=document.createElement("OPTION");
		optn.text=text;
		optn.value=value;
		selectbox.options.add(optn);
	}

</script>   

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php
   

    $sqlDetalis="select irs.id  as gid      ,  irs.id_week   , irs.id_client , irs.product, 
                        irs.prod_name , irs.buyer   , irs.grower_id , irs.qty_pack  , irs.size   , 
                        irs.steams    , irs.comment,
                        s.name as psubcatego , p.name as name_product , sz.name as size_name,
                        cli.name as subclient,
                        irs.id_state , irs.fact_id,
                        irs.date_ship  , irs.date_del
                  from order_subclient irs
                 INNER JOIN product p on irs.product = p.id 
                 INNER JOIN sizes sz  on irs.size = sz.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on irs.id_client = cli.id 
                 where irs.fact_id   = '".$idfac."' 
                 order by s.name,p.name ";

    $result = mysqli_query($con, $sqlDetalis);     


 ?>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
</head>
<section id="middle">
            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
			<!--section id="middle"-->

				<!-- page title -->
				<header id="page-header">
					<h1>Customer Order</h1>
					<ol class="breadcrumb">
						<li><a href="#">SubClient</a></li>
						<li class="active">Assign</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
            <div class="panel-heading">

                 <a href="<?php echo SITE_URL; ?>buyer/client_calendar_special.php?id_cli=<?php echo $scli ?>" class="btn btn-success btn-xs relative">Back </a>                

                                 
                 
                 
                <!-- right options -->
                <!--ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul-->
                <!-- /right options -->
                
            </div>                                            

						<div class="panel-body">
                                                    <!--input type="submit" id="submitu" class="btn btn-success btn-sm" name="submitu" value="Update Order"-->     
                                                    <input type="submit" id="submiso" class="btn btn-success btn-sm" name="submiso" value="Add Order">     
							<div class="row">                                                                             
								<div class="col-md-6 col-sm-6 text-left">

								
								<ul class="list-unstyled">
                                                                    
								    <li><strong>Client.............:</strong>
                       
                                                                        <?php   $sql_cli = "select id,name 
                                                                                              from sub_client 
                                                                                             where id != 1 
                                                                                               and buyer = '" . $userSessionID . "'
                                                                                             order by name";
                                                                                             
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                        ?>	                            
                                                                            
                                                                        <select name="cli_add" id="cli_add" onclick="checkOptionship();" style="margin-top:10px; height:35px; padding:3px; width:250px;" class="listmenu"  >
                                                                                    <option value="">--Select--</option>
                                                                            <?php				       
                                                                                while ($row_client = mysqli_fetch_assoc($result_cli)) {
                                                                            ?>                       
                                                                                     <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                            <?php    }	?>		
                                                                        </select>
                                                                    </li>
                                                                    
                                                                    
                            <li><strong>Delivery Date:</strong>
                                    <label class="field cls_date_start_dated" id="cls_date_del">
                                                <input value="" class="form-control required start_date" placeholder="Select Date Delivery" style="width: 250px!important;text-indent: 32px;border-radius: 5px;" type="text">
                                    </label>                            
                            </li>                                                                                                                                                                                                                                                                                                                                                                         

                            <li><strong>Ship Date.......:</strong>
                                    <label class="field cls_date_start_dates" id="cls_date_ship" name="cls_date_ship" >
                                                <input value="" class="form-control required start_date" placeholder="Select Date Shipment" style="width: 250px!important;text-indent: 32px;border-radius: 5px;" type="text" value="<?php echo $_POST["cls_date_ship"];?>" >
                                    </label>                            
                            </li>     

									<!--li><strong>Label..............:</strong>
                                                                                                   
                                                                        <select name="qty_pack1" id="qty_pack1"  onclick="selectscategory();"  style="margin-top:10px; height:35px; padding:3px; width:250px;" class="listmenu" >
                                                                                <?php
                                                                                 for ($ib = 1; $ib <= 1; $ib++) {
                                                                                ?>
                                                                                       <option value="<?= $ib ?>"><?= $ib ?></option>
                                                                                <?php } ?>                                                                                                                                        
                                                                        </select>
                                                                        </li-->

                                    
                                                                        <input type="hidden" name="sname" id="sname" value="<?php echo $_POST["sname"];?>" /> 
                                                                        <input type="hidden" name="dname" id="sname" value="<?php echo $_POST["dname"];?>" /> 
                                    
                                                                        
                                                                        <li><strong>Variety...........:</strong>
                                                                                                                                                                                                        
                                                                            <select name="var_add" id="var_add"   style="margin-top:10px; height:35px; padding:3px; width:250px;" class="listmenu" >
                                                                                    <option value="">--Select--</option>
                                                                            </select>

<button style="background:#8a2b83!important;" onclick="selectscategory()" class="btn btn-success btn-xs relative" type="button">View Variertys</button>                                                                                             
                                                                        </li>
                                                                        
                 
                 
									<!--li><strong>Bunch......xx.....:</strong>                                                                                                   
                                                                            <select name="qty_pack" id="qty_pack" onclick="grabaFecha();" style="margin-top:10px; height:35px; padding:3px; width:250px;" class="listmenu" >
                                                                                    <option value="">--Select--</option>
                                                                                <?php
                                                                                 for ($ib = 1; $ib <= 10; $ib++) {
                                                                                ?>
                                                                                       <option value="<?= $ib ?>"><?= $ib ?></option>
                                                                                <?php } ?>                                                                                                                                        
                                                                            </select>
                                                                        </li-->
                                                                        
									<li><strong>Bunch..............:</strong>                                                                            
                                                                            <input type="number" name="qty_pack" id="qty_pack"  onclick="bunQuantity();"   style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_pack"];?>" />                                                
                                                                        </li>                                                                        
                                                                        


								</ul>
								</div>

								<div class="col-md-6 col-sm-6 text-right">


								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Shipment</th>                                                                                    
											<th>Delivery</th>                                                                                                                                                                            
											<th>Variety</th>
                                                                                        <th>Size</th>
                                                                                        <th>Bunches</th>
                                                                                        <th>State</th>
                                                                                        <th>Client</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                                 $cn=1;
                                                                                 
                                         while($row = mysqli_fetch_assoc($result))  {
                                                                                  
                                                                                         
                                                                                          
                                                                            ?>
										<tr>
                                                                                    <td>
                                                                                       <div><?php echo $row['date_ship']; ?></div>
                                                                                    </td>   
                                                                                    <td>
                                                                                              <div><?php echo $row['date_del']; ?></div>                                                                                    
                                                                                    </td>  
                                                                                      
                                                                                      
										    <td>
                                                                                        <?php

        

         // Verificacion Stems/Bunch
                                                                                            
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);

                          ?>

                                                                                        <div><?php echo $row['name_product']." ".$row['psubcatego']; ?></div>
											<!--small><?php echo $row['psubcatego'] ?></small-->
										</td> 
                                                                                                                            
                                                                                    
										<td>
                                                                                        <?php echo $row['size_name']."  cm." ; ?>
                                                                                </td>                                                                                    
                                                                                    
										<td>                                                                   
                                                                                        <input type="text" class="form-control" name="qty_pack-<?php echo $row['gid'] ?>" id="qty_pack-<?php echo $row['gid'] ?>" value="<?php echo $row['qty_pack'] ?>">
                                                                                </td>

                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="id_state-<?php echo $row["gid"] ; ?>" id="id_state<?php echo $row["gid"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="0" <?php  if($row['id_state'] == 0) echo "selected";?>>REQUEST</option>
                                                                                            <option value="1" <?php  if($row['id_state'] == 1) echo "selected";?>>CANCEL</option>   
                                                                                        </select>                                                                        
                                                                                    </td>                                                                                                                                                                     
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/>
                                                                                                                                                                                                                                                     

                                                                                    
										    <td>
                                                                                        <?php echo $row['subclient'];?>
                                                                                    </td>
                                                                                    
                                                                                 
										</tr>																				
											<?php 
                                                                                                $totalCal = $totalCal + $Subtotal;   
                                                                                                $cn++;
                                                                                                                                                                      
                                                                             } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>-</strong> </h4>

                                                                        <br>

									<address>
                                                                                --<br>
									</address>

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<!--ul class="list-unstyled">
                                                                            <li> <strong>Sub - Total Amount: </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>                                                                            </li>                                                                                                                                  
									</ul-->  
								</div>
                                                           
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">
						<div class="panel-body">
							<!--a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_invoice_client.php?b=<?php echo $idfac."&id_cli=".$cliente_inv ?>" target="_blank"><i class="fa fa-print"></i> INVOICE</a-->                                                    
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
                                
			<!--/section-->
                        
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
        </form>
</section>
<?php require_once '../includes/footer_new.php'; ?>
<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
                      
                $('#var_add_xx').select({
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                });        

    });

</script>

<script>
    function grabaFecha() {
        
        var shipDate = $('#cls_date_ship').find("input").val();
                      
         $('input[name=sname]').val(shipDate);     
         
         console.log(shipDate);
         
        var delDate = $('#cls_date_del').find("input").val();
                      
         $('input[name=dname]').val(delDate);     
         
                                 
    }        
    

    function bunQuantity() {
        
        var productText = $('#var_add' + ' :selected').text();  
                        		               
        var divisiones = productText.split("-", 1);
              
         //console.log(divisiones);  
                  
         $('input[name=qty_pack]').val(divisiones);                                       
     }    


    function boxQuantityChange(id, cartid, main_tr, with_a) {
        
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val  = $("#request_qty_box_" + main_tr + " option:selected").val();
        
        	//console.log(selected_val);
                
		var nbox = selected_val.split('-');
                var tbox = $.trim(selected_text).split(" ");
                
		$('input[name=boxcant]').val(nbox[0]);
                
                $('input[name=boxtypen]').val(tbox[1]);
                
          //                      
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);
                                 
    }    
    
    
    // opcion que funciona del calendario
    function checkOptionship() {
    
        var shippingMethod = '132';
        
        //var fecha_fin = $('#cls_date_del').find("input").val();
        //console.log(fecha_fin);
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_cliship.php',
                data: 'shippingMethod' + shippingMethod,
                success: function (data) {
                    $('.cls_date_start_dates').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            }); 
            
            checkOptiondel();
    }
    
    function checkOptiondel() {
    
      //  var shippingMethod = '132';
        
        var fecha_ini = $('#cls_date_ship').find("input").val();
        console.log(fecha_ini);
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_clidel.php',
                data: 'fecha_ini=' + fecha_ini,
                success: function (data) {
                    $('.cls_date_start_dated').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            }); 
            
           //  send_date();     // Graba la fecha
                   
           
    } 
     function send_date() {
        
        var delDate = $('#cls_date_ship').find("input").val();
                      
         $('input[name=sname]').val(delDate);     
         
         console.log(delDate);
                      
                $('#var_add').select2({
                    
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        


     }   
</script>    
    
    