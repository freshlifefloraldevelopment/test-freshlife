<?php
session_start();

$userSessionID = $_SESSION["buyer"];

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini.php');

$sel_cli = "select br.date_del , br.id_order , br.id_client , sc.name as namecli,br.label , br.lote
						 from reser_requests br
						inner JOIN buyer_orders bo ON br.id_order = bo.id
						inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
						where br.buyer = '" . $userSessionID . "'
							and br.comment   like 'SubClient%'
							and bo.assigned = 1
						group by br.id_order , br.id_client ,br.lote
						union
					 select  br2.date_del , br2.id_order , br2.id_client , sc.name as namecli,br2.label , br2.lote
						 from buyer_requests br2
						inner JOIN buyer_orders bo ON br2.id_order = bo.id
						inner JOIN sub_client sc ON IFNULL(br2.id_client,0) = sc.id
						where br2.buyer = '" . $userSessionID . "'
							and br2.comment   like 'SubClient%'
							and bo.assigned = 1
						group by br2.id_order,br2.id_client  , br2.lote
						order by id_order desc,namecli " ;

$rs_cli = mysqli_query($con, $sel_cli);
while ($client_week = mysqli_fetch_assoc($rs_cli)) {
  $quiebre = $client_week['id_order'];
}

?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->
				<div id="middle" class="flex-fill">

					<!--

						PAGE TITLE
					-->
					<div class="page-title bg-transparent b-0">

						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Reports
						</h1>

					</div>


					<div class="row gutters-sm">

						<div class="col-12 mb-3">

							<!-- start:portlet -->
							<div class="portlet">

								<div class="portlet-header border-bottom">
									<span>Reports</span>
								</div>

								<div class="portlet-body">
									<div class="container ">

									<div class="row">

										<div class="col-lg-6 mb-4">

											<div id="clipboard_1" class="mb-4">
												<!-- Aero List -->
												<ul class="list-group list-group-flush rounded overflow-hidden">

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background: rgb(221, 247, 212);">VA</span>
																	Variety
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer11.php?fac_id=<?php echo $quiebre."&id_cli=".$quiebre; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Variety / Show me varieties</span></a>
																</p>
															</div>

														</div>
													</li>




													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background: rgb(247, 212, 213);">AV</span>
																	Availability
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer.php?b=<?php echo $quiebre."&id_buy=".$userSessionID; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Availability / Show me varieties availables</span></a>
																</p>
															</div>

														</div>
													</li>
                                                                                                        
                                                                                                        
													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background: rgb(247, 212, 213);">AV</span>
																	Holland
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer41.php?b=<?php echo $quiebre."&id_buy=".$userSessionID; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Holland / Show me varieties </span></a>
																</p>
															</div>

														</div>
													</li>  
                                                                                                        
													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background: rgb(247, 212, 213);">AV</span>
																	Customer Holland
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer61.php?b=<?php echo $quiebre."&id_buy=".$userSessionID; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Holland Sales / Show me Customer </span></a>
																</p>
															</div>

														</div>
													</li>                                                                                                                                                                                                                
												</ul>
												<!-- /Aero List -->
											</div>

										</div>

										<div class="col-lg-6 mb-4">

											<div id="clipboard_2" class="mb-4">
												<!-- Aero List -->
												<ul class="list-group list-group-flush rounded overflow-hidden">

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background-color:#fad776">CU</span>
																	Customer
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer21.php?fac_id=<?php echo $quiebre."&id_cli=".$quiebre; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Customer / Show me customers</span></a>
																</p>
															</div>

														</div>
													</li>


													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background-color:#fad776">HG</span>
																	Hard Goods
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer31.php?fac_id=<?php echo $quiebre."&id_cli=".$quiebre; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Hard Goods / Vases</span></a>
																</p>
															</div>

														</div>
													</li>

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	<span class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center js-sowformstringified" style="background-color:#fad776">CU</span>
																	Customer Holland
																</p>

																<p class="m-2">
																	<a class="transition-hover-start p-3 line-height-1" href="/buyer/print_packing_customer51.php?fac_id=<?php echo $quiebre."&id_cli=".$quiebre; ?>"><i class="fi fi-round-lightning float-start "></i> <span class="h5-xs d-block fs--18">Holland Vendor / Show me customers</span></a>
																</p>
															</div>

														</div>
													</li>
												</ul>
												<!-- /Aero List -->
											</div>

										</div>


									</div>

									</div>
								</div>

							</div>
							<!-- end:portlet -->

						</div>

					</div>



				</div>
				<!-- /MIDDLE -->
			</div><!-- FOOTER -->



			      <!--Select Orders Modal Open-->
			      <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
			          <div class="modal-dialog modal-md modal-md" role="document">
			              <div class="modal-content">

			                  <!-- header modal -->
			                  <div class="modal-header">
			                    <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
			                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span class="fi fi-close fs--18" aria-hidden="true"></span>
			                    </button>

			                  </div>
			                  <!-- body modal 3-->
			                  <form action="../en/florMP.php" method="post" id="payment-form">
			                  <div class="modal-body">
			                      <div class="table-responsive">

			                        <font color="#000">Please, before to continue select an order.</font><br><br>

			                        <div class="form-label-group mb-3">
			                        <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
			                                    <option value='0'>Select Previous Order</option>
			                                    <?php
			                                            $sel_order="select id , order_number ,del_date , qucik_desc
			                                                          from buyer_orders
			                                                         where del_date >= '" . date("Y-m-d") . "'
			                                                           and is_pending=0 and buyer_id = '".$userSessionID."'  ";

			                                            $rs_order=mysqli_query($con,$sel_order);

			                                        while($orderCab=mysqli_fetch_array($rs_order))  {
			                                    ?>
			                                            <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
			                                    <?php
			                                        }
			                                       ?>
			                        </select>

			                        <label for="select_options">Select Previous Order</label>
			                         <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
			                      </div>



			                                              <br>
			                                             <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

			                      </div>



			                  </div>

			                  <div class="modal-footer request_product_modal_hide_footer">
			                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			                  <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
			                      <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

			                  </div>
			                  </form>
			              </div>
			          </div>
			      </div>



			<?php include('../back-end/inc/footer.php'); ?>
