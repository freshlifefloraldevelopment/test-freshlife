<?php

// PO 2018-08-24

$menuoff = 1;
$page_id = 421;
$message = 0;

//include("../config/config_new.php");

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];

/* * *******get the data of session user*************** */

/*
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?"))
        {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        header("location:" . SITE_URL);
        die;
    }
} else {
    header("location:" . SITE_URL);
    die;
}
*/
$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'   ";

$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$page_request = "buyer_invoices";

?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

</head>
<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Customer List.</h1>
        <ol class="breadcrumb">
            <li><a href="#">Buyer</a></li>
            <li class="active">invoice</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                 <a href="<?php echo SITE_URL; ?>buyer/customer_list_add.php" class="btn btn-success btn-xs relative"> Add Client </a>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <a href="#" class="btn btn-primary btn-xs white" data-toggle="modal" data-target=".search_modal_open"><i class="fa fa-filter"></i> Filter</a>
                </ul>
                <!-- /right options -->

            </div>


                                        <div class="modal fade search_modal_open" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form name="form1" id="form1" method="post" action="buyer/my-offers.php" class="modal fade search_modal_open">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Choose Filter</h4>
                                                        </div>
                                                        <!-- Modal Body -->
                                                        <div class="modal-body">
                                                            <div class="panel-body">



                                                                <!--label>Subclient.</label>
                                                                <div class="form-group">

                                                                    <select style="width: 100%; display: none;" name="filter_grower" id="filter_grower" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Subclient</option>
                                                                        <?php
                                                                        $subcategory_sql = "select sb.id , sb.name
                                                                                              from sub_client sb
                                                                                             where  sb.id != 1
                                                                                               and sb.buyer = '" . $userSessionID . "'
                                                                                              order by sb.name ";

                                                                        $result_subcategory = mysqli_query($con, $subcategory_sql);

                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div-->






                                            <!-- date picker -->

                                            <!--<?php //echo $products["prodcutid"]; ?>_<?php ///echo $i; ?>--->
                                            <label class="field cls_date_start_date" id="filter_category">
                                                <input value="" class="form-control required start_date" placeholder="Select Date of Arrival" style="width: 400px!important;text-indent: 32px;border-radius: 5px;" type="text">
                                            </label>


                                                            </div>
                                                        </div>
                                                        <!-- Modal Footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                            <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>images/ajax-loader.gif"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <!--th>Sr.</th-->
                                <th>Client</th>
                                <!--th>Information</th>
                                <th>Last Date</th-->
                            </tr>
                        </thead>
                        <tbody id="list_inventory">
                            <?php
                            $sql = "select sc.id, sc.name, sc.buyer ,company
                                      from sub_client sc
                                     inner join buyers b on sc.buyer = b.id
                                     where sc.buyer = '".$userSessionID."'
                                       and sc.id not in (0,1,47)
                                       and sc.active = 'active'
                                     order by sc.name ";

			    $client_res = mysqli_query($con, $sql);

				while ($client = mysqli_fetch_assoc($client_res)) {

                                                  $sel_mov="select id , id_fact , buyer_id , id_client , order_number
                                                              from invoice_orders_subcli
                                                             where buyer_id  = '".$userSessionID."'
                                                               and id_client = '".$client["id"]."'  " ;

                                                 $rs_mov = mysqli_query($con,$sel_mov);

                                                 $total_mov = mysqli_num_rows($rs_mov);

                                                  $sel_datemax="select DATE_FORMAT(max(date_added), '%d-%m-%y') final_date
                                                                  from invoice_orders_subcli
                                                                 where buyer_id  = '".$userSessionID."'
                                                                   and id_client = '".$client["id"]."'  " ;

                                                 $rs_datemax = mysqli_query($con,$sel_datemax);

                                                 $date_max = mysqli_fetch_assoc($rs_datemax);

                                                 $last_mov = $date_max["final_date"];

				?>
                                    <tr>
                                        <td><?php echo $client['id']; ?></td>
                                        <td><?php echo $client['name']; ?></td>

                                        <?php if ($total_mov < 1) { ?>
                                              <td> <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">No Moving</a></td>
                                        <?php }else{?>
                                              <td> <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #00b300">Last Sale</a></td>
                                        <?php }  ?>

                                        <td><?php echo $last_mov; ?></td>

                                        <td><a href="<?php echo SITE_URL; ?>buyer/client_price_special.php?id_cli=<?php echo $client["id"]?>" class="btn btn-success btn-xs relative"> Special Price </a></td>
                                        <td><span data-toggle="modal" data-target=".orders_method_modal"><a href="javascript:selectID(<?php echo $client["id"]?>)" class="btn btn-secondary btn-xs relative"> <span class="fa-stack fa-lg">
                                        <i class="fa fa-square-o fa-stack-2x"></i>
                                        <i class="fa fa-key" aria-hidden="true">
                                      </span></i> User / Pass </a></span></td>
                                        <td><a href="<?php echo SITE_URL; ?>buyer/customer_pre_invoice.php?id_cli=<?php echo $client["id"]?>" class="btn btn-success btn-xs relative"> Invoice </a></td>
				    </tr>
				<?php
				$i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
<script>

function cleanField(){
  document.getElementById('passId').value = '';
}
function selectID(a){
document.getElementById('valueClient_Id').value = a;

$.ajax({
    type: 'post',
    url: 'consultaCliente.php',
    data: 'CID=' + a,
    success: function (data) {
        document.getElementById('dataClientUserPass').innerHTML = data;
    },
    error: function () {
      swal("Error! Try again pleae!", {
        icon: "error",
      });
    }
});
}

function update_request(){
  var user = document.getElementById('userId').value;
  var pass = document.getElementById('passId').value;
  var clientID = document.getElementById('valueClient_Id').value;

  if (user == "") {
    swal("Error! Type your new email!", {
      icon: "error",
    });
  }else{

    if (pass == "") {
        swal("Error! Type your new password!", {
        icon: "error",
      });
    }else{

      $.ajax({
          type: 'post',
          url: 'updateDataCliente.php',
          data: 'CID=' + clientID + '&emailID=' + user + '&passID=' + pass,
          success: function (data) {

              if(data == 1){
                swal("Ok! Your data was updated successfully!", {
                icon: "success",
              });
              }

              $('#orders_method_modal').modal('hide');

          },
          error: function () {
            swal("Error! Tlease try egain!", {
            icon: "error",
          });
          }
      });
    }

  }





}
</script>

<!--Select Orders Modal Open-->
<div class="modal fade orders_method_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Update Email and Password</h4>
            </div>
            <!-- body modal 3-->
            <form action="#" method="post" id="payment-form">
            <div class="modal-body">
                <div class="table-responsive">

                  <font color="#000"><strong>Please, change your email or password.</strong></font><br><br>

                  <table id="dataClientUserPass" width="100%" border="0">

                  </table>
                      <input type="hidden" name="valueClient_Id" id="valueClient_Id" value="notvalue" />
                      <input type="hidden" name="pass_Id_hidden" id="valueClient_Id" value="notvalue" />
                      <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">The buyer can't not login to the site until you inform new password.</font></em>

                </div>



            </div>

            <div class="modal-footer request_product_modal_hide_footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                <button style="background:#8a2b83!important;" type="button" onclick="update_request();" class="btn btn-primary" class="btn btn-default btn-xs">Update</button>

            </div>
            </form>
        </div>
    </div>
</div>
