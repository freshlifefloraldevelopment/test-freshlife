<?php

include "../config/config_gcp.php";

$row        = array();
$return_arr = array();
$row_array  = array();

if (isset($_GET['q']) && strlen($_GET['q']) > 0) {
    
    
        if (isset($_GET['q'])) {
            $getVar = $_GET['q'];
            
            $whereClause = "p.name LIKE '%" . $getVar . "%' ";
        }
        
        
        if (isset($_GET['s'])) {
            $firstVar = $_GET['s'];
            
            //$sqlgrower = "select growerid from grower_product_box_packing where id = '" . $firstVar . "'   ";            
            //$resultgrow = mysqli_query($con, $sqlgrower);
            //while ($rowgrow = mysqli_fetch_array($resultgrow)) {                
            //    $filtroGrow = $rowgrow['growerid'];                
            //}
            
            
            $sqlprod = "select prodcutid from grower_product_box_packing where id = '" . $firstVar . "'   ";            
            
            $resultprod = mysqli_query($con, $sqlprod);
            
            while ($rowprod = mysqli_fetch_array($resultprod)) {                
                $filtroProd = $rowprod['prodcutid'];                
            }            

            //////// Todas la Fincas
            
            $completa = 0;
            
            $sqlgrow = "select gp.grower_id 
                         from grower_product gp
                         inner join growers g  on gp.grower_id=g.id
                         where product_id = '" . $filtroProd . "' 
                           and g.active='active'   ";            
            
            
            
            $resultGrow = mysqli_query($con, $sqlgrow);
            

            
            while ($rowGrow = mysqli_fetch_array($resultGrow)) {                
                $filtroGrow .= $rowGrow['grower_id'].",";                
            }            
                $filtroGrow .= $completa;                
        }        

         //$activity = "(1)".$firstVar." (2)".$filtroProd." (3)".$filtroGrow  ;
         //$insert_login = "insert into activity set grower='99993',ldate='" . date("Y-m-d") . "' , type='market' , activity='" . $activity . "',ltime='12',atype='YE'";
         //mysqli_query($con,$insert_login);
         
         $sql = "select p.name as variety     ,
                        s.name as subcatego   ,
                        bt.name as caja       ,
                        ff.name as featurename,
                        sh.name as sizename   ,
                        gpb.id as idx          ,
                        gpb.sizeid as sizes   ,
                        b.id as boxid         , 
                        bs.id as bunchsizeid  ,
                        bs.name as bunchesv   ,                                                                                                                                        
                        gpb.prodcutid   ,                                                                                                                                         
                        gpb.qty         ,                                                                                                                                         
                        gpb.feature     , 
                        gpb.growerid    ,
                        p.id            , 
                        p.subcategoryid ,                                                                                                                                        
                        p.color_id      ,                                                                                                                                         
                        s.cat_id        ,
                        g.growers_name  ,                                                                                                                                                                                                                                                                                
                        b.width , b.length , b.height   ,
                        b.name as boxname,
                        bt.name as boxt  ,                                                                                                                                        
                        c.name as colorname 
                   from grower_product_box_packing gpb
                  inner join product p     on gpb.prodcutid = p.id
                  inner join subcategory s on p.subcategoryid=s.id  
                  inner join colors c      on p.color_id=c.id 
                  inner join growers g     on gpb.growerid=g.id                                                                                                                                   
                  inner join sizes sh      on gpb.sizeid=sh.id 
                   left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                   left join boxes b on gpb.box_id=b.id
                   left join boxtype bt on b.type = bt.id             
                   left join features ff on gpb.feature=ff.id                                                                                                                                                                                                                                                                      
                  where g.active='active'
                    and gpb.growerid in ('" . $filtroGrow . "')
                    and $whereClause   
                    order by g.growers_name ,p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),bt.name LIMIT 0,50    ";
        
                        
        $result = mysqli_query($con, $sql);
        
        while ($row = mysqli_fetch_array($result)) {
            $row_array['id']   = $row['prodcutid'];
            $row_array['text'] = $row['variety']."-".$row['sizename']." cm ".$row['bunchesv']."st/bu ".$row['featurename'];
            array_push($return_arr, $row_array);
            
        }
   
} else {
    $row_array['id'] = 0;
    $row_array['text'] = 'Start Typing....';
    array_push($return_arr, $row_array);
}


$ret = array();
$ret['results'] = $return_arr;
echo json_encode($ret);
?>
