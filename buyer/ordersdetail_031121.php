<?php
session_start();

$userSessionID = $_SESSION["buyer"];
if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 16 Jun 2021
Structure MarketPlace previous to buy
**/
// start a session

$idR = $_GET["GETiD"];




// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini.php');



 $selOrderNum= "select gpb.id as cartid,
p.image_path as img,
s.name as subs,
p.name as namep,
ff.name as featurename,
sh.name as sizename,
gpb.qty as st_request,
gpb.lfd as date_lfd,
gp.stem_bunch id_stbu,
bs.name stem_bunch_name,
gpb.type as order_type,
gpb.price_front,
gpb.id_grower,
gpb.unit,
gpb.id_status,
gpb.countdown
from buyer_requests gpb
left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
left join subcategory s on p.subcategoryid=s.id
left join features ff on gpb.feature=ff.id
left join sizes sh on gpb.sizeid=sh.id
left join grower_parameter gp on (gp.idsc=s.id and gpb.sizeid=gp.size)
left join bunch_sizes  bs on gp.stem_bunch = bs.id
where gpb.buyer  = '$userSessionID'
and gpb.id_order = '$idR'
and gpb.id_status = '2'
group by gpb.id
order by gpb.id desc";
$numero_filas = mysqli_num_rows(mysqli_query($con,$selOrderNum));

                                    //and gpb.lfd >='" . date("Y-m-d") . "'
?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->
<style>

.color-not-available {
  background: #b02a37 !important; // Define your own color in your CSS
}
.color-not-available:hover, .color-not-available:active {
  background: #e35d6a !important; // Define your own color's darkening/lightening in your CSS
}

.color-complete {
  background: #6dbb30 !important; // Define your own color in your CSS
}
.color-complete:hover, .color-complete:active {
  background: #95d661 !important; // Define your own color's darkening/lightening in your CSS
}

.color-approve-change {
  background: #495057 !important; // Define your own color in your CSS
}
.color-approve-change:hover, .color-approve-change:active {
  background: #6c757d !important; // Define your own color's darkening/lightening in your CSS
}

.color-inprocess {
  background: #fd7e14 !important; // Define your own color in your CSS
}
.color-inprocess:hover, .color-inprocess:active {
  background: #feb272 !important; // Define your own color's darkening/lightening in your CSS
}

.countdown-label {
	color: #fff;
	text-align: center;
	text-transform: uppercase;
  margin-top: 1px
}
#countdown{
box-shadow: 0 1px 2px 0 rgba(1, 1, 1, 0.4);
	height: 57px;
	text-align: center;
background: #495057;

	border-radius: 5px;

	margin: auto;

}

#countdown #tiles{
  color: #fff;
	position: relative;
	z-index: 1;
text-shadow: 1px 1px 0px #ccc;
	display: inline-block;
  font-family: Arial, sans-serif;
	text-align: center;

  padding: 5px;
  border-radius: 5px 5px 0 0;
  font-size: 14px;
  font-weight: thin;
  display: block;

}

.color-full {
  background: #4c2c92;
}
.color-half {
  background: #4c2c92;
}
.color-empty {
  background: #e5554e;
}

#countdown #tiles > span{
	width: 60px;
	max-width: 70px;

	padding: 18px 0;
	position: relative;
}





#countdown .labels{
	width: 70%;
	height: 25px;
	text-align: center;
	position: absolute;
	bottom: 8px;
}

#countdown .labels li{
	width: 102px;
	font: bold 15px 'Droid Sans', Arial, sans-serif;
	color: #f47321;
	text-shadow: 1px 1px 0px #000;
	text-align: center;
	text-transform: uppercase;
	display: inline-block;
}
</style>

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Orders Detail  # <?php echo $idR; ?> </strong>
                <input type="hidden" id="numRecords" value="<?php echo $numero_filas ?>" />
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">

							Orders Detail # <?php echo $idR; ?>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


              <div class="modal-body">
                <table class="table-datatable table table-bordered table-hover table-striped"
                        data-lng-empty="No data available in table"
                        data-responsive="true"
                        data-header-fixed="true"
                        data-select-onclick="true"
                        data-enable-paging="true"
                        data-enable-col-sorting="true"
                        data-autofill="false"
                        data-group="false"
                        data-items-per-page="10">
                        <thead>
                          <tr>
                            <th>Img</th>
                            <th>Request</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Farm</th>
                            <th>Order Type</th>
                            <th>Status</th>


                          </tr>
                        </thead>
                        <tbody>

                          <?php

                          $sel_order= "select gpb.id as cartid,
                          p.image_path as img,
                          s.name as subs,
                          p.name as namep,
                          ff.name as featurename,
                          sh.name as sizename,
                          gpb.qty as st_request,
                          gpb.lfd as date_lfd,
                          gp.stem_bunch id_stbu,
                          bs.name stem_bunch_name,
                          gpb.type as order_type,
                          gpb.price_front,
                          gpb.id_grower,
                          gpb.unit,
                          gpb.id_status,
                          gpb.countdown
                          from buyer_requests gpb
                          left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                          left join subcategory s on p.subcategoryid=s.id
                          left join features ff on gpb.feature=ff.id
                          left join sizes sh on gpb.sizeid=sh.id
                          left join grower_parameter gp on (gp.idsc=s.id and gpb.sizeid=gp.size)
                          left join bunch_sizes  bs on gp.stem_bunch = bs.id
                          where gpb.buyer  = '$userSessionID'
                          and gpb.id_order = '$idR'
                          group by gpb.id
                          order by gpb.id desc";
                          
                           //and gpb.lfd >='" . date("Y-m-d") . "'

                                /*   $sel_order= "select gpb.id as cartid,
                                  p.image_path as img,
                                  s.name as subs,
                                  p.name as namep,
                                  ff.name as featurename,
                                  sh.name as sizename,
                                  gpb.qty as st_request,
                                  gpb.lfd as date_lfd,
                                  cod_order as ponumber
                                  from buyer_requests gpb
                                  left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                  left join subcategory s on p.subcategoryid=s.id
                                  left join features ff on gpb.feature=ff.id
                                  left join sizes sh on gpb.sizeid=sh.id
                                  where gpb.buyer = '$userSessionID'
                                  and gpb.id_order = '$idR'
                                  and gpb.lfd >='" . date("Y-m-d") . "'
                                  order by gpb.id desc";
                                      */
                                /*  $sel_order= "select gpb.id as cartid,
                                    p.image_path as img,
                                    s.name as subs,
                                    p.name as namep,
                                    ff.name as featurename,
                                    sh.name as sizename,
                                    gpb.qty as st_request,
                                    gpb.lfd as date_lfd,
                                    cod_order as ponumber
                                    from buyer_requests gpb
                                    left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                    left join subcategory s on p.subcategoryid=s.id
                                    left join features ff on gpb.feature=ff.id
                                    left join sizes sh on gpb.sizeid=sh.id
                                    where gpb.buyer = '318'
                                    and gpb.id_order = '1295'
                                    order by gpb.id desc";*/
$i = 0;
                                  $rs_order=mysqli_query($con,$sel_order);
                                  while($orderCab=mysqli_fetch_array($rs_order))
                                  {

                              //   $_SESSION[$cartid] = $orderCab['cartid'];
                                $cartid = $orderCab['cartid'];
                                $img = $orderCab['img'];
                                $subs = $orderCab['subs'];
                                $namep = $orderCab['namep'];
                                $featurename= $orderCab['featurename'];
                                $sizename = $orderCab['sizename'];
                                $st_request = $orderCab['st_request'];
                                $date_lfd = $orderCab['date_lfd'];
                                $cod_order = $orderCab['ponumber'];
                                $unit = $orderCab['unit'];
                                $status = $orderCab['id_status'];
                                $price_front = $orderCab['price_front'];
                                $id_grower  = $orderCab['id_grower'];
                                $order_type = $orderCab['order_type'];
                                $st_x_bunches = $orderCab['stem_bunch_name']."st/bu";
                                $timeEnd = $orderCab['countdown'];

                                $stillNeeded = 0;


                                $sel_orderCountPrevious = "select br.type tipoReq,
                                g.file_path5 as img ,
                                p.name as variedad,
                                sz.name as size_name_request,
                                br.price_front as priceRequest,
                                br.qty as cantRequest,
                                p.image_path as fotoRequest,
                                g.growers_name  as  growerRequest,
                                gor.type_market as tipoOfer,
                                gor.product_subcategory as subcat_offer,
                                gor.product as variedadOffer       ,
                                gor.size   as sizeOffer       ,
                                gor.steams as steamsOffer        ,
                                gor.price as preciOfer  ,
                                gor.bunchqty*gor.steams as Stems_offered   ,
                                gor.bunchqty  as cantofer     ,
                                gor.grower_id as growerOffer  ,
                                gor.countdown   ,
                                br.id_status as status_request,
                                (select image_path from product where name = gor.product and subcate_name = gor.product_subcategory group by image_path) as fotoOffer,
                                f.name as featurename,
                                (select bs.name from grower_parameter gp inner JOIN bunch_sizes bs ON bs.id = gp.stem_bunch where gp.idsc = p.subcategoryid and gp.size = sz.id group by bs.name) as stemsRequest,
                                gor.request_id
                                from buyer_requests br
                                left JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                left JOIN product p ON br.product = p.id
                                left JOIN buyers b ON br.buyer = b.id
                                left JOIN buyer_orders bo ON br.id_order = bo.id
                                left JOIN sizes sz ON br.sizeid = sz.id
                                left JOIN growers g ON br.id_grower = g.id
                                left JOIN features f ON br.feature = f.id
                                where br.buyer  = '$userSessionID'
                                and gor.request_id = '$cartid'";

                              $valorTotalOfertPrevious  = mysqli_num_rows(mysqli_query($con,$sel_orderCountPrevious));




                                if($unit=='0' OR $unit==39)
                                {
                                  $unit = 'Stems';
                                }else {
                                  $unit = 'Bunches';
                                }

                                $rs_status   = @mysqli_query($con,"Select * from status where id='$status'");
                                $orderStatus = @mysqli_fetch_array($rs_status);

                                 $id_Status =   $orderStatus['0'];
                                 $name_Status =   $orderStatus['1'];

                                 if($id_Status==1)
                                 {
                                   $icon = '<i class="align-button float-left fa fa-info-circle" aria-hidden="true"></i>';
                                   $botonR = 'color-inprocess bg-gradient-warning text-white';
                                   $infomodal ='title="Request In process" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Please wait, we are processing your request."';
                                   $titleModel = '';
                                 }

                                 if($id_Status==2)
                                 {
                                    $i = $i+1;

                                  $minutesadd = 0;
                                  date_default_timezone_set("America/Vancouver");
                                   $new_time = date("Y-m-d H:i:s", strtotime("+$minutesadd minute"));

                                   $icon = '<i class="fas fa-random m-0-md" aria-hidden="true"></i>';
                                   $botonR = 'color-approve-change bg-gradient-purple text-white';
                                   $botonRM = 'color-approve-change text-white';
                                   $titleModel = 'Approve change';
                                   $modalTitle = 'badge badge-purple';
                                   $botonProcess = "<button type='button' onclick='sendApprove($valorTotalOfertPrevious)' class='btn btn-success' data-dismiss='modal'><i class='fi fi-close'></i> Process Request</button>";

                                      $dateR = date('Y-m-d H:i:s');
                                    // $new_time = date("Y-m-d H:i:s", strtotime('+5 minute'));
                                    $timeEnd;
                                     $to_time = strtotime($dateR);
                                     $from_time = strtotime($timeEnd);

                                      $valueMinutes = round(($from_time - $to_time) / 60,0);
                                      $porciones = explode(".", $valueMinutes);

                                    //  echo $porciones[0];
                                    $infomodal = "data-toggle='modal' data-target='#billing_modal-$cartid-$st_request'";

                                 }

                                 if($id_Status==3)
                                 {
                                   $icon = '<i class="align-button float-left fa fa-check-circle-o "></i>';
                                   $botonR = 'color-complete bg-gradient-success text-white';
                                   $botonRM = 'color-complete text-white';
                                   $infomodal = "data-toggle='modal' data-target='#billing_modal-$cartid-$st_request'";
                                   $titleModel = 'Order Complete';
                                   $modalTitle = 'badge badge-success';
                                 }

                                 if($id_Status==4)
                                 {
                                   $icon = '<i class="align-button float-left fa fa-ban" aria-hidden="true"></i>';
                                   $botonR = 'color-not-available bg-gradient-danger text-white';
                                   $infomodal ='title="Request Not available" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Unfortunately we could not found a fit offert."';
                                   $titleModel = '';
                                 }



                                 if($id_grower!=100000 && $id_grower!=200000 && $id_grower!=0){
                                   $rs_Grower   = @mysqli_query($con,"Select * from growers where id='$id_grower'");
                                   $orderGrower = @mysqli_fetch_array($rs_Grower);

                                     $idGrowerR =   $orderGrower['0'];
                                    $nameGrowerR =   $orderGrower['1'];

                                    $nameGrowerR = "<span class='text-primary fs--14'><span class='text-primary'>*** </span><strong>$nameGrowerR</strong></span>";

                                  }else {

                                     $id_grower;

                                              if($id_grower==100000){
                                                  $nameGrowerR = '<span class="text-primary fs--14"><span class="text-danger">*** </span><strong>Best deal</strong></span>';
                                              }

                                              if($id_grower==200000){
                                                  $nameGrowerR = '<span class="text-primary fs--14"><span class="text-danger">*** </span><strong>Best deal</strong></span>';
                                              }

                                              if($id_grower==0){
                                                  $nameGrowerR = '<span class="text-danger fs--12"><strong>No Grower</strong></span>';
                                              }
                                  }




                                  if($order_type==0){
                                    $orderName = 'Standing Order';
                                  }else
                                  {
                                    $orderName = 'Open Market';
                                  }


                                $sel_order1="select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                                from grower_offer_reply gr
                                left join growers g on gr.grower_id=g.id
                                left join country cr on g.country_id=cr.id
                                where gr.request_id='$cartid'
                                and gr.buyer_id='$userSessionID'
                                order by gr.date_added desc";


                                /*
                                $sel_order1="select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                              from grower_offer_reply gr
                              left join growers g on gr.grower_id=g.id
                              left join country cr on g.country_id=cr.id
                              where gr.request_id='50195'
                              and gr.buyer_id='318'
                              order by gr.date_added desc";
                              */

                              $rs_order1=mysqli_query($con,$sel_order1);
                              $rs_order1NumR=mysqli_num_rows($rs_order1);
                              while($orderCab1=mysqli_fetch_array($rs_order1))  {

                                $stillNeeded += $orderCab1['Stems_offered'];

                              }


                          ?>

                          <tr>
                            <td>
                              <input type="hidden" value="<?php echo $cartid;  ?>" id="valRBID<?php echo $i; ?>" />
                              <span class="fs--15">
                              <figure style="border-radius: 50%; height: 53px; width: 53px;" class="d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
        												<img style="border-radius: 50%; height: 50px; width: 50px;"  src="https://app.freshlifefloral.com/<?php echo $img; ?>" alt="<?php echo $namep; ?>">
        	                     </figure>

                            </span></td>
                            <td><span class="fs--15"><?php echo $subs." <br> ".$namep." <br> ".$sizename." [cm]"." ".$st_x_bunches; ?></span>
                              <br>
                              <?php if($featurename!=''){?>
                                <sup>
                                <span class="text-primary fs--13">
                               <span class="text-danger">*** </span><strong>Feature: <?php echo $featurename; ?></strong>
                              </span>

                              </sup>
                              <?php }?>
                            </td>
                            <td align="left"><span class="fs--15">
                              <?php echo $st_request." [".$unit."]";  ?></span>
                            </td>
                            <td align="left">
                              <span class="fs--15">
                                <span class="d-block fs--15">Price: <span class="text-primary  fs--15"><?php echo $price_front; ?></span> <strong> <sup class="text-primary fs--10">USD</sup></strong></span>

                            </span></td>
                            <td><span class="fs--15"><?php echo $nameGrowerR; ?></span></td>
                            <td><span class="fs--15"><?php echo $orderName; ?></span></td>
                            <td>


                              <button <?php echo $infomodal; ?> onclick="return false;"  class="btn btn-sm btn-block b-0 w--200 <?php echo $botonR; ?> b-0">
                                                    <span class="p-0-xs">
                                                      <?php  echo $icon;?>
                                                      <span class="text-uppercase fs--14">

                                                        <?php echo $name_Status; ?>
                                                        <!-- <sup class="badge badge-danger position-absolute end-0 mt--n5 text-warning fs--14"><?php echo $rs_order1NumR; ?></sup> -->
                                                      </span>
                                                    </span>
                                                  </button>

                                <?php
                                if($id_Status==2)
                                {
                                ?>
                                <p>
                                <input type="hidden" id="set-time<?php echo $i; ?>" value="<?php echo $porciones[0];?>"/>
                                <div id="countdown" class="b-0 w--200 float-left">

                                <div id='tiles' class="color-full b-0 w--200"></div>
                                <div class="countdown-label b-0 w--200 ">Time Remaining</div>
                                </div>
                                <?php
                                }
                                ?>


                                                                                                          <!-- Billing Modal -->
                                                                                                          <div class="modal fade" id="billing_modal-<?php echo $cartid;?>-<?php echo $st_request; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                                                                                                          		<div class="modal-dialog modal-md modal-xl" role="document">
                                                                                                          				<div class="modal-content">

                                                                                                          						<!-- Header -->
                                                                                                          						<div class="modal-header <?php echo $botonRM; ?> ">
                                                                                                          								<h5 class="modal-title" id="exampleModalLabelMd"><?php echo $titleModel; ?>
                                                                                                                            </h5>
                                                                                                          								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                          										<span class="fi fi-close fs--18" aria-hidden="true"></span>
                                                                                                          								</button>
                                                                                                          						</div>

                                                                                                          						<!-- Content -->


                                                                                                          						<div class="modal-body">

                                                                                                                        <div class="container">
                                                                                                                            <div class="row">
                                                                                                                              <div class="col-5">
                                                                                                                                <h5>Request: <span class="<?php echo $modalTitle; ?>"><?php echo $st_request." Boxes ".$subs." ".$namep." ".$featurename." ".$sizename." [cm]" ?></span></h5>
                                                                                                                              </div>
                                                                                                                              <div class="col-7">
                                                                                                                                <img class="float-left" alt="<?php echo $st_request." Boxes ".$subs." ".$namep." ".$featurename." ".$sizename." [cm]" ?>" src="https://app.freshlifefloral.com/product-image/big/031921185209_crop.jpg" width="55">

                                                                                                                              </div>
                                                                                                                            </div>
                                                                                                                          </div>

                                                                                                                        <hr class="my-4">
                                                                                                                          <div class="col-5">
                                                                                                                            <?php
                                                                                                                            $sel_orderCount = "select br.type tipoReq,
                                                                                                                            g.file_path5 as img ,
                                                                                                                            p.name as variedad,
                                                                                                                            sz.name as size_name_request,
                                                                                                                            br.price_front as priceRequest,
                                                                                                                            br.qty as cantRequest,
                                                                                                                            p.image_path as fotoRequest,
                                                                                                                            g.growers_name  as  growerRequest,
                                                                                                                            gor.type_market as tipoOfer,
                                                                                                                            gor.product_subcategory as subcat_offer,
                                                                                                                            gor.product as variedadOffer       ,
                                                                                                                            gor.size   as sizeOffer       ,
                                                                                                                            gor.steams as steamsOffer        ,
                                                                                                                            gor.price as preciOfer  ,
                                                                                                                            gor.bunchqty*gor.steams as Stems_offered   ,
                                                                                                                            gor.bunchqty  as cantofer     ,
                                                                                                                            gor.grower_id as growerOffer  ,
                                                                                                                            gor.countdown   ,
                                                                                                                            br.id_status as status_request,
                                                                                                                            (select image_path from product where name = gor.product and subcate_name = gor.product_subcategory group by image_path) as fotoOffer,
                                                                                                                            f.name as featurename,
                                                                                                                            (select bs.name from grower_parameter gp inner JOIN bunch_sizes bs ON bs.id = gp.stem_bunch where gp.idsc = p.subcategoryid and gp.size = sz.id group by bs.name) as stemsRequest,
                                                                                                                            gor.request_id
                                                                                                                            from buyer_requests br
                                                                                                                            left JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                                                                                                            left JOIN product p ON br.product = p.id
                                                                                                                            left JOIN buyers b ON br.buyer = b.id
                                                                                                                            left JOIN buyer_orders bo ON br.id_order = bo.id
                                                                                                                            left JOIN sizes sz ON br.sizeid = sz.id
                                                                                                                            left JOIN growers g ON br.id_grower = g.id
                                                                                                                            left JOIN features f ON br.feature = f.id
                                                                                                                            where br.buyer  = '$userSessionID'
                                                                                                                            and gor.request_id = '$cartid'";

                                                                                                                          $valorTotalOfert  = mysqli_num_rows(mysqli_query($con,$sel_orderCount));
                                                                                                                           ?>
                                                                                                                            <h5>Offers:</h5>
                                                                                                                          </div>
                                                                                                                          <input type="hidden" value="<?php echo $valorTotalOfert; ?>" id="totalOffert" name="totalOffert" />


                                                                                                                        <table class="table table-framed">
                                                                                                          											<thead>
                                                                                                          												<tr>
                                                                                                                                  <?php  if($id_Status==2)
                                                                                                                                    { ?>

                                                                                                                                      <th  class="text-green-500 w--50">
                                                                                                                                          <input type="hidden" value="<?php echo $cartid; ?>" id="requestIDID" name="requestIDID" />
                                                                                                                                        <div class="text-success-500 font-weight-normal fs--14">APPROVE</div>
                                                                                                                                        <label class="form-checkbox-primary float-start">
                                                                                                                                          <input style="height:20px; width:20px; vertical-align: middle;" onclick="approveAll(<?php echo $valorTotalOfert; ?>)" value="approve" id="approve_decline_all" type="radio" name="approve_decline_all">
                                                                                                                                          <i></i>
                                                                                                                                        </label>
                                                                                                                                      </th>

                                                                                                                                      <th  class="text-red-500 w--50">
                                                                                                                                        <div class="text-danger-500 font-weight-normal fs--14">DECLINE</div>
                                                                                                                                        <label class=" form-checkbox-primary float-start">
                                                                                                                                          <input style="height:20px; width:20px; vertical-align: middle;" onclick="declineAll(<?php echo $valorTotalOfert; ?>)" value="decline" id="approve_decline_all" type="radio" name="approve_decline_all">
                                                                                                                                          <i></i>
                                                                                                                                        </label>
                                                                                                                                      </th>
                                                                                                                                  <?php  } ?>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      IMG
                                                                                                                                    </th>
                                                                                                          												  <th class="text-gray-500 font-weight-normal fs--14 w--700">
                                                                                                                                      PRODUCT NAME
                                                                                                                                    </th>
                                                                                                          													 <th class="text-gray-500 font-weight-normal fs--14 w--300">
                                                                                                                                      PRICE
                                                                                                                                    </th>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--300">
                                                                                                                                      BRAND
                                                                                                                                    </th>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      STATUS
                                                                                                                                    </th>
                                                                                                          												</tr>
                                                                                                          											</thead>
                                                                                                                                <tbody id="item_approve">
                                                                                                                                  <!-- product -->
                                                                                                                                  <?php
                                                                                                                                  // $sel_orderInt= "select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered   from grower_offer_reply gr left join growers g on gr.grower_id=g.id left join country cr on g.country_id=cr.id where gr.request_id='$cartid' and gr.buyer_id='$userSessionID' order by gr.date_added desc";
                                                                                                                                  /*  $sel_orderInt ="select br.type tipoReq,
                                                                                                                                                           g.file_path5 as img ,
                                                                                                                                                           p.name as variedad,
                                                                                                                                                           sz.name as size_name_request,
                                                                                                                                                           br.price_front as priceRequest,
                                                                                                                                                           br.qty as cantRequest,
                                                                                                                                                           p.image_path as fotoRequest,
                                                                                                                                                           g.growers_name  as  growerRequest,
                                                                                                                                                           gor.type_market as tipoOfer,
                                                                                                                                                           gor.product_subcategory as subcat_offer,
                                                                                                                                                           gor.product as variedadOffer       ,
                                                                                                                                                           gor.size   as sizeOffer       ,
                                                                                                                                                           gor.steams as steamsOffer        ,
                                                                                                                                                           gor.price as preciOfer  ,
                                                                                                                                                           gor.bunchqty*gor.steams as Stems_offered   ,
                                                                                                                                                           gor.bunchqty  as cantofer     ,
                                                                                                                                                           gor.grower_id as growerOffer  ,
                                                                                                                                                           gor.countdown   ,
                                                                                                                                                           br.id_status as status_request,
                                                                                                                                                           (select image_path from product where name = p.name and subcate_name = gor.product_subcategory) as fotoOffer,
                                                                                                                                                           f.name as featurename,
                                                                                                                                                           (select bs.name from grower_parameter gp inner JOIN bunch_sizes bs ON bs.id = gp.stem_bunch where gp.idsc = p.subcategoryid and gp.size = sz.id) as stemsRequest,
                                                                                                                                                           gor.request_id
                                                                                                                                                        from buyer_requests br
                                                                                                                                                        left JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                                                                                                                                        left JOIN product p ON br.product = p.id
                                                                                                                                                        left JOIN buyers b ON br.buyer = b.id
                                                                                                                                                        left JOIN buyer_orders bo ON br.id_order = bo.id
                                                                                                                                                        left JOIN sizes sz ON br.sizeid = sz.id
                                                                                                                                                        left JOIN growers g ON br.id_grower = g.id
                                                                                                                                                        left JOIN features f ON br.feature = f.id
                                                                                                                                                where br.buyer  = '$userSessionID'
                                                                                                                                                and gor.request_id = '$cartid'";

                                                                                                                                                */

                                                                                                                                                   $sel_orderInt = "select
                                                                                                                                                gor.id as idOfert,
                                                                                                                                                br.type tipoReq,
                                                                                                                                                  g.file_path5 as img ,
                                                                                                                                                  p.name as variedad,
                                                                                                                                                  sz.name as size_name_request,
                                                                                                                                                  br.price_front as priceRequest,
                                                                                                                                                  br.qty as cantRequest,
                                                                                                                                                  p.image_path as fotoRequest,
                                                                                                                                                  g.growers_name  as  growerRequest,
                                                                                                                                                  gor.type_market as tipoOfer,
                                                                                                                                                  gor.product_subcategory as subcat_offer,
                                                                                                                                                  gor.product as variedadOffer       ,
                                                                                                                                                  gor.size   as sizeOffer       ,
                                                                                                                                                  gor.steams as steamsOffer        ,
                                                                                                                                                  gor.price as preciOfer  ,
                                                                                                                                                  gor.bunchqty*gor.steams as Stems_offered   ,
                                                                                                                                                  gor.bunchqty  as cantofer     ,
                                                                                                                                                  gor.grower_id as growerOffer  ,
                                                                                                                                                  gor.countdown   ,
                                                                                                                                                  br.id_status as status_request,
                                                                                                                                                  gor.status_approved as status_approved,
                                                                                                                                                  (select image_path from product where name = gor.product and subcate_name = gor.product_subcategory group by image_path) as fotoOffer,
                                                                                                                                                  f.name as featurename,
                                                                                                                                                  (select bs.name from grower_parameter gp inner JOIN bunch_sizes bs ON bs.id = gp.stem_bunch where gp.idsc = p.subcategoryid and gp.size = sz.id group by bs.name) as stemsRequest,
                                                                                                                                                  gor.request_id
                                                                                                                                                  from buyer_requests br
                                                                                                                                                  left JOIN grower_offer_reply gor ON (gor.request_id = br.id and gor.buyer_id=br.buyer)
                                                                                                                                                  left JOIN product p ON br.product = p.id
                                                                                                                                                  left JOIN buyers b ON br.buyer = b.id
                                                                                                                                                  left JOIN buyer_orders bo ON br.id_order = bo.id
                                                                                                                                                  left JOIN sizes sz ON br.sizeid = sz.id
                                                                                                                                                  left JOIN growers g ON br.id_grower = g.id
                                                                                                                                                  left JOIN features f ON br.feature = f.id
                                                                                                                                                  where br.buyer  = '$userSessionID'
                                                                                                                                                  and gor.request_id = '$cartid'";

                                                                                                                                                $rs_order1=mysqli_query($con,$sel_orderInt);
                                                                                                                                                    $rs_order1NumR=mysqli_num_rows($rs_order1);
                                                                                                                                                    $j = 0;
                                                                                                                                                    while($orderCab1=mysqli_fetch_array($rs_order1))
                                                                                                                                                    {

                                                                                                                                                      $idOferta = $orderCab1['idOfert'];
                                                                                                                                                      $fotoOferta = $orderCab1['fotoOffer'];
                                                                                                                                                      $fotoRequest = $orderCab1['fotoRequest'];
                                                                                                                                                      $varidadRequest = $orderCab1['variedad'];
                                                                                                                                                      $varidadOferta = $orderCab1['variedadOffer'];
                                                                                                                                                      $productSubcategory = $orderCab1['subcat_offer'];
                                                                                                                                                      $sizeRequest = $orderCab1['size_name_request'];
                                                                                                                                                      $sizeOferta = $orderCab1['sizeOffer'];
                                                                                                                                                      $TallosBuncheRequest = 25; //$orderCab1['variedad'];
                                                                                                                                                      $TallosBuncheOferta =  25; //$orderCab1['variedad'];
                                                                                                                                                      $featureRequest = ''; //$orderCab1['variedad'];
                                                                                                                                                      $featureOferta =  ''; //$orderCab1['variedad'];
                                                                                                                                                      $qtyRequest = $orderCab1['cantRequest'];
                                                                                                                                                      $qtyOferta =  $orderCab1['cantofer'];
                                                                                                                                                      $priceRequest = $orderCab1['priceRequest'];
                                                                                                                                                      $priceOferta =  $orderCab1['preciOfer'];

                                                                                                                                                    /*  $timeEndi = $orderCab1['countdown'];


                                                                                                                        																		date_default_timezone_set("America/Vancouver");

                                                                                                                        																		$dateRi = date('Y-m-d H:i:s');
                                                                                                                        																		// $new_time = date("Y-m-d H:i:s", strtotime('+5 minute'));
                                                                                                                        																		//	$timeEnd;
                                                                                                                        																		$to_timei = strtotime($dateRi);
                                                                                                                        																		$from_timei = strtotime($timeEndi);

                                                                                                                        																		$valueMinutesi = round(($from_timei - $to_timei) / 60,0);
                                                                                                                        																		$porcionesi = explode(".", $valueMinutesi);
                                                                                                                                                            */

                                                                                                                                                      $farmRequest = $orderCab1['growerRequest'];
                                                                                                                                                      if($farmRequest==''){
                                                                                                                                                        $farmRequestLabel = '<span class="text-primary fs--14"><span class="text-danger">*** </span><strong>Best deal</strong></span>';
                                                                                                                                                      }
                                                                                                                                                      /*else{

                                                                                                                                                        $farmRequestArray = @mysqli_query($con,"SELECT * FROM growers where id='$farmRequest'");
                                                                                                                                                        $farmRequestLabel = @mysqli_fetch_array($farmRequestArray);
                                                                                                                                                      }*/

                                                                                                                                                      $farmOferta =  $orderCab1['growerOffer'];
                                                                                                                                                      $farmOfertaArray = @mysqli_query($con,"SELECT * FROM growers where id='$farmOferta'");
                                                                                                                                                      $farmOfertaLabel = @mysqli_fetch_array($farmOfertaArray);

                                                                                                                                                      $tipoRequest = $orderCab1['tipoReq'];
                                                                                                                                                      $tipoOferta =  $orderCab1['tipoOfer'];

                                                                                                                                                      if($tipoRequest==0){
                                                                                                                                                        $tipoRequest = 'Standing Order';
                                                                                                                                                      }else
                                                                                                                                                      {
                                                                                                                                                        $tipoRequest = 'Open Market';
                                                                                                                                                      }

                                                                                                                                                      if($tipoOferta==0){
                                                                                                                                                        $tipoOferta = 'Standing Order';
                                                                                                                                                      }else
                                                                                                                                                      {
                                                                                                                                                        $tipoOferta = 'Open Market';
                                                                                                                                                      }

                                                                                                                                                       $status_request = $orderCab1['status_approved'];


                                                                                                                                                        if($status_request==1)
                                                                                                                                                        {
                                                                                                                                                          $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-sucess btn btn-sm p-0 fs--15' href='#'>Complete</a></div>";
                                                                                                                                                        }

                                                                                                                                                        if($status_request==2){

                                                                                                                                                        $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-purple btn btn-sm p-0 fs--15' href='#'>Waiting approve...</a></div>";
                                                                                                                                                      }

                                                                                                                                                      if($status_request==3){

                                                                                                                                                      $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-success btn btn-sm p-0 fs--15' href='#'>Complete</a></div>";
                                                                                                                                                    }

                                                                                                                                                    if($status_request==4){

                                                                                                                                                    $orderStatus = "<div data-toggle='tooltip' data-original-title='Status' class='select_options w--120'><a class='text-danger btn btn-sm p-0 fs--15' href='#'>Not Approved</a></div>";
                                                                                                                                                  }

                                                                                                                                   ?>
                                                                                                                                  <tr>
                                                                                                                                    <?php  if($id_Status==2)
                                                                                                                                      {
                                                                                                                                         $j = $j+1;
                                                                                                                                        ?>
                                                                                                                                    <th>

                                                                                                                                      <label >
                                                                                                      																	<input checked onclick="approveOne(<?php echo $j; ?>)"  style="height:20px; width:20px; vertical-align: middle; background:#f9ffe4;" id="approve<?php echo $j;?>" type="radio" value="1" name="approve<?php echo $j;?>">
                                                                                                                                      </label>
                                                                                                                                      	<i></i>
                                                                                                      															</th>

                                                                                                                                    <th>
                                                                                                                                      <label>
                                                                                                                                        <input onclick="declineOne(<?php echo $j; ?>)"  style="height:20px; width:20px; vertical-align: middle; background:#faeae7;" id="decline<?php echo $j;?>" type="radio" value="4" name="decline<?php echo $j;?>">
                                                                                                                                      </label>
                                                                                                                                        <i></i>

                                                                                                                                        <input type="hidden" value="<?php echo $idOferta; ?>" id="ofertId<?php echo $j; ?>" name="ofertId<?php echo $j; ?>" />

                                                                                                                                    </th>
                                                                                                                                  <?php  } ?>
                                                                                                                                    <td>
                                                                                                                                      <img alt="<?php echo $varidadOferta; ?>" src="https://app.freshlifefloral.com/<?php echo $fotoOferta; ?>" width="65">


                                                                                                                                    </td><!-- product name -->
                                                                                                                                    <td>
                                                                                                                                      <h6 class="modal-title" id="exampleModalLabelMd"><?php echo $productSubcategory." ".$varidadOferta." ".$sizeOferta." [cm]"." ".$TallosBuncheOferta." st/bu"." ".$featureOferta; ?></h5>
                                                                                                                                      <span class="d-block text-black fs--14">Quantity: <?php echo $qtyOferta; ?></span>
                                                                                                                                      <span class="d-block text-black fs--15"><?php echo $productSubcategory." ".$varidadRequest." ".$sizeRequest." [cm]"." ".$TallosBuncheRequest." st/bu"." ".$featureRequest." / $".$priceRequest; ?></span>
                                                                                                                                      <span class="d-block text-black fs--15"><?php echo $farmOfertaLabel[1]; ?></span>

                                                                                                                                      <!-- Modal -->
                                                                                                                                      <div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
                                                                                                                                        <div class="modal-dialog" role="document">
                                                                                                                                          <div class="modal-content">
                                                                                                                                            <!-- Header -->
                                                                                                                                            <div class="modal-header">
                                                                                                                                              <h5 class="modal-title" id="exampleModalLabelMd">
                                                                                                                                                Standard Rose Freedom 50 25st/bu
                                                                                                                                              </h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
                                                                                                                                            </div><!-- Content -->
                                                                                                                                            <div class="modal-body">
                                                                                                                                              <img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
                                                                                                                                            </div>
                                                                                                                                          </div>
                                                                                                                                        </div>
                                                                                                                                      </div><!-- /Modal -->

                                                                                                                                    </td><!-- price -->
                                                                                                                                    <td>
                                                                                                                            <span class="d-block text-danger fs--14"><sup>*</sup>Price <?php echo $priceOferta; ?> <sup class="text-muted fs--10">USD</sup></span>
                                                                                                                                      <a class="d-block text-success fs--15 js-ajax-modal"><?php echo $farmRequestLabel; ?></a>
                                                                                                                                      <span class="d-block text-muted fs--13"><?php echo $tipoRequest; ?></span>
                                                                                                                                      <span class="d-block text-muted fs--13">Quantity: <?php echo $qtyRequest; ?></span>
                                                                                                                                    </td><!-- brand -->
                                                                                                                                    <td class="text-muted text-left">
                                                                                                                                      <span class="fs--15"><?php echo $tipoOferta; ?></span>
                                                                                                                                    </td><!-- status -->
                                                                                                                                    <td class="text-left custom_td">
                                                                                                                                    <div class="d-flex flex-fill ml-0 mr-0">

                                                                                                                        <!-- QUANTITY INPUT -->
                                                                                                                        <?php echo $orderStatus; ?>
                                                                                                                      </div>
                                                                                                                        </td><!-- options -->
                                                                                                                            </tr>
                                                                                                                          <?php } ?>
                                                                                                                                  <!-- product -->
                                                                                                                                </tbody>
                                                                                                                                <tfoot>
                                                                                                                                  <?php  if($id_Status==2)
                                                                                                                                    { ?>
                                                                                                                                  <th  class="text-green-500 w--50">

                                                                                                                                  </th>

                                                                                                                                  <th  class="text-red-500 w--50">

                                                                                                                                  </th>
                                                                                                                                <?php } ?>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      IMG
                                                                                                                                    </th>
                                                                                                          												  <th class="text-gray-500 font-weight-normal fs--14 w--700">
                                                                                                                                      PRODUCT NAME
                                                                                                                                    </th>
                                                                                                          													 <th class="text-gray-500 font-weight-normal fs--14 w--300">
                                                                                                                                      PRICE
                                                                                                                                    </th>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--300">
                                                                                                                                      BRAND
                                                                                                                                    </th>
                                                                                                                                    <th class="text-gray-500 font-weight-normal fs--14 w--100">
                                                                                                                                      STATUS
                                                                                                                                    </th>
                                                                                                          												</tr>
                                                                                                      													</tfoot>
                                                                                                          										</table>
                                                                                                          						</div>


                                                                                                          						<!-- Footer -->
                                                                                                          						<div class="modal-footer">

                                                                                                                        <?php
                                                                                                                        echo $botonProcess;
                                                                                                                       ?>

                                                                                                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                                                                              <i class="fi fi-close"></i>
                                                                                                                              Close
                                                                                                                          </button>
                                                                                                          						</div>

                                                                                                          				</div>
                                                                                                          		</div>
                                                                                                          </div>
                                                                                                          <!-- Billing Modal -->




                            </td>


                          </tr>




                          <?php





                              }
                             ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Img</th>
                            <th>Request</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Farm</th>
                            <th>Order Type</th>
                            <th>Status</th>


                          </tr>
                        </tfoot>
                      </table>
              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->


      <!--Select Orders Modal Open-->
      <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
          <div class="modal-dialog modal-md modal-md" role="document">
              <div class="modal-content">

                  <!-- header modal -->
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fi fi-close fs--18" aria-hidden="true"></span>
                    </button>

                  </div>
                  <!-- body modal 3-->
                  <form action="../en/florMP.php" method="post" id="payment-form">
                  <div class="modal-body">
                      <div class="table-responsive">

                        <font color="#000">Please, before to continue select an order.</font><br><br>

                        <div class="form-label-group mb-3">
                        <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                    <option value='0'>Select Previous Order</option>
                                    <?php
                                            $sel_order="select id , order_number ,del_date , qucik_desc
                                                          from buyer_orders
                                                         where del_date >= '" . date("Y-m-d") . "'
                                                           and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                            $rs_order=mysqli_query($con,$sel_order);

                                        while($orderCab=mysqli_fetch_array($rs_order))  {
                                    ?>
                                            <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                    <?php
                                        }
                                       ?>
                        </select>

                        <label for="select_options">Select Previous Order</label>
                         <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                      </div>



                                              <br>
                                             <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                      </div>



                  </div>

                  <div class="modal-footer request_product_modal_hide_footer">
                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                  <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                      <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                  </div>
                  </form>
              </div>
          </div>
      </div>



			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
			<script>
			function checkOrderPrevious(){
			      var orderP =  document.getElementById('selectPreviousOrder').value;
			      if(orderP==0){
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = true;
			  }
			      else{
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = false;
			        }
			    }


			        function checkOption() {


			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            if (myRadio.filter(':checked').length > 0) {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shippingMethod,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        $('#nextOpt').click();
			                        _pickers();//show calendar
			                    }
			                });




			            } else {
			                $('#erMsg').show();
			            }
			        }

			        //esta  es  la  nueva  opcion  by  Jose Portilla
			        function shippingChange(product_id, sizename, i) {
			            var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
			            if (shipping_val != "") {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        _pickers();//show calendar
			                    }
			                });

			            } else {
			                $('#erMsg').show();
			            }
			        }

			        function send_request() {
			            var delDate = $('#cls_date').find("input").val();
			            var qucik_desc = $('#qty_desc').val();
			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            var dateRange = "";

			            var flag_s = true;


			            if (qucik_desc == "") {
			                alert("Please enter quick description.");
			                flag_s = false;
			            }
			            else if (delDate == "") {
			                alert("Please select delivery date.");
			                flag_s = false;
			            }

			            if (flag_s == true) {
			                $.ajax({
			                    type: 'post',
			                    url: '/file/redirectrequest.php',
			                    data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
			                    success: function (data) {
			                        alert("Your order was created successfully");
			                        window.location.href = '/buyer/ordersnd.php';
			                    },
			                    error: function () {
			                        alert(" ! Your order has not been created !");
			                    }
			                });
			            }


			        }
			</script>



      <script type="text/javascript">

      window.onload=function() {

        var num = document.getElementById("numRecords").value;

        for(var i=1; i<=num; i++){


        var minutes = $('#set-time'+i).val();
        var idRBID = $('#valRBID'+i).val();
      //  alert(minutes);
        var datosRBID = "IDGRWO="+idRBID;

        var target_date = new Date().getTime() + ((minutes * 60 ) * 1000);
        // set the countdown date
        var time_limit = ((minutes * 60 ) * 1000);
        //set actual timer
        setTimeout(
          function()
          {
            $.ajax({
                 type: "POST",
                 url: "UpdateOfferComplete.php",
                 data: datosRBID,
                 cache: false,
                 success: function(r){
                     // alert(r);
                    //  return false;
                     if(r==11)
                     {
                       swal("Request Approved!", "Your request was Approved.","warning");
                       var x = setInterval(function() {
                         location.reload();

                         /*********************************/
                         // Actualizar Oferta             //
                         /*********************************/




                         swal.close();
                       }, 2000);
                     }
                 }
             });






          }, time_limit );

        var days, hours, minutes, seconds; // variables for time units

        var countdown = document.getElementById("tiles"); // get tag element


        getCountdown();

        setInterval(function () { getCountdown(); }, 1000);

        function getCountdown(){

          // find the amount of "seconds" between now and target
          var current_date = new Date().getTime();
          var seconds_left = (target_date - current_date) / 1000;

        if ( seconds_left >= 0 ) {
          console.log(time_limit);
           if ( (seconds_left * 1000 ) < ( time_limit / 2 ) )  {
             $( '#tiles' ).removeClass('color-full');
             $( '#tiles' ).addClass('color-half');

            }
            if ( (seconds_left * 1000 ) < ( time_limit / 4 ) )  {
              $( '#tiles' ).removeClass('color-half');
              $( '#tiles' ).addClass('color-empty');
            }

          days = pad( parseInt(seconds_left / 86400) );
          seconds_left = seconds_left % 86400;

          hours = pad( parseInt(seconds_left / 3600) );
          seconds_left = seconds_left % 3600;

          minutes = pad( parseInt(seconds_left / 60) );
          seconds = pad( parseInt( seconds_left % 60 ) );

          // format countdown string + set tag value
          countdown.innerHTML = "<span>" + hours + ":</span><span>" + minutes + ":</span><span>" + seconds + "</span>";



        }



        }

        function pad(n) {
          return (n < 10 ? '0' : '') + n;
        }

      }
      }

      function approveAll(h)
      {
        //var totalOffert = document.getElementById('totalOffert').value;

        for(var i=1; i<=h; i++){


        document.getElementById('approve'+i).value = 1;
        document.getElementById('approve'+i).checked = true;

        document.getElementById('decline'+i).value = 4;
        document.getElementById('decline'+i).checked = false;

        }

      }

      function declineAll(k)
      {
      //  var totalOffert = document.getElementById('totalOffert').value;

        for(var i=1; i<=k; i++){

        document.getElementById('decline'+i).value = 4;
        document.getElementById('decline'+i).checked = true;

        document.getElementById('approve'+i).value = 1;
        document.getElementById('approve'+i).checked = false;

        }

      }

      function approveOne(a){

        document.getElementById('decline'+a).value = 4;
        document.getElementById('decline'+a).checked = false;

        document.getElementById('approve'+a).value = 1;
        document.getElementById('approve'+a).checked = true;
      }

      function declineOne(a){

        document.getElementById('decline'+a).value = 4;
        document.getElementById('decline'+a).checked = true;

        document.getElementById('approve'+a).value = 1;
        document.getElementById('approve'+a).checked = false;
      }

      function verifica(l){

        //var totalOffert = $('#totalOffert').val();
      //  alert(l);
        var total = 0;

              for(var i=1; i<=l; i++){
              if(document.getElementById('decline'+i).checked || document.getElementById('approve'+i).checked){
                  total = total + 1;
                }
              }

        if(l==total)
        {
          return true;
        }else{
          return false;
        }
    }

      function sendApprove(l)
      {
        if(verifica(l)){
          swal({
                 title: "Are you sure?",
                 text: "Once Approved, you will not be able change the action!",
                 icon: "warning",
                 buttons: true,
                 dangerMode: true,
               })
               .then((willDelete) =>
               {
                 if (willDelete) {


                   var idRBID = $('#requestIDID').val();


              //     alert(datosRBID);
                //   return false;

                var offersId=[];
                var values=[];

                for (var i=1; i<=l; i++) {

                    var ofertId   = $("#ofertId" + i).val();
                      var value;

                    if(document.getElementById('approve'+i).checked){
                       value = document.getElementById('approve'+i).value
                    //  alert(1);
                    }
                    else{
                    if(document.getElementById('decline'+i).checked){
                       value = document.getElementById('decline'+i).value
                    //  alert(2);
                    }
                   }

                    offersId[i]=ofertId;
                    values[i]=value;
                }

                var datosRBID = "IDGRWO="+idRBID+'&s_offersId=' + offersId + '&s_values=' + values+ '&s_total=' + l;
              //  alert(datosRBID);
              //  return false;

                $.ajax({
                     type: "POST",
                     url: "UpdateOfferCompleteApproved.php",
                     data: datosRBID,
                     cache: false,
                     success: function(r){
                        //  alert(r);
                        //  return false;
                         //if(r==11)
                         //{
                           swal("Request Approved!", "Your request was Approved.","warning");
                           var x = setInterval(function() {
                             location.reload();

                             /*********************************/
                             // Actualizar Oferta             //
                             /*********************************/




                             swal.close();
                           }, 2000);
                        // }
                     }
                 });



                 } else {
                   //Nothing to do!

                 }
               });

        }
        else{
          swal("Incorrect value!", "All options are required","warning");
        }

      }

      </script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
