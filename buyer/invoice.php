<!doctype html>

<?php

// PO 2018-08-24

require_once("../config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
$userSessionID = $_SESSION["buyer"];
$idfac = $_GET['idi'];

if (isset($_GET['sd']) && !empty($_GET['sd'])) {
    $id = $_GET['sd'];
    $qry = "DELETE FROM `buyer_shipping_methods` WHERE `id` = '$id'";


    echo '<script language="javascript">alert("' . $id . '");</script>';
    if (mysqli_query($con, $qry)) {
        header("location:" . SITE_URL . "buyer/buyers-account.php");
        die;
    }
}



/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php


   $buyerEntity = "select b.first_name,b.last_name,c.name from buyers  b , country c where b.id = '" . $userSessionID . "'  and c.id=b.country" ;

   $buyer = mysqli_query($con, $buyerEntity);
   $buy = mysqli_fetch_array($buyer);
   
   
   $buyerOrder = "select id_fact         , buyer_id         , order_number, 
                         order_date      , shipping_method  , del_date    , 
                         date_range      , is_pending       , order_serial, 
                         seen            , delivery_dates   , lfd_grower  , 
                         quick_desc      , bill_number      , gross_weight, 
                         volume_weight   , freight_value    , guide_number, 
                         total_boxes     , sub_total_amount , tax_rate    , 
                         shipping_charge , handling         , grand_total , 
                         bill_state      , date_added       , user_added  ,
                         air_waybill     , charges_due_agent,
                         credit_card_fees, per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact  = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab);
   
$id_fact_cab = $buyerOrderCab['id_fact'];
$buyer_cab   = $buyerOrderCab['buyer_id'];

   $sqlDetalis="select id_fact            , id_order       , order_serial  , 
                       cod_order          , product        , sizeid        , 
                       qty                , buyer          , date_added    , 
                       bunches            , box_name       , lfd           , 
                       comment            , box_id         , shpping_method, 
                       mreject            , bunch_size     , unseen        , 
                       inventary          , offer_id       , prod_name     , 
                       product_subcategory, size           , boxtype       , 
                       bunchsize          , boxqty         , bunchqty      , 
                       steams             , gorPrice       , box_weight    , 
                       box_volumn         , grower_box_name, reject        , 
                       reason             , coordination   , cargo         , 
                       color_id           , gprice         , tax           , 
                       cost_ship          , round(handling,0) as handling       , grower_id     , offer_id_index,
                       substr(rg.growers_name,1,19) as name_grower  , salesPrice,salesPriceCli      
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 where buyer    = '" . $buyer_cab . "'
                   and id_fact  = '" . $id_fact_cab . "' order by grower_id , id_order ";

    $result = mysqli_query($con, $sqlDetalis);     


 ?>

<form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" action="invoice.php" >
    
	<!--<body>-->
		
			<section id="middle">

				<!-- page title -->
				<header id="page-header">
					<h1>Invoice</h1>
					<ol class="breadcrumb">
						<li><a href="#">Pages</a></li>
						<li class="active">Invoice Page</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">

						<div class="panel-body">

							<div class="row">

								<div class="col-md-6 col-sm-6 text-left">
                                                            <img src="<?php echo SITE_URL; ?>includes/assets/images/logo.png" alt="admin panel" height="35"/> 
<br>
									<h4><strong>Client</strong> Details.</h4>
									<ul class="list-unstyled">
										<li><strong>First Name   :</strong><?php echo $buy['first_name']; ?></li>
										<li><strong>Last Name    :</strong><?php echo $buy['last_name']; ?> </li>
										<li><strong>Country      :</strong><?php echo $buy['name']; ?></li>
										<li><strong>Delivery Date:</strong> <?php echo $buyerOrderCab['del_date']; ?></li>                                                                                                                                                                                                                                             
									</ul>

								</div>

								<div class="col-md-6 col-sm-6 text-right">
                                                                        <br>                                                                    
                                                                        <br>                                                                                                                                            
									<h4><strong>Shipping</strong> Details</h4>

									<ul class="list-unstyled">
										<li><strong>P.O. NUMBER:</strong><?php echo $buyerOrderCab['order_number']; ?></li>

                                                                                <li>
                                                                                    <strong>Total Boxes:</strong><?php echo $buyerOrderCab['total_boxes']; ?>
                                                                                </li>
                                                                                <?php if ($buyerOrderCab['bill_state'] == "P") {  ?>                                                                              
                                                                                        <li><strong>Gross Weight:</strong> Pending... </li>                                                                                
                                                                                        <li><strong>Volume Weight:</strong> Pending... </li>
                                                                                <?php }else {  ?> 
                                                                                        <li><strong>Gross Weight:</strong><?php echo $buyerOrderCab['gross_weight']; ?></li>                                                                                
                                                                                        <li><strong>Volume Weight:</strong><?php echo $buyerOrderCab['volume_weight']; ?></li>
                                                                                <?php }  ?>        
                                                                                

                                                                                
                                                                                <li>                                                                                    
                                                                                    <div class="col-md-6 col-sm-6 text-right"></div>  
                                                                                </li>
									</ul>

								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Variety</th>
											<th>Stem/Bunch</th>
											<th>Sale Price</th>                                                                                        
											<th>Qty</th>
											<th>Subtotal</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                              while($row = mysqli_fetch_assoc($result))  {
                                                                            ?>
										<tr>
										    <td>
                                                                                        <?php
                                                                                                $res = "";
                                                                                            if ($row['boxtype'] == "HB") {
                                                                                                $res = "Half Boxes";
                                                                                            } else if ($row['boxtype'] == "EB") {
                                                                                                $res = "Eight Boxes";
                                                                                            } else if ($row['boxtype'] == "QB") {
                                                                                                $res = "Quarter Boxes";
                                                                                            } else if ($row['boxtype'] == "JB") {
                                                                                                $res = "Jumbo Boxes";
                                                                                            }
        
        $sel_boxg = "select id_order,offer_id_index,sum(boxqty) as totbox,count(*) as reg
                     from invoice_requests 
                    where id_fact='" . $row['id_fact'] . "'
                      and grower_id='" . $row['grower_id'] . "'
                    group by id_order,offer_id_index
                    order by id_order ";
        $rs_boxg = mysqli_query($con,$sel_boxg);       
          
        $totalr = mysqli_num_rows($rs_boxg);
        
        $cajas = 0;
        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {

              if ($tot_boxg['totbox'] > $tot_boxg['reg']) {
                        if ($tot_boxg['reg']== 1) {
                             $cajas =  $cajas + $tot_boxg['totbox'] ;                                     
                        }else{
                             $cajas =  $cajas + ($tot_boxg['totbox']/$tot_boxg['reg']) ;                                                                 
                        }                                    
              }else{
                  $cajas = $cajas + 1 ;
              }
        }

         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type from product where id = '" . $row['product'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']*$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }        



                                                                                        ?>
                                                                                        <?php  if ($row['grower_id'] != $tmp_idorder) {?>                                                                                                
                                                                                                <p>     </p>
                                                                                                <strong><?php echo $row['name_grower']." (". $cajas." ".$res.")"  ?></strong> 
                                                                                                <p>     </p>
                                                                                        <?php  }?>
                                                                                                
												<div><?php echo $row['prod_name']." ".$row['size']." cm"." ".$row['steams']." st/bu"; ?></div>
												<small><?php echo $row['product_subcategory'] ?></small>
										    </td> 
                                        
                                                                                    <td>
                                                                                        <?php  if ($row['grower_id'] != $tmp_idorder) {?>
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br> 
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br>
                                                                                        <?php  }?>        
                                                                                        <?php echo $unitFac ?>   
                                                                                    </td>   
                                                                                    
                                                                                    
										    <td>
                                                                                        <?php  if ($row['grower_id'] != $tmp_idorder) {?>
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br> 
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br>
                                                                                        <?php  }?>        
                                                                                        <?php echo "$".number_format($row['salesPriceCli'], 2, '.', ',') ; ?>
                                                                                    </td>                                                                                    
                                                                                    
										    <td>
                                                                                        <?php  if ($row['grower_id'] != $tmp_idorder) {?>
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br> 
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br>
                                                                                        <?php  }?>        
                                                                                        <?php echo $qtyFac;?>
                                                                                    </td>
                                                                                    
										    <td>
                                                                                        <?php  if ($row['grower_id'] != $tmp_idorder) {?>
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br> 
                                                                                                <?php " " ?>   <br>
                                                                                                <?php " " ?>   <br>
                                                                                        <?php  }?>
                                                                                        <?php echo "$".number_format($Subtotal, 2, '.', ',');?>
                                                                                    </td>                                                                                        
                                                                                 
										</tr>																				
											<?php 
                                                                                                $totalCal = $totalCal + $Subtotal;   
                                                                                                $tmp_idorder = $row['grower_id'];
                                                                               } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>Contact</strong> Details</h4>

									<p class="nomargin nopadding">
										<strong>Note:</strong> 
										<?php echo $buyerOrderCab['quick_desc']; ?>
									</p><br><!-- no P margin for printing - use <br> instead -->

									<address>
                                                                                Av. Interoceanica OE6-73 y Gonzalez Suarez<br>
                                                                                Quito, Ecuador<br>
                                                                                Phone: +593 602 2630<br>
                                                                                Email:info@freshlifefloral.com                                                                                
									</address>

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<ul class="list-unstyled">
                                                                            <li>
                                                                                <strong>Sub - Total Amount: </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>
                                                                            </li>                                                     
                                                                            <?php if ($buyerOrderCab['bill_state'] == "P") {  ?>      
                                                                                    <li><strong>Freight ($ per kg):</strong> Pending... </li>                                                                            
                                                                                    <li><strong>Air Waybill AWC:</strong> <?php echo "$".number_format($buyerOrderCab['air_waybill'], 2, '.', ',');  ?>  </li>                                                                                
                                                                                    <li><strong>Charges Dues Agent AWA:</strong> <?php echo "$".number_format($buyerOrderCab['charges_due_agent'], 2, '.', ',');  ?></li>
                                                                                    <li><strong>FLF Handling:</strong> Pending... </li>                                                                            
                                                                                    <li><strong>Total:</strong> Pending... </li>  
                                                                            <?php }else {  ?>  
                                                                                    <li><strong>Freight ($ per kg):         </strong> <?php echo "$".number_format($buyerOrderCab['freight_value'], 2, '.', ',');?> </li>                                                                            
                                                                                    <li><strong>Air Waybill AWC:            </strong> <?php echo "$".number_format($buyerOrderCab['air_waybill'], 2, '.', ',');  ?>  </li>                                                                                
                                                                                    <li><strong>Charges Dues Agent AWA:     </strong> <?php echo "$".number_format($buyerOrderCab['charges_due_agent'], 2, '.', ',');  ?></li>
                                                                                    
                                                                                    <li><strong>FLF Handling:               </strong> <?php echo "$".number_format($buyerOrderCab['handling'], 2, '.', ',');  ?> </li>                                                                            
                                                                                    <li><strong>Total:                      </strong> <?php echo "$".number_format($buyerOrderCab['grand_total'], 2, '.', ',');  ?> </li>                                                                                      
                                                                            <?php }  ?>                                                                                            
                                                                                

									</ul>  

								</div>
                                                            
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">
						<div class="panel-body">
							<a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_invoice2.php?b=<?php echo $idfac; ?>" target="_blank"><i class="fa fa-print"></i> INVOICE</a>                                                    
							<a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_invoice_comm2.php?b=<?php echo $idfac; ?>" target="_blank"><i class="fa fa-print"></i> COMMER. INVOICE</a>
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
			</section>
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	</body>
        </form>
</html>
