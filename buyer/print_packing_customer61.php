<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];

    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact  = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Packing   
   
      $sqlDetalis="select br.id_order as id_fact,
                          br.product            ,                            
                          br.qty                ,                            
                          cl.name  as colorname ,                          
                          br.id as idbr         , 
                          br.lfd                ,
                          br.feature            , 
                          p.id as codvar        ,
                          p.name    prod_name   ,
                          f.name as featurename ,
                          x.name as namecli     ,
                          'Assign' as type      ,
                          br.id_client          ,                           
                          p.stems               , cl.name as colorname,          
                          (substr(p.price,2,10)*p.stems*br.qty)/0.75 as price_sale,
                          substr(p.price,2,10) as price
                     from reser_requests br
                    inner join product p on br.product = p.id 
                    inner join sub_client x ON br.id_client = x.id                          
                    inner join colors cl ON p.color_id = cl.id                          
                     left join features f on br.feature = f.id 
                    where br.buyer     = '" . $userSessionID . "'
                      and br.id_order  = '" . $idfac . "' 
                      and p.id_tag = 5                          
                order by namecli   ";

        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'HOLLAND BY CUSTOMER - SALES',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Name ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$idfac,0,1,'R');  
       
    
    if ($userSessionID != 318) {
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');
       $pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');     
    }else{
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');  
    }
      
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');        

    
    $pdf->Cell(70,6,'-'.$buy['company'],0,1,'L');  
    
    
    $pdf->Ln(10);
    //$pdf->Cell(40,6,'Box Number',0,0,'C');    
    $pdf->Cell(120,6,'Client',0,0,'L');
    $pdf->Cell(20,6,'Bunch',0,0,'L');
    $pdf->Cell(20,6,'Price',0,0,'L');
    $pdf->Cell(20,6,'Total',0,1,'R');    

    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;

    
    while($row = mysqli_fetch_assoc($result))  {        
        
         if ($row['id_client'] != $tmp_idorder) {
               $pdf->SetFont('Arial','B',9);                   

                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $pdf->Cell(70,6,$row['namecli'],0,1,'L');   
                                                    
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
         }        

         $pdf->SetFont('Arial','',8);

         //$pdf->Cell(70,4,$row['prod_name'],0,0,'L');  
         $pdf->Cell(120,4,$row['prod_name']." ".$row['subcatename']." ".$row['stems']." st/bu ".$row['colorname'],0,0,'L');                                                                
                           
         $pdf->Cell(20,4,$row['qty'],0,0,'L');                                                   
         $pdf->Cell(20,4,'$'.$row['price'],0,0,'L');                                                   
         $pdf->Cell(20,4,'$'.number_format($row['price_sale'], 2, '.', ','),0,1,'R');    

         
               $totalgen = $totalgen + $row['price_sale'];
               
                $tmp_idorder = $row['id_client'];       
    }
            
        $pdf->Ln(2);
        $pdf->Cell(100,6,'___________________________________________________________________________________________________________________',0,1,'L');  

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(180,4,'Total : $'.number_format($totalgen, 2, '.', ','),0,1,'R');    
    
    
  $pdf->Output();
  ?>