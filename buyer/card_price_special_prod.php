<?php
session_start();


require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');


if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}

if ($_SESSION["grower"] >= 1) {
    header("location: /vendor-account.php");
    die;
}

$userSessionID = $_SESSION["buyer"];

$clienteId = $_GET['id_cli'];
$subcateId = $_GET['idsub'];
$sizeId    = $_GET['idsize'];
$relaId    = $_GET['id_gid'];


if (isset($_GET['sd']) && !empty($_GET['sd'])) {
    $id = $_GET['sd'];

    echo '<script language="javascript">alert("' . $id . '");</script>';
    if (mysqli_query($con, $qry)) {
        header("location:/ buyer/buyers-account.php");
        die;
    }
}


//  Actualizacion en Grupo
if (isset($_REQUEST["total"])) {

    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {

            $update = "update card_param_product_price
                          set price_card  = '" . $_POST["price_card-". $_POST["pro-" . $i]] . "'
                        where id='" . $_POST["pro-" . $i] . "'  ";

            mysqli_query($con, $update);

    }

  }


  if(isset($_POST["submiso"]) && $_POST["submiso"]=="Add PR" && $_POST["submiso"]!='')	{

   $sel_card="select gp.id   ,
                      gp.idsc ,
                      gp.subcategory ,
                      gp.size ,
                      gp.stem_bunch  ,
                      gp.description ,
                      gp.type , gp.feature ,
                      gp.id_ficha ,
                      s.name as namesize   ,
                      f.name as namefeature ,
                      b.name as namestems
                 from grower_parameter gp
                inner JOIN sizes s       ON gp.size       = s.id
                inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                 left JOIN features f    ON gp.feature    = f.id
                where gp.id = '" . $_POST["var_add"] . "' " ;

   $rs_card = mysqli_query($con,$sel_card);
   $ficha   = mysqli_fetch_array($rs_card);

       $insert="insert into card_param_product_price set
                            idsc        = '".$ficha["idsc"]."'  ,
                            size        = '".$ficha["size"]."'  ,
                            stem_bunch  = '".$ficha["stem_bunch"]."'  ,
                            description = 'PROD-99' ,
                            type        = 0        ,
                            feature     = '".$ficha["feature"]."'   ,
                            id_ficha    = '".$ficha["id_ficha"]."'  ,
                            relacion    = 0  ,
                            price_card  = '".$_POST["price_card"]."',
                            id_param    = '".$relaId."'             ,
                            product_id  = '".$_POST["var_prod"]."'   ";

                     @mysqli_query($con,$insert);
  }

$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 01 Jul 2021
Structure MarketPlace previous to buy
**/


// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

include('../back-end/inc/header_ini.php');
?>
<script src="../back-end/assets/js/blockui.js"></script>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->

<?php


     $sqlDetalis="select cpc.id  as gid      , cpc.idsc    , cpc.size     , cpc.stem_bunch , cpc.description ,
                        cpc.type      , cpc.feature , cpc.id_ficha , cpc.relacion   , cpc.price_card  ,
                        s.name as subcate  , sz.name as namesize    , f.name as namefeature ,
                        b.name as namestems , p.name as pname
                   from card_param_product_price cpc
                  INNER join subcategory s on cpc.idsc = s.id
                  INNER join product p on cpc.product_id = p.id
                  INNER JOIN sizes sz on cpc.size = sz.id
                  inner JOIN bunch_sizes b ON cpc.stem_bunch = b.id
                   left JOIN features f   ON cpc.feature    = f.id
                  where cpc.idsc      = '" . $subcateId . "'
                    and cpc.size      = '" . $sizeId . "'  ";




    $result = mysqli_query($con, $sqlDetalis);


 ?>

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Special Price Product GENERAL </strong>
						</h1>
					</div><!-- Primary-->


					<section class="rounded mb-3 bg-white" id="section_1">

            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">

						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
              <a href="/buyer/card_price_gen.php" type="submit" class="btn btn-success mb-3 mt-3 d-block-xs w-100-xs">
  												<i class="fa fa-long-arrow-left" aria-hidden="true"></i>
  												Back
  											</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>

							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>





								<div class="row row-grid b-0">

									<div class="col-12 col-sm-12 col-md-12 col-lg-12">

                    <input  id="submitu" type="submit" class="btn btn-warning mb-3 mt-3 d-block-xs w-100-xs" name="submitu" value="Update PR">


										<div class="px-2 pb-2">


                      <h4><strong>Special Price Product</strong> </h4>
                      <table class="table table-align-middle table-sm">


                      		<tbody>

                      			<tr>
                      				<td>
                                Card
                            	</td>
                      				<!-- SKU -->
                      				<td>
                                <?php 	 $sel_prodadd="select gp.id   ,
                                                             gp.idsc ,
                                                             gp.subcategory ,
                                                             gp.size ,
                                                             gp.stem_bunch  ,
                                                             gp.description ,
                                                             gp.type ,
                                                             gp.feature ,
                                                             gp.id_ficha ,
                                                             s.name as namesize   ,
                                                             f.name as namefeature ,
                                                             b.name as namestems
                                                        from grower_parameter gp
                                                       inner JOIN sizes s       ON gp.size       = s.id
                                                       inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                                                        left JOIN features f    ON gp.feature    = f.id
                                                       where gp.idsc = '".$subcateId."'
                                                         and gp.size = '".$sizeId."'  " ;

                                                $rs_prodadd=mysqli_query($con,$sel_prodadd);

                                ?>

                                <select name="var_add" id="var_add" class="form-control form-control-lg" >
                                    <?php
                                        while($prodcal=mysqli_fetch_array($rs_prodadd)){

                                    ?>
                                            <option value="<?php echo $prodcal["id"]?>">
                                                <?php echo $prodcal["subcategory"]." Card ".$prodcal["id_ficha"]." ".$prodcal["namesize"]." cm ".$prodcal["namestems"]." st/bu"." ".$prodcal["namefeature"] ?>
                                            </option>
                                    <?php    }	?>
                                </select>
                      				</td>
                              		</tr>
                            <tr>

                              <td>
                                Product
                              </td>
                              <!-- SKU -->
                              <td>
                                <?php 	$sel_val="select gcd.product_id , p.name as nameprod
                                                    from growcard_prod_bunch_sizes gcd
                                                   inner join product p on gcd.product_id = p.id
                                                   where p.subcategoryid   = '".$subcateId."'
                                                   group by gcd.product_id
                                                   order by p.name" ;

                                                $rs_val = mysqli_query($con,$sel_val);

                                ?>

                                <select name="var_prod" id="var_prod"  class="form-control form-control-lg" >
                                            <option value="">--Select--</option>
                                    <?php
                                        while($prodcli =mysqli_fetch_array($rs_val)){

                                    ?>
                                            <option value="<?php echo $prodcli["product_id"]?>">
                                                <?php echo $prodcli["nameprod"]?>
                                            </option>
                                    <?php    }	?>
                                </select>
                              </td>
                      			</tr>
                            <tr>
                              <td>
                                Price
                              </td>
                              <td>
                                <input type="number"  step="0.01" class="form-control form-control-lg" name="price_card" id="price_card" value="<?php echo $_POST["price_card"];?>" />

                              </td>

                            </tr>

                      		</tbody>
                      	</table>

  <input  id="submiso" type="submit" class="btn btn-purple btn-lg mb-3 mt-3 d-block-xs w-100-xs float-right" name="submiso" value="Add PR">


									</div>
                  </div>







								</div>


								<!-- customer detail -->

							</div>






								<table class="table-datatable table table-bordered table-hover table-striped"
                        data-lng-empty="No data available in table"
                        data-responsive="true"
                        data-header-fixed="true"
                        data-select-onclick="true"
                        data-enable-paging="true"
                        data-enable-col-sorting="true"
                        data-autofill="false"
                        data-group="false"
                        data-items-per-page="10">
									<thead>
										<tr>
                      <th>Subcategory - Product</th>
                      <th>Card</th>
                      <th>Size</th>
                      <th>Stems</th>
                      <th>Features</th>
                      <th>Price</th>
                      <th></th>
										</tr>
									</thead>
                  <tbody>

                <?php

                                                $cn=1;

                                         while($row = mysqli_fetch_assoc($result))  {

                                         ?>
                  <tr>
                    <td>
                      <div><?php echo $row['subcate']." ".$row['pname']; ?></div>
                    </td>

                    <td>
                        <?php echo $row['id_ficha']; ?>
                    </td>

                    <td>
                        <?php echo $row['namesize']."  cm." ; ?>
                      </td>

                    <td>
                          <?php echo $row['namestems'];?>
                    </td>

                    <td>
                        <?php echo $row['namefeature'];?>
                    </td>


                    <td>
                        <input type="text" class="form-control form-control-sm" name="price_card-<?php echo $row["gid"]; ?>" id="price_card-<?php echo $row["gid"]; ?>" value="<?php echo $row['price_card']; ?>">
                    </td>

                    <td>
                           <button  onclick="deleteR('<?php echo $row["gid"]; ?>','<?php echo $subcateId; ?>','<?php echo $sizeId; ?>','<?php echo $relaId; ?>')" class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" type="button">Delete</button>
                    </td>



                    <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/>


                  </tr>
                  <?php
                                                                                      $cn++;
                                                                             } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Subcategory - Product</th>
                      <th>Card</th>
                      <th>Size</th>
                      <th>Stems</th>
                      <th>Features</th>
                      <th>Price</th>
                      <th></th>
                    </tr>
                  </tfoot>
								</table>










								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
          	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
          </form>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>



                                                    <br>
                                                   <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>




			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
      <script>
      /*Button send DELETE */
      function deleteR(id, subc, sized, relad) {

          var flag_s = true;

          var id_delete      = id;


      if (flag_s == true) {

          $.ajax({
              type: 'post',
              url: '/buyer/deleteProductPrice.php',
              data: 'id_delete=' + id_delete,
              success: function (data_s) {

                  if (data_s == 1) {
                    swal("Correct","Record delected!","success")
                    .then((value) => {
                    
                      window.location.href = '/buyer/card_price_special_prod.php?idsub='+subc+'&idsize='+sized+'&id_gid='+relad;

                    });
                  } else {
                          swal("Sorry!", "There is some error. Please try again 1", "error");
                  }
              }
          });
      }

      }
      </script>




			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
