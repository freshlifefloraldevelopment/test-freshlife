<?php
session_start();

$userSessionID = $_SESSION["buyer"];
if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 16 Jun 2021
Structure MarketPlace previous to buy
**/
// start a session

$idR = $_GET["GETiD"];

// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini.php');
?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('../back-end/inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							<strong> Orders Detail  # <?php echo $idR; ?> </strong>
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">

							Orders Detail # <?php echo $idR; ?>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm" >
							<div class="col-12 col-lg-3 col-xl-3 mb-5" style="display:none">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0" style="display:none">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>


              <div class="modal-body">
                <table class="table-datatable table table-bordered table-hover table-striped"
                        data-lng-empty="No data available in table"
                        data-responsive="true"
                        data-header-fixed="true"
                        data-select-onclick="true"
                        data-enable-paging="true"
                        data-enable-col-sorting="true"
                        data-autofill="false"
                        data-group="false"
                        data-items-per-page="10">
                        <thead>
                          <tr>
                            <th>Img</th>
                            <th>Order</th>
                            <th>St. Requested</th>
                            <th>St. Still Needed</th>
                            <th>Date</th>
                            <th>P.O. Number</th>
                            <th>Offers</th>

                          </tr>
                        </thead>
                        <tbody>

                          <?php
                                   $sel_order= "select gpb.id as cartid,
                                  p.image_path as img,
                                  s.name as subs,
                                  p.name as namep,
                                  ff.name as featurename,
                                  sh.name as sizename,
                                  gpb.qty as st_request,
                                  gpb.lfd as date_lfd,
                                  cod_order as ponumber
                                  from buyer_requests gpb
                                  left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                  left join subcategory s on p.subcategoryid=s.id
                                  left join features ff on gpb.feature=ff.id
                                  left join sizes sh on gpb.sizeid=sh.id
                                  where gpb.buyer = '$userSessionID'
                                  and gpb.id_order = '$idR'
                                  and gpb.lfd >='" . date("Y-m-d") . "'
                                  order by gpb.id desc";

                                /*  $sel_order= "select gpb.id as cartid,
                                    p.image_path as img,
                                    s.name as subs,
                                    p.name as namep,
                                    ff.name as featurename,
                                    sh.name as sizename,
                                    gpb.qty as st_request,
                                    gpb.lfd as date_lfd,
                                    cod_order as ponumber
                                    from buyer_requests gpb
                                    left join (select id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                    left join subcategory s on p.subcategoryid=s.id
                                    left join features ff on gpb.feature=ff.id
                                    left join sizes sh on gpb.sizeid=sh.id
                                    where gpb.buyer = '318'
                                    and gpb.id_order = '1295'
                                    order by gpb.id desc";*/

                                  $rs_order=mysqli_query($con,$sel_order);
                                  while($orderCab=mysqli_fetch_array($rs_order))  {

                              //   $_SESSION[$cartid] = $orderCab['cartid'];
                                $cartid = $orderCab['cartid'];
                                $img = $orderCab['img'];
                                $subs = $orderCab['subs'];
                                $namep = $orderCab['namep'];
                                $featurename= $orderCab['featurename'];
                                $sizename = $orderCab['sizename'];
                                $st_request = $orderCab['st_request'];
                                $date_lfd = $orderCab['date_lfd'];
                                $cod_order = $orderCab['ponumber'];

                                $stillNeeded = 0;



                                $sel_order1="select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                                from grower_offer_reply gr
                                left join growers g on gr.grower_id=g.id
                                left join country cr on g.country_id=cr.id
                                where gr.request_id='$cartid'
                                and gr.buyer_id='$userSessionID'
                                order by gr.date_added desc";


                                /*
                                $sel_order1="select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                              from grower_offer_reply gr
                              left join growers g on gr.grower_id=g.id
                              left join country cr on g.country_id=cr.id
                              where gr.request_id='50195'
                              and gr.buyer_id='318'
                              order by gr.date_added desc";
                              */

                              $rs_order1=mysqli_query($con,$sel_order1);
                              $rs_order1NumR=mysqli_num_rows($rs_order1);
                              while($orderCab1=mysqli_fetch_array($rs_order1))  {

                                $stillNeeded += $orderCab1['Stems_offered'];

                              }


                          ?>

                          <tr>
                            <td><span class="fs--15">
                              <figure style="border-radius: 50%; height: 53px; width: 53px;" class="d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
        												<img style="border-radius: 50%; height: 50px; width: 50px;"  src="https://app.freshlifefloral.com/<?php echo $img; ?>" alt="<?php echo $namep; ?>">
        	                     </figure>

                            </span></td>
                            <td><span class="fs--15"><?php echo $subs." <br> ".$namep." ".$featurename." ".$sizename." [cm]"; ?></span></td>
                            <td align="center"><span class="fs--15"><?php echo $st_request;  ?></span></td>
                            <td align="center"><span class="fs--15"><?php echo $stillNeeded; ?></span></td>
                            <td><span class="fs--15"><?php echo $date_lfd; ?></span></td>
                            <td><span class="fs--15"><?php echo $cod_order; ?></span></td>
                            <td>
                              <button  data-toggle="modal" data-target="#billing_modal-<?php echo $cartid;?>-<?php echo $st_request; ?>" onclick="return false;" class="btn btn-sm btn-block btn-danger bg-gradient-success text-white b-0">
                                                    <span class="p-0-xs">
                                                      <span class="fs--14">Offer <sup class="badge badge-danger position-absolute end-0 mt--n5 text-warning fs--14"><?php echo $rs_order1NumR; ?></sup></span>
                                                    </span>
                                                  </button>




                                                                                                          <!-- Billing Modal -->
                                                                                                          <div class="modal fade" id="billing_modal-<?php echo $cartid;?>-<?php echo $st_request; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                                                                                                          		<div class="modal-dialog modal-md modal-xl" role="document">
                                                                                                          				<div class="modal-content">

                                                                                                          						<!-- Header -->
                                                                                                          						<div class="modal-header">
                                                                                                          								<h5 class="modal-title" id="exampleModalLabelMd"><?php echo $st_request." Boxes ".$subs." ".$namep." ".$featurename." ".$sizename." [cm]" ?></h5>
                                                                                                          								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                          										<span class="fi fi-close fs--18" aria-hidden="true"></span>
                                                                                                          								</button>
                                                                                                          						</div>

                                                                                                          						<!-- Content -->

                                                                                                          						<div class="modal-body">
                                                                                                          							<table class="table-datatable table table-bordered table-hover table-striped"
                                                                                                          											data-lng-empty="No data available in table"
                                                                                                          											data-responsive="true"
                                                                                                          											data-header-fixed="true"
                                                                                                          											data-select-onclick="true"
                                                                                                          											data-enable-paging="true"
                                                                                                          											data-enable-col-sorting="true"
                                                                                                          											data-autofill="false"
                                                                                                          											data-group="false"
                                                                                                          											data-items-per-page="10">
                                                                                                          											<thead>
                                                                                                          												<tr>
                                                                                                          													<th>Grower</th>
                                                                                                          													<th>Size</th>
                                                                                                          													<th>Stems offered</th>


                                                                                                          												</tr>
                                                                                                          											</thead>
                                                                                                          											<tbody>

                                                                                                                                  <?php

                                                                                                                                  $sel_orderInt= "select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                                                                                                                                  from grower_offer_reply gr
                                                                                                                                  left join growers g on gr.grower_id=g.id
                                                                                                                                  left join country cr on g.country_id=cr.id
                                                                                                                                  where gr.request_id='$cartid'
                                                                                                                                  and gr.buyer_id='$userSessionID'
                                                                                                                                  order by gr.date_added desc";

                                                                                                                                  /*
                                                                                                                                  $sel_orderInt= "select g.file_path5 as img , product , size , steams , price , bunchqty*steams as Stems_offered
                                                                                                                                   from grower_offer_reply gr
                                                                                                                                   left join growers g on gr.grower_id=g.id
                                                                                                                                   left join country cr on g.country_id=cr.id
                                                                                                                                   where gr.request_id='50159'
                                                                                                                                   and gr.buyer_id='318'
                                                                                                                                   order by gr.date_added desc";*/

                                                                                                                                          $rs_orderInt=mysqli_query($con,$sel_orderInt);
                                                                                                                                      while($orderCabInt=mysqli_fetch_array($rs_orderInt))  {

                                                                                                                                         $img = $orderCabInt['img'];
                                                                                                                                         $product = $orderCabInt['product'];
                                                                                                                                         $size = $orderCabInt['size'];
                                                                                                                                         $steams = $orderCabInt['steams'];
                                                                                                                                         $price = $orderCabInt['price'];
                                                                                                                                         $Stems_offered = $orderCabInt['Stems_offered'];


                                                                                                                                  ?>

                                                                                                                                  <tr>
                                                                                                          													<td><span class="fs--15">
                                                                                                                                      <figure style="border-radius: 50%; height: 73px; width: 73px;" class="d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
                                                                                                                												<img style="border-radius: 50%; height: 70px; width: 70px;"  src="https://app.freshlifefloral.com/user/<?php echo $img; ?>" >
                                                                                                                	                     </figure>

                                                                                                                                    </span></td>
                                                                                                          													<td><span class="fs--14"><?php echo $product." ".$size."cm ".$steams."st/bu "." $".$price; ?></span></td>
                                                                                                          													<td><span class="fs--14"><?php echo "$Stems_offered";  ?></span></td>


                                                                                                          												</tr>
                                                                                                                                  <?php
                                                                                                                                      }
                                                                                                                                     ?>
                                                                                                          											</tbody>
                                                                                                          											<tfoot>
                                                                                                                                  <tr>
                                                                                                                                    <th>Grower</th>
                                                                                                          													<th>Size</th>
                                                                                                          													<th>Stems offered</th>


                                                                                                                                  </tr>
                                                                                                          											</tfoot>
                                                                                                          										</table>
                                                                                                          						</div>

                                                                                                          						<!-- Footer -->
                                                                                                          						<div class="modal-footer">
                                                                                                          								<button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                                                          										<i class="fi fi-close"></i>
                                                                                                          										Close
                                                                                                          								</button>
                                                                                                          						</div>

                                                                                                          				</div>
                                                                                                          		</div>
                                                                                                          </div>
                                                                                                          <!-- Billing Modal -->




                            </td>


                          </tr>




                          <?php





                              }
                             ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Img</th>
                            <th>Order</th>
                            <th>St. Requested</th>
                            <th>St. Still Needed</th>
                            <th>Date</th>
                            <th>P.O. Number</th>
                            <th>Offers</th>


                          </tr>
                        </tfoot>
                      </table>
              </div>


									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->


      <!--Select Orders Modal Open-->
      <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
          <div class="modal-dialog modal-md modal-md" role="document">
              <div class="modal-content">

                  <!-- header modal -->
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fi fi-close fs--18" aria-hidden="true"></span>
                    </button>

                  </div>
                  <!-- body modal 3-->
                  <form action="../en/florMP.php" method="post" id="payment-form">
                  <div class="modal-body">
                      <div class="table-responsive">

                        <font color="#000">Please, before to continue select an order.</font><br><br>

                        <div class="form-label-group mb-3">
                        <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                    <option value='0'>Select Previous Order</option>
                                    <?php
                                            $sel_order="select id , order_number ,del_date , qucik_desc
                                                          from buyer_orders
                                                         where del_date >= '" . date("Y-m-d") . "'
                                                           and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                            $rs_order=mysqli_query($con,$sel_order);

                                        while($orderCab=mysqli_fetch_array($rs_order))  {
                                    ?>
                                            <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                    <?php
                                        }
                                       ?>
                        </select>

                        <label for="select_options">Select Previous Order</label>
                         <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                      </div>



                                              <br>
                                             <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                      </div>



                  </div>

                  <div class="modal-footer request_product_modal_hide_footer">
                      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                  <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                      <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                  </div>
                  </form>
              </div>
          </div>
      </div>



			<?php include('../back-end/inc/footer.php'); ?>
			<script src="../back-end/assets/js/blockui.js"></script>
			<script>
			function checkOrderPrevious(){
			      var orderP =  document.getElementById('selectPreviousOrder').value;
			      if(orderP==0){
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = true;
			  }
			      else{
			        document.getElementById('valueOrderId_MP').value = orderP;
			        document.getElementById('orders_modal').disabled = false;
			        }
			    }


			        function checkOption() {


			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            if (myRadio.filter(':checked').length > 0) {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shippingMethod,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        $('#nextOpt').click();
			                        _pickers();//show calendar
			                    }
			                });




			            } else {
			                $('#erMsg').show();
			            }
			        }

			        //esta  es  la  nueva  opcion  by  Jose Portilla
			        function shippingChange(product_id, sizename, i) {
			            var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
			            if (shipping_val != "") {
			                $('#erMsg').hide();
			                $.ajax({
			                    type: 'post',
			                    url: '/file/get_date_modal.php',
			                    data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
			                    success: function (data) {
			                        $('.cls_date_start_date').html(data);
			                        _pickers();//show calendar
			                    }
			                });

			            } else {
			                $('#erMsg').show();
			            }
			        }

			        function send_request() {
			            var delDate = $('#cls_date').find("input").val();
			            var qucik_desc = $('#qty_desc').val();
			            var myRadio = $('input[name=row_id]');
			            var shippingMethod = myRadio.filter(':checked').val();
			            var dateRange = "";

			            var flag_s = true;


			            if (qucik_desc == "") {
			                alert("Please enter quick description.");
			                flag_s = false;
			            }
			            else if (delDate == "") {
			                alert("Please select delivery date.");
			                flag_s = false;
			            }

			            if (flag_s == true) {
			                $.ajax({
			                    type: 'post',
			                    url: '/file/redirectrequest.php',
			                    data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
			                    success: function (data) {
			                        alert("Your order was created successfully");
			                        window.location.href = '/buyer/ordersnd.php';
			                    },
			                    error: function () {
			                        alert(" ! Your order has not been created !");
			                    }
			                });
			            }


			        }
			</script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
