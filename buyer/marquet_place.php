<?php
// PO   2-jul-2018
require_once("../config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}

if (isset($_GET["id"]) && $_GET["id"] != "") {
   $idOrden = $_GET["id"];
}else{
	$idOrden=0;
}

if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
if (isset($_POST['filter_order']) || isset($_SESSION["filter_order"])) {
    
    if (isset($_POST['filter_order'])) {
        $_SESSION["filter_order"] = $_POST['filter_order'];
        $_SESSION["order"]        = $_POST['order'];
    } else {
        $_POST['filter_order'] = $_SESSION["filter_order"];
        $_POST['order']        = $_SESSION["order"];
    }
    
}


$userSessionID = $_SESSION["buyer"];

$shippingMethod = $_SESSION['shippingMethod'];
$getShippingMethod = "select connections from shipping_method where id = '" . $shippingMethod . "'";
$shippingMethodRes = mysqli_query($con, $getShippingMethod);
$shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);

$shippingMethodArray = unserialize($shippingMethodData['connections']);
$firstConnection     = $shippingMethodArray['connection_1'];
$getConnectionType = "select type from connections where id='" . $firstConnection . "'";
$conType = mysqli_query($con, $getConnectionType);
$connectionsType = mysqli_fetch_assoc($conType);
$connectionType = $connectionsType['type'];

foreach ($shippingMethodArray as $connections) {
    $getConnections = "select charges_per_kilo,charges_per_shipment from connections where id='" . $connections . "'";
    $conDatas = mysqli_query($con, $getConnections);
    $connectionDatas = mysqli_fetch_assoc($conDatas);
    $cpk = unserialize($connectionDatas['charges_per_kilo']);
    foreach ($cpk as $perkilo) {
        $chargesPerKilo = $chargesPerKilo + $perkilo;
    }
    $cps = unserialize($connectionDatas['charges_per_shipment']);
    foreach ($cps as $pership) {
        $chargesPerShip = $chargesPerShip + $pership;
    }
}
/*********get the data of session user****************/
if ($stmt = $con->prepare("SELECT id,first_name,last_name,state,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image , handling_fees , shipping , tax , duties FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $state, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image, $handling_fees, $shipping, $tax , $duties);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*********If not exist send to home page****************/
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*********If not statement send to home page****************/
    header("location:" . SITE_URL);
    die;
}
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$shipping_method = trim($shipping, ",");


/*
if (!isset($_REQUEST["gid6"])) {
    $sel_login_check = "select * from activity where buyer='" . $userID . "' and ldate='" . date("Y-m-d") . "' and ltime='" . $hm . "' and type='buyer' and atype='li'";
    $rs_login_check = mysqli_query($con, $sel_login_check);
    $login_check = mysqli_num_rows($rs_login_check);
    if ($login_check >= 1) {
    } else {
        $name = $first_name . " " . $last_name;
        $activity = "Live Inventory at " . $hm;
        $insert_login = "insert into activity set buyer='" . $userID . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='li'";
        mysqli_query($con, $insert_login);
    }
}*/

/*
$sel_testi = "select gpb.id , g.growers_name as gname , g.id as gid , 
                     g.blockstate as gbs 
                from grower_product_box_packing gpb
                left join product p on gpb.prodcutid = p.id
                left join growers g on gpb.growerid=g.id		  
               where g.active='active' 
                 and Date_format(gpb.date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y') 
                 and Date_format(gpb.date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y') 
                 and g.growers_name is not NULL ";

$sel_testi .= " group by g.id order by p.name, g.growers_name";
$rs_testi = mysqli_query($con, $sel_testi);
$total_testi = mysqli_num_rows($rs_testi);

if ($total_testi >= 1) {
    $check_grower = "";
    while ($testi = mysqli_fetch_array($rs_testi)) {
        if ($state > 0 && $testi["gbs"] != "") {
            $temp47 = explode(",", $testi["gbs"]);
            if (in_array($state, $temp47)) {
            } else {
                $check_grower .= $testi["gid"] . ",";
            }
        } else {
            $check_grower .= $testi["gid"] . ",";
        }
    }
}

$check_grower = rtrim($check_grower, ',');  */




if ($shipping_method > 0) {
    $select_shipping_info = "select name,description from shipping_method where id='" . $shipping_method . "'";
    $rs_shipping_info = mysqli_query($con, $select_shipping_info);
    $shipping_info = mysqli_fetch_array($rs_shipping_info);
}


$sel_connections = "select days from connections where shipping_id='" . $shipping_method . "' order by id desc limit 0,1";
$rs_connections = mysqli_query($con, $sel_connections);
$connections = mysqli_fetch_array($rs_connections);
$days = explode(",", $connections["days"]);
$count1 = sizeof($days);
$res = "";
for ($io = 0; $io <= $count1 - 1; $io++) {
    if ($days[$io] == 0 || $days[$io] == 1) {
        $res .= "6";
        $res .= ",";
    } else {
        $res .= $days[$io] - 1;
        $res .= ",";
    }

}
$avdays   = explode(",", $res);
$result   = array_unique($avdays);
$cunav    = sizeof($result);
$datedd   = $shpping_onr;
$tdate    = date("Y-m-d");
$date1    = date_create($datedd);
$date2    = date_create($tdate);
$interval = $date2->diff($date1);
$checka2  = $interval->format('%R%a');

if ($checka2 > 0 || $checka2 < 0) {
    $weekday = date('l', strtotime($datedd));

    switch ($weekday) {
        case 'Monday':
            $opq = 1;
            break;
        case 'Tuesday':
            $opq = 2;
            break;
        case 'Wednesday':
            $opq = 3;
            break;
        case 'Thursday':
            $opq = 4;
            break;
        case 'Friday' :
            $opq = 5;
            break;
        case 'Saturday':
            $opq = 6;
            break;
        case 'Sunday':
            $opq = 0;
            break;
    }
    if (in_array($opq, $result)) {
        $next = $opq;
    } else {
        for ($i = 1; $i <= 6; $i++) {
            $opq = $opq + 1;
            if (in_array($opq, $result)) {
                $next = $opq;
                break;
            }
            if ($opq == 6) {
                $opq = 1;
            }
        }
    }

    switch ($next) {

        case 1:
            $dayname = "monday";
            break;
        case 2:
            $dayname = "tuesday";
            break;
        case 3:
            $dayname = "wednesday";
            break;
        case 4:
            $dayname = "thursday";
            break;
        case 5:
            $dayname = "friday";
            break;
        case 6:
            $dayname = "saturday";

    }

    $shpping_onr = date('Y-m-d', strtotime('next ' . $dayname));
} else {
    $tommorow = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));
    $shpping_onr = date("Y-m-d", $tommorow);
}
$tempk      = explode("-", $shpping_onr);
$shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
$today      = date("Y-m-d");


function get_bunchs($product_id, $product_size, $buyer_id)
{
    global $con;
    $getbunches = mysqli_query($con, "SELECT gs.sizes,bs.name AS bunchname  
                                        FROM grower_product_bunch_sizes AS gs 
                                        LEFT JOIN bunch_sizes bs ON gs.bunch_sizes=bs.id 
                                       WHERE gs.grower_id = '" . $buyer_id . "' 
                                         AND gs.product_id = '" . $product_id . "'    
                                         AND gs.sizes     = '" . $product_size . "'");
    
    $rowbunches = mysqli_fetch_assoc($getbunches);
    return $rowbunches;
}


function getWeekday($date){
    return date('w', strtotime($date));
}

$day_of_week = getWeekday($today); // returns 4
switch ($day_of_week) {
    case 0:
        $starting_date = date('Y-m-d', strtotime($today . ' +8 day'));
        $days_add = 8;
        $days_add2 = 13;
        break;
    case 1:
        $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));
        $days_add = 7;
        $days_add2 = 12;
        break;
    case 2:
        $starting_date = date('Y-m-d', strtotime($today . ' +6 day'));
        $days_add = 6;
        $days_add2 = 11;
        break;
    case 3:
        $starting_date = date('Y-m-d', strtotime($today . ' +5 day'));
        $days_add = 5;
        $days_add2 = 10;
        break;
    case 4:
        $starting_date = date('Y-m-d', strtotime($today . ' +11 day'));
        $days_add = 11;
        $days_add2 = 16;
        break;
    case 5:
        $starting_date = date('Y-m-d', strtotime($today . ' +10 day'));
        $days_add = 10;
        $days_add2 = 15;
        break;
    case 6:
        $starting_date = date('Y-m-d', strtotime($today . ' +9 day'));
        $days_add = 9;
        $days_add2 = 14;
        break;
    case 7:
        $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));
        $days_add = 7;
        $days_add2 = 12;
        break;
}
$end_date = date('Y-m-d', strtotime($starting_date . '+5 day'));

if ($starting_date != "" && $end_date != "") {
    function week_number($date)
    {
        return ceil(date('j', strtotime($date)) / 7);
    }

    $week_no = week_number($starting_date);
    switch ($week_no) {
        case 1:
            $option_update = 1;
            break;
        case 2:
            $option_update = 2;
            break;
        case 3:
            $option_update = 1;
            break;
        case 4:
            $option_update = 4;
            break;
        case 5:
            $option_update = 5;
            break;
    }
    $temp_starting_date    = explode("-", $starting_date);
    $orginal_starting_date = $temp_starting_date[1] . "-" . $temp_starting_date[2] . "-" . $temp_starting_date[0];
    $temp_end_date         = explode("-", $end_date);
    $orginal_end_date      = $temp_end_date[1] . "-" . $temp_end_date[2] . "-" . $temp_end_date[0];
}
$page_request = "inventory";
?>
<?php
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_buyer.php";
include '/pagination/Pagination.php';
?>

<?php
$dates = date("jS F Y");

// Inicio Paginacion

$sel_pagina = "select gpb.prodcutid      , gpb.comment , gpb.id as gid , 
                      gpb.stock as stock , gpb.qty     
                 from grower_product_box_packing gpb
                inner join growers g      on gpb.growerid      = g.id                 
                inner join product p      on gpb.prodcutid     = p.id
                 left join subcategory s  on p.subcategoryid   = s.id  
                 left join colors c       on p.color_id        = c.id 
                 left join features ff    on gpb.feature       = ff.id
                 left join sizes sh       on gpb.sizeid        = sh.id 
                 left join boxes b        on gpb.box_id        = b.id
                 left join boxtype bt     on b.type            = bt.id
                 left join bunch_sizes bs on gpb.bunch_size_id = bs.id
                where g.active='active' 
                  and gpb.stock > 0    ";

$rs_pagina = mysqli_query($con, $sel_pagina);
$total_pagina = mysqli_num_rows($rs_pagina);
$num_record = $total_pagina;
$display = 50;
$XX = '<div class="notfound">No Item Found !</div>';

//   Fin Paginacion

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
$sql_order = "select connections 
                from buyer_orders  as  bu  
                left join buyer_shipping_methods bys on bu.shipping_method     = bys.shipping_method_id
                left join shipping_method s          on bys.shipping_method_id = s.id
               where  bu.id='" . $_POST['filter_order'] . "' ";

$row_sql_perkilo = mysqli_query($con, $sql_order);
$row_perkilo = mysqli_fetch_assoc($row_sql_perkilo);

$conections      = $row_perkilo['connections'];
$ids_connections = unserialize($conections);
$ids_connect = array();

foreach ($ids_connections as $key => $value) {
    $ids_connect[] = $value;
}
*/

$sql_order = "select s.connect_group as connections
                from buyer_shipping_methods  bm  
               inner join shipping_method s on bm.shipping_method_id = s.id
               where bm.buyer_id= '" . $_SESSION["buyer"] . "'  ";

$row_sql_perkilo = mysqli_query($con, $sql_order);
$row_perkilo = mysqli_fetch_assoc($row_sql_perkilo);

$conections      = "0".$row_perkilo['connections'];

$ids_connect = explode(",", $conections);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$cost_ship = 0;

$adi_cost_ship = 0;

//for ($i = 0; $i < count($ids_connect); $i++) {
   
    $sqlids = "select id ,
                      charges_per_kilo , 
                      charges_per_shipment          
                 from connections 
                where id = '" . $ids_connect[1] . "' ";   
    
    $row_sqlids = mysqli_query($con, $sqlids);
    $row_sql    = mysqli_fetch_assoc($row_sqlids);
    
    $cost     = $row_sql['charges_per_kilo'];
    $cost_un  = unserialize($cost);
    $cost_sum = 0;
    
    foreach ($cost_un as $key => $value) {
        $cost_sum = $cost_sum + $value;
    }
    
    $cost_ship =  $cost_sum;
 
    
    
    // Cargos Adicionales
    $adi_cost = $row_sql['charges_per_shipment'];
    $adi_cost_un = unserialize($adi_cost);
    $adi_cost_sum = 0;
    
    foreach ($adi_cost_un as $key => $value) {
        $adi_cost_sum = $adi_cost_sum + $value;
    }
    $adi_cost_ship =  $adi_cost_sum;    
//}


///////////////////////////////////////////////////////////////////




function weight_query($grower_id, $product_id, $size_id, $feature, $box_id){
    $sel_weight = "select weight 
                     from grower_product_box_weight 
                    where growerid  ='" . $grower_id . "' 
                      and prodcutid ='" . $product_id . "' 
                      and sizeid    ='" . $size_id . "' 
                      and feature   ='" . $feature . "' 
                      and box_id    ='" . $box_id . "' 
                    order by id desc limit 0,1";
    return $sel_weight;
}

function category(){
    global $con;
    $category_sql = "SELECT id,name from category order by  name";
    $result_category = mysqli_query($con, $category_sql);
    return $result_category;
}

function subcategory(){
    global $con;
    $subcategory_sql = "SELECT id,name from subcategory order by  name";
  //$subcategory_sql = "SELECT id,name from product order by  name";    
    $result_subcategory = mysqli_query($con, $subcategory_sql);
    return $result_subcategory;
}

function growers(){
    global $con;
    $getGrowers = "select id,growers_name from growers where active = 'active' order by growers_name";
    $GrowersRes = mysqli_query($con, $getGrowers);
    return $GrowersRes;
}

function sizes(){
    global $con;
    $getSize = "select * from sizes order by length(name),name";
    $SizeRes = mysqli_query($con, $getSize);
    return $SizeRes;
}

function special_feature(){
    global $con;
    $getFeatures = "select * from features order by name";
    $featuresRes = mysqli_query($con, $getFeatures);
    return $featuresRes;
}

function bunches(){
    global $con;
    $getbunch_sizes = "select * from bunch_sizes order by length(name),name;";
    $bunch_sRes = mysqli_query($con, $getbunch_sizes);
    return $bunch_sRes;
}


function query_main($user, $init, $display){ 

 $query = "select gpb.prodcutid         , gpb.comment           , gpb.id as gid   , gpb.stock as stock       , gpb.qty         , 
                  gpb.date_update       , gpb.hora              , gpb.sizeid      , gpb.feature              , gpb.type as bv  , 
                  gpb.boxname as bvname , gpb.box_type as bvtype, gpb.price       , gpb.growerid             , p.id            ,
                  p.name                , p.color_id            , p.image_path    , p.box_type as p_box_type , s.name as subs  , 
                  g.id as grower_id     , g.file_path5          , g.growers_name  , sh.name as sizename      , ff.name as featurename, 
                  b.name as boxname     ,
                  b.width  ,
                  b.length ,
                  b.height ,
                  bs.name as bname , bt.name as boxtype , b.type , c.name as colorname , gpb.box_id 
             from grower_product_box_packing gpb
            inner join growers g      on gpb.growerid      = g.id
            inner join product p      on gpb.prodcutid     = p.id
            inner join subcategory s  on p.subcategoryid   = s.id  
             left join colors c       on p.color_id        = c.id 
             left join features ff    on gpb.feature       = ff.id
             left join sizes sh       on gpb.sizeid        = sh.id 
             left join boxes b        on gpb.box_id        = b.id
             left join boxtype bt     on b.type            = bt.id
             left join bunch_sizes bs on gpb.bunch_size_id = bs.id
            where g.active='active' 
              and gpb.stock > 0 
            order by date_update desc,p.name,s.name,g.growers_name  LIMIT " . $init . ",$display";
    
    return $query;

}

if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = query_main($userSessionID, $_POST["startrow"], $display);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = query_main($userSessionID, 0, $display);
    $result2 = mysqli_query($con, $query2);
}





?>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
</head>
<link rel="stylesheet" type="text/css" href="/includes/assets/css/loading.css">
<!--comienza el  estilo-->
<style>

    .cssload-spin-box {
        position: absolute;
        margin: auto;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        width: 23px;
        height: 23px;
        border-radius: 100%;
        box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -o-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -ms-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -webkit-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        -moz-box-shadow: 23px 23px rgb(111, 171, 0), -23px 23px rgb(214, 34, 94), -23px -23px rgb(111, 171, 0), 23px -23px rgba(214, 34, 94, 0.97);
        animation: cssload-spin ease infinite 7s;
        -o-animation: cssload-spin ease infinite 7s;
        -ms-animation: cssload-spin ease infinite 7s;
        -webkit-animation: cssload-spin ease infinite 7s;
        -moz-animation: cssload-spin ease infinite 7s;
    }
 
 

				  
    @keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }
						  
 

				 
    @-o-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

    @-ms-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

    @-webkit-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

    @-moz-keyframes cssload-spin {
        0%,
        100% {
            box-shadow: 23px 23px rgb(214, 34, 94), -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.98), 23px -23px rgb(223, 223, 223);
        }
        25% {
            box-shadow: -23px 23px rgb(223, 223, 223), -23px -23px rgba(214, 34, 94, 0.97), 23px -23px rgb(223, 223, 223), 23px 23px rgb(214, 34, 94);
        }
        50% {
            box-shadow: -23px -23px rgba(111, 171, 0, 0.98), 23px -23px rgb(223, 223, 223), 23px 23px rgb(111, 171, 0), -23px 23px rgb(223, 223, 223);
        }
        75% {
            box-shadow: 23px -23px #dfdfdf, 23px 23px #4f4d49, -23px 23px #dfdfdf, -23px -23px #4f4d49;
        }
    }

</style>
<!--termina  el  estilo  -->						
<style>
    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__rendered, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px !important;
        line-height: 36px !important;
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    td {
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    .modal_k {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('../images/ajax-loader.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

    body.loading .modal_k {
        display: block;
    }

    .pagination > li {
        float: left;
    }

    #middle div.panel-heading {
        height: auto;
    }

    .text-center {
        width: 30px;
    }

    .price_field {
        overflow: inherit !important;
        width: 405px;
    }

    .request_product_modal_hide {
        height: 400px;
        overflow-y: auto;
    }

		</style>
<!--stilo  del  loading-->
<style type='text/css'>
    #loading {
        width: 100%;
        height: 100%;
        background-color: white;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 9999;
        opacity: 0.5;
        filter: alpha(opacity=70);
    }
</style>
<!--/stilo  del  loading-->
<style>				  
    .noRow {
        display: none;
    }
</style>	


<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Price and Availability</h1>
        <ol class="breadcrumb">
            <li><a href="#">Inventory</a></li>
            <li class="active">Buy Flowers</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">

                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">
                            <div id="panel-2" class="panel panel-default">
                                <div class="panel-heading">
                                      <!--a href="<?php echo SITE_URL; ?>buyer/customer_list_addx.php" class="btn btn-success btn-xs relative"> Shoping Cart </a-->                
                                    <a href="<?php echo SITE_URL; ?>buyer/my-offers-shopping-cart.php" ><i class="fa fa-shopping-cart"></i> Shopping Cart </a>     
                                    <br>
                                    <span class="title elipsis">
                                        <br>
                                        <strong>Inventory </strong> <!-- panel title -->
                                    </span>
                                                    <!--Send  Request-->
                                                    
                                                        <div class="btn-group">
                                                            <button type="button" onclick="modelClick(<?php echo $products['prodcutid'] ?>,<?php echo $i ?>,<?php echo $products['bname'] ?>,<?php echo $products['sizeid'] ?>,<?php echo $products['feature'] ?>,<?php echo $products['bunchsizeid'] ?>,<?php echo $chargesPerKilo; ?>,<?php echo $products["sizename"]; ?>,<?php echo $products['cat_id']; ?>,<?php echo $i; ?>)"
                                                                    id="price_modal"
                                                                    class="btn btn-default btn-xs" style="background:#34495E;color:white" data-toggle="modal" data-target=".price_modal"><i class="fa fa-send"></i></i>Request Product
                                                            </button>

                                                        </div>
                                                                                                       
                                                        <!--div class="btn-group">
                                                            <button type="button" onclick="modelClick(<?php echo $products['prodcutid'] ?>,<?php echo $i ?>,<?php echo $products['bname'] ?>,<?php echo $products['sizeid'] ?>,<?php echo $products['feature'] ?>,<?php echo $products['bunchsizeid'] ?>,<?php echo $chargesPerKilo; ?>,<?php echo $products["sizename"]; ?>,<?php echo $products['cat_id']; ?>,<?php echo $i; ?>)"
                                                                    id="modify_modal_req_<?php echo $i; ?>"
                                                                    class="btn btn-default btn-xs" style="background:#34495E;color:white" data-toggle="modal" data-target=".modify_modal_req_<?php echo $i; ?>"><i class="fa fa-send"></i>Request Box Mix
                                                            </button>
                                                        </div-->
                                                    
                                                        <div class="btn-group">
                                                            <button type="button" 
                                                                    id="color_modal"
                                                                    class="btn btn-default btn-xs" style="background:#34495E;color:white" data-toggle="modal" data-target=".color_modal"><i class="fa fa-send"></i></i>Request Product Color
                                                            </button>
                                                        </div>                                                    
                                                    
                                                    
                                                        <?php
                                                        $getbunches = mysqli_query($con, "SELECT gs.sizes , bs.name as bunchname  
                                                                                            FROM grower_product_bunch_sizes as gs 
                                                                                            left join bunch_sizes bs on gs.bunch_sizes=bs.id 
                                                                                           WHERE gs.product_id = '" . $products['prodcutid'] . "' 
                                                                                             AND gs.sizes      = '" . $products['sizeid'] . "'");   
                                                        
                                                        
                                                        $rowbunches_marquet = mysqli_fetch_assoc($getbunches);
                                                        
                                                        $ir = 1;
                                                        
                                                        ?>
                                                    
                                                        <!--Price Modal Start--------------------------------------------------------->
                                                        
                                                        <div class="modal fade price_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            
                                                            <div class="modal-dialog modal-lg">
                                                                
                                                                <div class="modal-content">                                                                                                                                        
                                                                    
                                                                    <div class="modal-header">
                                                                                              <!--Product-->
                                                                                                            
                                                                                              <select class="listmenu"  name="productvar"  id="productvar" style="width: 100%; diplay: none;">
                                                                                                   <option value=""> -- Select Product -- </option>
                                                                                              </select>                                                                                                            
                                                                    </div>
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    <div class="modal-body final_price_section"
                                                                         style="z-index: 99999999; min-height: 295px; display: none;">
                                                                        <div class="col-md-12">
                                                                            <div class="margin-bottom-20">
                                                                                <label class="field">
                                                                                    <input id="price_section_<?php echo $ir ?>" value="0.01" class="form-control select_price_cls" style="width: 840px;" type="text">
                                                                                </label>
                                                                            </div>
                                                                            <div class="slider-wrapper black-slider"></div>
                                                                            <div class="table-responsive" style="margin-top:30px;">
                                                                                <table class="table table-hover" id="price_model_<?php echo $ir; ?>">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Grower Name</th>
                                                                                        <th>type</th>
                                                                                        <th>Bunches</th>
                                                                                        <th>Bunch/Stem</th>
                                                                                        <th>Grower Price</th>
                                                                                        <th>Tax</th>
                                                                                        <th>Shipping Cost</th>
                                                                                        <th>Handling</th>
                                                                                        <th>Final Price</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>Standard Rose Amarreto 40cm</td>
                                                                                        <td style="text-align: center;">Stem</td>
                                                                                        <td align="center"></td>
                                                                                        <td align="center"></td>
                                                                                        <td align="center"></td>
                                                                                        <td align="center"></td>
                                                                                        <td align="center"></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" onclick="backPricePopup()">Back</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    <div class="modal-body request_product_modal_hide">

                                                                        </label>

                                                                        <!--<br>-->
                                                                        <!--</div>-->
                                                                        <div class="row margin-bottom-10">
                                                                            <div class="col-md-12">
                                                                                <h4 style="clear: both;margin-top: 20px;margin-left: 14px;margin-bottom: 0px;">Select Product and Price</h4>
                                                                                <div class="product_price_add2"></div>
                                                                                <span id="add_product_section1" style="display: inherit!important;">
                                                                                    
                                                                                    <!--Select  Quantity (1) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Select Quantity </label>
                                                                                        <br>
                                                                                        <div class="stepper-wrap" style="width: 100%; diplay: none;">
                                                                                            <input type="text" id="box_quantity_<?php echo $ir ?>" name="box_quantity_<?php echo $ir ?>"
                                                                                                   class="form-control stepper" max="1000" min="1" value="1" style="margin: 0px;">
                                                                                        </div>
                                                                                        <a id="addProduct" onclick="myfunction_addproduct('<?php echo $products["prodcutid"] . '_' . $products["sizename"]; ?>')"
                                                                                           class="btn btn-3d btn-xs btn-reveal btn-red margin-top-10"
                                                                                           style="background:#8a2b83!important;display: none;"><i class="fa fa-plus"></i><span>Add to list</span></a>
                                                                                    </div>
                                                                                    
                                                                                    <!--Select  Box Type (2) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Select Unit Type </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="filter_category" id="box_type_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="">Select unit</option>                                                                                            
                                                                                            <?php
                                                                                            $sql_unit = "select * from  units";
                                                                                            $result_units = mysqli_query($con, $sql_unit);
                                                                                            while ($row_category = mysqli_fetch_assoc($result_units)) { ?>
                                                                                                <option value="<?php echo $row_category['id']; ?>"><?= $row_category['descrip']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                     <!--Select  Order (3) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Select Order </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                            <select style="width: 100%; diplay: none;" name="filter_order" id="box_order_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1"    >
                                                                                            <option value="">Select order</option>
                                                                                                <?php
                                                                                                $category_sql = "select  id,qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' and  del_date >= '" . date("Y-m-d") . "' and is_pending=0";
                                                                                                $result_category = mysqli_query($con, $category_sql);
                                                                                                while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                                                    <option value="<?php echo $row_category['id']; ?>"><?= $row_category['qucik_desc']; ?></option>
                                                                                                <?php }
                                                                                                ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                             <input type="hidden" id="ship" name="ship" value="">
                                                                                             <div id="erMsg" style="display:none;color:red;">Please select Order.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                     
                                                                                    <!--Suggest Price (4) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Suggest price (optional)</label>
                                                                                        </br>
                                                                                        <label class="field">
                                                                                            <span class="input-group-addon" style="width: 35px;border: 1px solid #ccc;position: absolute;height: 39px;"><i class="fa fa-usd" style="margin-top: 6px;"></i></span>
                                                                                            <div class="fancy-file-upload fancy-file-primary price_field">

                                                                                                <input type="hidden" name="buyer_<?php echo $ir; ?>"     id="buyer_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="grower_<?php echo $ir; ?>"    id="grower_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="tax_<?php echo $ir; ?>"       id="tax_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="shipping_<?php echo $ir; ?>"  id="shipping_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="handling_<?php echo $ir; ?>"  id="handling_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="catid_<?php echo $ir; ?>"     id="catid_<?php echo $ir; ?>" value="<?= $products['cat_id']; ?>" >
                                                                                                <input name="buyer_price_<?php echo $ir; ?>"             id="buyer_price_<?php echo $ir; ?>"
                                                                                                       value="" class="form-control required" placeholder="suggest a price for your request" style="text-indent: 4px;" type="text">
                                                                                               <span class="button final_price_click"
                                                                                                     onclick="modelClick_New(0,<?php echo $ir ?>,<?php echo $products['bname'] ?>,<?php echo $products['sizeid'] ?>,<?php echo $products['feature'] ?>,<?php echo $products['bunchsizeid'] ?>,<?php echo $chargesPerKilo; ?>,<?php echo $products["sizename"]; ?>,<?php echo $products['cat_id']; ?>,<?php echo $ir; ?>)">Grower Price
                                                                                               </span>                                                                                           
                                                                                            </div>
                                                                                        </label>
                                                                                    </div>
                                                                                    

                                                                        
                                                                            </div>
                                                                        </div>
                                                                                                                                                                                                                                                                                                
                                                                        <!-- Comment (6) -->
                                                                        <div class="col-md-12">
                                                                            <hr>
                                                                            <form class="validate" action="" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                                                                <fieldset>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-12 col-sm-12">
                                                                                                <h4>Let your growers know if you have any special requirements</h4>
                                                                                                <label>Comment</label>
                                                                                                <textarea id="comment_<?php echo $ir; ?>" name="contact[experience]" rows="4" class="form-control required"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </fieldset>
                                                                                <input name="is_ajax" value="true" type="hidden"></form>
                                                                               
                                                                                    <!--Select Grower (4.5) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <h4>Additional Options</h4>                                                                                        
                                                                                        <label>Grower </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="filter_grower" id="grow_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="0">Select Grower</option>
                                                                                            <?php
                                                                                            $sql_grow = "select id,growers_name from growers where active = 'active' order by growers_name";
                                                                                            $result_grow = mysqli_query($con, $sql_grow);
                                                                                            while ($row_grower = mysqli_fetch_assoc($result_grow)) { ?>
                                                                                                <option value="<?php echo $row_grower['id']; ?>"><?= $row_grower['growers_name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>

					    			                   <!-- Select Order Type (7) -->    
                                            			                   <div class="col-md-6 margin-top-10">	
                                                                                       <br>
                                                                                       <br>
                                                			                   <label >Select market type</label>												
                                                			                   <div class="fancy-form fancy-form-select">
                                                    			                   <select style="width: 100%; diplay: none;" name="filter_category" id="type_req_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                        			                   <option value="1">Open Market</option>
                                                        			                   <option value="0">Standing Order</option>
                                                        			                   <option value="3">Cancel</option>                                                                                                   
                                                     			                   </select>
                                                     			                   <i class="fancy-arrow"></i>
                                                			                   </div>																						
                                            			                   </div>
                                                                                   
                                                                                    <!--Select Customer Tag (8) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Customer Tag </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="filter_customer" id="tag_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="0">Select Customer</option>
                                                                                            <?php
                                                                                            $sql_cli = "select id,name from sub_client order by name";
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>                                                                                   
                                                                                                                                                                
                                                                     </div>                                                                                                                                                
                                                                        
                                                                        
                                                                    </div>
                                                                    
                                                                    <hr>
                                                                    <div class="modal-footer request_product_modal_hide_footer">
                                                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                                        <button style="background:#8a2b83!important;" onclick="requestProduct('<?php echo $ir ?>')" class="btn btn-primary" type="button">Send Request.-</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Price Modal End------------------------------------------------------------>

                                                        <!--Color Modal Start--------------------------------------------------------->
                                                        
                                                        <div class="modal fade color_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            
                                                            <div class="modal-dialog modal-lg">
                                                                
                                                                <div class="modal-content">                                                                                                                                        
                                                                    
                                                                    <div class="modal-header">
                                                                                              <!--Product-->
                                                                                                            
                                                                                              <select class="listmenu"  name="productcol"  id="productcol" style="width: 100%; diplay: none;">
                                                                                                   <option value=""> -- Select Product Color -- </option>
                                                                                              </select>                                                                                                            
                                                                    </div>
                                                                    
                                                                                                                                                                                                                                                                                
                                                                    
                                                                    <div class="modal-body request_product_modal_hide">

                                                                        </label>

                                                                        <!--<br>-->
                                                                        <!--</div>-->
                                                                        <div class="row margin-bottom-10">
                                                                            <div class="col-md-12">
                                                                                <h4 style="clear: both;margin-top: 20px;margin-left: 14px;margin-bottom: 0px;">Select Product Color</h4>
                                                                                <div class="product_price_add2"></div>
                                                                                <span id="add_product_section1" style="display: inherit!important;">
                                                                                    
                                                                                    <!--Select  Quantity (1) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Select Quantity </label>
                                                                                        <br>
                                                                                        <div class="stepper-wrap" style="width: 100%; diplay: none;">
                                                                                            <input type="text" id="box_quantityco_<?php echo $ir ?>" name="box_quantityco_<?php echo $ir ?>"
                                                                                                   class="form-control stepper" max="1000" min="1" value="1" style="margin: 0px;">
                                                                                        </div>
                                                                                        <a id="addProduct" onclick="myfunction_addproduct('<?php echo $products["prodcutid"] . '_' . $products["sizename"]; ?>')"
                                                                                           class="btn btn-3d btn-xs btn-reveal btn-red margin-top-10"
                                                                                           style="background:#8a2b83!important;display: none;"><i class="fa fa-plus"></i><span>Add to list</span></a>
                                                                                    </div>
                                                                                    
                                                                                    <!--Select  Box Type (2) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Select Unit Type </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="color_category" id="box_typeco_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="">Select unit</option>                                                                                            
                                                                                            <?php
                                                                                            $sql_unit = "select * from  units";
                                                                                            $result_units = mysqli_query($con, $sql_unit);
                                                                                            while ($row_category = mysqli_fetch_assoc($result_units)) { ?>
                                                                                                <option value="<?php echo $row_category['id']; ?>"><?= $row_category['descrip']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                     <!--Select  Order (3) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Select Order </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                            <select style="width: 100%; diplay: none;" name="order_order" id="box_orderco_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1"    >
                                                                                            <option value="">Select order</option>
                                                                                                <?php
                                                                                                $category_sql = "select  id,qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' and  del_date >= '" . date("Y-m-d") . "' and is_pending=0";
                                                                                                $result_category = mysqli_query($con, $category_sql);
                                                                                                while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                                                    <option value="<?php echo $row_category['id']; ?>"><?= $row_category['qucik_desc']; ?></option>
                                                                                                <?php }
                                                                                                ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                             <input type="hidden" id="shipco" name="shipco" value="">
                                                                                             <div id="erMsg" style="display:none;color:red;">Please select Order.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                     
                                                                                    <!--Suggest Price (4) -->
                                                                                    <!--div class="col-md-6 margin-top-10">
                                                                                        <label>Suggest price (optional)</label>
                                                                                        </br>
                                                                                        <label class="field">
                                                                                            <span class="input-group-addon" style="width: 35px;border: 1px solid #ccc;position: absolute;height: 39px;"><i class="fa fa-usd" style="margin-top: 6px;"></i></span>
                                                                                            <div class="fancy-file-upload fancy-file-primary price_field">

                                                                                                <input type="hidden" name="buyer_<?php echo $ir; ?>"     id="buyer_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="grower_<?php echo $ir; ?>"    id="grower_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="tax_<?php echo $ir; ?>"       id="tax_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="shipping_<?php echo $ir; ?>"  id="shipping_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="handling_<?php echo $ir; ?>"  id="handling_<?php echo $ir; ?>">
                                                                                                <input type="hidden" name="catid_<?php echo $ir; ?>"     id="catid_<?php echo $ir; ?>" value="<?= $products['cat_id']; ?>" >
                                                                                                <input name="buyer_price_<?php echo $ir; ?>"             id="buyer_price_<?php echo $ir; ?>"
                                                                                                       value="" class="form-control required" placeholder="suggest a price for your request" style="text-indent: 4px;" type="text">
                                                                                               <span class="button final_price_click"
                                                                                                     onclick="modelClick_New(0,<?php echo $ir ?>,<?php echo $products['bname'] ?>,<?php echo $products['sizeid'] ?>,<?php echo $products['feature'] ?>,<?php echo $products['bunchsizeid'] ?>,<?php echo $chargesPerKilo; ?>,<?php echo $products["sizename"]; ?>,<?php echo $products['cat_id']; ?>,<?php echo $ir; ?>)">Grower Price
                                                                                               </span>                                                                                           
                                                                                            </div>
                                                                                        </label>
                                                                                    </div-->
                                                                                    

                                                                        
                                                                            </div>
                                                                        </div>
                                                                                                                                                                                                                                                                                                
                                                                        <!-- Comment (6) -->
                                                                        <div class="col-md-12">
                                                                            <hr>
                                                                            <form class="validate" action="" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                                                                <fieldset>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-12 col-sm-12">
                                                                                                <h4>Let your growers know if you have any special requirements</h4>
                                                                                                <label>Comment</label>
                                                                                                <textarea id="commentco_<?php echo $ir; ?>" name="contact[experience]" rows="4" class="form-control required"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </fieldset>
                                                                                <input name="is_ajax" value="true" type="hidden"></form>
                                                                               
                                                                                    <!--Select Grower (4.5) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <h4>Additional Options</h4>                                                                                        
                                                                                        <label>Grower </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="color_grower" id="growco_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="0">Select Grower</option>
                                                                                            <?php
                                                                                            $sql_grow = "select id,growers_name from growers where active = 'active' order by growers_name";
                                                                                            $result_grow = mysqli_query($con, $sql_grow);
                                                                                            while ($row_grower = mysqli_fetch_assoc($result_grow)) { ?>
                                                                                                <option value="<?php echo $row_grower['id']; ?>"><?= $row_grower['growers_name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>

					    			                   <!-- Select Order Type (7) -->    
                                            			                   <div class="col-md-6 margin-top-10">	
                                                                                       <br>
                                                                                       <br>
                                                			                   <label >Select market type</label>												
                                                			                   <div class="fancy-form fancy-form-select">
                                                    			                   <select style="width: 100%; diplay: none;" name="color_category" id="type_reqco_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                        			                   <option value="1">Open Market</option>
                                                        			                   <option value="0">Standing Order</option>
                                                        			                   <option value="3">Cancel</option>                                                                                                   
                                                     			                   </select>
                                                     			                   <i class="fancy-arrow"></i>
                                                			                   </div>																						
                                            			                   </div>
                                                                                   
                                                                                    <!--Select Customer Tag (8) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        <label>Customer Tag </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="color_customer" id="tagco_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="0">Select Customer</option>
                                                                                            <?php
                                                                                            $sql_cli = "select id,name from sub_client order by name";
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>                                                                                   
                                                                                                                                                                
                                                                     </div>                                                                                                                                                
                                                                        
                                                                        
                                                                    </div>
                                                                    
                                                                    <hr>
                                                                    <div class="modal-footer request_product_modal_hide_footer">
                                                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                                        <button style="background:#008f39!important;" onclick="requestColor('<?php echo $ir ?>')" class="btn btn-primary" type="button">Send Request Color</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Color Modal End ------------------------------------------------------------>




                                                        
                                                            <!-- Modify Modal Ofertas New>-->
                                                            <div class="modal fade modify_modal_req_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-medium" style="width: 1000px">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <?php
                                                                                $rowbunches = get_bunchs($products['product'], $products['sizeid'], $userSessionID);
                                                                                $ix=1;
                                                                                
                                                                                $boxtypex="1";
                                                                                $productx="222";
                                                                                $sizeidx="3";                                                                                
                                                                                
                                                                        //  Total de Ofertas
                                                                        
                                                                        $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,
                                                                                             cr.flag , (gr.bunchqty*gr.steams) as totstems
                                                                                        from grower_offer_reply gr 
                                                                                        left join growers g  on gr.grower_id = g.id 
                                                                                        left join country cr on g.country_id = cr.id
                                                                                       where gr.request_id='" . $products["cartid"] . "' 
                                                                                         and gr.buyer_id='" . $products['buyer'] . "' 
                                                                                       order by gr.grower_id , gr.offer_id_index";

                                                                                $rs_growers1 = mysqli_query($con, $sel_check);                                                                        
                                                                                
                                                                                $bq = 0;
                                                                                $bunst = 0;
                                
                                                                                while ($growers_1 = mysqli_fetch_assoc($rs_growers1)) {
                                                                                      //  if ($growers_1['status'] == 1) {      Confirmados
                                                                                            $bq    += $growers_1['bunchqty'] ;
                                                                                            $bunst += $growers_1['totstems'] ;
                                                                                            $type_unit = $growers_1['unit'] ;
                                                                                      //  }
                                                                                }                                                                                
                                                                        
                                                                                
                                                                                if ($type_unit == 'ST') {                                                                    
                                                                                    $cant_required = ($bunst)." " .$box_type_name;                                                               
                                                                                    $cumpli        =(($bunst)/$products["qty"])*100;                                
                                                                                }else {
                                                                                    $cant_required = ($bq)." " .$box_type_name;                                                               
                                                                                    $cumpli        =(($bq)/$products["qty"])*100;
                                                                                }                                
                                
                                                                        //  Fin Ofertas                                                                                
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        ?>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">
                                                                                <?php echo $products["qty"] ; ?>
                                                                                <?php echo $rowbunches['bunchname'] . " Varieties " ; ?>
                                                                            </h4>
                                                                        </div>
                                                                        
                                                                        
                                                                        
                                                                        <!-- body modal -->
                                                                        <div class="modal-body"><?php //echo '<pre>'; print_r($products);     ?>
                                                                            <div class="row">
                                                                                <ul class="nav nav-tabs nav-clean">
                                                                                    <li class="dropdown active">
                                                                                        <a data-toggle="dropdown" id="offer_dropdown<?php echo $i; ?>" class="dropdown-toggle" href="#" aria-expanded="false">Box Mix<span class="caret"></span></a>
                                                                                        <ul class="dropdown-menu" id="offer_ul_s<?php echo $i; ?>">

                                                                                            <li>

                                                                                            </li>

                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class=""><a data-toggle="tab" href="#tab_b_<?php echo $i; ?>" aria-expanded="false">Order</a></li>

                                                                                </ul>
                                                                                <!-- tabs content -->
                                                                                <div class="col-md-12 col-sm-12 nopadding">
                                                                                    <div class="tab-content tab-stacked">
                                                                                        <div id="tab_a_<?php echo $i; ?>" class="tab-pane active">
                                                                                            
                                                                                            <!-- classic select22 -->
                                                                                            <?php                                                                                            
                                                                                                $order_sql = "select  id,qucik_desc  
                                                                                                                   from  buyer_orders 
                                                                                                                  where  buyer_id='" . $userSessionID . "' 
                                                                                                                    and  del_date >= '" . date("Y-m-d") . "' 
                                                                                                                    and is_pending=0";
                                                                                                
                                                                                                $result_order = mysqli_query($con, $order_sql);
                                                                                            
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="order_buyer_box" id="order_buyer_box_<?php echo $i; ?>"   style="width:300px;">
                                                                                                <option value="">Select Order</option>
                                                                                                <?php
                                                                                                                                                                                               
                                                                                                while ($buyer_order_sel = mysqli_fetch_array($result_order)) {   ?>
                                                                                                        <option value="<?php echo $buyer_order_sel['id']; ?>"><?= $buyer_order_sel['qucik_desc']; ?></option>                                                                                                        
                                                                                                    <?php 
                                                                                                } ?>
                                                                                            </select> 
                                                                                            
                                                                                            <!-- Grower -->
                                                                                            <?php                                                                                            
                                                                                                $grower_sql = "select  id,growers_name  
                                                                                                                   from  growers 
                                                                                                                  where active = 'active'
                                                                                                                  order by growers_name ";
                                                                                                
                                                                                                $result_grower = mysqli_query($con, $grower_sql);
                                                                                            
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="request_grower" id="request_grower_<?php echo $i; ?>"   style="width:300px;">
                                                                                                <option value="">Select Grower</option>
                                                                                                <?php
                                                                                                                                                                                               
                                                                                                while ($grower_sel = mysqli_fetch_array($result_grower)) {   ?>
                                                                                                        <option value="<?php echo $grower_sel['id']; ?>"><?= $grower_sel['growers_name']; ?></option>                                                                                                        
                                                                                                    <?php 
                                                                                                } ?>
                                                                                            </select>                                                                                             

                                                                                            <!-- classic select21 -->
                                                                                            <?php
                                                                                            $sql_boxes = "SELECT  boxes FROM  growers WHERE  id ='341'";
                                                                                            $rs_boxes = mysqli_query($con, $sql_boxes);
                                                                                            $boxes = mysqli_fetch_array($rs_boxes);
                                                                                            $a = substr($boxes['boxes'], 0, -1);
                                                                                            $a = substr($a, 1);


                                                                                            $sel_boxes = " SELECT b.id,b.name , b.width , b.length , b.height , bo.name AS name_box , b.type,un.code ,
                                                                                                                  unt.name as name_unit
                                                                                                             FROM boxes b 
                                                                                                             LEFT JOIN  boxtype bo     ON  b.type=bo.id 
                                                                                                             LEFT JOIN  units un       ON  b.type=un.id 
                                                                                                             LEFT JOIN  units_type unt ON  un.type=unt.id 
                                                                                                            WHERE  b.id IN(" . $a . ")   ";
                                                                                                                                                                                       
                                                                                            $rs_boxes = mysqli_query($con, $sel_boxes);


                                                                                            
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="request_qty_box" id="request_qty_box_<?php echo $i; ?>"
                                                                                                     style="width:300px;">
                                                                                                <option value="">Select Box Quantity</option>
                                                                                                <?php
                                                                                                
                                                                                                
                                                                                                while ($box_sel = mysqli_fetch_array($rs_boxes)) {

                                                                                                        ?>
                                                                                                        <option value="<?php echo $box_sel['width'] * $box_sel['length'] * $box_sel['height']; ?>">
                                                                                                            <?php echo $box_sel['name_box'] . " " . $box_sel['width'] . "x" . $box_sel['length'] . "x" . $box_sel['height']; ?>
                                                                                                        </option>
                                                                                                    <?php 
                                                                                                } ?>
                                                                                            </select> 
                                                                                            
                                                             
                                                                                            <input type="hidden" class="cls_hidden_selected_qty" name="cls_hidden_selected_qty" value=""/>
                                                                                            <form name="sendoffer" id="sendoffer<?= $products["cartid"] ?>" method="post">
                                                                                                <input type="hidden" name="mainprice" id="mainprice" value=" <?php
                                                                                                if ($products["price"] != "0.00") {
                                                                                                    echo $products["price"];
                                                                                                }

                                                                                                ?> "/>
                                                                                                <!--Inside Table-->

                                                                                                <input type="hidden" name="offer" id="offer" value="<?= $products["cartid"] ?>">
                                                                                                <input type="hidden" name="buyer" id="buyer" value="<?= $products["buyer"] ?>">
                                                                                                <input type="hidden" name="id_client" id="id_client" value="<?= $products["id_client"] ?>">                                                                                                
                                                                                                
                                                                                                <input type="hidden" name="product" id="product" value="<?= $products["name"] ?> <?= $products["featurename"] ?>">                                                                                                                                                                                                
                                                                                                <input type="hidden" name="product_subcategory" id="product_subcategory" value="<?= $products["subs"] ?>">
                                                                                                <input type="hidden" name="product_ship" id="product_ship" value="1">
                                                                                                <input type="hidden" name="shipp" id="shipp" value="<?= $products["shpping_method"] ?>">
                                                                                                <input type="hidden" name="product_su" id="product_su" value="1">
                                                                                                <input type="hidden" name="volume" id="volume" value="1">
                                                                                                <input type="hidden" name="code_order" id="code_order" value="<?= substr($products["cod_order"], 0, 4) ?>">
                                                                                                <input type="hidden" name="size_name" id="size_name" value="<?= $products["sizename"] ?>"> 
                                                                                                <input type="hidden" name="stems" id="stems" value="<?= $rowbunches['bunchname'] ?>">   
                                                                                                <input type="hidden" name="boxcant" id="boxcant" value="1"> 
                                                                                                <input type="hidden" name="boxtype" id="boxtype" value="<?= $box_type["name"] ?>">                                                                                                 
                                                                                                <input type="hidden" name="boxtypen" id="boxtypen" value="HB">

                                                                                                
                                                                                                <?php

                                                                                                $products["boxtype"];
                                                                                                $temp = explode("-", $products["boxtype"]);
                                                                                                $box_type_id = $temp[0];
                                                                                                $sel_box_ids = "SELECT bt.id AS boxtypeid,
                                                                                                                       bt.name AS boxtype,bo.width,
                                                                                                                       bo.length,bo.height,
                                                                                                                       bo.name AS box_value,
                                                                                                                       go.grower_id,
                                                                                                                       go.boxes 
                                                                                                                  FROM grower_product_box go 
                                                                                                                  LEFT JOIN boxes bo ON bo.id=go.boxes
                                                                                                                  LEFT JOIN boxtype bt ON bo.type=bt.id
                                                                                                                 WHERE go.grower_id=" . $userSessionID . " 
                                                                                                                   AND bt.id       ='" . $box_type_id . "' 
                                                                                                                 GROUP BY bo.name";         

                                                                                                $rs_box_ids = mysqli_query($con, $sel_box_ids);
                                                                                                $totalboxtype = mysqli_num_rows($rs_box_ids);
                                                                                                $bi = 1;
                                                                                                while ($box_ids = mysqli_fetch_array($rs_box_ids)) {
                                                                                                    $sel_weight = "SELECT weight 
                                                                                                                     FROM grower_product_box_weight 
                                                                                                                    WHERE growerid=" . $userSessionID . " 
                                                                                                                      AND box_id  ='" . $box_ids["boxes"] . "'";   
                                                                                                    $rs_weight = mysqli_query($con, $sel_weight);
                                                                                                    $weight = mysqli_fetch_array($rs_weight);
                                                                                                    $optionbb[$bi] .= $box_ids["boxtype"] . "-" . $box_ids["box_value"] . "-" . ($box_ids["length"] * $box_ids["height"] * $box_ids["width"]) . "-" . $weight["weight"];
                                                                                                    $bi++;
                                                                                                }
                                                                                                                                                                                                
                                                                                                
                                                                                                ?>
                                                                                                <input type="hidden" name="boxtype-<?= $products["cartid"] ?>" id="boxtype-<?= $products["cartid"] ?>" value="<?php echo $optionbb[1]; ?>">
                                                                                                <div class="table-responsive" style="margin-top:20px;">

                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_<?php echo $i; ?>">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Product</th>
                                                                                                            <th>Bunch Quantity</th>
                                                                                                            <th>Comment</th>
                                                                                                            <th>Group</th>                                                                                                            
                                                                                                            <th>  </th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <?php $a = 0; ?>
                                                                                                        <tr id="product_tr_0">
                                                                                                            <?php $a = $a + 1; ?>
                                                                                                            <!--Product-->
                                                                                                            <td>
                                                                                                                <?php
                                                                                                                $sql_box_ids3 = "SELECT p.name as variety      , gr.id AS id      , gr.box_id   , b.type            , b.name              , 
                                                                                                                                        gs.sizes               , gs.bunch_value   , gs.is_bunch , gs.is_bunch_value , s.name AS size_name ,
                                                                                                                                        fe.name as featurename , 
                                                                                                                                        bt.name as caja        , gr.bunch_size_id , bs.name as bunchesv
                                                                                                                                   FROM grower_product_bunch_sizes gs
                                                                                                                                   LEFT JOIN product AS p   ON gs.product_id=p.id
                                                                                                                                   LEFT JOIN grower_product_box_packing  gr ON( gr.sizeid        = gs.sizes       AND 
                                                                                                                                                                                gr.bunch_size_id = gs.bunch_sizes AND 
                                                                                                                                                                                gr.growerid      = gs.grower_id   AND 
                                                                                                                                                                                gr.prodcutid     = gs.product_id)
                                                                                                                                   LEFT JOIN sizes AS s     ON gs.sizes=s.id
                                                                                                                                   LEFT JOIN boxes AS b     ON b.id=gr.box_id
                                                                                                                                   LEFT JOIN boxtype AS bt  ON b.type=bt.id
                                                                                                                                   LEFT JOIN features AS fe ON fe.id=gr.feature
                                                                                                                                   LEFT JOIN bunch_sizes AS bs  ON gr.bunch_size_id=bs.id
                                                                                                                                  WHERE  gs.grower_id='341' 
                                                                                                                                    AND bt.name in ('HB','QB','EB')
                                                                                                                                  ORDER BY p.name, s.name  ";                                                                                                                
                                                                                                                
                                                                                                                $sel_box_ids3 = mysqli_query($con, $sql_box_ids3);
                                                                                                                
                                                                                                                ?>
                                                                                                                <select class="form-controll disabled-css offer-select" name="productsize[]"  id="productsize-<?= $products["cartid"] ?>-0">
                                                                                                                    <option value="">Select Product</option>
                                                                                                                    <?php

                                                                                                                    while ($box_ids3 = mysqli_fetch_array($sel_box_ids3)) {
                                                                                                                        $bunch_value_s = "";
                                                                                                                         
                                                                                                                        
                                                                                                                        if ($box_ids3['is_bunch'] == "0") {
                                                                                                                            $bunch_value_s = $box_ids3['bunchesv'];
                                                                                                                        } else {
                                                                                                                            $bunch_value_s = $box_ids3['bunchesv'];
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        <!--aqui  estoy  enviando  el  id-->
                                                                                                                        <option value="<?= $box_ids3["id"] ?>" size_id="<?php echo $box_ids3["sizes"]; ?>" prod_name="<?php echo $box_ids3["variety"]; ?>" s_name="<?php echo $box_ids3["size_name"]; ?>" b_type="<?php echo $box_ids3["caja"]; ?>" cod_id="<?php echo $box_ids3["id"]; ?>"> 
                                                                                                                                 <?= $box_ids3["variety"] ?> <?= $box_ids3["caja"] ?> <?= $box_ids3["featurename"] ?> <?= $box_ids3["size_name"] ?> cm <?php echo $bunch_value_s . " st/bu"; ?></option>
                                                                                                                        <?php
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                            </td>

                                                                                                            <!-- Quantity-->
                                                                                                            <td>
                                                                                                                <select class="form-controll disabled-css offer-select" 
                                                                                                                        onchange="changeBunchQty(0,<?php echo $products["cartid"]; ?>,<?php echo $i; ?>)" name="bunchqty[]"
                                                                                                                        id="bunchqty-<?= $products["cartid"] ?>-0">
                                                                                                                    <option value="">Select Bunch QTY</option>
                                                                                                                    <?php
                                                                                                                    for ($ci = 1; $ci <= 500; $ci++) {
                                                                                                                        ?>
                                                                                                                        <option value="<?= $ci ?>"><?= $ci ?></option>
                                                                                                                    <?php } ?>
                                                                                                                </select>

                                                                                                            </td>
                                                                                                            <!--Comment-->
                                                                                                            <td>
                                                                                                                <input type="text" name="requestb[]" id="requestb-<?= $products["cartid"] ?>-0" placeholder="Comment..." class="form-control typeahead"/>
                                                                                                            </td>
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            <!--Request Group-->
                                                                                                            <td>

                                                                                                                <select class="form-controll disabled-css offer-select"   name="price[]" id="price-<?= $products["cartid"] ?>-0">
                                                                                                                    <option value="">Select Group</option>
                                                                                                                    <option value="mix1">mix1</option>
                                                                                                                    <option value="mix2">mix2</option>
                                                                                                                    <option value="mix3">mix3</option>
                                                                                                                    <option value="mix4">mix4</option>
                                                                                                                </select>

                                                                                                            </td>    
                                                                                                            
                                                                                                            

                                                                                                            
                                                                                                            <!--Column name-->
                                                                                                            <td>
                                                                                                                <input type="hidden" class="boxqty<?php echo $i; ?>" name="boxqty[]" id="boxqty_0_<?php echo $i; ?>_<?php echo $products["cartid"]; ?>" value=""/>
                                                                                                                <input type="hidden" name="box[]" id="box" value="$i"/>
                                                                                                                <input type="hidden" name="total_qty_bunches[]" id="total_qty_bunches_0_<?php echo $i; ?>_<?php echo $products["cartid"]; ?>" value="0"/>
                                                                                                                
                                                                                                                <a class="add_new_row" id="add_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $i; ?>"
                                                                                                                   onclick="addNewVariety(0,1,<?php echo $ix; ?>,<?php echo $boxtypex; ?>,<?php echo $userSessionID; ?>,<?php echo $productx; ?>,<?php echo $sizeidx; ?>)"
                                                                                                                   href="javascript:void(0);"><span class="label label-success">Add Variety </span> </a>&nbsp;
                                                                                                                   
                                                                                                                <a href='javascript:void(0)' id='edit_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $i; ?>'
                                                                                                                   onclick='editVariety(0,<?php echo $products["cartid"]; ?>,<?php echo $i; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-edit white"></i> Edit </a>
                                                                                                                            
                                                                                                                <a href='javascript:void(0)' id='delete_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $i; ?>'
                                                                                                                   onclick='deleteVariety(0,<?php echo $products["cartid"]; ?>,<?php echo $i; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-times white"></i> Delete </a>


                                                                                                            </td>
                                                                                                        </tr>


                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <input type="hidden" name="total_a" id="total_a" value="1"/>
                                                                                                    <input type="hidden" name="total_availabel_product" id="total_availabel_product_<?php echo $i; ?>" value=""/>
                                                                                                    <div class="col-lg-4">
                                                                                                        <div class="error-box">
                                                                                                            <span id="errormsg-<?= $products["cartid"] ?>" style="color:#FF0000; font-size:12px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                                                        </div>
                                                                                                        <div class="heading-title heading-border-bottom">
                                                                                                            <h3 style="background-color:transparent!important;">Box Competition</h3>
                                                                                                        </div>

                                                                                                        <div class="progress progress-lg"><!-- progress bar -->
                                                                                                            <div class="progress-bar progress-bar-warning progress-bar-striped active text-left" role="progressbar" id="progressbar_id_<?php echo $s; ?>"
                                                                                                                 aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                                                                                <span>Completed 0%</span>
                                                                                                            </div>
                                                                                                        </div><!-- /progress bar -->



                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Inside Table-->
                                                                                                <input type="hidden" name="hdn_offer_id_index" id="hdn_offer_id_index" value="<?php echo $cnt_offer; ?>"/>
                                                                                            </form>
                                                                                        </div>

                                                                                        <div id="tab_b_<?php echo $i; ?>" class="tab-pane">

                                                                                            <!--Inside Table-->
                                                                                            <div class="table-responsive" id="order_details">
                                                                                                <?php
                                                                                                $buyre_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE id='" . $products['shpping_method'] . "'";
                                                                                                $rs_buyre_detail = mysqli_query($con, $buyre_detail_sql);
                                                                                                $row_buyre = mysqli_fetch_array($rs_buyre_detail);

                                                                                                $country_sql = "SELECT * FROM country WHERE id='" . $row_buyre['country'] . "'";
                                                                                                $rs_country_detail = mysqli_query($con, $country_sql);
                                                                                                $row_country = mysqli_fetch_array($rs_country_detail);
                                                                                                //echo "<pre>";print_r($row_buyre);echo "</pre>";
                                                                                                ?>

                                                                                                <ul class="list-unstyled">
                                                                                                    <div style="float:left;width:300px;"><i class="fa fa-map-marker abs"></i>
                                                                                                        <li class="footer-sprite address">
                                                                                                            <?php echo $row_buyre['first_name'] . " " . $row_buyre['last_name'] ?>,<br><?php echo $row_buyre['address']; ?>,
                                                                                                            <?php
                                                                                                            if ($row_buyre['address2'] != "") {
                                                                                                                echo $row_buyre['address2'] . ",";
                                                                                                            }
                                                                                                            ?>
                                                                                                            <?php
                                                                                                            if ($row_buyre['state'] != "") {
                                                                                                                ?>
                                                                                                                <br><?php echo $row_buyre['state']; ?>,
                                                                                                            <?php }
                                                                                                            if ($row_country['name']) {
                                                                                                                echo $row_country['name'] . "<br>";
                                                                                                            } ?>


                                                                                                        </li>
                                                                                                    </div>
                                                                                                    <?php
                                                                                                    $shipping_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE id='" . $products['shpping_method'] . "'";
                                                                                                    $rs_shipping_detail = mysqli_query($con, $shipping_detail_sql);
                                                                                                    $row_shipping = mysqli_fetch_array($rs_shipping_detail);

                                                                                                    $cargo_agency_sql = "SELECT * FROM cargo_agency WHERE id='" . $row_shipping['cargo_agency_id'] . "'";
                                                                                                    $rs_cargo_agency = mysqli_query($con, $cargo_agency_sql);
                                                                                                    $row_cargo_agency = mysqli_fetch_array($rs_cargo_agency);
                                                                                                    ?>
                                                                                                    <div style="float:left;width:1200px;">
                                                                                                        <i class="fa fa-user  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Client: <?php echo $row_buyre['first_name'] . " " . $row_buyre['last_name'] ?>
                                                                                                        </li>
                                                                                                        <i class="fa fa-plane abs"></i>
                                                                                                        <li class="footer-sprite email">
                                                                                                            Cargo Agency: <?php
                                                                                                            if ($row_cargo_agency['name'] != "") {
                                                                                                                echo $row_cargo_agency['name'];
                                                                                                            } else {
                                                                                                                echo "-";
                                                                                                            }
                                                                                                            ?>
                                                                                                        </li>
                                                                                                        <i class="fa fa-phone  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Coordination : Flora and Design
                                                                                                        </li>
                                                                                                        <i class="fa fa-pencil-square-o  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Marks: Fresh Life Floral - <?= $row_shipping['shipping_code']; ?>
                                                                                                        </li>

                                                                                                    </div>

                                                                                                </ul>
                                                                                                <a href="#" class="btn btn-3d btn-green"><i class="fa fa-comments-o"></i>Comments: <?= $product_comment; ?></a>
                                                                                            </div>
                                                                                            <!--Inside Table-->

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $b=0; ?>
                                                                        <?php $ss=10; ?>                                                                        
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer noborder offer_model_popup">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <a href="javascript:void(0);" onclick="modifyOffer('<?= $products['prodcutid'] ?>', '<?= $i ?>')" class="btn btn-primary">Send Request</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!----Fin Type Offers-->                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                    <!-- right options -->
                                    <ul class="options pull-right relative list-unstyled">
                                        <div class="tags_selected" id="tab_for_filter" style="float: left; margin-top: 4px; margin-right: 4px; display: none;"></div>
                                        <li>
                                        <!--    <a class="date_filter btn btn-danger btn-xs white" id="dates"><?= $_POST['order'] ?></a>-->
                                            <a class="date_filter btn btn-danger btn-xs white" id="dates"><?php echo $dates; ?></a>
                                            <a href="#" class="btn btn-primary btn-xs white" data-toggle="modal" data-target=".search_modal_open"><i class="fa fa-filter"></i> Filter</a>
                                        </li>
                                        <div class="modal fade search_modal_open" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form name="form1" id="form1" method="post" action="buyer/inventory.php" class="modal fade search_modal_open">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Choose Filter</h4>
                                                        </div>
                                                        <!-- Modal Body -->
                                                        <div class="modal-body">
                                                            <div class="panel-body">
                                                                
                                                                <label>Category</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_category" id="filter_category" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Category</option>
                                                                        <?php
                                                                        $result_category = category();
                                                                        while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                            <option value="<?= $row_category['id']; ?>"><?= $row_category['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                
                                                                <label>Variety</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_variety" id="filter_variety" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Variety</option>
                                                                        <?php
                                                                        $result_subcategory = subcategory();
                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                                                                                
                                                                                                                                
                                                                <label>Growers</label>
                                                                <div class="form-group">
                                                                    <select name="filter_grower" id="filter_grower" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Select Growers</option>
                                                                        <?php
                                                                        $GrowersRes = growers();
                                                                        while ($growers = mysqli_fetch_array($GrowersRes)) { ?>
                                                                            <option value="<?php echo $growers['id'] ?>"><?php echo $growers['growers_name'] ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                
                                                                <label>Size</label>
                                                                <div class="form-group">
                                                                    <select name="filter_size" id="filter_size" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Select Size</option>
                                                                        <?php
                                                                        $SizeRes = sizes();
                                                                        while ($featursi = mysqli_fetch_array($SizeRes)) { ?>
                                                                            <option value="<?php echo $featursi['id'] ?>"><?php echo $featursi['name'] ?>cm</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                
                                                                
                                                                
                                                             <!--   <label>Date</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" id="datepicker" name="datepicker">
                                                                </div>-->
                                                                

                                                                
                                                                
                                                            </div>


                                                        </div>
                                                        <!-- Modal Footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                            <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>images/ajax-loader.gif"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                </div>
                                
                                
                                    
                                    
                                    <!--class  for  search-->
                                    <form class="clearfix well well-sm search-big nomargin" action="/buyer/inventory.php" method="get">
                                        <div class="autosuggest fancy-form input-group" data-minLength="1" data-queryURL="<?php echo SITE_URL ?>/includes/autosuggest.php?limit=10&search=">
                                            <div class="input-group-btn">
                                                <button data-toggle="dropdown" class="btn btn-default input-lg dropdown-toggle noborder-right" type="button" aria-expanded="false">Filter <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li class="active"><a href="#"><i class="fa fa-check"></i> Everything</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="j#">Growers</a></li>
                                                    <li><a href="j#">Product</a></li>
                                                    <li><a href="j#">Category</a></li>
                                                    <li><a href="#">Other</a></li>
                                                </ul>
                                            </div>
                                            <input type="text" name="src" id="typeSearch" placeholder="Search From Here..." class="form-control typeahead"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-default input-lg noborder-left" type="submit"><i class="fa fa-search fa-lg nopadding"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- end class  for  search-->

                                <!-- panel content -->
                <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
                    
                                <div class="panel-body">
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                            <!--   <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif"> -->
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="dataRequest">
                                        <table class="table table-hover inventory_table" id="tablex" name="tablex">
                                            <thead>
                                            <tr>
                                                <th>Grower</th>
                                                <th>Category</th>
                                                <th>Product</th>
                                                <th>Packs</th>
                                                <th align="center">Grower<br>Price</th>                                                
                                                <th align="center">Stem/<br>Bunch</th>
                                                <th align="center">Boxes<br> Available</th>
                                                <th align="center">Date Upd.</th>
                                                <th align="center">Hora </th>
                                                <th>Send</th>
                                            </tr>
                                            </thead>
                                            <tbody id="list_inventory">
                                            <?php
                                            
                                            $i = 0;
                                            
                                        while ($products = mysqli_fetch_array($result2)) {
                                                
                                                $price["price"] = $products["price"];
                                                $sel_weight = weight_query($products["growerid"], $products["prodcutid"], $products["sizeid"], $products["feature"], $products["box_id"]);
                                                $rs_weight = mysqli_query($con, $sel_weight);
                                                $weight = mysqli_fetch_array($rs_weight);
                                                $growers["box_weight"] = $weight["weight"];
                                                if ($growers["shipping_method"] > 0) {
                                                    $total_shipping = 0;
                                                    $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";
                                                    $rs_connections = mysqli_query($con, $sel_connections);
                                                    while ($connections = mysqli_fetch_array($rs_connections)) {
                                                    }
                                                }
                                                $k = explode("/", $products["file_path5"]);
                                                $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);
                                                $logourl = SITE_URL . "user/logo/" . $k[1];
                                                ?>
                                                           <input type="hidden" id="bv-<?= $products["gid"] ?>" value="<?php echo $products["bv"]; ?>"/>
		                                           <input type="hidden" id="<?= $products["gid"] ?>" value="<?= $products["gid"] ?>"/>
                                                           
                                 <?php if ($products["bv"] != 2) { ?>
                                                           
                                                    <?php
                                                    
                                                    $total_price = $price["price"];
                                                    
                                                    $total_taxs = 0;
                                                    
                                                    $total_handling_feess = 0;
                                                    $total_product_price = $price["price"];
                                                    //before  $info["tax"] $tax
                                                    
                                                    if ($tax != 0) {
                                                        $total_price = $total_price + (($price["price"] * $tax) / 100);
                                                        $total_taxs = ($price["price"] * $tax) / 100;
                                                    }
                                                    
                                                    if ($duties != 0) {
                                                        $total_price = $total_price + (($price["price"] * $duties) / 100);
                                                        $total_duties = ($price["price"] * $duties) / 100;
                                                    }
                                                    
                                                    //before  $info["handling_fees"]
                                                    if ($handling_fees != 0) {
                                                        $total_price = $total_price + (($price["price"] * $handling_fees) / 100);
                                                        $total_handling_feess = ($price["price"] * $handling_fees) / 100;
                                                    }
                                                    $total_price = $total_price + $chargesPerKilo;
                                                    $total_price = round(($total_price), 2);
                                                    $final_price2 = $total_price;


                                        } else {
                                                    $sel_products_box = "select * from grower_box_products where box_id ='" . $products["gid"] . "'";
                                                    $rs_product_box = mysqli_query($con, $sel_products_box);
                                                    $total_product_box = mysqli_num_rows($rs_product_box);
                                                    $sel_products_box_price = "SELECT SUM(price*bunchqty),
                                                                                      SUM(bunchsize*bunchqty) 
                                                                                 FROM grower_box_products 
                                                                                where box_id ='" . $products["gid"] . "'";
                                                    
                                                    $rs_products_box_price = mysqli_query($con, $sel_products_box_price);
                                                    $products_box_price = mysqli_fetch_array($rs_products_box_price);
                                                    $final_multi_price_qty = $products_box_price['SUM(bunchsize*bunchqty)'];
                                                    $final_multi_price = ($products_box_price['SUM(price*bunchqty)'] / $final_multi_price_qty);
                                        }

                                                $total_price = $price["price"];
                                                $total_taxs = 0;
                                                $total_handling_feess = 0;
                                                //before $info["tax"] $tax
                                                
                                                if ($tax != 0) {
                                                    $total_price = $total_price + (($price["price"] * $tax) / 100);
                                                    $total_taxs = ($price["price"] * $tax) / 100;
                                                }
                                                
                                                    if ($duties != 0) {
                                                        $total_price = $total_price + (($price["price"] * $duties) / 100);
                                                        $total_duties = ($price["price"] * $duties) / 100;
                                                    }                                                
                                                
                                                if ($handling_fees != 0) {
                                                    //$info["handling_fees"] $handling_fees
                                                    $total_price  = $total_price + (($price["price"] * $handling_fees) / 100);
                                                            $total_handling_feess = ($price["price"] * $handling_fees) / 100;
                                                }
                                                
                                                $w = ($products["width"] * $products["length"] * $products["height"] * $cost_ship) / 6000;
                                                
                                                $z = $products["bname"] * $products["qty"];
                                                
                                                
                                                $product_shipping = round(($w / $z), 2);

                                                if ($product_shipping > 0) {
                                                    $total_price = $total_price + $product_shipping;
                                                    $product_shippings = $product_shipping;
                                                }
                                                
                                                $total_price = $total_price + $chargesPerKilo;
                                                $total_price = round(($total_price), 2);
                                                
                                                if ($products['p_box_type'] == "0") {
                                                    $typ_pro_st_bu = "Stem";
                                                } else {
                                                    $typ_pro_st_bu = "Bunch";
                                                }


                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $getprofile = "SELECT profile_image,file_path5 FROM growers WHERE id='" . $products["growerid"] . "'";
                                                        $row_getprofile = mysqli_query($con, $getprofile);
                                                        $row_Profile = mysqli_fetch_assoc($row_getprofile);
                                                        
                                                        if ($row_Profile['profile_image'] != "") { ?>
                                                            <img width="50" src="<?php echo SITE_URL . "user/" . $row_Profile['file_path5']; ?>">
                                                        <?php } else if ($row_Profile['file_path5'] != "") { ?>
                                                            <img width="100" src="<?php echo SITE_URL . "user/" . $row_Profile['file_path5']; ?>">
                                                        <?php } ?>
                                                            
                                                        <!--Modal image for 3452 00-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_<?php echo $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><img width="100%" src="<?php echo $logourl ?>"/></h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!---->
                                                    </td>
                                                    <td><?= $products["subs"] ?></td>
                                                    <td>
                                                        <a href="" data-toggle="modal" data-target="#single_product_modal<?php echo $products["gid"] ?>">
                                                            <?php echo $products['name'] . " " . $products["colorname"] . " " . $products["sizename"] . " cm " . $products['featurename'] . $products['bname'] . " St/Bu"; ?>
                                                        </a>
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_modal<?php echo $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" 
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><?= $products["name"] ?> <?= $products["colorname"] ?> <?= $products["featurename"] ?></h4>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <img src="<?php echo SITE_URL . $products["image_path"] ?>" style="width: 300px;height: 250px">
                                                                            <h7 id="myLargeModalLabel3" class="modal-header" style="font-size: 14px;">Fresh Life Floral</h7>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-body"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?= $products["bname"] * $products["qty"] ?></td>
                                                    <td>
                                                        <a href="" data-toggle="modal" data-target="#single_product_price<?php echo $products["gid"] ?>">$<?php echo sprintf("%.2f", $price["price"]) ?> </a>
                                                        <!--Modal image for 3452-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_price<?php echo $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;">ALL IN PRICE</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <p>Grower Price.....................$<?= sprintf("%.2f", $price["price"]) ?></p>
                                                                                <p>Shipping............................$<?= sprintf("%.2f", $product_shipping) ?></p>
                                                                                <p>Tax.....................................$<?= sprintf("%.2f", $total_taxs) ?></p>
                                                                                <p>Duties..............................$<?= sprintf("%.2f", $total_duties) ?></p>
                                                                                <p>Handling............................$<?= sprintf("%.2f", $total_handling_feess) ?></p>                                                                                
                                                                                <!--<hr>-->
                                                                                <div class="divider divider-dotted"><!-- divider --></div>
                                                                                
                                                                                <p style="color: #273746"><strong>FinalPrice.......................$<?= sprintf("%.2f", $total_price) ?></strong></p>

                                                                                <p>Additional Charges............................$<?= sprintf("%.2f", $adi_cost_ship) ?></p> 
                                                                                <!--p>Cost Ship............................$<?= $cost_ship ?></p-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    
                                                    
                                                    
                                                    <td><?= $typ_pro_st_bu ?></td>
                                                    <td><?= $products["stock"] . "" . $products["boxtype"] ?></td>
                                                    
                                                    <td><?= $products['date_update'] ?></td>
                                                    
                                                    <td><?= $products['hora'] ?></td>                                                    
                                                    
                                                    
                                                    <!--Modal Principal -->
                                                    
                                                    <td>
                                                        <button type="button" onclick="set_price(<?= $i ?>,<?= $total_price ?>)" id="btn_<?php echo $i ?>" class="btn btn-default btn-xs modal_click_1 btn_modal" data-toggle="modal" data-target=".qty_modal_<?php echo $i; ?>" style="background-color:#212f3d;color:white">
                                                            <i class="fa fa-send"></i> Request
                                                        </button>
                                                        
                                                        
                                                        <div class="modal fade qty_modal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
                                                                        <h4 class="modal-title" id="myModalLabel">Order Details</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <h4>Please Select Quantity</h4>
                                                                        <input type="text" value="<?= $products["stock"] ?>" min="1" max="<?= $products["stock"] ?>" class="form-control stepper" id="qty_amount_<?= $i ?>">
                                                                        
                                                                                     <!--Select  Order (3) -->
                                                                                    <div class="col-md-12">
                                                                                        <label>Select Order </label>
                                                                                        <div class="fancy-form fancy-form-select">
                                                                                            <select style="width: 100%; diplay: none;" name="filter_order" id="box_order_<?php echo $products["prodcutid"] . '_' . $i; ?>" class="form-control select2 fancy-form-select" tabindex="-1"
                                                                                                    onchange="orderChange(<?php echo $products["prodcutid"]; ?>,<?php echo $products["sizeid"]; ?>,<?php echo $i; ?>,<?= $total_price ?>)">
                                                                                            <option value="">Select order</option>
                                                                                                <?php
                                                                                                $category_sql = "select  id,qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' and  del_date >= '" . date("Y-m-d") . "' and is_pending=0";
                                                                                                $result_category = mysqli_query($con, $category_sql);
                                                                                                while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                                                    <option value="<?php echo $row_category['id']; ?>"><?= $row_category['qucik_desc']; ?></option>
                                                                                                <?php }
                                                                                                ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                             <input type="hidden" id="ship" name="ship" value="">
                                                                                             <div id="erMsg" style="display:none;color:red;">Please select Order.</div>
                                                                                        </div>
                                                                                    </div>                                                                        
                                                                        <br>
                                                                        
                                                                     <!--   <h4>Price</h4> -->
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                
                                                                                <div class="margin-bottom-20">Select your price
                                                                                    <input type="text" id="select_price_cls_<?= $i ?>" class="form-control select_price_cls clss" disabled>
                                                                                </div>


                                                                                     
                                                                                    <!-- Barra de precios -->                                                                                                                                                                                                                                                                                                                                
                                                                                
                                                                                <div class="slider-wrapper black-slider" >
                                                                                    <div id="slider5_<?= $i ?>" class="slider5" ></div>
                                                                                </div>
                                                                                    
                                                                                    
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="table-responsive" style="margin-top:30px;">
                                                                                    <table class="table table-hover" id="<?= $products["gid"] . "_" ?><?= $i ?>">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>Bunch/Stem</th>
                                                                                            <th>Grower Price</th>
                                                                                            <th>Shipping Cost</th>
                                                                                            <th>Tax</th>
                                                                                            <th>Handling</th>
                                                                                            <th>Final Price</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td style="background-color: white"><input id="bs_st" name="bs_st" class="form-control xs" style="border-width: 0px;" disabled type="text" value="Stem"></td>
                                                                                            <td><input style="background-color: white" id="grower_price_<?= $i ?>" name="grower_price_<?= $i ?>" class="form-control " style="border: none" disabled type="text" name="price" value="<?= sprintf("%.2f", $price["price"]) ?>"></td>
                                                                                            <td style="background-color: white"><input style="background-color: white" id="ship_cost_<?= $i ?>" name="ship_cost_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $product_shipping) ?>"></td>
                                                                                            <td style="background-color: white"><input style="background-color: white" id="tax_<?= $i ?>" name="tax_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $total_taxs) ?>"></td>
                                                                                            <td><input style="background-color: white" id="handling_<?= $i ?>" name="handling_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $total_handling_feess) ?>"></td>
                                                                                            <td>
                                                                                                <input style="background-color: white" id="final_price_<?= $i ?>" name="final_price_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $total_price) ?> ">
                                                                                                <input type="hidden" id="weight_<?= $i ?>" name="weight_<?= $i ?>" value="<?= $products['width'] ?>">
                                                                                                <input type="hidden" id="length_<?= $i ?>" name="length_<?= $i ?>" value="<?= $products['length'] ?>">
                                                                                                <input type="hidden" id="height_<?= $i ?>" name="height_<?= $i ?>" value="<?= $products['height'] ?>">
                                                                                                <input type="hidden" id="tax_porcent_<?= $i ?>" name="tax_porcent_<?= $i ?>" value="<?= $tax ?>">
                                                                                                <input type="hidden" id="ha_<?= $i ?>" name="ha_<?= $i ?>" value="<?php echo $handling_fees ?>" ">
                                                                                                <input type="hidden" id="box_type_<?= $i ?>" name="box_type_<?= $i ?>" value="<?= $products["type"] ?>">
                                                                                                <input type="hidden" id="productid_<?= $i ?>" name="productid_<?= $i ?>" value="<?= $products["prodcutid"] ?>">
                                                                                                <input type="hidden" id="order_<?= $i ?>" name="order_<?= $i ?>" value="<?= $_SESSION["filter_order"] ?>">
                                                                                                <input type="hidden" id="grower_id_<?= $i ?>" name="grower_id_<?= $i ?>" value="<?= $products['grower_id'] ?>">
                                                                                                <input type="hidden" id="size_id_<?= $i ?>" name="size_id_<?= $i ?>" value="<?= $products['sizeid'] ?>">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>


                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <div class="col-md-10"></div>
                                                                                    <div class="col-md-2" align="rigth">

                                                                                        <!--  data-dismiss="modal"-->
                                                                                        <button type="button" class="btn btn-primary " onclick="return restore_price(<?= $i ?>,<?= $price["price"] ?>,<?= $product_shipping ?>,<?= sprintf("%.2f", $total_taxs) ?>,<?= sprintf("%.2f", $total_handling_feess) ?>,<?= sprintf("%.2f", $total_price) ?>  ) "
                                                                                                style="background:#8a2b83!important;">Restore Price
                                                                                        </button>

                                                                                    </div>


                                                                                </div>
                                                                                <hr>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                                                                        <button type="button" data-toggle="modal" data-target="#modal-send-inventory" class="btn btn-primary " onclick="return send_request(<?= $i ?>)"
                                                                                style="background:#8a2b83!important;" data-dismiss="modal">Send Request
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                               
                                                </tr>
                                                  <input type="hidden" id="totalResco_<?php echo $i; ?>" name="totalResco_<?php echo $i; ?>" value=""/>
                                                  <input type="hidden" id="totalResco_<?php echo $i; ?>" name="totalResco_<?php echo $i; ?>" value=""/>
                                                  
                                                <input type="hidden" id="grower_all_send_number<?php echo $i; ?>" name="grower_all_send_number<?php echo $i; ?>" value=""/>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                                                                                       
                                    
                                </div>
                                         </form>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
        
        
        <div class="modal fade request_modal_1" id="modal-send-inventory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- header modal -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="modal-title"
                              id="myLargeModalLabel">We are now sending your request to different growers</span>
                        <div class="pull-right" style="width:150px;margin-right:100px;">
                            <div class="progress progress-striped active">
                                <!--Here all number asign as 54 is variable which need to be dynamic-->
                                <div class="progress-bar progress-bar-success" style="width:0%"><span class="count">85</span>&nbsp;Growers
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- body modal -->
                    <div class="modal-body">
                        <div>
                            <div class="alert alert-warning notify_1">
                                Your order has been sent to <span id="msg_count">54</span> growers
                            </div>
                            <div class="text-right" style="border-top: rgba(0,0,0,0.02) 1px solid;">
                                <ul class="pagination">
                                    <a class="btn btn-purple pull-right btn-sm nomargin-top nomargin-bottom"
                                       href="<?php echo SITE_URL . "buyer/my-offers.php" ?>" style="background-color:#8a2b83;">Go to grower's offers</a>
                                    <a class="btn btn-success pull-right btn-sm nomargin-top nomargin-bottom"
                                       data-dismiss="modal" href="javascript:void(0);">Continue buying</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

         <!-- /PANEL -->

            <nav>
                <ul class="pagination">
                    <?php
                    if ($_POST["startrow"] != 0) {

                        $prevrow = $_POST["startrow"] - $display;

                        print("<li><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a></li>");
                    }
                    $pages = intval($num_record / $display);

                    if ($num_record % $display) {

                        $pages++;
                    }
                    $numofpages = $pages;
                    $cur_page = $_POST["startrow"] / $display;
                    $range = 5;
                    $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                    $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                    $page_min = $cur_page - $range_min;
                    $page_max = $cur_page + $range_max;
                    $page_min = ($page_min < 1) ? 1 : $page_min;
                    $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                    if ($page_max > $numofpages) {
                        $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                        $page_max = $numofpages;
                    }
                    if ($pages > 1) {
                        print("&nbsp;");
                        for ($i = $page_min; $i <= $page_max; $i++) {
                            if ($cur_page + 1 == $i) {
                                $nextrow = $display * ($i - 1);
                                print("<li class='active'><a href='javascript:void();'>$i</a></li>");
                            } else {

                                $nextrow = $display * ($i - 1);
                                print("<li><a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a></li>");
                            }
                        }
                        print("&nbsp;");
                    }
                    if ($pages > 1) {

                        if (!(($_POST["startrow"] / $display) == $pages - 1) && $pages != 1) {

                            $nextrow = $_POST["startrow"] + $display;

                            print("<li><a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a></li> ");
                        }
                    }

                    if ($num_record < 1) {
                        print("<span class='text'>" . $XX . "</span>");
                    }
                    ?></ul>
            </nav>        
        
        
    </div>
    
        <form method="post" name="frmfprd" action="">
            <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
        </form>        
    
</section>

<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>

<script type="text/javascript">
    jQuery(window).ready(function () {
        loadScript('/includes/assets/js/jquery/jquery-ui.min.js', 
        function () {
            /** jQuery UI **/
            loadScript('/includes/assets/js/jquery/jquery.ui.touch-punch.min.js', 
            function () {
                /** Mobile Touch Slider **/
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', 
                function () { /** Slider Script **/
                    /** Slider 5******************** **/
                    var id = "";
                    jQuery(".slider5").slider({
                        //value: 0.01, 
                        animate: true,
                        min: 0.05,
                        max: 2,
                        step: 0.01,
                        range: "min",
                        slide: function (event, ui) {
                              jQuery(".select_price_cls").val(ui.value);                         
                            var idx = $(this).attr("id");
                            var arr = idx.split('_');
                            
                            $("#grower_price_" + arr[1]).val(+ui.value); 
                                                        
                            var taxes = $("#tax_porcent_" + arr[1]).val();
                            var red = (ui.value * taxes) / 100;
                            var handling = $("#ha_" + arr[1]).val();
                            var reda = (ui.value * handling) / 100;
                                                       
                            $("#final_price_" + arr[1]).val(ui.value);
                            
                            var grower_pricex = (ui.value / (1 + (taxes / 100) + (handling / 100))).toFixed(2);
                            var handling = (grower_pricex * handling / 100).toFixed(2);
                            var tax = (ui.value); 
                            
                            $("#grower_price_" + arr[1]).val(+grower_pricex);
                            $("#handling_" + arr[1]).val(+handling);
                            $("#tax_" + arr[1]).val(+tax);

                        }
                    });

                });
            });
        });
    });
    $(".clss").keypress(function (e) {
        if (e.which == 13) {
            // Acciones a realizar, por ej: enviar formulario.
            //$('#frm').submit();
            var id = $(".clss").attr("id");
            var i = id.split('_');
            var final_price = $("#select_price_cls_" + i[3]).val();
            $("#slider5_" + i[3]).slider("value", final_price);
        }
    });

        function funPage(pageno) {
            document.frmfprd.startrow.value = pageno;
            document.frmfprd.submit();
        }
    function set_price($i, $price) {
        console.log($i);
        $("#select_price_cls_" + $i).val($price);
        //$("#slider5_" + $i).slider("value", $price);
        $("#slider5_" + $i).hide();
    }

    function restore_price(i, price, shipping, tax, handling, final_price) {        
        
        $("#grower_price_" + i).val(price);
        $("#ship_cost_" + i).val(shipping);
        $("#tax_" + i).val(tax);
        $("#handling_" + i).val(handling);
        $("#final_price_" + i).val(final_price);
        $("#select_price_cls_" + i).val(final_price);
        $("#slider5_" + i).slider("value", final_price);
        
    }


     function orderChange(product_id, sizename, i,price) {
         
         $("#slider5_" + i).show();
         $("#slider5_" + i).slider("value", price);   
         
         $("#select_price_cls_" + i ).removeAttr("disabled");         
         
        var order_val = $('#box_order_' + product_id + '_' + i + ' :selected').val();
        var shipcost;
        
             
        
        if (order_val != "") {
            $.ajax({
                async: false,
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getshipcost.php', 
                data: 'order_val=' + order_val,
                success: function (data) {
                    shipcost = data;
                }
            });

        }
               
        if (shipcost == 0) {
            $('.elmn').addClass('noRow');
        }
        else {
            $('.elmn').removeClass('noRow');
        }
    }



    function send_request(i) {

        var qty = $('#qty_amount_' + i).val();
        var boxtype = $('#box_type_' + i).val();
        var price = $("#grower_price_" + i).val();
        var ship_cost = $("#ship_cost_" + i).val();
        var tax = $("#tax_" + i).val();
        var handling = $("#handling_" + i).val();
        var final_price = $("#final_price_" + i).val();
        var product_id = $("#productid_" + i).val();
        var order_id = $("#order_" + i).val();
        var grower_id = $("#grower_id_" + i).val();
        var size_id = $("#size_id_" + i).val();
        var comments = $("#contact_" + i).val();
		var totalRes =1;
		 console.log("order_id" + order_id);
		 console.log("Griwer" + grower_id);

        console.log("Este  es el  tax:" + tax);

        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/re_prod_invent.php',
            data: 'sizeId=' + size_id + '&box_quantity=' + qty + '&productId=' + product_id + '&grower_id=' + grower_id + '&buyerPrice=' + price +
            '&comment=' + comments + '&order_val=' + order_id + '&type_box=' + boxtype +
            '&tax=' + tax + '&ship_cost=' + ship_cost + '&hand_l=' + handling,
            success: function (data_s) {

                console.log(data_s);
                 jQuery("#msg_count").html("0");
                 $(".count").html(totalRes);
                 setTimeout(function () {
                 jQuery("#msg_count").html(totalRes);
                 jQuery('.notify_1').show();
                 }, 7000);
                 jQuery(".progress-bar").animate({width: "100%"}, 6000);
                 $('.count').each(function () {
                 $(this).prop('Counter', 0).animate(
                 {Counter: $(this).text()},
                 {
                 duration: 7000, easing: 'swing', step: function (now) {
                 $(this).text(Math.ceil(now));
                 }
                 });
                 });                 

            }
        });
 

        function buynow(gid) {
            var bv = $('#bv-' + gid).val();
            var fields_name = {};
            fields_name['gid6'] = gid;
            fields_name['bv'] = bv;
            if (bv != 2) {
                var subcategoryname = $('#subcategoryname-' + gid).val();
                var productname = $('#productname-' + gid).val();
                var featurename = $('#featurename-' + gid).val();
                var sizename = $('#sizename-' + gid).val();
                var boxweight = $('#boxweight-' + gid).val();
                var boxvolumn = $('#boxvolumn-' + gid).val();
                var bunchsize = $('#bunchsize-' + gid).val();
                var bunchqty = $('#bunchqty-' + gid).val();
                var shipping = $('#shipping-' + gid).val();
                var handling = $('#handling-' + gid).val();
                var tax = $('#tax-' + gid).val();
                var totalprice = $('#totalprice-' + gid).val();
                var ooprice = $('#ooprice-' + gid).val();
                var boxtypename = $('#boxtypename-' + gid).val();
                var stock = $('#stock-' + gid).val();
                var boxtype = $('#boxtype-' + gid).val();
                var product = $('#product-' + gid).val();
                var sizeid = $('#sizeid-' + gid).val();
                var feature = $('#feature-' + gid).val();
                var grower = $('#grower-' + gid).val();
                fields_name['subcategoryname'] = subcategoryname;
                fields_name['productname'] = productname;
                fields_name['featurename'] = featurename;
                fields_name['sizename'] = sizename;
                fields_name['boxweight'] = boxweight;
                fields_name['boxvolumn'] = boxvolumn;
                fields_name['bunchsize'] = bunchsize;
                fields_name['bunchqty'] = bunchqty;
                fields_name['shipping'] = shipping;
                fields_name['handling'] = handling;
                fields_name['tax'] = tax;
                fields_name['totalprice'] = totalprice;
                fields_name['ooprice'] = ooprice;
                fields_name['boxtypename'] = boxtypename;
                fields_name['stock'] = stock;
                fields_name['boxtype'] = boxtype;
                fields_name['product'] = product;
                fields_name['sizeid'] = sizeid;
                fields_name['feature'] = feature;
                fields_name['grower'] = grower;

            } else {
                var gprice = $('#gprice-' + gid).val();
                var boxtype = $('#boxtype-' + gid).val();
                var stock = $('#stock-' + gid).val();
                var product = $('#product-' + gid).val();
                var sizeid = $('#sizeid-' + gid).val();
                var feature = $('#feature-' + gid).val();
                var grower = $('#grower-' + gid).val();
                var multi = $('#multi-' + gid).val();

                fields_name['gprice'] = gprice;
                fields_name['boxtype'] = boxtype;
                fields_name['stock'] = stock;
                fields_name['product'] = product;
                fields_name['sizeid'] = sizeid;
                fields_name['feature'] = feature;
                fields_name['grower'] = grower;
                fields_name['multi'] = multi;
            }

            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_buy_ajax.php',
                data: fields_name,

                success: function (data) {
                    if (data == 'true') {
                        alert('Your reuqest has been sent...');
                    } else {
                        alert('There is some error. Please try again');
                    }
                }
            });
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
              //  $('#productvar').select2();
                        
                $('#productvar').select2({
                    ajax: {
                        url: "search_variety.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });
                        
                $('#productcol').select2({
                    ajax: {
                        url: "search_variety_color.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });
                
                
                        
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });

        $body = $("body");
        
        
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {
                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" &&
                    jQuery("#filter_variety").val() == "" &&
                    jQuery("#filter_grower").val() == "" &&
                    jQuery("#filter_size").val() == "" 
                   // jQuery("#datepicker").val() == "" &&
                   // jQuery("#filter_perbunch").val() == "" &&
                   // jQuery("#filter_sfeat").val() == "" 
                    ) {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option 1.");
                }


                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_variety = jQuery("#filter_variety").val();
                    var filter_grower = jQuery("#filter_grower").val();
                    var filter_size = jQuery("#filter_size").val();
                    //var filter_date = jQuery("#datepicker").val();
                    
                    var filter_category_text = jQuery("#filter_category option:selected").text();
                    var filter_variety_text = jQuery("#filter_variety option:selected").text();
                    var filter_grower_text = jQuery("#filter_grower option:selected").text();
                    var filter_size_text = jQuery("#filter_size option:selected").text();
                    
                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_grower=' + filter_grower +
                        '&filter_size=' + filter_size ,
                        success: function (data) {
                            jQuery("#tab_for_filter").html("");
                            $("#pagination_main").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#list_inventory").html(data);   //nombre del  id  del <tbody>
                            var pass_delete_f = "";
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }
                            
                            
                            
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;

                            }

                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';

                            //alert(pass_delete_f);
                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);

                            }
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);

                            }
                            
                                                       
                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                    'value="' + filter_grower + '" />' + filter_grower_text);


                            }

                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_size_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                    'value="' + filter_size + '" />' + filter_size_text);
                            }


                            jQuery("#tab_for_filter").show();
                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                                data: 'filter_category=' + filter_category +
                                      '&filter_variety=' + filter_variety +
                                      '&filter_grower=' + filter_grower +
                                      '&filter_size=' + filter_size ,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();

    });



    function deleteFilter(pass_click_cate, filter_category_text, filter_category, filter_variety_text, filter_variety, 
                          filter_grower_text, filter_grower, filter_size_text, filter_size) {
                              
        var a_tag_html = $("#tab_for_filter").find("a").length
        
        
        if (a_tag_html == "1") {
            location.reload();
        }
        else {
            if (pass_click_cate == filter_category_text) {
                filter_category = "";
            }
            if (pass_click_cate == filter_variety_text) {
                filter_variety = "";
            }

            if (pass_click_cate == filter_grower_text) {
                filter_grower = "";
            }
            if (pass_click_cate == filter_size_text) {
                filter_size = "";
            }

            var check_value = 0;
            var data_ajax = "";
            if (filter_category == "" &&
                filter_variety == "" &&
                filter_grower == "" &&
                filter_size == "" 
                //filter_perbunch == "" &&
                //filter_sfeat == "" 
                ) {
                check_value = 1;
            }
            if (check_value == 1) {
                alert("Please select any one option 3.");
            }
            else {
                if (filter_category == "0") {
                    filter_category = "";
                }
                if (filter_variety == "0") {
                    filter_variety = "";
                }

                if (filter_grower == "0") {
                    filter_grower = "";
                }
                if (filter_size == "0") {
                    filter_size = "";
                }
                $body.addClass("loading");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                    data: 'filter_category=' + filter_category +
                          '&filter_variety=' + filter_variety +
                          '&filter_grower=' + filter_grower +
                          '&filter_size=' + filter_size +
                    '&page_number=1',
                    success: function (data) {
                        $body.removeClass("loading");
                        jQuery("#tab_for_filter").html("");
                        $("#pagination_main").hide();
                        jQuery('.search_modal_open').modal('hide');
                        jQuery("#list_inventory").html(data);
                        var pass_delete_f = "";
                        if (filter_category == "") {
                            var filter_category_temp = "0";
                        }
                        else {
                            var filter_category_temp = filter_category;

                        }
                        if (filter_variety == "") {
                            var filter_variety_temp = "0";

                        }
                        else {
                            var filter_variety_temp = filter_variety;
                        }
                        
                      
                        
                        if (filter_grower == "") {
                            var filter_grower_temp = "0";
                        }
                        else {
                            var filter_grower_temp = filter_grower;
                        }
                        if (filter_size == "") {
                            var filter_size_temp = "0";
                        }
                        else {
                            var filter_size_temp = filter_size;
                        }
                        
                        pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                        pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                        pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                        pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                        if (filter_category != "") {
                            var pass_click_cate = "'" + filter_category_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                        }
                        if (filter_variety != "") {
                            var pass_click_cate = "'" + filter_variety_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                        }

                        if (filter_grower != "") {
                            var pass_click_cate = "'" + filter_grower_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                'value="' + filter_grower + '" />' + filter_grower_text);
                        }
                        if (filter_size != "") {
                            var pass_click_cate = "'" + filter_size_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_size_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                'value="' + filter_size + '" />' + filter_size_text);
                        }

                        jQuery("#tab_for_filter").show();
                        jQuery.ajax({
                            type: 'post',
                            url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                            data: 'filter_category=' + filter_category +
                                  '&filter_variety=' + filter_variety +
                                  '&filter_grower=' + filter_grower +
                                  '&filter_size=' + filter_size +
                            '&page_number=1',
                            success: function (data) {
                                jQuery("#pagination_nav").html(data);
                                jQuery(".ajax_loader_s").hide();
                            }
                        });
                    }
                });
            }
        }
    }
    /*fin aumentado por mi deleter  filter*/
    $(function () {
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    $('.imgDiv').hide();
                    $("#show_hide").toggle("slow");
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
    /*no tocar*/

function addNewVariety(id , cat_id , main_tr , boxtype, userSessionID, product, sizeid) {

 console.log("Ver1 ");
 
            var productsize = $("#productsize-" + cat_id + "-" + id).val();            
            var bunchqty    = $("#bunchqty-"    + cat_id + "-" + id).val();            
            var price       = $("#price-"       + cat_id + "-" + id).val();
            var requestb    = $("#requestb-"    + cat_id + "-" + id).val();            
            
            if (productsize == "") {
                $("#productsize-" + cat_id + "-" + id).css("border", "2px solid red");
            }
             else if (bunchqty == "") {
                $("#bunchqty-" + cat_id + "-" + id).css("border", "2px solid red");
            }
             else if (price == "") {
                $("#price-" + cat_id + "-" + id).css("border", "2px solid red");
              }  
            else if (requestb == "") {
                $("#requestb-" + cat_id + "-" + id).css("border", "2px solid red");                                            
            }else{
                $("#productsize-" + cat_id + "-" + id).attr("disabled", "disable");                
                $("#bunchqty-"    + cat_id + "-" + id).attr("disabled", "disable");                
                $("#price-"       + cat_id + "-" + id).attr("disabled", "disable");
                $("#requestb-"    + cat_id + "-" + id).attr("disabled", "disable");                                

                $("#productsize-" + cat_id + "-" + id).addClass("disabled-css");
                $("#bunchqty-"    + cat_id + "-" + id).addClass("disabled-css");
                $("#price-"       + cat_id + "-" + id).addClass("disabled-css");
                $("#requestb-"    + cat_id + "-" + id).addClass("disabled-css");                                

                var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
                var conta_fil = parseInt($("#total_a").val()) + 1;

                $("#total_a").val(conta_fil);                                

                //console.log("Ver5 ".$("#total_a").val());


                var box_qty_cnt = $("#main_table_" + main_tr + " tbody tr").length;
                box_qty_cnt = parseInt(box_qty_cnt) - 1;
                
                $("#request_qty_box_" + main_tr).attr("onchange", "boxQuantityChange(" + total_tr + "," + cat_id + "," + main_tr + ")");
                

                var productsize_html = $("#productsize-" + cat_id + "-0").html();
                var first_td = "<td><select class='form-controll offer-select' name='productsize[]' id='productsize-" + cat_id + "-" + total_tr + "'>" + productsize_html + "</select></td>";
 
                var bunchqty_html = $("#bunchqty-" + cat_id + "-0").html();
                var second_td = "<td><select class='form-controll offer-select' onchange='changeBunchQty(" + total_tr + "," + cat_id + "," + main_tr + ")' name='bunchqty[]' id='bunchqty-" + cat_id + "-" + total_tr + "'>" + bunchqty_html + "</select></td>";                

                var price_html = $("#price-" + cat_id + "-0").html();                
                var fourth_td = "<td><select class='form-controll modal_option offer-select' name='price[]' id='price-" + cat_id + "-" + total_tr + "'>" + price_html + "</select></td>";

                var requestb_html = $("#requestb-" + cat_id + "-0").html();
                var sixth_td = "<td><input type='text' class='form-control typeahead' name='requestb[]' placeholder='Comment...' id='requestb-" + cat_id + "-" + total_tr + "'>" + requestb_html + "</td>";                  

                var fifth_td = "<td><a href='javascript:void(0)' id='add_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='addNewVariety(" + total_tr + "," + cat_id + "," + main_tr + "," + boxtype + "," + userSessionID + "," + product + "," + sizeid + ")' class='add_new_row'><span class='label label-success'>Add Variety </span></a>&nbsp;<a class='btn btn-default btn-xs' href='javascript:void(0)' id='edit_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "'  onclick='editVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-edit white'></i> Edit </a><a class='btn btn-default btn-xs' href='javascript:void(0)' id='delete_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "'  onclick='deleteVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-times white'></i> Delete </a><input type='hidden' name='boxqty[]' class='boxqty" + main_tr + "' id='boxqty_" + total_tr + "_" + main_tr + "_" + cat_id + "' value='0' /><input type='hidden' name='total_qty_bunches[]'  id='total_qty_bunches_" + total_tr + "_" + main_tr + "_" + cat_id + "' value='0' /></td>";
                               
                $("#main_table_" + main_tr).append("<tr id='product_tr_" + total_tr + "'>" + first_td + second_td + sixth_td + fourth_td + fifth_td + "</tr>");
                                
            }
       
    }    


    function changeBunchQty(id, cartid, main_tr) {
            var porcentaje = [];
        var selected_val = $("#bunchqty-" + cartid + "-" + id + " option:selected").val();//cantidad selecionada
        total_qty_bunches = $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cartid).val();//total de bunches q me permite la caja
        $("#boxqty_" + id + "_" + main_tr + "_" + cartid).val(selected_val);
        
        var total_selected_val = 0;
        var this_box = 0;	
        var total = $("#total_a").val();
		
		for (var i = 0; i < total; i++) {
                            this_box= parseInt($("#boxqty_"+i+"_"+main_tr+"_"+cartid).val());
                            total_selected_val = parseInt(total_selected_val) + this_box;
		}
                                               		
        var nfilas = $("#main_table_" + main_tr + " tbody tr").length;
        //var total_qty = total_qty_bunches; POT
          var total_qty = 100;
        
        
        
        if (parseInt(this_box) > parseInt(total_qty)) {
            alert("Your bunch quantity greater than requested bunch qty.");
        }else{

            $("#total_availabel_product_" + main_tr).val(total_selected_val);
            //var per_val = parseInt(this_box) * 100 / parseInt(total_qty);  POT
            var per_val = 45
            
            porcentaje[nfilas - 1] = per_val;

            var porcentajes = 0;
            
              nfilas = 10;  // POT
              
            for (var i = 0; i < nfilas; i++) {
                porcentajes = porcentajes + porcentaje[i];
            }

            $("#progressbar_id_"+main_tr).css("width",parseFloat(per_val).toFixed(2)+"%");
             $("#progressbar_id_"+main_tr).html("<span>Completed "+parseFloat(per_val).toFixed(2)+"%</span>");

            $("#progressbar_id_" + main_tr).css("width", parseFloat(porcentajes).toFixed(2) + "%");
            $("#progressbar_id_" + main_tr).html("<span>Completed " + parseFloat(porcentajes).toFixed(2) + "%</span>");
        }
    }
    /*Button send request */
    function requestProduct(i) {
    
        var buyer     = '<?= $_SESSION["buyer"]; ?>'; // codigo  del  buyer
        var flag_s = true; 
        var dateRange = "";
        
        jQuery('.notify_1').hide();
        jQuery(".price_modal").modal('toggle');
        jQuery(".request_modal_1").modal('show');
                        
        var order_val      = $('#box_order_' + i + ' :selected').val();        
        
        var productId      = $('#productvar' + ' :selected').val();   
        
        var productText    = $('#productvar' + ' :selected').text();         
                     
        var sizeId         = $('#productvar' + " option:selected").attr("size_id");
        var featureId      = $('#productvar' + " option:selected").attr("cod_idfea");
        
        var noofstems      = $('#no_of_stems_'   + i).val();  
        
        var qty_boxtype    = $('#box_quantity_' + i).val() + "_" + $('#box_type_'  + i).val();        
        var box_quantity   = qty_boxtype;        
        var type_box       = $('#box_type_'      + i + ' :selected').val();                
        var type           = $('#type_req_'      + i).val();        
        var comment_pro    = $('#comment_'       + i).val();
        var tag            = $('#tag_'           + i).val();                
        var req_grow       = $('#grow_'          + i).val();         
        
        var totalRes       = $('#totalRes_'    + i).val();               
        var shippingFee    = $('#shippingFee_' + i).val();
        
        if (order_val != "") {
            $('#erMsg').hide();
        }else {
            flag_s = false;
            alert("Please select Order");            
            $('#erMsg').show();
        }
        
    if (flag_s == true) { 
        
//   TEMPORAL 1 
/*
        if (price.trim() == "") {
                   
            $.ajax({
                async: false,
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getPGrowers.php',
                data: 'select_box_type=' + type_box + '&sizeid=' + sizeId + '&box_quantity=' + boxes + '&order_val=' + order_val,
                dataType: "html",
                success: function (data) {
                    var split_data = data.split("######");
                    jQuery("#price_model_" + i + " tbody").html(split_data[0]);
                    jQuery(".select_price_cls").val(split_data[1]);
                    jQuery("#totalRes_" + i).val(split_data[2]);
                    jQuery("#grower_all_send_number" + i).val(split_data[3]);
                    $("#buyer_" +  "_" + i).val(split_data[8]);
                    $("#grower_" + "_" + i).val(split_data[9]);
                    $("#tax_" +  "_" + i).val(split_data[10]);
                    $("#shipping_" +  "_" + i).val(split_data[11]);
                    $("#handling_" +  "_" + i).val(split_data[12]);
                }
            });
        }*/
  
        console.log("flag_s "  + flag_s);
        console.log("order_val "   + order_val);  
        console.log("productId "   + productId); 
        console.log("productText "   + productText); 
 

        totalRes   = $('#totalRes_' + i).val();
        
        console.log("Box Cant "+ box_quantity);
        
        //alert('Product Ajax '+ SITE_URL);
        
        $.ajax({
            type: 'post',
            url: 'https://app.freshlifefloral.com/buyer/request_product_ajax_mp.php',
            data: 'shippingFee=' + shippingFee + 
                  '&date_range=' + dateRange +
                   '&order_val=' + order_val + 
                   '&productId=' + productId +                    
                      '&sizeId=' + sizeId +                    
                   '&featureId=' + featureId +                      
                   '&noofstems=' + noofstems + 
                '&box_quantity=' + box_quantity + 
                       '&buyer=' + buyer + 
                    '&type_box=' + type_box + 
                        '&type=' + type + 
                 '&comment_pro=' + comment_pro +                   
                 '&productText=' + productText +                   
                         '&tag=' + tag +                          
                    '&req_grow=' + req_grow,       
            success: function (data_s) {
                console.log(data_s);
                //var respuesta = JSON(data);
                
                if (data_s == 'true') {
                    jQuery("#msg_count").html("0");  
                   // $(".count").html(totalRes);
                   $(".count").html(totalRes);
                    setTimeout(function () {
                        jQuery("#msg_count").html(totalRes);
                        jQuery('.notify_1').show();
                    }, 7000);
                    jQuery(".progress-bar").animate({width: "100%"}, 6000);
                    $('.count').each(function () {
                        $(this).prop('Counter', 0).animate(
                            {Counter: $(this).text()},
                            {
                                duration: 7000, easing: 'swing', step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                            });
                    });
                } else {
                    alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    }
</script>

<script type='text/javascript'> 
 $(document).ready(function () {
								
        $('#loading').css("display", "none");
    });
		 

    $(".load").click(function (event) {
        $('#loading').css("display", "block");
    });									  
</script>

<script type='text/javascript'> 
    /*Button send request */
    function requestColor(i) {
    
        var buyer     = '<?= $_SESSION["buyer"]; ?>'; // codigo  del  buyer
        var flag_s    = true; 
        var dateRange = "";
        
       // jQuery('.notify_1').hide();
       
        jQuery(".color_modal").modal('toggle');
        
        //jQuery(".request_modal_1").modal('show');
                        
        var order_val      = $('#box_orderco_' + i + ' :selected').val();        
        
        var productId      = $('#productcol' + ' :selected').val();           
        var productText    = $('#productcol' + ' :selected').text();         
                     
        var sizeId         = $('#productcol' + " option:selected").attr("size_id");
        var featureId      = $('#productcol' + " option:selected").attr("cod_idfea");
        
        var noofstems      = $('#no_of_stemsco_'   + i).val();          
        var qty_boxtype    = $('#box_quantityco_' + i).val() + "_" + $('#box_typeco_'  + i).val();        
        
        var box_quantity   = qty_boxtype;        
        var type_box       = $('#box_typeco_'      + i + ' :selected').val();                
        var type           = $('#type_reqco_'      + i).val();        
        var comment_pro    = $('#commentco_'       + i).val();
        var tag            = $('#tagco_'           + i).val();                
        var req_grow       = $('#growco_'          + i).val();         
        
        var totalRes       = $('#totalResco_'    + i).val();               
        var shippingFee    = $('#shippingFeeco_' + i).val();
        
        
        
        if (order_val != "") {
            $('#erMsg').hide();
        }else {
            flag_s = false;
            alert("Please select Order");            
            $('#erMsg').show();
        }
        
    if (flag_s == true) {           
 
        totalRes   = $('#totalRes_' + i).val();
                               
        $.ajax({
            type: 'post',
            url: 'https://app.freshlifefloral.com/buyer/request_product_ajax_color.php',
            data: 'shippingFee=' + shippingFee + 
                  '&date_range=' + dateRange +
                   '&order_val=' + order_val + 
                   '&productId=' + productId +                    
                      '&sizeId=' + sizeId +                    
                   '&featureId=' + featureId +                      
                   '&noofstems=' + noofstems + 
                '&box_quantity=' + box_quantity + 
                       '&buyer=' + buyer + 
                    '&type_box=' + type_box + 
                        '&type=' + type + 
                 '&comment_pro=' + comment_pro +                   
                 '&productText=' + productText +                   
                         '&tag=' + tag +                          
                    '&req_grow=' + req_grow,       
            success: function (data_s) {
                
                if (data_s == 'true') {
                    jQuery("#msg_count").html("0");  

                   $(".count").html(totalRes);
                    setTimeout(function () {
                        jQuery("#msg_count").html(totalRes);
                    }, 7000);
                    jQuery(".progress-bar").animate({width: "100%"}, 6000);
                    $('.count').each(function () {
                        $(this).prop('Counter', 0).animate(
                            {Counter: $(this).text()},
                            {
                                duration: 7000, easing: 'swing', step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                            });
                    });
                } else {
                    alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    }    

</script>
<?php include("../includes/footer_new.php"); ?>