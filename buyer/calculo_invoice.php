<?php

  // PO 2018-07-02

include("../config/config_gcp.php");

$id_order     = $_GET['id_fact'];
$buyer_ofer   = $_GET['id_buy'];
$delNow       = date('Y-m-d');
  

// CABECERA
$query_cab = "select id_fact         , buyer_id         , order_number    , order_date  , shipping_method , 
                     del_date        , date_range       , is_pending      , order_serial, seen            , 
                     delivery_dates  , lfd_grower       , quick_desc      , bill_number , gross_weight    , 
                     volume_weight   , freight_value    , guide_number    , total_boxes , sub_total_amount, 
                     tax_rate        , shipping_charge  , handling        , grand_total , bill_state      , 
                     date_added      , user_added       ,
                     air_waybill     , charges_due_agent, credit_card_fees, per_kg      ,
                     handling_lax    , brokerage_lax
                from invoice_orders  
               where id_fact  = '" . $id_order   . "'  
                 and buyer_id = '" . $buyer_ofer . "'  ";

    $fact_ord    = mysqli_query($con, $query_cab);
    $factura_cab = mysqli_fetch_assoc($fact_ord);
    
    
// DETALLE
$sqlDetalis = "select product,bunchqty ,steams ,salesPriceCli
                from invoice_requests  
               where id_fact  = '" . $id_order   . "'  
                 and buyer = '" . $buyer_ofer . "'  ";

$result   = mysqli_query($con, $sqlDetalis); 

$Subtotal = 0;
$totalClient = 0;
        
while($row = mysqli_fetch_assoc($result))  {
             // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type from product where id = '" . $row['product'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['bunchqty'] * $row['salesPriceCli'];
                    $unitFac = "BUNCHES";                   
              }        
        $totalClient = $totalClient + $Subtotal;
}
    
    ///////////////////////////////////
    
    $id_fact                = $factura_cab['id_fact'];
    $per_kg                 = $factura_cab['per_kg'];
    $volume_weight          = $factura_cab['volume_weight'];
    $gross_weight           = $factura_cab['gross_weight'];    
    $total_boxes            = $factura_cab['total_boxes'];

    $air_waybill            = $factura_cab['air_waybill'];        
    $charges_due_agent      = $factura_cab['charges_due_agent']; 
    
    $handling_lax           = $factura_cab['handling_lax'];        
    $brokerage_lax          = $factura_cab['brokerage_lax'];     
    
  //  $subtotalCal            = $factura_cab['sub_total_amount'];                
    
      $subtotalCal            = $totalClient; 
      
      
      if ($buyer_ofer == 314) {     // DOH
                    $grand_total = $subtotalCal + 70;          
                    
                    $set_tts = "UPDATE invoice_orders 
                                   SET sub_total_amount  ='" . $subtotalCal .  "',                                                                              
                                       grand_total       ='" . $grand_total .  "',                                                                                                                     
                                       bill_state        = 'F'  
                                 WHERE id_fact='" . $id_order . "' ";

                    mysqli_query($con,$set_tts);    
                    
      }else if ($buyer_ofer == 48) {  // APX
          
                    $grand_total = $subtotalCal + ($subtotalCal*0.08);    //  El porcentaje es para APEX      
                    
                    $set_tts = "UPDATE invoice_orders 
                                   SET sub_total_amount  ='" . $subtotalCal .  "',                                                                              
                                       grand_total       ='" . $grand_total .  "',                                                                                                                     
                                       bill_state        = 'F'  
                                 WHERE id_fact='" . $id_order . "' ";

                    mysqli_query($con,$set_tts);                    
          
      }else if ($buyer_ofer == 318) {      // VIC        

        $freight_value = $gross_weight * $per_kg;     
        
        $handling_parcial = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling_lax + $brokerage_lax)/0.91;
        $handling = $handling_parcial*(9/100);
        
       // $credit_card_fees = ( ($subtotalCal*(2.9/100)) + ($freight_value*(2.9/100)) + ($air_waybill*(2.9/100)) + ($charges_due_agent*(2.9/100)) );        
       // $grand_total      = $subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling + $credit_card_fees;
        
        $grand_total      = $subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling + $handling_lax + $brokerage_lax ;        
                          
                    $set_tts = "UPDATE invoice_orders 
                                   SET sub_total_amount  ='" . $subtotalCal .       "',
                                       volume_weight     ='" . $volume_weight .     "',    
                                       gross_weight      ='" . $gross_weight .      "', 
                                       total_boxes       ='" . $total_boxes .       "',                                        
                                       freight_value     ='" . $freight_value .     "',                                                                               
                                       air_waybill       ='" . $air_waybill .       "',                                                                                                                      
                                       charges_due_agent ='" . $charges_due_agent . "', 
                                       handling          ='" . $handling .          "',                                       
                                       credit_card_fees  ='" . $credit_card_fees .  "',                                                                              
                                       grand_total       ='" . $grand_total .       "',                                                                                                                     
                                       bill_state        = 'F'  
                                 WHERE id_fact='" . $id_order . "' ";

                    mysqli_query($con,$set_tts);
                    
       } else if ($buyer_ofer == 315) {            // NIC  

        $freight_value = $gross_weight * $per_kg;     
        
        $handling_parcial = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent)/0.905;
        $handling = $handling_parcial*(9.5/100);    
        
        $credit_card_fees = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling)*0.03;        
        
        $grand_total      = $subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling + $credit_card_fees;        
                          
                    $set_tts = "UPDATE invoice_orders 
                                   SET sub_total_amount  ='" . $subtotalCal .       "',
                                       volume_weight     ='" . $volume_weight .     "',    
                                       gross_weight      ='" . $gross_weight .      "', 
                                       total_boxes       ='" . $total_boxes .       "',                                        
                                       freight_value     ='" . $freight_value .     "',                                                                               
                                       air_waybill       ='" . $air_waybill .       "',                                                                                                                      
                                       charges_due_agent ='" . $charges_due_agent . "', 
                                       handling          ='" . $handling .          "',                                       
                                       credit_card_fees  ='" . $credit_card_fees .  "',                                                                              
                                       grand_total       ='" . $grand_total .       "',                                                                                                                     
                                       bill_state        = 'F'  
                                 WHERE id_fact='" . $id_order . "' ";

                    mysqli_query($con,$set_tts);
                    
       }  else if ($buyer_ofer == 321) {     //  BLO
           
        //$freight_value = $gross_weight * $per_kg; 
           
        $freight_value = 0;
        
        $handling_parcial = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent)/0.905;
        $handling = $handling_parcial*(9.5/100);    
        
        $credit_card_fees = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling)*0.03;        
        
        $grand_total      = $subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling ;        
                          
                    $set_tts = "UPDATE invoice_orders 
                                   SET sub_total_amount  ='" . $subtotalCal .       "',
                                       volume_weight     ='" . $volume_weight .     "',    
                                       gross_weight      ='" . $gross_weight .      "', 
                                       total_boxes       ='" . $total_boxes .       "',                                        
                                       freight_value     ='" . $freight_value .     "',                                                                               
                                       air_waybill       ='" . $air_waybill .       "',                                                                                                                      
                                       charges_due_agent ='" . $charges_due_agent . "', 
                                       handling          ='" . $handling .          "',                                       
                                       credit_card_fees  ='" . $credit_card_fees .  "',                                                                              
                                       grand_total       ='" . $grand_total .       "',                                                                                                                     
                                       bill_state        = 'F'  
                                 WHERE id_fact='" . $id_order . "' ";

                    mysqli_query($con,$set_tts);    
                    
       }  else if ($buyer_ofer == 322) {  //  LALI   
           
        $freight_value = $gross_weight * $per_kg;     
        
        $handling_parcial = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent)/0.905;
        $handling = $handling_parcial*(9.5/100);    
        
        //$credit_card_fees = ($subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling)*0.03;        
        
        $grand_total      = $subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling ;        
                          
                    $set_tts = "UPDATE invoice_orders 
                                   SET sub_total_amount  ='" . $subtotalCal .       "',
                                       volume_weight     ='" . $volume_weight .     "',    
                                       gross_weight      ='" . $gross_weight .      "', 
                                       total_boxes       ='" . $total_boxes .       "',                                        
                                       freight_value     ='" . $freight_value .     "',                                                                               
                                       air_waybill       ='" . $air_waybill .       "',                                                                                                                      
                                       charges_due_agent ='" . $charges_due_agent . "', 
                                       handling          ='" . $handling .          "',                                       
                                       credit_card_fees  ='" . $credit_card_fees .  "',                                                                              
                                       grand_total       ='" . $grand_total .       "',                                                                                                                     
                                       bill_state        = 'F'  
                                 WHERE id_fact='" . $id_order . "' ";

                    mysqli_query($con,$set_tts);           
       }
       
   /*       $ins_lfd = "update buyer_requests 
                         set lfd ='" . $delNow . "'
                       where id_order = '170' "; 
          
          mysqli_query($con,$ins_lfd);        
          
          $ins_order = "update invoice_orders
                           set order_number = 'VIC120319'
                         where id_fact = '171' "; 
          
          mysqli_query($con,$ins_order);                  
     */     
header('location:invoice_mgmt.php')
?> 