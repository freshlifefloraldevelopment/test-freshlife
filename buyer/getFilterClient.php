<?php

require_once("../config/config_gcp.php");

include("../pagination/pagination.php");


if (isset($_GET["lang"]) && $_GET["lang"] != "") {
$_SESSION["lang"] = $_GET["lang"];

}

if (!isset($_SESSION["lang"])) {

    $_SESSION["lang"] = "en";

}

if ($_SESSION["login"] != 1) {

    header("location:" . SITE_URL);

    die;

}



if ($_SESSION["grower"] >= 1) {

    header("location:" . SITE_URL . "vendor-account.php");

    die;

}



$userSessionID = $_SESSION["buyer"];

$shippingMethod = $_SESSION['shippingMethod'];

$getShippingMethod = "select connections from shipping_method where id = '" . $shippingMethod . "'";

$shippingMethodRes = mysqli_query($con, $getShippingMethod);

$shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);

$shippingMethodArray = unserialize($shippingMethodData['connections']);

$firstConnection = $shippingMethodArray['connection_1'];

$getConnectionType = "select type from connections where id='" . $firstConnection . "'";

$conType = mysqli_query($con, $getConnectionType);

$connectionsType = mysqli_fetch_assoc($conType);

$connectionType = $connectionsType['type'];



foreach ($shippingMethodArray as $connections) {

    $getConnections = "select charges_per_kilo,charges_per_shipment from connections where id='" . $connections . "'";

    $conDatas = mysqli_query($con, $getConnections);

    $connectionDatas = mysqli_fetch_assoc($conDatas);

    $cpk = unserialize($connectionDatas['charges_per_kilo']);

    foreach ($cpk as $perkilo) {

        $chargesPerKilo = $chargesPerKilo + $perkilo;

    }

    $cps = unserialize($connectionDatas['charges_per_shipment']);

    foreach ($cps as $pership) {

        $chargesPerShip = $chargesPerShip + $pership;

    }

}

/*********get the data of session user****************/

if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {

    $stmt->bind_param('i', $userSessionID);

    $stmt->execute();

    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);

    $stmt->fetch();

    $stmt->close();

    if (empty($userID)) {

        /*********If not exist send to home page****************/

        header("location:" . SITE_URL);

        die;

    }

} else {

    /*********If not statement send to home page****************/

    header("location:" . SITE_URL);

    die;

}

$img_url = 'profile_images/noavatar.jpg';

if ($profile_image) {

    $img_url = 'profile_images/' . $profile_image;

}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";

$rs_info = mysqli_query($con, $sel_info);

$info = mysqli_fetch_array($rs_info);

$shipping_method = trim($info["shipping"], ",");

if (!isset($_REQUEST["gid6"])) {

    $sel_login_check = "select * from activity where buyer='" . $info["id"] . "' and ldate='" . date("Y-m-d") . "' and ltime='" . $hm . "' and type='buyer' and atype='li'";

    $rs_login_check = mysqli_query($con, $sel_login_check);

    $login_check = mysqli_num_rows($rs_login_check);

    if ($login_check >= 1) {

    } else {

        $name = $info["first_name"] . " " . $info["last_name"];

        $activity = "Live Inventory at " . $hm;

        $insert_login = "insert into activity set buyer='" . $info["id"] . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='li'";

        mysqli_query($con, $insert_login);
    }

}
$sel_testi = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs 
                from grower_product_box_packing gpb
                left join product p on gpb.prodcutid = p.id
                left join growers g on gpb.growerid=g.id		  
               where g.active='active' and g.growers_name is not NULL ";

$sel_testi .= " group by g.id order by p.name, g.growers_name";/**/
$rs_testi = mysqli_query($con, $sel_testi);
$total_testi = mysqli_num_rows($rs_testi);

if ($total_testi >= 1) {

    $check_grower = "";

    while ($testi = mysqli_fetch_array($rs_testi)) {

        if ($info["state"] > 0 && $testi["gbs"] != "") {

            $temp47 = explode(",", $testi["gbs"]);

            if (in_array($info["state"], $temp47)) {

            } else {
                $check_grower .= $testi["gid"] . ",";
            }

        } else {

            $check_grower .= $testi["gid"] . ",";
        }
    }
}

$check_grower = rtrim($check_grower, ',');

if ($shipping_method > 0) {

    $select_shipping_info = "select name,description from shipping_method where id='" . $shipping_method . "'";

    $rs_shipping_info = mysqli_query($con, $select_shipping_info);

    $shipping_info = mysqli_fetch_array($rs_shipping_info);
}

$sel_connections = "select days from connections where shipping_id='" . $shipping_method . "' order by id desc limit 0,1";

$rs_connections = mysqli_query($con, $sel_connections);

$connections = mysqli_fetch_array($rs_connections);

$days = explode(",", $connections["days"]);

$count1 = sizeof($days);

$res = "";

for ($io = 0; $io <= $count1 - 1; $io++) {

    if ($days[$io] == 0 || $days[$io] == 1) {

        $res .= "6";

        $res .= ",";
    } else {

        $res .= $days[$io] - 1;

        $res .= ",";
    }
}

$avdays = explode(",", $res);

$result = array_unique($avdays);

$cunav = sizeof($result);

$datedd = $shpping_onr;

$tdate = date("Y-m-d");

$date1 = date_create($datedd);

$date2 = date_create($tdate);

$interval = $date2->diff($date1);

$checka2 = $interval->format('%R%a');

if ($checka2 > 0 || $checka2 < 0) {

    $weekday = date('l', strtotime($datedd));

    if ($weekday == 'Monday') {

        $opq = 1;

    } else if ($weekday == 'Tuesday') {

        $opq = 2;

    } else if ($weekday == 'Wednesday') {

        $opq = 3;

    } else if ($weekday == 'Thursday') {

        $opq = 4;

    } else if ($weekday == 'Friday') {

        $opq = 5;

    } else if ($weekday == 'Saturday') {

        $opq = 6;

    } else if ($weekday == 'Sunday') {

        $opq = 0;
    }

    if (in_array($opq, $result)) {

        $next = $opq;

    } else {

        for ($i = 1; $i <= 6; $i++) {

            $opq = $opq + 1;

            if (in_array($opq, $result)) {

                $next = $opq;
                break;
            }

            if ($opq == 6) {
                $opq = 1;
            }
        }
    }

    if ($next == 1) {

        $dayname = "monday";

    } else if ($next == 2) {

        $dayname = "tuesday";

    } else if ($next == 3) {

        $dayname = "wednesday";

    } else if ($next == 4) {

        $dayname = "thursday";

    } else if ($next == 5) {

        $dayname = "friday";

    } else if ($next == 6) {

        $dayname = "saturday";
    }

    $shpping_onr = date('Y-m-d', strtotime('next ' . $dayname));

} else {

    $tommorow = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));

    $shpping_onr = date("Y-m-d", $tommorow);
}

$tempk = explode("-", $shpping_onr);

$shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];

$today = date("Y-m-d");

function getWeekday($date){
    return date('w', strtotime($date));
}

$day_of_week = getWeekday($today); // returns 4

if ($day_of_week == 0) {

    $starting_date = date('Y-m-d', strtotime($today . ' +8 day'));

    $days_add = 8;

    $days_add2 = 13;
}

if ($day_of_week == 1) {

    $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));

    $days_add = 7;

    $days_add2 = 12;
}

if ($day_of_week == 2) {

    $starting_date = date('Y-m-d', strtotime($today . ' +6 day'));

    $days_add = 6;

    $days_add2 = 11;
}

if ($day_of_week == 3) {

    $starting_date = date('Y-m-d', strtotime($today . ' +5 day'));

    $days_add = 5;

    $days_add2 = 10;
}

if ($day_of_week == 4) {

    $starting_date = date('Y-m-d', strtotime($today . ' +11 day'));

    $days_add = 11;

    $days_add2 = 16;
}

if ($day_of_week == 5) {

    $starting_date = date('Y-m-d', strtotime($today . ' +10 day'));

    $days_add = 10;

    $days_add2 = 15;

}

if ($day_of_week == 6) {

    $starting_date = date('Y-m-d', strtotime($today . ' +9 day'));

    $days_add = 9;

    $days_add2 = 14;

}

if ($day_of_week == 7) {

    $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));

    $days_add = 7;

    $days_add2 = 12;
}

$end_date = date('Y-m-d', strtotime($starting_date . '+5 day'));

if ($starting_date != "" && $end_date != "") {

    function week_number($date)  {
        return ceil(date('j', strtotime($date)) / 7);
    }



    $week_no = week_number($starting_date);

    if ($week_no == 1) {

        $option_update = 1;

    } else if ($week_no == 2) {

        $option_update = 2;

    } else if ($week_no == 3) {

        $option_update = 1;

    } else if ($week_no == 4) {

        $option_update = 4;

    } else if ($week_no == 5) {

        $option_update = 5;
    }

    $temp_starting_date = explode("-", $starting_date);

    $orginal_starting_date = $temp_starting_date[1] . "-" . $temp_starting_date[2] . "-" . $temp_starting_date[0];

    $temp_end_date = explode("-", $end_date);

    $orginal_end_date = $temp_end_date[1] . "-" . $temp_end_date[2] . "-" . $temp_end_date[0];

}

$page_request = "inventory";

?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .not_set_border td {
        border: none !important;
    }

</style>

<section id="middle11">
<?php

$wh = "";

//         $activity = "(1)  ".$_REQUEST['filter_variety'] ;
//         $insert_login = "insert into activity set buyer='1' ,grower='1' , ldate='" . date("Y-m-d") . "',type='grower',activity='" . $activity . "',ltime='1',atype='SU' ,name='su'";

if ($_REQUEST['filter_variety'] != "") {

    $wh .= " AND p.id = " . $_REQUEST['filter_variety'];
}

if ($_REQUEST['filter_category'] != "") {
    $wh .= " AND bo.id=" . $_REQUEST['filter_category'];
}

if ($_REQUEST['filter_grower'] != "") {
    $wh .= " AND ipx.cliente_id=" . $_REQUEST['filter_grower'];
}

if ($_REQUEST['filter_size'] != "") {
    $wh .= " AND gr.grower_id  =" . $_REQUEST['filter_size'];
}

if ($_REQUEST['filter_box'] != "") {
    $wh .= " AND ipx.qty_box_packing  =" . $_REQUEST['filter_box'];
}


/*-------------------------------------------------------------------------------*/

$sel_products = "select gpb.id 
                   from grower_product_box_packing gpb
                   left join product p on gpb.prodcutid = p.id
                   left join growers g on gpb.growerid=g.id
                  where g.active='active' and gpb.stock > 0 and p.name is not null order by p.name";

$rs_prodcuts = mysqli_query($con, $sel_products);

$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;

$from=0;
$display=50;
$page = (int) (!isset($_REQUEST["page_number"]) ? 1 : $_REQUEST["page_number"]);
$page = ($page == 0 ? 1 : $page);
$from = ($page - 1) * $display;

$dates = date("jS F Y");

if ($_REQUEST['filter_date']!="") {
    $variable = $_REQUEST['filter_date'];
    $fech = date("Y-n-j");

    if (strtotime($fech) == strtotime($variable)) {
                
        $dates = date("jS F Y");
        $query2 = "select  gr.id as grid,g.growers_name,gr.bunchqty,gr.steams,
                     (gr.bunchqty*gr.steams) as totstems , 
                     scl.name as client_name,
                     gr.offer_id , br.id_order , bo.qucik_desc , 
                     bo.id as po,
                     br.feature , 
                     f.name as features, ipx.grower_id,gr.price, ipx.size,
                     bo.order_number as embarque , gr.product  ,ipx.box_name , ipx.qty_pack,
                     ipx.id as idbox,
                     gr.request_id , ipx.cliente_id,
                     p.image_path as img_url , s.name as subs,
                     ipx.price_cad
                from grower_offer_reply gr 
               inner join buyer_requests br on gr.offer_id = br.id 
               inner join product p on gr.product = p.name 
               left join subcategory s  on p.subcategoryid   = s.id                              
               inner join buyer_orders bo on br.id_order = bo.id                  
               inner join growers g on gr.grower_id = g.id 
               inner join invoice_packing ip on (bo.id = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  ON ip.id = ipx.id_order               
                left join sub_client scl on ipx.cliente_id = scl.id
                left join features f ON br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and gr.offer_id >= '6731'
                 and reject in (0) 
                 and g.id is not NULL $wh group by ipx.id";

        $query2 .= " order by bo.id desc , gr.grower_id , gr.product LIMIT $from,$display";
        
        $result2 = mysqli_query($con, $query2);

    } else {

        
         
        $date = new DateTime($variable);

        $dates = $date->format('jS F Y');

        $query2 = "select  gr.id as grid,g.growers_name,gr.bunchqty,gr.steams,
                     (gr.bunchqty*gr.steams) as totstems , 
                     scl.name as client_name,
                     gr.offer_id , br.id_order , bo.qucik_desc , 
                     bo.id as po,
                     br.feature , 
                     f.name as features, ipx.grower_id,gr.price, ipx.size,
                     bo.order_number as embarque , gr.product  ,ipx.box_name , ipx.qty_pack,
                     ipx.id as idbox,
                     gr.request_id, ipx.cliente_id,
                     p.image_path as img_url , s.name as subs,
                     ipx.price_cad
                from grower_offer_reply gr 
               inner join buyer_requests br on gr.offer_id = br.id 
               inner join product p on gr.product = p.name 
               left join subcategory s  on p.subcategoryid   = s.id                              
               inner join buyer_orders bo on br.id_order = bo.id                  
               inner join growers g on gr.grower_id = g.id 
               inner join invoice_packing ip on (bo.id = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  ON ip.id = ipx.id_order               
                left join sub_client scl on ipx.cliente_id = scl.id
                left join features f ON br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and gr.offer_id >= '6731'
                 and reject in (0) 
                 and g.id is not NULL $wh group by ipx.id";

        $query2 .= " ";

        $query2 .= " order by bo.id desc ,gr.grower_id , gr.product LIMIT $from,$display";

        $result2 = mysqli_query($con, $query2);
    }


} else {

            //grower_offer_reply  (1.0)                        
         $activity = "(1) ".$userSessionID." (2) ".$_REQUEST['filter_variety']." (3) ".$wh ." (22)". $_REQUEST['filter_category'] ;
         $insert_login = "insert into activity set grower='" . $userSessionID . "' , ldate='" . date("Y-m-d") . "' , type='client' , 
                                                   activity='" . $activity . "' , ltime='" . $hm . "' , atype='CL1'";
         mysqli_query($con,$insert_login);
         
      $query2 = "select  gr.id as grid,g.growers_name,gr.bunchqty,gr.steams,
                     (gr.bunchqty*gr.steams) as totstems , 
                     scl.name as client_name,
                     gr.offer_id , br.id_order , bo.qucik_desc , 
                     bo.id as po,
                     br.feature , 
                     f.name as features, ipx.grower_id,gr.price, ipx.size,
                     bo.order_number as embarque , gr.product  ,ipx.box_name , ipx.qty_pack,
                     ipx.id as idbox,
                     gr.request_id, ipx.cliente_id,
                     p.image_path as img_url , s.name as subs,
                     ipx.price_cad
                from grower_offer_reply gr 
               inner join buyer_requests br on gr.offer_id = br.id 
               inner join product p on gr.product = p.name 
               left join subcategory s  on p.subcategoryid   = s.id               
               inner join buyer_orders bo on br.id_order = bo.id                  
               inner join growers g on gr.grower_id = g.id 
               inner join invoice_packing ip on (bo.id = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  ON ip.id = ipx.id_order               
                left join sub_client scl on ipx.cliente_id = scl.id
                left join features f ON br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and gr.offer_id >= '6731'
                 and reject in (0) 
                 and g.id is not NULL $wh group by ipx.id";

    $query2 .= " ";

    $query2 .= " order by bo.id desc,gr.grower_id , gr.product LIMIT $from,$display";

    $result2 = mysqli_query($con, $query2);
}

?>
            <div id="content" class="padding-20">
            <div class="panel-body">
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                         <!--   <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif"> -->
                                        </div>
                                    </div>                
                
                
                <div  class="dataRequest">
                    
                
            <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>                        </tr>                        
                        </thead>                
                        
                        <tbody id="list_inventory11">
<?php

$i = 1;

$tp = mysqli_num_rows($result2);

if ($tp == 0) {

    echo $XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

} else {

    while ($products = mysqli_fetch_array($result2)) {

        $price["price"] = $products["price"];

        if ($products["growerid"] != "407") {
            $sel_weight = "select weight from grower_product_box_weight where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' and box_id='" . $products["box_id"] . "' order by id desc limit 0,1";
            $rs_weight = mysqli_query($con, $sel_weight);
            $weight = mysqli_fetch_array($rs_weight);
        } else {
            $sel_weight = "select weight from grower_product_box_weight where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' order by id desc limit 0,1";
            $rs_weight = mysqli_query($con, $sel_weight);
            $weight = mysqli_fetch_array($rs_weight);
        }

        $growers["box_weight"] = $weight["weight"];
        $growers["box_volumn"] = round(($products["width"] * $products["length"] * $products["height"]) / 1728, 2);
        $growers["shipping_method"] = $shipping_method;
        
        if ($growers["shipping_method"] > 0) {

            $total_shipping = 0;
            $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";
            $rs_connections = mysqli_query($con, $sel_connections);

            while ($connections = mysqli_fetch_array($rs_connections)) {

                if ($connections["type"] == 2) {

                    $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);

                } else if ($connections["type"] == 4) {

                    $total_shipping = $total_shipping + round(($growers["box_volumn"] * $connections["shipping_rate"]), 2);

                } else if ($connections["type"] == 1) {

                    $box_type_calc = $products["boxtype"];

                    if ($box_type_calc == 'HB') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;

                        $connections["price_per_box"] = $connections["price_per_box"] / 2;
                    }

                    if ($box_type_calc == 'JUMBO') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;

                        $connections["price_per_box"] = $connections["price_per_box"] / 2;

                    } else if ($box_type_calc = 'QB') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4;

                        $connections["price_per_box"] = $connections["price_per_box"] / 4;

                    } else if ($box_type_calc = 'EB') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8;

                        $connections["price_per_box"] = $connections["price_per_box"] / 8;

                    }

                    $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];

                    if ($growers["box_weight"] <= $connections["box_weight_arranged"]) {

                        $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);

                    } else {

                        $total_shipping = $total_shipping + round(($connections["box_weight_arranged"] * $connections["shipping_rate"]), 2);

                        $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"], 2);

                        $total_shipping = $total_shipping + round(($remaining * $connections["addtional_rate"]), 2);

                    }

                } else if ($connections["type"] == 3) {

                    $total_shipping = $total_shipping + $connections["box_price"];
                }

            }

        }

        $k = explode("/", $products["file_path5"]);

        $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);

        $logourl = SITE_URL . "user/logo/" . $k[1];

        ?>

        <input type="hidden" id="bv-<?= $products["gid"] ?>"  value="<?php echo $products["bv"]; ?>"/>

        <?php if ($products["bv"] != 2) { ?>

            <?php

            $total_price = $price["price"];

            $total_taxs = 0;

            $total_handling_feess = 0;

            $total_product_price = $price["price"];

            if ($info["tax"] != 0) {

                $total_price = $total_price + (($price["price"] * $info["tax"]) / 100);

                $total_taxs = ($price["price"] * $info["tax"]) / 100;
            }


            if ($info["handling_fees"] != 0) {

                $total_price = $total_price + (($price["price"] * $info["handling_fees"]) / 100);

                $total_handling_feess = ($price["price"] * $info["handling_fees"]) / 100;
            }

            $total_price = $total_price + $chargesPerKilo;

            $total_price = round(($total_price), 2);

            $final_price2 = $total_price;

            ?>

            <input type="hidden" id="subcategoryname-<?php echo $products["gid"] ?>"  value="<?php echo $products["subs"] ?>">
            <input type="hidden" id="productname-<?php echo $products["gid"] ?>" value="<?php echo $products["name"] ?> <?php echo $products["featurename"] ?>">
            <input type="hidden" id="featurename-<?php echo $products["gid"] ?>" value="<?php echo $products["featurename"] ?>">
            <input type="hidden" id="sizename-<?php echo $products["gid"] ?>"   value="<?php echo $products["sizename"] ?> cm">
            <input type="hidden" id="boxweight-<?php echo $products["gid"] ?>"  value="<?php echo $weight["weight"] ?>">
            <input type="hidden" id="boxvolumn-<?php echo $products["gid"] ?>"  value="<?php echo ($products["width"] * $products["length"] * $products["height"]) ?>">
            <input type="hidden" id="bunchsize-<?php echo $products["gid"] ?>"  value="<?php echo $products["bname"] ?> Stems">
            <input type="hidden" id="bunchqty-<?php echo $products["gid"] ?>"   value="<?php echo $products["qty"] ?>">
            <input type="hidden" id="shipping-<?php echo $products["gid"] ?>"   value="<?php echo $product_shippings ?>">
            <input type="hidden" id="handling-<?php echo $products["gid"] ?>"   value="<?php echo $total_handling_feess ?>">
            <input type="hidden" id="tax-<?php echo $products["gid"] ?>"        value="<?php echo $total_taxs ?>">
            <input type="hidden" id="totalprice-<?php echo $products["gid"] ?>" value="<?php echo $final_price2 ?>">
            <input type="hidden" id="ooprice-<?php echo $products["gid"] ?>"    value="<?php echo $price["price"] ?>">
            <input type="hidden" id="boxtypename-<?php echo $products["gid"] ?>" value="<?php echo $products["boxtype"] ?>">
            <input type="hidden" id="stock-<?php echo $products["gid"] ?>"      value="<?php echo $products["stock"] ?>">
            <input type="hidden" id="boxtype-<?php echo $products["gid"] ?>"    value="<?php echo $products["type"] ?>-">
            <input type="hidden" id="product-<?php echo $products["gid"] ?>"    value="<?php echo $products["prodcutid"] ?>">
            <input type="hidden" id="sizeid-<?php echo $products["gid"] ?>"     value="<?php echo $products["sizeid"] ?>">
            <input type="hidden" id="feature-<?php echo $products["gid"] ?>"    value="<?php echo $products["feature"] ?>">
            <input type="hidden" id="grower-<?php echo $products["gid"] ?>"     value="<?php echo $products["growerid"] ?>">

            <?php

        } else {

            $sel_products_box = "select * from grower_box_products where box_id ='" . $products["gid"] . "'";
            $rs_product_box = mysqli_query($con, $sel_products_box);
            $total_product_box = mysqli_num_rows($rs_product_box);
            $sel_products_box_price = "SELECT SUM(price*bunchqty),SUM(bunchsize*bunchqty) FROM grower_box_products where box_id ='" . $products["gid"] . "'";
            $rs_products_box_price = mysqli_query($con, $sel_products_box_price);
            $products_box_price = mysqli_fetch_array($rs_products_box_price);
            $final_multi_price_qty = $products_box_price['SUM(bunchsize*bunchqty)'];
            $final_multi_price = ($products_box_price['SUM(price*bunchqty)'] / $final_multi_price_qty);

            ?>

            <input type="hidden" id="gprice-<?php  echo $products["gid"] ?>" value="<?php echo sprintf("%.2f", ($final_multi_price)) ?>">
            <input type="hidden" id="boxtype-<?php echo $products["gid"] ?>" value="<?php echo $products["bvtype"] ?>">
            <input type="hidden" id="stock-<?php   echo $products["gid"] ?>" value="<?php echo $products["stock"] ?>">
            <input type="hidden" id="product-<?php echo $products["gid"] ?>" value="<?php echo $products["prodcutid"] ?>">
            <input type="hidden" id="sizeid-<?php  echo $products["gid"] ?>" value="<?php echo $products["sizeid"] ?>">
            <input type="hidden" id="feature-<?php echo $products["gid"] ?>" value="<?php echo $products["feature"] ?>">
            <input type="hidden" id="grower-<?php  echo $products["gid"] ?>" value="<?php echo $products["growerid"] ?>">
            <input type="hidden" id="multi-<?php   echo $products["gid"] ?>" value="1">

        <?php } 
      /*  
      $getBoxReply = "select ip.cod_order , ip.prod_name , ip.bu_qty_pack , 
                                ip.offer_id , ip.id_order,
                                box_name 
                        from invoice_packing ip
                        left join invoice_packing_box ipx  ON ip.id = ipx.id_order      
                       where ip.id_fact   = '" . $products['request_bo'] . "'
                         and ip.grower_id = '" . $products['grower_id'] . "'
                         and ip.id_grower_offer  = '" . $products['grid'] . "' limit 1";
                                                                            
      $rs_getBoxReply = mysqli_query($con, $getBoxReply);
*/
      $boxPacking = $products['box_name'];
      $box_num = "";
      
      while ($row_BoxReply = mysqli_fetch_array($rs_getBoxReply)) {
                                $box_num .=  $row_BoxReply['qty_box_packing']."-";         // acumular
      }
        if ($box_num != "") {
            $boxPacking= $box_num."box";  
        }
        
        
        ?>

        <tr>

            <!--Grower-->
            <td><img src="https://app.freshlifefloral.com/<?= $products["img_url"]; ?>" width="70"></td>
            <td width="90"> <?=   $products["subs"]; ?> </td>                                    
            <td width="60"> <?=   $products["po"];  ?>  </td>

            <td width="120"> <?=  $products['growers_name'] ;  ?>  </td>
            
            <td width="50"> <?= $boxPacking ?></td>                                                

            <!--Pack-->

                         <?php 
                                  
                               if ($products["type_market"] == "0") {
                                    $market = "Stand";
                                }else{
                                    $market = "Open";
                                }
                             ?>                   
            
            <!--td><?= $market ?></td-->                                            

            <!--Variety pot-->
            <!--td width="60"><?= $products["embarque"] ?></td-->       
            <td> <?php echo  $products['product']." ".$products['size']." cm ".$products['qty_pack']." Bunches ".$products['steams']." st/bu "  ?> </td>
            
            <!--Price-->

           <td> <?php echo  number_format($products['price_cad'], 2, '.', ',')  ?> </td>
                     
                                                    <!-- Subclientes -->
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="filter_change" id="change_<?php echo $products["grid"] . '_' . $i; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                               <?php
                                                                                                    $boxDataShow = $products["qty_pack"];
                                                                                                    for ($ii = $boxDataShow; $ii >= 1; $ii--) {
                                                                                                ?>
                                                                                            <option value="<?= $ii ?>"><?= $ii ?></option>
                                                                                            <?php } ?>                                         
                                                                                        </select>                                                                        
                                                                                    </td>                                                                        
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="filter_customer" id="client_<?php echo $products["grid"] . '_' . $i; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $products['client_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_cli = "select id,name from sub_client where id != 1 order by name";
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>                                                                        
                                                                                    </td>                                                                                   
            
            <!--Send  Request-->
             <td>                                      
                 <input type="submit" name="btn_add_to_cart" class="btn btn-success btn-xs relative" style="border:0px solid #fff; " value="Save"   
                         onclick="add_to_cart(<?= $i  ?>,<?= $userSessionID ?>,<?= $products['grower_id']; ?>,<?= $products["grid"]; ?>,<?= $products['idbox'] ?>,<?= $products["request_id"]; ?>,<?= $i ?> );"/>                                                                                                                                                                                                                                                                                                                                                                                                            

                               
            
            
                
           <!-- </td>-->
        </tr>
        <?php
        $i++;
}


}
?>
        </tbody>
        </table>    
                </div>
            </div> 
  </div>              
</section>

<script type="text/javascript">
    $(document).ready(function () {
        
        
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
        

 });
 
</script>
 

<script>   
    
    function add_to_cart(offer, buyer, grower, gor_id ,idbox,request_group,cn) {
    
        var changeQty = $('#change_' + gor_id + '_' + cn).val();
        var subClient = $('#client_' + gor_id + '_' + cn).val();        
        
        
        var k = confirm('You want to assign Bunches to a client?');
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " gor_id " + gor_id + " idbox " + idbox + " request_group " + request_group + " changeQty " + changeQty + " subClient " + subClient + " cn " + cn);        
        if (k == true) { 
            alert('Confirmed assignment'); 
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " gor_id " + gor_id + " idbox " + idbox + " request_group " + request_group + " changeQty " + changeQty + " subClient " + subClient);
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/buyer/packing-viaje.php',
                data: 'offer=' + offer + '&buyer=' + buyer + '&grower=' + grower + '&gor_id=' + gor_id + '&idbox=' + idbox + '&request_group=' + request_group + '&changeQty=' + changeQty + '&subClient=' + subClient,
                success: function (data) {
                    location.reload();
                }   
            });
        }

    }

   
</script>
