<?php

// PO 2018-04-01

require_once("../config/config_gcp.php");

//require_once("functions.php"); correo

//include "Encryption.class.php";
//$key = "23c34eWrg56fSdrt"; // Encryption Key
//$crypt = new Encryption($key);

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "file/vendor-account.php");
    die;
}
$userSessionID = $_SESSION["buyer"];
if (isset($_GET['sd']) && !empty($_GET['sd'])) {
    $id = $_GET['sd'];
    $qry = "DELETE FROM `buyer_shipping_methods` WHERE `id` = '$id'";


    echo '<script language="javascript">alert("' . $id . '");</script>';
    if (mysqli_query($con, $qry)) {
        header("location:" . SITE_URL . "buyer/buyers-account.php");
        die;
    }
}
if ($_POST['boton'] == 'Send Message') {
    $affair = $_POST['af'];
    $message = $_POST['area'];/*info@freshlifefloral.com*/
    $suer_data = "select (concat_ws(' ',last_name,first_name)) as name ,email,phone from  buyers WHERE id= " . $userSessionID;
    $user_res = mysqli_query($con, $suer_data);
    $user = mysqli_fetch_array($user_res);
    $message = "Hi FreshLifeFloral Admin ,  Buyer:" . $user['name'] . "  Email: " . $user['email'] . " Phone:" . $user['phone'] . "  Mesage : ";
    $message .= $_POST['area'];
    mail("info@freshlifefloral.com", $affair, $message);

}
function time_elapsed_string($ptime)
{
    $etime = $ptime - time();

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        7 * 24 * 60 * 60 => 'week',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'week' => 'weeks',
        'day' => 'days',
        'hour' => 'hours',
        'minute' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' left';
        }
    }
}

if (isset($_POST['btn_payment_save'])) {
    //echo "<pre>";print_r($_REQUEST);echo "</pre>";
    //echo $_SESSION["buyer"];
    $query = "INSERT INTO buyers_payment
            (buyer_id,payment_method,cc_name_on_card,cc_type,cc_number,cc_exp_month,cc_exp_year,cc_cvv,paypal_email,entry_date)
VALUES ('" . $_SESSION["buyer"] . "',
        '" . $_REQUEST['payment']['method'] . "',
        '" . $crypt->encrypt($_REQUEST['payment']['name']) . "',
        '" . $crypt->encrypt($_REQUEST['payment']['state']) . "',
        '" . $crypt->encrypt($_REQUEST['payment']['cc_number']) . "',
        '" . $crypt->encrypt($_REQUEST['payment']['cc_exp_month']) . "',
        '" . $crypt->encrypt($_REQUEST['payment']['cc_exp_year']) . "',
        '" . $crypt->encrypt($_REQUEST['payment']['cc_cvv']) . "',
        '" . $_REQUEST['paypal_email'] . "',
        '" . date("Y-m-d h:s:i") . "');";

    //echo $query;
    //exit();

        
    if (mysqli_query($con, $query)) {

        $sql_shipping = "UPDATE profile_completion SET status='1' WHERE profile='payment-information' and buyer_id='" . $userSessionID . "'";
        mysqli_query($con, $sql_shipping);
        $update_msg = "Payment Update Successfully";
    } else {
        $update_error = "Please check data properly and try again.";
    }
}


//update profile data
if (isset($_POST['update_profile']) && (!empty($_POST['id']))) {


    $no_error = 1;
    $update_error = '';

    $id         = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name  = $_POST['last_name'];
    $phone      = $_POST['phone'];
    $address    = $_POST['address'];
    $address2   = $_POST['address2'];
    $city       = $_POST['city'];

    if ($_POST['country'] == 240) {
        $state_text = $_POST['state_text_us'];
        $zip        = $_POST['zip'];
    } else {
        $state_text = $_POST['state_text_other'];
    }

    $country            = $_POST['country'];
    $shipping_address   = $_POST['shipping_address'];
    $shipping_address2  = $_POST['shipping_address2'];
    $shipping_city      = $_POST['shipping_city'];
    
    if ($country == 240) {
        $shipping_state_text = $_POST['shipping_state_text_us'];
    } else {
        $shipping_state_text = $_POST['shipping_state_text_other'];
    }
    $shipping_zip       = $_POST['shipping_zip'];
    $shipping_country   = $_POST['shipping_country'];
    $web_site           = $_POST['web_site'];
    $company            = $_POST['company'];
    $coordination       = $_POST['coordination'];
    $is_public          = $_POST['is_public'];
    $biographical_info  = $_POST['biographical_info'];
    $profile_image      = $_POST['profile_image_old'];
    $name_on_card       = $_POST['name_on_card'];
    $credit_card_type   = $_POST['credit_card_type'];
    $credit_card        = $_POST['credit_card'];
    $cvv                = $_POST['cvv'];
    $expiration_month   = $_POST['expiration_month'];
    $expiration_year    = $_POST['expiration_year'];
    $company_role       = $_POST['company_role'];
    $password           = $_POST['password'];
    $repeat_password    = $_POST['repeat_password'];

    if (!empty($password) && $password != $repeat_password) {
        $update_error = "Password did not match";
        $no_error = 0;
    }
    if (!empty($password) && strlen($password) <= 5) {
        $update_error = "Password should be greater then 5 character";
        $no_error = 0;
    }
    if (empty($first_name)) {
        $update_error = "Please fill the First Name";
        $no_error = 0;
    }
    if (empty($last_name)) {
        $update_error = "Please fill the Last Name";
        $no_error = 0;
    }
    if (empty($address)) {
        $update_error = "Please fill the Address1";
        $no_error = 0;
    }
    if (empty($city)) {
        $update_error = "Please fill the City";
        $no_error = 0;
    }
    if ($_POST['country'] == 240) {
        $state_text = $_POST['state_text_us'];
        if (empty($state_text)) {
            $update_error = "Please fill the state";
            $no_error = 0;
        }

        if (empty($zip)) {
            $update_error = "Please fill the zipcode";
            $no_error = 0;
        } else {
            if (preg_match('/^[0-9]+$/', $zip)) {
            } else {
                $update_error = "Please enter valid zipcode";
                $no_error = 0;
            }
        }

    } else {
        $state_text = $_POST['state_text_other'];
        if (empty($state_text)) {
            $update_error = "Please fill the state";
            $no_error = 0;
        }
    }

      if ($is_public) {
        $is_public = 1;
      } else {
        $is_public = 0;
      }


    if (!empty($_FILES["profile_image"]["name"])) {  
        
		$target_path  = "/var/www/html/images/profile_images/"; 
		$target_path1 = "/var/www/html/images/profile_images/"; 
                
		$target_path   = $target_path . basename( $_FILES['profile_image']['name']); 
		$imageFileType = pathinfo($target_path, PATHINFO_EXTENSION);
		
        $file_name = $id . time() . "." . $imageFileType;
        $target_file = $target_path1 . $file_name;
		
		$check = getimagesize($_FILES["profile_image"]["tmp_name"]);
                
        if ($check == false) {
            $update_error = "File is not an image."; 
            $no_error = 0;
        }

        // Check file size
        if ($_FILES["profile_image"]["size"] > 1000000) {
            $update_error = "Sorry, your file is too large.";
            $no_error = 0;
        }

        // Allow certain file formats
        if ($imageFileType != "JPEG" && $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $update_error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $no_error = 0;
        }
        if ($no_error == 1) {
		if(move_uploaded_file($_FILES['profile_image']['tmp_name'], $target_file)) 
		{ 
                    // comentario
		//echo "El archivo ". $target_file. "JJJ ". basename( $_FILES['profile_image']['name'])." ha sido subido exitosamente!"; 
		} 
		else
		{ 
                    // comentario
		//echo "Hubo un error al subir tu archivo! Por favor intenta de nuevo."; 
		}
		}
		$profile_image=$file_name;
    }

    /*if (!empty($password) && $no_error == 1) {
        $stmt = $con->prepare("UPDATE buyers SET password='" . $password . "' WHERE id=$userSessionID");
        $stmt->bind_param("si", $password, $id);
        $stmt->execute();
        $stmt->close();
    }*/
    
    /**
     Cambio para validaci�n con password MD5
     */
    
    if (!empty($password) && $no_error == 1) {
            $pswdx = md5($password);
            $stmt = $con->prepare("UPDATE buyers SET pswdx='" . $pswdx . "' WHERE id=$userSessionID");
            $stmt->bind_param("si", $password, $id);
            $stmt->execute();
            $stmt->close();
        }


    if ($no_error == 1) {
       // $stmt = $con->prepare("UPDATE buyers SET first_name='$first_name', last_name='$last_name', phone='$phone', address='$address', address2='$address2', city='$city', state_text='$state_text', zip='$zip', country='$country', shipping_address='$shipping_address', shipping_address2='$shipping_address2', shipping_city='$shipping_city', shipping_state_text='$shipping_state_text', shipping_zip='$shipping_zip', shipping_country='$shipping_country',name_on_card='$name_on_card',credit_card_type='$credit_card_type',credit_card='$credit_card',cvv='$cvv',expiration_month='$expiration_month',expiration_year='$expiration_year',web_site='$web_site', company='$company',company_role='$company_role',coordination='$coordination', is_public='$is_public', biographical_info='$biographical_info', profile_image='$profile_image' WHERE id=$id");
        $stmt = $con->prepare("UPDATE buyers SET first_name='$first_name', last_name='$last_name', address='$address', address2='$address2', city='$city', state_text='$state_text', zip='$zip', country='$country', shipping_address='$shipping_address', shipping_address2='$shipping_address2', shipping_city='$shipping_city', shipping_state_text='$shipping_state_text', shipping_zip='$shipping_zip', shipping_country='$shipping_country',name_on_card='$name_on_card',credit_card_type='$credit_card_type',credit_card='$credit_card',cvv='$cvv',expiration_month='$expiration_month',expiration_year='$expiration_year',web_site='$web_site', company='$company',company_role='$company_role', is_public='$is_public', biographical_info='$biographical_info', profile_image='$profile_image' WHERE id=$id");        
        $stmt->bind_param("sssssssssssissi", $first_name, $last_name, $phone, $address, $city, $state_text, $zip, $country, $shipping_address, $shipping_city, $shipping_state_text, $shipping_zip, $shipping_country, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $web_site, $company, $coordination, $is_public, $biographical_info, $profile_image, $id);
        $stmt->execute();
        $stmt->close();

        if ($profile_image != "") {
            mysqli_query($con, "UPDATE profile_completion SET status='1' WHERE buyer_id='" . $id . "' AND profile='update-profile'");
        } else {
            mysqli_query($con, "UPDATE profile_completion SET status='0' WHERE buyer_id='" . $id . "' AND profile='update-profile'");
        }


        $update_msg = "Profile Update Successfully";
    }
}

//update profile data 1
if (isset($_POST['update_profile_1']) ) {

    $no_error_1     = 1;
    $update_error_1 = '';
    
    $id         = $_POST['id'];
    //$cargoAgency
    $coordination       = $_POST['coordination'];
    //$invoice    
    $company            = $_POST['company'];    
    $country            = $_POST['country'];    
    $city       	= $_POST['city'];    
    $address    	= $_POST['address'];
    $phone      	= $_POST['phone'];    
    
    //echo ' id '.$id .' coord '.$coordination .' company '.$company .' country '.$country .' city '.$city .' address '.$address .' phone '.$phone ;

    if (empty($coordination)) {
        $update_error_1 = "Please fill Coordination";
        $no_error_1 = 0;
    }
    //if (empty($company)) {
    //    $update_error_1 = "Please fill the Box Marks";
    //    $no_error_1 = 0;
    //}
    if (empty($city)) {
        $update_error_1 = "Please fill the City";
        $no_error_1 = 0;
    }    
    if (empty($address)) {
        $update_error_1 = "Please fill the Address";
        $no_error_1 = 0;
    }
    if (empty($phone)) {
        $update_error_1 = "Please fill the Phone";
        $no_error_1 = 0;
    }

    if ($no_error_1 == 1) {
            
        mysqli_query($con, "UPDATE buyers SET coordination='" . $coordination . "', country='" . $country . "', company='" . $company . "', city='" . $city . "', address='" . $address . "',  phone='" . $phone . "'  WHERE id='" . $id . "' ");        
        
      //  mysqli_query($con, "UPDATE profile_completion SET status='1' WHERE buyer_id='" . $id . "' AND profile='update-profile'");

        $update_msg_1 = "Update Successfully 1";
    }
}						

//submit_contribution

if (isset($_REQUEST['submit_contribution'])) {

    $groswer_name = "";
    if ($_REQUEST["grower_name"] != "") {
        $groswer_name = $_REQUEST["grower_name"];
    } else {
        $groswer_name = $_REQUEST["custom_growername"];
    }
    $abs_path = __DIR__ . "/upload_recent_purchase/";

    $target_dir = "upload_recent_purchase/";
    $strtotime = date("Y-m-d h:i:s");
    $new_file_name_invoice = strtotime($strtotime) . "_" . basename($_FILES["file_recent_purchase"]["name"]);
    $target_file_invoice = $target_dir . $new_file_name_invoice;

    if (move_uploaded_file($_FILES["file_recent_purchase"]["tmp_name"], $target_file_invoice)) {
        $contributions = "yes";
    } else {
        $contributions = "no";
    }
    $abs_path = __DIR__ . "/upload_updated_varieties/";
    $target_dir = "upload_updated_varieties/";
    $new_file_name_var = strtotime($strtotime) . "_" . basename($_FILES["file_updated_var"]["name"]);
    $target_file_var = $target_dir . $new_file_name_var;

    $sql_contribution = "UPDATE profile_completion SET status='1' WHERE profile='contribution' and buyer_id='" . $userSessionID . "'";
    mysqli_query($con, $sql_contribution);

    if (move_uploaded_file($_FILES["file_updated_var"]["tmp_name"], $target_file_var)) {
        $contributions = "yes";
        $csv_file = $abs_path . $new_file_name_var;

        if (($handle = fopen($csv_file, "r")) !== FALSE) {
            fgetcsv($handle);
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    $col[$c] = $data[$c];
                }

                $verities_name = $col[0];
                $market_type = $col[1];
                $price_k = $col[2];
                $size_k = $col[3];
                $country_k = $col[4];
                $date_k = $col[5];

                $final_date = date("Y-m-d", strtotime($col5));
                $final_size_sql = "SELECT * from sizes where name='" . $size_k . "'";
                $result_sk = mysqli_query($con, $final_size_sql);
                $row_sk = mysqli_fetch_array($result_sk);
                $final_size_val = $row_sk['id'];
                $query = "INSERT INTO historic_price(grower_id,verities_id,market_type,size,price,country,date,entry_date) VALUES('" . $groswer_name . "','" . $verities_name . "','" . $market_type . "','" . $final_size_val . "','" . $price_k . "','" . $country_k . "','" . $final_date . "','" . date("Y-m-d h:i:s") . "')";
                $s = mysqli_query($con, $query);
            }
            fclose($handle);
        }

    } else {
        $contributions = "no";
    }

    $temp_rating = $_REQUEST["quality_rating"] + $_REQUEST["freshness_rating"] + $_REQUEST["trustworthiness_rating"] + $_REQUEST["pricing_rating"] + $_REQUEST["packing_rating"];
    $final_rating = $temp_rating * 5 / 25;
    $ins = "insert into buyer_contributions set grower_name='" . $groswer_name . "', grower_id= " . $_REQUEST['grower_id'] . ", buyer_id='" . $userSessionID . "',comment='" . $_REQUEST['comment'] . "',quality_rating='" . $_REQUEST["quality_rating"] . "',freshness_rating='" . $_REQUEST["freshness_rating"] . "',trustworthiness_rating='" . $_REQUEST["trustworthiness_rating"] . "',pricing_rating='" . $_REQUEST["pricing_rating"] . "',packing_rating='" . $_REQUEST["packing_rating"] . "',final_rating='" . round($final_rating, 1) . "',invoice_file='" . $new_file_name_invoice . "',variety_file='" . $new_file_name_var . "',entry_date='" . date("Y-m-d h:i:s") . "'";

    mysqli_query($con, $ins);
    $id = mysqli_insert_id($con);

    $sql = "INSERT INTO user_ratings (ur_quality_rating, ur_freshness_rating, ur_trustworthiness_rating, ur_pricing_rating, ur_packing_rating, ur_final_rating, ur_comment, ur_user_idFk, ur_user_type, ur_grower_idFk, ur_create_datetime) VALUES(" . $_REQUEST["quality_rating"] . "," . $_REQUEST["freshness_rating"] . "," . $_REQUEST["trustworthiness_rating"] . "," . $_REQUEST["pricing_rating"] . "," . $_REQUEST["packing_rating"] . "," . round($final_rating, 1) . ",'" . $_REQUEST['comment'] . "'," . $userSessionID . ", 'buyer', " . $_REQUEST['grower_id'] . ", '" . date("Y-m-d H:i:s") . "')";
    $res = mysqli_query($con, $sql);
    if (mysqli_insert_id($con) > 0) {

        $suer_data = "SELECT first_name, last_name FROM buyers WHERE id= " . $userSessionID;
        $user_res = mysqli_query($con, $suer_data);
        $user = mysqli_fetch_array($user_res);

     // po   $email_subject = "Buyer Rating For Grower"; // The Subject of the email
     // po   $email_message = "Hi FreshLifeFloral Admin, <br><br> One Buyer commented/reviewed on a grower. Please Click 'Approve Comment' link if you want to display this review under Grower Section. <br><br> Buyer Full Name: " . $user['first_name'] . " " . $user['last_name'] . "<br><br> Comment: " . $_REQUEST['comment'] . " <br><br> Buyer Ratings: " . round($final_rating, 1) . " <br><br> Grower Full Name: " . $groswer_name . " <br><br> Grower's Profile Link: <a href='" . SITE_URL . "single-growers.php?id=" . $_REQUEST['grower_id'] . "'> Grower Profile </a> <br><br> Link: <a href='" . SITE_URL . "approve_comment.php?id=" . mysqli_insert_id($con) . "'>Approve Comment</a> <br><br>  Thank You"; // Message that the email has in it

     // po   date_default_timezone_set('Asia/Karachi');
        /***** Exists in functins.php  ******/
     // po   send_mail($email_subject, $email_message);
    }
    //exit();
}
//End submit_contribution
//Delete Avatar
if (isset($_GET['delete_avatar']) && (!empty($_GET['id']))) {
    $user_id = $_GET['id'];
    if ($stmt = $con->prepare("SELECT id, profile_image FROM buyers WHERE id =$userSessionID")) {
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->bind_result($userID, $profile_image);
        $stmt->fetch();
        $stmt->close();

        unlink('/images/profile_images/' . $profile_image);

        $stmt = $con->prepare("UPDATE buyers SET profile_image='' WHERE id=$userSessionID");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $stmt->close();
        mysqli_query($con, "UPDATE profile_completion SET status='0' WHERE buyer_id='" . $userSessionID . "' AND profile='profile-picture'");
        header("location:buyer/buyers-account.php?uam=1");
    }
}
if (isset($_GET['uam'])) {
    $avter_msg = "Avatar image deleted successfully";
}
//manage labels for form area 
if ($_SESSION["lang"] == "ru") {
    $labels_arr['first_name'] = '????????????';
    $labels_arr['last_name'] = '???????';
    $labels_arr['email'] = 'Email';
    $labels_arr['phone'] = '???????';
    $labels_arr['address'] = '?????';
    $labels_arr['city'] = '?????';
    $labels_arr['state_text'] = '???????????';
    $labels_arr['zip'] = '???????? ??????';
    $labels_arr['country'] = '??????';
    $labels_arr['web_site'] = '????';
    $labels_arr['company'] = '???????? ????????';
    $labels_arr['coordination'] = '???????????';
} else {
    $labels_arr['first_name'] = 'First Name';
    $labels_arr['last_name'] = 'Last Name';
    $labels_arr['email'] = 'Email';
    $labels_arr['phone'] = 'Phone';
    $labels_arr['address1'] = 'Address1';
    $labels_arr['address2'] = 'Address2';
    $labels_arr['city'] = 'City';
    $labels_arr['state_text'] = 'State';
    $labels_arr['province'] = 'Province';
    $labels_arr['zip'] = 'Zip';
    $labels_arr['country'] = 'Country';
    $labels_arr['web_site'] = 'Web Site';
    $labels_arr['company'] = 'Company';
    $labels_arr['coordination'] = 'Coordination';
}
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}
//echo "imagen";

$img_url = '/images/profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}

$page_request = "dashboard";
?>

<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>


<!--Aqui estaba  el  script uno-->
<!-- /ASIDE -->
<div class="modal fade req_for_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Please select one of your orders to proceed <span class="c_or">OR</span>
                    <ul class="nav nav-tabs pull-right" style="margin-right: 10%;margin-top: -7px;">
                        <span style="float:left;width:100%;" data-toggle="modal" data-target=".buying_method_modal"><a href="javascript:void(0)" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;width:100%;"><i class="fa fa-plus"></i>Create new order</a></span>
                    </ul>
                </h4>
            </div>
            <!-- body modal 1-->
            <div class="modal-body">

                <div class="table-responsive">
                    <table class="table table-hover req_table">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Order Number</th>
                            <th>Shipping Options</th>
                            <th>Delivery Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $getPendingOrders = "select * from buyer_orders where buyer_id = '" . $userSessionID . "' and del_date >= '" . date('Y-m-d') . "'";
                        $getPendingOrdersRes = mysqli_query($con, $getPendingOrders);
                        
                        if (mysqli_num_rows($getPendingOrdersRes) > 0) {
                            while ($pendingOrders = mysqli_fetch_assoc($getPendingOrdersRes)) {
                                $getShip = mysqli_query($con, "select * from buyer_shipping_methods where id='" . $pendingOrders['shipping_method'] . "'");
                                $getShipData = mysqli_fetch_assoc($getShip);
                                $descShip = '';
                                if ($getShipData['shipping_method_id'] != 0) {
                                    $shippingMethodid = $getShipData['shipping_method_id'];
                                    $getShippingMethod = "select * from shipping_method where id = '" . $shippingMethodid . "'";
                                    $shippingMethodRes = mysqli_query($con, $getShippingMethod);
                                    $shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);
                                    $descShip = $shippingMethodData['description'];
                                } else {
                                    if ($getShipData['own_shipping'] == '1') {
                                        $getshipping_type = "select * from cargo_agency where id='" . $getShipData['cargo_agency_id'] . "'";
                                        $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                        $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                        //echo '<pre>';print_r($shippingType);
                                        $descShip = $shippingType['name'];
                                    } else {
                                        $shipping_type = $getShipData['choose_shipping'];

                                        if (!empty($shipping_type) && $shipping_type != '') {
                                            $getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
                                            $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                            $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                            $descShip = $shippingType['name'];
                                        } else {
                                            $descShip = $getShipData['name'];
                                        }
                                    }
                                }
                                ?>
                                <tr>
                                    <td><label class="radio"><input type="radio" name="order_id" value="<?php echo $pendingOrders['id'] ?>"><i></i></label></td>
                                    <td><?= $pendingOrders['order_number']; ?></td>
                                    <td><?= $descShip; ?></td>
                                    <td><?= $pendingOrders['del_date']; ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="2">No Results Found.....</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MIDDLE -->
<section id="middle">
    <div id="content" class="padding-20">
        <div class="page-profile">
            <?php
            if ($update_msg) {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_msg . '</div>';
            } elseif ($update_error) {
                echo '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_error . '</div>';
            } elseif ($avter_msg) {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $avter_msg . '</div>';
            }
            if ($contributions == "yes") {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>Contributions data has been uploaded.</div>';
            }
            ?>
            <div id="shippingAlert" style="display:none;" class="alert"><a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">x</a>
                <div class="innerMsg"></div>
            </div>
            <div class="row">
                <!-- COL 1 -->
                <div class="col-md-4 col-lg-3">

                    <section class="panel">
                        <!-- po
                         <span style="float:left;width:100%;" data-toggle="modal" data-target=".buying_method_modal">
                             <a href="javascript:void(0)" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;width:100%;"><i class="fa fa-plus"></i>Create new order.</a>
                         </span> -->
                        
                        <div class="panel-body noradius padding-10">
                            <figure class="margin-bottom-10">
                                <!-- image -->
                                <p align="center"><img class="img-responsive" style="max-width: 200px; max-height: 200px" src="<?php echo $img_url ?>"  alt=""></p>
                            </figure>
                            <!-- /image -->
                            <!-- progress bar -->
                            <?php
                            $progress_que = "SELECT * from profile_completion where buyer_id='" . $userSessionID . "'";
                            $result_progress = mysqli_query($con, $progress_que);
                            $tot_pro = 0;
                            $profile_compelete = 0;
                            while ($row_progress = mysqli_fetch_array($result_progress)) {
                                if ($row_progress['status'] == "1") {
                                    $profile_compelete++;
                                }
                                $tot_pro++;
                            }
                            $progress = $profile_compelete * 100 / $tot_pro;
                            ?>

                            <!-- /progress bar -->
                            <!-- updated -->
                            <ul class="list-unstyled size-13">
                                <?php
                                
                                $progress_que = "SELECT * from profile_completion where buyer_id='" . $userSessionID . "'";
                                $result_progress = mysqli_query($con, $progress_que);

                                
                                ?>
                            </ul>
                            <!-- /updated -->
                            <hr class="half-margins">
                            <!-- About -->
                            <h3 class="text-black"> <?php echo $first_name . " " . $last_name; ?>
                                <small class="text-gray size-14"> / <?php echo $company_role; ?></small>
                            </h3>
                            <?php

                            if ($progress != 100) {
                                ?>

                            <?php } else {
                                ?>
                                <h4>Shipping Information</h4>
                                <div class="table-responsive">
                                    <table class="table table-hover nomargin">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <!-- <th>Description</th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        //echo $userSessionID;
                                        $getShippingMethod = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "'";
                                        $shippingMethodRes = mysqli_query($con, $getShippingMethod);                            
                                                        
                                        if (mysqli_num_rows($shippingMethodRes) > 0) {
                                            $temp_shipping_array = array();
                                            while ($shippingMethodData = mysqli_fetch_assoc($shippingMethodRes)) {
                                                //echo '<pre>';print_r($shippingMethodData);
                                                if ($shippingMethodData['shipping_method_id'] != 0) {
                                                    $dt = mysqli_query($con, "select * from shipping_method where id =" . $shippingMethodData['shipping_method_id']);
                                                    $sdata = mysqli_fetch_assoc($dt);
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $sdata['name'] ?></td>
                                                    </tr>

                                                    <?php
                                                } else {

                                                    $shipping_type = $shippingMethodData['choose_shipping'];

                                                    if (!empty($shipping_type) && $shipping_type != '') {
                                                        $getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
                                                        $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                        $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                        $shippingMethodDataname = $shippingType['name'];
                                                        if ($shipping_type == "fedex") {
                                                            $shippingMethodDataname = "22333 Barbacoa Dr., Fed Ex delivery, A1 Floral Farms";
                                                        } else if ($shipping_type == "local") {
                                                            $shippingMethodDataname = "Atlanta International Airport, American Airlines, Serenity Flowers";
                                                        } else if ($shipping_type == "FOB") {
                                                            $shippingMethodDataname = "Tabacarcen, GyG Cargo, Flora & Design";
                                                        }
                                                    } else {
                                                        $shippingMethodDataname = $shippingMethodData['name'];
                                                        if ($shipping_type == "fedex") {
                                                            $shippingMethodDataname = "22333 Barbacoa Dr., Fed Ex delivery, A1 Floral Farms";
                                                        } else if ($shipping_type == "local") {
                                                            $shippingMethodDataname = "Atlanta International Airport, American Airlines, Serenity Flowers";
                                                        } else if ($shipping_type == "FOB") {
                                                            $shippingMethodDataname = "Tabacarcen, GyG Cargo, Flora & Design";
                                                        }
                                                    }

                                                    ?>
                                                    <?php
                                                    if (!in_array($shippingMethodDataname, $temp_shipping_array)) { ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $shippingMethodDataname; ?>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    ?>

                                                    <?php
                                                    array_push($temp_shipping_array, $shippingMethodDataname);
                                                }
                                            }
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="3">No Shipping Methods Found.....</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                            <!-- /About -->
                            <hr class="half-margins">
                        </div>
                    </section>
                </div>
                <!-- /COL 1 -->
                <!-- COL 2 -->
                <div class="col-md-6 col-lg-6">
                    
                        
                         <span style="float:left;width:100%;" data-toggle="modal" data-target=".buying_method_modal">
                             <a href="javascript:void(0)" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;width:100%;"><i class="fa fa-plus"></i>Create new order.</a>
                         </span> 
                    
                    <div class="tabs white nomargin-top">
                        <ul class="nav nav-tabs tabs-primary">
                            <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <!--<li><a href="#contributions" data-toggle="tab">Contributions</a></li> -->
                            <li><a href="#shipping" data-toggle="tab">Shipping</a></li>

                            <!--Order create modal open-->
                            <div class="modal fade buying_method_next" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content" style="min-height: 300px;">
                                        <!-- header modal -->
                                        <div class="modal-header" >
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">x</span></button>
                                            <h4 class="modal-title" id="myLargeModalLabel">Select Date</h4>
                                        </div>
                                        <!-- body modal 2-->
                                        <div class="modal-body">
                                            <div class="col-md-12 margin-bottom-20">
                                                <h4>Quick Description</h4>
                                                <textarea value="2" class="form-control" id="qty_desc" name="qty_desc" ></textarea>
                                            </div>
                                            
                                            <!-- date picker -->
                                            <label>Select delivery date</label>
                                            <!--<?php //echo $products["prodcutid"]; ?>_<?php ///echo $i; ?>--->
                                            <label class="field cls_date_start_date" id="cls_date">
                                                <input value="" class="form-control required start_date" placeholder="Select Date" style="width: 400px!important;text-indent: 32px;border-radius: 5px;" type="text">
                                            </label>
                                            
                                            <br>
                                            <div class="to_right">
                                                <a href="" class="btn btn-3d btn-purple no_back" style="background-color:#923A8D!important;" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                <a href="#" class="btn btn-3d btn-purple" data-toggle="modal" data-target=".buying_method_modal" style="background-color:#923A8D!important;" data-dismiss="modal"><i class="fa fa-chevron-left"></i>Back</a>&nbsp;&nbsp;
                                                <button style="background:#8a2b83!important;" onclick="send_request()" class="btn btn-primary" type="button">Create order</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Order create modal End-->
                        </ul>
                        
                        
                        <!--Next Modal Open-->
                        <div class="modal fade buying_method_modal" tabindex="-1" role="dialog"
                             aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content" style="min-height: 570px;">

                                    <!-- header modal -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">X</span></button>
                                        <h4 class="modal-title" id="myLargeModalLabel">Select Shipping Option</h4>
                                    </div>
                                    <!-- body modal 3-->
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover nomargin">
                                                <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Company</th>
                                                    <th>Description</th>
                                                    <th>Code</th>
                                                    <th>Destiny</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $getShippingMethod = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "'";
                                                $shippingMethodRes = mysqli_query($con, $getShippingMethod);
                                                if (mysqli_num_rows($shippingMethodRes) > 0) {
                                                    while ($shippingMethodData = mysqli_fetch_assoc($shippingMethodRes)) {
                                                        //echo '<pre>';//print_r($shippingMethodData);
                                                        if ($shippingMethodData['shipping_method_id'] != 0) {
                                                            $dt = mysqli_query($con, "select * from shipping_method where id =" . $shippingMethodData['shipping_method_id']);
                                                            $sdata = mysqli_fetch_assoc($dt);
                                                            ?>
                                                            <tr>

                                                                <td align="left">
                                                                    <?php
                                                                    if ($shippingMethodData['default_shipping'] == 1) {
                                                                        ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['shipping_method_id']; ?>" checked="checked">
                                                                        <?php

                                                                    } else { ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['shipping_method_id'] ?>">
                                                                    <?php }
                                                                    ?>
                                                                </td>


                                                               <!-- <td><?php echo $sdata['name']; ?></td>-->
                                                                <td><?php echo $shippingMethodData['company'] ?></td>
                                                                <td><?php echo $sdata['description']; ?></td>
                                                                <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                                <td><?php
                                                                    if ($sdata['destination_country'] != 0) {
                                                                        $getCountry = "select *from country where id =" . $sdata['destination_country'];
                                                                        $countryRes = mysqli_query($con, $getCountry);
                                                                        $country_s = mysqli_fetch_assoc($countryRes);
                                                                        echo $country_s['name'];
                                                                    } else {
                                                                        echo "No defined";
                                                                    } ?>
                                                                </td>
                                                            </tr>
                                                        <?php } else {
                                                            // echo $shippingMethodData['id'];
                                                            if ($shippingMethodData['own_shipping'] == '1') {
                                                                $getshipping_type = "select * from cargo_agency where id='" . $shippingMethodData['cargo_agency_id'] . "'";
                                                                $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                                $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                                //echo '<pre>';print_r($shippingType);
                                                                $shippingMethodDataname = $shippingType['name'];
                                                            } else {
                                                                $shipping_type = $shippingMethodData['choose_shipping'];
                                                                if (!empty($shipping_type) && $shipping_type != '') {
                                                                    $getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
                                                                    //echo $getshipping_type;
                                                                    $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                                    $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                                    $shippingMethodDataname = $shippingType['name'];
                                                                } else {
                                                                    $shippingMethodDataname = $shippingMethodData['name'];
                                                                }
                                                            }
                                                            ?>
                                                            <tr>

                                                                <td align="center">
                                                                    <?php
                                                                    if ($shippingMethodData['default_shipping'] == 1) {
                                                                        ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['id']; ?>" checked="checked">
                                                                        <?php

                                                                    } else { ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['id'] ?>">
                                                                    <?php }
                                                                    ?>
                                                                </td>
                                                                <td><?php echo $shippingMethodDataname; ?></td>
                                                                <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                                <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                                <td><?php
                                                                    if ($shippingMethodData['country'] != 0) {
                                                                        $getCountry = "select *from country where id =" . $shippingMethodData['country'];
                                                                        $countryRes = mysqli_query($con, $getCountry);
                                                                        $country_s = mysqli_fetch_assoc($countryRes);
                                                                        echo $country_s['name'];
                                                                    } else {
                                                                        echo "No defined";
                                                                    } ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td colspan="3">No Shipping Method Found.....</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="to_right">
                                            <br>
                                            <div id="erMsg" style="display:none;color:red;">Please select a shipping method.</div>
                                            <a href="" class="btn btn-3d btn-purple no_back" style="background-color:#923A8D!important;" data-dismiss="modal" aria-label="Close">Cancel</a>
                                            <a id="nextOpt" style="display:none" href="javascript:void(0);" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal">dfsd Jose </a>
                                            <a href="javascript:void(0);" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;" onclick="checkOption();">Next <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <!-- Overview -->
                            <div id="overview" class="tab-pane active">
                                <table class="table table-vertical-middle margin-bottom-60">
                                    <thead>
                                    <tr class="size-15">
                                        <th class="weight-400">ORDERS</th>
                                        <th class="weight-400">DELIVERY DATE</th>
                                        <th class="text-center hidden-xs width-100 weight-300">ADD ONS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                   // $getOrders = "select * from buyer_orders where buyer_id='" . $userSessionID . "' and del_date >= '" . date('Y-m-d H:i:s') . "' and is_pending=0";  en la App esta is_pending = 1
                                   // 
                                    $getOrders = "select * from buyer_orders where buyer_id='" . $userSessionID . "' and del_date >= '" . date('Y-m-d') . "' ";
									//echo  $getOrders ;
                                    $getOrderRes = mysqli_query($con, $getOrders);
                                    if (mysqli_num_rows($getOrderRes) > 0) {
                                        $index = 0;
                                        while ($row_s_request = mysqli_fetch_array($getOrderRes)) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <h4 class="nomargin size-16">
                                                        <a href="#" data-toggle="modal" data-target=".ofers_modal_<?php echo $index; ?>"><?php echo $row_s_request['id']."-".$row_s_request['qucik_desc']; ?></a>
                                                        <!--Offer's Modal Start-->
                                                        <div class="modal fade ofers_modal_<?php echo $index; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-full">
                                                                <div class="modal-content">
                                                                    <!-- header modal -->
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                                        <h4 class="modal-title" id="myLargeModalLabel"><?php echo $row_s_request['id']."-".$row_s_request['qucik_desc']; ?></h4>
                                                                    </div>
                                                                    <!-- body modal 4-->
                                                                    <div class="modal-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-hover table-vertical-middle nomargin">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th class="width-30">Img</th>
                                                                                    <th>Order</th>
                                                                                    <th>St.Requested</th>
                                                                                    <th>St.Still Needed</th>
                                                                                    <th>Date</th>
                                                                                    <th>P.O. Number</th>
                                                                                    <th>Offers</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <?php

                                                                                $i = 1;
                                                                                
                                                                                $query2 = "select gpb.*, gpb.id as cartid,gpb.date_added ,
                                                                                                  p.id,p.name,p.color_id,p.image_path,
                                                                                                  s.name as subs,sh.name as sizename,ff.name as featurename                                                                                                   
                                                                                             from buyer_requests gpb
                                                                                             left join (select  id as id,subcategoryid,name,color_id,image_path from product) p on gpb.product = p.id
                                                                                             left join subcategory s on p.subcategoryid=s.id  
                                                                                             left join features ff   on gpb.feature=ff.id
                                                                                             left join sizes sh      on gpb.sizeid=sh.id 
                                                                                            where gpb.buyer='" . $userSessionID . "' 
                                                                                              and gpb.id_order = '" . $row_s_request["id"] . "'
                                                                                              and gpb.lfd>='" . date("Y-m-d") . "' 
                                                                                            order by gpb.id desc  ";

                                                                                $result2 = mysqli_query($con, $query2);
                                                                                $tp = mysqli_num_rows($result2);
                                                                                if ($tp >= 1) {
                                                                                    while ($producs = mysqli_fetch_array($result2)) {
                                                                                        //echo '<pre>';print_r($producs);die;
                                                                                        $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag from grower_offer_reply gr 
                                                                                          left join growers g on gr.grower_id=g.id 
                                                                                          left join country cr on g.country_id=cr.id
                                                                                          where gr.offer_id='" . $producs["cartid"] . "' and 
                                                                                          gr.buyer_id='" . $userSessionID . "' and 
                                                                                          g.id is not NULL order by gr.date_added desc";
                                                                                        $rs_growers = mysqli_query($con, $sel_check);
                                                                                        $rs_growers1 = mysqli_query($con, $sel_check);
                                                                                        $totalio = mysqli_num_rows($rs_growers);

                                                                                        $sel_check_1 = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag from grower_offer_reply gr 
                                                                                        left join growers g on gr.grower_id=g.id 
                                                                                        left join country cr on g.country_id=cr.id
                                                                                        where gr.offer_id='" . $producs["cartid"] . "' and 
                                                                                        gr.buyer_id='" . $userSessionID . "' and 
                                                                                        g.id is not NULL GROUP BY gr.grower_id 
                                                                                        order by gr.date_added desc";
                                                                                        $rs_growers_1 = mysqli_query($con, $sel_check_1);
                                                                                        $totalio_gro = mysqli_num_rows($rs_growers_1);

                                                                                        ?>
                                                                                        <tr>
                                                                                            <td class="text-center"><a data-toggle="modal" data-target=".flower_smal_<?php echo $i; ?>"><img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="60"></a>
                                                                                                <!--Pop Up-->
                                                                                                <div class="modal fade flower_smal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                                                                    <div class="modal-dialog modal-sm">
                                                                                                        <div class="modal-content">
                                                                                                            <!-- header modal -->
                                                                                                            <div class="modal-header">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                                                                <h4 class="modal-title" id="mySmallModalLabel">Flower Name</h4>
                                                                                                            </div>

                                                                                                            <!-- body modal 5-->
                                                                                                            <div class="modal-body">
                                                                                                                <img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="100%">
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Pop Up-->
                                                                                            </td>
                                                                                            <td><?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] . "cm" ?></td>
                                                                                            <td><?= $producs["qty"] ?><?php
                                                                                                if ($producs["boxtype"] != "") {
                                                                                                    $temp = explode("-", $producs["boxtype"]);
                                                                                                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                                    $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                                    $box_type = mysqli_fetch_array($rs_box_type);
                                                                                                    echo $box_type["name"];
                                                                                                }
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $bq = '0';
                                                                                                while ($growers_1 = mysqli_fetch_assoc($rs_growers1)) {
                                                                                                    $bq = $growers_1['boxqty'] + $bq;
                                                                                                }
                                                                                                if (!empty($bq) && ($producs["qty"] < $bq || $producs["qty"] == $bq)) {
                                                                                                    echo '0';
                                                                                                } else {
                                                                                                    echo $producs["qty"] - $bq;
                                                                                                }
                                                                                                if ($producs["boxtype"] != "") {
                                                                                                    echo '  ' . $box_type["name"];
                                                                                                }
                                                                                                ?>
                                                                                            </td>
                                                                                            <td><?php
                                                                                                if ($producs["lfd2"] == '0000-00-00') {
                                                                                                    echo date("F j, Y", strtotime($producs["lfd"]));
                                                                                                } else {
                                                                                                    echo date("F j, Y", strtotime($producs["lfd"])) . " ";
                                                                                                }
                                                                                                ?></td>
                                                                                            
                                                                                            <td><?= $producs["cod_order"] ?> </td>
                                                                                            
                                                                                            <td><a data-toggle="modal" data-target=".open_offer_modal<?php echo $i; ?>" class="btn btn-success btn-xs relative">
                                                                                                    Offer<span class="badge badge-dark badge-corner radius-0"><?php echo $totalio_gro; ?></span>
                                                                                                </a>
                                                                                                <!--Offer Modal Start-->
                                                                                                <div class="modal fade open_offer_modal<?php echo $i; ?>" tabindex="-1" role="dialog"
                                                                                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                                                    <div class="modal-dialog modal-lg">
                                                                                                        <div class="modal-content">
                                                                                                            <!-- header modal -->
                                                                                                            <div class="modal-header">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                                    <span aria-hidden="true">&times;</span>
                                                                                                                </button>
                                                                                                                <h4 class="modal-title"
                                                                                                                    id="myLargeModalLabel"><?php echo $producs['qty'] . " " . $box_type["name"]; ?>
                                                                                                                    Boxes <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?>
                                                                                                                    cm</h4>
                                                                                                            </div>
                                                                                                            <!-- body modal 6-->
                                                                                                            <div class="modal-body">
                                                                                                                <div class="table-responsive">
                                                                                                                    <table class="table table-hover">
                                                                                                                        <thead>
                                                                                                                        <tr>
                                                                                                                            <th>Grower</th>
                                                                                                                            <th>Size</th>
                                                                                                                            <th>Stems offered</th>
                                                                                                                            <th>Confirm</th>
                                                                                                                        </tr>
                                                                                                                        </thead>
                                                                                                                        <tbody>
                                                                                                                        <?php
                                                                                                                        $al_ready_offer = array();
                                                                                                                        $cn = 2;
                                                                                                                        $bt = 2;
                                                                                                                        while ($growers = mysqli_fetch_assoc($rs_growers)) {
                                                                                                                            //echo "<pre>";print_r($growers);echo "</pre>";


                                                                                                                            if ($growers["shipping_method"] > 0) {
                                                                                                                                $total_shipping = 0;
                                                                                                                                $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";
                                                                                                                                $rs_connections = mysqli_query($con, $sel_connections);
                                                                                                                                while ($connections = mysqli_fetch_array($rs_connections)) {
                                                                                                                                    if ($connections["type"] == 2) {
                                                                                                                                        $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);
                                                                                                                                    } else if ($connections["type"] == 4) {
                                                                                                                                        $total_shipping = $total_shipping + round(($growers["box_volumn"] * $connections["shipping_rate"]), 2);
                                                                                                                                    } else if ($connections["type"] == 1) {
                                                                                                                                        $box_type_calc = $producs["boxtype"];

                                                                                                                                        if ($box_type_calc == 'HB') {
                                                                                                                                            $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                                                                                                                                            $connections["price_per_box"] = $connections["price_per_box"] / 2;
                                                                                                                                        }
                                                                                                                                        if ($box_type_calc == 'JUMBO') {
                                                                                                                                            $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                                                                                                                                            $connections["price_per_box"] = $connections["price_per_box"] / 2;
                                                                                                                                        } else if ($box_type_calc = 'QB') {
                                                                                                                                            $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4;
                                                                                                                                            $connections["price_per_box"] = $connections["price_per_box"] / 4;
                                                                                                                                        } else if ($box_type_calc = 'EB') {
                                                                                                                                            $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8;
                                                                                                                                            $connections["price_per_box"] = $connections["price_per_box"] / 8;
                                                                                                                                        }
                                                                                                                                        $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];
                                                                                                                                        if ($growers["box_weight"] <= $connections["box_weight_arranged"]) {
                                                                                                                                            $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);
                                                                                                                                        } else {
                                                                                                                                            $total_shipping = $total_shipping + round(($connections["box_weight_arranged"] * $connections["shipping_rate"]), 2);
                                                                                                                                            $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"], 2);
                                                                                                                                            $total_shipping = $total_shipping + round(($remaining * $connections["addtional_rate"]), 2);
                                                                                                                                        }
                                                                                                                                    } else if ($connections["type"] == 3) {
                                                                                                                                        $total_shipping = $total_shipping + $connections["box_price"];
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                            $k = explode("/", $growers["file_path5"]);
                                                                                                                            ?>
                                                                                                                            <?php
                                                                                                                            if (in_array($growers['grower_id'], $al_ready_offer)) {
                                                                                                                                if ($growers["offer_id_index"] == $cn) {
                                                                                                                                    ?>
                                                                                                                                    <tr class="set_border">
                                                                                                                                    <?php
                                                                                                                                    $cn++;
                                                                                                                                } else { ?>
                                                                                                                                    <tr class="not_set_border">
                                                                                                                                <?php }
                                                                                                                                ?>

                                                                                                                            <?php } else { ?>
                                                                                                                                <tr class="set_border">
                                                                                                                            <?php } ?>

                                                                                                                            <td>
                                                                                                                                <?php
                                                                                                                                if (!in_array($growers['grower_id'], $al_ready_offer)) {
                                                                                                                                    ?>
                                                                                                                                    <img
                                                                                                                                    src="<?php echo SITE_URL; ?>user/logo/<?php echo $k[1]; ?>"
                                                                                                                                    width="50"><?php //echo $growers["growers_name"]; ?>
                                                                                                                                <?php }
                                                                                                                                ?>
                                                                                                                            </td>
                                                                                                                            <?php
                                                                                                                            $getbunchSize = "SELECT *  FROM grower_product_bunch_sizes WHERE grower_id = '" . $growers['grower_id'] . "' AND product_id = '" . $producs['product'] . "' AND sizes = '" . $producs['sizeid'] . "'";
                                                                                                                            //echo $getbunchSize;
                                                                                                                            $rs_bunchSize = mysqli_query($con, $getbunchSize);
                                                                                                                            $row_bunchSize = mysqli_fetch_array($rs_bunchSize);
                                                                                                                            $bunch_size_s = "";
                                                                                                                            if ($row_bunchSize['bunch_value'] == 0) {
                                                                                                                                $bunch_size_s = $row_bunchSize['is_bunch_value'];
                                                                                                                            } else {
                                                                                                                                $bunch_size_s = $row_bunchSize['bunch_value'];
                                                                                                                            }
                                                                                                                            ?>
                                                                                                                            <td><?= $growers["product"] ?> <?= $growers["size"]." cm." ?> <?php echo $bunch_size_s . "st/bu"; ?>
                                                                                                                                $<?php echo $growers['price']; ?></td>
                                                                                                                            

                                                                                                                            <td>
                                                                                                                                <?php

                                                                                                                                if (!in_array($growers['grower_id'], $al_ready_offer)) {
                                                                                                                                    ?>

                                                                                                                                    <?= $producs["qty"] ?><?php
                                                                                                                                    if ($producs["boxtype"] != "") {
                                                                                                                                        $temp = explode("-", $producs["boxtype"]);
                                                                                                                                        $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                                                                        $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                                                                        $box_type = mysqli_fetch_array($rs_box_type);
                                                                                                                                        echo $box_type["name"];
                                                                                                                                    }
                                                                                                                                    ?>

                                                                                                                                <?php } ?>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <?php
                                                                                                                                if (!in_array($growers['grower_id'], $al_ready_offer)) {

                                                                                                                                    if ($growers["reject"] != 1) {
                                                                                                                                        $tdate2 = date("Y-m-d");
                                                                                                                                        $date12 = date_create($grower_offer["date_added"]);
                                                                                                                                        $date22 = date_create($tdate2);
                                                                                                                                        $interval = $date22->diff($date12);
                                                                                                                                        $checka1 == $interval->format('%R%a');;

                                                                                                                                        $expired = 0;

                                                                                                                                        if ($checka1 <= 2) {

                                                                                                                                            $sel_buyer_info = "select live_days from buyers where id='" . $grower_offer["buyer_id"] . "'";
                                                                                                                                            $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                                                                                                            $buyer_info = mysqli_fetch_array($rs_buyer_info);

                                                                                                                                            if ($buyer_info["live_days"] > 0) {
                                                                                                                                                $tdate = date("Y-m-d");
                                                                                                                                                $tdate;
                                                                                                                                                $date1 = date_create($producs['lfd']);
                                                                                                                                                $date2 = date_create($tdate);
                                                                                                                                                $interval = $date2->diff($date1);
                                                                                                                                                $checka2 = $interval->format('%R%a');

                                                                                                                                                if ($checka2 == 0) {
                                                                                                                                                    $expired = 1;
                                                                                                                                                }

                                                                                                                                                if ($checka2 >= $buyer_info["live_days"]) {
                                                                                                                                                    $expired = 0;
                                                                                                                                                } else {
                                                                                                                                                    $expired = 1;
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            $expired = 1;
                                                                                                                                        }
                                                                                                                                        if ($growers["status"] == 1) {
                                                                                                                                            ?>
                                                                                                                                            <a href="javascript:void(0);"
                                                                                                                                               class="btn btn-success btn-xs relative"
                                                                                                                                               style="border:0px solid #fff;">Confirmed</a>
                                                                                                                                        <?php } else if ($growers["status"] == 0) {
                                                                                                                                            if ($info["active"] == 'suspended') {
                                                                                                                                                if ($expired == 0) {
                                                                                                                                                    ?>
                                                                                                                                                    <a href="<?php echo SITE_URL; ?>suspend.php?id=<?= $info["id"] ?>" class="btn btn-success btn-xs relative"
                                                                                                                                                       style="border:0px solid #fff;">Add to Cart</a>

                                                                                                                                                <?php } else { ?>
                                                                                                                                                    <span
                                                                                                                                                            style=" font-size:18px; font-size:bold; display:block; color: #FF0000;">Bid closed</span>
                                                                                                                                                    <?php
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                if ($expired == 0) {
                                                                                                                                                    ?>
                                                                                                                                                    <a href="<?php echo SITE_URL; ?>/my-offers.php?offerid=<?= $producs["cartid"] ?>&replyid=<?= $growers["grid"] ?>"
                                                                                                                                                       class="btn btn-success btn-xs relative"
                                                                                                                                                       style="border:0px solid #fff;"
                                                                                                                                                       onClick="return confirm('Are you sure you want to accept this offer ?')">Add to Cart</a>
                                                                                                                                                <?php } else { ?>
                                                                                                                                                    <span
                                                                                                                                                            style=" font-size:18px; font-size:bold; display:block; color: #FF0000;">Bid closed</span>
                                                                                                                                                    <?php
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            ?>
                                                                                                                                            <span
                                                                                                                                                    style=" font-size:10px; font-size:normal; display:block; color:#333">Accept Date</span>
                                                                                                                                            <?php
                                                                                                                                            $temp_offer_date = explode("-", $growers["accept_date"]);
                                                                                                                                            $temp_offer = $temp_offer_date[1] . "-" . $temp_offer_date[2] . "-" . $temp_offer_date[0];
                                                                                                                                            echo $temp_offer;
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        ?>
                                                                                                                                        <a class="btn btn-danger btn-xs relative"
                                                                                                                                           style="border:0px solid #fff;">Rejected
                                                                                                                                            : <?= $growers["reason"] ?></a>
                                                                                                                                    <?php } ?>


                                                                                                                                <?php } else {
                                                                                                                                    if ($growers["offer_id_index"] == $bt) {
                                                                                                                                        if ($growers["reject"] != 1) {
                                                                                                                                            $tdate2 = date("Y-m-d");
                                                                                                                                            $date12 = date_create($grower_offer["date_added"]);
                                                                                                                                            $date22 = date_create($tdate2);
                                                                                                                                            $interval = $date22->diff($date12);
                                                                                                                                            $checka1 == $interval->format('%R%a');;

                                                                                                                                            $expired = 0;

                                                                                                                                            if ($checka1 <= 2) {

                                                                                                                                                $sel_buyer_info = "select live_days from buyers where id='" . $grower_offer["buyer_id"] . "'";
                                                                                                                                                $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                                                                                                                $buyer_info = mysqli_fetch_array($rs_buyer_info);

                                                                                                                                                if ($buyer_info["live_days"] > 0) {
                                                                                                                                                    $tdate = date("Y-m-d");
                                                                                                                                                    $tdate;
                                                                                                                                                    $date1 = date_create($producs['lfd']);
                                                                                                                                                    $date2 = date_create($tdate);
                                                                                                                                                    $interval = $date2->diff($date1);
                                                                                                                                                    $checka2 = $interval->format('%R%a');

                                                                                                                                                    if ($checka2 == 0) {
                                                                                                                                                        $expired = 1;
                                                                                                                                                    }

                                                                                                                                                    if ($checka2 >= $buyer_info["live_days"]) {
                                                                                                                                                        $expired = 0;
                                                                                                                                                    } else {
                                                                                                                                                        $expired = 1;
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                $expired = 1;
                                                                                                                                            }
                                                                                                                                            if ($growers["status"] == 1) {
                                                                                                                                                ?>
                                                                                                                                                <a href="javascript:void(0);"
                                                                                                                                                   class="btn btn-success btn-xs relative"
                                                                                                                                                   style="border:0px solid #fff;">Confirmed</a>
                                                                                                                                            <?php } else if ($growers["status"] == 0) {
                                                                                                                                                if ($info["active"] == 'suspended') {
                                                                                                                                                    if ($expired == 0) {
                                                                                                                                                        ?>
                                                                                                                                                        <a href="<?php echo SITE_URL; ?>suspend.php?id=<?= $info["id"] ?>"
                                                                                                                                                           class="btn btn-success btn-xs relative"
                                                                                                                                                           style="border:0px solid #fff;">Add
                                                                                                                                                            to
                                                                                                                                                            Cart</a>

                                                                                                                                                    <?php } else { ?>
                                                                                                                                                        <span
                                                                                                                                                                style=" font-size:18px; font-size:bold; display:block; color: #FF0000;">Bid closed</span>
                                                                                                                                                        <?php
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    if ($expired == 0) {
                                                                                                                                                        ?>
                                                                                                                                                        <a href="<?php echo SITE_URL; ?>my-offers.php?offerid=<?= $producs["cartid"] ?>&replyid=<?= $growers["grid"] ?>"
                                                                                                                                                           class="btn btn-success btn-xs relative"
                                                                                                                                                           style="border:0px solid #fff;"
                                                                                                                                                           onClick="return confirm('Are you sure you want to accept this offer ?')">Add
                                                                                                                                                            to
                                                                                                                                                            Cart</a>
                                                                                                                                                    <?php } else { ?>
                                                                                                                                                        <span
                                                                                                                                                                style=" font-size:18px; font-size:bold; display:block; color: #FF0000;">Bid closed</span>
                                                                                                                                                        <?php
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                ?>
                                                                                                                                                <span
                                                                                                                                                        style=" font-size:10px; font-size:normal; display:block; color:#333">Accept Date</span>
                                                                                                                                                <?php
                                                                                                                                                $temp_offer_date = explode("-", $growers["accept_date"]);
                                                                                                                                                $temp_offer = $temp_offer_date[1] . "-" . $temp_offer_date[2] . "-" . $temp_offer_date[0];
                                                                                                                                                echo $temp_offer;
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            ?>
                                                                                                                                            <a class="btn btn-danger btn-xs relative"
                                                                                                                                               style="border:0px solid #fff;">Rejected
                                                                                                                                                : <?= $growers["reason"] ?></a>
                                                                                                                                        <?php }
                                                                                                                                        $bt++;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                            </td>
                                                                                                                            </tr>
                                                                                                                            <?php
                                                                                                                            array_push($al_ready_offer, $growers['grower_id']);
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Offer Modal End-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $i++;
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Offer's Modal End-->
                                                        <!-- 25-10-2016 -->

                                                        <ul class="list-inline categories nomargin text-muted size-11 hidden-xs">
                                                            <li><?php
                                                                $get_ind_offer = "SELECT id total_offer_grower from grower_offer_reply where offer_id='" . $row_s_request['buyer_request_id'] . "' GROUP BY offer_id";
                                                                //echo $get_ind_offer;
                                                                $result_s_k = mysqli_query($con, $get_ind_offer);
                                                                $row_offfer_value = mysqli_fetch_array($result_s_k);
                                                                $row_total_offer = mysqli_num_rows($result_s_k);
                                                                if ($row_total_offer != "")
                                                                    echo $row_total_offer;
                                                                else
                                                                    echo "0";
                                                                ?> Grower Offers
                                                            </li>
                                                        </ul>

                                                    </h4>
                                                </td>
                                                <td class="hidden-xs">
                                                <!--<a href="#" data-toggle="modal" data-target=".status_modal_<?php echo $index; ?>">Status</a>-->
                                                    <a href="#" data-toggle="modal" data-target=".ofers_modal_<?php echo $index; ?>"><?php echo $row_s_request['del_date']; ?></a>
                                                    <!--Click on Status and Modal Open Start-->
                                                    <div class="modal fade status_modal_<?php echo $index; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-full">
                                                            <div class="modal-content">

                                                                <!-- header modal -->
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Ãƒâ€�?</span></button>
                                                                    <h4 class="modal-title" id="myLargeModalLabel">Title Here</h4>
                                                                </div>

                                                                <!-- body modal 7-->
                                                                <div class="modal-body">
                                                                    <div id="overview" class="tab-pane active">
                                                                        <!-- COMMENT -->
                                                                        <?php

                                                                        $i = 1;
                                                                        $query2 = "select gpb.*, 
                                                                                          gpb.id as cartid , gpb.date_added , p.id          , p.name             , 
                                                                                          p.color_id        , p.image_path   , s.name as subs,sh.name as sizename ,
                                                                                          ff.name as featurename 
                                                                                     from buyer_requests gpb
                                                                                     left join product p on gpb.product = p.id
                                                                                     left join subcategory s on p.subcategoryid=s.id  
                                                                                     left join features ff on gpb.feature=ff.id
                                                                                     left join sizes sh on gpb.sizeid=sh.id 
                                                                                    where gpb.buyer='" . $userSessionID . "' 
                                                                                      and gpb.id='" . $row_s_request['buyer_request_id'] . "' 
                                                                                      and ( gpb.type!=1 and gpb.type!=7 and gpb.type!=5 )  
                                                                                      and gpb.lfd>='" . date("Y-m-d") . "' 
                                                                                    order by gpb.id desc";
                                                                        
                                                                        
                                                                        $result2 = mysqli_query($con, $query2);
                                                                        $tp = mysqli_num_rows($result2);
                                                                        if ($tp >= 1) {
                                                                            while ($producs = mysqli_fetch_array($result2)) {
                                                                                ?>
                                                                                <ul class="comment list-unstyled">
                                                                                    <li class="comment">

                                                                                        <!-- avatar -->
                                                                                        <img class="avatar" src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="avatar" width="50" height="50">

                                                                                        <!-- comment body -->
                                                                                        <div class="comment-body cls_comment_body">
                                                                                            <a href="#" class="comment-author">
                                                                                                <small class="text-muted pull-right" style="display: none;">12 Minutes ago</small>
                                                                                                <?php
                                                                                                if ($producs["boxtype"] != "") {
                                                                                                    $temp = explode("-", $producs["boxtype"]);
                                                                                                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                                    $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                                    $box_type = mysqli_fetch_array($rs_box_type);
                                                                                                    $bx_type_s = $box_type["name"];
                                                                                                }
                                                                                                $current_date_s = date("Y-m-d");
                                                                                                $str_current_date = strtotime($current_date_s);
                                                                                                $str_lfd = strtotime($producs['lfd']);
                                                                                                $status_s = "Open Bid";
                                                                                                if ($str_current_date > $str_lfd) {
                                                                                                    $status_s = "Closed Bid";
                                                                                                }
                                                                                                ?>
                                                                                                <span><?php echo $producs["qty"] . " " . $bx_type_s; ?>  <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?>
                                                                                                    cm <?php echo $status_s; ?></span>
                                                                                            </a>
                                                                                            <p>
                                                                                                <?php
                                                                                                if ($producs["comment"] != "") {
                                                                                                    echo $producs["comment"];
                                                                                                } else {
                                                                                                    echo "-";
                                                                                                }
                                                                                                ?>
                                                                                            </p>
                                                                                        </div><!-- /comment body -->
                                                                                        <!-- options -->
                                                                                        <ul class="list-inline size-11 margin-top-10">
                                                                                            <li>
                                                                                                <a href="#" class="text-info" data-toggle="modal"
                                                                                                   data-target=".msg_all_grower_2"><i class="fa fa-reply"></i>Message all growers</a>
                                                                                                <!--Message All Grower Modal Start-->
                                                                                                <div class="modal fade msg_all_grower_2" tabindex="-1"
                                                                                                     role="dialog" aria-labelledby="mySmallModalLabel"
                                                                                                     aria-hidden="true">
                                                                                                    <div
                                                                                                            class="modal-dialog modal-sm">
                                                                                                        <div
                                                                                                                class="modal-content">

                                                                                                            <!-- header modal -->
                                                                                                            <div class="modal-header">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                                    <span aria-hidden="true">Ãƒâ€�?</span>
                                                                                                                </button>
                                                                                                                <h4 class="modal-title" id="mySmallModalLabel">Message AllGrowers</h4>
                                                                                                            </div>

                                                                                                            <!-- body modal 8-->
                                                                                                            <div class="modal-body">
                                                                                                                <div class="input-group">
                                                                                                                    <input id="btn-input" class="form-control" placeholder="Type your message..."
                                                                                                                           type="text">
                                                                                                                    <span class="input-group-btn">
                                                    <button class="btn btn-primary" id="btn-chat">
                                                        <i class="fa fa-envelope-o"></i> Send
                                                    </button> 
                                                </span>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Message All Grower Modal End-->
                                                                                            </li>
                                                                                            <li>
                                                                                                <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i>Confirmed orders</a>
                                                                                            </li>

                                                                                            <li class="pull-right">
                                                                                                <a href="#"
                                                                                                   class="text-danger">Delete</a>
                                                                                            </li>
                                                                                            <li class="pull-right">
                                                                                                <a href="#"
                                                                                                   class="text-primary">Edit</a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li><!-- /options -->
                                                                                    <?php
                                                                                    $grower_reply_sql = "SELECT gor.*,g.growers_name,g.file_path5,g.profile_image 
                                                                 FROM grower_offer_reply as gor 
                                                                 INNER JOIN growers as g ON g.id =  gor.grower_id    
                                                                 where offer_id='" . $producs['cartid'] . "' AND gor.status=0";
                                                                                    //echo $grower_reply_sql;
                                                                                    $grower_result = mysqli_query($con, $grower_reply_sql);
                                                                                    while ($grower_info = mysqli_fetch_array($grower_result)) {
                                                                                        //echo "<pre>";print_r($grower_info);echo "</pre>";
                                                                                        ?>
                                                                                        <li class="comment comment-reply ">

                                                                                            <!-- avatar -->
                                                                                            <img class="avatar"
                                                                                                 src="<?php echo SITE_URL; ?>/user/<?php echo $grower_info['file_path5']; ?>"
                                                                                                 alt="avatar" width="50"
                                                                                                 height="40">

                                                                                            <!-- comment body -->
                                                                                            <div class="comment-body cls_comment_body">
                                                                                                <a href="#"
                                                                                                   class="comment-author">
                                                                                                    <small class="text-muted pull-right" style="display: none;">Delivery Date: 24th July 2016</small>
                                                                                                    <span><?php echo $grower_info['growers_name']; ?></span>
                                                                                                </a>
                                                                                                <p>
                                                                                                    <?php echo $grower_info['boxqty'] . " " . $grower_info['boxtype'] . " " . $grower_info['product'] . " " . $grower_info['size'] . " @ " . $grower_info['price']; ?>
                                                                                                </p>
                                                                                            </div><!-- /comment body -->

                                                                                            <!-- options -->
                                                                                            <ul class="list-inline size-11">
                                                                                                <li><a href="#" class="text-success"><i class="fa fa-thumbs-up"></i>Confirmed</a></li>
                                                                                                <li class="pull-right">
                                                                                                    <a href="#" class="text-danger">Cancel
                                                                                                        Order</a>
                                                                                                </li>
                                                                                                <li class="pull-right">
                                                                                                    <a href="#"
                                                                                                       class="text-primary">Message
                                                                                                        Grower</a>
                                                                                                </li>
                                                                                            </ul><!-- /options -->

                                                                                        </li>
                                                                                    <?php }
                                                                                    ?>
                                                                                </ul>
                                                                                <!-- /COMMENT -->
                                                                            <?php }
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>

                                                                <!-- Modal Footer -->
                                                                <div class="modal-footer" style="display: none;">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="button" class="btn btn-primary">Save
                                                                        changes
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Click on Status and Modal End-->
                                                </td>
                                                <td class="text-center hidden-xs">
                                                    <a href="#" data-toggle="modal"
                                                       data-target=".add_product_modal_<?php echo $index; ?>">Add
                                                        Product</a>
                                                    <!--Add Product Modal Start-->
                                                    <div class="modal fade add_product_modal_<?php echo $index; ?>"
                                                         tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">

                                                                <!-- header modal -->
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">Ãƒâ€�?</span></button>
                                                                    <h4 class="modal-title" id="mySmallModalLabel">Small
                                                                        modal</h4>
                                                                </div>

                                                                <!-- body modal 9-->
                                                                <div class="modal-body">
                                                                    <a href="<?php echo SITE_URL; ?>buyer/name-your-price.php?buyer_order_id=121"
                                                                       class="btn btn-3d btn-xlg btn-purple margin-bottom-10">
                                                                        REQUEST FOR PRODUCT
                                                                        <span class="block font-lato">Let Fresh Life Floral saves you</span>
                                                                        <span class="block font-lato">time and money!</span>
                                                                    </a>
																
                                                                    <a href="<?php echo SITE_URL; ?>/buyer/inventory.php?id=<?php echo $row_s_request['id']; ?>"
                                                                       class="btn btn-3d btn-xlg btn-purple margin-bottom-10">
                                                                        AVAILABILITY
                                                                        <span class="block font-lato">Growers Inventory</span>
                                                                    </a>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Add Product Modal End-->
                                                </td>
                                            </tr>
                                            <?php
                                            $index++;
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="5">No Results Found.....</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                            
                            
                            
                            <!-- Modal Edit #1 pestaÃ±a -->
                            
                            <div id="edit" class="tab-pane">
                                <form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" onsubmit="javascript: return validate_profile_form(profile_form);">
                                    
                                    <h4>Personal Information</h4>
                                    
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_first_name"></div>
                                            <div style="clear:both;"></div>
                                            <label class="col-md-3 control-label" for="first_name"><?php echo $labels_arr['first_name']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $first_name; ?>" onkeypress="javascript:remove_validate_text('first_name');">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_last_name"></div>
                                            <div style="clear:both;"></div>
                                            <label class="col-md-3 control-label"
                                                   for="last_name"><?php echo $labels_arr['last_name']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="last_name"
                                                       value="<?php echo $last_name; ?>" name="last_name"
                                                       onkeypress="javascript:remove_validate_text('last_name');">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="company"><?php echo $labels_arr['company']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="company" name="company" value="<?php echo $company; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="company-role">Company
                                                Role</label>
                                            <div class="col-md-8">
                                                <select id="company_role" class="form-control" name="company_role">
                                                    <option value="">-Select-</option>
                                                    <option value="buyer" <?php if ($company_role == "buyer") echo "SELECTED"; ?>>Buyer</option>
                                                    <option value="ceo" <?php if ($company_role == "ceo") echo "SELECTED"; ?>>CEO</option>
                                                    <option value="owner" <?php if ($company_role == "owner") echo "SELECTED"; ?>>Owner</option>
                                                    <option value="sales" <?php if ($company_role == "sales") echo "SELECTED"; ?>>Sales
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="country"><?php echo $labels_arr['country']; ?></label>
                                            <div class="col-md-8">
                                                <select size="1" name="country" id="country" class="form-control">
                                                    <option value="">Country (required)</option>
                                                    <?php
                                                    if ($stmt = $con->prepare("select id,name,flag from country order by name")) {
                                                        $stmt->bind_param('i', $userSessionID);
                                                        $stmt->execute();
                                                        $stmt->bind_result($cid, $cname, $cflag);
                                                        //echo "<pre>";print_r($country);echo "</pre>";
                                                        while ($stmt->fetch()) {
                                                            $select = '';
                                                            if ($country == $cid) {
                                                                $select = "selected='selected'";
                                                            }
                                                            echo '<option value="' . $cid . '" ' . $select . '>' . $cname . '</option>';
                                                        }
                                                        $stmt->close();
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="address"><?php echo $labels_arr['address1']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="address" id="address"
                                                       value="<?php echo $address; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="address2"><?php echo $labels_arr['address2']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="address2" id="address2" value="<?php echo $address; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="city"><?php echo $labels_arr['city']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="city" name="city"
                                                       value="<?php echo $city; ?>">
                                            </div>
                                        </div>
                                        <?php
                                        $dis_block_other = "none";
                                        $dis_block_us = "none";
                                        //echo "<pre>";print_r($country);echo "</pre>";
                                        if ($country == "240") {
                                            $dis_block_us = "block";
                                        } else {
                                            $dis_block_other = "block";
                                        }
                                        ?>
                                        <div id="other_block" style="display:<?php echo $dis_block_other; ?>">
                                            <div class="form-group" id="us_state">
                                                <label class="col-md-3 control-label"
                                                       for="state_text"><?php echo $labels_arr['state_text']; ?></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="state_text_other" name="state_text_other" value="<?php echo $state_text; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="us_block" style="display:<?php echo $dis_block_us; ?>">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="state_text"><?php echo $labels_arr['province']; ?></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="state_text_us" name="state_text_us" value="<?php echo $state_text; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" id="us_zip">
                                                <label class="col-md-3 control-label" for="zip"><?php echo $labels_arr['zip']; ?></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" maxlength="5" id="zip" name="zip" value="<?php echo $zip; ?>">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="web_site"><?php echo $labels_arr['web_site']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="web_site" name="web_site" value="<?php echo $web_site; ?>">
                                            </div>
                                        </div>

                                    </fieldset>
                                    <hr>

                                    <h4>About</h4>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Public Profile</label>
                                            <div class="col-md-8">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="1"
                                                           <?php if ($is_public) { ?>checked="checked" <?php } ?>
                                                           id="profilePublic" name="is_public">
                                                    <i></i> </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="sky-form">
                                                <label class="col-xs-3 control-label">Profile Image</label>
                                                <div class="col-md-8">
                                                    <label for="file" class="input input-file">
                                                        <div class="button">
                                                            <input type="file" name="profile_image" id="file"
                                                                   onChange="document.getElementById('fake_button').value = this.value">
                                                            Browse
                                                        </div>
                                                        <input type="text" id="fake_button" readonly="">
                                                    </label>
                                                    <?php
                                                    if ($profile_image) {
                                                        echo '<a href="buyers-account.php?delete_avatar=1&id=' . $userID . '" class="btn btn-danger btn-xs nomargin"><i class="fa fa-times"></i> Remove Current Image</a> </div>';
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($profile_image) {
                                                        ?>
                                                        <label class="col-xs-3 control-label">&nbsp;</label>
                                                        <div class="col-md-8">
                                                            <img class="img-responsive"
                                                                 style="width:100px;height:80px;margin-top:10px;" alt=""
                                                                 src="/images/profile_images/<?php echo $profile_image; ?>">
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <hr>
                                    <h4>Change Password</h4>
                                    <fieldset class="mb-xl">
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_password"></div>
                                            <div style="clear:both;"></div>
                                            <label class="col-md-3 control-label" for="profileNewPassword">New
                                                Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="password"
                                                       onkeyup="javascript:check_password('password');"
                                                       class="form-control" id="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_repeat_password"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="profileNewPasswordRepeat">Repeat
                                                New Password</label>
                                            <div class="col-md-8">
                                                <input type="password" name="repeat_password" id="repeat_password"
                                                       onkeyup="javascript:check_password('repeat_password');"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3">
                                             <input type="hidden" name="id"                value="<?php echo $userID; ?>">
                                             <input type="hidden" name="profile_image_old" value="<?php echo $profile_image; ?>">
                                             <input type="hidden" name="update_profile"    value="1">                                            
                                            <button type="submit" class="btn btn-primary">Submit 1</button>
                                            <button type="reset"  class="btn btn-default">Reset</button>
                                        </div>
                                    </div>
                                 
                                    
                                </form>
                            </div>
                                        
                                        
                            <!-- Contribution -->
                            <div id="contributions" class="tab-pane">
                                <form class="contributions_from form-horizontal" action="" id="contributions_from"
                                      enctype="multipart/form-data" method="POST">
                                    <div class="step_1">
                                        <h4>Please select a grower from the list, if the grower you will write about is
                                            not on the list just write down it's name on the next field</h4>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Choose Growers Name</label>
                                                <div class="col-md-8">
                                                    <div class="autosuggest" data-minLength="1"
                                                         data-queryURL="php/view/demo.autosuggest.php?limit=10&search=">
                                                        <!-- <input type="text" name="src" placeholder="Find Growers" class="form-control typeahead" required/> -->
                                                        <?php
                                                        $auto_growers = "SELECT id,growers_name from growers order by growers_name";
                                                        $rs_auto = mysqli_query($con, $auto_growers);
                                                        $product_array = array();
                                                        ?>
                                                        <!--cls_filter-->
                                                        <!--form-control-->

                                                        <select class="form-control select2 cls_filter " style="width: 100%; display: none;" id="grower_id" name="grower_id" tabindex="-1">
                                                            <option value="">--Select--</option>
                                                            <?php
                                                            while ($row_auto = mysqli_fetch_array($rs_auto)) {
                                                                ?>
                                                                <option
                                                                        value="<?php echo $row_auto['id']; ?>"><?php echo  $row_auto['growers_name']; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>

                                                        <input type="hidden" id="grower_name" name="grower_name"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">Type Grower's Name(if not found)</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="custom_growername"
                                                           name="custom_growername">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-9 col-md-offset-3">
                                                    <a href="javascript:void(0);"
                                                       class="btn btn-primary next_1">Next</a>

                                                </div>
                                            </div>
                                    </div>

                                    <div class="step_2">
                                        <h4>Growers Review</h4>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Quality Rating</label>
                                                <div class="col-md-8 top_7">
                                                    <input id="quality_rating" name="quality_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">

                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Freshness&nbsp;Rating</label>
                                                <div class="col-md-8 top_7">
                                                    <input id="freshness_rating" name="freshness_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">

                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Trustworthiness Rating</label>
                                                <div class="col-md-8 top_7">
                                                    <input id="trustworthiness_rating" name="trustworthiness_rating"
                                                           type="hidden" class="rating" min="0" max="5" step="0.5"
                                                           data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false"
                                                           data-rating-class="rating-fa">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Pricing Rating</label>
                                                <div class="col-md-8 top_7">
                                                    <input id="pricing_rating" name="pricing_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Packing Rating</label>
                                                <div class="col-md-8 top_7">
                                                    <input id="packing_rating" name="packing_rating" type="hidden" value="0" class="rating" min="0" max="5" step="0.5"
                                                           data-size="sm" data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">

                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Write Review</label>
                                                <div class="col-md-8 top_7">
                                                    <div class="fancy-form">
                                                        <textarea rows="5" class="form-control word-count" name="comment" data-maxlength="200" data-info="textarea-words-info"
                                                                  placeholder="Write your review"></textarea>
                                                        <i class="fa fa-comments"><!-- icon --></i>
                                                        <span class="fancy-hint size-11 text-muted">
                                                            <strong>Hint:</strong> 200 words allowed!
                                                            <span class="pull-right">
                                                                <span id="textarea-words-info">0/200</span> Words
                                                            </span>
                                                        </span>

                                                    </div>

                                                </div>
                                            </div>
                                        </fieldset>


                                        <div class="row">
                                            <div class="col-md-9 col-md-offset-3">
                                                <a class="btn btn-primary back_1" href="#">Back</a>
                                                <a class="btn btn-primary next_2" href="#">Next</a>

                                            </div>
                                        </div>
                                    </div>
                                    <!--Review-->
                                    <!--Drop-->
                                    <div class="step_3">
                                        <h4>Please Upload and invoice from a recent purchase</h4>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">Select
                                                    market type</label>
                                                <div class="col-md-8 ">
                                                    <select name="type" class="form-control" name="recent_purchase" id="recent_purchase">
                                                        <option value="">Please Select market type</option>
                                                        <option value="1">Open Market</option>
                                                        <option value="0">Standing Order</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label for="profileFirstName" class="col-md-3 control-label">Please
                                                    Upload and invoice from a recent purchase</label>
                                                <div class="col-md-8 ">
                                                    <div class="fancy-file-upload">
                                                        <i class="fa fa-upload"></i>
                                                        <input type="file"
                                                               onchange="jQuery(this).next('input').val(this.value);"
                                                               name="file_recent_purchase" id="file_recent_purchase"
                                                               class="form-control">
                                                        <input type="text" id="recent_file_temp" readonly=""
                                                               placeholder="no file selected" class="form-control">
                                                        <span class="button">Choose File</span>
                                                    </div>

                                                </div>
                                            </div>
                                        </fieldset>

                                        <div class="row">
                                            <div class="col-md-9 col-md-offset-3">
                                                <a class="btn btn-primary back_2" href="#">Back</a>
                                                <a class="btn btn-primary next_3" href="#">Next</a>

                                            </div>
                                        </div>
                                    </div>
                                    <!--Drop-->
                                    <!--Drop2-->
                                    <div class="step_4">
                                        <h4>Please upload an updated variety list for this grower</h4>
                                        <fieldset>
                                            <div class="form-group">
                                                <label for="profileFirstName" class="col-md-3 control-label">Please
                                                    upload an updated variety list for this grower</label>
                                                <div class="col-md-8 ">
                                                    <div class="fancy-file-upload">
                                                        <i class="fa fa-upload"></i>
                                                        <input type="file"
                                                               onchange="jQuery(this).next('input').val(this.value);"
                                                               required name="file_updated_var" id="file_updated_var"
                                                               class="form-control">
                                                        <input type="text" id="temp_file_updated" readonly=""
                                                               placeholder="no file selected" class="form-control">
                                                        <span class="button">Choose File</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-md-9 col-md-offset-3">
                                                <a class="btn btn-primary back_3" href="#">Back</a>
                                                <button type="submit" name="submit_contribution" onClick="checkUpdatedVar();" class="btn btn-primary next_4">
                                                    Submit2
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Drop2-->
                                </form>
                            </div>
                            <!-- End of contribution -->
                            <!-- Shipping Start -->
                            <div id="shipping" class="tab-pane">
                                <div class="imgDiv" style="width:100%; text-align: center; display: none;"><img src="<?php echo SITE_URL; ?>/includes/assets/images/loaders/5.gif" id="loaderImg"/></div>

                                <input name="shippingMethodId" type="hidden" id="shippingMethodId" value="0"/>
                                <h4>Shipping Options</h4>
                                <div class="table-responsive">
                                    <table class="table table-hover nomargin">
                                        <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Code</th>
                                            
                                        </tr>
                                        </thead>
                                        
                                        <tbody>
                                        <?php
                                         $i = 1;
                                        $getShippingMethod = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "'";
                                        $shippingMethodRes = mysqli_query($con, $getShippingMethod);
                                        
                                        if (mysqli_num_rows($shippingMethodRes) > 0) {
                                            
                                            while ($shippingMethodData = mysqli_fetch_assoc($shippingMethodRes)) {                                               

                                                if ($shippingMethodData['shipping_method_id'] != 0) {
                                                    $dt = mysqli_query($con, "select * from shipping_method where id =" . $shippingMethodData['shipping_method_id']);
                                                    $sdata = mysqli_fetch_assoc($dt);
                                                    ?>
                                                    <tr id="tr_shipping_row<?php echo $shippingMethodData["id"]; ?>">
                                                        <!--Description-->
                                                        <td><?php echo $sdata['description'] ?></td>
                                                        <!--Code-->
                                                        <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                        <td><a data-toggle="modal" data-target=".coordina_modal" 
                                                               class="btn btn-success btn-xs relative" onclick="<?php echo $shippingMethodData['shipping_code']; ?>">Edit</a>                                                                                                                       
                                                    </tr>
                                                    
                                                    <?php
                                                } else {

                                                    if ($shippingMethodData['own_shipping'] == '1') {
                                                        $getshipping_type = "select * from cargo_agency where id='" . $shippingMethodData['cargo_agency_id'] . "'";
                                                        $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                        $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                        $shippingMethodDataname = $shippingType['name'];
                                                    } else {
                                                        $shipping_type = $shippingMethodData['choose_shipping'];
                                                        if (!empty($shipping_type) && $shipping_type != '') {
                                                            $getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
                                                            echo $getshipping_type;
                                                            $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                            $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                            $shippingMethodDataname = $shippingType['description'];
                                                        } else {
                                                            $shippingMethodDataname = $shippingMethodData['name'];
                                                        }
                                                    }
                                                    ?>
                                                    <tr id="tr_shipping_row<?php echo $shippingMethodData["id"]; ?>">
                                                        <!--Selected-->
                                                        <td>
                                                            <?php

                                                            if ($shippingMethodData['default_shipping'] == 1) {
                                                                ?>
                                                                <input type="radio" name="order_idlsd" value="<?php echo $shippingMethodData['id']; ?>" checked="checked">
                                                                <?php

                                                            } else { ?>
                                                                <input type="radio" name="order_idlsd" value="<?php echo $shippingMethodData['id'] ?>">
                                                            <?php }
                                                            ?>
                                                        </td>
                                                        <!--Descripcion-->
                                                        <td><?php echo $shippingMethodDataname; ?></td>
                                                        <!--Code-->
                                                        <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                        <!--Action-->
                                                        <td>
                                                            <a class="btn btn-default btn-xs" onclick="defaultShipping(<?php echo $shippingMethodData["id"]; ?>,<?php echo $userSessionID; ?>)"
                                                               href="javascript:void(0);"><i  class="fa fa-times white"></i>Select Default</a>
                                                            <a class="btn btn-default btn-xs" onclick="deleteShipping(<?php echo $shippingMethodData["id"]; ?>)"
                                                               href="javascript:void(0);"><i  class="fa fa-times white"></i><?php //echo $shippingMethodData['id'];?>Erase </a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }                                                                                                                                    
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="3">No Shipping Methods Found.....</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                          <!-- Buyer Modal Start Edit 2 -->
                          <form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" onsubmit="javascript: return validate_profile_form(profile_form);">                                
                                <div class="modal fade coordina_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                <h4 class="modal-title" id="myLargeModalLabel"> Coordination </h4>
                                                <div class="modal-body">Coordinacion
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                        <tbody>
                                                  <?php          
                                                $getShipping = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "' and shipping_code='" . $shippingMethodData['shipping_code'] . "' ";
                                                $shippingMethod = mysqli_query($con, $getShipping);   
                                                $MethodRes = mysqli_fetch_assoc($shippingMethod);
                                                
                                                $cargo_id = $MethodRes['cargo_agency_id'];
                                                        $getshipping_type = "select * from cargo_agency where id='" . $cargo_id . "'";                                                        
                                                        $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                        $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                        $cargoAgency = $shippingType['name'];   
                                                        ?>
                                                            
                                                    <fieldset>                                                            
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label" for="cargo">Cargo Agency</label>
                                                                    <div class="col-md-8">
                                                                        <select size="1" name="cargo" id="cargo" class="form-control">
                                                                            <option value="">Cargo Agency (required)</option>
                                                                            <?php
                                                                            if ($stmt = $con->prepare("select id,name,logo from cargo_agency order by name")) {
                                                                                $stmt->bind_param('i', $userSessionID);
                                                                                $stmt->execute();
                                                                                $stmt->bind_result($cid, $cname, $cflag);
                                                                                while ($stmt->fetch()) {
                                                                                    $select = '';
                                                                                    if ($cargo == $cid) {
                                                                                        $select = "selected='selected'";
                                                                                    }
                                                                                    echo '<option value="' . $cid . '" ' . $select . '>' . $cname . '</option>';
                                                                                }
                                                                                $stmt->close();
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="company">Coordination</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control" id="coordination" name="coordination" value="<?php echo $coordination; ?>">
                                                                </div>
                                                            </div>                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="invoice">Invoice Name</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control" id="invoice" name="invoice" value="<?php echo $invoice; ?>">
                                                                </div>
                                                            </div>  

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="box">Box Marks</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control" id="box" name="box" value="<?php echo $company; ?>">
                                                                </div>
                                                            </div>  
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="country">Country</label>
                                                                    <div class="col-md-8">
                                                                        <select size="1" name="country" id="country" class="form-control">
                                                                            <option value="">Country (required)</option>
                                                                            <?php
                                                                            if ($stmt = $con->prepare("select id,name,flag from country order by name")) {
                                                                                $stmt->bind_param('i', $userSessionID);
                                                                                $stmt->execute();
                                                                                $stmt->bind_result($cid, $cname, $cflag);
                                                                                while ($stmt->fetch()) {
                                                                                    $select = '';
                                                                                    if ($country == $cid) {
                                                                                        $select = "selected='selected'";
                                                                                    }
                                                                                    echo '<option value="' . $cid . '" ' . $select . '>' . $cname . '</option>';
                                                                                }
                                                                                $stmt->close();
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="city">City</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>">
                                                                </div>
                                                            </div>                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="address">Street Address</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control" id="address" name="address" value="<?php echo $address; ?>">
                                                                </div>
                                                            </div>  

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" for="phone">Phone Number</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $phone; ?>">
                                                                </div>
                                                            </div>                                                            
                                                        </fieldset>  
                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                                            <input type="hidden" name="update_profile_1" value="1">
                                            <button type="submit" class="btn btn-primary">Submit3</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>   
                                                        
                                                        
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                </form>                              
                                <!-- Buyer Modal End--> 
                                
                                <center>
                                    <hr>
                                    <a href="javascript:void(0)" class="btn btn-3d btn-reveal btn-purple" id="create_new_ship" style="background-color:#923A8D!important;margin-top: 50px;margin-bottom: 50px;"><i class="fa fa-plus"></i><span>Add New Shipping</span></a>
                                </center>
                                <hr>
                                <fieldset id="select_country" style="display: none;">
                                    <h4>Describe your shipping needs</h4>
                                    <div class="form-group">
                                        <form id="profile_forms" name="profile_forms" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>"
                                              onsubmit="javascript: return validate_profile_email(profile_forms);">
                                            <label for="profileFirstName" class="col-md-3 control-label">Affair</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="af" name="af" placeholder="Please write the Affair" onkeypress="javascript:remove_validate_text('af');">
                                                <div class="col-md-8 error error_af"></div>

                                            </div>
                                            <hr>
                                            <hr>
                                            <label for="profileFirstName" class="col-md-3 control-label">Message:</label>
                                            <div class="col-md-9">
                                                <div class="col-md-8 error error_area"></div>
                                                <textarea class="form-control" rows="10" name="area" placeholder="Please write your message...." id="area"></textarea>
                                                This message will be sent to the administrator
                                                <input name="boton" type="submit" value="Send Message" class="btn btn-3d btn-reveal btn-purple" style="background-color:#923A8D!important;margin-top: 50px;margin-bottom: 50px;">

                                            </div>
                                        </form>
                                    </div>
                                </fieldset>
                                <fieldset id="shipping_mod">
                                    <hr>
                                    <div class="form-group">
                                        <h4 class="col-md-8 control-label left_align" for="profileFirstName"> Do you have your own shipping arrangements?</h4>
                                        <div class="col-md-12 top_7 radio"></div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Yes</label>
                                            <div class="col-md-8">
                                                <label class="radio" id="yes_ship">
                                                    <input type="radio" name="own_shipping" value="1" id="yes_ship"><i></i> I handle my cargo agency and shipping options
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">No</label>
                                            <div class="col-md-8">
                                                <label class="radio" id="no_ship"><input type="radio" name="own_shipping" value="0" id="no_ship"><i>
                                                    </i> I would like Fresh LIfe Floral to arrange shipping for me </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="arrange_shipping" style="display:none">
                                    <hr>
                                    <div class="form-group">
                                        <h4 class="col-md-8 control-label left_align"> Would you like Fresh Life to
                                            clear customs and agriculture for you?</h4>
                                        <div class="col-md-12 top_7 radio">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Yes</label>
                                            <div class="col-md-8">
                                                <label class="radio" id="yes_ship_paper">
                                                    <input type="radio" id="profilePublic" name="shiping_need"
                                                           value="1">
                                                    <i></i> Take care of paperwork for me
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">No</label>
                                            <div class="col-md-8">
                                                <label class="radio" id="no_ship_paper">
                                                    <input type="radio" id="profilePublic" name="shiping_need" value="0">
                                                    <i></i> I handle my own customs and USDA papers
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </fieldset>
                                <!--Only if the country is listed on database-->
                                <div class="table-responsive" id="for_non_usa">
                                    <table class="table  nomargin">
                                        <thead>
                                        <tr class="ship_table">
                                            <th>Cargo Agency Logo</th>
                                            <th>Airline logo</th>
                                            <th>Rate per Kilo</th>
                                            <th>Available shipping dates</th>
                                            <th>Miminum Kilos per shipment</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table_body" class="for_non_usa_methods">

                                        </tbody>
                                    </table>
                                    <div id="non_usa_select" style="display:none">
                                        <h4>Choose Address you Preferred</h4>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Profile Address</label>
                                            <div class="col-md-8">
                                                <label class="radio top6" id="local_same_address_non_usa">
                                                    <input type="radio" id="profilePublic" name="add_non_usa"
                                                           value="non_usa_same">
                                                    <i></i>&nbsp; Use my profile information to pick up these boxes
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Different Address</label>
                                            <div class="col-md-8">
                                                <label class="radio top6" id="local_now_same_address_non_usa">
                                                    <input type="radio" id="profilePublic" name="add_non_usa"
                                                           value="non_usa_other">
                                                    <i></i>&nbsp; Use a different address to pick up my boxes
                                                </label>
                                            </div>
                                        </div>
                                        <fieldset id="local_same_show_non_usa" style="display:none">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">First
                                                    Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" readonly=""
                                                           name="non_usa_same_firstname" id="profileFirstName"
                                                           value="<?php echo $first_name; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">Last
                                                    Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" readonly=""
                                                           name="non_usa_same_lastname" id="profileLastName"
                                                           value="<?php echo $last_name; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="profileAddress">Address</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" readonly=""
                                                           name="non_usa_same_address" id="profileAddress"
                                                           value="<?php echo $address; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="profileAddress">State</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" readonly=""
                                                           name="non_usa_same_state" id="profileAddress"
                                                           value="<?php echo $state; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileAddress">Zip</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" readonly="" name="non_usa_same_zip" id="profileAddress" value="<?php echo $zip; ?>">
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset id="local_dif_show_non_usa" style="display:none">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileFirstName">First Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nonFirstName" id="profileFirstName">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">Last Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nonLastName"
                                                           id="profileLastName">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileAddress">Address</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nonAddress" id="profileAddress">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileAddress">State</label>
                                                <div class="col-md-8"><input type="text" class="form-control" name="nonState" id="profileAddress">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="profileAddress">Zip</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="nonZip"
                                                           id="profileAddress">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                </div>
                                <!--Only if the country is listed on database-->
                                <fieldset id="yes_handle_cargo">
                                    <h4>Select Cargo Agency</h4>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="profileFirstName">Select Cargo
                                            Agency </label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="cargo_agency_id"
                                                    id="cargo_agency_id">
                                                <option value="">Select here</option>
                                                <?php
                                                if ($stmt = $con->prepare("select id,name from cargo_agency order by name")) {
                                                    $stmt->bind_param('i', $userSessionID);
                                                    $stmt->execute();
                                                    $stmt->bind_result($cargoId, $cargoName);
                                                    while ($stmt->fetch()) {
                                                        ?>
                                                        <option
                                                                value="<?php echo $cargoId; ?>"><?php echo $cargoName; ?></option>
                                                        <?php
                                                    }
                                                    $stmt->close();
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label left_align" for="profileLastName">Select
                                            leaving Farm Date</label>
                                        <div class="col-md-8 " id="daycheck">
                                            <label class="checkbox">
                                                <input type="checkbox" value="1" name="day[]">
                                                <i></i> Monday
                                            </label><br>
                                            <label class="checkbox">
                                                <input type="checkbox" value="2" name="day[]">
                                                <i></i> Tuesday
                                            </label><br>
                                            <label class="checkbox"><input type="checkbox" value="3" name="day[]"><i></i> Wednesday</label><br>
                                            <label class="checkbox"><input type="checkbox" value="4" name="day[]"><i></i> Thursday</label><br>
                                            <label class="checkbox"><input type="checkbox" value="5" name="day[]"><i></i> Friday</label><br>
                                            <label class="checkbox"><input type="checkbox" value="6" name="day[]"><i></i> Saturday</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label left_align" for="profileLastName">What
                                            names do growers use to coordinate your boxes?</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="grower_box_name"
                                                   id="profileLastName">
                                        </div>
                                    </div>


                                    <div>
                                        <h4>Choose Address you Preferred</h4>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Profile Address</label>
                                            <div class="col-md-8">
                                                <label class="radio top6" id="local_same_address3">
                                                    <input type="radio" id="profilePublic" name="profile_1" value="1"><i></i>&nbsp; Use my profile information for invoicing.
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Different Address</label>
                                            <div class="col-md-8">
                                                <label class="radio top6" id="local_now_same_address3">
                                                    <input type="radio" id="profilePublic" name="profile_1" value="0"><i></i>&nbsp; Use a different address for invoicing.
                                                </label>
                                            </div>
                                        </div>
                                        <fieldset id="local_same_show3" style="display:none">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">FirstName</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_fname_same" readonly="" id="profileLastName" value="<?php echo $first_name; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">Last Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_lname_same" readonly="" id="profileLastName" value="<?php echo $last_name; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileAddress">Address</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_address_same" readonly="" id="profileAddress" value="<?php echo $address; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="profileCompany">State</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                           name="yes_handle_cargo_state_same" readonly=""
                                                           id="profileCompany" value="<?php echo $state; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileAddress">Zip</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_zip_same" readonly="" id="profileAddress" value="<?php echo $zip; ?>">
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset id="local_dif_show3" style="display:none">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">First Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_fname_other" id="profileLastName">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileLastName">LastName</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_lname_other" id="profileLastName">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="profileAddress">Address</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="yes_handle_cargo_address_other" id="profileAddress">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="profileCompany">State</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                           name="yes_handle_cargo_state_other" id="profileCompany">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="profileAddress">Zip</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                           name="yes_handle_cargo_zip_other" id="profileAddress">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                </fieldset>
                                <fieldset id="for_usa">
                                    <div class="form-group">
                                        <h4 class="col-md-12 control-label left_align bot25"> Please select one of
                                            the three options available (<a href="#">Compare</a>)</h4>
                                        <div class="col-md-12 top_7 radio">
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Fedex</label>
                                                <div class="col-md-8">
                                                    <label class="radio top6" id="fedx">
                                                        <input type="radio" name="chooseShipping" value="fedex"
                                                               id="fedx">
                                                        <i></i> &nbsp;Door to door Service directly from the grower
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Local Airport</label>
                                                <div class="col-md-8">
                                                    <label class="radio top6" id="local">
                                                        <input type="radio" name="chooseShipping" value="local"
                                                               id="local">
                                                        <i></i> &nbsp;You will pick up the boxes at your cityÃ¢â‚¬â„¢s
                                                        local airport
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Miami Drop Off</label>
                                                <div class="col-md-8">
                                                    <label class="radio top6" id="miami">
                                                        <input type="radio" name="chooseShipping" value="miami"
                                                               id="miami">
                                                        <i></i>&nbsp; We will deliver in Miami to the carrier of
                                                        your choice
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </fieldset>
                                <!--Fedex Form Starts-->
                                <div id="for_fedx">

                                    <h4>Fed Ex</h4>
                                    <div class="form-group" id="for_fedx_r1">
                                        <label class="col-xs-3 control-label">Profile Address</label>
                                        <div class="col-md-8">
                                            <label class="radio top6" id="same_address">
                                                <input type="radio" id="profilePublic" value="fed_same"
                                                       name="address_type">
                                                <i></i>&nbsp; Use same address as my profile
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="for_fedx_r2">
                                        <label class="col-xs-3 control-label">Different Address</label>
                                        <div class="col-md-8">
                                            <label class="radio top6" id="not_same_address">
                                                <input type="radio" id="profilePublic" value="fed_other"
                                                       name="address_type">
                                                <i></i>&nbsp; I want to use different address for ship
                                            </label>
                                        </div>
                                    </div>
                                    <fieldset id="same_address_show">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileFirstName">First
                                                Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="fed_same_firstname" id="profileFirstName"
                                                       value="<?php echo $first_name; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">Last
                                                Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="fed_same_lastname" id="profileLastName"
                                                       value="<?php echo $last_name; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileAddress">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="fed_same_address" id="profileAddress"
                                                       value="<?php echo $address; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileCompany">Company</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="fed_same_company" id="profileCompany"
                                                       value="<?php echo $company; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">State</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="fed_same_state" id="profileAddress"
                                                       value="<?php echo $state; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">Zip</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="fed_same_zip" id="profileAddress"
                                                       value="<?php echo $zip; ?>">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <!--Fedex diffrent address-->
                                    <fieldset id="not_same_show">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileFirstName">First
                                                Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="fedxFirstName"
                                                       id="profileFirstName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">Last
                                                Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="fedxLastName"
                                                       id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileAddress">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="fedxAddress"
                                                       id="profileAddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileCompany">Company</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="fedxCompany"
                                                       id="profileCompany">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">State</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="fedexState"
                                                       id="profileAddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">Zip</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="fedexZip"
                                                       id="profileAddress">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <!--Fedex Form Starts-->
                                <!--Local Form Starts-->
                                <div id="for_local">


                                    <h4>Local Airport Delivery</h4>
                                    <div class="form-group" id="for_local_r1">
                                        <label class="col-xs-3 control-label">Profile Address</label>
                                        <div class="col-md-8">
                                            <label class="radio top6" id="local_same_address">
                                                <input type="radio" id="profilePublic" value="local_same"
                                                       name="address_type">
                                                <i></i>&nbsp; Use my profile information to pick up these boxes
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="for_local_r2">
                                        <label class="col-xs-3 control-label">Different Address</label>
                                        <div class="col-md-8">
                                            <label class="radio top6" id="local_now_same_address">
                                                <input type="radio" id="profilePublic" value="local_other"
                                                       name="address_type">
                                                <i></i>&nbsp; Use a different address to pick up my boxes
                                            </label>
                                        </div>
                                    </div>
                                    <fieldset id="local_same_show">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileFirstName">Choose Airport</label>
                                            <div class="col-md-8">
                                                <select class="form-control" name="local_airport_name">
                                                    <option value="">Select here</option>
                                                    <?php
                                                    if ($stmt = $con->prepare("select airport_id,airport_name from usa_airports order by airport_name")) {
                                                        $stmt->bind_param('i', $userSessionID);
                                                        $stmt->execute();
                                                        $stmt->bind_result($airport_id, $airport_name);
                                                        while ($stmt->fetch()) {
                                                            ?>
                                                            <option
                                                                    value="<?php echo $airport_id; ?>"><?php echo $airport_name; ?></option>
                                                            <?php
                                                        }
                                                        $stmt->close();
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">First
                                                Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly="" name="local_same_firstname" id="profileLastName" value="<?php echo $first_name; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">Last Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly="" name="local_same_lastname" id="profileLastName" value="<?php echo $last_name; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileAddress">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly="" name="local_same_address" id="profileAddress" value="<?php echo $address; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileCompany">State</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly="" name="local_same_atate" id="profileCompany" value="<?php echo $company; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">Zip</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly=""
                                                       name="local_same_zip" id="profileAddress"
                                                       value="<?php echo $zip; ?>">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <!--For Local Diffrent address-->
                                    <fieldset id="local_dif_show">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileFirstName">Choose Airport</label>
                                            <div class="col-md-8">
                                                <select class="form-control" name="airport_name">
                                                    <option value="">Select here</option>
                                                    <?php
                                                    if ($stmt = $con->prepare("select airport_id,airport_name from usa_airports order by airport_name")) {
                                                        $stmt->bind_param('i', $userSessionID);
                                                        $stmt->execute();
                                                        $stmt->bind_result($airport_id, $airport_name);
                                                        while ($stmt->fetch()) {
                                                            ?>
                                                            <option
                                                                    value="<?php echo $airport_id; ?>"><?php echo $airport_name; ?></option>
                                                            <?php
                                                        }
                                                        $stmt->close();
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">First Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="localFirstName" id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">Last Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="localLastName" id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="localAddress" id="profileAddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileCompany">State</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="localState" id="profileAddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">Zip</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="localZip" id="profileAddress">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <!--Local Form Starts-->
                                <!--Miami Drop off Form Starts-->
                                <div id="for_miami">
                                    <!--                                        <h4>Miami Drop off</h4>-->
                                    <fieldset>
                                        <div class="form-group">
                                            <h4 class="col-md-12 control-label left_align bot25"
                                                for="profileFirstName"> Please select one of the following and the
                                                two following options appear</h4>
                                            <div class="col-md-12 top_7 radio">
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label">Pick Up</label>
                                                    <div class="col-md-8">
                                                        <label class="radio top6">
                                                            <input type="radio" name="cargo_agencyradio" value="cargo" id="cargo_agency">
                                                            <i></i> I will pick up from the cargo agency in Miami
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label">Deliver</label>
                                                    <div class="col-md-8">
                                                        <label class="radio top6">
                                                            <input type="radio" name="cargo_agencyradio" value="shipping" id="shiping_company">
                                                            <i></i> Please deliver my boxes to a shipping company
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </fieldset>
                                    <fieldset id="miami_1st">
                                        <div class="form-group">
                                            <h4 class="col-md-12 control-label left_align" for="profileFirstName">
                                                Input the details of the person or company picking up your boxes</h4>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">First Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="cargo_agencyfname" id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">Last Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="cargo_agencylname" id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileAddress">Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="cargo_agencyaddress"
                                                       id="profileAddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"
                                                   for="profileCompany">Company</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="cargo_agencycompany"
                                                       id="profileCompany">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileCompany">Pickup
                                                Address</label>
                                            <div class="col-md-8">
                                                <p>9440 N.W. 12th St.<br> Miami, FL 33172.<br>Tel: (305) 392 6228 |
                                                    (305) 392 6229. <br>Fax:(305) 392 2994.</p>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset id="miami_2nd">
                                        <div class="form-group">
                                            <h4 class="col-md-12 control-label left_align" for="profileFirstName">
                                                Please put the name of the company and address where your boxes will
                                                be delivered to:</h4>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">First Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="shiping_companyfname" id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileLastName">Last Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="shiping_companylname" id="profileLastName">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileAddress">Company Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="shiping_companycname" id="profileAddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileCompany">Company Address</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="shiping_companycaddress" id="profileCompany">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <!--Miami Drop off Form Starts-->
                                <div class="row" id="last_step" style="display: none;">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary">Submit4</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>
                                <!--</form>-->
                            </div>
                            <!-- Shipping End -->
                            <!--Payment tab start-->
                            <!-- Edit -->
                            <div id="payment" class="tab-pane">

                                <form class="row clearfix" method="post" action="">

                                    <div class="col-lg-6 col-sm-6">
                                        <div class="heading-title">
                                            <h4>Payment Method Created</h4>
                                            <?php
                                            $getPaymentMethod = "select * from buyers_payment where buyer_id='" . $userSessionID . "'";
                                            $paymentMethodRes = mysqli_query($con, $getPaymentMethod);

                                            if (mysqli_num_rows($paymentMethodRes) > 0) {

                                                while ($paymentRow = mysqli_fetch_array($paymentMethodRes)) {
                                                    //echo "<pre>";print_r($paymentRow);echo "</pre>";
                                                    if ($paymentRow['payment_method'] == "2") {
                                                        ?>
                                                        <address>
                                                            <p>Name on card
                                                                : <?php echo strtoupper($crypt->decrypt($paymentRow['cc_name_on_card'])); ?></p>
                                                            <p>Card Type :
                                                                <?php
                                                                if ($crypt->decrypt($paymentRow['cc_type']) == "AE") {
                                                                    echo "American Express";
                                                                } elseif ($crypt->decrypt($paymentRow['cc_type']) == "VI") {
                                                                    echo "Visa";
                                                                } elseif ($crypt->decrypt($paymentRow['cc_type']) == "MC") {
                                                                    echo "Mastercard";
                                                                } elseif ($crypt->decrypt($paymentRow['cc_type']) == "DI") {
                                                                    echo "Discover";
                                                                }

                                                                ?>
                                                            </p>
                                                            <p>
                                                                Card Number: <?php
                                                                $credit_card_number = $crypt->decrypt($paymentRow['cc_number']);
                                                                echo str_repeat("x", (strlen($credit_card_number) - 4)) . substr($credit_card_number, -4, 4);
                                                                ?>
                                                            </p>
                                                            <!-- Phone: 1-800-565-2390 <br>
                                                            Fax: 1-800-565-2390 <br> -->
                                                            <p>
                                                                <?php if ($paymentRow['paypal_email'] != "") { ?>
                                                                    Paypal Email: <?php echo $paymentRow['paypal_email']; ?>
                                                                <?php } ?>
                                                            </p>
                                                            <label id="direct_payment"
                                                                   class="radio pull-left nomargin-top">
                                                                <input
                                                                        type="radio" <?php if ($paymentRow['cc_default'] == "1") echo "checked"; ?>
                                                                        onclick="setDefaultCC(<?php echo $paymentRow['id']; ?>)"
                                                                        id="cc_default_<?php echo $paymentRow['id']; ?>"
                                                                        name="cc_default"/>
                                                                <i></i>
                                                                <span class="weight-300">Default Credit Card </span>
                                                            </label>
                                                        </address>
                                                    <?php }
                                                }
                                            } else { ?>
                                                <address style="margin-top: 10px;">
                                                    Data Not Found
                                                </address>
                                            <?php } ?>
                                        </div>

                                    </div>

                                    <div class="col-lg-6 col-sm-6">
                                        <div class="heading-title">
                                            <h4>Payment Method</h4>
                                        </div>

                                        <!-- PAYMENT METHOD -->
                                        <fieldset class="margin-top-40">
                                            <div class="toggle-transparent toggle-bordered-full clearfix">
                                                <div class="toggle active">
                                                    <div class="toggle-content" style="display: block;">

                                                        <div class="row nomargin-bottom">

                                                            <div class="col-lg-12 nomargin clearfix">
                                                                <label class="radio pull-left" id="card_payment">
                                                                    <input id="payment_card" name="payment[method]"
                                                                           type="radio" value="2">
                                                                    <i></i> <span class="weight-300">Credit Card</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-12 nomargin clearfix">
                                                                <label class="radio pull-left nomargin-top"
                                                                       id="wire_payment">
                                                                    <input id="payment_check" name="payment[method]"
                                                                           type="radio" value="1">
                                                                    <i></i> <span
                                                                            class="weight-300">Wire Transfer</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-12 nomargin clearfix">
                                                                <label class="radio pull-left nomargin-top"
                                                                       id="direct_payment">
                                                                    <input id="direct_check" name="payment[method]"
                                                                           type="radio" value="1">
                                                                    <i></i> <span class="weight-300">Direct Deposit (Us customers Only)</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-12 nomargin clearfix">
                                                                <label class="radio pull-left nomargin-top"
                                                                       id="check_payment">
                                                                    <input id="payment_check" name="payment[method]"
                                                                           type="radio" value="1">
                                                                    <i></i> <span
                                                                            class="weight-300">Check / Money order</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-12 nomargin clearfix">
                                                                <label class="radio pull-left nomargin-top"
                                                                       id="paypal_payment">
                                                                    <input id="direct_check" name="payment[method]"
                                                                           type="radio" value="1">
                                                                    <i></i> <span class="weight-300">Paypal</span>
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!-- /PAYMENT METHOD -->


                                        <!-- CREDIT CARD PAYMENT -->
                                        <fieldset id="ccPayment" class="margin-top-30 softhide" style="display: block;">

                                            <div class="toggle-transparent toggle-bordered-full clearfix">
                                                <div class="toggle active">
                                                    <div class="toggle-content" style="display: block;">

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label for="payment:name">Name on Card *</label>
                                                                <input id="payment:name" name="payment[name]"
                                                                       type="text" class="form-control required"
                                                                       autocomplete="off">
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label for="payment:name">Credit Card Type *</label>
                                                                <select id="payment:state" name="payment[state]"
                                                                        class="form-control pointer required">
                                                                    <option value="">Select...</option>
                                                                    <option value="AE">American Express</option>
                                                                    <option value="VI">Visa</option>
                                                                    <option value="MC">Mastercard</option>
                                                                    <option value="DI">Discover</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label for="payment:cc_number">Credit Card Number
                                                                    *</label>
                                                                <input id="payment:cc_number" name="payment[cc_number]"
                                                                       type="text" class="form-control required"
                                                                       autocomplete="off" maxlength="16">
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label for="payment:cc_exp_month">Card Expiration
                                                                    *</label>

                                                                <div class="row nomargin-bottom">
                                                                    <div class="col-lg-6 col-sm-6">
                                                                        <select id="payment:cc_exp_month"
                                                                                name="payment[cc_exp_month]"
                                                                                class="form-control pointer required">
                                                                            <option value="0">Month</option>
                                                                            <option value="01">01 - January</option>
                                                                            <option value="02">02 - February</option>
                                                                            <option value="03">03 - March</option>
                                                                            <option value="04">04 - April</option>
                                                                            <option value="05">05 - May</option>
                                                                            <option value="06">06 - June</option>
                                                                            <option value="07">07 - July</option>
                                                                            <option value="08">08 - August</option>
                                                                            <option value="09">09 - September</option>
                                                                            <option value="10">10 - October</option>
                                                                            <option value="11">11 - November</option>
                                                                            <option value="12">12 - December</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-lg-6 col-sm-6">
                                                                        <select id="payment:cc_exp_year"
                                                                                name="payment[cc_exp_year]"
                                                                                class="form-control pointer required">
                                                                            <option value="0">Year</option>
                                                                            <option value="2015">2015</option>
                                                                            <option value="2016">2016</option>
                                                                            <option value="2017">2017</option>
                                                                            <option value="2018">2018</option>
                                                                            <option value="2019">2019</option>
                                                                            <option value="2020">2020</option>
                                                                            <option value="2021">2021</option>
                                                                            <option value="2022">2022</option>
                                                                            <option value="2023">2023</option>
                                                                            <option value="2024">2024</option>
                                                                            <option value="2025">2025</option>
                                                                        </select>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label for="payment:cc_cvv">CVV2 *</label>
                                                                <input id="payment:cc_cvv" name="payment[cc_cvv]"
                                                                       type="text" class="form-control required"
                                                                       autocomplete="off" maxlength="4">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                        <!-- /CREDIT CARD PAYMENT -->
                                        <!-- Wire Transfer -->
                                        <div class="toggle-transparent toggle-bordered-full clearfix" id="wire_trans">
                                            <div class="toggle active">
                                                <div class="toggle-content" style="display: block;">
                                                    
                                                    <span class="clearfix">
                                                        <strong
                                                                class="pull-left">Please make your wire transfers to:</strong>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">Fresh Life Floral</span>
                                                        <span class="pull-left">Name on the Account:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">164104524328</span>
                                                        <span class="pull-left">Account Number:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">Checking Account</span>
                                                        <span class="pull-left">Account Type:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">BOFAUS3N</span>
                                                        <span class="pull-left">SWIFT Code:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">026009593</span>
                                                        <span class="pull-left">Routing Number:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-left">Beneficiary Address:<br>
                                                        22333   BARBACOA DR.SAUGUS, CA, 91350 U.S.A
                                                        </span>
                                                    </span>
                                                    <span class="clearfix">
                                                        
                                                        <span class="pull-left">Bank Address:<br>
                                                        100 N   TRYON ST #130 CHARLOTTE, NC, 28202 U.S.A
                                                        </span>
                                                        
                                                    </span>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Wire trasnfer -->
                                        <!-- Cheque -->
                                        <div class="toggle-transparent toggle-bordered-full clearfix" id="Cheque">
                                            <div class="toggle active">
                                                <div class="toggle-content" style="display: block;">
                                                    
                                                    <span class="clearfix">
                                                        <strong class="pull-left">Mailing Information:</strong>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">Fresh Life Floral</span>
                                                        <span class="pull-left">Make check payable to:</span>
                                                    </span>


                                                    <span class="clearfix">
                                                        <span class="pull-left">Please mail your checks to:<br>
                                                        22333   BARBACOA DR.SAUGUS, CA, 91350 U.S.A
                                                        </span>
                                                    </span>


                                                </div>
                                            </div>
                                        </div>
                                        <!-- Cheque -->
                                        <!-- Deposite -->
                                        <div class="toggle-transparent toggle-bordered-full clearfix deposite"
                                             id="deposite">
                                            <div class="toggle active">
                                                <div class="toggle-content" style="display: block;">
                                                    
                                                    <span class="clearfix">
                                                        <strong class="pull-left">Please make your deposite to:</strong>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">Fresh Life Floral</span>
                                                        <span class="pull-left">Name on the Account:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">164104524328</span>
                                                        <span class="pull-left">Account Number:</span>
                                                    </span>
                                                    <span class="clearfix">
                                                        <span class="pull-right">Checking Account</span>
                                                        <span class="pull-left">Account Type:</span>
                                                    </span>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- deposite -->
                                        <!-- paypal -->
                                        <div class="toggle-transparent toggle-bordered-full clearfix" id="paypal">
                                            <div class="toggle active">
                                                <div class="toggle-content" style="display: block;">
                                                    
                                                    <span class="clearfix">
                                                        <strong class="pull-left">Please input your email address associated with your paypal account:</strong>
                                                    </span>
                                                    <input type="email" name="paypal_email" id="paypal_email"
                                                           class="form-control" placeholder="Enter Your Paypal Email">


                                                </div>
                                            </div>
                                        </div>
                                        <!-- paypal -->
                                        <hr>
                                        <button name="btn_payment_save" id="btn_payment_save"
                                                class="btn btn-primary btn-lg btn-block size-13"
                                                style="background-color:#923A8D!important;width:100%;"><i
                                                    class="fa fa-mail-forward"></i> Submit Payment Method Now
                                        </button>

                                    </div>
                                </form>

                            </div>
                            <!--Payment tab end-->
                        </div>
                    </div>
                </div>



                    
                    
                <!-- /COL 3 -->
                <div class="col-md-12 col-lg-3">
                    <!-- projects -->
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <h2 class="panel-title elipsis">
                                <i class="fa fa-rss"></i> GROWER'S OFFERS
                            </h2>
                        </header>
                        <div class="panel-body noradius padding-10">
                            <ul class="comment list-unstyled">
                                <!--Growers start-->
                                <?php
                                $query2 = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,
                                                         ff.name as featurename 
                                             from buyer_requests gpb
                                             left join product p     on gpb.product    = p.id
                                             left join subcategory s on p.subcategoryid= s.id  
                                             left join features ff   on gpb.feature    = ff.id
                                             left join sizes sh      on gpb.sizeid     = sh.id 
                                            where gpb.buyer='" . $userSessionID . "' 
                                              and ( gpb.type!=1 and gpb.type!=7 and gpb.type!=5 )  
                                              and gpb.lfd ='" . date("Y-m-d") . "' 
                                            order by gpb.id desc";
                                
                                $result2 = mysqli_query($con, $query2);
                                $tp = mysqli_num_rows($result2);
                                
                                if ($tp >= 1) {
                                    
                                    while ($producs = mysqli_fetch_array($result2)) {
                                        //echo '<pre>';print_r($producs);
                                        $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.farms,g.file_path5,cr.name as countryname,
                                                                  cr.flag 
                                                        from grower_offer_reply gr 
                                                        left join growers g on gr.grower_id=g.id 
                                                        left join country cr on g.country_id=cr.id
                                                       where gr.offer_id='" . $producs["cartid"] . "' 
                                                        and gr.buyer_id='" . $userSessionID . "' 
                                                        and g.id is not NULL 
                                                       GROUP BY gr.grower_id 
                                                       order by gr.date_added desc";
                                        
                                        $rs_growers = mysqli_query($con, $sel_check);
                                        $rs_growers1 = mysqli_query($con, $sel_check);
                                        $totalio = mysqli_num_rows($rs_growers);
                                        
                                        while ($growers = mysqli_fetch_assoc($rs_growers)) {
                                            $getGrowerssql = "SELECT * FROM farms where id='" . $growers["farms"] . "'";
                                            $rs_growers1 = mysqli_query($con, $getGrowerssql);
                                            $growers_row = mysqli_fetch_assoc($rs_growers1);

                                            $k = explode("/", $growers["file_path5"]);
                                            ?>
                                            <li class="comment grower-comments">
                                                <!-- avatar -->
                                                <div class="avter_left">
                                                    <img class="avatar-grower-buyer" alt="avatar"
                                                         src="<?php echo SITE_URL; ?>user/logo/<?php echo $k[1]; ?>"
                                                         class="avatar">
                                                </div>
                                                <!-- comment body -->
                                                <div class="comment-body comment-right">
                                                    <a class="comment-author" href="#">
                                                        <?php
                                                        $firm_name = explode(" ", $growers['growers_name']);
                                                        ?>
                                                        <span class="g_title"><?php echo ucwords(strtolower($growers_row['name'])); ?></span>
                                                    </a>
                                                    <?php
                                                    if ($growers["boxqty"] != "") { ?>
                                                        <p>
                                                            <?php
                                                            echo $growers["boxqty"] ?><?php echo $growers["boxtype"] ?><?php echo $growers['product_subcategory'] ?><?php echo $growers["product"] ?><?php echo $growers["size"]; ?><?php echo "$" . $growers["price"]; ?>
                                                        </p>
                                                    <?php } else { ?>
                                                        <p>
                                                            <?php echo "-"; ?>
                                                        </p>
                                                    <?php }
                                                    ?>

                                                </div><!-- /comment body -->
                                            </li>
                                            <?php
                                        }
                                    }
                                } else {
                                    ?>
                                    <li class="comment">
                                        No Offers Found...!
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </section>
                    <!-- /projects -->
                </div>
            </div>
        </div>
    </div>
</section>

<script src="../includes/assets/js/jquery.raty.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/assets/js/star-ratings.js"></script>
<script>

    $(document).ready(function () {
                

                        
        $('#grower_id').change(function () {
            var g_name = $("#grower_id option:selected").text();
            $('#grower_name').val(g_name);
            //alert(g_name);
        });
    });

</script>
<script type="text/javascript">
    function shippingInfo(id) {
        $('#shippingMethodId').val(id);
        $('#to_select' + id).toggleClass("selected");
        $("#non_usa_select").show();
    }
    $(function () {
        $('input:radio[name=shiping_need]').change(function () {
            if (this.value == '1') {
                $('#for_usa').show();
                $("#yes_handle_cargo").hide();
                $("#for_non_usa").hide();
                $("#last_step").hide();
                $("#local_dif_show_non_usa").hide();
                $("#local_same_show_non_usa").hide();

            }
            else if (this.value == '0') {
                var check_usa = $("#country_name").val();
                //console.log(check_usa);
                if (check_usa == 'USA' || check_usa == 'USA (LA) ') {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>shippingmethodusa_ajax.php',
                        data: 'cname=' + check_usa + '&userSessionID=<?php echo $userSessionID; ?>',
                        success: function (data) {
                            $('.for_non_usa_methods').html(data);
                            $("#for_non_usa").show();
                            $("#for_usa").hide();
                            $("#miami_1st").hide();
                            $("#miami_2nd").hide();
                            $("#for_miami").hide();
                            $("#for_fedx").hide();
                            $("#for_local").hide();
                            $("#last_step").show();
                            $("#local_dif_show_non_usa").hide();
                            $("#local_same_show_non_usa").hide();
                        }
                    });

                } else {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>shippingmethod_ajax.php',
                        data: 'cname=' + check_usa + '&userSessionID=<?php echo $userSessionID; ?>',
                        success: function (data) {
                            $('.for_non_usa_methods').html(data);
                            $("#for_non_usa").show();
                            $("#for_usa").hide();
                            $("#miami_1st").hide();
                            $("#miami_2nd").hide();
                            $("#for_miami").hide();
                            $("#for_fedx").hide();
                            $("#for_local").hide();
                            $("#last_step").show();
                            $("#local_dif_show_non_usa").hide();
                            $("#local_same_show_non_usa").hide();
                        }
                    });
                }
            }
        });

        $('form#formShippingInfo').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>shipping_info_ajax.php',
                data: $('form#formShippingInfo').serialize(),
                success: function (data) {
                    if (data == 'true') {
                        $('.imgDiv').hide();
                        //alert-success alert-danger
                        $('#shippingAlert .innerMsg').html('Informaion Updated');
                        $('#shippingAlert').addClass('alert-success').show();

                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    } else {
                        $('.imgDiv').hide();
                        //alert-success alert-danger
                        $('#shippingAlert .innerMsg').html('There is some error. Please try again');
                        $('#shippingAlert').addClass('alert-danger').show();
                    }

                }
            });
        });
    });
    function clear_form_elements(id_name) {
        jQuery("#" + id_name).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                case 'select-one':
                case 'select-multiple':
                    jQuery(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });
    }
    $(document).ready(function () {
        $('input:radio[name=chooseShipping]').change(function () {
            if (this.value == 'fedx') {
                clear_form_elements('local_dif_show');
                clear_form_elements('for_miami');
                clear_form_elements('for_local_r1');
                clear_form_elements('for_local_r2');
                $('#local_same_show').hide();

                $('#miami_1st').hide();
                $('#miami_2nd').hide();
                $('#local_dif_show').hide();
                $("#non_usa_select").hide();
            } else if (this.value == 'local') {
                $('#not_same_show').hide();
                $('#same_address_show').hide();
                $('#miami_1st').hide();
                $('#miami_2nd').hide();
                clear_form_elements('not_same_show');
                clear_form_elements('for_miami');
                clear_form_elements('for_fedx_r1');
                clear_form_elements('for_fedx_r2');
                $("#non_usa_select").hide();

            } else {
                $('#local_same_show').hide();
                $('#local_dif_show').hide();
                $('#not_same_show').hide();
                $('#same_address_show').hide();
                $("#non_usa_select").hide();
                clear_form_elements('not_same_show');
                clear_form_elements('local_dif_show');
                clear_form_elements('for_fedx_r1');
                clear_form_elements('for_fedx_r2');
                clear_form_elements('for_local_r1');
                clear_form_elements('for_local_r2');

            }
        });

        $('input:radio[name=profile_1]').change(function () {
            if (this.value == '1') {

                $('#local_same_show3').show();
                $('#local_dif_show3').hide();

            } else {
                $('#local_dif_show3').show();
                $('#local_same_show3').hide();

            }
        });

        $('input:radio[name=order_id]').change(function () {
            var val = this.value;
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>redirectrequest_pending.php',
                data: 'id=' + val,
                success: function (data) {
                    if (data == 'true') {
                        window.location.href = '<?php echo SITE_URL; ?>name-your-price.php';
                    }

                }
            });

        });
        $('input:radio[name=own_shipping]').change(function () {
            if (this.value == '1') {
                var check_usa = $("#country_name").val();
                if (check_usa == 'USA' || check_usa == 'USA (LA) ') {
                    $("#yes_handle_cargo").show();
                    $("#for_usa").hide();
                    $("#miami_1st").hide();
                    $("#miami_2nd").hide();
                    $("#for_miami").hide();
                    $("#for_fedx").hide();
                    $("#for_local").hide();
                    $("#last_step").show();
                    $('#local_same_show3').hide();
                    $("#local_dif_show_non_usa").hide();
                    $("#local_same_show_non_usa").hide();

                } else {
                    $("#yes_handle_cargo").hide();
                    $('#local_same_show3').hide();
                    $("#for_non_usa").hide();
                    $("#last_step").hide();
                    $("#local_dif_show_non_usa").hide();
                    $("#local_same_show_non_usa").hide();

                }
                $("#yes_handle_cargo").show();
                $("#last_step").show();
                $("#non_usa_select").hide();
                $("#arrange_shipping").hide();
                clear_form_elements('for_usa');
                clear_form_elements('not_same_show');
                clear_form_elements('local_dif_show');
                clear_form_elements('for_miami');
            }
            else if (this.value == '0') {
                var check_usa = $("#country_name").val();
                if (check_usa == 'USA' || check_usa == 'USA (LA) ') {
                    //$("#for_usa").show();
                    $("#yes_handle_cargo").hide();
                    $('#local_same_show3').hide();
                    $("#for_non_usa").hide();
                    $("#last_step").hide();
                    $("#local_dif_show_non_usa").hide();
                    $("#local_same_show_non_usa").hide();
                    $("#non_usa_select").hide();
                    $("#arrange_shipping").show();
                }
                else {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>shippingmethod_ajax.php',
                        data: 'cname=' + check_usa + '&userSessionID=<?php echo $userSessionID; ?>',
                        success: function (data) {
                            $('.for_non_usa_methods').html(data);
                        }
                    });
                    $("#for_non_usa").show();
                    $("#last_step").show();
                    $("#yes_handle_cargo").hide();
                    $('#local_same_show3').hide();
                    $("#non_usa_select").hide();
                    $("#arrange_shipping").hide();
                }
                //clear_form_elements('yes_handle_cargo');
            }
        });
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest_country.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #country_name').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            if (((suggestion).length >= 3)) {
                $("#shipping_mod").show().delay(5000);
                $("#for_usa").hide();
                $("#miami_1st").hide();
                $("#miami_2nd").hide();
                $("#for_miami").hide();
                $("#for_fedx").hide();
                $("#for_local").hide();
                $("#yes_handle_cargo").hide();
                $('#local_same_show').hide();
                $('#local_dif_show').hide();
                $('#not_same_show').hide();
                $('#same_address_show').hide();
                $('#miami_1st').hide();
                $('#miami_2nd').hide();
                $('#for_non_usa').hide();
                $('#non_usa_select').hide();
                $("#local_dif_show_non_usa").hide();
                $("#local_same_show_non_usa").hide();
                $("#non_usa_select").hide();
                $('#arrange_shipping').hide();
                $('#local_same_show3').hide();

                clear_form_elements('shipping_mod');
                clear_form_elements('not_same_show');
                clear_form_elements('local_dif_show');
                clear_form_elements('for_fedx_r1');
                clear_form_elements('for_fedx_r2');
                clear_form_elements('for_local_r1');
                clear_form_elements('for_local_r2');
            }
            else {
                $("#shipping_mod").hide();
                $("#for_usa").hide();
                $("#miami_1st").hide();
                $("#miami_2nd").hide();
                $("#for_miami").hide();
                $("#for_fedx").hide();
                $("#for_local").hide();
                $("#local_dif_show_non_usa").hide();
                $("#local_same_show_non_usa").hide();
                $("#non_usa_select").hide();
                $('#arrange_shipping').hide();
                $('#local_same_show3').hide();

            }

        });
    });
    $.fn.raty.defaults.path = 'images';
    $(function () {
        $('#quality_rating1').raty({scoreName: 'quality_rating'});
        $('#freshness_rating1').raty({scoreName: 'freshness_rating'});
        $('#trustworthiness_rating1').raty({scoreName: 'trustworthiness_rating'});
        $('#pricing_rating1').raty({scoreName: 'pricing_rating'});
        $('#packing_rating1').raty({scoreName: 'packing_rating'});
    });
    function redirectRequest() {
        var delDate = $('#deliveryDate').val();
        var dateRange = $('#dateRange').val();
        var myRadio = $('input[name=row_id]');
        var shippingMethod = myRadio.filter(':checked').val();
        if (myRadio.filter(':checked').length > 0) {
            $('#erMsg').hide();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>file/redirectrequest.php',
                data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod,
                success: function (data) {
                    if (data == 'true') {
                        window.location.href = '<?php echo SITE_URL; ?>name-your-price.php';
                    }
                }
            });
        } else {
            $('#erMsg').show();
        }
    }
// opcion que funciona del calendario
    function checkOption() {    
                    
                
        var myRadio = $('input[name=row_id]');
        var shippingMethod = myRadio.filter(':checked').val();
        if (myRadio.filter(':checked').length > 0) {
            $('#erMsg').hide();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal.php',
                data: 'shippingMethod=' + shippingMethod,
                success: function (data) {
                    $('.cls_date_start_date').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            });                        
            
            
            
            
        } else {
            $('#erMsg').show();
        }
    }

    //esta  es  la  nueva  opcion  by  Jose Portilla
    function shippingChange(product_id, sizename, i) {
        var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
        if (shipping_val != "") {
            $('#erMsg').hide();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal.php',
                data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
                success: function (data) {
                    $('.cls_date_start_date').html(data);
                    _pickers();//show calendar
                }
            });

        } else {
            $('#erMsg').show();
        }
    }


    function checkUpdatedVar() {
        var check_updated = $("#file_updated_var").val();
        //alert(check_updated);
        if (check_updated == "") {
            $("#file_updated_var").css("border", "1px solid red");
            $("#temp_file_updated").css("border", "1px solid red");
            return false;
        }
        else {
            $("#contributions_from").submit();
        }
    }
    function setDefaultCC(ccid) {

        var buyer_id = "<?php echo $userSessionID;?>";
        $.ajax({
            url: 'save_default_setting.php',
            type: 'post',
            data: {'default_type': 'cc', 'ccid': ccid, 'buyer_id': buyer_id},
            success: function () {

            }
        }); // end ajax call
    }
    function setDefaultShipping(shippingId) {
        var buyer_id = "<?php echo $userSessionID;?>";
        $.ajax({
            url: 'save_default_setting.php',
            type: 'post',
            data: {'default_type': 'shipping', 'shippingId': shippingId, 'buyer_id': buyer_id},
            success: function () {

            }
        }); // end ajax call
    }
    $(document).ready(function () {
        $('#quality_rating').on('rating.change', function (event, value, caption) {
            $("#quality_rating").val(value);
        });
        $('#freshness_rating').on('rating.change', function (event, value, caption) {
            $("#freshness_rating").val(value);
        });
        $('#trustworthiness_rating').on('rating.change', function (event, value, caption) {
            $("#trustworthiness_rating").val(value);
        });
        $('#pricing_rating').on('rating.change', function (event, value, caption) {
            $("#pricing_rating").val(value);
        });

        $('#packing_rating').on('rating.change', function (event, value, caption) {
            $("#packing_rating").val(value);
        });

        $("#country").change(function () {
            var country_val = $('#country :selected').val();
            //alert(country_val);
            if (country_val == "240") {
                $("#us_block").show();
                $("#other_block").hide();
                $("#state_text_other").attr("disabled", "disable");
                $("#zip").removeAttr("disabled");
                $("#state_text_us").removeAttr("disabled");
            }
            else {
                $("#us_block").hide();
                $("#other_block").show();
                $("#state_text_other").removeAttr("disabled");
                $("#zip").attr("disabled", "disable");
                $("#state_text_us").attr("disabled", "disable");
            }
        });

        $("#shipping_country").change(function () {
            var country_val = $('#shipping_country :selected').val();
            //alert(country_val);
            if (country_val == "3") {
                $("#shipping_us_block").show();
                $("#shipping_other_block").hide();

            }
            else {
                $("#shipping_us_block").hide();
                $("#shipping_other_block").show();
            }
        });

        //Add review code
        jQuery("#reviewBtn").click(function () {
            var review = jQuery("#review").val();

            var grower_id = $('#grower_id :selected').val();
            //var quality_rating=jQuery("input[name=quality_rating]").val();
            var freshness_rating = jQuery("input[name=freshness_rating]").val();
            var trustworthiness_rating = jQuery("input[name=trustworthiness_rating]").val();
            var pricing_rating = jQuery("input[name=pricing_rating]").val();
            var packing_rating = jQuery("input[name=packing_rating]").val();

            var buyer_id = "<?php echo $_SESSION['buyer']; ?>";

            if (review != "" && grower_id != "" && buyer_id != "") {
                jQuery.ajax({
                    url: '<?php echo SITE_URL; ?>ajax/addReview.php',
                    type: 'POST',
                    data: "grower_id=" + grower_id + "&buyer_id=" + buyer_id + "&f_review=buyer_page&review=" + review + "&freshness_rating=" + freshness_rating + "&trustworthiness_rating=" + trustworthiness_rating + "&pricing_rating=" + pricing_rating + "&packing_rating=" + packing_rating,
                    success: function (result) {
                        if (result == "step1") {
                            $("#first_step").show();
                            $("#second_step").hide();
                            $("#third_step").hide();
                        }
                        else if (result == "step2") {
                            $("#first_step").hide();
                            $("#second_step").show();
                            $("#third_step").hide();
                        }
                        else if (result == "step3") {
                            $("#first_step").hide();
                            $("#second_step").hide();
                            $("#third_step").show();
                        }

                    }
                });
            }
            else {
                if (grower_id == "")
                    jQuery("#grower_id").css("border", "1px solid red");
                else
                    jQuery("#grower_id").css("border", "1px solid #000");
                if (review == "")
                    jQuery("#review").css("border", "1px solid red");
                else
                    jQuery("#review").css("border", "1px solid #000");
            }
        });
    });
    function deleteShipping(shipping_id) {
        if (confirm('Are you sure, you want to delete this shipping method ?')) {
            jQuery.ajax({
                url: '<?php echo SITE_URL; ?>ajax/deleteshipping.php',
                type: 'POST',
                data: "shipping_id=" + shipping_id,
                success: function (result) {
                    if (result != "") {
                        jQuery("#tr_shipping_row" + shipping_id).remove();
                    }
                }
            });
        }


    }
    function defaultShipping(shipping_id, buyer_id) {
        // document.write(buyer_id);
        jQuery.ajax({
            url: '<?php echo SITE_URL; ?>ajax/selectedshipping.php',
            type: 'POST',
            data: "shipping_id=" + shipping_id + "&buyer_id=" + buyer_id,
        });
        location.href = "/buyer/buyers-account.php";

    }
</script>
<script>
    //script uno
    function validate_profile_email(theForm) {
        var error = 0;
        if (theForm.af.value == "" || theForm.af.value == "Please write the Affair") {
            $('.error_af').html('Please fill the Affair');
            theForm.af.focus();
            $("#af").attr("style", "border:1px solid #FF0000;");
            error = 1;
        } else {
            $('.error_af').html('');
            $("#af").attr("style", "border:1px solid #BDBDBD;");
        }
        if (theForm.area.value == "" || theForm.area.value == "Please write your message....") {
            $('.error_area').html('Please fill the message');
            theForm.af.focus();
            $("#area").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        esle
        {
            $('.error_area').html('');
            $("#area").attr("style", "border:1px solid #BDBDBD;");
        }
        if (error == 1) {
            return false;
        } else {
            return true;
        }
    }
    function validate_profile_form(theForm) {
        var error = 0;
        if (theForm.first_name.value == "" || theForm.first_name.value == "Full Name") {
            $('.error_first_name').html('Please fill the First Name');
            theForm.first_name.focus();
            $("#first_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.last_name.value == "" || theForm.last_name.value == "Last Name") {
            $('.error_last_name').html('Please fill the Last Name');
            theForm.last_name.focus();
            $("#last_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.phone.value == "" || theForm.phone.value == "Phone") {
            $('.error_phone').html('Please fill the Phone');
            theForm.phone.focus();
            $("#phone").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (error == 1) {
            return false;
        } else {
            return true;
        }
    }
    function remove_validate_text(id) {
        $('.error_' + id).html('');
        $("#" + id).removeAttr("style");
    }
    function check_password(current_element) {
        var pass_val = $('#password').val();
        var rep_pass_val = $('#repeat_password').val();

        console.log(pass_val.length);
        if (pass_val.length > 0) {
            if (current_element == 'password') {
                if (pass_val.length <= 5) {
                    $('.error_password').html("Password should be greater then 5 character");
                    $('#password').attr("style", "border:1px solid #FF0000;");
                } else {
                    $('.error_password').html("");
                    $('#password').removeAttr("style");
                }
            }

            if (current_element == 'repeat_password') {
                if (pass_val != rep_pass_val) {
                    $('.error_repeat_password').html("Password did not match!");
                    $('#repeat_password').attr("style", "border:1px solid #FF0000;");
                } else {
                    $('.error_repeat_password').html("");
                    $('#repeat_password').removeAttr("style");
                }
            }
        }
    }

    function send_request() {
        var delDate = $('#cls_date').find("input").val();
        var qucik_desc = $('#qty_desc').val();
        var myRadio = $('input[name=row_id]');
        var shippingMethod = myRadio.filter(':checked').val();
        var dateRange = "";

        var flag_s = true;


        if (qucik_desc == "") {
            alert("Please enter quick description.");
            flag_s = false;
        }
        else if (delDate == "") {
            alert("Please select delivery date.");
            flag_s = false;
        }

        if (flag_s == true) {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>file/redirectrequest.php',
                data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
                success: function (data) {
                    alert("Your order was created successfully");
                    window.location.href = '<?php echo SITE_URL; ?>buyer/buyers-account.php';
                },
                error: function () {
                    alert(" ! Your order has not been created !"); 
                }
            });
        }


    }
    ;
</script>
<?php require_once("../includes/profile-footer.php"); ?> 
