<?php

$menuoff = 1;
$page_id = 421;
$message = 0;



require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>
<style>
td.ex {
  color: rgb(0,0,255);
}
</style>
<section id="middle">
  <form name="frmrequest" id="frmrequest" method="post" action="">

    <!-- page title -->
    <header id="page-header">
        <h1>Orders List</h1>
        <ol class="breadcrumb">
            <li><a href="#">Avalaibility</a></li>
            <li class="active">Pendings</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Buyer Orders</strong> <!-- panel title -->
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>Id</th>             
                                <th>Buyer</th>  
                                <th>Order </th>                    
                                <th>Descrip.</th>                    
                                <th>Delivery</th>                                        
                                <th>Status</th>                                                            
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                               $estadoReady = '1';
                               $estadoNotr  = '0';                               
                               
                              $sql = "select bo.id              , bo.buyer_id , bo.order_number , bo.order_date , 
                                             bo.shipping_method , bo.del_date , bo.order_serial , bo.seen       , 
                                             bo.delivery_dates  , bo.qucik_desc as descrip , 
                                             b.first_name       , b.last_name              , bo.join_order      ,
                                             bo.availability
                                        from buyer_orders bo
                                       inner join buyers b  ON bo.buyer_id = b.id          
                                       where bo.del_date > date_add(curdate(), INTERVAL -60 DAY) 
                                         and bo.buyer_id = '".$userSessionID."'
                                       order by bo.id desc";
                            
                                $invoices_res = mysqli_query($con, $sql);

                                while ($invoice = mysqli_fetch_assoc($invoices_res)) { 
                                    
                                      $id_fact = $invoice['id'];
                                      
                                      $id_ava = $invoice['availability'];
                                      
                                  if ($id_ava == 0) {
                                        $disp_id = "Not ready";
                                        $color_id = "grey";
                                  } elseif ($id_ava == 1) {
                                        $disp_id = "Published";
                                        $color_id = "orange";
                                  } elseif ($id_ava == 2) {
                                        $disp_id = "Ready";                                        
                                        $color_id = "green";
                                  } elseif ($id_ava == 4) {
                                        $disp_id = "Completed";
                                        $color_id = "black";
                                  } else {
                                      $disp_id = "Not ready";
                                  }   
                                  
                                  if ($invoice["join_order"] != 0) {
                                        $joinedId = "(Joined ".$invoice["join_order"].")";
                                  } else {
                                        $joinedId = " ";
                                  }                                                                                                     
                                ?>
                                    <tr>
                                            <td><?php echo $invoice['id']; ?></td>
                                            <td><?php echo $invoice['first_name'];?> <?php echo $product["last_name"]." ".$joinedId?></td>                                                                
                                            <td><?php echo $invoice['order_number']; ?></td>                                                                                                            
                                            <td><?php echo $invoice['descrip']; ?></td>                                                                
                                            <td><?php echo $invoice['del_date']; ?></td>
                                            
                                            
                              <?php   if ($id_ava == 0) {  ?>
                              
                                    <td><font color="grey"><?php echo $disp_id; ?></font></td>                               
                                    
                              <?php } elseif ($id_ava == 1) { ?>                                    
                                    
                                    <td><font color="orange" size=4 ><?php echo $disp_id; ?></font></td>   
                                    
                              <?php } elseif ($id_ava == 2) { ?>                                    
                                    
                                    <td><font color="green"><?php echo $disp_id; ?></font></td>    
                                    
                              <?php } elseif ($id_ava == 4) { ?>                                    
                                    
                                    <td><font color="black"><?php echo $disp_id; ?></font></td>                                                                                                       
                                  
                              <?php }else{ ?>
                                    
                                    <td><font color="red"><?php echo $disp_id; ?></font></td>  
                                    
                              <?php }
                              
                              ?>                                            
                                            
                                            
                                                                                                                                   
                                            <td>
                                                   <button style="background:#8a2b83!important;" onclick="ready('<?php echo $id_fact; ?>','<?php  echo $userSessionID; ?>','<?php  echo $estadoReady; ?>')" class="btn btn-primary" type="button">Publish Availability</button>                                                                                                                              
                                            </td>    
                                            
                                            <td>
                                                   <button style="background:#8a2b83;" onclick="ready('<?php echo $id_fact; ?>','<?php  echo $userSessionID; ?>','<?php  echo $estadoNotr; ?>')" class="btn btn-primary" type="button">End Cycle</button>                                                                                                                              
                                            </td>                                                                                                                                                                              
                                            
                                    </tr>
                                    <?php
                                    $i++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
    </form>
</section>
<?php require_once '../includes/footer_new.php'; ?>

<script>
    
    function ready(factid , buyerid , estadoid) {                                  
                       
        var flag_s = true; 
                     
        var id_fact  = factid;                     
        var id_buyer = buyerid;                                              
        
        var id_estado  = estadoid;     
        
        alert(id_fact);
        
        alert(id_estado);
                                                     
    if (flag_s == true) {           
        
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/dispo-ready.php',
            data: 'id_fact   =' + id_fact +
                  '&id_buyer =' + id_buyer +
                  '&id_estado=' + id_estado,
            success: function (data_s) {
                
                if (data_s == 'true') {
                       alert('OK');
                } else {
                    //alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    }      
    
</script>    
