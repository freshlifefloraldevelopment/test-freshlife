<?php
session_start();

$userSessionID = $_SESSION["buyer"];
if ($_SESSION["login"] != 1) {
    header("location:  /login.php");
    die;
}
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 16 Jun 2021
Structure MarketPlace previous to buy
**/
// start a session


// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
$mS = 1;
include('../back-end/inc/header_ini.php');


//getAccountManager

$rs_account = mysqli_query($con,getAccountManager($userSessionID));
$row_account = mysqli_fetch_array($rs_account);
$nameCompany = $row_account['company'];
$first_nameCompany = $row_account['first_name'];
$last_nameCompany = $row_account['last_name'];
$emailCompany = $row_account['email'];
$phoneCompany = $row_account['phone'];

//getAddress
$rs_accountAdd = mysqli_query($con,getAccountAddress($userSessionID));
$row_accountAdd = mysqli_fetch_array($rs_accountAdd);
$companyAddress = $row_accountAdd['address'];


//getBillInformation
$rs_accountBill = mysqli_query($con,getAccountBilling($userSessionID));
$row_accountBill = mysqli_fetch_array($rs_accountBill);
$nameCompanySh = $row_accountBill['company'];
$first_nameCompanySh = $row_accountBill['first_name'];
$last_nameCompanySh = $row_accountBill['last_name'];
$addressCompanySh = $row_accountBill['address'];
$countryCompanySh = $row_accountBill['name'];
$cityCompanySh = $row_accountBill['city'];
$phoneCompanySh = $row_accountBill['phone'];
$correoCompanySh = $row_accountBill['correo'];

?>
<style type="text/css">
	.portlet-body.max-h-500 {
    margin-left: 18px;
}
</style>
			<div id="wrapper_content" class="d-flex flex-fill">

				<?php
        $m=2;
        include('../back-end/inc/sidebar-menu.php'); ?>


				<!-- MIDDLE -->
				<div id="middle" class="flex-fill">

					<!--
						PAGE TITLE
					-->
					<div class="page-title bg-transparent b-0">

						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Dashboard
						</h1>

					</div>






					<!-- WIDGETS -->
					<div class="row gutters-sm">
						<!-- WIDGET : TASKS -->
						<div class="col-12 col-xl-4 mb-3">
							<div class="portlet">

								<div class="portlet-header">
								<span class="d-block text-muted text-truncate font-weight-medium">
										<?php echo $nameCompany; ?> Imports
									</span>
								</div>

								<div  class="portlet-body max-h-500">
<div class="border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Account Manager</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
											<?php
											echo $first_nameCompany." ".$last_nameCompany."<br>";
											echo $emailCompany."<br>";
											echo $phoneCompany;
										 ?>
										</p>

									</div>


								</div>

							</div>

						</div>
						<!-- /WIDGET : TASKS -->




						<!-- WIDGET : TASKS -->
						<div class="col-12 col-xl-4 mb-3">

							<div class="portlet">

								<div class="portlet-header">
								<span class="d-block text-muted text-truncate font-weight-medium">
										Shipping Information
									</span>
								</div>

								<div  class="portlet-body max-h-500">

                                                                        <div class="border-bottom border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Address</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
                                                                                    <?php echo $companyAddress;?>
										</p>

									</div>
                                                                    
                                                                        <div class="border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Shipping Options</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
											<?php
                                                                                    //getAccountShippings

                                                                                    $rs_accountSh = mysqli_query($con,getAccountShippings($userSessionID));
                                                                                    //$row_accountSh = mysqli_fetch_array($rs_accountSh);
                                                                                    while ($row_accountSh = mysqli_fetch_assoc($rs_accountSh)) {    
                                                                                        $idSh      = $row_accountSh['id'];
                                                                                        $nameSh    = $row_accountSh['name'];
                                                                                        $contactSh = $row_accountSh['contact_name'];
                                                                                        $accountSh = $row_accountSh['Instructions'];
                                                                                        $descripSh = $row_accountSh['description'];
                                                                                        $codeSh    = $row_accountSh['shipping_code'];   
                                                                                        $ii++;

                                                                                        
											echo $ii.") ".$nameSh."<br>";
											echo "&nbsp;&nbsp;&nbsp;&nbsp;".$contactSh."<br>";
											echo "&nbsp;&nbsp;&nbsp;&nbsp;".$descripSh."-".$codeSh."<br><br><br>";
}                                                                                        
											?>
										</p>

									</div>


								</div>

							</div>

						</div>
						<!-- /WIDGET : TASKS -->
						<div class="col-12 col-xl-4 mb-3">

							<div class="portlet">

<div class="portlet-header">

									<div class="float-end">

<button style="margin-right: 10px;" class="dropdown-toggle btn btn-sm btn-soft btn-primary px-2 py-1 fs--15 mt--n3" data-toggle="modal" data-target="#billing_modal" onclick="return false;">Billing History</button>



									</div>


									<span class="d-block text-muted text-truncate font-weight-medium">
										Account Information
									</span>

								</div>

								<div  class="portlet-body max-h-500">


<div class="border-bottom border-light">
										<p class="fs--14 mb-2 pt-1">
Purchase Limit: $0<br>
Available: $0
										</p>

									</div>
<div class="border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Billing Information</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
											<?php
											echo $nameCompanySh." Imports"."<br>";
											echo $first_nameCompanySh." ".$last_nameCompanySh."<br>";
											echo $addressCompanySh."<br>";
											echo $countryCompanySh." - ".$cityCompanySh."<br>";
                                                                                        echo $phoneCompanySh."<br>";
											echo $correoCompanySh;
											?>
										</p>

									</div>


								</div>

							</div>

						</div>






					</div>
					<!-- /WIDGETS -->


				</div>
				<!-- /MIDDLE -->

			</div><!-- FOOTER -->
<!-- Card Modal -->
<div class="modal fade" id="card_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog modal-md modal-lg" role="document">
				<div class="modal-content">

						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">Payment</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->
						<div class="modal-body">
								<div class="form-advanced-list p-4 shadow-xs border rounded mb-5">










									<label class="form-radio form-radio-primary d-block py-3 border-bottom">Credit Card

										<span class="float-end mt--n3 ml--n10 mr--n10">
											<img src="assets/images/credit_card/visa.svg" width="38" height="24" alt="cc">
											<img src="assets/images/credit_card/mastercard.svg" width="38" height="24" alt="cc">
											<span class="fs--11 d-block text-align-end">and more...</span>
										</span>

									</label>

									<!-- CREDIT CARD FORM -->
									<div id="payment_card_form" class="form-advanced-list-reveal-item bg-gradient-light px-4 pt-4 rounded mt--n10 border bt-0">

										<div class="row">

											<div class="col-12 pl--5 pr--5 mb-3">

												<div class="input-group-over">
													<input type="hidden" name="cc_type" id="cc_type" value="">

													<div class="form-label-group">
														<input placeholder="Card Number" id="cc_number" type="text" data-card-type="#cc_type" class="form-control cc-format cc-number">
														<label for="cc_number">Card Number</label>
													</div>


													<span class="px-3 text-muted">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="22px" height="22px" viewBox="0 0 47 47" xml:space="preserve">
															<path fill="#bbbebf" d="M23.498,10.141c-2.094,0-3.796,1.709-3.796,3.807v3.706h7.596v-3.706C27.298,11.851,25.593,10.141,23.498,10.141z"></path>
															<path fill="#bbbebf" d="M3.431,0v23.928C3.431,36.67,23.5,47,23.5,47s20.069-10.33,20.069-23.072V0H3.431z M34.351,30.148c0,1.571-1.286,2.856-2.855,2.856h-15.99c-1.57,0-2.855-1.285-2.855-2.856v-9.64c0-1.569,1.286-2.855,2.855-2.855h0.387v-3.706c0-4.198,3.413-7.613,7.606-7.613c4.195,0,7.609,3.415,7.609,7.613v3.706h0.388c1.57,0,2.855,1.286,2.855,2.855V30.148z"></path>
														</svg>
													</span>
												</div>

											</div>

											<div class="col-12 col-md-5 pl--5 pr--5 mb-2">

												<div class="form-label-group">
													<input placeholder="Cardholder Name" id="cc_name" type="text" value="" class="form-control">
													<label for="cc_name">Cardholder Name</label>
												</div>

											</div>

											<div class="col-6 col-md-3 pl--5 pr--5 mb-4">

												<div class="form-label-group">
													<input placeholder="MM / YY" id="cc_date" type="text" value="" class="form-control cc-format cc-expire" maxlength="5">
													<label for="cc_date">MM / YY</label>
												</div>

											</div>

											<div class="col-6 col-md-4 pl--5 pr--5 mb-4">

												<div class="form-fancy form-fancy-input mb--6">

													<div class="input-group-over">

														<div class="form-label-group">
															<input placeholder="CVV" id="cc_cvv" type="text" value="" class="form-control cc-format cc-cvc">
															<label for="cc_cvv">CVV</label>
														</div>

														<span class="px-3 text-muted" title="" data-html="true" data-toggle="tooltip" data-original-title="<span class='fs--12'>3-digit security code on the back of your card.<br><span class='fs--11'>(amex = 4-digit code on the front)</</span>">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" xml:space="preserve" width="22px" height="22px">
																<path fill="#bbbebf" d="M256,0C134.684,0,36,96.243,36,215.851c0,68.63,33.021,132.639,88.879,173.334V492c0,7.938,4.694,15.124,11.962,18.313c7.225,3.173,15.704,1.809,21.577-3.593l82.144-75.555c5.17,0.355,10.335,0.535,15.438,0.535c121.316,0,220-96.243,220-215.851C476,96.13,377.21,0,256,0z M256,391.701c-6.687,0-13.516-0.376-20.298-1.118c-5.737-0.634-11.466,1.253-15.715,5.161    l-55.108,50.687v-67.726c0-6.741-3.396-13.029-9.034-16.726C105.848,329.2,76,274.573,76,215.851C76,118.886,156.747,40,256,40    s180,78.886,180,175.851C436,312.814,355.252,391.701,256,391.701z"></path>
																<path fill="#bbbebf" d="M266,98.877h-70c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h70c16.542,0,30,13.458,30,30c0,16.542-13.458,30-30,30h-20c-11.046,0-20,8.954-20,20v28.793c0,11.046,8.954,20,20,20s20-8.954,20-20v-8.793c38.598,0,70-31.401,70-70C336,130.279,304.598,98.877,266,98.877z"></path>
																<circle fill="#bbbebf" cx="246" cy="319.16" r="27"></circle>
															</svg>
														</span>

													</div>

												</div>

											</div>
											<div class="col-12 col-md-5 pl--5 pr--5 mb-2">

												<div class="form-label-group">
													<input placeholder="Dollar Amount ($)" id="cc_name" type="text" value="" class="form-control">
													<label for="cc_name">Dollar Amount</label>
												</div>

											</div>
											<div class="col-12 col-md-5 pl--5 pr--5 mb-2">

												<div class="form-label-group">
													<button type="button" class="btn btn-success">
										Place Your Order
								</button>
												</div>

											</div>

										</div>
									</div>
									<!-- /CREDIT CARD FORM -->


								</div>
						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
										<i class="fi fi-close"></i>
										Close
								</button>
						</div>

				</div>
		</div>
</div>
<!-- /card Modal -->

<!-- Billing Modal -->
<div class="modal fade" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog modal-md modal-xl" role="document">
				<div class="modal-content">

						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">Buyer Invoices</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->

						<div class="modal-body">
							<table class="table-datatable table table-bordered table-hover table-striped"
											data-lng-empty="No data available in table"
											data-responsive="true"
											data-header-fixed="true"
											data-select-onclick="true"
											data-enable-paging="true"
											data-enable-col-sorting="true"
											data-autofill="false"
											data-group="false"
											data-items-per-page="10">
											<thead>
												<tr>
													<th>Invoice Num</th>
													<th>Descrip</th>
													<th>PerKg</th>
													<th>AirWaybill</th>
													<th>Gross W.</th>
													<th>Delivery</th>
                          <th>St</th>
                          <th>List</th>
                          <th>View</th>
                          <th></th>

												</tr>
											</thead>
											<tbody>

                        <?php

                                $sel_order= "SELECT id_fact, del_date, is_pending , quick_desc, order_serial , seen , bill_number, gross_weight , volume_weight , freight_value, total_boxes, sub_total_amount, grand_total , bill_state , air_waybill, charges_due_agent, credit_card_fees, per_kg from invoice_orders WHERE buyer_id = '$userSessionID' order by id_fact desc; ";
                                $rs_order=mysqli_query($con,$sel_order);
                            while($orderCab=mysqli_fetch_array($rs_order))  {



                              $invoice_num = $orderCab['id_fact']." - ".$orderCab['bill_number'];
                              $descrip = $orderCab['quick_desc'];
                              $per_kg = $orderCab['per_kg'];
                              $air_waybill = $orderCab['air_waybill'];
                              $gross_weight = $orderCab['gross_weight'];
                              $del_date = $orderCab['del_date'];
                              $bill_state = $orderCab['bill_state'];


                              if($orderCab['is_pending']=="1"){
                                $value_color = "warning";
                                $valur_bt = "Pending";
                                  $href = "javascript:void(0);";
                              }else{
                                $value_color = "success";
                                $valur_bt = "View";

                                $href = "/buyer/invoice.php?idi=$id_fact";
                              }



                        ?>

                        <tr>
													<td><span class="fs--15"><?php echo $invoice_num; ?></span></td>
													<td><span class="fs--15"><?php echo $descrip; ?></span></td>
													<td><span class="fs--15"><?php echo number_format($per_kg, 2, '.', ',');  ?></span></td>
													<td><span class="fs--15"><?php echo number_format($air_waybill, 2, '.', ','); ?></span></td>
													<td><span class="fs--15"><?php echo $gross_weight; ?></span></td>
													<td><span class="fs--15"><?php echo $del_date; ?></span></td>
                          <td><span class="fs--15"><?php echo $bill_state; ?></span></td>
                          <td>
                            <a href="/buyer/document-list.php?idi=<?php echo $id_fact; ?>" class="btn btn-sm btn-block btn-danger bg-gradient-success text-white b-0">
                            											<span class="p-0-xs">
                            												<span class="fs--15">Docs</span>
                            											</span>
                            										</a>
                          </td>
                          <td>
                            <a href="<?php echo $href; ?>" class="btn btn-sm btn-block btn-danger bg-gradient-<?php echo $value_color; ?> text-white b-0">
                                                  <span class="p-0-xs">
                                                    <span class="fs--15"><?php echo $valur_bt; ?></span>
                                                  </span>
                                                </a>
                          </td>
                          <td>
                            <a href="/buyer/invoice-price.php?idi=<?php echo $id_fact; ?>" class="btn btn-sm btn-block btn-danger bg-gradient-success text-white b-0">
                                                  <span class="p-0-xs">
                                                    <span class="fs--15">Price</span>
                                                  </span>
                                                </a>
                          </td>

												</tr>
                        <?php
                            }
                           ?>
											</tbody>
											<tfoot>
                        <tr>
                          <th>Invoice Num</th>
                          <th>Descrip</th>
                          <th>PerKg</th>
                          <th>AirWaybill</th>
                          <th>Gross W.</th>
                          <th>Delivery</th>
                          <th>St</th>
                          <th>List</th>
                          <th>View</th>
                          <th></th>

                        </tr>
											</tfoot>
										</table>
						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
										<i class="fi fi-close"></i>
										Close
								</button>
						</div>

				</div>
		</div>
</div>
<!-- Billing Modal -->
<?php include('../back-end/inc/footer.php'); ?>
<script src="../back-end/assets/js/blockui.js"></script>
<script>
function checkOrderPrevious(){
      var orderP =  document.getElementById('selectPreviousOrder').value;
      if(orderP==0){
        document.getElementById('valueOrderId_MP').value = orderP;
        document.getElementById('orders_modal').disabled = true;
  }
      else{
        document.getElementById('valueOrderId_MP').value = orderP;
        document.getElementById('orders_modal').disabled = false;
        }
    }


        function checkOption() {


            var myRadio = $('input[name=row_id]');
            var shippingMethod = myRadio.filter(':checked').val();
            if (myRadio.filter(':checked').length > 0) {
                $('#erMsg').hide();
                $.ajax({
                    type: 'post',
                    url: '/file/get_date_modal.php',
                    data: 'shippingMethod=' + shippingMethod,
                    success: function (data) {
                        $('.cls_date_start_date').html(data);
                        $('#nextOpt').click();
                        _pickers();//show calendar
                    }
                });




            } else {
                $('#erMsg').show();
            }
        }

        //esta  es  la  nueva  opcion  by  Jose Portilla
        function shippingChange(product_id, sizename, i) {
            var shipping_val = $('#shipping_id_' + product_id + '_' + i + ' :selected').val();
            if (shipping_val != "") {
                $('#erMsg').hide();
                $.ajax({
                    type: 'post',
                    url: '/file/get_date_modal.php',
                    data: 'shippingMethod=' + shipping_val + "&product_id=" + product_id + "&sizename=" + sizename + "&index=" + i,
                    success: function (data) {
                        $('.cls_date_start_date').html(data);
                        _pickers();//show calendar
                    }
                });

            } else {
                $('#erMsg').show();
            }
        }

        function send_request() {
            var delDate = $('#cls_date').find("input").val();
            var qucik_desc = $('#qty_desc').val();
            var myRadio = $('input[name=row_id]');
            var shippingMethod = myRadio.filter(':checked').val();
            var dateRange = "";

            var flag_s = true;


            if (qucik_desc == "") {
                alert("Please enter quick description.");
                flag_s = false;
            }
            else if (delDate == "") {
                alert("Please select delivery date.");
                flag_s = false;
            }

            if (flag_s == true) {
                $.ajax({
                    type: 'post',
                    url: '/file/redirectrequest.php',
                    data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
                    success: function (data) {
                        alert("Your order was created successfully");
                        window.location.href = '/buyer/buyers-account.php';
                    },
                    error: function () {
                        alert(" ! Your order has not been created !");
                    }
                });
            }


        }
</script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>


                        <!--Select Orders Modal Open-->
                        <div class="modal fade orders_method_modal" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                            <div class="modal-dialog modal-md modal-md" role="document">
                                <div class="modal-content">

                                    <!-- header modal -->
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span class="fi fi-close fs--18" aria-hidden="true"></span>
                                      </button>

                                    </div>
                                    <!-- body modal 3-->
                                    <form action="../en/florMP.php" method="post" id="payment-form">
                                    <div class="modal-body">
                                        <div class="table-responsive">

                                          <font color="#000">Please, before to continue select an order.</font><br><br>

                                          <div class="form-label-group mb-3">
                                          <select class="form-control"  id="selectPreviousOrder" onchange="checkOrderPrevious()" name="order_id">
                                                      <option value='0'>Select Previous Order</option>
                                                      <?php
                                                              $sel_order="select id , order_number ,del_date , qucik_desc
                                                                            from buyer_orders
                                                                           where del_date >= '" . date("Y-m-d") . "'
                                                                             and is_pending=0 and buyer_id = '".$userSessionID."'  ";

                                                              $rs_order=mysqli_query($con,$sel_order);

                                                          while($orderCab=mysqli_fetch_array($rs_order))  {
                                                      ?>
                                                              <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                                      <?php
                                                          }
                                                         ?>
                                          </select>

                                          <label for="select_options">Select Previous Order</label>
                                           <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                                        </div>



                                                                <br>
                                                               <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                                        </div>



                                    </div>

                                    <div class="modal-footer request_product_modal_hide_footer">
                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                                        <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        <!--Order create modal open-->
                        <div class="modal fade buying_method_next" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content" style="min-height: 300px;">
                                  <!-- header modal -->
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabelMd">Select Date</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span class="fi fi-close fs--18" aria-hidden="true"></span>
                                    </button>

                                  </div>
                                  <!-- body modal 3-->
                                    <div class="modal-body">
                                        <div class="col-md-12 margin-bottom-20">
                                            <h4>Quick Description</h4>
                                            <textarea value="2" class="form-control" id="qty_desc" name="qty_desc" ></textarea>
                                        </div>
                                        <br>
                                        <!-- date picker -->
                                        <div class="col-md-12 margin-bottom-20">
                                        <label>Select delivery date</label>
                                        <!--<?php //echo $products["prodcutid"]; ?>_<?php ///echo $i; ?>--->
                                        <label class="cls_date_start_date" id="cls_date">
                                            <input value="" class="datepicker link-muted js-datepickified required start_date" placeholder="Select Date" type="text">
                                        </label>

                                        <br>
                                        </div>
                                        <div class="to_right">
                                            <a href="" class="btn btn-3d btn-purple no_back" style="background-color:#923A8D!important;" data-dismiss="modal" aria-label="Close">Cancel</a>
                                            <a href="#" class="btn btn-3d btn-purple" data-toggle="modal" data-target=".buying_method_modal" style="background-color:#923A8D!important;" data-dismiss="modal"><i class="fa fa-chevron-left"></i>Back</a>&nbsp;&nbsp;
                                            <button style="background:#8a2b83!important;" onclick="send_request()" class="btn btn-primary" type="button">Create order</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Order create modal End-->


                        <!--Next Modal Open-->
                        <div class="modal fade buying_method_modal" tabindex="-1" role="dialog"
                             aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content" style="min-height: 570px;">

                                    <!-- header modal -->

                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabelMd">Select Shipping Option</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span class="fi fi-close fs--18" aria-hidden="true"></span>
                                      </button>

                                    </div>
                                    <!-- body modal 3-->
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover nomargin">
                                                <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Company</th>
                                                    <th>Description</th>
                                                    <th>Code</th>
                                                    <th>Destiny</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $getShippingMethod = "select * from buyer_shipping_methods where buyer_id='" . $userSessionID . "'";
                                                $shippingMethodRes = mysqli_query($con, $getShippingMethod);
                                                if (mysqli_num_rows($shippingMethodRes) > 0) {
                                                    while ($shippingMethodData = mysqli_fetch_assoc($shippingMethodRes)) {
                                                        //echo '<pre>';//print_r($shippingMethodData);
                                                        if ($shippingMethodData['shipping_method_id'] != 0) {
                                                            $dt = mysqli_query($con, "select * from shipping_method where id =" . $shippingMethodData['shipping_method_id']);
                                                            $sdata = mysqli_fetch_assoc($dt);
                                                            ?>
                                                            <tr>

                                                                <td align="left">
                                                                    <?php
                                                                    if ($shippingMethodData['default_shipping'] == 1) {
                                                                        ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['shipping_method_id']; ?>" checked="checked">
                                                                        <?php

                                                                    } else { ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['shipping_method_id'] ?>">
                                                                    <?php }
                                                                    ?>
                                                                </td>


                                                               <!-- <td><?php echo $sdata['name']; ?></td>-->
                                                                <td><?php echo $shippingMethodData['company'] ?></td>
                                                                <td><?php echo $sdata['description']; ?></td>
                                                                <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                                <td><?php
                                                                    if ($sdata['destination_country'] != 0) {
                                                                        $getCountry = "select *from country where id =" . $sdata['destination_country'];
                                                                        $countryRes = mysqli_query($con, $getCountry);
                                                                        $country_s = mysqli_fetch_assoc($countryRes);
                                                                        echo $country_s['name'];
                                                                    } else {
                                                                        echo "No defined";
                                                                    } ?>
                                                                </td>
                                                            </tr>
                                                        <?php } else {
                                                            // echo $shippingMethodData['id'];
                                                            if ($shippingMethodData['own_shipping'] == '1') {
                                                                $getshipping_type = "select * from cargo_agency where id='" . $shippingMethodData['cargo_agency_id'] . "'";
                                                                $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                                $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                                //echo '<pre>';print_r($shippingType);
                                                                $shippingMethodDataname = $shippingType['name'];
                                                            } else {
                                                                $shipping_type = $shippingMethodData['choose_shipping'];
                                                                if (!empty($shipping_type) && $shipping_type != '') {
                                                                    $getshipping_type = "select * from shipping_method where shipping_type='" . $shipping_type . "'";
                                                                    //echo $getshipping_type;
                                                                    $shippingTypeQry = mysqli_query($con, $getshipping_type);
                                                                    $shippingType = mysqli_fetch_assoc($shippingTypeQry);
                                                                    $shippingMethodDataname = $shippingType['name'];
                                                                } else {
                                                                    $shippingMethodDataname = $shippingMethodData['name'];
                                                                }
                                                            }
                                                            ?>
                                                            <tr>

                                                                <td align="center">
                                                                    <?php
                                                                    if ($shippingMethodData['default_shipping'] == 1) {
                                                                        ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['id']; ?>" checked="checked">
                                                                        <?php

                                                                    } else { ?>
                                                                        <input type="radio" name="row_id" value="<?php echo $shippingMethodData['id'] ?>">
                                                                    <?php }
                                                                    ?>
                                                                </td>
                                                                <td><?php echo $shippingMethodDataname; ?></td>
                                                                <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                                <td><?php echo $shippingMethodData['shipping_code'] ?></td>
                                                                <td><?php
                                                                    if ($shippingMethodData['country'] != 0) {
                                                                        $getCountry = "select *from country where id =" . $shippingMethodData['country'];
                                                                        $countryRes = mysqli_query($con, $getCountry);
                                                                        $country_s = mysqli_fetch_assoc($countryRes);
                                                                        echo $country_s['name'];
                                                                    } else {
                                                                        echo "No defined";
                                                                    } ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td colspan="3">No Shipping Method Found.....</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="to_right">
                                            <br>
                                            <div id="erMsg" style="display:none;color:red;">Please select a shipping method.</div>
                                            <a href="" class="btn btn-3d btn-purple no_back" style="background-color:#923A8D!important;" data-dismiss="modal" aria-label="Close">Cancel</a>
                                            <a id="nextOpt" style="display:none" href="javascript:void(0);" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal">dfsd Jose </a>
                                            <a href="javascript:void(0);" class="btn btn-3d btn-purple" style="background-color:#923A8D!important;" onclick="checkOption();">Next <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
