<?php

// PO 2018-08-24

$menuoff = 1;
$page_id = 421;
$message = 0;

//include("../config/config_new.php");

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Upload Records</h1>
        <ol class="breadcrumb">
            <li><a href="#">Buyer</a></li>
            <li class="active">Request</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Buyer file(.CVS)</strong> <!-- panel title -->
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>Reg.</th>
                                <th>variety</th>
                                <th>Size</th>                                 
                                <th>Stems</th>                                 
                                <th>Growers</th>                                                                 
                                <th>Date Upload</th>                                                                                                 
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i= 1;
                            
                            $sql = "SELECT id, variety, size, stems, growers, date_added
                                      FROM request_tmp  ";
                            
							$request_csv = mysqli_query($con, $sql);
                                                        
							while ($csv = mysqli_fetch_assoc($request_csv)) { 
                                                              
							?>
							<tr>
                                                                <td><?php echo $i; ?></td>
								<td><?php echo $csv['variety']; ?></td>                                                                
                                                                <td><?php echo $csv['size']; ?></td>
								<td><?php echo $csv['stems']; ?></td>                                                                
								<td><?php echo $csv['growers']; ?></td>
								<td><?php echo $csv['date_added']; ?></td> 
                                                                                                                           
							</tr>
							<?php
							$i++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
