<?php

// PO 2018-08-24

$menuoff = 1;
$page_id = 421;
$message = 0;


$id_client  = $_GET['id_cli'];

//include("../config/config_new.php");

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$sel_cli = "select cc.id , cc.id_week , cc.id_client,cc.id_year,
                   sc.name as subclient
              from client_calendar cc
             INNER JOIN sub_client sc ON cc.id_client = sc.id 
             where id_client ='" . $id_client . "' ";

$rs_cli = mysqli_query($con, $sel_cli);


$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Calendar Client</h1>
        <ol class="breadcrumb">
            <li><a href="#">Client</a></li>
            <li class="active">Order</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <a href="<?php echo SITE_URL; ?>buyer/customer_list.php" class="btn btn-success btn-xs relative">Back to Client</a>                
                <a href="<?php echo SITE_URL; ?>buyer/more_weeks.php?idcus=<?php echo $id_client ?>" class="btn btn-success btn-xs relative">More Weeks</a>                
                <!--span class="title elipsis">
                    <strong>Standing Order </strong>                     
                </span-->
                <!--?php echo " (".$cliente['name'].")"; ?--><!-- panel title -->

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Client</th>
                                <th>Year</th>                                 
                                <th>Week</th>                                 

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                                     $ii=1;
                                                       
							while ($client_week = mysqli_fetch_assoc($rs_cli)) { 

							?>
							<tr>

                                                                <td><?php echo $ii; ?></td>
                                                                <td><?php echo $client_week['subclient']; ?></td>                                                              
                                                                <td><?php echo $client_week['id_year']; ?></td>                                                              
                                                                <td><?php echo $client_week['id_week']; ?></td>                                                              
                                                           
                                                                <td>
                                                                  <a href="<?php echo SITE_URL; ?>buyer/client_standing.php?idwk=<?php echo $client_week["id_week"]."&id_cli=".$client_week["id_client"]."&idyr=".$client_week["id_year"] ?>" class="btn btn-success btn-xs relative">Standing Order</a>
                                                                </td>  
                                                                                                                                      
                                                                
							</tr>
							<?php
							$ii++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
