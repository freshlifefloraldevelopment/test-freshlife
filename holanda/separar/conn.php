<?php

/*
* Mysql database class - only one connection allowed
*/
/////////////////////////////////////////////////////////////////////////////////////
////// Reference http://www.whatsnoodle.com/creating-a-singleton-class-in-php ///////
///////////////// https://gist.github.com/jonashansen229/4534794 ////////////////////
/////////////////////////////////////////////////////////////////////////////////////

class  DatabaseConnection
{
private $_connection;
private static $_instance; //The single instance
private $_host = "localhost";
private $_username = "root";
private $_password = "";
private $_database = "test"; // database name here
//private static $connection = null;

public static function getInstance(){
  if(!self::$_instance) { // If no instance then make one
  self::$_instance = new self();
}
  return self::$_instance;
}
/**
* Protected constructor to prevent creating a new instance of the
* *Singleton* via the `new` operator from outside of this class.
     */
// Constructor
private function __construct() {
  $this->_connection = new mysqli($this->_host, $this->_username,
    $this->_password, $this->_database);

  // Error handling
  if(mysqli_connect_error()) {
    trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
       E_USER_ERROR);
  }
}
// Magic method clone is empty to prevent duplication of connection
private function __clone() { }

// Get mysqli connection
public function getConnection() {
  return $this->_connection;
}

protected function _close() {
      if ($this->_connection != NULL) {
          $this->_connection = NULL;
      }
}

  public function __destruct() {
      $this->_close();
  }
}

?>
