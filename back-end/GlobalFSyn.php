<?php

/**
#Market place cart
Developer educristo@gmail.com
Start 12 Abril 2021
Structure MarketPlace previous to buy
**/

function getAccountManager($userSessionID){
	$sql_account = "Select company,first_name,last_name,email,phone
from buyers
where id = '$userSessionID'";
return $sql_account;

}

function getAccountAddress($userSessionID){
	$sql_address = "Select address
	                 from buyers
	                where id = '$userSessionID'";

return $sql_address;

}

function getAccountBilling($userSessionID){
	$sql_bill = "select bsm.company , bsm.first_name , bsm.last_name , bsm.address , c.name ,
                            bsm.city, bsm.phone , email as correo
                       from buyer_shipping_methods bsm
                       left JOIN country c ON bsm.country = c.id
                       left JOIN buyers b ON bsm.buyer_id = b.id
                     where bsm.buyer_id = '$userSessionID'";

return $sql_bill;

}


function getAccountShippings($userSessionID){
	$sql_shippingInfo = "select ca.id , ca.name, ca.contact_name, ca.coordination as Instructions , sm.description,
                                    bsm.shipping_code
                               from buyer_shipping_methods bsm
                               left join shipping_method sm ON bsm.shipping_method_id = sm.id
                               left join connections co ON sm.connections = co.id
                               left join cargo_agency ca ON co.cargo_agency = ca.id
                              where bsm.buyer_id = '$userSessionID'";

return $sql_shippingInfo;

}



function getDataBuyer($con,$userSessionID){

	$sql_name = "SELECT first_name as fname, last_name as lname FROM buyers
									where id = '$userSessionID' ";

										$rs_nameB = mysqli_query($con,$sql_name);
										$row_nameB = mysqli_fetch_array($rs_nameB);
										$fnameB = $row_nameB['fname'];
										$lnameB = $row_nameB['lname'];

										return $fname." ".$lnameB;
}

function getemailBuyer($con,$userSessionID){

	$sql_name = "SELECT email FROM buyers
									where id = '$userSessionID' ";

										$rs_emailB = mysqli_query($con,$sql_name);
										$row_emailB = mysqli_fetch_array($rs_emailB);
										$emailB = $row_emailB['email'];

										return $emailB;
}

function getNameGrower($con, $growerID_Prod){

	 $sql_name = "SELECT growers_name FROM growers
									where id = '$growerID_Prod' ";

										$rs_name = mysqli_query($con,$sql_name);
										$row_nameB = mysqli_fetch_array($rs_name);
										$nameG = $row_nameB['growers_name'];

										return $nameG;
}

function calculateKilo($userSessionID,$con){
   $getBuyerShippingMethod = "select shipping_method_id
                              from buyer_shipping_methods
                             where buyer_id ='" . $userSessionID . "'";

  $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
  $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
   $shipping_method_id = $buyerShippingMethod['shipping_method_id'];

     $getShippingMethod = "select connect_group
                             from shipping_method
                            where id='" . $shipping_method_id . "'";

      $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
      $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

       $temp_conn = explode(',', $shippingMethodDetail['connect_group']);

      $id_conn = $temp_conn[1];  // Default

      $getConnect = "select charges_per_kilo
                       from connections
                      where id='" . $id_conn . "'";

      $rs_connect = mysqli_query($con, $getConnect);

      $charges = mysqli_fetch_assoc($rs_connect);

       /////////////////////////////////////////////////////////////

      $cost = $charges['charges_per_kilo'];
      $cost_un = unserialize($cost);

      $cost_sum = 0;

      foreach ($cost_un as $key => $value) {
          $cost_sum = $cost_sum + $value;
      }

      return $charges_per_kilo_trans =  $cost_sum;
}

function calculatePriceGrower($growerid,$productid,$sizeid,$idsc,$con,$userSessionID,$featureId){

	//$tasaKilo = calculateKilo($userSessionID,$con);

    $getGrowerPriceMeth1 = "select gp.price as price
															from growcard_prod_price gp
															INNER JOIN growers g ON gp.growerid = g.id
															INNER JOIN product p ON gp.productid = p.id
															INNER JOIN subcategory s ON p.subcategoryid = s.id
															INNER JOIN colors c on p.color_id = c.id
															where g.active = 'active'
															and gp.growerid = '$growerid'
															and gp.productid = '$productid'
															and gp.sizeid = '$sizeid'";
  $getGrowerPriceMeth1_array = mysqli_query($con, $getGrowerPriceMeth1);
  $GrowerPriceMeth1 = mysqli_fetch_assoc($getGrowerPriceMeth1_array);
  $priceGrower = $GrowerPriceMeth1['price'];
															if($priceGrower<=0)
															{
																$getGrowerPriceMeth2 = "select gp.id,gp.price_adm as price,
																s.name as sizename , gp.feature as feature, gp.factor,
																gp.stem_bunch , b.name as stems
																from grower_parameter gp
																inner JOIN sizes s ON gp.size = s.id
																inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
																left JOIN features f ON gp.feature = f.id
																where gp.idsc = '$idsc'
																and gp.size = '$sizeid'";
																$getGrowerPriceMeth2_array = mysqli_query($con, $getGrowerPriceMeth2);
																$GrowerPriceMeth2 = mysqli_fetch_assoc($getGrowerPriceMeth2_array);
																$priceGrower = sprintf('%.2f',round($GrowerPriceMeth2['price'],2));


															$sql_priceG = "select gp.id, gp.price as price,
							                gp.variation_price as vp, gp.date_ini,  gp.date_end
							                from growcard_product_price gp
							                where
							                gp.prodcutid = '$productid'
							                and gp.sizeid    = '$sizeid'
															and gp.feature   = '$featureId'
							                and gp.growerid  = '$growerid'";

							                $rs_priceG  = mysqli_query($con,$sql_priceG);
															$número_filas = mysqli_num_rows($rs_priceG);

																if($número_filas>0)
																{

									                $row_priceG = mysqli_fetch_array($rs_priceG);

																	$date_ini_G = $row_priceG['date_ini'];
																	$date_end_G = $row_priceG['date_end'];
																	$idGro = $row_priceG['id'];


																				if($date_ini_G =="" || $date_end_G=="" || $date_ini_G =="no-date" || $date_end_G=="no-date"){
																					 $priceGrower = $row_priceG['price'];
																				}else
																				{

																							 if($date_ini_G <= date('Y-m-d'))
																							 {
																								 if($date_end_G >= date('Y-m-d'))
																								 {
																								 $priceGrower = $row_priceG['price'] + $row_priceG['vp'];
																								 }else{
																									 //Fechas y vriation a cero a nivel de Grower

																									 $update_resG = "UPDATE  growcard_product_price
																																	 SET
																																	 variation_price = '0.00',
																																	 date_ini = 'no-date',
																																	 date_end = 'no-date'
																																	 WHERE
																																	 id      = '$idGro'
																																	 ";

																															 @mysqli_query($con, $update_resG);

																									 $priceGrower = sprintf('%.2f',round($GrowerPriceMeth2['price'],2));
																								 }
																							 }else{
																								 $priceGrower = sprintf('%.2f',round($GrowerPriceMeth2['price'],2));
																							 }
																				}
																	}
															}

  	return $priceGrower;
}


function calculateCost_Transp($growerid,$productid,$sizeid,$idsc,$con,$userSessionID){

	$tasaKilo = calculateKilo($userSessionID,$con);

   $getGrowerPriceMeth1 = "select gp.price as price
															from growcard_prod_price gp
															INNER JOIN growers g ON gp.growerid = g.id
															INNER JOIN product p ON gp.productid = p.id
															INNER JOIN subcategory s ON p.subcategoryid = s.id
															INNER JOIN colors c on p.color_id = c.id
															where g.active = 'active'
															and gp.growerid = '$growerid'
															and gp.productid = '$productid'
															and gp.sizeid = '$sizeid'";
  $getGrowerPriceMeth1_array = mysqli_query($con, $getGrowerPriceMeth1);
  $GrowerPriceMeth1 = mysqli_fetch_assoc($getGrowerPriceMeth1_array);
  $priceGrower = $GrowerPriceMeth1['price'];
		if($priceGrower<=0){
			$getGrowerPriceMeth2 = "select gp.id,gp.price_adm as price,
			s.name as sizename , gp.feature as feature, gp.factor,
			gp.stem_bunch , b.name as stems, gp.unit
			from grower_parameter gp
			inner JOIN sizes s ON gp.size = s.id
			inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
			left JOIN features f ON gp.feature = f.id
			where gp.idsc = '$idsc'
			and gp.size = '$sizeid'";
			$getGrowerPriceMeth2_array = mysqli_query($con, $getGrowerPriceMeth2);
			$GrowerPriceMeth2 = mysqli_fetch_assoc($getGrowerPriceMeth2_array);

			$priceBunch = $tasaKilo * $GrowerPriceMeth2['factor'];
		 	$priceSteam = $priceBunch / $GrowerPriceMeth2['stems'];
			//$a = $GrowerPriceMeth2['price'];
			$priceGrower = sprintf('%.2f',round($priceSteam,2));
		}



		if($row_price["unit"]== 39)
		{
			$priceBunch = $tasaKilo * $GrowerPriceMeth2['factor'];
			$priceSteam = $priceBunch / $GrowerPriceMeth2['stems'];
			//$a = $GrowerPriceMeth2['price'];
			$priceGrower = sprintf('%.2f',round($priceSteam,2));
	 }else{
			if($row_price["unit"]== 40){

				$priceBunch = $tasaKilo * $GrowerPriceMeth2['factor'];
				//$a = $GrowerPriceMeth2['price'];
				$priceGrower = sprintf('%.2f',round($priceBunch,2));
			}else{
				$priceBunch = $tasaKilo * $GrowerPriceMeth2['factor'];
				$priceSteam = $priceBunch / $GrowerPriceMeth2['stems'];
				//$a = $GrowerPriceMeth2['price'];
				$priceGrower = sprintf('%.2f',round($priceSteam,2));
			}

		}

  	return $priceGrower;
}



function calculatePriceCliente($growerid,$productid,$sizeid,$idsc,$userSessionID,$con,$unitID_Prod,$featureId){
	$tasaKilo = calculateKilo($userSessionID,$con);

								if($featureId!=''){
									$filterCat = "and gp.feature = '$featureId'";
								}else{ $filterCat ='';}

							 $sql_price = "select gp.id,gp.price_adm as price,
							s.name as sizename , gp.feature as feature, gp.factor,
							gp.stem_bunch , b.name as stems,
							gp.unit
							from grower_parameter gp
							inner JOIN sizes s ON gp.size = s.id
							inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
							left JOIN features f ON gp.feature = f.id
							where gp.idsc = '$idsc'
							and gp.size = '$sizeid'
							$filterCat";

							$rs_price = mysqli_query($con,$sql_price);
							$row_price = mysqli_fetch_array($rs_price);

							$row_priceS = $row_price['price'];
						  $row_priceSS= $row_price['price_card'];



							$sql_priceG = "select gp.id, gp.price as price,
							gp.variation_price as vp, gp.date_ini,  gp.date_end
							from growcard_product_price gp
							where
							gp.prodcutid = '$productid'
							and gp.sizeid    = '$sizeid'
							and gp.feature   = '$featureId'
							and gp.growerid  = '$growerid'";

							$rs_priceG  = mysqli_query($con,$sql_priceG);
							$número_filas = mysqli_num_rows($rs_priceG);

								if($número_filas>0)
								{

									$row_priceG = mysqli_fetch_array($rs_priceG);

									$date_ini_G = $row_priceG['date_ini'];
									$date_end_G = $row_priceG['date_end'];
									$idGro = $row_priceG['id'];

									if($date_ini_G =="" || $date_end_G=="" || $date_ini_G =="no-date" || $date_end_G=="no-date"){
										$row_priceS = $row_price['price'];
									 	$row_priceSS= $row_price['price'];
									}else{

										 if($date_ini_G <= date('Y-m-d'))
										 {
											 if($date_end_G >= date('Y-m-d'))
											 {
										//	 $row_priceG = $row_priceG['price'] + $row_priceG['vp'];

											 $row_priceS = $row_priceG['price'] + $row_priceG['vp'];;
											 $row_priceSS= $row_priceG['price'] + $row_priceG['vp'];;
											 }else{
												 //Fechas y vriation a cero a nivel de Grower

												 $update_resG = "UPDATE  growcard_product_price
																				 SET
																				 variation_price = '0.00',
																				 date_ini = 'no-date',
																				 date_end = 'no-date'
																				 WHERE
																				 id      = '$idGro'
																				 ";

																		 @mysqli_query($con, $update_resG);

																		 $row_priceS = $row_price['price'];
				 				 									 	$row_priceSS= $row_price['price'];
											 }
										 }else{
											 $row_priceS = $row_price['price'];
											 $row_priceSS= $row_price['price'];
										 }

									}



								}



								if($row_price["unit"]== 39)
								{
								 $priceBunch = $tasaKilo * $row_price['factor'];
								 $priceSteam = $priceBunch / $row_price['stems'];
								 $priceCalculado = sprintf('%.2f',round($priceSteam + $row_priceS,2));
							 }else{
									if($row_price["unit"]== 40){

										 $priceBunch = $tasaKilo * $row_price['factor'];
										 $priceCalculado = sprintf('%.2f',round($priceBunch + $row_priceSS,2));
									}else{
										$priceBunch = $tasaKilo * $row_price['factor'];
										$priceSteam = $priceBunch / $row_price['stems'];
										$priceCalculado = sprintf('%.2f',round($priceSteam + $row_priceS,2));
									}

								}





							return $priceCalculado;
					//	 $sql_price;
}

function detailsOrder($orderId,$con){
$selecDetailsOrder = "select order_number, del_date from buyer_orders where id = '$orderId'";
$rs_DetailOrder = mysqli_query($con,$selecDetailsOrder);
$row_DetailOrder = mysqli_fetch_array($rs_DetailOrder);

$date=date_create($row_DetailOrder['del_date']);

return 	"	Delivery Date: <strong>".date_format($date,'jS \of F\, Y')."</strong>";
}

function query_main($init,$display,$growerID,$fcat,$fcolor,$fsize,$ffeat,$fproductID){
$posicion_coincidencia = strpos($init, ',');
//se puede hacer la comparacion con 'false' o 'true' y los comparadores '===' o '!=='
if ($posicion_coincidencia === false)
{
      if($fcat!=''){
        $filterCat = "and p.subcategoryid = '$fcat'";
      }else{ $filterCat ='';}

      if($fproductID!=''){
				$filterProd2 = ", sz.id as siId, sz.name as NSsize";
				$filterProd1 = " LEFT JOIN growcard_prod_bunch_sizes  gr ON(gr.grower_id=gp.grower_id AND gr.product_id=gp.product_id) LEFT JOIN sizes sz ON gr.sizes = sz.id";
        $filterProd = "and p.id = '$fproductID'";
				$filterProd1_1 = ",sz.name ";
      }else{ $filterProd ='';}

      if($fcolor!=''){
        $fcolor = substr($fcolor, 0, -1);
        $filterColor = "and p.color_id in ($fcolor)";
        $filterColor1 = " INNER JOIN colors c on p.color_id = c.id ";
      }else{ $filterColor =''; $filterColor1 ='';}

      if($fsize!=''){
        $fsize = substr($fsize, 0, -1);
        $filterSizes = "and ps.sizes in ($fsize)";
        $filterSizes1 = " INNER JOIN growcard_prod_sizes ps ON (gp.grower_id = ps.grower_id and gp.product_id = ps.product_id) INNER join sizes sz on ps.sizes = sz.id ";
        $filterSizes2 = ", sz.id as siIId";
      }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

      if($ffeat!=''){
        $ffeat = substr($ffeat, 0, -1);
        $filterFeatures = "and f.id IN ($ffeat)";
    //    $filterFeatures1 = " LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id) LEFT JOIN features f on fe.features = f.id";
      //  $filterFeatures2 = ", f.name as fname, f.id as fid";
      }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}





    $query = "select  p.id as id, p.name as name, p.categoryid as categoryid, p.image_path as img,
    p.subcate_name as subcatename, p.subcategoryid as subcategoryid, sz1.sizes as siId,
		f.name as fname, f.id as fid $filterProd2 $filterSizes2
    from grower_product gp
    INNER JOIN growers g ON gp.grower_id = g.id
    INNER JOIN product p ON gp.product_id = p.id
    INNER JOIN subcategory s ON p.subcategoryid = s.id
		INNER JOIN growcard_prod_sizes sz1 ON ( gp.grower_id = sz1.grower_id and gp.product_id = sz1.product_id)
		LEFT JOIN grower_parameter gm ON (gm.idsc = p.subcategoryid and gm.size = sz1.sizes)
		LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id and gm.feature = fe.features)
		LEFT JOIN features f ON fe.features = f.id
    $filterColor1
    $filterSizes1
		$filterProd1
    where g.active = 'active'
    and p.status = 0
      and gp.grower_id = '$growerID'";

    $query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd.   "   group by p.id,f.id order by p.name $filterProd1_1 LIMIT $init,$display";

} else{

  if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = ", sz.id, sz.name as namesize";
		$filterProd1 = " LEFT JOIN growcard_prod_bunch_sizes  gr ON(gr.grower_id=gp.grower_id AND gr.product_id=gp.product_id) LEFT JOIN sizes sz ON gr.sizes = sz.id";
		$filterProd = "and p.id = '$fproductID'";
		$filterProd1_1 = ",sz.name ";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = " INNER JOIN colors c on p.color_id = c.id ";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
    $filterSizes = "and ps.sizes in ($fsize)";
    $filterSizes1 = " INNER JOIN growcard_prod_sizes ps ON (gp.grower_id = ps.grower_id and gp.product_id = ps.product_id) INNER join sizes sz on ps.sizes = sz.id ";
    $filterSizes2 = ", sz.id as siId";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
    $filterFeatures = "and f.id IN ($ffeat)";
    $filterFeatures1 = " LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id) LEFT JOIN features f on fe.features = f.id";
    $filterFeatures2 = ", f.name as fname, f.id as fid";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

  $limites = explode(",",$init);
  $Linicio = $limites[0];
  $Lfin = $limites[1];

  if($Lfin==0){
    $Lfin=100;
  }else{
      $Lfin = $limites[1]- $limites[0];
  }

	$query = "select  p.id as id, p.name as name, p.categoryid as categoryid, p.image_path as img,
	p.subcate_name as subcatename, p.subcategoryid as subcategoryid $filterProd2 $filterSizes2 $filterFeatures2
	from grower_product gp
	INNER JOIN growers g ON gp.grower_id = g.id
	INNER JOIN product p ON gp.product_id = p.id
	INNER JOIN subcategory s ON p.subcategoryid = s.id
	INNER JOIN growcard_prod_sizes sz ON ( gp.grower_id = sz.grower_id and gp.product_id = sz.product_id)
	LEFT JOIN grower_parameter gm ON (gm.idsc = p.subcategoryid and gm.size = sz.sizes)
	LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id and gm.feature = fe.features)
	LEFT JOIN features f ON fe.features = f.id
	$filterColor1
	$filterSizes1
	$filterFeatures1
	$filterProd1
	where g.active = 'active'
	and p.status = 0
		and gp.grower_id = '$growerID'";

  $query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd.   "  order by p.name $filterProd1_1 LIMIT $Linicio,$Lfin";
}
    return $query;
}


function numberRecord($con,$growerID,$fcat,$fcolor,$fsize,$ffeat,$fproductID){

  if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = ", sz.id, sz.name as namesize";
		$filterProd1 = " LEFT JOIN growcard_prod_bunch_sizes  gr ON(gr.grower_id=gp.grower_id AND gr.product_id=gp.product_id) LEFT JOIN sizes sz ON gr.sizes = sz.id";
		$filterProd = "and p.id = '$fproductID'";
		$filterProd1_1 = ",sz.name ";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = " INNER JOIN colors c on p.color_id = c.id ";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
    $filterSizes = "and ps.sizes in ($fsize)";
    $filterSizes1 = " INNER JOIN growcard_prod_sizes ps ON (gp.grower_id = ps.grower_id and gp.product_id = ps.product_id) INNER join sizes sz on ps.sizes = sz.id ";
    $filterSizes2 = ", sz.id as siId";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
    $filterFeatures = "and f.id IN ($ffeat)";
    $filterFeatures1 = " LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id) LEFT JOIN features f on fe.features = f.id";
    $filterFeatures2 = ", f.name as fname, f.id as fid";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

        $sel_pagina = "select  p.id as id, p.name as name, p.categoryid as categoryid, p.image_path as img,
				p.subcate_name as subcatename, p.subcategoryid as subcategoryid $filterProd2 $filterSizes2 $filterFeatures2
				from grower_product gp
				INNER JOIN growers g ON gp.grower_id = g.id
				INNER JOIN product p ON gp.product_id = p.id
				INNER JOIN subcategory s ON p.subcategoryid = s.id
				$filterColor1
				$filterSizes1
				$filterFeatures1
				$filterProd1
				where g.active = 'active'
				and p.status = 0
					and gp.grower_id = '$growerID'";

      $sel_pagina1 = $sel_pagina.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd;

    $rs_pagina    = mysqli_query($con, $sel_pagina1);
    $total_pagina = mysqli_num_rows($rs_pagina);

   return $num_recordMB   = $total_pagina;
}

function query_mainGrowers($init,$display,$growerID,$fcat,$fcolor,$fsize,$ffeat,$fproductID,$fSale)
{

if($fSale!=''){

	if($fcat!=''){
		$filterCat = "and p.subcategoryid = '$fcat'";
	}else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = " and p.id = '$fproductID'";
		$filterProd1 = "";
		$filterProd = "";
		$filterProd1_1 = "";
		$gruop_by1 = "";
	}else{ $filterProd ='';}

	if($fcolor!=''){
		$fcolor = substr($fcolor, 0, -1);
		$filterColor = "and p.color_id in ($fcolor)";
		$filterColor1 = "";
	}else{ $filterColor =''; $filterColor1 ='';}

	if($fsize!=''){
		$fsize = substr($fsize, 0, -1);
		$filterSizes = "and gp.sizes in ($fsize)";
		$filterSizes1 = "";
		$filterSizes2 = "";
	}else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

	if($ffeat!=''){
		$ffeat = substr($ffeat, 0, -1);
		$filterFeatures = "and gp.grower_id in ($ffeat)";
		$filterFeatures1 = "";
		$filterFeatures2 = "";
	}else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}


	$query = "
					select gp.sizes, p.subcategoryid , p.name , p.color_id , p.image_path ,p.categoryid, p.id, d.id,
					d.price, d.date_ini, d.date_end, d.offer_price, d.growerid, d.feature, d.prodcutid
					from growcard_product_price d
					left join product p on d.prodcutid = p.id
					left join growcard_prod_bunch_sizes gp on p.id = gp.product_id
					left join growers g on gp.grower_id = g.id
					where
					g.active ='active'
					and g.market_place = '1'
					and p.status = 0
					and d.price > 0.00
					and d.offer_price > 0.00
					and CURDATE() BETWEEN d.date_ini AND  d.date_end
				";

	$group_by = " GROUP BY " .$gruop_by1. " p.categoryid , p.subcategoryid , p.name ,  p.color_id,  d.growerid ";

	$query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd2.' '.$group_by.   "LIMIT $init,$display";



}else{
$posicion_coincidencia = strpos($init, ',');
//se puede hacer la comparacion con 'false' o 'true' y los comparadores '===' o '!=='
if ($posicion_coincidencia === false)
{

$gruop_by1 = " ";

			if($fcat!=''){
        $filterCat = "and p.subcategoryid = '$fcat'";
      }else{ $filterCat ='';}

      if($fproductID!=''){
				$filterProd2 = " and p.id = '$fproductID'";
				$filterProd1 = "";
        $filterProd = "";
				$filterProd1_1 = "";
				$gruop_by1 = "";
      }else{ $filterProd ='';}

      if($fcolor!=''){
        $fcolor = substr($fcolor, 0, -1);
        $filterColor = "and p.color_id in ($fcolor)";
        $filterColor1 = "";
      }else{ $filterColor =''; $filterColor1 ='';}

      if($fsize!=''){
        $fsize = substr($fsize, 0, -1);
        $filterSizes = "and gp.sizes in ($fsize)";
        $filterSizes1 = "";
        $filterSizes2 = "";
      }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

      if($ffeat!=''){
        $ffeat = substr($ffeat, 0, -1);
				$filterFeatures = "and gp.grower_id in ($ffeat)";
		    $filterFeatures1 = "";
		    $filterFeatures2 = "";
      }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

    $query = "
						select gp.sizes, p.subcategoryid , p.name , p.color_id , p.image_path ,g.growers_name, p.categoryid, p.id
						from product p
						left join growcard_prod_bunch_sizes gp on p.id = gp.product_id
						left join growers g         on gp.grower_id = g.id
						where g.active       ='active'
						and g.market_place = '1'
						and p.status       = 0
						";

		$group_by = " GROUP BY " .$gruop_by1. " p.categoryid , p.subcategoryid , p.name ,  p.color_id ";

    $query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd2.' '.$group_by.   "LIMIT $init,$display";

} else{

$gruop_by1 = " ";

	if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = " and p.id = '$fproductID'";
		$filterProd1 = "";
		$filterProd = "";
		$filterProd1_1 = "";
			$gruop_by1 = "";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = "";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
		$filterSizes = "and gp.sizes in ($fsize)";
		$filterSizes1 = "";
		$filterSizes2 = "";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
		$filterFeatures = "and gp.grower_id in ($ffeat)";
    $filterFeatures1 = "";
    $filterFeatures2 = "";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

  $limites = explode(",",$init);
  $Linicio = $limites[0];
  $Lfin = $limites[1];

  if($Lfin==0){
    $Lfin=100;
  }else{
      $Lfin = $limites[1]- $limites[0];
  }

	$query = "select gp.sizes,p.subcategoryid , p.name , p.color_id , p.image_path ,g.growers_name, p.categoryid, p.id
   from product p
   left join growcard_prod_bunch_sizes gp on p.id = gp.product_id
   left join growers g         on gp.grower_id = g.id
  where g.active       ='active'
    and g.market_place = '1'
    and p.status       = 0
    ";

		$group_by = " GROUP BY " .$gruop_by1. " p.categoryid , p.subcategoryid , p.name ,  p.color_id ";
		$query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd2.' '.$group_by.   "LIMIT $init,$display";

	}

 }
   return $query;
}

function numberRecordGrowers($con,$growerID,$fcat,$fcolor,$fsize,$ffeat,$fproductID,$fSale){

if($fSale!=''){


	if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = " and p.id = '$fproductID'";
		$filterProd1 = "";
		$filterProd = "";
		$filterProd1_1 = "";
		$gruop_by1 = "";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = "";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
		$filterSizes = "and gp.sizes in ($fsize)";
		$filterSizes1 = "";
		$filterSizes2 = "";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
    $filterFeatures = "and gp.grower_id in ($ffeat)";
    $filterFeatures1 = "";
    $filterFeatures2 = "";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

        $sel_pagina = "
			select gp.sizes, p.subcategoryid , p.name , p.color_id , p.image_path ,g.growers_name, p.categoryid, p.id, d.id, d.price, d.date_ini, d.date_end
from growcard_product_price d
left join product p on d.prodcutid = p.id
left join growcard_prod_bunch_sizes gp on p.id = gp.product_id
left join growers g on gp.grower_id = g.id
where
g.active ='active'
and g.market_place = '1'
and p.status = 0
and d.price > 0.00
and d.offer_price > 0.00
and CURDATE() BETWEEN d.date_ini AND  d.date_end
";

$group_by = " GROUP BY " .$gruop_by1. " p.categoryid , p.subcategoryid , p.name ,  p.color_id, d.growerid ";

       $sel_pagina1 = $sel_pagina.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd2.' '.$group_by;

    $rs_pagina    = mysqli_query($con, $sel_pagina1);
    $total_pagina = mysqli_num_rows($rs_pagina);

}
	else{


	$gruop_by1 = " ";

  if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = " and p.id = '$fproductID'";
		$filterProd1 = "";
		$filterProd = "";
		$filterProd1_1 = "";
		$gruop_by1 = "";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = "";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
		$filterSizes = "and gp.sizes in ($fsize)";
		$filterSizes1 = "";
		$filterSizes2 = "";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
    $filterFeatures = "and gp.grower_id in ($ffeat)";
    $filterFeatures1 = "";
    $filterFeatures2 = "";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

        $sel_pagina = "select gp.sizes,p.subcategoryid , p.name , p.color_id , p.image_path ,g.growers_name, p.categoryid, p.id
   from product p
   left join growcard_prod_bunch_sizes gp on p.id = gp.product_id
   left join growers g         on gp.grower_id = g.id
  where g.active       ='active'
    and g.market_place = '1'
    and p.status       = 0
";

$group_by = " GROUP BY " .$gruop_by1. " p.categoryid , p.subcategoryid , p.name ,  p.color_id ";

       $sel_pagina1 = $sel_pagina.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd2.' '.$group_by;

    $rs_pagina    = mysqli_query($con, $sel_pagina1);
    $total_pagina = mysqli_num_rows($rs_pagina);


}

 return $num_recordMB   = $total_pagina;
}

?>
