<?php
session_start();
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 28 Dec 2021
**/
?>

<!-- Footer -->
<footer id="footer" class="border-top">
	<div class="container pt-6 pb-1">

		<div class="row gutters-xl">

			<div class="col-12 col-lg-7 mb-5">

				<div class="mb-4">
					<img src="https://www.freshlifefloral.com/hubfs/fresh%20life%20logos/logo.jpeg" width="250" height="47" alt="...">
				</div>

				<p class="text-dark">
				Our customer's challenges and concerns continually shape our team's culture, pushing us closer to become the best alternative to fulfill your various flower sourcing needs every day!
				</p>

				<p class="text-dark">
					&copy; Fresh Life Floral Inc.
				</p>

			</div>

			<div class="col-12 col-lg-5">

				<div class="row">

					<div class="col-6 fs--18">

						<h4 class="h5">
						Fresh Life Floral
						</h4>
						<ul class="list-unstyled mb-5">

							<li class="list-item">
								<a href="#!">About Us</a>
							</li>

							<li class="list-item">
								<a href="#!">Careers</a>
							</li>

							<li class="list-item">
								<a href="#!">Contact Us</a>
							</li>

						</ul>



						<h4 class="h5">
							Smarty Support
						</h4>
						<ul class="list-unstyled mb-5">

							<li class="list-item">
								<a href="#!" target="_blank">Help Center</a>
							</li>

							<li class="list-item">
								<a href="#!">Direct Contact</a>
							</li>

						</ul>

					</div>

					<div class="col-6">

						<h4 class="h5">
							Smarty Legal
						</h4>
						<ul class="list-unstyled mb-5">

							<li class="list-item">
								<a href="#!">Terms &amp; Conditions</a>
							</li>

							<li class="list-item">
								<a href="#!">Privacy</a>
							</li>

						</ul>



						<h4 class="h5">
							Smarty Social
						</h4>
						<ul class="list-unstyled mb-5">

							<li class="list-item">
								<a href="https://www.facebok.com" target="_blank" rel="noopener">Facebook</a>
							</li>

							<li class="list-item">
								<a href="https://www.twitter.com" target="_blank" rel="noopener">Twitter</a>
							</li>

							<li class="list-item">
								<a href="https://www.linkedin.com" target="_blank" rel="noopener">Linkedin</a>
							</li>

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

</footer>
<!-- /Footer -->


</div><!-- /#wrapper -->

		<script src="../back-end/assets/js/core.min.js">
		</script>
		<script src="../back-end/assets/js/vendor_bundle.min.js">
		</script>


 <script type="text/javascript">var plugin_path = '/includes/assets/plugins/';</script>
 <script type="text/javascript" src="/includes/assets/js/app_2.js"></script>
 <script type="text/javascript" src="/includes/assets/js/custom.js"></script>
 <script type="text/javascript" src="/includes/assets/plugins/typeahead.bundle.js"></script>
 <script type="text/javascript" src="../back-end/assets/js/custom.js"></script>
		<!--

            [SOW Ajax Navigation Plugin] [AJAX ONLY, IF USED]
            If you have specific page js files, wrap them inside #page_js_files
            Ajax Navigation will use them for this page!
            This way you can load this page in a normal way and/or via ajax.
            (you can change/add more containers in sow.config.js)

            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            NOTE: This is mostly for frontend, full ajax navigation!
            Admin Panels use a backend, so the content should be served without
            menu, header, etc! Else, the ajax has no reason to be used because will
            not minimize server load!

            /documentation/plugins-sow-ajax-navigation.html
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        -->
		<div id="page_js_files">
			<!-- specific page javascript files here -->
		</div>
	</body>
</html>
