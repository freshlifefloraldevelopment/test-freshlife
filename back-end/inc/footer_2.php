<footer class="aside-primary text-white" id="footer">
				<div class="p-3 fs--14">
					&copy; 2021 - Fresh Life Floral Inc.
					<div class="d-inline-block float-end dropdown">
						<ul class="list-inline m-0">
							<!-- LANGUAGE -->
							<li class="dropdown list-inline-item m-0">
								<a aria-expanded="false" aria-haspopup="true" class="d-inline-block" data-toggle="dropdown" href="#!" id="topDDLanguage"><i class="flag flag-us"></i> <span class=
								"pl-2 pr-2">ENGLISH</span></a>
								<div aria-labelledby="topDDLanguage" class="dropdown-menu fs--13 px-1 pt-1 pb-0 m-0 max-h-50vh scrollable-vertical dropdown-menu-right">
									<a class="active dropdown-item text-muted text-truncate line-height-1 rounded p--12 mb-1" href="#!"><i class="flag flag-us"></i> ENGLISH</a> <a class=
									"dropdown-item text-muted text-truncate line-height-1 rounded p--12 mb-1" href="#!"><i class="flag flag-de"></i> GERMAN</a> <a class=
									"dropdown-item text-muted text-truncate line-height-1 rounded p--12 mb-1" href="#!"><i class="flag flag-fr"></i> FRANCAISE</a>
								</div>
							</li><!-- /LANGUAGE -->
							<!-- CURRENCY -->
							<li class="dropdown list-inline-item m-0">
								<span class="text-muted">/</span><!-- optional separator -->
								 <a aria-expanded="false" class="d-inline-block" data-toggle="dropdown" href="#" id="topDDCurrency"><span class="pl-2 pr-2">USD</span></a>
								<div aria-labelledby="topDDCurrency" class="dropdown-menu text-center fs--13 px-1 pt-1 pb-0 m-0 max-h-50vh w-auto scrollable-vertical dropdown-menu-right">
									<a class="active dropdown-item text-muted text-truncate line-height-1 rounded pt--12 pb--12 mb-1" href="#!">USD</a> <a class=
									"dropdown-item text-muted text-truncate line-height-1 rounded pt--12 pb--12 mb-1" href="#!">EUR</a> <a class="dropdown-item text-muted text-truncate line-height-1 rounded pt--12 pb--12 mb-1"
									href="#!">GBP</a>
								</div>
							</li><!-- /CURRENCY -->
						</ul>
					</div>
				</div>
			</footer><!-- /FOOTER -->
		</div><!-- /#wrapper -->
                
<script src="../back-end/assets/js/core.min.js">
		</script>
		<script src="../back-end/assets/js/vendor_bundle.min.js">
		</script>

		<script type="text/javascript">var plugin_path = '/includes/assets/plugins/';</script>
		<script type="text/javascript" src="/includes/assets/js/app_2.js"></script>                
	</body>
</html>
