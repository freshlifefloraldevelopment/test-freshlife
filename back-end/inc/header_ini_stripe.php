<?php
if ($_SERVER['SERVER_ADDR'] != '50.62.68.1') {
    @session_start();
    if ($_SESSION['client_session'] != 1) {

        if ($_SERVER['SERVER_ADDR'] == '50.62.68.1') {

            header("Location:https://freshlifefloral.com"); // Uncomment when completed
        } else if ($_SERVER['SERVER_ADDR'] == '35.227.110.85') {
            header("Location:https://app.freshlifefloral.com");

        }
    }
}
 $query_login = "select id,name,menu from control_menu where id = 1  ";

  $menu_login    = mysqli_query($con, $query_login);
  $valida_menu = mysqli_fetch_assoc($menu_login);

  $control_menu = $valida_menu['menu'];
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>
			Fresh Life Floral
		</title>
		<meta content="Fresh Life Floral" name="description">
		<meta content="width=device-width, maximum-scale=5, initial-scale=1, user-scalable=0" name="viewport"><!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<!-- up to 10% speed up for external res -->
		<link href="https://fonts.googleapis.com/" rel="dns-prefetch">
		<link href="https://fonts.gstatic.com/" rel="dns-prefetch">
		<link href="https://fonts.googleapis.com/" rel="preconnect">
		<link href="https://fonts.gstatic.com/" rel="preconnect"><!-- preloading icon font is helping to speed up a little bit -->
		<link href="/back-end/assets/fonts/flaticon/Flaticon.woff2" rel="preload" type="font/woff2"><!-- non block rendering : page speed : js = polyfill for old browsers missing `preload` -->
    <link href="/back-end/assets/css/vendor.fullcalendar.min.css" rel="stylesheet" type="text/css"/>
    <link href="/back-end/assets/css/core.min.css" rel="stylesheet">
		<link href="/back-end/assets/css/vendor_bundle.min.css" rel="stylesheet">
		<link href="/back-end/assets/css/style.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet"><!-- favicon -->
	 <link rel="shortcut icon" type="image/ico" href="https://app.freshlifefloral.com/images/favicon.ico">
		<link href="/back-end/assets/images/manifest/manifest.json" rel="manifest">
		<meta content="#377dff" name="theme-color">
	</head>
	<body class="layout-admin">
		<div class="d-flex align-items-stretch flex-column" id="wrapper">
			<!--
                HEADER

                .header-match-aside-primary
            -->
