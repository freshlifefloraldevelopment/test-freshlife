
<?php
session_start();
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 30 Dec 2021
**/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fresh Life Floral</title>
  <meta name="description" content="Fresh Life Floral">

      <meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1, user-scalable=0">
      <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

      <!-- up to 10% speed up for external res -->
      <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
      <link rel="dns-prefetch" href="https://fonts.gstatic.com/">
      <link rel="preconnect" href="https://fonts.googleapis.com/">
      <link rel="preconnect" href="https://fonts.gstatic.com/">
      <!-- preloading icon font is helping to speed up a little bit -->
      <link rel="preload" href="assets/fonts/flaticon/Flaticon.woff2" as="font" type="font/woff2" crossorigin>

      <link rel="stylesheet" href="assets/css/core.min.css">
      <link rel="stylesheet" href="assets/css/vendor_bundle.min.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap">

  <!-- favicon -->
  <link rel="shortcut icon" href="https://www.freshlifefloral.com/hubfs/FAVICON.jpg">
  <link rel="apple-touch-icon" href="https://www.freshlifefloral.com/hubfs/FAVICON.jpg">

  <meta name="theme-color" content="#377dff">

</head>
	<body class="header">
		<div id="wrapper">
			<!--
                HEADER

                .header-match-aside-primary
            -->
            <!-- HEADER -->
      			<header id="header" class="shadow-xs">

      				<!-- NAVBAR -->
      				<div class="container position-relative">


      					<!--

      						[SOW] SEARCH SUGGEST PLUGIN
      						++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++
      						PLEASE READ DOCUMENTATION
      						documentation/plugins-sow-search-suggest.html
      						++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++

      					-->





      					<nav class="navbar navbar-expand-lg navbar-light justify-content-lg-between justify-content-md-inherit">

      						<div class="align-items-start">

      							<!-- mobile menu button : show -->
      							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMainNav" aria-controls="navbarMainNav" aria-expanded="false" aria-label="Toggle navigation">
      								<svg width="25" viewBox="0 0 20 20">
      									<path d="M 19.9876 1.998 L -0.0108 1.998 L -0.0108 -0.0019 L 19.9876 -0.0019 L 19.9876 1.998 Z"></path>
      									<path d="M 19.9876 7.9979 L -0.0108 7.9979 L -0.0108 5.9979 L 19.9876 5.9979 L 19.9876 7.9979 Z"></path>
      									<path d="M 19.9876 13.9977 L -0.0108 13.9977 L -0.0108 11.9978 L 19.9876 11.9978 L 19.9876 13.9977 Z"></path>
      									<path d="M 19.9876 19.9976 L -0.0108 19.9976 L -0.0108 17.9976 L 19.9876 17.9976 L 19.9876 19.9976 Z"></path>
      								</svg>
      							</button>

      							<!--
      								Logo : height: 70px max
      							-->

      							<a class="m-0" href="https://www.freshlifefloral.com/homenewfresh?hs_preview=pwrwtbzo-34363123332">
      								<img src="https://www.freshlifefloral.com/hubfs/LOGO%20FRESH%20LIFE.png" width="150" height="30" alt="Fresh Life Floral">
      							</a>

      						</div>





      						<!-- Menu -->
      						<!--

      							Dropdown Classes (should be added to primary .dropdown-menu only, dropdown childs are also affected)
      								.dropdown-menu-dark 		- dark dropdown (desktop only, will be white on mobile)
      								.dropdown-menu-hover 		- open on hover
      								.dropdown-menu-clean 		- no background color on hover
      								.dropdown-menu-invert 		- open dropdown in oposite direction (left|right, according to RTL|LTR)
      								.dropdown-menu-uppercase 	- uppercase text (font-size is scalled down to 13px)
      								.dropdown-click-ignore 		- keep dropdown open on inside click (useful on forms inside dropdown)

      								Repositioning long dropdown childs (Example: Pages->Account)
      									.dropdown-menu-up-n100 		- open up with top: -100px
      									.dropdown-menu-up-n100 		- open up with top: -150px
      									.dropdown-menu-up-n180 		- open up with top: -180px
      									.dropdown-menu-up-n220 		- open up with top: -220px
      									.dropdown-menu-up-n250 		- open up with top: -250px
      									.dropdown-menu-up 			- open up without negative class


      								Dropdown prefix icon (optional, if enabled in variables.scss)
      									.prefix-link-icon .prefix-icon-dot 		- link prefix
      									.prefix-link-icon .prefix-icon-line 	- link prefix
      									.prefix-link-icon .prefix-icon-ico 		- link prefix
      									.prefix-link-icon .prefix-icon-arrow 	- link prefix

      								.nav-link.nav-link-caret-hide 	- no dropdown icon indicator on main link
      								.nav-item.dropdown-mega 		- required ONLY on fullwidth mega menu

      								Mobile animation - add to .navbar-collapse:
      								.navbar-animate-fadein
      								.navbar-animate-fadeinup
      								.navbar-animate-bouncein

      						-->
      						<div class="collapse navbar-collapse navbar-animate-fadein" id="navbarMainNav">


      							<!-- MOBILE MENU NAVBAR -->
      							<div class="navbar-xs d-none"><!-- .sticky-top -->

      								<!-- mobile menu button : close -->
      								<button class="navbar-toggler pt-0" type="button" data-toggle="collapse" data-target="#navbarMainNav" aria-controls="navbarMainNav" aria-expanded="false" aria-label="Toggle navigation">
      									<svg width="20" viewBox="0 0 20 20">
      										<path d="M 20.7895 0.977 L 19.3752 -0.4364 L 10.081 8.8522 L 0.7869 -0.4364 L -0.6274 0.977 L 8.6668 10.2656 L -0.6274 19.5542 L 0.7869 20.9676 L 10.081 11.679 L 19.3752 20.9676 L 20.7895 19.5542 L 11.4953 10.2656 L 20.7895 0.977 Z"></path>
      									</svg>
      								</button>

      								<!--
      									Mobile Menu Logo
      									Logo : height: 70px max
      								-->
      								<a class="navbar-brand" href="https://www.freshlifefloral.com/homenewfresh?hs_preview=pwrwtbzo-34363123332">
      									<img src="https://www.freshlifefloral.com/hubfs/LOGO%20FRESH%20LIFE.png" width="150" height="30" alt="Fresh Life Floral">

      								</a>



      							</div>
      							<!-- /MOBILE MENU NAVBAR -->



      							<ul class="navbar-nav">
      															<!-- Menu -->
      								<!--

      									Dropdown Classes (should be added to primary .dropdown-menu only, dropdown childs are also affected)
      										.dropdown-menu-dark 		- dark dropdown (desktop only, will be white on mobile)
      										.dropdown-menu-hover 		- open on hover
      										.dropdown-menu-clean 		- no background color on hover
      										.dropdown-menu-invert 		- open dropdown in oposite direction (left|right, according to RTL|LTR)
      										.dropdown-menu-uppercase 	- uppercase text (font-size is scalled down to 13px)
      										.dropdown-click-ignore 		- keep dropdown open on inside click (useful on forms inside dropdown)

      										Repositioning long dropdown childs (Example: Pages->Account)
      											.dropdown-menu-up-n100 		- open up with top: -100px
      											.dropdown-menu-up-n100 		- open up with top: -150px
      											.dropdown-menu-up-n180 		- open up with top: -180px
      											.dropdown-menu-up-n220 		- open up with top: -220px
      											.dropdown-menu-up-n250 		- open up with top: -250px
      											.dropdown-menu-up 			- open up without negative class


      										Dropdown prefix icon (optional, if enabled in variables.scss)
      											.prefix-link-icon .prefix-icon-dot 		- link prefix
      											.prefix-link-icon .prefix-icon-line 	- link prefix
      											.prefix-link-icon .prefix-icon-ico 		- link prefix
      											.prefix-link-icon .prefix-icon-arrow 	- link prefix

      										.nav-link.nav-link-caret-hide 	- no dropdown icon indicator on main link
      										.nav-item.dropdown-mega 		- required ONLY on fullwidth mega menu

      								-->
      								<!-- mobile only image + simple search (d-block d-sm-none) -->



      								<!-- home -->
      								<li class="nav-item m-4">

      									<li class="nav-item font-weight-normal" role="none">
      										<a href="https://www.freshlifefloral.com" id="mainNavHome" class="nav-link" role="menuitem">
      											Home
      										</a>
      									</li>


      									<li class="nav-item font-weight-normal">
      										<a href="https://www.freshlifefloral.com/aboutus" id="mainNavHome" class="nav-link" role="menuitem">
      											Abous Us
      										</a>
      									</li>

      									<li class="nav-item font-weight-normal">
      										<a href="https://www.freshlifefloral.com/service-page" id="mainNavHome" class="nav-link" role="menuitem">
      											Services
      										</a>
      									</li>

      									<li class="nav-item font-weight-normal">
      										<a href="https://app.freshlifefloral.com/en/variety-page.php" id="mainNavHome" class="nav-link" role="menuitem">
      											Varieties
      										</a>
      									</li>

      									<li class="nav-item font-weight-normal">
      										<a href="https://app.freshlifefloral.com/en/growers-page.php"id="mainNavHome" class="nav-link" role="menuitem">
      											Growers
      										</a>
      									</li>


      									<li class="nav-item font-weight-normal">
      										<a href="https://www.freshlifefloral.com/new-blog" id="mainNavHome" class="nav-link" role="menuitem">
      											Blog
      										</a>
      									</li>

      									<li class="nav-item font-weight-normal">
      										<a href="https://www.freshlifefloral.com/contact" id="mainNavHome" class="nav-link" role="menuitem">
      											Contact Us
      										</a>
      									</li>


      								</li>






      							</ul>

      						</div>





      						<!-- OPTIONS -->
      						<ul class="list-inline list-unstyled mb-0 d-flex align-items-end">

      							<!-- join us button -->
      							<li class="list-inline-item mx-1 dropdown">

      								<a href="https://app.freshlifefloral.com/login.php" class="btn btn-pill btn-sm btn-primary">

      									<!-- visible : mobile only (including small screen : iPhone 5) -->
      									<span class="d-block d-sm-none">
      										Sign In
      									</span>

      									<!-- visible : desktop only -->
      									<span class="d-none d-sm-block font-weight-normal">
      										<i class="fi fi-layers"></i>
      										<span class="d-inline-block pl-2 pr-2">
      											Sign In
      										</span>
      									</span>
      								</a>
      							</li>

      						</ul>
      						<!-- /OPTIONS -->



      					</nav>

      				</div>
      				<!-- /NAVBAR -->

      			</header>
      			<!-- /HEADER -->
