
<aside class="aside-start aside-primary aside-hide-xs d-flex flex-column h-auto" id="aside-main">
					<!--
                        LOGO
                        visibility : desktop only
                    -->
					<div class="d-none d-sm-block">
						<div class="clearfix d-flex justify-content-between">
							<!-- Logo : height: 60px max -->
							<a class="align-self-center navbar-brand p-3" href="index.html"><img alt="Fresh Life Floral" src="../back-end/images/logo.jpeg" width="100%" style="margin-bottom: 30px;"></a>
						</div>
					</div><!-- /LOGO -->
					<div class="aside-wrapper scrollable-vertical scrollable-styled-light align-self-baseline h-100 w-100">
						<!--

                            All parent open navs are closed on click!
                            To ignore this feature, add .js-ignore to .nav-deep

                            Links height (paddings):
                                .nav-deep-xs
                                .nav-deep-sm
                                .nav-deep-md    (default, ununsed class)

                            .nav-deep-hover     hover background slightly different
                            .nav-deep-bordered  bordered links


                            ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            IMPORTANT NOTE:
                                Curently using ajax navigation!
                                remove .js-ajax class to have regular links!
                            ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        -->
						<nav class="nav-deep nav-deep-dark nav-deep-hover pb-5">
							<ul class="nav flex-column">
								<!--li class="nav-item <?php if($m==2){echo 'active';} ?>">
									<a class="nav-link" href="/buyer/buyers-account.php"><i class="fi fi-dashboard"></i> <b>Dashboard</b></a>
								</li-->

								<li  class="nav-item">
                                                                <!-- href="../en/variety-page.php" -->
                                                                <!-- data-toggle="modal" data-target=".buying_method_modal"  -->
                                                                <!-- <a class="nav-link active" href="/buyer/ordersnd.php" >
                                                                <i class="main-icon fa fa-cog fa-spin"></i> <span>Create New Order</span>
                                                                <span class="badge badge-success badge-pill">New</span>

											</a> -->




									</li>


								<li  class="nav-item">
                                                                <!--href="../en/variety-page.php" -->
											<a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target=".orders_method_modal">
													<i class="main-icon fa fa-cart-plus fa-spin"></i> <span>Market Place</span>
													<span class="badge badge-warning badge-pill">New</span>

											</a>



									</li>

								<li class="nav-item">
									<a class="nav-link" href="#"><span class="group-icon float-end">
										<i class="fi fi-arrow-end-slim"></i> <i class="fi fi-arrow-down-slim"></i></span> <i class="fi fi-users"></i> Customer</a>
									<ul class="nav flex-column">
										<li class="nav-item">
											<a class="nav-link" href="/buyer/customer_list.php">Customer List</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/buyer/client_calendar_special.php">Customer Order </a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/buyer/card_price_gen.php">Price List</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/buyer/customerwebshop.php">Customer webshop</a>
										</li>

									</ul>
								</li>


								<li class="nav-item">
									<a class="nav-link" href="#"><span class="group-icon float-end">
										<i class="fi fi-arrow-end-slim"></i> <i class="fi fi-arrow-down-slim"></i></span> <i class="fa fa-file-text"></i> Report</a>
									<ul class="nav flex-column">
										<li class="nav-item">
											<a class="nav-link" href="/buyer/buyer_invoices.php">Invoice</a>
										</li>
									</ul>

									<ul class="nav flex-column">
										<li class="nav-item">
                                                                                    <a class="nav-link" href="/buyer/buyer_orders_list.php">Orders</a>
										</li>
									</ul>

									<!--ul class="nav flex-column">
										<li class="nav-item">
                                                                                    <a class="nav-link" href="/growers/growers-page.php">Growers</a>
										</li>
									</ul-->
								</li>

							</ul>
						</nav>
					</div>
				</aside><!-- /SIDEBAR -->
