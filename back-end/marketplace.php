<?php
if(!$growerID_Prod){
	header("Location: ../en/variety-page.php");
	exit();
}
include('inc/header.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->
				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Marketplace
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
							Arrival date: <a href="#" class="datepicker link-muted"
														data-layout-rounded="false"
														data-title="Smarty Datepicker"
														data-show-weeks="true"
														data-today-highlight="true"
														data-today-btn="true"
														data-autoclose="true"
														data-date-start="today"
														data-format="MM/DD/YYYY"
														data-quick-locale='{
															"days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
															"daysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
															"daysMin": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
															"months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
															"monthsShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
															"today": "Today",
															"clear": "Clear",
															"titleFormat": "MM yyyy"
														}'
														data-id="1"
														data-ajax-url="php/demo.ajax_request.php"
														data-ajax-params="['action','date_change']['section','customer_invoice']"
														data-ajax-method="POST"

														data-toast-success="Sucessfully Updated!"
														data-toast-position="top-center">
													08/08/2020
												</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>
						<div class="card fs--18 pt-2 pb-3 bg-white shadow-md rounded mb-3 d-flex b-0 p--20">
	<form 	action="open-request.php"
							method="GET"
							data-autosuggest="on"
							data-mode="json"
							data-json-max-results='10'
							data-json-related-title='Search Varieties'
							data-json-related-item-icon='fi fi-star-empty'
							data-json-suggest-title='Suggestions for you'
							data-json-suggest-noresult='No results for'
							data-json-suggest-item-icon='fi fi-search'
							data-json-suggest-min-score='5'
							data-json-highlight-term='true'
							data-contentType='application/json; charset=utf-8'
							data-dataType='json'

							data-container="#sow-search-container"
							data-input-min-length="2"
							data-input-delay="100"
							data-related-keywords=""
							data-related-url="_ajax/open_request_releated_data.json"
							data-suggest-url="_ajax/open_request_data_suggestions.json"
							data-related-action="related_get"
							data-suggest-action="suggest_get"
							class="js-ajax-search sow-search sow-search-mobile-float d-flex-1-1-auto mx-4">
						<div class="sow-search-input w-100 d-flex align-items-center">

							<div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

								<input placeholder="Search Varieties..." name="s" type="text" class="form-control-sow-search form-control form-control-pill b-0 bg-gray-100" value="" autocomplete="off">

								<span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit" class="btn btn-primary btn-noshadow m-0 px-2 py-1 b-0 bg-transparent text-muted">
										<i class="fi fi-search fs--20"></i>
									</button>

									<!-- close : mobile only (d-inline-block d-lg-none) -->
									<a href="javascript:;" class="btn-sow-search-toggler btn btn-light btn-noshadow m-0 px-2 py-1 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

							</div>

						</div>

						<!-- search suggestion container -->
						<div class="sow-search-container rounded-xl w-100 p-0 hide shadow-md" id="sow-search-container">
							<div class="sow-search-container-wrapper rounded-xl">

								<!-- main search container -->
								<div class="sow-search-loader p--15 text-center hide">
									<i class="fi fi-circle-spin fi-spin text-muted fs--30"></i>
								</div>

								<!--
									AJAX CONTENT CONTAINER
									SHOULD ALWAYS BE AS IT IS : NO COMMENTS OR EVEN SPACES!
								--><div class="sow-search-content rounded w-100 scrollable-vertical"></div>

							</div>
						</div>
						<!-- /search suggestion container -->

						<!--

							overlay combinations:
								overlay-dark opacity-* [1-9]
								overlay-light opacity-* [1-9]

						-->
						<div class="sow-search-backdrop overlay-dark opacity-3 hide"></div>

					</form>
					<!-- /SEARCH -->
						</div>

						<div class="row gutters-sm">
							<div class="col-12 col-lg-3 col-xl-3 mb-5">
								<!-- CATEGORIES -->
								<nav class="card nav-deep nav-deep-light b-0 p-0-md p-0-xs shadow-xs rounded">
									<!-- mobile trigger : categories -->
									<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#nav_responsive" data-toggle-container-class=
									"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3"><span class="group-icon px-2 py-2 float-start"><i class="fi fi-bars-2"></i> <i class="fi fi-close"></i></span>
									<span class="h5 py-2 m-0 float-start">Categories</span></button> <!-- desktop only -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3" data-toggle="collapse" href="#nav_responsive" role="button" aria-expanded="false" aria-controls="nav_responsive">
										<a class="text-danger float-end w--20 d-inline" href="#"><span class="group-icon">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span></a> Categories
									</div>


									<ul class="nav flex-column collapse px-4" id="nav_responsive">
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><span class="badge badge-warning float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1">New in stock</span> <i class="fi fi-arrow-end m-0 fs--12"></i>
											<span class="px-2 d-inline-block">Carnation</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Gypsophilia</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Garden Roses</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Standard Roses</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><span class="badge badge-warning float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1">New in stock</span> <i class="fi fi-arrow-end m-0 fs--12"></i>
											<span class="px-2 d-inline-block">Carnation</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Gypsophilia</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Garden Roses</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Standard Roses</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><span class="badge badge-warning float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1">New in stock</span> <i class="fi fi-arrow-end m-0 fs--12"></i>
											<span class="px-2 d-inline-block">Carnation</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Gypsophilia</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Garden Roses</span></a>
										</li>
										<li class="nav-item">
											<a class="nav-link px-0" href="#!"><i class="fi fi-arrow-end m-0 fs--12"></i> <span class="px-2 d-inline-block">Standard Roses</span></a>
										</li>
									</ul>
								</nav><!-- /CATEGORIES-->
								<!-- mobile trigger : filters -->
								<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#sidebar_filters" data-toggle-body-class=
								"overflow-hidden" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen"><i class=
								"px-2 py-2 fs--15 float-start fi fi-eq-horizontal"></i> <span class="h5 py-2 m-0 float-start">Filters</span></button>
								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<a class="text-danger float-end w--20 d-inline" href="#"><i class="fi fi-close"></i></a> Reset Filters
									</div><!-- /Reset Filters -->
									<!-- Price -->
									<!-- <div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_price_list" href="#"><i class="fi fi-close"></i></a> Price
										</h3>
										<div id="filter_price_list">
											<label class="form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="10:100"> <i></i> $10 &ndash; $100</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="100:300"> <i></i> $100 &ndash; $300</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="300:500"> <i></i> $300 &ndash; $500</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="500:1000"> <i></i> $500 &ndash; $1000</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="1000:3000"> <i></i> $1000 &ndash; $3000</label> <small class=
											"text-muted d-block border-top mt-3 pt-3 mb-3">Custom price</small>
											<div class="row gutters-xs">
												<div class="col-5">
													<label class="d-block fs--13 mb-1">From</label> <input class="form-control form-control-sm" type="number" value="">
												</div>
												<div class="col-5">
													<label class="d-block fs--13 mb-1">To</label> <input class="form-control form-control-sm" type="number" value="">
												</div>
												<div class="col-2">
													<button class="btn btn-sm btn-block btn-light mt-4 px-2" type="submit"><i class="fi fi-arrow-end m-0"></i></button>
												</div>
											</div>
										</div>
									</div> --><!-- Color -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_color_list" href="#"><i class="fi fi-close"></i></a> Color
										</h3>
										<div id="filter_color_list">
											<label class="form-selector"><input name="color[]" type="checkbox"> <i style="background: #377dff"></i></label> <label class="form-selector"><input name="color[]" type="checkbox">
											<i style="background: #6c757d"></i></label> <label class="form-selector"><input name="color[]" type="checkbox"> <i style="background: #dc3545"></i></label> <label class=
											"form-selector"><input name="color[]" type="checkbox"> <i style="background: #fad776"></i></label> <label class="form-selector"><input name="color[]" type="checkbox"> <i style=
											"background: #e83e8c"></i></label> <label class="form-selector"><input name="color[]" type="checkbox"> <i style="background: #6610f2"></i></label> <label class="form-selector"><input name=
											"color[]" type="checkbox"> <i style="background: #4c2c92"></i></label> <label class="form-selector"><input name="color[]" type="checkbox"> <i style="background: #ffffff"></i></label>
											<label class="form-selector"><input name="color[]" type="checkbox"> <i style="background: #000000"></i></label> <label class="form-selector"><input name="color[]" type="checkbox">
											<i style="background: #ff0000"></i></label>
										</div>
									</div><!-- Size -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_size_list" href="#"><i class="fi fi-close"></i></a> Sizes
										</h3>
										<div id="filter_size_list">
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>40 CM</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>50 CM</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>60 CM</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>70 CM</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>80 CM</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>90 CM</span>
											</label>
										</div>
									</div>
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_size_list" href="#"><i class="fi fi-close"></i></a> Variety Features
										</h3>
										<div id="filter_size_list">
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>250gr</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>500gr</span>
											</label>
											<label class="form-selector">
												<input name="size[]" type="radio">
												<span>750gr</span>
											</label>
										</div>
									</div>
									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="Growers" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> Farm Name 1 <span class="text-muted fs--12 d-inline-block">(11)</span>
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="2"> <i></i> Farm Name 2 <span class="text-muted fs--12 d-inline-block">(45)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="3"> <i></i> Farm Name 3 <span class="text-muted fs--12 d-inline-block">(45)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="4"> <i></i> Farm Name 4 <span class="text-muted fs--12 d-inline-block">(13)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="5"> <i></i> Farm Name 5 <span class="text-muted fs--12 d-inline-block">(21)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="6"> <i></i> Farm Name 6 <span class="text-muted fs--12 d-inline-block">(21)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="7"> <i></i> Farm Name 7 <span class="text-muted fs--12 d-inline-block">(21)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="8"> <i></i> Farm Name 8 <span class="text-muted fs--12 d-inline-block">(21)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="9"> <i></i> Farm Name 9 <span class="text-muted fs--12 d-inline-block">(15)</span></label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary"><input name="brand[]" type="checkbox" value="10"> <i></i> Farm Name 10 <span class="text-muted fs--12 d-inline-block">(21)</span></label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-9 col-xl-9 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container">
										<form action="#" class="bs-validate" id="form_id" method="post" name="form_id" novalidate="">
											<!--

                                                IMPORTANT
                                                The "action" hidden input is updated by javascript according to button params/action:
                                                    data-js-form-advanced-hidden-action-id="#action"
                                                    data-js-form-advanced-hidden-action-value="delete"

                                                In your backend, should process data like this (PHP example):

                                                    if($_POST['action'] === 'delete') {

                                                        foreach($_POST['item_id'] as $item_id) {
                                                            // ... delete $item_id from database
                                                        }

                                                    }

                                            -->
											<input id="action" name="action" type="hidden" value=""><!-- value populated by js -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">
														<!-- SELECTED ITEMS -->
														<div class="clearfix">
															<!-- using .dropdown, autowidth not working -->
															<a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-light js-stoppropag" data-toggle="dropdown" href="#"><span class="group-icon"><i class=
															"fi fi-dots-vertical-full"></i> <i class="fi fi-close"></i></span> <span>Products per page</span></a>
															<div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-250">
																<div class="scrollable-vertical max-h-50vh">
																	<a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href=
																	"#"> 1 - 20</a> <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">20 - 30</a> <a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">30- 40</a>
																	<a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">40 - 50</a>
																	<div class="dropdown-divider"></div><a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">50 and up</a>
																</div>
															</div>
														</div><!-- /SELECTED ITEMS -->


													</div>
													<div class="col-12 col-md-6 mt-4">
														<!-- pagination -->
														<nav aria-label="pagination">
															<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
																<li class="page-item disabled btn-pill">
																	<a aria-disabled="true" class="page-link" href="#" tabindex="-1">Prev</a>
																</li>
																<li class="page-item active">
																	<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">2</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">3</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">Next</a>
																</li>
															</ul>
														</nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->
											<div class="table-responsive pd-15">
												<table class="table table-framed">
													<thead>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">GROWER</th>
															<th class="text-gray-500 font-weight-normal fs--14 min-w-300">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100">
																BOXES AVAILABLE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100 text-left">
																SELECT QUANTITY
															</th>
														</tr>
													</thead><!-- #item_list used by checkall: data-checkall-container="#item_list" -->
													<tbody id="item_list">
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
											<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->

														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup>Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
																<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->

														</tr>
														<!-- product -->


													</tbody>
													<tfoot>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">
																GROWER
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																BOXES AVAILABLE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--130 text-left">
																SELECT QUANTITY
															</th>
														</tr>
													</tfoot>
												</table>
											</div><!-- options and pagination -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">
														<!-- SELECTED ITEMS -->
														<div class="clearfix">
															<!-- using .dropdown, autowidth not working -->
															<a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-light js-stoppropag" data-toggle="dropdown" href="#"><span class="group-icon"><i class=
															"fi fi-dots-vertical-full"></i> <i class="fi fi-close"></i></span> <span>Products per page</span></a>
															<div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-250">
																<div class="scrollable-vertical max-h-50vh">
																	<a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href=
																	"#"> 1 - 20</a> <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">20 - 30</a> <a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">30- 40</a>
																	<a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">40 - 50</a>
																	<div class="dropdown-divider"></div><a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">50 and up</a>
																</div>
															</div>
														</div><!-- /SELECTED ITEMS -->


													</div>
													<div class="col-12 col-md-6 mt-4">
														<!-- pagination -->
														<nav aria-label="pagination">
															<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
																<li class="page-item disabled btn-pill">
																	<a aria-disabled="true" class="page-link" href="#" tabindex="-1">Prev</a>
																</li>
																<li class="page-item active">
																	<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">2</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">3</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">Next</a>
																</li>
															</ul>
														</nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->
										</form>
									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->
			</div><!-- FOOTER -->
<!-- Modal -->
<div class="modal fade" id="buy_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog" role="document">
				<div class="modal-content">

						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">Confirmation</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->
						<div class="modal-body">
<p>Order Sent</p>

						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<div class="d-flex">
									<a href="#" class="btn btn-primary">
		CONTINUE SHOPPING
		<i class="fi fi-arrow-end"></i>
</a>
<a href="#" class="btn btn-primary">
		GO TO ORDERS
		<i class="fi fi-arrow-end"></i>
</a>
</div>
						</div>

				</div>
		</div>
</div>
<!-- /Modal -->
<?php include('inc/footer.php'); ?>
