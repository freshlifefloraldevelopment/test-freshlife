<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

include('../config/config_gcp.php');

//require_once("config/config_gcp.php");

$htmlLoadData="";
if(isset($_POST["idGrower"]) && $_POST['idGrower']!=''){
$idGrower = $_POST['idGrower'];
$idColor = ','.$_POST['idColor'];

		$sql_colors = "select c.id as id, c.name as name ,count(*) as num_reg
from grower_product gp
INNER JOIN growers g ON gp.grower_id = g.id
INNER JOIN product p ON gp.product_id = p.id
INNER JOIN subcategory s ON p.subcategoryid = s.id
INNER JOIN colors c on p.color_id = c.id
where gp.grower_id = '$idGrower'
group by c.id,c.name
order by c.name";

       $rs_colors = mysqli_query($con,$sql_colors);

           while ($row_colors = mysqli_fetch_array($rs_colors))
           {
              $color_name = $row_colors['name'];
             if($color_name=="Bicolor"){
               $background = "background: linear-gradient(to right, red,orange,yellow,green,blue,indigo,violet);";
             }
             if($color_name=="Black"){
               $background = "background-color: #000000";
             }
             if($color_name=="Blue"){
               $background = "background-color: #0000CC";
             }
             if($color_name=="Burgundy"){
               $background = "background-color: #45001C";
             }
             if($color_name=="Cream"){
               $background = "background-color: #FAF2D1";
             }
             if($color_name=="Earth Tones"){
               $background = "background-color: #D19C4C";
             }
             if($color_name=="Green"){
               $background = "background-color: #009900";
             }
             if($color_name=="Hot Pink"){
               $background = "background-color: #f514d7";
             }
             if($color_name=="Lavender"){
               $background = "background-color: #39e3dd";
             }
             if($color_name=="Light Pink"){
               $background = "background-color: #FADADD";
             }
             if($color_name=="Orange"){
               $background = "background-color: #FF9900";
             }
             if($color_name=="Peach"){
               $background = "background-color: #ffcba4";
             }
             if($color_name=="Pink"){
               $background = "background-color: #ffc0cb";
             }
             if($color_name=="Purple"){
               $background = "background-color: #990099";
             }
             if($color_name=="Red"){
               $background = "background-color: #FF0000";
             }
             if($color_name=="White"){
               $background = "background-color: #FFFFFF";
             }
             if($color_name=="Yellow"){
               $background = "background-color: #FFFF00";
             }
						 if($color_name=="Light Blue"){
               $background = "background-color: #d5e7f7";
             }


						 $colorcselect = '';
						 if (strpos($idColor, ','.$row_colors["id"].',') !== false)
						 {
							  $colorcselect = 'checked';
						 }

           $htmlLoadData .='<label class="form-selector"><input name="color[]" href="javascript:void(0);" onclick="get_color_data('.$row_colors["id"].')" type="checkbox" '.$colorcselect.' > <i style="'.$background.'"></i></label>';
					 $row_colors["id"]='';
					 }

 echo $htmlLoadData;
}
?>
