<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

    include('../config/config_gcp.php');

$idSizes = str_replace(',',' ',$_POST['idSize']);

$htmlLoadData="";

		$sql_sizes = "select gp.id   , gp.idsc , gp.subcategory , gp.size , gp.stem_bunch  , gp.description ,
                      gp.type , gp.feature , gp.factor , gp.id_ficha ,gp.price_adm ,
                      s.name as namesize, s.id as size_id, f.name as namefeature , b.name as namestems
                 from grower_parameter gp
                inner JOIN sizes s ON gp.size = s.id
                inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                 left JOIN features f  ON gp.feature = f.id
                where gp.idsc     = '2'
                  and gp.id_ficha = '1'
                order by s.name";

       $rs_sizes = mysqli_query($con,$sql_sizes);

           while ($row_sizes= mysqli_fetch_array($rs_sizes))
           {

						 $Sizesselect = '';
						 if (strpos($idSizes, $row_sizes['size_id']) !== false)
						 {
						 	 $Sizesselect = 'checked';
						 }

           $htmlLoadData .='<label class="form-selector"><input onclick="get_size_data('.$row_sizes['size_id'].')" name="size['.$row_sizes['size_id'].']" type="checkbox" '.$Sizesselect.'><span>'.$row_sizes['namesize']." cm".'</span></label>';
           }

 echo $htmlLoadData;

?>
