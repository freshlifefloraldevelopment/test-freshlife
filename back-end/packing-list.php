<?php
if(!$growerID_Prod){
	header("Location: ../en/variety-page.php");
	exit();
}
include('inc/header.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							PACKING LIST
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
							Arrival date: <a href="#" class="datepicker link-muted"
														data-layout-rounded="false"
														data-title="Smarty Datepicker"
														data-show-weeks="true"
														data-today-highlight="true"
														data-today-btn="true"
														data-autoclose="true"
														data-date-start="today"
														data-format="MM/DD/YYYY"
														data-quick-locale='{
															"days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
															"daysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
															"daysMin": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
															"months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
															"monthsShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
															"today": "Today",
															"clear": "Clear",
															"titleFormat": "MM yyyy"
														}'
														data-id="1"
														data-ajax-url="php/demo.ajax_request.php"
														data-ajax-params="['action','date_change']['section','customer_invoice']"
														data-ajax-method="POST"

														data-toast-success="Sucessfully Updated!"
														data-toast-position="top-center">
													08/08/2020
												</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>
						<div class="card fs--18 pt-2 pb-3 bg-white shadow-md rounded mb-3 d-flex b-0 p--20">
	<form 	action="open-request.php"
							method="GET"
							data-autosuggest="on"
							data-mode="json"
							data-json-max-results='10'
							data-json-related-title='Search Varieties'
							data-json-related-item-icon='fi fi-star-empty'
							data-json-suggest-title='Suggestions for you'
							data-json-suggest-noresult='No results for'
							data-json-suggest-item-icon='fi fi-search'
							data-json-suggest-min-score='5'
							data-json-highlight-term='true'
							data-contentType='application/json; charset=utf-8'
							data-dataType='json'

							data-container="#sow-search-container"
							data-input-min-length="2"
							data-input-delay="100"
							data-related-keywords=""
							data-related-url="_ajax/open_request_releated_data.json"
							data-suggest-url="_ajax/open_request_data_suggestions.json"
							data-related-action="related_get"
							data-suggest-action="suggest_get"
							class="js-ajax-search sow-search sow-search-mobile-float d-flex-1-1-auto mx-4">
						<div class="sow-search-input w-100 d-flex align-items-center">

							<div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

								<input placeholder="Search Varieties..." name="s" type="text" class="form-control-sow-search form-control form-control-pill b-0 bg-gray-100" value="" autocomplete="off">

								<span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit" class="btn btn-primary btn-noshadow m-0 px-2 py-1 b-0 bg-transparent text-muted">
										<i class="fi fi-search fs--20"></i>
									</button>

									<!-- close : mobile only (d-inline-block d-lg-none) -->
									<a href="javascript:;" class="btn-sow-search-toggler btn btn-light btn-noshadow m-0 px-2 py-1 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

							</div>

						</div>

						<!-- search suggestion container -->
						<div class="sow-search-container rounded-xl w-100 p-0 hide shadow-md" id="sow-search-container">
							<div class="sow-search-container-wrapper rounded-xl">

								<!-- main search container -->
								<div class="sow-search-loader p--15 text-center hide">
									<i class="fi fi-circle-spin fi-spin text-muted fs--30"></i>
								</div>

								<!--
									AJAX CONTENT CONTAINER
									SHOULD ALWAYS BE AS IT IS : NO COMMENTS OR EVEN SPACES!
								--><div class="sow-search-content rounded w-100 scrollable-vertical"></div>

							</div>
						</div>
						<!-- /search suggestion container -->

						<!--

							overlay combinations:
								overlay-dark opacity-* [1-9]
								overlay-light opacity-* [1-9]

						-->
						<div class="sow-search-backdrop overlay-dark opacity-3 hide"></div>

					</form>
					<!-- /SEARCH -->
						</div>

						<div class="row gutters-sm">
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container">
										<form action="#" class="bs-validate" id="form_id" method="post" name="form_id" novalidate="">
											<!--

                                                IMPORTANT
                                                The "action" hidden input is updated by javascript according to button params/action:
                                                    data-js-form-advanced-hidden-action-id="#action"
                                                    data-js-form-advanced-hidden-action-value="delete"

                                                In your backend, should process data like this (PHP example):

                                                    if($_POST['action'] === 'delete') {

                                                        foreach($_POST['item_id'] as $item_id) {
                                                            // ... delete $item_id from database
                                                        }

                                                    }

                                            -->
											<input id="action" name="action" type="hidden" value=""><!-- value populated by js -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">
														<!-- SELECTED ITEMS -->
														<div class="clearfix">
															<!-- using .dropdown, autowidth not working -->
															<a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-success js-stoppropag" data-toggle="dropdown" href="#"> <span>Apply changes</span></a>

														</div><!-- /SELECTED ITEMS -->


													</div>
													<div class="col-12 col-md-6 mt-4">
														<!-- pagination -->
														<nav aria-label="pagination">
															<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
																<li class="page-item disabled btn-pill">
																	<a aria-disabled="true" class="page-link" href="#" tabindex="-1">Prev</a>
																</li>
																<li class="page-item active">
																	<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">2</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">3</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">Next</a>
																</li>
															</ul>
														</nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->
											<div class="table-responsive pd-15">
												<table class="table table-framed">
													<thead>
														<tr>
															<th class="text-gray-500 w--50">
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input class="checkall" data-checkall-container="#item_list" type="checkbox" name="checkbox">
																	<i></i>
																</label>
															</th>
															<th>&nbsp;</th>
															<th class="text-gray-500 font-weight-normal fs--14 min-w-300">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																QUANTITY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--130 text-left">
																CUSTOMER
															</th>
														</tr>
													</thead><!-- #item_list used by checkall: data-checkall-container="#item_list" -->
													<tbody id="item_list">
														<!-- product -->
														<tr>
															<th>
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input type="checkbox" name="item_id[]" value="1">
																	<i></i>
																</label>
															</th>
															<td>
																<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Order number: 916, Shipment: 123-12345675, Invoice: 1004</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Grower: Florana Farms S.A.</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box number: 2</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>
															</td><!-- status -->
															<td class="text-left">
																<div class="dropdown d-inline-block">
											<a href="#" id="ddHClean_demo1" class="text-success btn btn-sm p-0" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
												<span>Happy Flowers</span>
											</a>

											<div aria-labelledby="ddHClean_demo1" class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-200">

												<div class="scrollable-vertical max-h-50vh">

													<a class="dropdown-item text-truncate active" href="#">
														Happy Flowers
													</a>
													<a class="dropdown-item text-truncate disabled" href="#">
														Name2
													</a>

													<a class="dropdown-item" href="#">
														Name3
													</a>
													<a class="dropdown-item" href="#">
														Name4
													</a>
													<a class="dropdown-item max-w-250 text-truncate" href="#">
														Name5
													</a>
												</div>

											</div>
										</div>
															</td><!-- options -->

														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<th>
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input type="checkbox" name="item_id[]" value="1">
																	<i></i>
																</label>
															</th>
															<td>
																<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Order number: 916, Shipment: 123-12345675, Invoice: 1004</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Grower: Florana Farms S.A.</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box number: 2</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>
															</td><!-- status -->
															<td class="text-left">
																<a class="text-success btn btn-sm p-0" href="#">Available</a>
															</td><!-- options -->

														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<th>
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input type="checkbox" name="item_id[]" value="1">
																	<i></i>
																</label>
															</th>
															<td>
																<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Order number: 916, Shipment: 123-12345675, Invoice: 1004</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Grower: Florana Farms S.A.</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box number: 2</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>
															</td><!-- status -->
															<td class="text-left">
																<a class="text-success btn btn-sm p-0" href="#">150</a>
															</td><!-- options -->

														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<th>
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input type="checkbox" name="item_id[]" value="1">
																	<i></i>
																</label>
															</th>
															<td>
																<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Order number: 916, Shipment: 123-12345675, Invoice: 1004</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Grower: Florana Farms S.A.</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box number: 2</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>
															</td><!-- status -->
															<td class="text-left">
																<a class="text-success btn btn-sm p-0" href="#">150</a>
															</td><!-- options -->

														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<th>
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input type="checkbox" name="item_id[]" value="1">
																	<i></i>
																</label>
															</th>
															<td>
																<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Order number: 916, Shipment: 123-12345675, Invoice: 1004</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Grower: Florana Farms S.A.</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box number: 2</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>
															</td><!-- status -->
															<td class="text-left">
																<a class="text-success btn btn-sm p-0" href="#">150</a>
															</td><!-- options -->

														</tr>
														<!-- product -->


													</tbody>
													<tfoot>
														<tr>
															<th class="text-gray-500 w--50">
																<label class="form-checkbox form-checkbox-primary float-start">
																	<input class="checkall" data-checkall-container="#item_list" type="checkbox" name="checkbox">
																	<i></i>
																</label>
															</th>
															<th>&nbsp;</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																QUANTITY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--130 text-left">
																CUSTOMER
															</th>
														</tr>
													</tfoot>
												</table>
											</div><!-- options and pagination -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">
														<!-- SELECTED ITEMS -->
														<div class="clearfix">
															<!-- using .dropdown, autowidth not working -->
															<a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-light js-stoppropag" data-toggle="dropdown" href="#"><span class="group-icon"><i class=
															"fi fi-dots-vertical-full"></i> <i class="fi fi-close"></i></span> <span>Products per page</span></a>
															<div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-250">
																<div class="scrollable-vertical max-h-50vh">
																	<a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href=
																	"#"> 1 - 20</a> <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">20 - 30</a> <a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">30- 40</a>
																	<a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">40 - 50</a>
																	<div class="dropdown-divider"></div><a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="#">50 and up</a>
																</div>
															</div>
														</div><!-- /SELECTED ITEMS -->


													</div>
													<div class="col-12 col-md-6 mt-4">
														<!-- pagination -->
														<nav aria-label="pagination">
															<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
																<li class="page-item disabled btn-pill">
																	<a aria-disabled="true" class="page-link" href="#" tabindex="-1">Prev</a>
																</li>
																<li class="page-item active">
																	<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">2</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">3</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">Next</a>
																</li>
															</ul>
														</nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->
										</form>
									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->
			</div><!-- FOOTER -->
<?php include('inc/footer.php'); ?>
