<?php
if(!$growerID_Prod){
	header("Location: ../en/variety-page.php");
	exit();
}
include('inc/header.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->
				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Orders
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
							Arrival date: <a href="#" class="datepicker link-muted"
														data-layout-rounded="false"
														data-title="Smarty Datepicker"
														data-show-weeks="true"
														data-today-highlight="true"
														data-today-btn="true"
														data-autoclose="true"
														data-date-start="today"
														data-format="MM/DD/YYYY"
														data-quick-locale='{
															"days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
															"daysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
															"daysMin": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
															"months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
															"monthsShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
															"today": "Today",
															"clear": "Clear",
															"titleFormat": "MM yyyy"
														}'
														data-id="1"
														data-ajax-url="php/demo.ajax_request.php"
														data-ajax-params="['action','date_change']['section','customer_invoice']"
														data-ajax-method="POST"

														data-toast-success="Sucessfully Updated!"
														data-toast-position="top-center">
													All
												</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm">
							<div class="col-12 col-lg-12 col-xl-12 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container">
										<form action="#" class="bs-validate" id="form_id" method="post" name="form_id" novalidate="">
											<!--

                                                IMPORTANT
                                                The "action" hidden input is updated by javascript according to button params/action:
                                                    data-js-form-advanced-hidden-action-id="#action"
                                                    data-js-form-advanced-hidden-action-value="delete"

                                                In your backend, should process data like this (PHP example):

                                                    if($_POST['action'] === 'delete') {

                                                        foreach($_POST['item_id'] as $item_id) {
                                                            // ... delete $item_id from database
                                                        }

                                                    }

                                            -->
											<input id="action" name="action" type="hidden" value=""><!-- value populated by js -->
											<div class="table-responsive pd-15">
												<table class="table table-framed">
													<thead>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">GROWER</th>
															<th class="text-gray-500 font-weight-normal fs--14 min-w-300">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																QUANTITY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--130 text-left">
																STATUS
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--60 text-align-end">
																&nbsp;
															</th>
														</tr>
													</thead><!-- #item_list used by checkall: data-checkall-container="#item_list" -->
													<tbody id="item_list">
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left">
																	87%
															</td><!-- options -->
															<td class="text-align-end">

																<button class="btn btn-sm btn-block btn-success text-white b-0 w--200" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--14">CONFIRMED</span>
											</span>
										</button>

															</td>
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#" class="badge badge-success">Open Request</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left">
																	87%
															</td><!-- options -->
															<td class="text-align-end">

																<button class="btn btn-sm btn-block btn-warning text-white b-0 w--200" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--14">IN PROCESS</span>
											</span>
										</button>

															</td>
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left">
																	87%
															</td><!-- options -->
															<td class="text-align-end">

																<div class="btn-group">

													<button type="button" class="btn btn-sm btn-block btn-purple text-white b-0 w--200" data-toggle="modal" data-target="#button_modal"><i class="fas fa-random m-0-md"></i> Approve Change</button>
												</div>

															</td>
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left">
																	87%
															</td><!-- options -->
															<td class="text-align-end">

																<button class="btn btn-sm btn-block btn-danger text-white b-0 w--200" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--14">NOT AVAILABLE</span>
											</span>
										</button>

															</td>
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
																<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <span class="d-block text-success fs--15">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></span> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left">
																	87%
															</td><!-- options -->
															<td class="text-align-end">

																<button class="btn btn-sm btn-block btn-danger text-white b-0 w--200" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--14">NOT AVAILABLE</span>
											</span>
										</button>

															</td>
														</tr>
														<!-- product -->


													</tbody>
													<tfoot>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">
																GROWER
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																QUANTITY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--130 text-left">
																STATUS
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100 text-align-end">
																&nbsp;
															</th>
														</tr>
													</tfoot>
												</table>
											</div><!-- options and pagination -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">
														<!-- SELECTED ITEMS -->
														<div class="clearfix">
															<!-- using .dropdown, autowidth not working -->
															<a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-light js-stoppropag" data-toggle="dropdown" href="#"><span class="group-icon"><i class=
															"fi fi-dots-vertical-full"></i> <i class="fi fi-close"></i></span> <span>Selected Products</span></a>
															<div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-250">
																<div class="scrollable-vertical max-h-50vh">
																	<a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" data-js-form-advanced-bulk-container-items="#item_list" data-js-form-advanced-bulk-hidden-action-id=
																	"#action" data-js-form-advanced-bulk-hidden-action-value="active" data-js-form-advanced-bulk-required-custom-modal="" data-js-form-advanced-bulk-required-custom-modal-content-ajax=""
																	data-js-form-advanced-bulk-required-modal-btn-text-no="Cancel" data-js-form-advanced-bulk-required-modal-btn-text-yes="Confirm" data-js-form-advanced-bulk-required-modal-size=
																	"modal-md" data-js-form-advanced-bulk-required-modal-txt-body-info="" data-js-form-advanced-bulk-required-modal-txt-body-txt="Set active {{no_selected}} selected products?"
																	data-js-form-advanced-bulk-required-modal-txt-subtitle="Selected Products: {{no_selected}}" data-js-form-advanced-bulk-required-modal-txt-title="Please Confirm"
																	data-js-form-advanced-bulk-required-modal-type="success" data-js-form-advanced-bulk-required-selected="true" data-js-form-advanced-bulk-required-txt-error="No Products Selected!"
																	data-js-form-advanced-bulk-required-txt-position="top-center" data-js-form-advanced-bulk-submit-without-confirmation="false" data-js-form-advanced-form-id="#form_id" href=
																	"#"><i class="fi fi-check"></i> Set : Active</a> <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" data-js-form-advanced-bulk-container-items=
																	"#item_list" data-js-form-advanced-bulk-hidden-action-id="#action" data-js-form-advanced-bulk-hidden-action-value="inactive" data-js-form-advanced-bulk-required-custom-modal=""
																	data-js-form-advanced-bulk-required-custom-modal-content-ajax="" data-js-form-advanced-bulk-required-modal-btn-text-no="Cancel"
																	data-js-form-advanced-bulk-required-modal-btn-text-yes="Confirm" data-js-form-advanced-bulk-required-modal-size="modal-md" data-js-form-advanced-bulk-required-modal-txt-body-info=""
																	data-js-form-advanced-bulk-required-modal-txt-body-txt="Set inactive {{no_selected}} selected products?" data-js-form-advanced-bulk-required-modal-txt-subtitle=
																	"Selected Products: {{no_selected}}" data-js-form-advanced-bulk-required-modal-txt-title="Please Confirm" data-js-form-advanced-bulk-required-modal-type="warning"
																	data-js-form-advanced-bulk-required-selected="true" data-js-form-advanced-bulk-required-txt-error="No Products Selected!" data-js-form-advanced-bulk-required-txt-position=
																	"top-center" data-js-form-advanced-bulk-submit-without-confirmation="false" data-js-form-advanced-form-id="#form_id" href="#"><i class="fi fi-close"></i> Set : Inactive</a> <a class=
																	"dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" data-js-form-advanced-bulk-container-items="#item_list" data-js-form-advanced-bulk-hidden-action-id="#action"
																	data-js-form-advanced-bulk-hidden-action-value="delete" data-js-form-advanced-bulk-required-custom-modal="" data-js-form-advanced-bulk-required-custom-modal-content-ajax=""
																	data-js-form-advanced-bulk-required-modal-btn-text-no="Cancel" data-js-form-advanced-bulk-required-modal-btn-text-yes="Yes, Delete" data-js-form-advanced-bulk-required-modal-size=
																	"modal-md" data-js-form-advanced-bulk-required-modal-txt-body-info="Please note: this is a permanent action!" data-js-form-advanced-bulk-required-modal-txt-body-txt=
																	"Are you sure? Delete {{no_selected}} selected products?" data-js-form-advanced-bulk-required-modal-txt-subtitle="Selected Products: {{no_selected}}"
																	data-js-form-advanced-bulk-required-modal-txt-title="Please Confirm" data-js-form-advanced-bulk-required-modal-type="danger" data-js-form-advanced-bulk-required-selected="true"
																	data-js-form-advanced-bulk-required-txt-error="No Products Selected!" data-js-form-advanced-bulk-required-txt-position="top-center"
																	data-js-form-advanced-bulk-submit-without-confirmation="false" data-js-form-advanced-form-id="#form_id" href="#"><i class="fi fi-thrash text-danger"></i> Set : Delete</a>
																	<div class="dropdown-divider"></div><a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" data-js-form-advanced-bulk-container-items="#item_list"
																	data-js-form-advanced-bulk-hidden-action-id="#action" data-js-form-advanced-bulk-hidden-action-value="myactionhere3" data-js-form-advanced-bulk-required-selected="true"
																	data-js-form-advanced-bulk-required-txt-error="No Products Selected!" data-js-form-advanced-bulk-required-txt-position="top-center"
																	data-js-form-advanced-bulk-submit-without-confirmation="true" data-js-form-advanced-form-id="#form_id" href="#"><i class="fi fi-mollecules text-danger"></i> Submit : No Confirm.</a>
																	<div class="dropdown-divider"></div><a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" data-js-form-advanced-bulk-container-items="#item_list"
																	data-js-form-advanced-bulk-hidden-action-id="#action" data-js-form-advanced-bulk-hidden-action-value="myactionhere1" data-js-form-advanced-bulk-required-custom-modal=
																	"#my_custom_modal" data-js-form-advanced-bulk-required-custom-modal-content-ajax="" data-js-form-advanced-bulk-required-selected="true" data-js-form-advanced-bulk-required-txt-error=
																	"No Products Selected!" data-js-form-advanced-bulk-submit-without-confirmation="false" data-js-form-advanced-form-id="#form_id" href="#"><i class="fi fi-heart-slim text-success"></i>
																	Inline Custom Modal</a>
																</div>
															</div>
														</div><!-- /SELECTED ITEMS -->
														<!-- Inline custom modal (should stay inside <form> to be able to post data) -->
														<div aria-hidden="true" aria-labelledby="modal-title-confirm" class="modal fade show" id="my_custom_modal" role="dialog" tabindex="-1">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<!--

                                                                        Header color - optional
                                                                            .bg-[primary|danger|warning|success|info|pink|indigo]-soft
                                                                    -->
																	<div class="modal-header b-0 bg-primary-soft">
																		<h5 class="modal-title font-weight-light fs--18" id="modal-title-confirm">
																			Inline custom modal
																		</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																	</div><!-- /header -->
																	<!-- body -->
																	<div class="modal-body pt--30 pb--30">
																		Selected items: <span class="js-form-advanced-selected-items">0</span>
																		<div class="fs--18">
																			Customize as you like!<br>
																			<br>
																			<!-- FILE UPLOADER -->
																			<div class="clearfix">
																				<!--

                                                                                    2. AJAX UPLOAD : DYNAMIC PROGRESS UNDER BUTTON
                                                                                    No any extra html code needed for the progress bar.

                                                                                -->
																				<label class="btn btn-warning btn-sm cursor-pointer position-relative"><!--
                                                                                        We use .absolute-full class instead of .viewport-out
                                                                                        Just to make sure the element is working crossbrowser!

                                                                                        .show-hover-container   = show delete button only on hover (always visible on mobile)

                                                                                     -->
																				 <input class="custom-file-input absolute-full js-advancified" data-file-ajax-callback-function="" data-file-ajax-delete-enable="true" data-file-ajax-delete-params=
																				"['action','delete_file']" data-file-ajax-delete-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php" data-file-ajax-progressbar-custom=""
																				data-file-ajax-progressbar-disable="false" data-file-ajax-reorder-enable="true" data-file-ajax-reorder-params="['action','reorder']" data-file-ajax-reorder-toast-position=
																				"bottom-center" data-file-ajax-reorder-toast-success="Order Saved!" data-file-ajax-reorder-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php"
																				data-file-ajax-toast-error-txt="One or more files not uploaded!" data-file-ajax-toast-success-txt="Successfully Uploaded!" data-file-ajax-upload-enable="true"
																				data-file-ajax-upload-params="['action','upload']['param2','value2']" data-file-ajax-upload-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php" data-file-btn-clear=
																				"a.js-form-advanced-btn-multiple-ajax-remove" data-file-exist-err-msg="File already exists:" data-file-ext="jpg,png,gif" data-file-ext-err-msg="Allowed:"
																				data-file-max-size-kb-per-file="3000" data-file-max-size-kb-total="5000" data-file-max-total-files="3" data-file-preview-class=
																				"show-hover-container shadow-md m-2 rounded float-start" data-file-preview-container=".js-form-advanced-container-table-form-test" data-file-preview-img-cover="false"
																				data-file-preview-img-height="120" data-file-preview-show-info="true" data-file-size-err-item-msg="File too large!" data-file-size-err-max-msg="Maximum allowed files:"
																				data-file-size-err-total-msg="Total allowed size exceeded!" data-file-toast-position="bottom-center" data-js-advanced-identifier="3557" multiple name=
																				"ajax_files_progress_dynamic[]" type="file"> <span class="group-icon"><i class="fi fi-arrow-upload"></i> <i class="fi fi-circle-spin fi-spin"></i></span> <span>Ajax
																				Uploader</span></label>
																				<div class="js-form-advanced-container-table-form-test position-relative mt-3 clearfix hide-empty js-sortablified" data-ajax-update-identifier="ajax_files_progress_dynamic"
																				data-ajax-update-params="['action','reorder']" data-ajax-update-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php" data-update-toast-position="bottom-center"
																				data-update-toast-success="Order Saved!" id="strand_e4w"></div><small class="d-block text-gray-400">Upload few images and then... reorder them :)</small>
																			</div><!-- /FILE UPLOADER -->
																			<br>
																			<small>Yes, ajax content for modals also supported!</small><br>
																			<small>Check <a class="js-ajax link-muted" href="plugins-sow-form-advanced.html">SOW : Form Advanced</a> for more &amp; documentation!</small>
																		</div>
																	</div><!-- /body -->
																	<!-- footer ; buttons -->
																	<div class="modal-footer">
																		<!-- submit button - actually submitting the form -->
																		<button class="btn pt--10 pb--10 fs--16 btn-primary" type="submit"><i class="fi fi-check"></i> Oh, Great!</button> <!-- cancel|close button -->
																		 <a class="btn pt--10 pb--10 fs--16 btn-light" data-dismiss="modal" href="#"><i class="fi fi-close"></i> Close</a>
																	</div><!-- /footer ; buttons -->
																</div>
															</div>
														</div><!-- /Inline custom modal -->
													</div>
													<div class="col-12 col-md-6 mt-4">
														<!-- pagination -->
														<nav aria-label="pagination">
															<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
																<li class="page-item disabled btn-pill">
																	<a aria-disabled="true" class="page-link" href="#" tabindex="-1">Prev</a>
																</li>
																<li class="page-item active">
																	<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">2</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">3</a>
																</li>
																<li class="page-item">
																	<a class="page-link" href="#">Next</a>
																</li>
															</ul>
														</nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->
										</form>
									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->
			</div><!-- FOOTER -->
<!-- Button Modal -->
											<div class="modal fade" id="button_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLg" aria-hidden="true">
												<div class="modal-dialog modal-xl" role="document">
													<div class="modal-content">

														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabelLg">Modal title</h5>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span class="fi fi-close fs--18" aria-hidden="true"></span>
															</button>
														</div>

														<div class="modal-body">
																									<table class="table table-framed">
													<thead>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">GROWER</th>
															<th class="text-gray-500 font-weight-normal fs--14 min-w-300">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100">
																BOXES AVAILABLE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100 text-left">
																SELECT QUANTITY
															</th>
														</tr>
													</thead><!-- #item_list used by checkall: data-checkall-container="#item_list" -->
													<tbody id="item_list">
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
											<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->

														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup>Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
																<td>
																<a data-target="#farm_modal_1" data-toggle="modal" href="#">4324</a>
<!--Farm Logo Modal-->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="farm_modal_1" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Header -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabelMd">
					4324
				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
			</div><!-- Content -->
			<div class="modal-body">
				<img alt="..." src="https://app.freshlifefloral.com/user/logo/020817114234-AGRINAG.png" width="100%">
			</div>
		</div>
	</div>
</div>
<!--Farm Logo Modal-->
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Grower Rating: 95%, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span>
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..."  src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html"
											data-ajax-modal-size="modal-md"
											data-ajax-modal-centered="true"
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Bunch/Stems: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-muted text-left">
																10
															</td><!-- status -->
														<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>

									</div>

									<div class="pl-2 pr-2 w--120">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;">
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->

														</tr>
														<!-- product -->


													</tbody>
													<tfoot>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">
																GROWER
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																BOXES AVAILABLE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--130 text-left">
																SELECT QUANTITY
															</th>
														</tr>
													</tfoot>
												</table>
														</div>

														<div class="modal-footer">
															<button type="button" class="btn btn-primary">
																<i class="fi fi-check"></i>
																Save changes
															</button>
															<button type="button" class="btn btn-secondary" data-dismiss="modal">
																<i class="fi fi-close"></i>
																Close
															</button>
														</div>

													</div>
												</div>
											</div>
<?php include('inc/footer.php'); ?>
