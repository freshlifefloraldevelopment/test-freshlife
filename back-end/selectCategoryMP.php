<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

include('../config/config_gcp.php');

//include('config/config_gcp.php');
$idCat = $_POST['idCateg'];
$htmlLoadData="";

	       $sql_categories = "select p.subcategoryid , c.name , c.id as id
                                    from grower_product gp
                                    inner join product  p on gp.product_id = p.id
                                    inner join subcategory c on p.subcategoryid = c.id
                                    inner join growers  g on gp.grower_id  = g.id
                                    inner join growcard_prod_bunch_sizes gps on (gp.grower_id = gps.grower_id and gp.product_id = gps.product_id)
                                    where g.active='active'
                                     and g.market_place = '1'                                   
                                     and p.status = 0
                                    group by p.subcategoryid
                                    order by c.name";

       $rs_categories = mysqli_query($con,$sql_categories);

           while ($row_categories = mysqli_fetch_array($rs_categories))   {
						 $Subcat = '';
                    if($idCat==$row_categories['id']){
                            $Subcat = '<span class="badge badge-success float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1">Selected</span>';
                    }
                    $htmlLoadData .='<li class="nav-item"><a class="nav-link px-0" href="javascript:onclick=funSearchPageCat('.$row_categories['id'].');">'.$Subcat.'<i class="fi fi-arrow-end m-0 fs--12"></i><span class="px-2 d-inline-block">'.$row_categories['name'].'</span></a></li>';
           }

 echo $htmlLoadData;
?>
