<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

include('../config/config_gcp.php');
$idCat = str_replace(',',' ',$_POST['idFeature']);

$htmlLoadData="";

		$sql_growers = "select id,growers_name
											from growers
										 where active='active'
										 and market_place = '1'
										 order by growers_name";

       $rs_growers = mysqli_query($con,$sql_growers);

           while ($row_growers = mysqli_fetch_array($rs_growers))
           {
						 $Subcat = '';
						 if (strpos($idCat, $row_growers['id']) !== false)
						 {
							 $Subcat = 'checked';
						 }

						  $GR = substr($row_growers['growers_name'],0, 3);
           $htmlLoadData .='<div class="iqs-item"><label class="form-checkbox form-checkbox-primary"><input onclick="get_grower_data('.$row_growers['id'].')" name="brand[]" type="checkbox" '.$Subcat.' value="1"> <i></i>'.$row_growers['growers_name'].' <span class="text-muted fs--12 d-inline-block"> ('.$GR.') </span></label></div>';
           }

 echo $htmlLoadData;
?>
