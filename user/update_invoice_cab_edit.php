<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        
  $fact_number = $_GET['id_fact'];
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
   // echo "(1) ".$_POST["total_boxes"]."(2) ".$_POST["gross_weight"]."(3) ".$_POST["volume_weight"]."(4) ".$_POST["handling_lax"]."(5) ".$_POST["brokerage_lax"]."(6) ".$_POST["per_kg"]."(7) ".$_POST["air_waybill"]."(8) ".$_POST["charges_due_agent"]."(9) ".$_POST["order_number"]."(10) ".$_POST["tot_shiping_imp_cost"]."(11) ".$_POST["cad"]."(12) ".$_POST["duties"]."(13) ".$_POST["porce_duties"]."(14) ".$_POST["porce_handling"];
    
    $ins = "update invoice_orders 
               set total_boxes          = '" . $_POST["total_boxes"]   . "', 
                   gross_weight         = '" . $_POST["gross_weight"]  . "',
                   volume_weight        = '" . $_POST["volume_weight"] . "',
                   per_kg               = '" . $_POST["per_kg"] . "'       ,
                   air_waybill          = '" . $_POST["air_waybill"]  . "' ,
                   charges_due_agent    = '" . $_POST["charges_due_agent"] . "',
                   order_number         = '" . $_POST["order_number"]      . "',
                   bill_state           = '" . $_POST["bill_state"]      . "'  ,
                   tot_shiping_imp_cost = '" . $_POST["tot_shiping_imp_cost"] . "',
                   cad                  = '" . $_POST["cad"]            . "'    ,
                   duties               = '" . $_POST["duties"]         . "'    ,
                   porce_duties         = '" . $_POST["porce_duties"]   . "'    ,
                   porce_handling       = '" . $_POST["porce_handling"] . "'    ,                            
                   box_packing          = '" . $_POST["box_packing"]   . "'     ,
                   price_Flf            = '" . $_POST["price_Flf"]   . "'       ,                       
                   price_client         = '" . $_POST["price_client"]   . "'    ,                                              
                   quality              = '" . $_POST["quality"]   . "'    ,                                                                     
                   truck                = '" . $_POST["truck"] . "'    
             where id_fact       = '" . $fact_number . "'   ";         
    
    if(mysqli_query($con, $ins))
        header("location:invoice_mgmt.php");
    
}

     $sel_invoice_cab = "select id_fact          , buyer_id        , order_number    , order_date   , shipping_method, 
                                del_date         , date_range      , is_pending      , order_serial , seen           , 
                                delivery_dates   , lfd_grower      , quick_desc      , bill_number  , gross_weight   , 
                                volume_weight    , freight_value   , per_kg          , guide_number , total_boxes    , 
                                sub_total_amount , tax_rate        , shipping_charge , handling     , air_waybill    ,
                                charges_due_agent, credit_card_fees, grand_total     , bill_state   , 
                                date_added       , user_added      , handling_lax    , brokerage_lax, tot_shiping_imp_cost,
                                cad              , duties          ,
                                total_cost       , price_cad       , handling_pro    , farm_price_tot ,
                                porce_duties     , porce_handling  , box_packing     , truck          , price_Flf    ,
                                price_client     , quality
                           from invoice_orders 
                          where id_fact  = '" . $fact_number . "'    ";

    $rs_invoice_cab = mysqli_query($con, $sel_invoice_cab);
    $row = mysqli_fetch_array($rs_invoice_cab);   
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()       {
                
                if (trim(document.frmcat.order_number.value) == "")  {
                    document.getElementById("lblorder_number").innerHTML = "Please enter Order Number ";
                    return false;
                }else {
                    document.getElementById("lblorder_number").innerHTML = "";
                    return true;
                }
                
                if (trim(document.frmcat.total_boxes.value) == "")  {
                    document.getElementById("lblfrom").innerHTML = "Please enter Boxes ";
                    return false;
                }else {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }
                
                if (trim(document.frmcat.gross_weight.value) == "")  {
                    document.getElementById("lblgross").innerHTML = "Please enter Gross Weight ";
                    return false;
                }else {
                    document.getElementById("lblgross").innerHTML = "";
                    return true;
                }                
                
                if (trim(document.frmcat.volume_weight.value) == "")  {
                    document.getElementById("lblvolume").innerHTML = "Please enter Volume Weight ";
                    return false;
                }else {
                    document.getElementById("lblvolume").innerHTML = "";
                    return true;
                }                                
                // New
                if (trim(document.frmcat.per_kg.value) == "")  {
                    document.getElementById("lblperkg").innerHTML = "Please enter Kg. ";
                    return false;
                }else {
                    document.getElementById("lblperkg").innerHTML = "";
                    return true;
                }  
                
                if (trim(document.frmcat.air_waybill.value) == "")  {
                    document.getElementById("lblair").innerHTML = "Please enter Air Waybill ";
                    return false;
                }else {
                    document.getElementById("lblair").innerHTML = "";
                    return true;
                }   
                
                if (trim(document.frmcat.charges_due_agent.value) == "")  {
                    document.getElementById("lblcharges").innerHTML = "Please enter Charges ";
                    return false;
                }else {
                    document.getElementById("lblcharges").innerHTML = "";
                    return true;
                } 
              /*  
                if (trim(document.frmcat.handling_lax.value) == "")  {
                    document.getElementById("lblhandling").innerHTML = "Please enter Handling Lax ";
                    return false;
                }else {
                    document.getElementById("lblhandling").innerHTML = "";
                    return true;
                }          
                
                if (trim(document.frmcat.brokerage_lax.value) == "")  {
                    document.getElementById("lblbrokerage").innerHTML = "Please enter Brokerage Lax ";
                    return false;
                }else {
                    document.getElementById("lblbrokerage").innerHTML = "";
                    return true;
                }  */                                              
                
                if (trim(document.frmcat.tot_shiping_imp_cost.value) == "")  {
                    document.getElementById("lbltot_shiping_imp").innerHTML = "Please enter Shipping Import Costs ";
                    return false;
                }else {
                    document.getElementById("lbltot_shiping_imp").innerHTML = "";
                    return true;
                }                                                                
                
                if (trim(document.frmcat.cad.value) == "")  {
                    document.getElementById("lblcad").innerHTML = "Please enter CAD ";
                    return false;
                }else {
                    document.getElementById("lblcad").innerHTML = "";
                    return true;
                }  
                if (trim(document.frmcat.duties.value) == "")  {
                    document.getElementById("lblduties").innerHTML = "Please enter Duties ";
                    return false;
                }else {
                    document.getElementById("lblduties").innerHTML = "";
                    return true;
                }                                                                                                
                
                if (trim(document.frmcat.porce_duties.value) == "")  {
                    document.getElementById("lblporduties").innerHTML = "Please enter %Duties ";
                    return false;
                }else {
                    document.getElementById("lblporduties").innerHTML = "";
                    return true;
                }     
                if (trim(document.frmcat.porce_handling.value) == "")  {
                    document.getElementById("lblporhand").innerHTML = "Please enter %Handling ";
                    return false;
                }else {
                    document.getElementById("lblporhand").innerHTML = "";
                    return true;
                }                                                                                                                                
            }

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Invoice Headboard</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="invoice_mgmt.php" onclick="this.blur();"><span> Invoice</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="update_invoice_cab_edit.php?id_fact=<?php echo $row["id_fact"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>



                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Order Number </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="order_number" id="order_number" value="<?php echo $row["order_number"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblorder_number"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Total Boxes </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="total_boxes" id="total_boxes" value="<?php echo $row["total_boxes"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblfrom"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Gross Weight </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="gross_weight" id="gross_weight" value="<?php echo $row["gross_weight"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblgross"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Volume Weight </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="volume_weight" id="volume_weight" value="<?php echo $row["volume_weight"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblvolume"></span></td>
                                                                                </tr>                                                                                                                                                                                                                                                                                                                                  
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Per Kg </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="per_kg" id="per_kg" value="<?php echo $row["per_kg"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblperkg"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Air Waybill </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="air_waybill" id="air_waybill" value="<?php echo $row["air_waybill"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblair"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Charges Due Agent </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="charges_due_agent" id="charges_due_agent" value="<?php echo $row["charges_due_agent"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcharges"></span></td>
                                                                                </tr>                                                                                  
                                                                                
                                                                                <!--tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Handling Lax </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="handling_lax" id="handling_lax" value="<?php echo $row["handling_lax"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblhandling"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Brokerage Lax </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="brokerage_lax" id="brokerage_lax" value="<?php echo $row["brokerage_lax"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblbrokerage"></span></td>
                                                                                </tr-->  


                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>% Duties </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="porce_duties" id="porce_duties" value="<?php echo $row["porce_duties"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblporduties"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>% Handling </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="porce_handling" id="porce_handling" value="<?php echo $row["porce_handling"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblporhand"></span></td>
                                                                                </tr>                                                                                  

                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Total Freight ($ per kg):</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="tot_shiping_imp_cost" id="tot_shiping_imp_cost" value="<?php echo $row["tot_shiping_imp_cost"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lbltot_shiping_imp"></span></td>
                                                                                </tr>                                                                                                                                                                  
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>CAD</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="cad" id="cad" value="<?php echo $row["cad"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcad"></span></td>
                                                                                </tr>                                                                                                                                                                                                                                                  
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Duties</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="duties" id="duties" value="<?php echo $row["duties"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblduties"></span></td>
                                                                                </tr>    
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Packing Box</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="box_packing" id="box_packing" value="<?php echo $row["box_packing"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblbox_packing"></span></td>
                                                                                </tr>                                                                                                                                                                                                                                                  
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Freight Truck</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="truck" id="truck" value="<?php echo $row["truck"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lbltruck"></span></td>
                                                                                </tr> 
                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>FreshLife Price</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_Flf" id="price_Flf" value="<?php echo $row["price_Flf"]?>" />
                                                                                </tr>                                                                                                                                                                    
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Client Price</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_client" id="price_client" value="<?php echo $row["price_client"]?>" />
                                                                                </tr>                                                                                                                                                                                                                                                    
                                                                                

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Quality</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="quality" id="quality" value="<?php echo $row["quality"]?>" />
                                                                                </tr>                                                                                                                                                                                                                                                    


                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Invoice Status </td>
                                                                                    
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                    <select name="bill_state" id="bill_state" class="listmenu">
                                                                                        <option value="P" <?php  if($row["bill_state"] == 'P') echo "selected";?>>Pending</option>
                                                                                        <option value="F" <?php  if($row["bill_state"] == 'F') echo "selected";?>>Billed</option>
                                                                                        <option value="I" <?php  if($row["bill_state"] == 'R') echo "selected";?>>In Process</option>                                                                                        
                                                                                    </select>                                                                                        
                                                                                        
                                                                                    </td>
                                                                                </tr>                                                                                    
                                                                                
                                                                                <!--tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Farm Price</td>
                                                                                    <td class="text" align="left"><?php echo number_format($row['farm_price_tot'], 2, '.', ',')?>  </td>      
                                                                                                                                                                                
                                                                                </tr-->                                                                                                                                                                                                                                                                                                                                                                                                                  
                                                                                <tr>

                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>

                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>