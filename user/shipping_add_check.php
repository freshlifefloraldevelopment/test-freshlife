<?php
include "../config/config_gcp.php";
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
    $count = $_POST['count'];
    $con_array = array();
    if ($count == 2) {
        $sourceConnection = $_POST['connections_1'];
        $getConnection = mysqli_query($con, "select * from connections where id=" . $sourceConnection);
        $connectionData = mysqli_fetch_assoc($getConnection);
        if ($connectionData['type'] == 3) {
            $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $connectionData['from1'] . "'";
            $airRes = mysqli_query($con, $getAirport);
            $fromData = mysqli_fetch_assoc($airRes);
            $source_country = $fromData['airport_country'];
            $source_country_name = $fromData['airport_name'];
            $destination_country = $connectionData['to1'];
            $getdestination_country_name = "SELECT * FROM country WHERE id='" . $destination_country . "'";
            $getdestination_country_nameres = mysqli_query($con, $getdestination_country_name);
            $getdestination_country_nameres = mysqli_fetch_assoc($getdestination_country_nameres);
            $destination_country_name = $getdestination_country_nameres['name'];
        } else if ($connectionData['type'] == 4) {
            $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $connectionData['from1'] . "'";
            $airRes = mysqli_query($con, $getAirport);
            $fromData = mysqli_fetch_assoc($airRes);
            $source_country = $fromData['airport_country'];
            $source_country_name = $fromData['airport_name'];
            $destination_country = $connectionData['to1'];
            $destination_country_name = 'Local';
        } else {
            $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $connectionData['from1'] . "'";
            $airRes = mysqli_query($con, $getAirport);
            $airportArray = array();
            $fromData = mysqli_fetch_assoc($airRes);
            $source_country_name = $fromData['airport_name'];

            $getAirportTo = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $connectionData['to1'] . "'";
            $airResTo = mysqli_query($con, $getAirportTo);
            $toData = mysqli_fetch_assoc($airResTo);

            $source_country = $fromData['airport_country'];
            $destination_country = $toData['airport_country'];
            $destination_country_name = $toData['airport_name'];
        }
    } else {
        $totalConnection = $count - 1;
        $sourceConnection = $_POST['connections_1'];
        $getConnection = mysqli_query($con, "select * from connections where id=" . $sourceConnection);
        $connectionData = mysqli_fetch_assoc($getConnection);

        $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $connectionData['from1'] . "'";
        $airRes = mysqli_query($con, $getAirport);
        $fromData = mysqli_fetch_assoc($airRes);
        $source_country = $fromData['airport_country'];
        $source_country_name = $fromData['airport_name'];

        $destinationConnection = $_POST['connections_' . $totalConnection];
        $getDesConnection = mysqli_query($con, "select * from connections where id=" . $destinationConnection);
        $connectionDesData = mysqli_fetch_assoc($getDesConnection);

        if ($connectionDesData['type'] == 4) {
            $destination_country = $connectionDesData['to1'];
            $destination_country_name = 'Local';
        } else if ($connectionDesData['type'] == 3) {
            $getAirportDes = "SELECT * FROM country WHERE id='" . $connectionDesData['to1'] . "'";
            $airResDes = mysqli_query($con, $getAirportDes);
            $toDataDes = mysqli_fetch_assoc($airResDes);
            $destination_country = $toDataDes['id'];
            $destination_country_name = $toDataDes['name'];
        }else {
            $getAirportDes = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $connectionDesData['to1'] . "'";
            $airResDes = mysqli_query($con, $getAirportDes);
            $toDataDes = mysqli_fetch_assoc($airResDes);
            $destination_country = $toDataDes['airport_country'];
            $destination_country_name = $toDataDes['airport_name'];
        }

    }
    $name = $source_country_name . ' to ' . $destination_country_name;
    for ($i = 1; $i <= $count; $i++) {
        if (!empty($_POST['connections_' . $i])) {
            $con_array['connection_' . $i] = $_POST['connections_' . $i];
        }
    }
    
    $temp = implode(',', $_POST['chk']);
    $temp = trim($temp, ",");
    
    $connections = serialize($con_array); //implode(',', $_POST['connections']);
    $ins = "insert into shipping_method set 
                        name                ='" . trim($name) . "'             ,
                        description         ='" . trim($_POST["cat_desc"]) . "',
                        connections         ='" . $connections . "'            ,
                        source_country      ='" . $source_country . "'         ,
                        destination_country ='" . $destination_country . "'    ,
                        destiny             ='" . $_POST["destiny"] . "'       ,
                        days                ='" . $temp . "'   ";
    
    mysqli_query($con, $ins);
    header('location:shipping_mgmt.php');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/select2/select2.min.js"></script>
        <link href="js/select2/select2.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function () {
                $('select').select2();
            });
            function select_connection(val, id) {
                var cnt = $('#count').val();
                if (id == cnt) {
                    var nxt_id = (id * 1) + 1;
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>user/connection_ajax.php',
                        data: 'cid=' + val + '&id=' + id,
                        success: function (res) {
                            $('#selectConnection_' + id).after(res);
                            $('select').select2();
                            $('#count').val((cnt * 1) + 1);
                        }
                    });
                } else {
                    var nxt_id = (id * 1) + 1;
                    for (var i = nxt_id; i <= cnt; i++) {
                        console.log(i);
                        $('#selectConnection_' + i).remove();
                    }
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>user/connection_ajax.php',
                        data: 'cid=' + val + '&id=' + id,
                        success: function (res) {
                            $('#selectConnection_' + id).after(res);
                            $('select').select2();
                            $('#count').val(nxt_id);
                        }
                    });
                }
            }

            function verify()         {
                var arrTmp = new Array();
                arrTmp[0] = checkcat_desc();
                arrTmp[1] = checkcat_con();
                var i;
                _blk = true;
                
                for (i = 0; i < arrTmp.length; i++)        {
                    if (arrTmp[i] == false)      {
                        _blk = false;
                    }
                }
                if (_blk == true)    {
                    return true;
                } else   {
                    return false;
                }
            }

            function trim(str) {
                if (str != null)     {
                    var i;
                    for (i = 0; i < str.length; i++)     {
                        if (str.charAt(i) != " ")    {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }
                    for (i = str.length - 1; i >= 0; i--)        {
                        if (str.charAt(i) != " ")       {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }
                    if (str.charAt(0) == " ")    {
                        return "";
                    }  else  {
                        return str;
                    }
                }
            }

            function checkcname()    {
                if (trim(document.frmcat.cname.value) == "")    {
                    document.getElementById("lblcname").innerHTML = "Please enter shipping method name";
                    return false;
                }  else   {
                    document.getElementById("lblcname").innerHTML = "";
                    return true;
                }
            }

            function checkcat_desc()      {
                if (trim(document.frmcat.cat_desc.value) == "")    {
                    document.getElementById("lblcat_desc").innerHTML = "Please enter shipping description";
                    return false;
                }  else {
                    document.getElementById("lblcat_desc").innerHTML = "";
                    return true;
                }
            }

            function checkcat_con()  {
                if (trim(document.frmcat.Connection_1.value) == "")  {
                    document.getElementById("lblcat_connections_1").innerHTML = "Please select atleast one connection";
                    return false;
                } else   {
                    document.getElementById("lblcat_connections_1").innerHTML = "";
                    return true;
                }
            }

        </script>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="5"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pagetitle">Add New Shipping Methos</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><table width="100%">
                                                                            <tr>
                                                                                <td><a class="pagetitle1" href="shipping_mgmt.php" onclick="this.blur();"><span> Manage Shipping Methods</span></a></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>

                                                                <tr>
                                                                    <td><div id="box">
                                                                            <input type="hidden" id="count" name="count" value="1" />
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                                                                </tr>
                                                                                <!--tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Name </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="cname" id="cname" value="" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcname"></span></td>
                                                                                </tr-->
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp; Description</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><textarea name="cat_desc" class="textarea"></textarea>
                                                                                        <br>
                                                                                            <span class="error" id="lblcat_desc"></span>
                                                                                    </td>
                                                                                </tr
                                                                                
                                                                                    <tr class="conn_des" id="selectDestiny_1">
                                                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp; Select Destiny</td>
                                                                                        <td width="66%" bgcolor="#f2f2f2">
                                                                                            <?php $qry = mysqli_query($con, "select * from destiny"); ?>
                                                                                            <select class="" id="destiny" onchange="select_connection(this.value, '1')" name="destiny" style="width:230px;">
                                                                                                <option value="">Select Destiny</option>
                                                                                                <?php while ($dest = mysqli_fetch_array($qry)) { 
                                                                                                    
                                                                                                    ?>
                                                                                                    <option value="<?php echo $dest['id'] ?>" <?php echo ($dest['id'] == $info['iddest'] ) ? 'selected="selected"' : ''; ?>><?php echo $dest['descripcion']; ?></option>
                                                                                                <?php } ?>
                                                                                            </select>
                                                                                            <br>
                                                                                                <span class="error" id="lbldestiny"></span>
                                                                                        </td>
                                                                                    </tr>    
                                                                                
                                                                                <tr id="transit_days">
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Delivery Day</td>
                                                                                    <td><div>
                                                                                            <input name="chk[]" id="chk1" type="checkbox" value="1" />&nbsp;&nbsp;Monday<br/>
                                                                                            <input name="chk[]" id="chk2" type="checkbox" value="2" />&nbsp;&nbsp;Tuesday<br/>
                                                                                            <input name="chk[]" id="chk3" type="checkbox" value="3" />&nbsp;&nbsp;Wednesday<br/>
                                                                                            <input name="chk[]" id="chk4" type="checkbox" value="4" />&nbsp;&nbsp;thursday<br/>
                                                                                            <input name="chk[]" id="chk5" type="checkbox" value="5" />&nbsp;&nbsp;Friday<br/>
                                                                                            <input name="chk[]" id="chk6" type="checkbox" value="6" />&nbsp;&nbsp;Saturday<br/>
                                                                                            <input name="chk[]" id="chk0" type="checkbox" value="0" />&nbsp;&nbsp;Sunday<br/> 
                                                                                        </div></td>
                                                                                </tr>
                                                                                
                                                                                
                                                                                <tr class="conn_tr" id="selectConnection_1">
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp; Select Connections</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <select class="" id="Connection_1" name="connections_1" style="width:330px;">
                                                                                            <option value="">Select Connection.</option>
                                                                                        </select>
                                                                                        <br>
                                                                                            <span class="error" id="lblcat_connections_1"></span>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                       

                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>

                                                            </table>
                                                        </form></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
<?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
