<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idbuy = $_GET['id_buy'];    
    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                    from invoice_orders
                   where buyer_id = '" . $idbuy . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Packing
   
   $sqlDetalis="select ir.id           , ir.id_fact  , ir.id_order     , ir.order_number   , ir.order_serial , 
                       ir.offer_id     , ir.product  , ir.prod_name    , ir.buyer          , ir.grower_id    , 
                       ir.box_qty_pack , ir.qty_pack as bunchqty       , ir.qty_box_packing, ir.box_type     , ir.comment      , 
                       ir.date_added   , ir.box_name , ir.size         , ir.steams         ,
                       substr(rg.growers_name,1,19) as name_grower     , ir.price          , ip.offer_id as id_request,
                       ip.prod_name as packing_name,
                       sc.id, sc.name as cliente,ir.cliente_id, ir.product_subcategory as subcate_real,
                       cl.name as colorname
                  from invoice_packing_box ir
                 INNER JOIN growers rg ON ir.grower_id = rg.id
                 inner join product p on ir.prod_name = p.name and ir.product_subcategory = p.subcate_name 
                 inner join colors cl ON p.color_id = cl.id                                           
                  left JOIN sub_client sc ON ir.cliente_id = sc.id
                  left join invoice_packing ip ON ir.id_order = ip.id      
                 where ir.buyer    = '" . $buyer_cab . "'
                   and ir.id_fact  = '" . $id_fact_cab . "' 
                 order by ir.grower_id , ir.qty_box_packing ";

        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  PACKING LIST',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Name ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');  
       
    
    if ($userSessionID != 318) {
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');
       $pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');     
    }else{
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');  
    }
      
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,0,'L');        
    $pdf->Cell(0,6,'Gross Weight: '.$buyerOrderCab['gross_weight'],0,1,'R');        
    
    $pdf->Cell(70,6,'-'.$buy['company'],0,0,'L');  
    $pdf->Cell(0,6,'Volume Weight: '.$buyerOrderCab['volume_weight'],0,1,'R');  
    
    $pdf->Ln(10);
    $pdf->Cell(40,6,'Box Number',0,0,'C');    
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(35,6,'Bunch/Box',0,1,'C');
   // $pdf->Cell(35,6,'Cliente',0,1,'C');    

    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
        
        //  Numero de Cajas
        $sel_boxg = "select box_name,count(*) 
                       from invoice_packing_box
                      where id_fact = '" . $row['id_fact'] . "'
                        and grower_id = '" . $row['grower_id'] . "'
                        and box_name != 'Box'    
                      group by box_name ";
        $rs_boxg = mysqli_query($con,$sel_boxg);       
          
        $totalr = mysqli_num_rows($rs_boxg);
        
        $cajas = 0;
        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {
                    $cajas = $cajas + 1;
        }
        
        
        //  Features
        
        /*
        $sel_feature = "select br.feature , f.name as features, br.id , br.product ,  br.qty , br.cod_order , 
                               br.buyer
                          from buyer_requests br
                         INNER JOIN features f  ON br.feature = f.id
                         where br.id_order = '" . $row['id_fact'] . "'
                           and br.id       = '" . $row['id_request'] . "' ";
        */
        
        $sel_feature = "select br.feature , f.name as features, br.id , br.product ,  br.qty , br.cod_order , 
                               br.buyer
                          from buyer_requests br
                         INNER JOIN features f  ON br.feature = f.id
                         where br.id       = '" . $row['id_request'] . "' ";        
        $rs_features = mysqli_query($con,$sel_feature);       
          
        
        $feature_name = " ";
        while($cod_feature = mysqli_fetch_array($rs_features))  {
                    $feature_name = $cod_feature['features'];
        }        
        
        
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type , subcategoryid from product where name = '" . $row['packing_name'] . "' and subcate_name = '" . $row['subcate_real'] . "' ";        
      //  $sel_bu_st = "select box_type , subcategoryid from product where id = '" . $row['product'] . "' ";                
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                   // $qtyFac = $row['bunchqty']*$row['steams'];
                    $qtyFac = $row['bunchqty'];                   
                    $unitFac = "STEMS";                     
              }else{
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }                        

        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);              
              
              
         //      $subtotalStems= $row['steams'] *$row['bunchqty'] ;                    
                 $subtotalStems= $row['bunchqty'] ;      
                  
         if ($row['grower_id'] != $tmp_idorder) {
               $pdf->SetFont('Arial','B',8);                   
               if ($sw == 1) { 
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Bunches  : ".$subStemsGrower,0,1,'L');                                 
               }
                      $sw = 1;             
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                     // $pdf->Cell(70,6,$row['name_grower']." (".$cajas." ".$row['boxtype'].")",0,1,'L');  
                      $pdf->Cell(70,6,$row['name_grower']." (".$cajas." "."Boxes".")",0,1,'L');   
                              $cajastot =  $cajastot + $cajas;
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $subStemsGrower = 0;                      
         }

  $pdf->SetFont('Arial','',8);
         $pdf->Cell(40,4,$row['box_name'],0,0,'C');   
         if ($subc['name'] == 'Snapdragon'||$subc['name'] == 'Alstroemeria'||$subc['name'] == 'Carnations'||$subc['name'] == 'Mini Carnations') { 
             $pdf->Cell(70,4,$row['subcate_real']." ".$row['packing_name']." ".$row['steams']." st/bu  ".$feature_name." ".$row['colorname'],0,0,'L');
         }else{
            $pdf->Cell(70,4,$row['subcate_real']." ".$row['packing_name']." ".$row['size']." cm ".$row['steams']." st/bu  ".$feature_name." ".$row['colorname'],0,0,'L');
         }
         
         $pdf->Cell(35,4,$row['bunchqty'],0,1,'C');                                                   
       //  $pdf->Cell(35,4,$row['cliente'],0,1,'C');
         
        

            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['grower_id'];
          //  $subStemsGrower= $subStemsGrower + ($row['steams'] *$row['bunchqty']) ; 
            $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;             
    }
    
    
    
    
                      $pdf->SetFont('Arial','B',8);                   
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Bunches: ".$subStemsGrower,0,1,'L');  
    
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
    
            $pdf->SetFont('Arial','B',10);            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L'); 
            $pdf->Cell(0,6,'Total Bunches Packing:'.number_format($totalStems, 0, '.', ','),0,1,'R');                    
  
            $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');   
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L'); 
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');   
    
  $pdf->Output();
  ?>