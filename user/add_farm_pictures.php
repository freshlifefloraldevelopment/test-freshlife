<?php	

	include "../config/config_gcp.php";

	@session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}
        
        if(isset($_POST['add_pictures'])){
            for($i = 0; $i < count($_FILES['farm_picture']['name']); $i++){
                if($_FILES['farm_picture']['name'][$i] != ""){
                    $tmp1 = $_FILES['farm_picture']['name'][$i];
                    $ext1 = explode('.',$tmp1);
                    $image=0;
                    $uploaddir = 'grower_farm_pictures/';
                    $uploadfile = $uploaddir .rand(0,99999)."-".date("YmdHis")."_".$ext1[0].".".$ext1[1];
                    if($ext1[1] == 'png' || $ext1[1] == 'jpg' || $ext1[1] == 'jpeg'){
                        move_uploaded_file($_FILES['farm_picture']['tmp_name'][$i],$uploadfile);
                        $grower_id = $_POST['grower_id'];
                        $sql = "INSERT INTO grower_farm_pictures (gfp_grower_idFk, gfp_picture, gfp_create_datetime) VALUES ($grower_id, '".$uploadfile."', '".date("Y-m-d H:i:s")."')";
                        $result = mysqli_query($sql);
                        
                    }
                }
            
            }
        
        
        }
        
        if(isset($_GET['id']) && isset($_GET['pid'])){
            $sql = "SELECT * 
                      FROM grower_farm_pictures 
                     WHERE gfp_grower_idFk ='".$_GET['id'] ."'  
                       AND gfp_id          ='".$_GET['pid']."' ";   
            
            $res = mysqli_query($con,$sql);
            
            if(mysqli_num_rows($res) > 0){
                $row = mysqli_fetch_array($res);
                $pic_name = $row['gfp_picture'];
                $path = dirname(__FILE__).'/'.$pic_name;
                $sql1 = "DELETE FROM grower_farm_pictures WHERE gfp_grower_idFk = '".$_GET['id'] ."' AND gfp_id = '".$_GET['pid']."'";
                $res1 = mysqli_query($con,$sql1);
                if(mysqli_affected_rows() > 0){
                  unlink($path);
                }
            }
            header("Location:".SITE_URL."user/add_farm_pictures.php?id=".$_GET['id']);
        
        }
        
        if(isset($_GET['id'])){
            $sql = "SELECT * FROM grower_farm_pictures WHERE gfp_grower_idFk = '".$_GET['id'] ."' ORDER BY gfp_id DESC";
            $res = mysqli_query($sql);
            
        
        }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {


			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Add Farm Pictures</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

				  <tr>

                    <td>
                        <style>
                            #add_pictures_table tr td{padding-bottom:20px;}
                            #container ul, #container h2, form{padding-left:3%; }
                            #container ul li{width:120px; height: 150px; margin-left:10px; margin-bottom: 10px; list-style-type:none; float:left;}
                            #container ul li img{width:120px; height: 120px;}
                            #container ul li a{text-align:center; display: inline-block; width:100%;}
                        </style>
                        <form action="" method="post" enctype="multipart/form-data">
					<table width="100%" id="add_pictures_table">

					<tr>

					<td>
                                            <label>Upload Farm Picture</label>
					</td>
                                        <td>
                                            <input type="file" name="farm_picture[]" /> 
                                        </td>
                                            <td><button type="button" onclick="add_more_picture();">Add</button></td>   
                                            <td></td>
					</tr>
                                        
					</table>
                                        <table width="100%">
                                            <tr><td><input type="hidden" name="grower_id" value="<?php echo (isset($_GET['id']))? $_GET['id'] : ''; ?>" /><input type="submit" name="add_pictures" value="Save" /></td></tr>
                                        </table>
                                    </form>
					</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">
                    <h2>Uploaded Farm Pictures</h2>
                    <ul>
                    <?php
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){
                    ?>        
                        <li><img src="<?php echo SITE_URL . 'user/' . $row['gfp_picture']; ?>" alt="Farm Picture"><a href="<?php echo SITE_URL?>user/add_farm_pictures.php?id=<?php echo $_GET['id'] ?>&pid=<?php echo $row['gfp_id']; ?>" type="button">Delete</a></li>
                    <?php
                        }
                    }
                    else{
                        echo '<b>You have not uploaded any farm picture yet.</b>';
                    }
                    
                    ?>
                    </ul>
                </div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>
<script>
    function add_more_picture(){
        var html = '<tr><td><label></label></td><td><input type="file" name="farm_picture[]" /></td><td><button type="button" onclick="remove_picture(this);">Remove</button></td></tr>';
        $('#add_pictures_table').append(html);
    }
    
    function remove_picture(obj){
        $(obj).parent().parent().remove();
        
    }
</script>
</body>

</html>
