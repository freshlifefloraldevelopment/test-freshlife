<?php
include "../config/config_new.php";
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
    $vcode = trim($_POST["code"]);
    $vvalue = trim($_POST["value"]);
    $vvalidtill = date("Y-m-d H:i:s", strtotime(trim($_POST["valid_till"])));
    $vbuyerid = trim($_POST["buyer"]);
    $vcreatedat = date("Y-m-d H:i:s");
    
	$v_sql = "SELECT * FROM `vouchers` WHERE `v_code` = '".$vcode."'";
	$v_res = mysqli_query($con, $v_sql);
	if(mysqli_num_rows($v_res) == 0){
	
		$ins = "INSERT INTO `vouchers` (`v_code`,`v_value`, `v_valid_till`, `v_buyer_id`, `v_created_at`) VALUES ('".$vcode."','" . $vvalue . "','" . $vvalidtill . "', '".$vbuyerid."', '".$vcreatedat."')";
		//echo $ins;
		mysqli_query($con, $ins);
		header('location:manage_vouchers.php');
	}
	else{
		$_SESSION['error'] = 'This voucher code already exists, Please try to add some unique code.';
	}
}

$b_sql = "SELECT * FROM buyers ORDER BY first_name";
$b_res=mysqli_query($con,$b_sql);
//var_dump($b_res);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
		<link rel="stylesheet" href="<?=$siteurl?>user/js/datepicker/css/jquery.datepick.css" type="text/css">
		<script src="<?=$siteurl?>user/js/datepicker/ui/jquery.datepick.js"></script>
		<!------ Code for $.browser error   --------->
		<script src="<?=$siteurl?>user/js/browser.js"></script>
		
		<!----- $.browser Code End Here   ------------>		
		
		<style>
			button.datepick-trigger{
				width: 100px;
				height: 24px;
			}
		</style>
		
		<script>
			$(document).ready(function(){
				$('#valid_till').datepick({
					//showOn: "button",
					//buttonImage: "<?=$siteurl?>css/calendar-green.gif",
					dateFormat: 'dd-mm-yy',
					buttonText:"Expire Date",
					yearRange:"2013:2050",
					minDate: new Date(),
					//buttonImageOnly: true,
					
				});
			});
		</script>
        <script type="text/javascript">
			function verify()
            {
                var isValid = true;
				
                if (trim(document.form_voucher.code.value) == "")
                {
                    document.getElementById("lblcode").innerHTML = "Please enter unique voucher code.";
                    isValid = false;
					
                }
                else
                {
                    document.getElementById("lblcode").innerHTML = "";
                } 
                
                if (trim(document.form_voucher.value.value) == "")
                {
                    document.getElementById("lblvalue").innerHTML = "Please enter voucher value.";
                    isValid = false;
					//alert(isValid);
                }
                else
                {
                    document.getElementById("lblvalue").innerHTML = "";
                }
                
                if (trim(document.form_voucher.valid_till.value) == "")
                {
                    document.getElementById("lblvalidtill").innerHTML = "Please enter voucher expiry date.";
                    isValid = false;
					
                }
                else
                {
                    document.getElementById("lblvalidtill").innerHTML = "";
                }
				
				if (trim(document.form_voucher.buyer.value) == "")
                {
                    document.getElementById("lblbuyer").innerHTML = "Please select a buyer to assign voucher.";
                    isValid = false;
					
                }
                else
                {
                    document.getElementById("lblbuyer").innerHTML = "";
                }
                
                return isValid;
				
            }

            function trim(str)
            {
                if (str != null)
                {

                    var i;
                    for (i = 0; i < str.length; i++)
                    {

                        if (str.charAt(i) != " ")
                        {

                            str = str.substring(i, str.length);
                            break;

                        }

                    }

                    for (i = str.length - 1; i >= 0; i--)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(0, i + 1);
                            break;
                        }

                    }

                    if (str.charAt(0) == " ")
                    {
                        return "";
                    }

                    else
                    {
                        return str;
                    }

                }

            }

            
        </script>

    </head>

    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/left.php"); ?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Add Discount Voucher</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="manage_vouchers.php" onclick="this.blur();"><span> Manage Discount Vouchers</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>
															
															<tr><td colspan="2">
																
																<?php
																	if($_SESSION['error'] != ''){
																		echo '<p style="color: #f00; padding-bottom:10px;">'.$_SESSION['error'].'</p>';
																		unset($_SESSION['error']);
																	}
																?>
															</td></tr>
															
                                                            <form name="form_voucher" method="post" onsubmit="return verify();">

                                                                <tr>
                                                                    <td><div id="box">
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fields Marked with (<span class="error">*</span>) are Mandatory</td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Voucher Code</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="text" class="textfieldbig" name="code" id="code" value="" />
                                                                                        <br><span class="error" id="lblcode"></span>	

                                                                                    </td>

                                                                                </tr>    
																				<tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Voucher Value ($)</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="text" class="textfieldbig" name="value" id="value" value="" />
                                                                                        <br><span class="error" id="lblvalue"></span>	

                                                                                    </td>

                                                                                </tr>
																				<tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Valid Till</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="text" name="valid_till" id="valid_till" style="height: 15px; width: 230px; border:1px solid #e0e0e0;  font-weight:normal;" />                    
                    </div>
																						<!--<input type="text" class="textfieldbig" name="valid_till" id="valid_till" value="" />-->
                                                                                        <br><span class="error" id="lblvalidtill"></span>	

                                                                                    </td>

                                                                                </tr>
																				<tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Select Buyer</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <!--<input type="text" class="textfieldbig" name="buyer" id="buyer" value="" />-->
																						<select class="listmenu" name="buyer" id="buyer" style="width:230px; height: 22px;">
																							<option value="">-- Select Buyer --</option>
																							<?php 
																								if($b_res){
																								while ($buyer = mysqli_fetch_array($b_res)) {
																							  ?>
																							  <option value="<?= $buyer["id"] ?>"><?= $buyer["first_name"].' '.$buyer["last_name"] ?></option>
																							  <?php
																								}
																							   }
																							?>
																						</select>
                                                                                        <br><span class="error" id="lblbuyer"></span>	

                                                                                    </td>

                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>
                                                            </form>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
