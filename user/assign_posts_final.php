<?php
    // PO #1  2-jul-2018
include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");

}


if (isset($_POST['change_ppic'])) {
    $today = date('mdyHis');
    if ($_FILES['ppic']['name'] != "") {
        $tmp1 = $_FILES['ppic']['name'];
        $ext1 = explode('.', $tmp1);
        $image = 0;
        $uploaddir = 'includes/assets/profile_pictures/';
        $filename = $today . "-" . $ext1[0] . "." . $ext1[1];
        $uploadfile1 = $uploaddir . $filename;

        move_uploaded_file($_FILES['ppic']['tmp_name'], $uploadfile1);
        $sql = "UPDATE admin SET picture = '" . $filename . "' WHERE id = " . $_SESSION['tomodachi-admin'];
        $res = mysqli_query($con, $sql);
        if ($res) {
            header("Location:home.php");
        }
    }

}


$sql_data = "SELECT uname, picture FROM admin WHERE id= " . $_SESSION['tomodachi-admin'] . " AND isadmin = 1";
$data_res = mysqli_query($con, $sql_data);
$row = mysqli_fetch_assoc($data_res);

$pic_path = SITE_URL . 'includes/assets/profile_pictures/' . $row['picture'];
$gr_id = $_GET['id'];

//$get_gr_data = mysqli_query($con,"select * from growers_assign_post where id='$gr_id'");
$get_gr_data = mysqli_query($con,"select * from growers_assign_post where id='5'");

if (mysqli_num_rows($get_gr_data) == 1) {
    $gt_row = mysqli_fetch_assoc($get_gr_data);

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">

            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>
    </head>


<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php include("includes/header_inner.php"); ?>

    <tr>

        <td height="5"></td>

    </tr>

    <tr>

        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <?php include("includes/left.php"); ?>

                    <td width="5">&nbsp;</td>

                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>

                                <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80"/></td>

                                <td valign="top" background="images/images_front/middle-topshade.gif" style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <tr>

                                            <td width="10">&nbsp;</td>

                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                    <tr>

                                                        <td height="5"></td>

                                                    </tr>

                                                    <tr>

                                                        <td class="pagetitle">Assign posts to growers</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>
                                                    <?php
                                                    if (isset($_POST['update_post'])) {
                                                        $grower_name = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['grower_name']))));
                                                        $posts_name = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['posts_name']))));
                                                        $slider_post = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['slider_post']))));
                                                        $bottom_post = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['bottom_post']))));
                                                        $upgr_id = $_GET['id'];
                                                        if ($grower_name != "" && $posts_name != "") {
                                                            $insert_g = mysqli_query($con,"update growers_assign_post  set grower_id='$grower_name',post_name='$posts_name',slider='$slider_post',post='$bottom_post' where id='$upgr_id'");
                                                            if ($insert_g) {
                                                                echo "Blog Post Updated!";
                                                                //header('location: all-assign-posts.php');
                                                            }
                                                        } else {
                                                            echo "Please select Growers and Post name!";
                                                        }
                                                    }
                                                    ?>
                                                    <?php
                                                    if (isset($_POST['save_post'])) {
                                                        $grower_name = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['grower_name']))));
                                                        $posts_name = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['posts_name']))));
                                                        $slider_post = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['slider_post']))));
                                                        $bottom_post = mysqli_real_escape_string(htmlentities(stripcslashes(($_POST['bottom_post']))));

                                                        if ($_POST["grower_name"] != "" && $_POST["posts_name"] != "") {

                                                                $set_order = "insert into growers_assign_post set 
                                                                    grower_id = '".$_POST["grower_name"]."',
                                                                    post_name = '".$_POST["posts_name"]."' ,
                                                                    slider    = '".$_POST["slider_post"]."', 
                                                                    post      = '".$_POST["bottom_post"]."'  ";
                                                                
                                                                $insert_g = mysqli_query($con,$set_order);
                                                                
                                                          //  $insert_g = mysqli_query($con,"insert into growers_assign_post values('$grower_name','$posts_name','$slider_post','$bottom_post')");
                                                                
                                                                
                                                        if ($insert_g) {
                                                                echo "Blog Post Assigned!";
                                                            }
                                                        } else {
                                                            echo "Please select Growers and Post name!";
                                                        }
                                                    }
                                                    ?>
                                                    <?php
/*                                                    
if (function_exists('simplexml_load_file')) {
    echo "simpleXML functions are available.<br />\n";
} else {
    echo "simpleXML functions are not available .<br />\n";
} */                                                   
                                                    if (!($_GET['id'])) {
                                                         
                                                        ?>
                                                        <form action="assign_posts.php" method="post">
                                                            <tr>

                                                                <td><select name="grower_name" style="width:200px;" required>
                                                                        <option value="">Select Growers</option>
                                                                        <?php
                                                                         
                                                                        $select_g = mysqli_query($con,"select id as idgrow, growers_name from growers where active ='active' order by growers_name asc");
                                                                        while ($row_g = mysqli_fetch_assoc($select_g)) {
                                                                            ?>
                                                                            <option value="<?php echo $row_g['idgrow']; ?>"><?php echo $row_g['growers_name']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="posts_name" style="width:200px;" required>
                                                                        <option>Select Posts</option>
                                                                       <!-- <option value="<?php echo $gt_row['post_name']; ?>" selected><?php echo $gt_row['post_name']; ?></option>   -->                                                                     
                                                                        <!--fetch rss data start-->


                                                                        <?php

                                                                        echo "0000000000000000000";
                                                                        $url = 'https://content.freshlifefloral.com/blog/rss.xml';
                                                                        $xml = simplexml_load_file($url);
                                                                        $tags = array();
                                                                        echo "00  ".$xml;
                                                                        foreach ($xml->channel->item as $item) {
                                                                            echo "1";
                                                                            $children = $item->children(); // get all children of each item tag
                                                                            
                                                                            foreach ($children as $node) {
                                                                                echo "2";
                                                                                $tags[] = $node->getName(); // get the node name of each children
                                                                            }
                                                                        }

                                                                        $count = array_count_values($tags); // count the values
                                                                        $rss = new DOMDocument();
                                                                        $rss->load('https://content.freshlifefloral.com/blog/rss.xml');
                                                                        $feed = array();
                                                                        foreach ($rss->getElementsByTagName('item') as $node) {
                                                                            echo "3";
                                                                            $item = array('title' => $node->getElementsByTagName('title')->item(0)->nodeValue);
                                                                            array_push($feed, $item);
                                                                        }
                                                                        $limit = $count['title'];
                                                                        echo "title -- ".$limit;
                                                                        for ($x = 0; $x < $limit; $x++) {
                                                                            $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
                                                                            ?>
                                                                            <option value="<?php echo $title; ?>"><?php echo $title; ?></option>

                                                                         
                                                                            <?php
                                                                        }

                                                                        ?>
                                                                        <!--fetch rss data start-->
                                                                        
                                                                    </select>
                                                                </td>

                                                                <td>
                                                                    <select name="slider_post">
                                                                        <option value="">Display on Slider?</option>
                                                                        <option value="1">Yes</option>
                                                                        <option value="0">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="bottom_post">
                                                                        <option value="">Display as Post?</option>
                                                                        <option value="1">Yes</option>
                                                                        <option value="0">No</option>
                                                                    </select>
                                                                </td>
                                                                <td><input type="submit" name="save_post" value="Assign Post"></td>

                                                            </tr>
                                                        </form>
                                                        <tr>

                                                        </tr>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <form action="assign_posts.php?id=<?php echo $_GET['id']; ?>" method="post">
                                                            <tr>

                                                                <td><select name="grower_name" style="width:200px;" required>
                                                                        <option value="">Select Growers...</option>
                                                                        <?php
                                                                        $select_g = mysqli_query($con,"select * from growers where active = 'active' ");
                                                                        while ($row_g = mysqli_fetch_assoc($select_g)) {
                                                                            ?>
                                                                            <option value="<?php echo $row_g['id']; ?>" <?php if ($row_g['id'] == $gt_row['grower_id']) {
                                                                                echo "selected";
                                                                            } else {
                                                                            } ?>><?php echo $row_g['growers_name']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="posts_name" style="width:200px;" required>
                                                                        <option>Select Posts</option>
                                                                        <option value="<?php echo $gt_row['post_name']; ?>" selected><?php echo $gt_row['post_name']; ?></option>
                                                                        <!--fetch rss data start-->


                                                                        <?php
                                                                        $url = 'https://content.freshlifefloral.com/blog/rss.xml';
                                                                        $xml = simplexml_load_file($url);
                                                                        $tags = array();
                                                                        foreach ($xml->channel->item as $item) {
                                                                            $children = $item->children(); // get all children of each item tag
                                                                            foreach ($children as $node) {
                                                                                $tags[] = $node->getName(); // get the node name of each children
                                                                            }
                                                                        }

                                                                        $count = array_count_values($tags); // count the values
                                                                        $rss = new DOMDocument();
                                                                        $rss->load('https://content.freshlifefloral.com/blog/rss.xml');
                                                                        $feed = array();
                                                                        foreach ($rss->getElementsByTagName('item') as $node) {
                                                                            $item = array(
                                                                                'title' => $node->getElementsByTagName('title')->item(0)->nodeValue
                                                                            );
                                                                            array_push($feed, $item);
                                                                        }
                                                                        $limit = $count['title'];
                                                                        for ($x = 0; $x < $limit; $x++) {
                                                                            $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
                                                                            ?>
                                                                            <?php if ($title != $gt_row['post_name']) { ?>
                                                                                <option value="<?php echo $title; ?>"><?php echo $title; ?></option>


                                                                                <?php
                                                                            }
                                                                        }

                                                                        ?>
                                                                        <!--fetch rss data start-->
                                                                    </select>
                                                                </td>

                                                                <td>
                                                                    <select name="slider_post">
                                                                        <option value="">Display as Slider?</option>
                                                                        <option value="1" <?php if ($gt_row['slider'] == '1') {
                                                                            echo "selected";
                                                                        } ?>>Yes
                                                                        </option>
                                                                        <option value="0" <?php if ($gt_row['slider'] == '0') {
                                                                            echo "selected";
                                                                        } ?>>No
                                                                        </option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="bottom_post">
                                                                        <option value="1" <?php if ($gt_row['post'] == '1') {
                                                                            echo "selected";
                                                                        } ?>>Yes
                                                                        </option>
                                                                        <option value="0" <?php if ($gt_row['post'] == '0') {
                                                                            echo "selected";
                                                                        } ?>>No
                                                                        </option>
                                                                    </select>
                                                                </td>
                                                                <td><input type="submit" name="update_post" value="Update Post"></td>

                                                            </tr>
                                                        </form>
                                                    <?php } ?>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>


                                                    </tr>

                                                    <tr>

                                                        <td><a href="all-assign-posts.php">View All Post</a></td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>


                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                </table>
                                            </td>

                                            <td width="10">&nbsp;</td>

                                        </tr>
                                    </table>
                                </td>

                                <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img src="images/images_front/middle-topright.gif" width="10" height="80"/></td>

                            </tr>

                            <tr>

                                <td background="images/images_front/middle-leftline.gif"></td>

                                <td>&nbsp;</td>

                                <td background="images/images_front/middle-rightline.gif"></td>

                            </tr>

                            <tr>

                                <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10"/></td>

                                <td background="images/images_front/middle-bottomline.gif"></td>

                                <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10"/></td>

                            </tr>

                        </table>
                    </td>

                </tr>

            </table>
        </td>

    </tr>

    <tr>

        <td height="10"></td>

    </tr>

    <?php include("includes/footer-inner.php"); ?>

    <tr>

        <td>&nbsp;</td>

    </tr>

</table>

</body>

</html>

