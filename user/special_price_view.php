<?php	
	include("../config/config_gcp.php");  
	session_start();
	
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}
	
	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM  grower_product_special_price WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
	  header("location:special_price_view.php?id=".$_GET["gid"]);
	}
	
	if(isset($_POST["Submit"]))

	{
	         $qsel="select * from grower_product_special_price where growerid='".$_GET["id"]."' order by prodcutname,date_added,id desc";
	         $rs=mysqli_query($con,$qsel);
			 while($price=mysqli_fetch_array($rs))
			 {
			    $upd="update grower_product_special_price set price=".$_POST[$price["id"]."-price"]." where id='".$price["id"]."'";
				// echo $upd ;
			    mysqli_query($con,$upd);
			 }
	}
	
	
	$sel_info="select * from growers where id='".$_GET["id"]."'";

	$rs_info=mysqli_query($con,$sel_info);

	$info=mysqli_fetch_array($rs_info);
	
	
	$qsel="select * from grower_product_special_price where growerid='".$_GET["id"]."' order by prodcutname,date_added,id desc";
	$rs=mysqli_query($con,$qsel);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
var oTable;
$(document).ready(function() {

   oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});
   
    $('#form1').submit( function () {
        var oSettings = oTable.fnSettings();
        oSettings._iDisplayLength = 5000;
        oTable.fnDraw();
    });
     
    
});
</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/grower-left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">View / Edit Special Prices (
                              <?php echo $info["growers_name"]?>
                              )</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td><a class="pagetitle1" href="special_price_mgmt.php?id=<?php echo $_GET["id"]?>" onclick="this.blur();"><span>Manage Special Prices </span></a> </td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><form action="" id="form1" method="post"><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th width="8%" align="center">Sr.</th>
                                          <th width="15%" align="left">Product</th>
                                           <th width="15%" align="left">Special Feature</th>
                                          <th width="15%" align="left">Size </th>
                                          <th width="15%" align="left">Price ( USD )</th>
                                          <th width="15%" align="left">Date Range </th>
                                           <th width="15%" align="left">Delete</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php

						  	$sr=1;

						  while($category=mysqli_fetch_array($rs))

						  {
						  
						     $temp=explode("-",$category["start_date"]);
							 $temp1=explode("-",$category["end_date"]);
							 
							  $sel_sp_feature="select * from features where id='".$category["feature"]."'";
								   $rs_sp_feature=mysqli_query($con,$sel_sp_feature);
								   $sp_feature=mysqli_fetch_array($rs_sp_feature);
							 

						  ?>
                                        <tr class="gradeU">
                                          <td align="center" class="text"><?php echo $sr;?></td>
                                          <td class="text" align="left"><?php echo $category["prodcutname"]?></td>
                                           <td class="text" align="left"><?php if($sp_feature["name"]!="") { ?> <?php echo $sp_feature["name"]?> <?php } ?></td>
                                          <td class="text" align="left"><?php echo $category["sizename"]?>
                                            CM</td>
                                         
                                          <td class="text" align="left"> <input type="text" name="<?php echo $category["id"]?>-price" value="<?php echo $category["price"]?>" size="10" style="width:70px;"  /></td>
                                          <td class="text" align="left"><?php echo $temp[1]."-".$temp[2]."-".$temp[0]?>
                                            to
                                            <?php echo $temp1[1]."-".$temp1[2]."-".$temp1[0]?></td>
                                            
                                             <td align="center" ><a href="?delete=<?php echo $category["id"]?>&gid=<?php echo $_GET["id"]?>"  onclick="return confirm('Are you sure, you want to delete this special price ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>


                                        </tr>
                                        <?php

						 		$sr++;

						 	}

						 ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div style="float:right;margin-top:20px;"><input type="hidden" name="total" value="<?php echo $sr?>"  />
<input name="Submit" type="Submit" class="buttongrey" value="Update Prices" /></div>
                              </div></form></td>
                          </tr>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
