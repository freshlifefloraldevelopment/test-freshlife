<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests

   $sqlDetalis="select id_fact            , id_order       , order_serial  , 
                       cod_order          , product        , sizeid        , 
                       qty                , buyer          , date_added    , 
                       bunches            , box_name       , lfd           , 
                       comment            , box_id         , shpping_method, 
                       mreject            , bunch_size     , unseen        , 
                       inventary          , offer_id       , prod_name     , 
                       product_subcategory, size           , boxtype       , 
                       bunchsize          , boxqty         , bunchqty      , 
                       steams             , gorPrice       , box_weight    , 
                       box_volumn         , grower_box_name, reject        , 
                       reason             , coordination   , cargo         , 
                       color_id           , gprice         , tax           , 
                       cost_ship          , round(handling,0) as handling       , grower_id     , offer_id_index,
                       substr(rg.growers_name,1,19) as name_grower , salesPrice ,salesPriceCli       
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 where buyer    = '" . $buyer_cab . "'
                   and id_fact  = '" . $id_fact_cab . "' order by grower_id , offer_id  ";   
   
        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'COMMERCIAL INVOICE',0,0,'L'); 
    $pdf->Image('logo.png',148,5); 
    
    $pdf->Ln(10);
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    //$pdf->Cell(70,6,'-'."LLC Lovely Roses",0,0,'L');
      $pdf->Cell(70,6,'- '.$buy['first_name']." ".$buy['last_name'],0,0,'L');    
    $pdf->Cell(0,6,'INVOICE: '.$buyerOrderCab['order_number'],0,1,'R');  
    
   // $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,0,'L');        
    $pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');    
    

    
    $pdf->Cell(70,6,'Receiver: '.$buy['company'],0,1,'L');  
    
    $pdf->Cell(70,6,'Cargo Agency: Ocean Beach',0,0,'L');    
    $pdf->Cell(0,6,'Delivery Terms: CPT',0,1,'R');        
    
    // Fin cabecera
    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Stem/Bunch',0,0,'C');
    $pdf->Cell(25,6,'Sale Price',0,0,'C');    
    $pdf->Cell(25,6,'Qty',0,0,'C');
    $pdf->Cell(25,6,'Subtotal',0,1,'C'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
        $sel_boxg = "select box_name,count(*) 
                       from invoice_packing_box
                      where id_fact = '" . $row['id_fact'] . "'
                        and grower_id = '" . $row['grower_id'] . "'
                        and box_name != 'Box'    
                      group by box_name ";
        $rs_boxg = mysqli_query($con,$sel_boxg);       
          
        $totalr = mysqli_num_rows($rs_boxg);
        
        $cajas = 0;
        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {
                  $cajas = $cajas + 1 ;                  
        }
                
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type , subcategoryid from product where name = '" . $row['prod_name'] . "' ";        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']*$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }        
        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);
         
               $subtotalStems= $row['steams'] *$row['bunchqty'] ;         
         
         if ($row['grower_id'] != $tmp_idorder) {
               $pdf->SetFont('Arial','B',8);                   
               if ($sw == 1) { 
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Stems  : ".$subStemsGrower,0,1,'L');                                 
                      $pdf->Cell(0,6,"Total Grower : $".$subTotalGrower,0,1,'L');                                                       
               }
                      $sw = 1;             
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                       $pdf->Cell(70,6,$row['name_grower']." (".$cajas." "."Boxes".")",0,1,'L');   
                              $cajastot =  $cajastot + $cajas;
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $subStemsGrower = 0;                      
                      $subTotalGrower = 0;                       
         }

  $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(70,4,$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu",0,0,'L');            
         $pdf->Cell(25,6,$unitFac,0,0,'C');                              
         $pdf->Cell(25,6,number_format($row['salesPriceCli'], 2, '.', ','),0,0,'C'); 

if ($bunch_stem['box_type'] == 0) {         
         $pdf->Cell(25,6,$row['bunchqty']*$row['steams'],0,0,'C');  
         $subTotalGrower= $subTotalGrower + ($row['steams']*$row['bunchqty']*$row['salesPriceCli']) ;             
}else{
         $subTotalGrower= $subTotalGrower + ($row['bunchqty']*$row['salesPriceCli']) ;             
         $pdf->Cell(25,6,$row['bunchqty'],0,0,'C');                                           
}         

         $pdf->Cell(25,6,number_format($Subtotal, 2, '.', ','),0,1,'C');  
         
         $pdf->Cell(70,4,$subc['name'],0,1,'L');     
         $pdf->Cell(70,2,'_______________________________________________________________________________________________________________________',0,1,'L');  

            $totalCal = $totalCal + $Subtotal;
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['grower_id'];
            $subStemsGrower= $subStemsGrower + ($row['steams'] *$row['bunchqty']) ; 
            
    }
    $quality = 70;    // parametrizar
    $handling = $totalCal*0.08 ;    // parametrizar    
    
                      $pdf->SetFont('Arial','B',8);                   
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Stems : ".$subStemsGrower,0,1,'L');  
                      $pdf->Cell(0,6,"Total Grower : $".$subTotalGrower,0,1,'L');                                                                             
    
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
    
    if ($userSessionID == 314) {  //DOH
            $pdf->SetFont('Arial','B',10);            
            $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,0,'R');        
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L'); 
            
                    $pdf->Cell(0,6,'Quality Control: $'.number_format($quality, 2, '.', ','),0,1,'R');              
  
            $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');   
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L'); 
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');    
            // $pdf->Cell(70,6,'ANA GUEVARA',0,1,'L');         
            
    } else if ($userSessionID == 48) {    //APX
            $pdf->SetFont('Arial','B',10);            
            $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,0,'R');        
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L'); 
            
                    $pdf->Cell(0,6,'FLF Handling: $'.number_format($handling, 2, '.', ','),0,1,'R');                     
  
            $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');   
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L'); 
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');    
            
    } else if ($userSessionID == 318) {    //VIC
            $pdf->SetFont('Arial','B',10); 
            
            $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,0,'R');        
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
            
            $pdf->Cell(30,6,'Total Boxes:'.$cajastot,0,0,'R');  
            $pdf->Cell(0,6,'Freight ($ per kg): $'.number_format($buyerOrderCab['freight_value'], 2, '.', ','),0,1,'R');            
            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L');
            $pdf->Cell(0,6,'Air Waybill AWC: $'.number_format($buyerOrderCab['air_waybill'], 2, '.', ','),0,1,'R');    

            $pdf->Cell(70,6,'Quito, Ecuador',0,0,'L');             
            $pdf->Cell(0,6,'Charges Dues Agent AWA: $'.number_format($buyerOrderCab['charges_due_agent'], 2, '.', ','),0,1,'R');   
            
            
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,0,'L');            
            $pdf->Cell(0,6,'Brokerage LAX: $'.number_format($buyerOrderCab['brokerage_lax'], 2, '.', ','),0,1,'R');     

            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');
            $pdf->Cell(0,6,'Handling LAX: $'.number_format($buyerOrderCab['handling_lax'], 2, '.', ','),0,1,'R');   
                       
            $pdf->Cell(0,6,'FLF Handling: $'.number_format($buyerOrderCab['handling'], 2, '.', ','),0,1,'R');     
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');    

            
    } else if ($userSessionID == 315) {    // NIC
            $pdf->SetFont('Arial','B',10); 
            
            $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,0,'R');        
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
            
            $pdf->Cell(30,6,'Total Boxes:'.$cajastot,0,0,'R');  
            $pdf->Cell(0,6,'Freight ($ per kg): $'.number_format($buyerOrderCab['freight_value'], 2, '.', ','),0,1,'R');            
            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L');
            $pdf->Cell(0,6,'Air Waybill AWC: $'.number_format($buyerOrderCab['air_waybill'], 2, '.', ','),0,1,'R');    

            $pdf->Cell(70,6,'Quito, Ecuador',0,0,'L');             
            $pdf->Cell(0,6,'Charges Dues Agent AWA: $'.number_format($buyerOrderCab['charges_due_agent'], 2, '.', ','),0,1,'R');   
            
            
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,0,'L');            
            $pdf->Cell(0,6,'FLF Handling: $'.number_format($buyerOrderCab['handling'], 2, '.', ','),0,1,'R');     
            
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');
            $pdf->Cell(0,6,'Credit Card Processing Fees: $'.number_format($buyerOrderCab['credit_card_fees'], 2, '.', ','),0,1,'R');
            
            $pdf->Cell(70,6,'--',0,0,'L');
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');  
            

    } else if ($userSessionID == 321) {    // BLO
            $pdf->SetFont('Arial','B',10); 
            
            $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,0,'R');        
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
                                    
            
            $pdf->Cell(30,6,'Total Boxes:'.$cajastot,0,0,'R');  
            $pdf->Cell(0,6,'FLF Handling: $'.number_format($buyerOrderCab['handling'], 2, '.', ','),0,1,'R');     
            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,1,'L');
           // $pdf->Cell(0,6,'Credit Card Processing Fees: $'.number_format($buyerOrderCab['credit_card_fees'], 2, '.', ','),0,1,'R');

            $pdf->Cell(70,6,'Quito, Ecuador',0,0,'L');             
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');          
            
            
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L');                        
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,1,'L');
            
    }  else if ($userSessionID == 322) {   // LALI
 
                    $quality = 47;    // parametrizar
                    
            $pdf->SetFont('Arial','B',10); 
            
            $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,0,'R');        
            $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
            
            $pdf->Cell(30,6,'Total Boxes:'.$cajastot,0,0,'R');  
            $pdf->Cell(0,6,'Freight ($ per kg): $'.number_format($buyerOrderCab['freight_value'], 2, '.', ','),0,1,'R');            
            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L');
            $pdf->Cell(0,6,'Air Waybill AWC: $'.number_format($buyerOrderCab['air_waybill'], 2, '.', ','),0,1,'R');    

            $pdf->Cell(70,6,'Quito, Ecuador',0,0,'L');             
            $pdf->Cell(0,6,'Charges Dues Agent AWA: $'.number_format($buyerOrderCab['charges_due_agent'], 2, '.', ','),0,1,'R');   
            
            
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,0,'L');            
            $pdf->Cell(0,6,'FLF Handling: $'.number_format($buyerOrderCab['handling'], 2, '.', ','),0,1,'R');     
            
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');
            $pdf->Cell(0,6,'Quality Control: $'.number_format($quality, 2, '.', ','),0,1,'R');     

            $pdf->Cell(70,6,'-',0,0,'L');
            $pdf->Cell(0,6,'Credit Card Processing Fees: $'.number_format($buyerOrderCab['credit_card_fees'], 2, '.', ','),0,1,'R');
            
            $pdf->Cell(70,6,'-',0,0,'L');
            $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');                      
    }
    
    
    
      
  $pdf->Output();
  ?>