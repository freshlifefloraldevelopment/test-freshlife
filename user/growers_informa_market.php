<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
   
    $userSessionID = $_SESSION["buyer"];
    $idbuy = $_GET['id_buy'];    
    $idfac = $_GET['b'];
    $cajastot = 0;
  
    // Datos Growers1
   $Infgrow = "select count(*) as totalGrowers 
                 from growers
                where active = 'active' and market_place = 1 " ;

   $info = mysqli_query($con, $Infgrow);
   $reportGrower = mysqli_fetch_array($info); 
   
   $totalGrower = $reportGrower['totalGrowers'];                         
   
   // Datos Growers2
   
   $sqlDetalis="select ir.grower_id , g.growers_name , max(ir.date_added) last_date , ir.grower_id,
                       g.file_path5 , g.logo         , g.market_place
                  from invoice_requests ir
                 inner join growers g on ir.grower_id = g.id 
                 where g.active = 'active'
                   and g.market_place = 1
                 group by ir.grower_id
                 order by g.market_place,g.file_path5,g.growers_name ";

        $result   = mysqli_query($con, $sqlDetalis);    

        
    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  Growers in Market Place',0,0,'L'); 
    
    
        $pdf->SetFont('Arial','B',10);    
        
    $pdf->Ln(15);
    $pdf->Cell(40,6,'##',0,0,'C');    
    $pdf->Cell(70,6,'Grower',0,0,'L');
    $pdf->Cell(35,6,'Last Date',0,0,'C');
    $pdf->Cell(15,6,'Logo',0,0,'C');
    $pdf->Cell(25,6,'Market Place',0,1,'C');

    $pdf->Cell(70,6,'______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    $ii = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
                                            
       $ii++;   

       if ($row['logo'] == 0) {
           $displayLogo = 'NO';
       }else{
           $displayLogo = 'SI';
       }
       
       if ($row['market_place'] == 0) {
           $displayMP = 'NO';
       }else{
           $displayMP = 'SI';
       }       

     $pdf->SetFont('Arial','',8);
         $pdf->Cell(40,4,$ii,0,0,'C'); 

         $pdf->Cell(70,4,$row['growers_name'],0,0,'L');                     
                           
         $pdf->Cell(35,4,$row['last_date'],0,0,'C');                                                   
         
         $pdf->Cell(15,4,$displayLogo,0,0,'C');                                                            
         $pdf->Cell(25,4,$displayMP,0,1,'C');                                                            
                           
    }
    
    $pdf->Ln(4);
      $pdf->SetFont('Arial','B',10);                           
    
    $pdf->Ln(2);
    $pdf->Cell(70,6,'______________________________________________________________________________________________',0,1,'L');  
    $pdf->Cell(70,6,'Total Active Growers  : '.$totalGrower,0,0,'L');   
       $pdf->Ln(6);     
    $pdf->SetFont('Arial','B',15);
    
                 
    $pdf->Cell(70,6,'Freshlifefloral',0,0,'L');   
    
  $pdf->Output();
  ?>