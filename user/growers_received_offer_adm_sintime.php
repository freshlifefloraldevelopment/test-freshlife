<?php

// PO 2018-09-21 

require_once("../config/config_gcp.php");
session_start();

$growerid   = $_GET['id'];    

$sel_info = "SELECT * FROM growers WHERE id='" . $growerid . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$growers_name = $info["growers_name"];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  

        
         
function shipping_method($shipping_method){
    global $con;
    $getShippingMethod = "SELECT * FROM buyer_shipping_methods WHERE shipping_method_id='" . $shipping_method . "'";
    $shippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);
    return $shippingMethodData;
}   

function country($shipping_country){
    global $con;
    $getCountry = "SELECT * FROM country WHERE id ='" . $shipping_country . "'";
    $countryRes = mysqli_query($con, $getCountry);
    $country = mysqli_fetch_assoc($countryRes);
    return $country;
}

function  box_type($box_type){
    global  $con;
    $sel_box_type = "SELECT id , code as name , descrip FROM units WHERE id='" .$box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type;

}
			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
	      $qsel="SELECT gpt.id AS cartid     ,   gpt.id_order      ,   gpt.order_serial    ,  gpt.cod_order     ,
                            gpt.product          ,   gpt.sizeid        ,   gpt.feature         ,  gpt.noofstems     ,
                            gpt.qty              ,   gpt.buyer         ,   gpt.boxtype         ,  gpt.type          ,
                            gpt.bunches          ,   gpt.box_name      ,   gpt.lfd             ,  gpt.comment       , 
                            gpt.box_id           ,   gpt.shpping_method,   gpt.isy             ,  gpt.bunch_size    ,
                            gpt.unseen           ,   gpt.req_qty       ,   gpt.inventary       ,  gpt.id_client     , 
                            pt.name as prod_name ,   pt.color_id       ,   c.name as colorname ,  gpt.request_color ,
                            st.name AS subs,
                            sht.name AS sizename,
                            fft.name AS featurename,
                            clt.name as namecli
                       FROM buyer_requests gpt
                      INNER JOIN request_growers rgt     ON gpt.id           = rgt.rid
                      INNER JOIN buyer_orders bo         ON gpt.id_order     = bo.id
                       LEFT JOIN product pt              ON gpt.product      = pt.id
                       LEFT JOIN colors c                ON pt.color_id      = c.id  
                       LEFT JOIN subcategory st          ON pt.subcategoryid = st.id  
                       LEFT JOIN features fft            ON gpt.feature      = fft.id
                       LEFT JOIN sizes sht               ON gpt.sizeid       = sht.id 
                       LEFT JOIN sub_client clt          ON gpt.id_client    = clt.id                
                      WHERE rgt.gid='" . $growerid . "' 
                        AND gpt.lfd < '" . date("Y-m-d") . "'
                        AND bo.check_ord = 1
                     UNION
                     SELECT gpb.id AS cartid     ,   gpb.id_order      ,   gpb.order_serial    ,  gpb.cod_order     ,
                            gpb.product          ,   gpb.sizeid        ,   gpb.feature         ,  gpb.noofstems     ,
                            gpb.qty              ,   gpb.buyer         ,   gpb.boxtype         ,  gpb.type          ,
                            gpb.bunches          ,   gpb.box_name      ,   gpb.lfd             ,  gpb.comment       , 
                            gpb.box_id           ,   gpb.shpping_method,   gpb.isy             ,  gpb.bunch_size    ,
                            gpb.unseen           ,   gpb.req_qty       ,   gpb.inventary       ,  gpb.id_client     , 
                            p.name as prod_name  ,   p.color_id        ,   c.name as colorname ,  gpb.request_color ,
                            s.name AS subs,
                            sh.name AS sizename,
                            ff.name AS featurename,
                            cl.name as namecli
                       FROM buyer_requests gpb
                      INNER JOIN request_growers rg     ON gpb.id          = rg.rid
                       LEFT JOIN product p              ON gpb.product     = p.id
                       LEFT JOIN colors c               ON p.color_id      = c.id
                       LEFT JOIN subcategory s          ON p.subcategoryid = s.id  
                       LEFT JOIN features ff            ON gpb.feature     = ff.id
                       LEFT JOIN sizes sh               ON gpb.sizeid      = sh.id 
                       LEFT JOIN sub_client cl ON gpb.id_client = cl.id                
                      WHERE rg.gid='" . $growerid . "' 
                        AND gpb.lfd>='" . date("Y-m-d") . "'  
                        AND gpb.isy = 0    
                      ORDER BY cartid DESC ";	

	 $rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        


                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                                  <td><a><span> RECEIVED OFFERS </span></a></td>
                  </tr>
                        
                            <tr>
                                <td>&nbsp;</td>
                            </tr>                                                
                        
                  <tr>
                      <td><strong> <?php echo $growers_name;?> </strong></td>
                  </tr>                        
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                        <tr>
				<!--td>
                                    <a href="print_confirm.php?id_grow=<?php echo $growerid?>" class="pagetitle" ><span> Confirm Grower </span></a>
				</td-->                        
                        </tr>
                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>       
                    <th align="left" width="10%">Num</th>                     
                    <th align="left" width="25%">Product</th>  
                    <th align="left" width="20%">Comment </th>                    
                    <th align="left" width="15%">Destiny</th>                    
                    <th align="left" width="15%">Lfd</th>                                        
                    <th align="left" width="10%">Units</th>                                                            
                    <th align="left" width="10%">Miss</th>                     
		</tr>

</thead>

	<tbody>
            <?php
                      $sr=1;
                      while($product=mysqli_fetch_array($rs))  {  
                          
                                $shippingMethodData = shipping_method($product['shpping_method']);
                                $country = country($shippingMethodData['country']);
                                $box_type=box_type($product['boxtype']);
                                ////////////////////////////////////////////////
                                // Verificacion Stems/Bunch
                                $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $product["product"] . "' ";
                                $rs_bu_st = mysqli_query($con,$sel_bu_st);       
                                $bunch_stem = mysqli_fetch_array($rs_bu_st); 
                                
                                $sel_box_type = "select substr(code,1,2) as name from units where id='" . $product["boxtype"] . "'";
                                $rs_box_type  = mysqli_query($con, $sel_box_type);
                                $box_type     = mysqli_fetch_array($rs_box_type);                                
                                $unit = $box_type["name"];                                
                                
                                ////////////////////////////////////////////////
                    /*
                                $qselOffer="select gor.steams , gor.bunchqty 
                                              from grower_offer_reply gor
                                             where gor.request_id = '" . $product["cartid"] . "' 
                                               and gor.buyer_id   = '" . $product["buyer"] . "' 
                                               and gor.grower_id  = '" . $growerid . "'     ";	
                    */   
                                
                                $qselOffer="select gor.steams , gor.bunchqty 
                                              from grower_offer_reply gor
                                             where gor.request_id = '" . $product["cartid"] . "' 
                                               and gor.buyer_id   = '" . $product["buyer"] . "'  ";	
                                   $cumple = 0;
                                   
                                 $rs_offer=mysqli_query($con,$qselOffer) ;
                                 
                             while($totoffer=mysqli_fetch_array($rs_offer))  {  
                                                                  
                                    if ($unit == 'ST'){                                        
                                            if ($bunch_stem['box_type'] == 0) {
                                                    $cumple = $cumple + ($totoffer["bunchqty"]*$totoffer["steams"]);
                                            }else{
                                                    $cumple = $cumple + ($totoffer["bunchqty"]);
                                            }                                               
                                    }else{
                                            $cumple = $cumple + ($totoffer["bunchqty"]);
                                    }
                                        
                             }
                              
                                    $porcen = $cumple/$product["qty"]*100;  
                                    
                                    $porcum = $product["qty"]-$cumple;                                
            ?>
                          <tr class="gradeU">    

                                <td class="text" align="left"><?php echo $product["id_order"]."-".$product["order_serial"]?>  </td>                                
                                <td class="text" align="left"><?php echo $product["subs"]." ".$product["prod_name"]." ".$product["sizename"]." cm. ".$product["featurename"]." ".$product["colorname"] ?></td>                                                                                  
                                <td class="text" align="left"><?php echo $product["namecli"]?>  </td>                                                                                                                  
                                <td class="text" align="left"><img src="<?php echo SITE_URL; ?>/includes/assets/images/flags/<?php echo $country['flag'] ?>" width="25"/><?php echo $country['name']; ?></td>
                                <td class="text" align="left"><?php echo $product["lfd"]?> </td> 
                                <td class="text" align="left"><?php echo $product["qty"]." ".$box_type["name"] ?> </td>                                                                  
                                <td class="text" align="left"><?php echo number_format($porcum, 0, '.', ',') ?> </td>                                     

                                <!--td class="text" align="center"><a href="print_confirm.php?idord=<?php echo $product["id_order"]."&id_grow=".$growerid ?>" >Confirm</a> </td-->	                                                                
                                <td class="text" align="center"><a href="grower_offer_adm_add_sintime.php?id=<?php echo $product["cartid"]."&id_grow=".$growerid ?>" >Offers</a> </td>	                                
                          </tr>
	    <?php 
			     $sr++;
			} ?> 	
	</tbody>
</table>
                    </div>
                </div>
            </div>
    </td>
    </tr>
    </table>
    </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>