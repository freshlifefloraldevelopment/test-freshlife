<?php

	include "../config/config_gcp.php";

session_start();

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
    header("location: index.php");
}

if(isset($_POST["Submit"]) && $_POST["Submit"]=="Save"){

        $today = date('mdyHis');

        $upd="update tag_subcategory 
                 set tag_name     = '".$_POST["tag_name"]."',
                     description  = '".$_POST["description"]."',
                     status       = '0' 
               where id = '".$_GET["id"]."'";                                                                                                         

        mysqli_query($con,$upd); 

        header('location:tag_subcategory_mgmt.php');
}

            $sel_category="select * from tag_subcategory where id='".$_GET["id"]."'";
            $rs_category=mysqli_query($con,$sel_category);

            $category=mysqli_fetch_array($rs_category);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript">

    function verify()       {

                var arrTmp = new Array();
                arrTmp[0] = checkcname();

                var i;
                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {

                    if (arrTmp[i] == false)  {
                        _blk = false;
                    }
                }

                if (_blk == true)   {
                    return true;
                } else {
                    return false;
                }
    }

    function trim(str)    {

                if (str != null)   {
                    var i;

                    for (i = 0; i < str.length; i++)   {

                        if (str.charAt(i) != " ") {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--)   {

                        if (str.charAt(i) != " ")   {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    } else {
                        return str;
                    }
                }
    }

    function checkcname()  {

                if (trim(document.frmcat.tag_name.value) == "")   {
                    document.getElementById("lblcname").innerHTML = "Please enter Tag Name";
                    return false;
                } else {
                    document.getElementById("lblcname").innerHTML = "";
                    return true;
                }
    }

    function checkcat_desc()  {

                if (trim(document.frmcat.cat_desc.value) == "")  {
                    document.getElementById("lblcat_desc").innerHTML = "Please enter category description";
                    return false;
                }else {
                    document.getElementById("lblcat_desc").innerHTML = "";
                    return true;
                }
    }

</script>

    </head>

    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php");?>

            <tr> <td height="5"></td> </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/product-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr> <td height="5"></td> </tr>

                                                            <tr>
                                                                <td class="pagetitle">Edit Tag Subcategory</td>
                                                            </tr>

                                                            <tr> <td>&nbsp;</td>  </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>
                                                                                <a class="pagetitle1" href="tag_subcategory_mgmt.php" onclick="this.blur();"><span> Manage Tag Subcategories</span></a>
                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" action="tag_subcategory_edit.php?id=<?php echo $_GET["id"] ?>" method="post" onsubmit="return verify();" enctype="multipart/form-data">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>

                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory                </td>

                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Tag Subcategory</td>

                                                                                    <td width="66%" bgcolor="#f2f2f2">

                                                                                        <input type="text" class="textfieldbig" name="tag_name" id="tag_name" value="<?php echo $category["tag_name"] ?>" />

                                                                                        <br><span class="error" id="lblcname"></span>	
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Description</td>

                                                                                    <td width="66%" bgcolor="#f2f2f2">

                                                                                        <input type="text" class="textfieldbig" name="description" id="description" value="<?php echo $category["description"] ?>" />

                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                               
                                                                                <tr>

                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /></td>

                                                                                </tr>

                                                                                                </table>

                                                                                       </div></td>

                                                                                    </tr>

                                                                                </form>

                                                                            </table></td>

                                <td width="10">&nbsp;</td>

                                </tr>

                                </table></td>

                                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                </tr>

                                <tr>
                                    <td background="images/middle-leftline.gif"></td>

                                    <td>&nbsp;</td>

                                    <td background="images/middle-rightline.gif"></td>
                                </tr>

                                <tr>

                                    <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                    <td background="images/middle-bottomline.gif"></td>

                                    <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                </tr>

                                </table></td>

                                </tr>

                                </table></td>

                                </tr>

                                <tr> <td height="10"></td>  </tr>

                                <?php include("includes/footer-inner.php"); ?>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

</table>

</body>

</html>
