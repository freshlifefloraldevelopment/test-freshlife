<?php

include "../config/config_gcp.php";

session_start();


if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_GET['delete'])) {
    $query = 'DELETE FROM growers_assign_post WHERE  id= ' . (int)$_GET['delete'];
    mysqli_query($con,$query);
}

$qsel = "select * from growers_assign_post order by grower_id";

$rs = mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

    <title>Admin Area</title>

    <link href="css/style.css" rel="stylesheet" type="text/css"/>

    <link href="css/demo_page.css" rel="stylesheet" type="text/css"/>

    <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css"/>

    <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf-8">

        $(document).ready(function () {

            oTable = $('#example').dataTable({

                //"sScrollXInner": "130%",

                "bJQueryUI": true,

                //"sScrollY": "536",

                "sPaginationType": "full_numbers"

            });

        });

    </script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php include("includes/header_inner.php"); ?>

    <tr>

        <td height="5"></td>

    </tr>

    <tr>

        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <?php include("includes/left.php"); ?>

                    <td width="5">&nbsp;</td>

                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>

                                <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80"/></td>

                                <td valign="top" background="images/images_front/middle-topshade.gif" style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <tr>

                                            <td width="10">&nbsp;</td>

                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                    <tr>

                                                        <td height="5"></td>

                                                    </tr>

                                                    <tr>

                                                        <td class="pagetitle">Manage Assign Posts</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>

                                                            <table width="100%">

                                                                <tr>

                                                                    <td>

                                                                        <a href="assign_posts.php" class="pagetitle" onclick="this.blur();"><span> + Assign New Posts</span></a>

                                                                    </td>

                                                                </tr>

                                                            </table>

                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>
                                                            <div id="box">

                                                                <div id="container">

                                                                    <div class="demo_jui">

                                                                        <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

                                                                            <thead>

                                                                            <tr>

                                                                                <th width="8%" align="center">#</th>

                                                                                <th width="15%" align="left">Grower Name</th>

                                                                                <th width="15%" align="left">Post Name</th>

                                                                                <th width="15%" align="left">As Slider</th>

                                                                                <th width="15%" align="left">As Post</th>

                                                                                <th align="center" width="10%">Edit</th>

                                                                                <th align="center" width="12%">Delete</th>

                                                                            </tr>

                                                                            </thead>

                                                                            <tbody>

                                                                            <?php

                                                                            $sr = 1;

                                                                            while ($category = mysqli_fetch_array($rs)) {
                                                                                $rlid = $category['grower_id'];
                                                                                $qt2 = mysqli_query($con,"select * from growers where id='" .$rlid."' ");
                                                                                $rr = mysqli_fetch_assoc($qt2);
                                                                                
                                                                                $grname = $rr['growers_name'];
                                                                                
                                                                                ?>

                                                                                <tr class="gradeU">

                                                                                    <td align="center" class="text"><?php echo $sr; ?></td>

                                                                                    <td class="text" align="left"><?php echo $grname ?></td>

                                                                                    <td class="text" align="left"><?php echo $category["post_name"] ?></td>

                                                                                    <td class="text" align="left"><?php if ($category["slider"] == 1) {
                                                                                            echo "Yes";
                                                                                        } else {
                                                                                            echo "No";
                                                                                        } ?></td>

                                                                                    <td class="text" align="left"><?php if ($category["post"] == 1) {
                                                                                            echo "Yes";
                                                                                        } else {
                                                                                            echo "No";
                                                                                        } ?></td>

                                                                                    <td align="center"><a href="assign_posts.php?id=<?php $category["id"] ?>"><img src="images/images_front/edit.gif" border="0" alt="Edit"/></a></td>

                                                                                    <td align="center"><a href="?delete=<?php $category["id"] ?>" onclick="return confirm('Are you sure, you want to delete this user ?');"><img src="images/images_front/delete.gif" border="0" alt="Delete"/></a></td>

                                                                                </tr>

                                                                                <?php

                                                                                $sr++;

                                                                            }

                                                                            ?>


                                                                            </tbody>

                                                                        </table>


                                                                    </div>

                                                                </div>


                                                            </div>

                                                        </td>

                                                    </tr>

                                                </table>
                                            </td>

                                            <td width="10">&nbsp;</td>

                                        </tr>

                                    </table>
                                </td>

                                <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img src="images/images_front/middle-topright.gif" width="10" height="80"/></td>

                            </tr>

                            <tr>

                                <td background="images/images_front/middle-leftline.gif"></td>

                                <td>&nbsp;</td>

                                <td background="images/images_front/middle-rightline.gif"></td>

                            </tr>

                            <tr>

                                <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10"/></td>

                                <td background="images/images_front/middle-bottomline.gif"></td>

                                <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10"/></td>

                            </tr>

                        </table>
                    </td>

                </tr>

            </table>
        </td>

    </tr>

    <tr>

        <td height="10"></td>

    </tr>

    <?php include("includes/footer-inner.php"); ?>

    <tr>

        <td>&nbsp;</td>

    </tr>

</table>

</body>

</html>
