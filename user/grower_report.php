<?php	

	include "../config/config.php";

	session_start();



	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)

	{

		header("location: index.php");

	}


   if($_SESSION['grower_id']!=0)
			{
			   header("location: growers.php?id=".$_SESSION['grower_id']);
			}  
			


	if(isset($_GET['delete'])) 

	{

	  $query = 'DELETE FROM growers WHERE  id= '.(int)$_GET['delete'];

	  mysql_query($query);

	

	}

	

	$qsel="select g.*,a.uname as person from growers g left join admin a on a.id=g.person_of_charge";
	
	if($_SESSION['uid']!=1)
	{
	
	   $qsel.=" WHERE ( g.person_of_charge='".$_SESSION['uid']."' OR g.person_of_charge=0 )";
	}
	
	 $qsel.=" order by g.growers_name";

	 $rs=mysql_query($qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});

			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/report-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Grower Report</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>
                  
                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>
        
			<th width="20%" align="left">Grower</th> 
            <th width="20%" align="left">Products</th> 
            <th width="20%" align="left">Default Prices</th> 
            <th width="20%" align="left">Special Prices</th> 
		</tr>

	</thead>

	<tbody>

		<?php

						  	$sr=1;

						  while($product=mysql_fetch_array($rs))

						  {

						     $temp=explode(",",$product["products"]);  

							 $no=count($temp);							 

							 if($no>=3)

							 {

							    $no = $no-2; 

							 }

							 else

							 {

							    $no = 0;

							 }

						  ?>

                        <tr class="gradeU">

                          

                          <td class="text" align="left"><?php  echo $product["growers_name"]?></td>
                           <td class="text" align="left"><a href="grower_product_report.php?id=<?php  echo $product["id"]?>"  style="color:#000; font-weight:bold;" >Products(<?php  echo $no?>)</a></td>
                            <td class="text" align="left"><a href="grower_default_price.php?id=<?php  echo $product["id"]?>"  style="color:#000; font-weight:bold;" >Prices</a></td>
                            
                             <td class="text" align="left"><a href="grower_special_price.php?id=<?php  echo $product["id"]?>"  style="color:#000; font-weight:bold;" >Special Prices</a></td>
                         

                          </tr>

						 <?php

						 		$sr++;

						 	}

						 ?> 

	

	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
