<?php


	include "../config/config_gcp.php";

	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}





	$qsel=" select gp.id   , gp.idsc , gp.subcategory , gp.size , gp.stem_bunch  , gp.description ,
							gp.type , gp.feature , gp.factor , gp.id_ficha ,gp.price_adm,
							s.name as namesize   , f.name as namefeature , b.name as namestems, gp.variation_price,
							gp.date_ini, gp.date_end
				 from grower_parameter gp
				inner JOIN sizes s ON gp.size = s.id
				inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
				 left JOIN features f  ON gp.feature = f.id
				where gp.idsc in (select subcaegoryid from grower_card group by subcaegoryid) order by subcategory asc";





	$rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,

					"sPaginationType": "full_numbers"



				});

			} );



			function validate2(evt)
			{
				var theEvent = evt || window.event;

				// Handle paste
				if (theEvent.type === 'paste') {
						key = event.clipboardData.getData('text/plain');

				} else {
				// Handle key press
						var key = theEvent.keyCode || theEvent.which;
						key = String.fromCharCode(key);

				}
				var regex = /[0-9]|\./;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();

				}
			}


function updateCard(id) {

		var txt;
		var r = confirm("Are you sure?");
		if (r == true) {
			var flag_s = true;
			var price = document.getElementById('price-'+id).value;
			var variationP = document.getElementById('variation-price-'+id).value;
			var date_start = document.getElementById('date_ini'+id).value;
			var date_end = document.getElementById('date_end'+id).value;


			var idsc = id;
			var datos = "id_sc="+idsc+"&id_price="+price+"&id_variation="+variationP+"&date_ini="+date_start+"&date_end="+date_end;
			//alert(datos);

	if (flag_s == true) {

			$.ajax({
					type: 'post',
					url: 'updateProductPriceSubcat.php',
					data: datos,
					success: function (data_s) {

							if (data_s == 1) {
							 location.reload();
							}
					}
			});
	}
		}



}

			/*Button send DELETE */
			function deleteT(idsc,size) {

					var txt;
					var r = confirm("Are you sure?");
					if (r == true) {
						var flag_s = true;

						var id_delete = idsc;
						var id_size = size;

						var datos = "id_delete="+id_delete+"&size_id="+id_size;
				//		alert(datos);


				if (flag_s == true) {

						$.ajax({
								type: 'post',
								url: 'deleteProductPriceSubcat.php',
								data: datos,
								success: function (data_s) {
								//	alert(data_s);
									//	if (data_s == 1) {
										 location.reload();
								//		}
								}
						});
				}
					} else {
					  txt = "You pressed Cancel!";
					}



			}




</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/product-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>








                <td>


									<table width="100%" border="0" cellspacing="0" cellpadding="0">

									 <tr>

										<td height="5"></td>

									</tr>

									<tr>
											 <td class="pagetitle"><h1> <img src="images/tag1.png" width="25" height="25" border="0" />  Manage Price Products </h1></td>
									</tr>
									<tr>
										<td>
											<div class="alert alert-secondary" role="alert">

											<img src="images/tag_level1.png" width="25" height="25" border="0" />  <strong>Level *1* Cards</strong>
											</div>

										</td>
									</tr>


									<tr>

										<td>
											<div class="row justify-content-start">
										 	 <div class="col-4">
										 		 <div class="form-group row text">
										 						<label for="staticEmail" class="col-sm-2 col-form-label">
										 								<img src="images/arrow.png" width="20" height="20" border="0" style="margin-left: 0px;
										 		 margin-right: auto; margin-top:-5px;" />
										 						</label>
										 						<div class="col-sm-1">
										 							<div style="background:#fff3cd; height:20px; width:50px; margin-left: -10px;"></div>
										 						</div>
										 						<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level *1* </strong> &nbsp;&nbsp;Grower
										 					</div>
										 	 </div>
										 	 <div class="col-6">

										 	 </div>
										  </div>
										  <div class="row justify-content-center">
										 	 <div class="col-4">
										 		 <div class="form-group row text">
										 						<label for="staticEmail" class="col-sm-2 col-form-label">
										 								<img src="images/arrow.png" width="20" height="20" border="0" style="margin-left: 0px;
										 		 margin-right: auto; margin-top:-5px;" />
										 						</label>
										 						<div class="col-sm-1">
										 							<div style="background:#c3e6cb; height:20px; width:50px; margin-left: -10px;"></div>
										 						</div>
										 						<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level *2* </strong> &nbsp;&nbsp;Product
										 					</div>
										 	 </div>
										 	 <div class="col-6">

										 	 </div>
										  </div>
										  <div class="row justify-content-end">
										 	 <div class="col-4">
										 		 <div class="form-group row text">
										 						<label for="staticEmail" class="col-sm-2 col-form-label">
										 								<img src="images/arrow.png" width="20" height="20" border="0" style="margin-left: 0px;
										 		 margin-right: auto; margin-top:-5px;" />
										 						</label>
										 						<div class="col-sm-1">
										 							<div style="background:#e5e0df; height:20px; width:50px; margin-left: -10px;"></div>
										 						</div>
										 						<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level *3* </strong> &nbsp;&nbsp;Card
										 					</div>
										 	 </div>
										 	 <div class="col-6">

										 	 </div>
										  </div>


										 </td>
									</tr>


		<tr>
                    <td>

		    </td>
                </tr>


                  <tr>

                    <td><div id="box">

		<div id="container">

			<div class="demo_jui">

<table id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>

			<th width="8%" align="center">Sr.</th>
			<th width="20%" align="left">Subcategory</th>
			<th width="15%" align="left">Feature</th>
			<th width="25%" align="left">Size</th>
			<th width="10%" align="left">Price</th>
			<th width="14%" align="left">Price variation</th>
			<th width="10%" align="left">Start date</th>
			<th width="10%" align="left">End date</th>


			<th width="10%" align="center"></th>
			<th width="10%" align="center"></th>
			<th width="10%" align="center"></th>
			<th width="3%" align="center"></th>
				<th width="3%" align="center"></th>

		</tr>

	</thead>

	<tbody>

		<?php
				$i=1;
				$sr=1;
				$verifyLevel="";

				while($subcategory=mysqli_fetch_array($rs))
				{
				$query_subcP = "SELECT count(*) FROM param_product_price where idsc ='".$subcategory["idsc"]."' and size='".$subcategory["size"]."' and feature='".$subcategory["feature"]."'";
				$verifyProductsLevel2P  = mysqli_query($con, $query_subcP);
			  $ArrayProductsLevel2P   = mysqli_fetch_array($verifyProductsLevel2P);

				if($ArrayProductsLevel2P['0']>0){
					$verifyLevel = '2';
				}else{
					$verifyLevel = '1';
				}

				 $date_ini = $subcategory["date_ini"];
				 $date_end = $subcategory["date_end"];

				if(($date_ini != null and $date_end != null) || ($date_ini == "no-date" and $date_end == "no-date"))
				{
								$date = new DateTime(date("h:i:sa"));
							//	$date2 = new DateTime($date_end);
							//	$diff = $date2->getTimestamp() - $date->getTimestamp();

								//if($diff<=0){
									//TOD CAMBIAR INCREMENTO A CERO;
							//	}
				}else{
							$diff = "";
				}

				$query_subcG = "SELECT * FROM growcard_product_price where sizeid='".$subcategory["size"]."'";
				$verifyProductsLevel2G  = mysqli_query($con, $query_subcG);

				while($ArrayProductsLevel2G  = mysqli_fetch_array($verifyProductsLevel2G))	  {
								$IdP			 = $ArrayProductsLevel2G['prodcutid'];
								$IdFeature = $ArrayProductsLevel2G['feature'];

								$selectCat = "SELECT * FROM product where id='$IdP'";
								$arrayCat  = mysqli_query($con, $selectCat);
								 $catIdA   = mysqli_fetch_array($arrayCat);


									if($catIdA['subcategoryid'] == $subcategory["idsc"])
									{

											if($subcategory["feature"] == $IdFeature)
											{
													$verifyLevel = '3';
											}

									}

								}

				if ($producs["gprice"] > 0) {
						$kp = $producs["gprice"];
				} else {
						$kp = $price["price"];


				}


					if($subcategory["date_ini"] !="" && $subcategory["date_end"]!=""){
						$fondoPV = "#FDECED";
					}else{
						$fondoPV = "";
					}






		 ?>

                       <tr class="gradeU" <?php
											 if($verifyLevel=='2')
											 { echo "style='background:#c3e6cb;'"; }
											 else{
												 if($verifyLevel=='3'){
													 echo "style='background:#ffeeba;'";
												 }
											 }
											 ?>>

                          <td align="center" class="text"><?php echo $sr;?></td>

                          <td width="20%" class="text" align="left"><?php echo $subcategory["subcategory"]; ?></td>

                          <td  width="15%" class="text" align="left"><?php echo $subcategory["namefeature"]; ?></td>

                          <td width="20%" class="text" align="left"><?php echo $subcategory["namesize"]." cm"?></td>

      										<td width="15%"><input type="text"  name="price-<?php echo $subcategory["id"]; ?>" id="price-<?php echo $subcategory["id"]; ?>" onkeypress="validate2(event)" size="5" value="<?php echo $subcategory["price_adm"]; ?>"></td>

													<td align="center" width="10%">
														<input style="background-color:<?php echo $fondoPV; ?>;"  type="text"  name="variation-price-<?php echo $subcategory["id"]; ?>" id="variation-price-<?php echo $subcategory["id"]; ?>" onkeypress="validate2(event)" size="5" value="<?php echo $subcategory["variation_price"]; ?>">
															<br>
															<span  id="clock1" class="text">

														</span>
															<input  size="1" type="hidden"  name="pro-<?php echo $i; ?>" value="<?php echo $subcategory["gid"]; ?>"/>
															<input  size="1" type="hidden" name="grower-<?php echo $subcategory["gid"]; ?>" value="<?php echo $subcategory["growerid"]; ?>">

													</td>

													<td width="7%" class="text" align="left">
													     <input style="background-color:<?php echo $fondoPV; ?>;" size="10" type="date" name="date_ini<?php echo $subcategory["id"]; ?>" id="date_ini<?php echo $subcategory["id"]; ?>" value="<?php echo $subcategory["date_ini"];?>" />
													</td>
													  <td width="7%" class="text" align="left">
																 <input style="background-color:<?php echo $fondoPV; ?>;" size="10"  type="date" name="date_end<?php echo $subcategory["id"]; ?>" id="date_end<?php echo $subcategory["id"]; ?>" value="<?php echo $subcategory["date_end"];?>" />
														</td>
													<td width="4%" align="center">

														<button class="buttonblue"  onclick="updateCard('<?php echo $subcategory["id"]; ?>')" type="button"><img src="images/reload.png" width="15" height="15" border="0" /></button>

													</td>
													<td width="13%" align="center"><a href="price_param_price.php?idsub=<?php echo $subcategory["idsc"]."&idsize=".$subcategory["size"]."&id_gid=".$subcategory["id"]."&iden_level=".$verifyLevel."&featureI=".$subcategory['feature']; ?>" > <img src="images/tag.png" width="15" height="15" border="0" /> Prod </a></td>
													<td width="13%" align="center"><a href="price_param_price_color.php?idsub=<?php echo $subcategory["idsc"]."&idsize=".$subcategory["size"]."&id_gid=".$subcategory["id"]."&iden_level=".$verifyLevel."&featureI=".$subcategory['feature']; ?>" > <img src="images/format_color_fill.png" width="15" height="15" border="0" /> Col </a></td>

													<td width="13%" align="center"><a href="price_param_grower.php?idsub=<?php echo $subcategory["idsc"]."&idsize=".$subcategory["size"]."&id_gid=".$subcategory["id"]."&featureI=".$subcategory['feature']; ?>" > <img src="images/home.png" width="15" height="15" border="0" /> Grow </a></td>

														<td width="3%">
															<?php if($verifyLevel=='2' or $verifyLevel=='3'){ ?>
																<button  onclick="deleteT('<?php echo $subcategory["idsc"]; ?>','<?php echo $subcategory["size"]; ?>')" type="button"><img src="images/cleaning_services.png" width="15" height="15" border="0" /></button>
															<?php } ?>
													</td>

                          <!--td class="text" align="center"><a href="packing_card_generate1.php?ids=<?php echo $subcategory["id"]?> "  >Generate </a> </td-->

                          <!--td class="text" align="center"><a href="packing_card_mgmt.php?ids=<?php echo $subcategory["id"]?> "  >Card </a> </td-->

                           </tr>

			 <?php

				 		$sr++;

				}


			 ?>



	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
