<?php
    // PO #1  2-jul-2018
include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_GET['delete'])) {
    $query = 'DELETE FROM tax_table WHERE  tax_table_id= ' . (int) $_GET['delete'];
    mysqli_query($con, $query);
}


$qsel = "select * from tax_table order by tax_table_id";
$rs = mysqli_query($con, $qsel);
$sel_info = "select * from tax_table order by tax_table_id";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">

            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>
    </head>

    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <?php include("includes/left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Manage Tax Tables</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>
                                                                            <td><a href="tax_add.php" class="pagetitle" onclick="this.blur();"><span> +Add Tax Tables </span></a></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><form action="" method="post" id="form1" >  <div id="box">
                                                                            <div id="container">
                                                                                <div class="demo_jui">
                                                                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th width="8%" align="center">Sr.</th>
                                                                                                <th width="20%" align="left">Name</th>
                                                                                                <th align="center" width="10%">Edit</th>
                                                                                                <th align="center" width="14%">Delete</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <?php
                                                                                            $sr = 1;
                                                                                            $temp = explode(",", $info["tax_table_id"]);
                                                                                            while ($colors = mysqli_fetch_array($rs)) {
                                                                                               
                                                                                                ?>
                                                                                                <tr class="gradeU">
                                                                                                    <td align="center" class="text"><?php echo $sr; ?></td>
                                                                                                    <td class="text" align="left"><?php echo $colors["name"] ?></td>
                                                                                                    <td align="center" ><a href="tax_edit.php?id=<?php echo $colors["tax_table_id"] ?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                                                                                    <td align="center" ><a href="?delete=<?php echo $colors["tax_table_id"] ?>"  onclick="return confirm('Are you sure, you want to delete this ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>
                                                                                                </tr>
                                                                                                <?php
                                                                                                $sr++;
                                                                                            }
                                                                                            ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>

                                                                            <div style="float:right;margin-top:20px;">
                                                                                <input name="Submit" type="Submit" class="buttongrey" value="Update Shipping Method" />
                                                                            </div>

                                                                        </div> 
                                                                        <input type="hidden" name="abc" id="abc" value="abc" />
                                                                        <input type="hidden" name="id" id="id" value="<?php echo $info["id"] ?>"  />
                                                                        <input type="hidden" name="totaldestination" id="totaldestination" value="<?php echo $sr ?>" />
                                                                    </form></td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
