<?php
  
include("../config/config_gcp.php");

$id_order     = $_GET['id_fact'];
$buyer_ofer   = $_GET['id_buy'];
$delNow       = date('Y-m-d');
  

// CABECERA
$query_cab = "select id_fact         , buyer_id         , order_number    , order_date  , shipping_method , 
                     del_date        , date_range       , is_pending      , order_serial, seen            , 
                     delivery_dates  , lfd_grower       , quick_desc      , bill_number , gross_weight    , 
                     volume_weight   , freight_value    , guide_number    , total_boxes , sub_total_amount, 
                     tax_rate        , shipping_charge  , handling        , grand_total , bill_state      , 
                     date_added      , user_added       ,
                     air_waybill     , charges_due_agent, credit_card_fees, per_kg      ,
                     handling_lax    , brokerage_lax    , tot_shiping_imp_cost          , cad
                from invoice_orders  
               where id_fact  = '" . $id_order   . "'  
                 and buyer_id = '" . $buyer_ofer . "'  ";

    $fact_ord    = mysqli_query($con, $query_cab);
    $factura_cab = mysqli_fetch_assoc($fact_ord);
    

// Box Cab Totales
$sqlBoxcabTot = "select sum(Peso) as peso
                from invoice_packing_cab  
               where id_fact = '" . $id_order   . "'  
                 and buyer   = '" . $buyer_ofer . "'  ";

$result_cabTot   = mysqli_query($con, $sqlBoxcabTot); 

$TotalesBox = mysqli_fetch_assoc($result_cabTot);

$pesoTotal = $TotalesBox['peso'];

$shippingImport = $factura_cab['tot_shiping_imp_cost'];


// CABECERA

$sqlDetalis = "select id            , id_fact       , id_order   , order_serial , buyer      , 
                      grower_id     , box_qty_pack  , box_type   , comment      , date_added , 
                      order_number  , Piezas        , Peso       , box_weight   , Dist       , 
                      Cost          , price_per_box , Stems      , price_st_bun , gyp
                 from invoice_packing_cab  
                where id_fact = '" . $id_order   . "'  
                  and buyer   = '" . $buyer_ofer . "'  order by id ";

$resultDet   = mysqli_query($con, $sqlDetalis); 

        
while($rowpeso = mysqli_fetch_array($resultDet))  {
    
    $pesoCal   = $rowpeso['Peso']/$pesoTotal*100;
    $costCal   = $shippingImport*$pesoCal/100;
    $pricexBox = $costCal/$rowpeso['Piezas'];
    $priceStBu = $costCal/$rowpeso['Stems'];
    
    if ($rowpeso['gyp'] != 0) { 
            $priceStBuGyp = $costCal/$rowpeso['gyp'];
    }else{
            $priceStBuGyp = 0;
    }

    
    
    
                        $set_dist3 = "UPDATE invoice_packing_cab 
                                        SET Dist ='" . $pesoCal   . "' ,
                                            Cost ='" . $costCal   . "' ,
                                            price_per_box ='" . $pricexBox   . "' ,
                                            price_st_bun ='" . $priceStBu   . "'  ,
                                            price_st_bunGyp ='" . $priceStBuGyp   . "'     
                                      WHERE id   ='" . $rowpeso['id'] . "' ";

                        mysqli_query($con,$set_dist3);  
                        
                        
                        
                        // Requests 
                        
                        $set_invReq  = "UPDATE invoice_requests 
                                           SET ship_cost ='" . $priceStBu   . "' 
                                         WHERE id_fact   ='" . $rowpeso['id_fact'] . "' 
                                           AND grower_id ='" . $rowpeso['grower_id'] . "' ";

                        mysqli_query($con,$set_invReq);                          
                        
                        /*
                        $set_pack  = "UPDATE invoice_packing_box 
                                           SET ship_cost ='" . $priceStBu   . "' 
                                         WHERE id_fact   ='" . $rowpeso['id_fact'] . "' 
                                           AND grower_id ='" . $rowpeso['grower_id'] . "' ";

                        mysqli_query($con,$set_pack);        */                                          
                        
                        
}
    

           // DETALLE
$sqlReq = "select *
             from invoice_requests  
            where id_fact = '" . $id_order   . "'  
              and buyer   = '" . $buyer_ofer . "'   ";

$resultPrice   = mysqli_query($con, $sqlReq); 

        
while($rowprice = mysqli_fetch_array($resultPrice))  {
    
    $cost_cad  = $rowprice['salesPriceCli']/$factura_cab['cad'];
    $price_cad = $rowprice['ship_cost']+$cost_cad;
    
                            $set_cad  = "UPDATE invoice_requests 
                                            SET cost_cad  ='" . $cost_cad   . "' ,
                                                price_cad ='" . $price_cad   . "'
                                          WHERE offer_id   ='" . $rowprice['offer_id'] . "'    ";

                            mysqli_query($con,$set_cad);                          
                            
} 

/*
$sqlPackCad = "select *
                 from invoice_packing_box  
                where id_fact = '" . $id_order   . "'  
                  and buyer   = '" . $buyer_ofer . "'   ";

$resultPackCad   = mysqli_query($con, $sqlPackCad); 

        
while($rowcad = mysqli_fetch_array($resultPackCad))  {
    
    $cost_cad_box  = $rowcad['price']/$factura_cab['cad'];
    $price_cad_box = $rowcad['ship_cost'] + $cost_cad_box;
    
                            $set_cad_box  = "UPDATE invoice_packing_box 
                                               SET cost_cad ='" . $cost_cad_box   . "' ,
                                                   price_cad ='" . $price_cad_box   . "'   
                                             WHERE id   ='" . $rowcad['id'] . "'    ";

                            mysqli_query($con,$set_cad_box);                          
                            
} 
*/


//header("location:manage_cost_subcli.php?id_fact=$id_order");
       
       header("location:invoice_mgmt.php");
       
?> 