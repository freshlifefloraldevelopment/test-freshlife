<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");

}

if ($_SESSION['grower_id'] != 0) {
    header("location: growers.php?id=" . $_SESSION['grower_id']);
}

if (isset($_POST['change_ppic'])) {
    $today = date('mdyHis');
    if ($_FILES['ppic']['name'] != "") {
        $tmp1 = $_FILES['ppic']['name'];
        $ext1 = explode('.', $tmp1);
        $image = 0;
        $uploaddir = '../includes/assets/profile_pictures/';
        $filename = $today . "-" . $ext1[0] . "." . $ext1[1];
        $uploadfile1 = $uploaddir . $filename;

        move_uploaded_file($_FILES['ppic']['tmp_name'], $uploadfile1);
        $sql = "UPDATE admin SET picture = '" . $filename . "' WHERE id = " . $_SESSION['tomodachi-admin'];
        $res = mysqli_query($con, $sql);
        if ($res) {
            header("Location:home.php");
        }
    }

}


$sql_data = "SELECT uname, report as picture FROM admin WHERE id= " . $_SESSION['tomodachi-admin'] . " AND isadmin = 1";
$data_res = mysqli_query($con, $sql_data);
$row = mysqli_fetch_assoc($data_res);

$pic_path = SITE_URL . 'user/packing1.png';

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Admin Area</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>    
    
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery/jquery.plugin.js"></script>    
    <script type="text/javascript" src="assets/js/app.js"></script>    

</head>


<body>
    <form name="forma" id="forma" action="print_boxsubcate.php" method="post" >
        
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php include("includes/header_inner.php"); ?>
    <tr>
        <td height="5"></td>
    </tr>
    <tr>

        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <?php include("includes/left.php"); ?>

                    <td width="5">&nbsp;</td>

                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>

                                <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80"/></td>

                                <td valign="top" background="images/images_front/middle-topshade.gif"
                                    style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <tr>

                                            <td width="10">&nbsp;</td>

                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                    <tr>
                                                        <td height="5"></td>

                                                    </tr>

                                                    <tr>
                                                        <td class="pagetitle">SUBCATEGORY BOX PACKING</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>
                                                        <td><img src="<?php echo $pic_path; ?>" alt="Profile Picture"
                                                                 width="140" height="140"/></td>

                                                    </tr>
                                                    

                                                    
                        <tr>
                                <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> &nbsp; Subcategory:</td>
                                <td width="66%" bgcolor="#f2f2f2">
                                    
                                        <select name="codsubcate" id="codsubcate" class="form-control select2 cls_filter">
                                              <option value=""> -- Select Subcategory -- </option>
                                          <?php
                        		       $sel_cate="select p.subcategoryid as id , s.name 
                                                            from grower_product_box_packing gpp
                                                            inner join growers g   on gpp.growerid = g.id 
                                                            inner join product p   on gpp.prodcutid = p.id 
                                                            inner join subcategory s  on p.subcategoryid = s.id
                                                            inner join sizes sz on gpp.sizeid = sz.id
                                                            inner join bunch_sizes bs on gpp.bunch_size_id=bs.id
                                                            inner join grower_product_box box on (gpp.growerid = box.grower_id and gpp.prodcutid = box.product_id and gpp.box_id = box.boxes )                 
                                                            inner join grower_product_price pr on (gpp.growerid = pr.growerid and gpp.prodcutid = pr.prodcutid and gpp.sizeid = pr.sizeid ) 
                                                            left join boxes bx on gpp.box_id=bx.id
                                                            left join boxtype bt on bx.type=bt.id                                                                             
                                                            left join features ff  ON gpp.feature = ff.id
                                                            where g.active = 'active'
                                                            group by s.name          
                                                            order by s.name";
                                                 
        					 $rs_cate=mysqli_query($con,$sel_cate);
						 while($idcate=mysqli_fetch_array($rs_cate)) {
                                           ?>
                                                <option value="<?php echo $idcate["id"]?>">
                                                    <?php echo $idcate["name"]?>
                                               </option>
                                          <?php } ?>
                                        </select>
                        </tr>                                                      
                                                    
                        <!--tr>                        
                                <td align="left" class="text" valign="top">Fecha Inicial : </td>
                        </tr>                    
                        
                        <tr>                                                  
                                <td bgcolor="#f2f2f2" class="text"><input type="date" name="fecha_ini" id="fecha_ini" />
                        </tr>                    
                        
                        <tr>                        
                                <td align="left" class="text" valign="top">Fecha Final : </td>
                        </tr>                    
                        
                        <tr>                                                  
                                <td bgcolor="#f2f2f2" class="text"><input type="date" name="fecha_fin" id="fecha_fin" />
                        </tr-->                                            

                                <input type="hidden" name="order" id="order" value="">
                        
                        
                            <tr> 
                              <button type="submit" class="btn btn-3d btn-purple" style="background-color:#06B120!important;">Print <i class="fa fa-chevron-right"></i></button>
                            </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                </table>
                                            </td>

                                            <td width="10">&nbsp;</td>

                                        </tr>
                                    </table>
                                </td>

                                <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img
                                            src="images/images_front/middle-topright.gif" width="10" height="80"/></td>

                            </tr>

                            <tr>

                                <td background="images/images_front/middle-leftline.gif"></td>

                                <td>&nbsp;</td>

                                <td background="images/images_front/middle-rightline.gif"></td>

                            </tr>

                            <tr>

                                <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10"/></td>

                                <td background="images/images_front/middle-bottomline.gif"></td>

                                <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10"/></td>

                            </tr>

                        </table>
                    </td>

                </tr>

            </table>
        </td>

    </tr>

    <tr>

        <td height="10"></td>

    </tr>

    <?php include("includes/footer-inner.php"); ?>

    <tr>

        <td>&nbsp;</td>

    </tr>

</table>

</body>
</form>

</html>
<script>
    
    function  verify() {
        alert ("hola");
           var order_val ="";
        //var order_val = $('#filter_order :selected').val();
        //$("#order").val($("#codvend option:selected").html());

         order_val = $('#codvend :selected').val();

        alert(" xxxxxx");
        
        if (order_val==""){

            alert ("Please choose an option to proceed");
            return false;
        }
        else{
            return true;
        }

    }    
    

</script>

