<?php

require_once("../config/config_gcp.php");
session_start();


$requestId  = $_GET['id'];

$growerid  = $_GET['id_grow'];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  

function  box_type($box_type){
    global  $con;
    $sel_box_type = "SELECT id , code as name , descrip FROM units WHERE id='" .$box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type;

}

function get_bunchs($product_id, $product_size, $growid){
    global $con;
    $getbunches = mysqli_query($con, "SELECT gs.sizes,bs.name AS bunchname , g.growers_name 
                                        FROM growcard_prod_bunch_sizes AS gs 
                                        LEFT JOIN bunch_sizes bs ON gs.bunch_sizes=bs.id 
                                        LEFT JOIN growers g ON gs.grower_id=g.id 
                                       WHERE gs.grower_id  = '" . $growid . "' 
                                         AND gs.product_id = '" . $product_id . "'  
                                       ORDER BY gs.id limit 0,1  ");
    
    $rowbunches = mysqli_fetch_assoc($getbunches);
    return $rowbunches;
}

			

        $sel_info = "SELECT gpb.id AS cartid     ,   gpb.id_order      ,   gpb.order_serial    ,  gpb.cod_order     ,
                            gpb.product          ,   gpb.sizeid        ,   gpb.feature         ,  gpb.noofstems     ,
                            gpb.qty              ,   gpb.buyer         ,   gpb.boxtype         ,  gpb.type          ,
                            gpb.bunches          ,   gpb.box_name      ,   gpb.lfd             ,  gpb.comment       , 
                            gpb.box_id           ,   gpb.shpping_method,   gpb.isy             ,  gpb.bunch_size    ,
                            gpb.unseen           ,   gpb.req_qty       ,   gpb.inventary       ,  gpb.id_client     , 
                            p.name as prod_name  ,   p.color_id        ,   c.name as colorname ,  gpb.request_color ,
                            s.name AS subs,
                            sh.name AS sizename,
                            ff.name AS featurename,
                            cl.name as namecli
                       FROM buyer_requests gpb
                      INNER JOIN product p           ON gpb.product     = p.id
                       LEFT JOIN colors c            ON p.color_id      = c.id
                       LEFT JOIN subcategory s       ON p.subcategoryid = s.id  
                       LEFT JOIN features ff         ON gpb.feature     = ff.id
                       LEFT JOIN sizes sh            ON gpb.sizeid      = sh.id 
                       LEFT JOIN sub_client cl       ON gpb.id_client = cl.id                
                      WHERE gpb.id='" . $requestId . "' ";
        
            $rs_info = mysqli_query($con, $sel_info);
            $products = mysqli_fetch_array($rs_info);

            
            $cab_qty = $products["qty"];
            
            
         
	if(isset($_GET['delete'])) 	{
	  $queryDel = 'DELETE FROM grower_offer_reply WHERE id = '.(int)$_GET['delete'];
	  mysqli_query($con,$queryDel);
          
          header('location:grower_offer_mgmt.php?id_offer='.$id_offer);          
	}
        
        $box_type=box_type($products['boxtype']); 
        $rowbunches = get_bunchs($products['product'], $products['sizeid'], $growerid );        
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="1200" border="0" cellspacing="0" cellpadding="0">

      <tr>


        <td width="5">&nbsp;</td>
        <td valign="top"><table width="1200" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="1200" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="1200" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr>
                      <td>
                          <strong> <?php echo $products["id_order"]." - ".$rowbunches['growers_name']." - ".$cab_qty. " " . $box_type["name"];?> 
                                   <?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] ?> cm <?php echo $rowbunches['bunchname'] . " st/buc ( Tot.Offers..:  ".$cant_required."  FULLFILLED..: " . round($cumpli,2) . " %)" ; ?>        
                          </strong>
                      </td>
                  </tr>                                                

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Offers</td>
                  </tr>
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                      
                      
                <td><div id="box">

		<div id="container" class="container">			
                    <div class="container-fluid">

<table width="1200" cellpadding="0" cellspacing="0" border="1" class="display" id="main_table_0" bordercolor="#e4e4e4">

	<thead>

		<tr>       

                    <th align="left" width="15%">Product</th>  
                    <th align="left" width="15%">Bunch</th>                    
                    <th align="left" width="15%">Price</th>                    
                    <th align="left" width="15%">Request</th>                                        
                    <th align="left" width="15%">Order</th>                                                            

		</tr>

</thead>
    <form name="sendoffer" id="sendoffer<?= $products["cartid"] ?>" method="post">

        <input type="hidden" name="offer" id="offer" value="<?= $products["cartid"] ?>">
        <input type="hidden" name="buyer" id="buyer" value="<?= $products["buyer"] ?>">
        <input type="hidden" name="id_client" id="id_client" value="<?= $products["id_client"] ?>">                                                                                                

        <input type="hidden" name="product" id="product" value="<?= $products["name"] ?> <?= $products["featurename"] ?>">                                                                                                                                                                                                
        <input type="hidden" name="product_subcategory" id="product_subcategory" value="<?= $products["subs"] ?>">
        <input type="hidden" name="product_ship" id="product_ship" value="1">
        <input type="hidden" name="shipp" id="shipp" value="<?= $products["shpping_method"] ?>">
        <input type="hidden" name="product_su" id="product_su" value="1">
        <input type="hidden" name="volume" id="volume" value="1">
        <input type="hidden" name="code_order" id="code_order" value="<?= substr($products["cod_order"], 0, 4) ?>">
        <input type="hidden" name="size_name" id="size_name" value="<?= $products["sizename"] ?>"> 
        <input type="hidden" name="stems" id="stems" value="<?= $rowbunches['bunchname'] ?>">   
        <input type="hidden" name="boxcant" id="boxcant" value="1"> 
        <input type="hidden" name="boxtype" id="boxtype" value="<?= $box_type["name"] ?>">                                                                                                 
        <input type="hidden" name="boxtypen" id="boxtypen" value="HB">
        
	<tbody>

		<?php
                    $sr=1;
                    //while($product=mysqli_fetch_array($rs))  {
		?>
                          <tr class="gradeU">   
                              
                                <!-- FIELD 1 Product-->   
                                
                            <td class="text" align="left" width="30%">
                                <?php
                                        $sql_box_ids3 = "SELECT p.name as variety ,gr.id AS id , gr.box_id , b.type , b.name , gs.sizes , gs.bunch_value , gs.is_bunch,
                                                                gs.is_bunch_value,s.name AS size_name,
                                                                fe.name as featurename , bt.name as caja,gr.bunch_size_id ,bs.name as bunchesv , sc.name as subcatego,
                                                                c.name as colorname
                                                           FROM growcard_prod_bunch_sizes gs
                                                           LEFT JOIN product AS p   ON gs.product_id=p.id
                                                           LEFT JOIN colors AS c   ON p.color_id=c.id
                                                           LEFT JOIN subcategory AS sc ON (p.subcategoryid = sc.id and p.categoryid = sc.cat_id)
                                                           LEFT JOIN growcard_prod_box_packing  gr ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
                                                           LEFT JOIN sizes AS s     ON gs.sizes=s.id
                                                           LEFT JOIN boxes AS b     ON b.id=gr.box_id
                                                           LEFT JOIN boxtype AS bt  ON b.type=bt.id
                                                           LEFT JOIN features AS fe ON fe.id=gr.feature
                                                           LEFT JOIN bunch_sizes AS bs  ON gr.bunch_size_id=bs.id
                                                          WHERE  gs.grower_id='" . $growerid . "' 
                                                            AND bt.name in ('HB','QB','EB')
                                                            AND sc.name = '" . $products["subs"] . "'
                                                          group by p.name,gs.sizes,fe.id                                                                
                                                          ORDER BY p.name, s.name  ";                                                                                                                                                                                                                        

                                        $sel_box_ids3 = mysqli_query($con, $sql_box_ids3);

				?>                                    
                                    
                                    <select class="form-controll offer-select" name="productsize[]"  
                                            id="productsize-<?= $products["cartid"] ?>-0">
                                        <option value="">Select Product</option>
                                <?php
                                            while ($box_ids3 = mysqli_fetch_array($sel_box_ids3)) {
                                                $bunch_value_s = "";

                                                if ($box_ids3['is_bunch'] == "0") {
                                                    $bunch_value_s = $box_ids3['bunchesv'];
                                                } else {
                                                    $bunch_value_s = $box_ids3['bunchesv'];
                                                }
				?>
                                                <option value="<?= $box_ids3["id"] ?>" size_id="<?php echo $box_ids3["sizes"]; ?>" prod_name="<?php echo $box_ids3["variety"]; ?>" s_name="<?php echo $box_ids3["size_name"]; ?>" b_type="<?php echo $box_ids3["caja"]; ?>" cod_id="<?php echo $box_ids3["id"]; ?>"> 
                                                         <?= $box_ids3["variety"]." ".$box_ids3["colorname"] ?> <?= $box_ids3["subcatego"] ?> <?= $box_ids3["featurename"] ?> <?= $box_ids3["size_name"] ?>cm <?php echo $bunch_value_s . "st"; ?></option>
                                <?php	 }	?> 

                                      </select>
                            </td>
                                
                                
                              
                                <!-- FIELD 2 Qty-->                            
                                <td class="text" align="left" width="30%">
                                    
                                        <select class="form-controll offer-select" name="bunchqty[]"
                                                id="bunchqty-<?= $products["cartid"] ?>-0">
                                            <option value="">Select</option>
                                            <?php
                                            for ($ia = 1; $ia <= 500; $ia++) {
                                                ?>
                                                <option value="<?= $ia ?>"><?= $ia ?></option>
                                            <?php } ?>
                                        </select>
                                    
                                </td>
                                
                                <!-- FIELD 3 Price -->                            
                                <td class="text" align="left" width="30%">
                                    
                                    <select class="form-controll offer-select" name="price[]" 
                                              id="price-<?= $products["cartid"] ?>-0">
                                        <option value="">Select</option>
                                        <?php
                                        for ($i = 1; $i <= 999; $i++) { ?>

                                                    <?php
                                                    $ipre = $i;

                                                    if ($i < 10) {?>
                                                            <option value="0.0<?php echo $ipre; ?>" price_u="0.0<?php echo $ipre; ?>" >0.0<?php echo $ipre; ?></option>  
                                                    <?php   } 
                                                    ?>  

                                                    <?php
                                                    if ($i>= 10 && $i < 100) {?>
                                                            <option value="0.<?php echo $ipre; ?>">0.<?php echo $ipre; ?></option>  
                                                    <?php   } 
                                                    ?>

                                                    <?php
                                                    if ($i>= 100 && $i<200) {?>
                                                            <?php  if (($ipre-100)>= 0 && ($ipre-100)<=9) {?>
                                                                    <option value="1.0<?php echo $ipre-100; ?>">1.0<?php echo $ipre-100; ?></option>
                                                            <?php   }else{ ?>
                                                                    <option value="1.<?php echo $ipre-100; ?>">1.<?php echo $ipre-100; ?></option>
                                                            <?php   } ?>

                                                    <?php   } 
                                                    ?>
                                                    <?php                                                                                                                                
                                                    if ($i>= 200 && $i<300 ) {?>
                                                            <?php  if (($ipre-200)>= 0 && ($ipre-200)<=9) {?>
                                                                    <option value="2.0<?php echo $ipre-200; ?>">2.0<?php echo $ipre-200; ?></option>
                                                            <?php   }else{ ?>
                                                                    <option value="2.<?php echo $ipre-200; ?>">2.<?php echo $ipre-200; ?></option>
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>   

                                                    <?php                                                                                                                                
                                                    if ($i>= 300 && $i<400) {?>
                                                            <?php  if (($ipre-300)>= 0 && ($ipre-300)<=9) {?>
                                                                    <option value="3.0<?php echo $ipre-300; ?>">3.0<?php echo $ipre-300; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="3.<?php echo $ipre-300; ?>">3.<?php echo $ipre-300; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>  

                                                    <?php                                                                                                                                
                                                    if ($i>= 400 && $i<500) {?>
                                                            <?php  if (($ipre-400)>= 0 && ($ipre-400)<=9) {?>
                                                                    <option value="4.0<?php echo $ipre-400; ?>">4.0<?php echo $ipre-400; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="4.<?php echo $ipre-400; ?>">4.<?php echo $ipre-400; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>                

                                                    <?php                                                                                                                                
                                                    if ($i>= 500 && $i<600) {?>
                                                            <?php  if (($ipre-500)>= 0 && ($ipre-500)<=9) {?>
                                                                    <option value="5.0<?php echo $ipre-500; ?>">5.0<?php echo $ipre-500; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="5.<?php echo $ipre-500; ?>">5.<?php echo $ipre-500; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>                

                                                    <?php                                                                                                                                
                                                    if ($i>= 600 && $i<700) {?>
                                                            <?php  if (($ipre-600)>= 0 && ($ipre-600)<=9) {?>
                                                                    <option value="6.0<?php echo $ipre-600; ?>">6.0<?php echo $ipre-600; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="6.<?php echo $ipre-600; ?>">6.<?php echo $ipre-600; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>

                                                    <?php                                                                                                                                
                                                    if ($i>= 700 && $i<800) {?>
                                                            <?php  if (($ipre-700)>= 0 && ($ipre-700)<=9) {?>
                                                                    <option value="7.0<?php echo $ipre-700; ?>">7.0<?php echo $ipre-700; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="7.<?php echo $ipre-700; ?>">7.<?php echo $ipre-700; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>                

                                                    <?php                                                                                                                                
                                                    if ($i>= 800 && $i<900) {?>
                                                            <?php  if (($ipre-800)>= 0 && ($ipre-800)<=9) {?>
                                                                    <option value="8.0<?php echo $ipre-800; ?>">8.0<?php echo $ipre-800; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="8.<?php echo $ipre-800; ?>">8.<?php echo $ipre-800; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>               

                                                    <?php                                                                                                                                
                                                    if ($i>= 900 && $i<1000) {?>
                                                            <?php  if (($ipre-900)>= 0 && ($ipre-900)<=9) {?>
                                                                    <option value="9.0<?php echo $ipre-900; ?>">9.0<?php echo $ipre-900; ?></option>  
                                                            <?php   }else{ ?>
                                                                    <option value="9.<?php echo $ipre-900; ?>">9.<?php echo $ipre-900; ?></option>  
                                                            <?php   } ?>        

                                                    <?php   }                                                                                                                                 
                                                    ?>


                                        <?php }
                                        ?>
                                    </select>
                                    
                                </td>
                              
                                <!-- FIELD 4-->                            
                                <td class="text" align="left" width="30%">
                                <?php
                                        $sql_request3 = "select ir.id as rid , 
                                                                  rg.name as prod_name , 
                                                                  sz.name as sizename , 
                                                                  ir.qty,
                                                                  ir.type , 
                                                                  if(ir.type=1,'OM','SO') as type_request,
                                                                  IF(ir.comment='MARKET PLACE', sum(gr.bunchqty*gr.steams), sum(gr.bunchqty)) as totstems,
                                                                  IFNULL((round((IF(ir.comment='MARKET PLACE', sum(gr.bunchqty*gr.steams), sum(gr.bunchqty))/ir.qty*100),2)),0) as porce,
                                                                  ir.id_client,
                                                                  s.name as product_subcategory,
                                                                  ir.qty - IF(ir.comment='MARKET PLACE', sum(gr.bunchqty*gr.steams), sum(gr.bunchqty)) as difer,
                                                                  substr(c.name,1,17) as cli_name
                                                             from buyer_requests ir
                                                             INNER JOIN product rg ON ir.product = rg.id
                                                             INNER JOIN sizes sz ON ir.sizeid = sz.id
                                                             LEFT JOIN subcategory s    ON rg.subcategoryid = s.id  
                                                              LEFT JOIN grower_offer_reply gr on ir.id = gr.request_id
                                                              LEFT JOIN sub_client c on ir.id_client = c.id
                                                             where ir.id_order = '" . $products["id_order"] . "'
                                                               and s.name = '" . $products["subs"] . "'       
                                                             group by ir.id    , rg.name  ,  sz.name  ,  ir.qty   , ir.type  , ir.id_client,   c.name 
                                                             order by rg.name,sz.name";
                                                                                                                                                                                                                                                                                                                                                
                                            $sel_request3 = mysqli_query($con, $sql_request3);
                                  
				?>                                    
                                            <select class="form-controll offer-select"    name="requestb[]"
                                                    id="requestb-<?= $products["cartid"] ?>-0">
                                                <option value="">Select Request</option>

                                <?php
                                                 while ($req_ids3 = mysqli_fetch_array($sel_request3)) {
                                                     
                                                   if ($req_ids3["cli_name"] != "NOT ASSIGNED") {
                                                       $nombre = substr($req_ids3["cli_name"],0,12);
                                                   }else{
                                                       $nombre = "";
                                                   }
				?>
                                                     <option value="<?= $req_ids3["rid"] ?>" b_clie="<?php echo $req_ids3["id_client"]; ?>"> 
                                                     <?= $req_ids3["prod_name"]." ".$req_ids3["product_subcategory"]." ".$req_ids3["sizename"]." cm REQ:".$req_ids3["qty"]." Offer:".$req_ids3["totstems"]."=".$req_ids3["difer"]." -> ".$req_ids3["porce"]."% ".$nombre ; ?></option>
                                <?php	 }	?> 

                                      </select>
                                </td>
                              
                                <!-- FIELD 5-->                            
                                <td class="text" align="left" width="30%">
                                        <select class="form-controll offer-select"  name="tmarquet[]"
                                                id="tmarquet-<?= $products["cartid"] ?>-0">                                                                                                                                                                                                                                            
                                                <option value="0">S.Order</option>                                                                                                                        
                                                <option value="1">O.Market</option> 
                                                <option value="1">Stock</option>                                                 
                                        </select>
                                </td>
                                
                                <td class="text" align="left" width="30%">
                                        <a class="add_new_row" id="add_variety_0_<?php echo $products["cartid"]; ?>-0"
                                           onclick="addNewVariety(0,<?php echo $products["cartid"]; ?>, 0 ,<?php echo $products['boxtype']; ?>,<?php echo $growerid; ?>,<?php echo $products["product"]; ?>,<?php echo $products["sizeid"]; ?>)"
                                           href="javascript:void(0);"><span class="label label-success">Add</span> </a>&nbsp;
                                </td> 
                                
                                <!--td class="text" align="left" width="30%">
                                        <a href='javascript:void(0)' id='edit_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $s; ?>'
                                           onclick='editVariety(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>)' class="btn btn-default btn-xs"><i
                                           class="fa fa-edit white"></i> Edit </a>                                    
                                </td-->
                                
                                <td class="text" align="left" width="30%">
                                        <a href='javascript:void(0)' id='delete_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $s; ?>'
                                           onclick='deleteVariety(0,<?php echo $products["cartid"]; ?>,0)' class="btn btn-default btn-xs"><i class="fa fa-times white"></i> Delete </a>                                    
                                </td>                                

                          </tr>            
            
                        
		<?php  $ss=10;  ?> 

	</tbody>
        <!-- Modal Footer -->
                <div class="modal-footer noborder offer_model_popup">
                    <button type="button" class="btn btn-primary" onclick="window.location.href='https://app.freshlifefloral.com/user/growers_orders_mgmt.php';" >Growers</button>  
                    <button type="button" class="btn btn-primary" onclick="regresar(<?= $growerid ?>)" >Back</button>
                    <button type="button" class="btn btn-primary" onclick="modifyOffer(<?= $products["cartid"] ?>,<?= $ss ?> ,<?= $growerid ?> )" >Send Offer</button>
                    
                    <!--input type="text" class="form-control" id="comment1" name="comment1" value=""  >
                    <button style="background:#8dcc81;" onclick="comentario('<?php echo $requestId; ?>','<?php  echo $_POST['comment2']; ?>')" class="btn btn-primary" type="button">Comment</button-->                        
                    Qyt Box :
                    <input type="text" class="form-control" id="growBox" name="growBox" size="4" value=""  >
                        
				<select id="box_id_grow" name="box_id_grow" class="listmenu">
                                        <option value="">Select Box</option>
                                <?php
                                        $sel_box="select id,name,factor 
                                                     from boxtype
                                                    where factor > 0  ";
                                        $rs_box=mysqli_query($con,$sel_box);
                                    while($factBox=mysqli_fetch_array($rs_box))  {
                                ?>
                                        <option value="<?php echo $factBox["id"]?>"><?php echo $factBox["name"]."  (".$factBox["factor"].")" ?></option>
                                <?php
                                    }
                                   ?> 
                                </select>                        
                        
                    <button style="background:#8dcc81;" onclick="confirmation('<?php echo $requestId; ?>','<?php  echo $growerid; ?>')" class="btn btn-primary" type="button">Box Confirm</button>                                            
                </div>
</form>        
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

<script>
    function addNewVariety(id, cat_id, main_tr, boxtype, userSessionID, product, sizeid) {
        var total_qty = "<?php echo $total_qty_bunches;?>";
        var total_selected_val = $("#total_availabel_product_" + main_tr).val();
        
        if (parseInt(total_selected_val) > parseInt(total_qty)) {
            alert("Your bunch quantity greater than requested bunch qty.");
        }else{
            var productsize = $("#productsize-" + cat_id + "-" + id).val();
            var bunchqty = $("#bunchqty-" + cat_id + "-" + id).val();            
            var price = $("#price-" + cat_id + "-" + id).val();
            var requestb = $("#requestb-" + cat_id + "-" + id).val();
            var tmarquet = $("#tmarquet-" + cat_id + "-" + id).val();
            
            if (productsize == "") {
                $("#productsize-" + cat_id + "-" + id).css("border", "2px solid red");
                $('#errormsg-' + cat_id).html("PLEASE SELECT PRODUCT");
            }
            else if (bunchqty == "") {
                $("#bunchqty-" + cat_id + "-" + id).css("border", "2px solid red");
            }
            else if (price == "") {
                $("#price-" + cat_id + "-" + id).css("border", "2px solid red");
            }
            else if (requestb == "") {
                $("#requestb-" + cat_id + "-" + id).css("border", "2px solid red");                            
            }
            else if (tmarquet == "") {
                $("#tmarquet-" + cat_id + "-" + id).css("border", "2px solid red");                            
            }
            else{
                $("#productsize-" + cat_id + "-" + id).attr("disabled", "disable");
                $("#bunchqty-" + cat_id + "-" + id).attr("disabled", "disable");
                $("#price-" + cat_id + "-" + id).attr("disabled", "disable");
                $("#requestb-" + cat_id + "-" + id).attr("disabled", "disable");                
                $("#tmarquet-" + cat_id + "-" + id).attr("disabled", "disable");                

                $("#productsize-" + cat_id + "-" + id).addClass("disabled-css");
                $("#bunchqty-" + cat_id + "-" + id).addClass("disabled-css");
                $("#price-" + cat_id + "-" + id).addClass("disabled-css");
                $("#requestb-" + cat_id + "-" + id).addClass("disabled-css");                
                $("#tmarquet-" + cat_id + "-" + id).addClass("disabled-css");                
                
                // Registros table
                var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
                
                var conta_fil = parseInt($("#total_a").val()) + 1;

                $("#total_a").val(conta_fil);                                

                //console.log($("#total_a").val());
                console.log("total_tr :");
                console.log(total_tr);


                var box_qty_cnt = $("#main_table_" + main_tr + " tbody tr").length;
                box_qty_cnt = parseInt(box_qty_cnt) - 1;
               // $("#request_qty_box_" + main_tr).attr("onchange", "boxQuantityChange(" + total_tr + "," + cat_id + "," + main_tr + ")");

                var productsize_html = $("#productsize-" + cat_id + "-0").html();
                var first_td = "<td><select class='form-controll offer-select'  name='productsize[]' id='productsize-" + cat_id + "-" + total_tr + "'>" + productsize_html + "</select></td>";
                
                var bunchqty_html = $("#bunchqty-" + cat_id + "-0").html();
                var second_td = "<td><select class='form-controll offer-select' name='bunchqty[]' id='bunchqty-" + cat_id + "-" + total_tr + "'>" + bunchqty_html + "</select></td>";
                
                var price_html = $("#price-" + cat_id + "-0").html();                
                var fourth_td = "<td><select class='form-controll offer-select' name='price[]' id='price-" + cat_id + "-" + total_tr + "'>" + price_html + "</select></td>";

                var requestb_html = $("#requestb-" + cat_id + "-0").html();
                var sixth_td = "<td><select class='form-controll offer-select'  name='requestb[]' id='requestb-" + cat_id + "-" + total_tr + "'>" + requestb_html + "</select></td>";

                var tmarquet_html = $("#tmarquet-" + cat_id + "-0").html();
                var seventh_td = "<td><select class='form-controll offer-select' name='tmarquet[]' id='tmarquet-" + cat_id + "-" + total_tr + "'>" + tmarquet_html + "</select></td>";
                
                var fifth_td = "<td><a href='javascript:void(0)' id='add_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='addNewVariety(" + total_tr + "," + cat_id + "," + main_tr + "," + boxtype + "," + userSessionID + "," + product + "," + sizeid + ")' class='add_new_row'><span class='label label-success'>Add Variety </span></a>&nbsp;</td> <td>	<a class='btn btn-default btn-xs' href='javascript:void(0)' id='delete_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='deleteVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-times white'></i> Delete </a></td>";
                $("#main_table_" + main_tr).append("<tr id='product_tr_" + total_tr + "'>" + first_td + second_td + fourth_td + sixth_td + seventh_td + fifth_td + "</tr>");
            }
        }
    }  
    
    function modifyOffer(id,main_tr,growerId) {
    
        var check = 0;
                        
      // Inicio Validacion Campos
                      
        if ($('#productsize-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please select Product");
            check = 1;
        }
        
        if ($('#price-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please enter your Price");
            check = 1;
        }

        if ($('#requestb-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please enter Requests");
            check = 1;
        }
        
        if ($('#tmarquet-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please enter Type Market");
            check = 1;
        }        
        
    // Fin Validacion Campos
    
        var nombres=[];
        var medidas=[];   
        var cajas=[];
        
        var clientes=[];        
        
        var precio=[];              
        var ramos=[];
        var pedido=[];
        var mercado=[];         
        
        //var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
        var total_tr = $("#main_table_0" + " tbody tr").length;
        
        for (var i=0; i<=main_tr; i++) {   

            var prod_name = $("#productsize-" + id + "-" + i + " option:selected").attr("prod_name");  
            var s_name    = $("#productsize-" + id + "-" + i + " option:selected").attr("s_name");  
            var b_type    = $("#productsize-" + id + "-" + i + " option:selected").attr("b_type");
            
            var b_client    = $("#requestb-" + id + "-" + i + " option:selected").attr("b_clie");            
                        
            var s_precio  = $("#price-" + id + "-" + i + " option:selected").val();                          
            var s_ramos   = $("#bunchqty-" + id + "-" + i + " option:selected").val();                          
            var s_numped  = $("#requestb-" + id + "-" + i + " option:selected").val();              
            var s_merca   = $("#tmarquet-" + id + "-" + i + " option:selected").val();              
            
            console.log("Prueba " + prod_name); 
            
            nombres[i]=prod_name;
            medidas[i]=s_name;            
            cajas[i]=b_type;  
            
            clientes[i]=b_client;              
            
            precio[i]=s_precio;                          
            ramos[i]=s_ramos;                          
            pedido[i]=s_numped;              
            mercado[i]=s_merca;                          
            
            console.log("Variedad " + nombres + main_tr + total_tr); 
        }
        
       if (check == 0) {
            $('#addanomo').val("1")
            $('#errormsg-' + id).html("");
                     
            var k = confirm('Are you sure you want send offer 1');
            if (k == true) {
               // $(".disabled-css").removeAttr("disabled");
                $.ajax({
                    type: 'post',
                    url: 'https://app.freshlifefloral.com/user/growers-send-offers-ajax-ver2.php',
                    data: [$('form#sendoffer' + id).serialize()]+ '&prod_name=' + nombres + '&s_name=' + medidas + '&b_type=' + cajas + '&idgrow=' + growerId +
                             '&b_precio=' + precio +
                             '&b_ramos='  + ramos  +
                             '&b_pedido=' + pedido +
                             '&b_mercado=' + mercado +
                             '&b_totaltr=' + total_tr +  
                             '&b_idcli=' + clientes,                    
                            success: function (data) {
                        var fichas = data.split('######');
				
                        //$('.count').each(function () {
                        //    $(this).prop('Counter', 0).animate(
                        //        {Counter: $(this).text()},
                        //        {
                        //            duration: 7000, easing: 'swing', step: function (now) {
                        //            $(this).text(Math.ceil(now));
                        //        }
                         //       });
                        //});
                        
                        console.log("este  es el  mensaje de  cuanto  esta  enviando:" + fichas[0]);
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
           }
        }
    
    }
    
    function deleteVariety(id, cartid, main_tr) {
        
        var box_qty_cnt = $("#main_table_" + main_tr + " tbody tr").length;
        
        if (box_qty_cnt > 1) {


            $("#product_tr_" + id).remove();
            
            var total_selected_val = 0;
            
            $(".boxqty" + main_tr).each(function () {
                total_selected_val = parseInt(total_selected_val) + parseInt($(this).val());
            });

         }    
     }
     
     function regresar(idgrower) {
         window.location.href="https://app.freshlifefloral.com/user/growers_received_offer_adm.php?id="+idgrower;
      } 
      
    /*Button send Comment */
    function comentario(requesId,commId) {   
        
        var regcomm = $('#comment1').val();
        
        //var regcomm = $('#comment1').find("input").val(); 
                       
        var flag_s = true;                      

        var numped         = requesId;                                              
        //var regcomm        = commId;                                              
                                                     
    if (regcomm != "") {           
        
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>user/grower_comment.php',
            data:  'numped=' + numped +
                   '&typecom=' + regcomm,       
            success: function (data_s) {
                
                if (data_s == 'true') {
                       alert('OK');
                } 
                  //       location.reload();
            }
        });
    }
    }        
    
    /*Button send Confirmation */
    function confirmation(requesId,growId) {   
    
        var numped = requesId; 
        
        var numBox = $('#growBox').val();                                     

        var idBox = $('#box_id_grow option:selected').val(); 
        
        var dataBox = "numped="+ numped + "&numBox="+ numBox + "&growId=" + growId + "&idBox="+ idBox ;
                                                                     
    if (numBox != "") {           
        
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>user/grower_box_confirmation.php',
            data:  dataBox ,       
            success: function (data_s) {
                
              //  if (data_s == 'true') {
                       alert('OK Coordination');
                       document.getElementById("growBox").value = ""
               // } 
            }
        });
    }
    }            
</script>