<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $idbuy = $_GET['id_buy'];    
    
    $id_order = $_GET['b'];    


    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id = b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy   = mysqli_fetch_array($buyer);
      
    $sqlbo = "select id , qucik_desc
                      from buyer_orders 
                     where id = '" . $id_order . "'         " ;
     
    $buyerbo = mysqli_query($con, $sqlbo);
    $buybo   = mysqli_fetch_array($buyerbo);    
   
   // Datos de la Orden  DOH
   
   $sqlDetalis="select unseen,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       p.name , 
                       p.box_type ,                        
                       br.qty ,  
                       br.lfd ,
                       br.boxtype,
                       sizeid ,
                       sz.name as size_name,
                       g.growers_name      ,
                       c.name as colorname ,
                       sc.name as subcate  ,
                       br.type
                  from buyer_requests br
                 INNER JOIN product p     ON br.product = p.id
                 INNER JOIN subcategory sc ON p.subcategoryid = sc.id
                 INNER JOIN buyers b     ON br.buyer = b.id
                 INNER JOIN buyer_orders bo ON br.id_order = bo.id
                 INNER JOIN sizes sz ON br.sizeid = sz.id  
                  LEFT JOIN colors c ON p.color_id = c.id
                  left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer    = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
                 order by sc.name,p.name ";

        $result   = mysqli_query($con, $sqlDetalis);    
        
    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  REQUEST VS OFFERS ',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,'-'.$buy['company'],0,1,'L');  
    $pdf->Cell(20,6,$buybo['id'],0,0,'L');  
    $pdf->Cell(40,6,$buybo['qucik_desc'],0,1,'L');  
    
    $pdf->Ln(10);
    $cabCount = 1;  
    
// Cabecera
    
    $pdf->Cell(40,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'R');
    $pdf->Cell(5,6,' ',0,0,'L');        
    $pdf->Cell(25,6,'Order',0,0,'L');
    $pdf->Cell(5,6,' ',0,0,'L');            
    $pdf->Cell(25,6,'Lfd',0,0,'L');    
    $pdf->Cell(25,6,'Size',0,0,'L'); 
    $pdf->Cell(25,6,'To Complete',0,1,'L');     
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    $totrequest = 0;
    $totoffer   = 0;
    $tmp_cate= "";
    while($row = mysqli_fetch_assoc($result))  {
        
                                ////////////////////////////////////////////////
                                // Verificacion Stems/Bunch
                                $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $row["product"] . "' ";
                                $rs_bu_st = mysqli_query($con,$sel_bu_st);       
                                $bunch_stem = mysqli_fetch_array($rs_bu_st); 
                                
                                $sel_box_type = "select substr(code,1,2) as name from units where id='" . $row["boxtype"] . "'";
                                $rs_box_type  = mysqli_query($con, $sel_box_type);
                                $box_type     = mysqli_fetch_array($rs_box_type);                                
                                $unit = $box_type["name"];                                
                                
                                ////////////////////////////////////////////////        
        
                              if ($row["type"] == "0") {
                                    $market = "Standing Order";
                              }else{
                                    $market = "Open Market";
                              }
 
       
           // Datos de la Oferta
   
   $sqlOffer="select gor.marks          ,
                     gor.id             ,  
                     gor.offer_id       , 
                     gor.offer_id_index , 
                     gor.grower_id      , 
                     gor.buyer_id       , 
                     gor.status         , 
                     gor.product        , 
                     gor.price          , 
                     gor.size           , 
                     gor.boxtype        , 
                     gor.bunchsize      , 
                     gor.boxqty         , 
                     gor.bunchqty       , 
                     gor.steams         ,
                     gor.req_group      ,
                     gor.request_id     ,
                     g.growers_name     ,
                     gor.product_subcategory,
                     gor.type_market    ,
                     c.name as colorname                     
                from grower_offer_reply gor
                left JOIN product p ON  gor.product = p.name and gor.product_subcategory = p.subcate_name 
                left JOIN colors c ON p.color_id = c.id
                left JOIN growers g ON gor.grower_id = g.id                                                                  
               where request_id = '" . $row['idreq'] . "'
                 and buyer_id   = '" . $idbuy . "'      " ;      
   
                 //and buyer_id = '" . $idbuy . "'   " ;    ------------------------------ REVISAR
   
                  

        $result_off   = mysqli_query($con, $sqlOffer);            
              
         $pdf->SetFont('Arial','B',8);
  /*
         $pdf->Cell(40,4,$row['name']." ".$row['subcate']." ".$row['colorname'],0,0,'L');            
         $pdf->Cell(25,6,$row['qty']." ".$unit,0,0,'R');                              
         $pdf->Cell(5,6,' ',0,0,'L');                      
         $pdf->Cell(25,6,$row['cod_order'],0,0,'L'); 
         $pdf->Cell(5,6,' ',0,0,'L');                 
         $pdf->Cell(25,6,$row['lfd'],0,0,'L');                                                            
         $pdf->Cell(25,6,$row['size_name']." cm. ",0,0,'L');  
         $pdf->Cell(25,6,$market,0,1,'L');                                                                     
    */     
         $totrequest = $totrequest + $row['qty'];
         
         
     //    $pdf->Cell(25,6,$row['growers_name'],0,1,'L'); 
         
                  $pdf->SetFont('Arial','',8);
                                     $partialStem = 0;  
                                                  
                        while($rowOff = mysqli_fetch_assoc($result_off))  {
                            
                            if ($unit == 'ST'){                                                                                                                                       
                                if ($row['box_type'] == 0) {
                                    $stems = $rowOff['bunchqty']*$rowOff['steams'];
                                } else {
                                    $stems = $rowOff['bunchqty'];
                                }
                            } else {
                                  $stems = $rowOff['bunchqty'];
                            }
                            
                              if ($rowOff["type_market"] == "0") {
                                    $market = "Standing Order";
                              }else{
                                    $market = "Open Market";
                              }
                                 
                               // $pdf->Cell(34,6,$market,0,0,'L');                                
                               // $pdf->Cell(70,6,$rowOff['product']." ".$rowOff['product_subcategory']." ".$rowOff['colorname'],0,0,'L');    
                               // $pdf->Cell(25,6,$stems,0,0,'L');    
                               // $pdf->Cell(25,6,$rowOff['size']." cm. ",0,0,'L');                                    
                               // $pdf->Cell(25,6,$rowOff['growers_name'],0,1,'L'); 
                                
                                $totoffer = $totoffer + $stems;
                                
                                $cabCount = 0;  
                                $partialStem = $partialStem + $stems;  
                        }  
                          $differ = $row['qty'] - $partialStem;
                          
                          $porceStems = ($partialStem/$row['qty'])*100;
                          
         if ($row['subcate'] != $tmp_cate) {
             $pdf->Ln(4);  
             $tmp_cate = $row['subcate'];
         }        
                          
                          
         $pdf->Cell(45,4,$row['subcate']." ".$row['name']." ".$row['colorname'],0,0,'L');            
         $pdf->Cell(25,6,$row['qty']." ".$unit,0,0,'R');                              
         $pdf->Cell(5,6,' ',0,0,'L');                      
         $pdf->Cell(25,6,$row['cod_order'],0,0,'L'); 
         $pdf->Cell(5,6,' ',0,0,'L');                 
         $pdf->Cell(25,6,$row['lfd'],0,0,'L');                                                            
         $pdf->Cell(25,6,$row['size_name']." cm. ",0,0,'L'); 
         
         $pdf->SetFont('Arial','B',8);         
            $pdf->Cell(25,6,$differ."    ".number_format($porceStems, 2, '.', ',')." %",0,1,'R');    
         $pdf->SetFont('Arial','',8);

               // $pdf->Ln(2);
               // $pdf->SetFont('Arial','B',10);
               // $pdf->Cell(30,10,' ',0,0,'L');     
               // $pdf->Cell(30,10,'%Fullfill   :      ',0,0,'R');     
               // $pdf->Cell(25,10,number_format($partialStem, 0, '.', ','),0,0,'L');    
               // $pdf->Cell(25,10,number_format($porceStems, 2, '.', ',')." %",0,0,'L');    
               // $pdf->Cell(30,10,'To complete :  '.$differ,0,1,'R');     
               // $pdf->Cell(70,6,'            _____________________________________________________________________________________',0,1,'L');  
                                
               //   $pdf->Ln(8);
                
                //if ($cabCount == 0) {
                //      $pdf->Ln(4);    
                //    $cabCount = 1;  
                //}
         

         
         $totalStems = $totalStems + $subtotalStems;                     
         $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;  

    }
               
  //  $pdf->Ln(2);
  //  $pdf->Cell(70,6,'_________________________________________________________________________________________________________',0,1,'L');  
  //  $pdf->SetFont('Arial','B',10);

  //  $pdf->Cell(30,10,'TOTALS :     ',0,0,'L');    
  //  $pdf->Cell(25,10,'Req.  : '.number_format($totrequest, 0, '.', ','),0,0,'L');    
  //  $pdf->Cell(25,10,'Offer : '.number_format($totoffer, 0, '.', ','),0,0,'L');    
  //  $pdf->Cell(25,10,'To complete : '.number_format($totrequest-$totoffer, 0, '.', ','),0,1,'L');  
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////
    /*
    
   $sqlmissing="select unseen,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       p.name , 
                       p.box_type ,                        
                       br.qty ,  
                       br.lfd ,
                       sizeid ,
                       sz.name as size_name,
                       g.growers_name      ,
                       c.name as colorname ,
                       sc.name as subcate  ,
                       IFNULL((select sum(gor.bunchqty * gor.steams) from grower_offer_reply gor  where request_id = br.id),0) as ofertas
                  from buyer_requests br
                 INNER JOIN product p     ON br.product = p.id
                 INNER JOIN subcategory sc ON p.subcategoryid = sc.id
                 INNER JOIN buyers b     ON br.buyer = b.id
                 INNER JOIN buyer_orders bo ON br.id_order = bo.id
                 INNER JOIN sizes sz ON br.sizeid = sz.id  
                  LEFT JOIN colors c ON p.color_id = c.id
                  left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer    = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
                 order by p.name ";

        $missing   = mysqli_query($con, $sqlmissing);        
    
    $pdf->Ln(10);
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  MISSING ',0,1,'L');     
    $pdf->Cell(70,6,'_______________________________________',0,1,'L');      
    
    $pdf->Ln(5);
    
    $totalMisqty = 0;
    
    while($mis = mysqli_fetch_assoc($missing))  {
               
         $pdf->SetFont('Arial','B',8);
         
          if ($mis['qty']-$mis['ofertas'] > 0) {
  
                    $pdf->Cell(70,4,$mis['name']." ".$mis['subcate']." ".$mis['colorname']." ".$mis['size_name']." cm. ",0,0,'L');            
                    $pdf->Cell(15,6,number_format($mis['qty']-$mis['ofertas'], 0, '.', ','),0,1,'R');   
                          
                    $totalMisqty  = $totalMisqty + $mis['qty'];              
                    $totalMisofer = $totalMisofer + $mis['ofertas'];              
           }         
    }    
    
    $pdf->Cell(50,10,' ',0,0,'L');     
    $pdf->Cell(25,10,'Total Missing......: '.number_format($totalMisqty-$totalMisofer, 0, '.', ','),0,1,'L');  
    
    */
    
  $pdf->Output();
  ?>