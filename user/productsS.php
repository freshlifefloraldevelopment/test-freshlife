<?php


/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/


require_once("../config/config_gcp.php");
$htmlLoadData="";
$htmlLoadData="<option value='0'>Select a Size</option>";
$idPr = $_POST['idPr'];
$idGr = $_POST['idGr'];


 if ($idGr != "" && $idPr != "") {

           $sql_boxes = "select gs.sizes as sizeid,
          sh.name as sizename
          from growcard_prod_sizes gs
          inner join product p     on gs.product_id=p.id
          inner join category pc   on p.categoryid=pc.id
          inner join subcategory s on p.subcategoryid=s.id
          inner join colors c      on p.color_id=c.id
          inner join sizes sh      on gs.sizes=sh.id
          left join growcard_prod_features gpf on gs.product_id=gpf.product_id and gs.grower_id = gpf.grower_id
          left join features ff on gpf.features=ff.id
          where gs.grower_id  ='$idGr'
          and gs.product_id  ='$idPr'
          group by  gs.sizes";

       $rs_boxes = mysqli_query($con,$sql_boxes);

           while ($row_boxes = mysqli_fetch_array($rs_boxes))
           {
             $productID = $row_boxes["sizeid"];
             $productName = $row_boxes["sizename"];

           $htmlLoadData .='<option value="'.$productID.'">'.$productName.' [cm]</option>';
           }

 }
 echo $htmlLoadData;
?>
