<?php

// PO 2018-09-21

require_once("../config/config_gcp.php");
session_start();

$id_offer  = $_GET['id_offer'];
$idbuyer = $_GET['id_buy'];
$fac_id  = $_GET['id_fact'];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}

$numRowsCab = @mysqli_query($con,"SELECT count(*) as total FROM grower_offer_reply_cab");
$countCab = @mysqli_fetch_assoc($numRowsCab);
$countCab1 = $countCab['total']+1;

	      $qsel="select gor.id  as gorid , gor.offer_id , gor.offer_id_index , gor.grower_id  , gor.size , gor.price ,
                       gor.steams       , gor.product  , gor.boxqty         , gor.buyer_id   , gor.grower_id  ,
                       gor.bunchqty     , gor.boxtype  , gor.status         , gor.request_id , g.growers_name as gname,
                       gor.type_market  , gor.product_subcategory           , br.id_order    , gor.cliente_id ,
                       substr(cl.name,1,12) as namecli, gor.id_group_cab, br.id as requestID
                  from grower_offer_reply gor
                 inner join growers g on gor.grower_id = g.id
                 inner join buyer_requests br on gor.offer_id = br.id
                  left join sub_client cl ON cl.id = gor.cliente_id
                 where gor.buyer_id = '" . $idbuyer . "'
                   and br.id_order  = '" . $fac_id . "'
                 ORDER BY gor.id_group_cab DESC";

$numero_filas = mysqli_num_rows(mysqli_query($con,$qsel));
	 $rs=mysqli_query($con,$qsel);

	if(isset($_GET['delete'])) 	{
	  $queryDel = 'DELETE FROM grower_offer_reply WHERE id = '.(int)$_GET['delete'];
	  mysqli_query($con,$queryDel);

          header('location:grower_offer_mgmt_check.php?id_fact='.$tmp_order.'&id_buy='.$tmp_buyer);

          //header('location:buy_flowers_request_others.php');
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
<style>



.color-not-without  {
  background: #ccc !important; // Define your own color in your CSS
}
.color-not-without :hover, .color-not-available:active {
  background: #ccc !important; // Define your own color's darkening/lightening in your CSS
}

.color-not-available {
  background: #b02a37 !important; // Define your own color in your CSS
}
.color-not-available:hover, .color-not-available:active {
  background: #e35d6a !important; // Define your own color's darkening/lightening in your CSS
}

.color-complete {
  background: #6dbb30 !important; // Define your own color in your CSS
}
.color-complete:hover, .color-complete:active {
  background: #95d661 !important; // Define your own color's darkening/lightening in your CSS
}

.color-approve-change {
  background: #6610f2 !important; // Define your own color in your CSS
}
.color-approve-change:hover, .color-approve-change:active {
  background: #9276c4 !important; // Define your own color's darkening/lightening in your CSS
}

.color-inprocess {
  background: #fd7e14 !important; // Define your own color in your CSS
}
.color-inprocess:hover, .color-inprocess:active {
  background: #feb272 !important; // Define your own color's darkening/lightening in your CSS
}


.countdown-label {
	color: #fff;
	text-align: center;
	text-transform: uppercase;
  margin-top: 1px
}
#countdown{
box-shadow: 0 1px 2px 0 rgba(1, 1, 1, 0.4);
	height: 30px;
	text-align: center;
background: #495057;
	border-radius: 5px;
	margin: auto;

}

#countdown #tiles{
  color: #fff;
	position: relative;
	z-index: 1;
text-shadow: 1px 1px 0px #ccc;
	display: inline-block;
  font-family: Arial, sans-serif;
	text-align: center;

  padding: 5px;
  border-radius: 5px 5px 0 0;
  font-size: 12px;
  font-weight: thin;
  display: block;

}

.color-full {
  background: #4c2c92;
	color: #fff;
	position: relative;
	z-index: 1;
	display: inline-block;
  font-family: Arial, sans-serif;
	text-align: center;

  padding: 5px;
  border-radius: 5px 5px 0 0;
  font-size: 12px;
  font-weight: thin;
  display: block;
}
.color-half {
  background: #4c2c92;
}
.color-empty {
  background: #e5554e;
}

#countdown #tiles > span{
	width: 70px;
	max-width: 70px;

	padding: 18px 0;
	position: relative;
}





#countdown .labels{
	width: 100%;
	height: 25px;
	text-align: center;
	position: absolute;
	bottom: 8px;
}

#countdown .labels li{
	width: 102px;
	font: bold 15px 'Droid Sans', Arial, sans-serif;
	color: #f47321;
	text-shadow: 1px 1px 0px #000;
	text-align: center;
	text-transform: uppercase;
	display: inline-block;
}
</style>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Offers</td>
                  </tr>

                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">
                    <div class="demo_jui">
										<?php
										$qsel_Previo="select gor.id  as gorid , gor.offer_id , gor.offer_id_index , gor.grower_id  , gor.size , gor.price ,
						                       gor.steams       , gor.product  , gor.boxqty         , gor.buyer_id   , gor.grower_id  ,
						                       gor.bunchqty     , gor.boxtype  , gor.status         , gor.request_id , g.growers_name as gname,
						                       gor.type_market  , gor.product_subcategory           , br.id_order    , gor.cliente_id ,
						                       substr(cl.name,1,12) as namecli, gor.id_group_cab, br.id as requestID
						                  from grower_offer_reply gor
						                 inner join growers g on gor.grower_id = g.id
						                 inner join buyer_requests br on gor.offer_id = br.id
						                  left join sub_client cl ON cl.id = gor.cliente_id
						                 where gor.buyer_id = '" . $idbuyer . "'
						                   and br.id_order  = '" . $fac_id . "'
						                 ORDER BY gor.id_group_cab DESC";


							 $rsPrevio=mysqli_query($con,$qsel_Previo);
							 while($productP=mysqli_fetch_array($rsPrevio))
							 {
								 $cadenaItemsData .= ','.$productP["id_group_cab"];
							 }


										?>
<input type="hidden" value="<?php echo $cadenaItemsData;  ?>" id="numRecords" />
<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>
                    <th align="left" width="30%" >Grower</th>
                    <th align="left" width="10%" >Head</th>
										<th align="left" width="25%" >Variety</th>
                    <th align="left" width="12%">Size</th>
                    <th align="left" width="12%">Bun</th>
                    <th align="left" width="12%">St.</th>
                    <th align="left" width="15%">Price</th>
                    <th align="left" width="20%">Market</th>
                    <th align="left" width="30%" >Status</th>
		</tr>

</thead>

	<tbody>

		<?php

		  	$sr=1;
				$flagZZ=0;
				 $flahH = 0;
        		  while($product=mysqli_fetch_array($rs))
							{
								$i = $i+1;


								 $head = $product["id_group_cab"];


								 $sel_status_approved = "SELECT cof.status, cof.request_id, cof.date as date_cab_offer FROM grower_offer_reply_cab cof WHERE id='" .$head. "'";
								$rs_status_approved = mysqli_query($con, $sel_status_approved);
								$id_Status = mysqli_fetch_array($rs_status_approved);

								//  $idFlagRId = $product["requestID"];
								//  $id_Status[1];

																			if($id_Status[0]==3)
																			{
																			$bgCell = '#F2F2F2';


																			}
																			else
																			{
																			$bgCell = '#FFFFFF';

																			}


															if ($product["type_market"] == "0") {
                                    $market = "Standing Order";
                              }else{
                                    $market = "Open Market";
                              }

                              if ($product["cliente_id"] == 0) {
                                    $soloclien = "";
                              }else{
                                    $soloclien = $product["namecli"];
                              }

                              $tmp_order = $product["id_order"];
                              $tmp_buyer = $product["buyer_id"];

															$cartid = $product["request_id"];
															//$timeEnd = $product['countdown'];
														 	$timeEndCab = $id_Status['date_cab_offer'];

																		date_default_timezone_set("America/Vancouver");

  																	$dateR = date('Y-m-d H:i:s');
																	  $to_time = strtotime($dateR);
																	  $from_time = strtotime($timeEndCab);

																	  $valueMinutes = round(($from_time - $to_time) / 60,0);
																	  $porcionesi = explode(".", $valueMinutes);

															if($id_Status[0]==3)
															{
																$icon = '<i class="align-button float-left fa fa-check-circle-o "></i>';
																$botonR = 'color-complete bg-gradient-success text-white';
																$botonRM = 'color-complete text-white';
																$titleModel = 'Order Complete';
																$name_Status =   'Complete';
															}

															if($id_Status[0]==4)
															{
																$icon = '<i class="align-button float-left fa fa-ban" aria-hidden="true"></i>';
																$botonR = 'color-not-available bg-gradient-danger text-white';
																$titleModel = '';
																$name_Status =   'Not approved';
															}

															if($id_Status[0]=='')
															{
																$icon = '<i class="align-button float-center fa fa-ban" aria-hidden="true"></i>';
																$botonR = 'color-not-without bg-gradient-danger text-white';
																$titleModel = '';
																$name_Status =   'N/A';
															}

															if($id_Status[0]==2)
															{
																$icon = '<i class="align-button float-left fa fa-ban" aria-hidden="true"></i>';
																$botonR = 'color-approve-change bg-gradient-danger text-white';
																$titleModel = '';
																$name_Status =  '...Waiting';
															}

		?>
                          <tr class="gradeU" style="background-color:<?php echo $bgCell ?>">

                                <td class="text" align="left">
																	<input type="hidden" value="<?php echo $head;  ?>" id="flagID" />
																	<input type="hidden" value="<?php echo $cartid;  ?>" id="valRBID<?php echo $i; ?>" />
																	<?php echo $product["gname"]?></td>
																<td class="text" align="left"><?php echo $product["id_group_cab"]; ?></td>
																<td class="text" align="left"><?php echo $product["product"]." ".$product["product_subcategory"]?></td>
                                <td class="text" align="left"><?php echo $product["size"]?>  </td>
                                <td class="text" align="left"><?php echo $product["bunchqty"]?> </td>
                                <td class="text" align="left"><?php echo $product["steams"]?> </td>
                                <td class="text" align="left"><?php echo $product["price"]?> </td>
                                <td class="text" align="left"><?php echo $market?> </td>
                                <td class="text" align="left"><?php //echo $soloclien?>

																	<button onclick="return false;"  class="text  <?php echo $botonR; ?>">
																												<span class="p-0-xs">

																													<span class="fs--10">

																														<?php echo $name_Status; ?>
																														<!-- <sup class="badge badge-danger position-absolute end-0 mt--n5 text-warning fs--14"><?php echo $rs_order1NumR; ?></sup> -->
																													</span>
																												</span>
																											</button>

																		<?php
																		if($id_Status[0]==2)
																		{
																				 $flagZZ = 1;
																		?>

																		<input type="hidden" id="set-time<?php echo $head; ?>" value="<?php echo $porcionesi[0];?>"/>
																		<input type="hidden" value="<?php echo $head; ?>" id="requestIDID<?php echo $head; ?>"/>

																		<div id="countdown" class="b-0 w--200 float-left">

																		<div id='tiles<?php echo $head; ?>' class="color-full b-0 w--200" style="color: #fff;
																		position: relative;
																		z-index: 1;
																	text-shadow: 1px 1px 0px #ccc;
																		display: inline-block;
																		font-family: Arial, sans-serif;
																		text-align: center;

																		padding: 5px;
																		border-radius: 5px 5px 0 0;
																		font-size: 12px;
																		font-weight: thin;
																		display: block;"></div>

																		</div>
																		<?php
																	}else{
																		$flagZZ = 0;
																	}
																		?>
																</td>

                                <td align="center" ><a href="grower_offer_edit.php?idor=<?php echo $product["gorid"]."&bck=".$product["offer_id"]."&id_buyerID=".$idbuyer."&qty=".$product["bunchqty"]."&varietyname=".$product["product"]." ".$product["product_subcategory"]; ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                <td align="center" >

																	<a href="?delete=<?php echo $product["gorid"]?>"  onclick="return confirm('Are you sure, you want to delete this Offer ? ');"><img src="images/images_front/delete.gif" border="0" alt="Delete" /></a>

																</td>

                          </tr>
				 <?php
			     $sr++;
			   } ?>
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

      <script type="text/javascript">

      window.onload=function() {


				setInterval(function(){ location.reload(); }, 100000);

        //var num = document.getElementById("numRecords").value;



				var b = document.getElementById("numRecords").value;
				var str = b;
				var result = Array.from(new Set(str)).join(',');
				var subst = result.substring(2);

	      var divisiones = subst.split(",");
	      var tamanioCab = divisiones.length;

	      var sec=0;
				var offersId=[];
				var timesSetId=[];

				      for(var i=0; i<tamanioCab;i++)
				      {

				        sec = divisiones[i];
				      //  alert(sec);
				      //  document.getElementById("tiles"+sec).value = '';

				        var minutes = $('#set-time'+sec).val();
				        var setTimeOut = $('#set-time'+sec).val();
				        var idRBID = $('#requestIDID'+sec).val();

				        offersId[i]=idRBID;
				        timesSetId[i]=setTimeOut;

				        var datosRBID = 's_offersId=' + offersId + '&s_times=' + timesSetId+ '&s_total=' + tamanioCab;

				        var target_date = new Date().getTime() + ((minutes * 60 ) * 1000);
				        // set the countdown date
				        var time_limit = ((minutes * 60 ) * 1000);
				        //set actual timer


								//alert(time_limit);

				        if(time_limit<=0){
				        setTimeout(
				          function()
				          {
				            $.ajax({
				                 type: "POST",
				                 url: "../buyer/UpdateOfferComplete.php",
				                 data: datosRBID,
				                 cache: false,
				                 success: function(r){
				                    //  alert(r);
				                    //  return false;
				                     //if(idRBID!=3)
				                     //{
				                         swal("Request Completed!", "Your request was completed.","warning");
				                       var x = setInterval(function() {
				                         location.reload();

				                         /*********************************/
				                         // Actualizar Oferta             //
				                         /*********************************/




				                         swal.close();
				                       }, 2000);
				                    // }
				                 }
				             });



				          }, time_limit );

				        }

				        //  countdown.innerHTML = '';

				          getCountdown(sec,target_date);
				          setInterval(getCountdown,1000,sec,target_date);

				        } // cierro el for





				        function getCountdown(sec,target_date)
				        {


				          //alert(sec);


				          //; // get tag element

				              var days, hours, minutes, seconds; // variables for time units
				                    // find the amount of "seconds" between now and target
				                    var current_date = new Date().getTime();
				                    var seconds_left = (target_date - current_date) / 1000;


				                    if ( seconds_left >= 0 )
				                    {

				                    days =  parseInt(seconds_left / 86400) ;
				                    seconds_left = seconds_left % 86400;

				                    hours = parseInt(seconds_left / 3600) ;
				                    seconds_left = seconds_left % 3600;

				                    minutes = parseInt(seconds_left / 60) ;
				                    seconds = parseInt( seconds_left % 60 ) ;

				                    // format countdown string + set tag value
				                    //document.getElementById("tiles"+sec).innerHTML = "loading";
				                  //  document.getElementById("tiles"+sec).innerHTML = "<span>" + 00 + "</span>";
				                  //  document.getElementById("tiles"+sec).innerHTML = "<span>" + minutes + "</span>";
				                    document.getElementById("tiles"+sec).innerHTML = "<span>" + hours + ":</span><span>" + minutes + ":</span><span>" + seconds + "</span>";


				                    }

				        }

								function pad(n) {
									return (n < 10 ? '0' : '') + n;
								}
      }
      </script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
			<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
