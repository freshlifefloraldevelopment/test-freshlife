<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
   
    $userSessionID = $_POST['codgrow'];
    
    $fini = $_POST['fecha_ini'];
    $ffin = $_POST['fecha_fin'];
    
    
    
    
    $idbuy = $_GET['id_buy'];    
    
        // Datos del Grower
    $growerEntity = "select g.id,g.growers_name
                      from growers  g 
                     where g.id = '" . $userSessionID . "'  "   ;
     
    $grower   = mysqli_query($con, $growerEntity);
    $nameGrow = mysqli_fetch_array($grower);
    


    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
      
   
   // Datos de la Orden  DOH
   
   $sqlDetalis="select CAST(br.id_order AS UNSIGNED) idorder,
                       unseen        , br.id as idreq,
                       bo.qucik_desc , b.first_name  ,                          
                       br.cod_order  , br.product    ,
                       p.name        , br.qty        ,  
                       br.lfd        , sizeid        ,
                       sz.name as size_name          , g.growers_name                       
                  from buyer_requests br
                 INNER JOIN product p             ON br.product   = p.id
                 INNER JOIN buyers b              ON br.buyer     = b.id
                 INNER JOIN buyer_orders bo       ON br.id_order  = bo.id
                 INNER JOIN sizes sz              ON br.sizeid    = sz.id  
                 INNER JOIN grower_offer_reply gr ON br.id        = gr.request_id 
                 left JOIN growers g              ON br.id_grower = g.id                                                  
                 where br.lfd >= '" . $fini . "'  
                   and br.lfd <= '" . $ffin . "'  
                   and br.type        = 0
                   and gr.grower_id   = '" . $userSessionID . "'  
                   and gr.type_market = '0'
                 order by p.name ";

        $result   = mysqli_query($con, $sqlDetalis);    
        

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  ORDER CONFIRMATION ',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,$nameGrow['growers_name'],0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'Initial: '.$fini,0,1,'L');           
    $pdf->Cell(70,6,'End   : '.$ffin,0,1,'L');    
    $pdf->Ln(10);
    
    $cabCount = 1;  
    
// Cabecera
    
    $pdf->Cell(40,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'R');
    $pdf->Cell(5,6,' ',0,0,'L');        
    $pdf->Cell(25,6,'Order',0,0,'L');
    $pdf->Cell(5,6,' ',0,0,'L');            
    $pdf->Cell(25,6,'Lfd',0,0,'L');    
    $pdf->Cell(35,6,'Size',0,0,'L'); 
    $pdf->Cell(25,6,'#Num.Req',0,1,'L');     
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
 
       
           // Datos de la Oferta
   
   $sqlOffer="select gor.marks          ,
                     gor.id             ,  
                     gor.offer_id       , 
                     gor.offer_id_index , 
                     gor.grower_id      , 
                     gor.buyer_id       , 
                     gor.status         , 
                     gor.product        , 
                     gor.price          , 
                     gor.size           , 
                     gor.boxtype        , 
                     gor.bunchsize      , 
                     gor.boxqty         , 
                     gor.bunchqty       , 
                     gor.steams         ,
                     gor.req_group      ,
                     gor.request_id     ,
                     gor.product_subcategory,
                     g.growers_name     ,                                       
                     b.first_name                                            
                from grower_offer_reply gor
                left JOIN growers g ON gor.grower_id = g.id                                                                  
                left JOIN buyers b ON gor.buyer_id = b.id                                                                                  
               where request_id  = '" . $row['idreq'] . "'
                 and grower_id   = '" . $userSessionID . "'  
                 and type_market = '0' " ;          
                        

        $result_off   = mysqli_query($con, $sqlOffer);            
              
         $pdf->SetFont('Arial','B',8);
  
         $pdf->Cell(40,4,$row['name'],0,0,'L');            
         $pdf->Cell(25,6,$row['qty'],0,0,'R');                              
         $pdf->Cell(5,6,' ',0,0,'L');                      
         $pdf->Cell(25,6,$row['cod_order'],0,0,'L'); 
         $pdf->Cell(5,6,' ',0,0,'L');                 
         $pdf->Cell(25,6,$row['lfd'],0,0,'L');                                                            
         $pdf->Cell(25,6,$row['size_name']." cm. ",0,0,'L');                                                                     
         $pdf->Cell(15,6,$row['idorder'],0,1,'L');                                                            
         
                  $pdf->SetFont('Arial','',8);
                  
                        while($rowOff = mysqli_fetch_assoc($result_off))  {
                            
                            
         // Verificacion Stems/Bunch
        
                        $sel_bu_st = "select box_type , subcategoryid 
                                        from product 
                                       where name = '" . $row['prod_name'] . "' 
                                         and subcate_name = '" . $row['product_subcategory'] . "'  "; 

                        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
                        $bunch_stem = mysqli_fetch_array($rs_bu_st);  
                        
              if ($bunch_stem['box_type'] == 0) {
                    $stems = $rowOff['bunchqty']*$rowOff['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $stems = $rowOff['bunchqty'];
                    $unitFac = "BUNCHES";                   
              }                                
        

                                 
                                $pdf->Cell(1,6,' ',0,0,'L');                                
                                $pdf->Cell(40,6,$rowOff['product']." ".$rowOff['product_subcategory'],0,0,'L');    
                                $pdf->Cell(25,6,$stems." ".$unitFac,0,0,'L');    
                                $pdf->Cell(15,6,$rowOff['size']." cm.",0,0,'L');                                    
                                $pdf->Cell(15,6,'$'.number_format($rowOff['price'], 2, '.', ','),0,0,'R');                                        
                                $pdf->Cell(25,6,$rowOff['first_name'],0,1,'L'); 
                                
                                $cabCount = 0;  
                            }         
                                  if ($cabCount == 0) {
                                        $pdf->Ln(4);    
                                      $cabCount = 1;  
                                  }
        
         $totalStems = $totalStems + $subtotalStems;                     
         $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;  
    }               
    
  $pdf->Output();
  ?>