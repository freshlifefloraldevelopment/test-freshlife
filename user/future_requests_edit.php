<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

  $off_id   = $_GET['idor'];  
  $fact_num = $_GET['id_fact'];  
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

      
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
     
    $ins = "update order_subclient 
               set estado        = '" . $_POST["estado"]   . "' ,
                   observaciones = '" . $_POST["observaciones"] . "'     
             where id = '" . $off_id . "' ";    
       
   if(mysqli_query($con, $ins))
    
    header("location:future_requests_detal.php?id_fact=$fact_num");   
}


  $sel_offers = "select  id            , id_week       , id_client     , product       , prod_name     ,
                         buyer         , grower_id     , qty_pack      , size          , steams        ,
                         comment       , id_state      , date_ship     , date_del      , fact_id       ,
                         cost          , price         , estado        , observaciones  
                   from order_subclient
                  where id = '" . $off_id . "' ";

$rs_offers = mysqli_query($con, $sel_offers);
$row = mysqli_fetch_array($rs_offers);   


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                
                arrTmp[0] = checkfrom();

                
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()       {
                
                if (trim(document.frmcat.estado.value) == "")  {
                    document.getElementById("lblestado").innerHTML = "Please enter process ";
                    return false;
                }else {
                    document.getElementById("lblestado").innerHTML = "";
                    return true;
                }                
                                                
            }
            

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Process </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="future_requests_detal.php" onclick="this.blur();"><span> Orders</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="future_requests_edit.php?idor=<?php echo $row["id"]."&id_fact=".$row["fact_id"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>



                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                                                                </tr>
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>State </td>
                                                                                    
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                    <select name="estado" id="estado" class="listmenu">
                                                                                        <option value="0" <?php  if($row["estado"] == 0) echo "selected";?>>Request</option>
                                                                                        <option value="1" <?php  if($row["estado"] == 1) echo "selected";?>>PROCESSED</option>
                                                                                        <option value="2" <?php  if($row["estado"] == 2) echo "selected";?>>Pending</option>
                                                                                    </select>                                                                                        
                                                                                        
                                                                                        <br>
                                                                                            <span class="error" id="lblestado"></span>
                                                                                    </td>
                                                                                </tr>                                                                                     
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Comments </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="observaciones" id="observaciones" value="<?php echo $row["observaciones"]?>" />
                                                                                        </td>
                                                                                </tr>                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>
                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>