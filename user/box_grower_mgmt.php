<?php

	include "../config/config_gcp.php";

	session_start();

	$qsel="select bgc.id as idbox      , bgc.order_id  , bgc.grower_id , bgc.box_id , 
                      bgc.box_qty  , bgc.factor    , bgc.date_adi  , bgc.status ,
                      g.growers_name , b.name as boxname
                 from box_grower_confirmation bgc
                inner join growers g on bgc.grower_id = g.id                                                  
                inner join boxtype b on bgc.box_id = b.id                                                                  
                order by bgc.id ";

	$rs=mysqli_query($con, $qsel);            

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	if(isset($_GET['delete'])) {
            $query = 'DELETE FROM box_grower_confirmation WHERE  id= '.(int)$_GET['delete'];
            mysqli_query($con,$query);	
            header("location: box_grower_mgmt.php");
	}
	  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/images_front/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Box Growers Confirmation</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

		    <tr>
                        <td>
                            <table width="100%">
                            <tr>
                                <td>
                                    <a href="boxgrow_add.php" class="pagetitle" onclick="this.blur();"><span> + Add New Box Grower  </span></a>
                                </td>
                            </tr>
                            </table>
                        </td>
                    </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

<td>
    <div id="box">

    <div id="container">			

    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>

			<th width="5%" align="center">Sr.</th>
                        
			<th width="10%" align="center">Order</th>                        

			<th width="35%" align="left">Grower</th>
                        
			<th width="15%" align="left">Box</th>       
                        
			<th width="15%" align="left">Qty</th>                                                
                        
			<th width="10%" align="center">Factor</th>                                                                        

                        <th align="center" width="10%">Edit</th>
                        
			<th align="center" width="12%">Del</th>

		</tr>

	</thead>

<tbody>

        <?php
                $sr=1;

                while($colors=mysqli_fetch_array($rs))	  {

                            if ($colors['status'] == 0) {
                                $estado = 'Pending';
                            }else{
                                $estado = 'Process';
                            }                            
        ?>
                <tr class="gradeU">

                    <td align="center" class="text"><?php echo $sr;?></td>

                    <td class="text" align="left"><?php echo $colors["order_id"]?></td>

                    <td class="text" align="left"><?php echo $colors["growers_name"]?></td>

                    <td class="text" align="left"><?php echo $colors["boxname"]?></td>

                    <td class="text" align="left"><?php echo $colors["box_qty"] ?> </td>  

                    <td class="text" align="left"><?php echo $colors["factor"] ?></td>                                

                    <td align="center" ><a href="boxgrow_edit.php?id=<?php echo $colors["idbox"]?>"><img src="images/images_front/edit.gif" border="0" alt="Edit" /></a></td>

                    <td align="center" ><a href="?delete=<?php echo $colors["idbox"]?>"  onclick="return confirm('Are you sure, you want to delete this box ?');"><img src="images/images_front/delete.gif" border="0" alt="Delete" /></a></td>

                </tr>

            <?php

                            $sr++;
                    }
            ?> 

</tbody>

</table>



        </div>

        </div>



	</div>

</td>
                      
                      
                      

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img src="images/images_front/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/images_front/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/images_front/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/images_front/middle-bottomline.gif"></td>

            <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
