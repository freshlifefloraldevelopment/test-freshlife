<?php	
	include "../config/config.php";

	@session_start();
        
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)

	{
            header("location: index.php");

	}
        
        if(isset($_GET['delete'])) 
        {
            $query = 'DELETE FROM sub_menu WHERE  s_menu_id= '.(int)$_GET['delete'];
            mysql_query($query);
            header("location:sub_menu_mgmt.php");
        }
        
	$sel_sub_menu="select sub_menu.*, main_menu.m_menu_name from sub_menu left join main_menu on main_menu.m_menu_id = sub_menu.m_menu_id order by main_menu.m_menu_order ASC";
	$rs_sub_menu=mysql_query($sel_sub_menu);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,

					"sPaginationType": "full_numbers"

				});

			} );

</script>

</head>



<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/website-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Manage Website Sub Menu </td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>
                  
                  <tr>

                    <td>

                        <table width="100%">

                            <tbody><tr>

                                <td>

                                    <a onclick="this.blur();" class="pagetitle" href="add_sub_menu_item.php"><span> + Add New Sub Menu Item  </span></a>

                                </td>

                            </tr>

                        </tbody></table>

                    </td>

                </tr>      
                    
                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>

			<th width="10%" align="center">Sr.</th>
                        
			<th width="20%">Main Menu Name</th>
                        
                        <th width="20%">Sub Menu Name</th>

			<th width="50%">Sub Menu Link</th>
                        
			<th width="50%">Menu Item Order</th>

			<th align="center" width="10%">Edit</th>

			<th align="center" width="10%">Delete</th>

		</tr>

	</thead>

	<tbody>

		<?php

						  	$sr=1;

						  while($sub_menu=mysql_fetch_array($rs_sub_menu))

						  { 
						  ?>

                        <tr class="gradeU">

                          <td align="center" class="text"><?=$sr;?></td>

                          <td class="text"><?=$sub_menu["m_menu_name"]?></td>
                          
                          <td class="text"><?=$sub_menu["s_menu_name"]?></td>

                          <td class="text"><?=$sub_menu["s_menu_link"]?></td>
                          
                          <td class="text"><?=$sub_menu["s_menu_order"]?></td>					

                          <td align="center" ><a href="edit_sub_menu_item.php?id=<?=$sub_menu["s_menu_id"]?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                          <td align="center" ><a href="sub_menu_mgmt.php?delete=<?=$sub_menu["s_menu_id"]?>"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>
                          </tr>

						 <?php

						 		$sr++;

						 	}

						 ?> 

	</tbody>

</table>

			</div>

			</div>

			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>

