<?php
	include "../config/config_gcp.php";
        
    @session_start();
    
    if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)    {
    	header("location: index.php");
    }
    if(isset($_POST["Submit"]) && $_POST["Submit"]=="Add")    {

			  		        $today = date('mdyHis');
						$tmp1 = $_FILES['image']['name'];
						$ext1 = explode('.',$tmp1);
						$image=0;
						// po $uploaddir = '../product-image/';
                                                
						$uploaddir = '/var/www/html/product-image/big/';                                                
																														
						if($ext1[1]=="JPG"){
							$extention="jpg";
						}else if($ext1[1]=="GIF"){
							$extention="gif";
						}else if($ext1[1]=="PNG"){
							$extention="png";
						}else{
							$extention=$ext1[1];
						}

                                                
						      $uploadfile1 = $uploaddir.$today."_crop.".$extention;                                                

						$filepath1 = 'product-image/'.$today."big.".$extention;

						$filepath3 = 'product-image/big/'.$today."_crop.".$extention;								

						move_uploaded_file($_FILES['image']['tmp_name'],$uploadfile1);				

						list($width, $height) = getimagesize("../$filepath1");
																			    					

						$image->new_width = 493;

						$image->new_height = 456;										

						$image->image_to_resize = "../$filepath1"; // Full Path to the file								

						$image->ratio = false; // Keep Aspect Ratio?												

						// Name of the new image (optional) - If it's not set a new will be added automatically								

						$image->new_image_name =$today.'_crop';								

						/* Path where the new image should be saved. If it's not set the script will output the image without saving it */								

						$image->save_folder = '../product-image/big/';								

						 																																			

						unlink("../$filepath1");												
        
        
        //exit();
        $temp_rating=$_POST["quality_rating"] + $_POST["freshness_rating"] + $_POST["trustworthiness_rating"] + $_POST["pricing_rating"]+ $_POST["packing_rating"];
        $final_rating=$temp_rating * 5 / 25;
        
	$ins="update review_rating 
                 set grower_id        ='".$_POST["grower_id"]."' ,
                     first_name       ='".$_POST["first_name"]."',
                     last_name        ='".$_POST["last_name"]."' ,
                     review           ='".$_POST["review"]."'    ,
                     quality_rating   ='".$_POST["quality_rating"]."',
                     freshness_rating ='".$_POST["freshness_rating"]."',
                     trustworthiness_rating='".$_POST["trustworthiness_rating"]."',
                     pricing_rating   ='".$_POST["pricing_rating"]."',
                     Packing_rating   ='".$_POST["packing_rating"]."',
                     final_rating     ='".round($final_rating)."',
                     image_path       = '".$filepath3."'         ,                         
                     entry_date       ='".date("Y-m-d h:i:s")."' 
               where id='".$_GET["id"]."'";                                                                                                                                                                                                                                                                                     
        
        
	mysqli_query($con,$ins);
        
        $res = mysqli_query("SELECT user_rating_idFk 
                              FROM review_rating 
                             WHERE id = ".$_GET["id"]);
        
        $row = mysqli_fetch_array($con,$res);
        
        $sql="update user_ratings 
                 set ur_grower_idFk      ='".$_POST["grower_id"]."',
                     ur_comment          ='".$_POST["review"]."',
                     ur_quality_rating   ='".$_POST["quality_rating"]."',
                     ur_freshness_rating ='".$_POST["freshness_rating"]."',
                     ur_trustworthiness_rating='".$_POST["trustworthiness_rating"]."',
                     ur_pricing_rating ='".$_POST["pricing_rating"]."',
                     ur_packing_rating ='".$_POST["packing_rating"]."',
                     ur_final_rating   ='".round($final_rating)."' 
               where ur_id=".$row["user_rating_idFk"];                                                                                                                                                                                                    
        
	mysqli_query($con,$sql);
        
        
	header('location:review-rating.php');
    }

    $sel_review="select * from review_rating where id='".$_GET["id"]."'";
    
    $rs_review=mysqli_query($con,$sel_review);
    $review_detail=mysqli_fetch_array($rs_review);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script src="js/jquery.raty.js" type="text/javascript"></script>
<script src="js/labs.js" type="text/javascript"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script type="text/javascript">

	function verify()	{ 

		var arrTmp=new Array();

		arrTmp[0]=checkuname();
		arrTmp[1]=checkpass();
                arrTmp[2]=checkimage();  
		
		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)	{

			if(arrTmp[i]==false)	{
			   _blk=false;
			}
		}

		if(_blk==true)	{
			return true;
		}else{
			return false;
		}
 	}	
		
	
	function trim(str) 	{    

		if (str != null) 	{        
			var i;        

			for (i=0; i<str.length; i++) 	{           

				if (str.charAt(i)!=" ") 	{               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            

			for (i=str.length-1; i>=0; i--)		{            

				if (str.charAt(i)!=" ")	{                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") {            
				return "";         
			} else 	{            
				return str;         
			}    
		}
	}
	

	function checkuname()	{

		if(trim(document.frmcat.uname.value) == "")	{	 
			document.getElementById("lbluname").innerHTML="Please enter username";
			return false;
		}else 	{
			document.getElementById("lbluname").innerHTML="";
			return true;
		}
	}
	
	function checkpass()	{

		if(trim(document.frmcat.pass.value) == "")	{	 
			document.getElementById("lblpass").innerHTML="Please enter password";
			return false;
		}else 	{
			document.getElementById("lblpass").innerHTML="";
			return true;
		}
	}
        
 	function checkimage(){    // 5

		if(trim(document.frmcat.image.value) == "")	{	 

			document.getElementById("lblimage").innerHTML="Please upload image";

			return false;

		}else {

			if(!validImageFile(document.frmcat.image.value))	{

				document.getElementById("lblimage").innerHTML="Please select valid image file";

				return false;

			}else{

				document.getElementById("lblimage").innerHTML="";

				return true;
			}
		}
	}       

</script>
<script type="text/javascript">

	function textareaCounter(field,cntfield,maxlimit) {

		if (field.value.length > maxlimit) 	{
			field.value = field.value.substring(0, maxlimit);
		}else	{
			cntfield.value = maxlimit - field.value.length;
		}
	}

	function funLoad(){

		var xmaxlimit1=220;

		var xmaxlimit2=220;

		document.frmcat.txtLen1.value=xmaxlimit1-document.frmcat.short_desc.value.length;

		

	}

	function validImageFile(strfile){

		var str = strfile;

		var pathLenth = strfile.length;

		var start = (str.lastIndexOf("."));

		var fileType = str.slice(start,pathLenth);

		fileType = fileType.toLowerCase();

		if (strfile.length > 0)

		{

		   if((fileType == ".gif") || (fileType == ".jpg") || (fileType == ".jpeg") || (fileType == ".png") || (fileType == ".bmp") || (fileType == ".GIF") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".PNG") || (fileType == ".BMP")) 

		   {
				return true;
		   }else{
				return false;
		   } 

		}

	}

</script>
</head>
<body>
    
  <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">
      
      
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Add Review & Rating</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td><a class="pagetitle1" href="review-rating.php" onclick="this.blur();"><span> Manage Review & Rating</span></a> </td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          
                            <tr>
                              <td><div id="box">
                                  <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                    <tr>
                                      <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Grower Name</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <select name="grower_id" id="grower_id">
                                          <option value="">-Select Grower-</option>
                                          <?php 
                                            $growers_sql="select growers_name,id 
                                                            from growers where active = 'active' order by growers_name ";
                                             $rs_growers=mysqli_query($con,$growers_sql);
                                             
                                             while($row_growers=mysqli_fetch_array($rs_growers))
                                             { ?>
                                                <option <?php if($review_detail['grower_id'] == $row_growers['id']) echo "SELECTED"; ?> value="<?php echo $row_growers['id'];?>" ><?php echo $row_growers['growers_name'];?></option>
                                             <?php }
                                          ?>
                                        </select>
                                        <br>
                                        <span class="error" id="lbluname"></span> </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>First Name</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <input type="text" class="textfieldbig" name="first_name" id="first_name" value="<?php echo $review_detail['first_name']?>" />
                                        <br>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Last Name</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <input type="text" class="textfieldbig" name="last_name" id="last_name" value="<?php echo $review_detail['last_name']?>" />
                                        <br>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Review</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                       
                                        <textarea class="textfieldbig" style="height:116px;width:365px;" name="review" rows="4" cols="5" id="review"><?php echo $review_detail['review']?></textarea>
                                        <br>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Quality Rating</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        
                                        <div id="quality_rating1" data-score="<?php echo $review_detail['quality_rating'];?>"></div>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr> 
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Freshness Rating</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <div id="freshness_rating1" data-score="<?php echo $review_detail['freshness_rating'];?>"></div>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr> 
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Trustworthiness Rating</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <div id="trustworthiness_rating1" data-score="<?php echo $review_detail['trustworthiness_rating'];?>"></div>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr> 
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Pricing Rating</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <div id="pricing_rating1" data-score="<?php echo $review_detail['pricing_rating'];?>"></div>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Packing Rating</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                        <div id="packing_rating1" data-score="<?php echo $review_detail['Packing_rating'];?>"></div>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr> 
                                      
                                    <tr>
                                      <td align="left" valign="middle" class="text">&nbsp;<span class="error">*</span>&nbsp; Image : </td>
                                      <td bgcolor="#f2f2f2"><input type="file" name="image" id="image" />
                                        <b> Image width should greater than height </b> <br>
                                        <span class="error" id="lblimage"></span> </td>
                                    </tr>                                      
                                    
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input name="Submit" type="Submit" class="buttongrey" value="Add" />
                                      </td>
                                    </tr>
                                  </table>
                                </div></td>
                            </tr>
                          
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
<script type="text/javascript">
$.fn.raty.defaults.path = 'images';

  $(function() {
    
    $('#quality_rating1').raty({
                                scoreName:'quality_rating',
                                 score: function() {
                                    return $(this).attr('data-score');
                                  }
                              });
    $('#freshness_rating1').raty({scoreName:'freshness_rating',
                                 score: function() {
                                    return $(this).attr('data-score');
                                  }});
    $('#trustworthiness_rating1').raty({scoreName:'trustworthiness_rating',
                                 score: function() {
                                    return $(this).attr('data-score');
                                  }});
    $('#pricing_rating1').raty({scoreName:'pricing_rating',
                                 score: function() {
                                    return $(this).attr('data-score');
                                  }});
    $('#packing_rating1').raty({scoreName:'packing_rating',
                                 score: function() {
                                    return $(this).attr('data-score');
                                  }});
  }); 

  
</script>
