<script type="text/javascript" src="../assets/js/sliding_effect.js"></script>

<?php
// PO # 3

if ($_SESSION['uid'] != 1) {
    $sel_rights = "SELECT rights from admin where id='" . $_SESSION['uid'] . "'";
    $rs_rights = mysqli_query($con,$sel_rights);
    $rights = mysqli_fetch_array($rs_rights);

    $p = $rights["rights"];
} else {
    $p = "All";
}
?>

<td width="210" valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
            <td><img src="images/images_front/admin-menu-top.gif" border="0"/></td>
        </tr>

        <tr>

            <td background="images/images_front/admin-menu-middle.gif" style="background-repeat:repeat-y;" valign="top">

                <table id="sliding-navigation" align="left" class="menutable">

                    <tr>
                        <td height="10" class="sliding-element"></td>
                    </tr>
                    <?php
                    $pos = strchr($p, "Manage Buyers");
                    if ($p == 'All' || $pos != false) {
                        ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="buyer_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Buyers</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                    

                    $pos = strchr($p, "Manage Invoice");
                    if ($p == 'All' || $pos != false) {
                        ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="invoice_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Invoice</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }
                    
                    $pos = strchr($p, "Manage Invoice Duties");
                    if ($p == 'All' || $pos != false) {
                        ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="invoice_mgmt_duties.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Invoice Duties</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                    
                    
                    $pos = strchr($p, "Growers Orders");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="growers_orders_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Growers Orders</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }
                    
                    $pos = strchr($p, "Confirm Orders");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="growers_confirm_orders.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Confirm Orders</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }  


                    
                    $pos = strchr($p, "Upload Invoice");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                        <li class="sliding-element" id="menulink"><a href="upload_florana_csv.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Upload Invoice</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    } 
                    
                    $pos = strchr($p, "Manage Other Fact");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="buy_flowers_request_fact.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Billing in Batch</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }  
                    
                    
                    $pos = strchr($p, "Manage Report");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="buy_flowers_request_report.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Reports</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                                                                
                    
                    $pos = strchr($p, "Manage Report Vday");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="buy_flowers_request_report_vday.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Reports Vday</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                                                                
                    
                    $pos = strchr($p, "Manage Other");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="buy_flowers_request_others.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Aditionals</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                                                                
                    
                    
                    
                    
                    
                    
                    $pos = strchr($p, "Manage Packing List");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="packing_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Packing List</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                        
                    
                    $pos = strchr($p, "Manage Offer");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="buy_flowers_request.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Offer</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                                            
                    
                    $pos = strchr($p, "Comment");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="coment_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Comment</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                                                                
                    
                    $pos = strchr($p, "BoxGrower");
                    if ($p == 'All' || $pos != false) {
                            ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="box_grower_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Box Grower</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }                                                                                                    
                    ?>

                        
                </table>


            </td>

        </tr>

        <tr>
            <td height="10"><img src="images/images_front/admin-menu-bottom.gif" border="0"/></td>
        </tr>

    </table>
</td>
