<?php	
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
	
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}
		
	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM buyers WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
	}

	$qsel="select id , first_name , last_name , active ,shipping
                 from buyers  ";
	$rs=mysqli_query($con,$qsel);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});

			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Manage Buyers</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>
                  
                  
                  		

				  <tr>

                    <td>

					<table width="100%">

					<tr>

					<td>

					<a href="buyer_add.php" class="pagetitle" onclick="this.blur();"><span> + Add New Buyer</span></a>

					</td>

					</tr>

					</table>

					</td>

                  </tr>
                  
                  
                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

	  <tr>
        
	    <th width="18%" align="left">Name</th> 
            
            <th width="15%" align="left">Standig Order Generate</th> 
          
            <th width="15%" align="left">Shipping</th>             
            
            <th width="8%" align="left">Customer Invoice</th> 
                      
            <th align="center" width="9%">Status</th>
           
            <th align="center" width="8%">Edit</th>

	    <th align="center" width="9%">View</th> 

	 </tr>

	</thead>

	<tbody>

		<?php

		$sr=1;

			while($product=mysqli_fetch_array($rs))

			  {
						  $shipping_method=trim($product["shipping"],",");
                                                  
			     if($shipping_method>0)      {
		                       $select_shipping_info="select name,description from shipping_method where id='".$shipping_method."'";
		                       $rs_shipping_info=mysqli_query($con,$select_shipping_info);
		                       $shipping_info=mysqli_fetch_array($rs_shipping_info);
	                      }

                               	       $sel_control="select id , descripcion, time  
                                                                from general_parameters
                                                               where id = 2  ";
                                       
		                       $rs_control =mysqli_query($con,$sel_control);
		                       $control=mysqli_fetch_array($rs_control);
                                       
                                       $swc = $control["time"];
						  ?>

                        <tr class="gradeU">

                                <td class="text" align="left"><?php echo $product["first_name"]?> <?=$product["last_name"]?></td>
                          
                                <td class="text" align="center"><a href="buy_standing_order.php?id=<?php echo $product["id"]?> "  >Standing Order </a> </td>                                
                                
                                <td class="text" align="left"><a href="shipping.php?id=<?php echo $product["id"]?>" style="color:#000; font-weight:bold;" >Shipping</a> </td>
                          
                          <?php  if($swc==1)      {?>
                                         <td class="text" align="center"><a href="upload_invoice_client_qb.php?id=<?php echo $product["id"]?> "  >Invoice </a> </td>
                          <?php  }else{ ?>
                                         <td class="text" align="center">INVOICE </td>
                         <?php   }        ?>

                                                      
                                <td class="text" align="left"><?php if($product["active"]=='deactive') { echo "Deactive"; } else if($product["active"]=='active') { echo "Active"; } else if($product["active"]=='demo') { echo "Demo"; } else if($product["active"]=='suspended') { echo "Suspended"; } ?> </td>

                                <td align="center" ><a href="buyer_edit.php?id=<?php echo $product["id"]?>" style="color:#000; font-weight:bold;" >Edit</a></td>
                                
                                <td align="center" ><a href="../saveloginBuyer.php?id=<?php echo $product["id"]?>" style="color:#000; font-weight:bold;" >View</a></td>
                                
                          
                          </tr>

						 <?php

						 		$sr++;

						 	}

						 ?> 

	

	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
