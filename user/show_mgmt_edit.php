<?php   
	include "../config/config_gcp.php";
        
        $fact_number = $_GET['numf'];
        $numfac      = $_GET['id_fact'];
        $grow_id     = $_GET['id_grow'];
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
    $ins = "update invoice_packing_box 
                   set ship_cost   = '" . $_POST["ship_cost"] . "'  
             where id = '" . $fact_number . "'   ";       
    
    if(mysqli_query($con, $ins))
            
        header("location:show_box_mgmt.php?id_fact=".$numfac."&id_grow=".$grow_id);
    
}

     $sel_packing_cab = "select id           , id_fact      , id_order        , order_number     , order_serial , 
                                offer_id     , product      , prod_name       , buyer            , grower_id    , 
                                box_qty_pack , qty_pack     , qty_box_packing , box_type         , comment      ,
                                date_added   , box_name     , size            , steams           , price        , 
                                cliente_id   , ship_cost    , cost_cad        , price_cad        , gorPrice     , 
                                duties       , handling_pro , total_duties    , processed        , id_refer     ,
                                price_quick  , weight
                           from invoice_packing_box 
                          where id  = '" . $fact_number . "'    ";

    $rs_packing_cab = mysqli_query($con, $sel_packing_cab);
    $row = mysqli_fetch_array($rs_packing_cab);   
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();                
                
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()  {
                
                if (trim(document.frmcat.ship_cost.value) == "")  {
                    document.getElementById("lblship").innerHTML = "Please enter Ship Cost ";
                    return false;
                }else {
                    document.getElementById("lblship").innerHTML = "";
                    return true;
                }                                                               
            }
                        
        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Packing Box</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="show_box_mgmt.php" onclick="this.blur();"><span> Packing Box</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="show_mgmt_edit.php?numf=<?php echo $row["id"]."&id_fact=".$row["id_fact"]."&id_grow=".$row["grower_id"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                                                                </tr>

                                                                                <!--tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Shipping Cost </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="ship_cost" id="ship_cost" value="<?php echo $row["ship_cost"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblship"></span></td>
                                                                                </tr-->
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      

                                                                <tr>
                                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Shipping Cost</td>
                                                                        <td width="66%" bgcolor="#f2f2f2">
                                                                        <select name="ship_cost" class="listmenu">
                                                                        <option value="">--Select--</option>

                                                                        <?php
                                                                                $sel_ship="select id as idship    , id_fact       , id_order   , order_serial , buyer      , 
                                                                                                grower_id       , box_qty_pack  , box_type   , comment      , date_added , 
                                                                                                order_number    , Piezas        , peso       , box_weight   , Dist       , 
                                                                                                Cost            , price_per_box , Stems      , price_st_bun , gyp        , 
                                                                                                price_st_bunGyp , category      , weight 
                                                                                                from invoice_packing_cab   
                                                                                                where id_fact = '".$numfac."' 
                                                                                            order by comment   " ;
                                
                                                                                            $rs_ship = mysqli_query($con,$sel_ship);
                                                                                
                                                                                while($codesh=mysqli_fetch_array($rs_ship)){
                                                                        ?>
                                                                        
                                                                                     <option value="<?php echo $codesh["Cost"]?>">
                                                                                            <?php echo $codesh["comment"]." - ".$codesh["Stems"]." - ".$codesh["Piezas"]." - ".$codesh["peso"]." - ".$codesh["box_weight"]." - ".$codesh["Dist"]." - ".$codesh["price_per_box"]." - ".$codesh["Cost"] ?>
                                                                                     </option>                                                                        

                                                                        <?php 	 }  ?> 
                                                                        </select>
                                                                        <br>
                                                                            <span class="error" id="lblship"></span>	
                                                                        </td>
                                                                </tr> 
                                                                                
                                                                                
                                                                                
                                                                                





                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>
                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>