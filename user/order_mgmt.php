<?	
	include "../config/config.php";
	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}

	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM `order` WHERE  order_id= '.(int)$_GET['delete'];
	  mysql_query($query);
	  
	  $query_orderitem = 'DELETE FROM `order_item` WHERE  order_id= '.(int)$_GET['delete'];
	  mysql_query($query_orderitem);
	
	}
	
	$sel_ord="select * from `order` order by order_id DESC";
	$rs_ord=mysql_query($sel_ord);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			(function($) {
			/*
			 * Function: fnGetColumnData
			 * Purpose:  Return an array of table values from a particular column.
			 * Returns:  array string: 1d data array 
			 * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function
			 *           int:iColumn - the id of the column to extract the data from
			 *           bool:bUnique - optional - if set to false duplicated values are not filtered out
			 *           bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)
			 *           bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array
			 * Author:   Benedikt Forchhammer <b.forchhammer /AT\ mind2.de>
			 */
			$.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
				// check that we have a column id
				if ( typeof iColumn == "undefined" ) return new Array();
				
				// by default we only wany unique data
				if ( typeof bUnique == "undefined" ) bUnique = true;
				
				// by default we do want to only look at filtered data
				if ( typeof bFiltered == "undefined" ) bFiltered = true;
				
				// by default we do not wany to include empty values
				if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
				
				// list of rows which we're going to loop through
				var aiRows;
				
				// use only filtered rows
				if (bFiltered == true) aiRows = oSettings.aiDisplay; 
				// use all rows
				else aiRows = oSettings.aiDisplayMaster; // all row numbers
			
				// set up data array	
				var asResultData = new Array();
				
				for (var i=0,c=aiRows.length; i<c; i++) {
					iRow = aiRows[i];
					var aData = this.fnGetData(iRow);
					var sValue = aData[iColumn];
					
					// ignore empty values?
					if (bIgnoreEmpty == true && sValue.length == 0) continue;
			
					// ignore unique values?
					else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;
					
					// else push the value onto the result data array
					else asResultData.push(sValue);
				}
				
				return asResultData;
			}}(jQuery));
			
				$(document).ready(function() {
				
					
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sScrollXInner":"120%",
					"sScrollX": "100%",
					"sPaginationType": "full_numbers"
				});
				
				/* Add a select menu for each TH element in the table footer */
				$("tfoot th").each( function ( i ) {
					//this.innerHTML = fnCreateSelect( oTable.fnGetColumnData(i) );
					$('select', this).change( function () {
						oTable.fnFilter( $(this).val(),5);
					} );
				} );
			} );
</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <? include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <? include("includes/left.php");?>
        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10">&nbsp;</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                    <td height="5"></td>
                  </tr>
                  <tr>
                    <td class="pagetitle">Manage Product Categories</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
				  <tr>
                    <td>
					<table width="100%">
					<tr>
					<td>
					<a href="category_add.php" class="pagetitle" onclick="this.blur();"><span> + Add New Category  </span></a>
					</td>
					</tr>
					</table>
					</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><div id="box">
		<div id="container">			
			<div class="demo_jui">
			<table cellpadding="0" cellspacing="0" border="0" class="display" >
				<tbody>
				</tbody>
				<tfoot>
		    <tr>
			<th colspan="6" align="right" class="brown">Filter by Status :
			 <select name="parentmenu" class="searchcombo">
											  	<option value="">-- All Orders --</option>
												<option value="Payment Received">Payment Received</option>
												<option value="Unpaid">Unpaid</option>
												<option value="Order In Process">Order In Process</option>
												<option value="Order Shipped">Order Shipped</option>
												<option value="Partially Shipped">Partially Shipped</option>
												<option value="Order Delivered">Order Delivered</option>
												<option value="Order Cancelled">Order Cancelled</option>
											  </select></th>
		 </tr>
		 <tr>
		 <td colspan="6">&nbsp;</td>
		 </tr>
	</tfoot>
			</table>
<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#DACCB6">
	<thead>
		<tr>
			<th width="6%" align="center">Sr.</th>
			<th width="10%" align="center">Order ID </th>
			<th width="26%">Order From </th>
			<th width="13%">Order Date </th>
			<th width="15%">Order Amount </th>
			<th width="15%">Status</th>
			<th align="center" width="7%">View</th>
			<th align="center" width="8%">Delete</th>
		</tr>
	</thead>
	<tbody>
		
						 <?
						  $sr=1;
						  while($ord=mysql_fetch_array($rs_ord))
						  {
						  	$odt=explode("-",$ord["order_date"]);
							$orderdate=$odt["2"]."-".$odt["1"]."-".$odt["0"];
						  ?>
                        <tr class="gradeU">
                          <td align="center" class="text"><?=$sr;?></td>
                          <td align="center" class="text"><?=$ord["order_id"]?></td>
                          <td class="text"><a href="order_view.php?id=<?=$ord["order_id"]?>" class="text11"><?=$ord["cust_name"]?></a></td>
                          <td class="text"><?=$orderdate;?></td>
						    <td class="text"><?=$ord["grandtotal"]?> ( In <?=$currency?> )</td>
						    <td class="text"><?=$ord["status"]?></td>
						    <td align="center"  title="View Order"><a href="order_view.php?id=<?=$ord["order_id"]?>"><img src="images/view.gif" border="0" alt="View Order" /></a></td>
                          <td align="center" title="Delete Order"><a href="?delete=<?=$ord["order_id"]?>" onclick="return confirm('Are you sure you want to Delete this order ?');"><img src="images/delete.gif" border="0" alt="Delete Order" /></a></td>
                          </tr>
						  
						  
						 <?
						 		$sr++;
						 	}
						 ?> 
	</tbody>
</table>
			</div>
			</div>
			</div>
		</td>
                  </tr>
                </table></td>
                <td width="10">&nbsp;</td>
              </tr>
            </table></td>
            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>
          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>
          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <? include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
