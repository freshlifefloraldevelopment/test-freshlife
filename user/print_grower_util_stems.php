<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
   
    $idbuy    = $_GET['id_buy'];      
    $id_order = $_GET['b'];   
    
function calculateKilo($userSessionID,$con){
   $getBuyerShippingMethod = "select shipping_method_id
                              from buyer_shipping_methods
                             where buyer_id ='" . $userSessionID . "'";

  $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
  $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
   $shipping_method_id = $buyerShippingMethod['shipping_method_id'];

     $getShippingMethod = "select connect_group
                             from shipping_method
                            where id='" . $shipping_method_id . "'";

      $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
      $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

       $temp_conn = explode(',', $shippingMethodDetail['connect_group']);

      $id_conn = $temp_conn[1];  // Default

      $getConnect = "select charges_per_kilo
                       from connections
                      where id='" . $id_conn . "'";

      $rs_connect = mysqli_query($con, $getConnect);

      $charges = mysqli_fetch_assoc($rs_connect);

       /////////////////////////////////////////////////////////////

      $cost = $charges['charges_per_kilo'];
      $cost_un = unserialize($cost);

      $cost_sum = 0;

      foreach ($cost_un as $key => $value) {
          $cost_sum = $cost_sum + $value;
      }

      return $charges_per_kilo_trans =  $cost_sum;
}    

     // Datos del Buyer orders
     $buyerOrder = "select id,order_number    , order_date      , shipping_method , del_date        , date_range      ,
                           is_pending      , order_serial    , seen            , delivery_dates  , lfd_grower    ,  
                           qucik_desc    
                      from buyer_orders  
                     where id = '" . $id_order . "'  "     ;
     
    $buyerO = mysqli_query($con, $buyerOrder);
    $buyOrder = mysqli_fetch_array($buyerO);   
    
    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);      
   
   // Datos de la Orden  DOH
   
   $sqlDetalis=" select gor.id as gorid           , br.price_front,cost_transp,
                        gor.product as name       , 
                        gor.size    as size_name  ,
                        gor.price                 ,
                        gor.steams                , 
                        gor.bunchqty              , 
                        gor.product_subcategory   ,
                        g.growers_name as gname   ,
                        p.name as prodName        ,
                        s.name as subcate         ,
                        br.price_front            ,
                        br.cost_transp            ,
                        sz.id , sz.name as sizereq,
                        br.qty                    ,
                        s.name as subcate_name    ,
                        p.subcategoryid           ,
                        ipx.size                  ,					 
                        ipx.steams                ,					                         
                        ipx.qty_pack as bunchqty  ,
                        ipx.duties       , 					 
                        ipx.ship_cost    ,					 
                        ipx.handling_pro ,  
                        ipx.price_cad    ,
                        ipx.box_packing  ,  
                        ipx.truck        ,                            
                        ipx.total_duties
                   from grower_offer_reply gor
                  inner join growers g          on gor.grower_id   = g.id
                  inner join buyer_requests br  on gor.request_id  = br.id
                  inner join product p          on br.product      = p.id
                  inner join subcategory s      on p.subcategoryid = s.id
                  inner join sizes sz           on br.sizeid       = sz.id                                    
                  inner join invoice_packing ip on (gor.grower_id = ip.grower_id and gor.id = ip.id_grower_offer )
                   left join invoice_packing_box ipx  on ip.id = ipx.id_order                                 
                  where gor.buyer_id = '" . $idbuy . "' 
                    and br.id_order  = '" . $id_order . "'  ";

        $result   = mysqli_query($con, $sqlDetalis);            

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'PRICE VARIATION',0,0,'L'); 
    $pdf->Image('logo.png',148,5); 
    $pdf->Ln(8);    
    
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Client Details ',0,0,'L');
    $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'Company Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,'Requests: '.$buyOrder['id']." ".$buyOrder['qucik_desc'],0,1,'L');           
    $pdf->Ln(5);    
    
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Stems',0,1,'L');    
    
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);   
    
    $tasaKilo = calculateKilo($idbuy,$con);
    $totDiffPr = 0;
    $totDiffFr = 0;
    $totDiff   = 0;    
            
    while($row = mysqli_fetch_assoc($result))  {
        
          $totGrowerDiff = $row['bunchqty']*$row['steams'];
                      
        // Price Grower Parameter
     $sel_priceFin = "select a1.price_adm ,a1.size,sz.name,a1.factor
                        from grower_parameter a1
                        left join sizes sz   ON a1.size=sz.id                  
                       where a1.idsc = '" . $row['subcategoryid'] . "'  
                         and sz.name = '" . $row['size_name'] . "'    ";
        
        $rs_priceFin = mysqli_query($con,$sel_priceFin);       
        $tmpPrice = mysqli_fetch_array($rs_priceFin);
        
        ////////////////////////////////////////////////////////////////
        
            $priceBunch = $tasaKilo * $tmpPrice['factor'];
            
            $priceSteam = $priceBunch / $totGrowerDiff;
            
            
           // $priceCalculado = sprintf('%.2f',round($priceSteam + $tmpPrice['price_adm'],2));  
           // $priceSteamR = sprintf('%.2f',round($priceSteam ,2));  
            
            $priceCalculado = $row['price_front'];            
            $priceSteamR    = $row['cost_transp'];            
            

                
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $row['idprod'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
        
              if ($bunch_stem['box_type'] == 0) {
                    $unitFac = "Stems";                     
              }else{
                    $unitFac = "Bunch";                   
              }                
         
                $pacFreight = $row['ship_cost'];
              
               $priceSintransporte = $priceCalculado-$priceSteamR;
              
            $pdf->SetFont('Arial','B',8);   
                $pdf->Cell(70,4,"OFFER: ".$row['product_subcategory']." ".$row['name']." ".$row['size_name']." cm. ",0,0,'L');            
                $pdf->Cell(25,4,$row['bunchqty']*$row['steams'],0,1,'L');                                                                                   

            $pdf->SetFont('Arial','',8);           
                $pdf->Cell(70,4,"REQUESTS:".$row['subcate_name']." ".$row['prodName']." ".$row['sizereq']." cm. ",0,0,'L');                                                                          
                $pdf->Cell(25,4,$row['qty'],0,1,'L');      

            $pdf->Ln(4);
                
            $pdf->SetFont('Arial','B',8);           
                $pdf->Cell(70,4,"PRICE Front-End",0,0,'L');                                                                          
                $pdf->Cell(25,4,"PRICE Grower",0,0,'L');                      
                $pdf->Cell(25,4,"TOTAL (Dif.)",0,1,'L');                                      
                
            $pdf->SetFont('Arial','',8);           
                $pdf->Cell(70,4,number_format($priceSintransporte, 2, '.', ','),0,0,'L');                
                $pdf->Cell(25,4,number_format($row['price'], 2, '.', ','),0,0,'L');                
                $pdf->Cell(35,4,number_format(($priceSintransporte-$row['price'])*$totGrowerDiff, 2, '.', ','),0,1,'L');                              
                
           $pdf->Ln(2);                
                
            $pdf->SetFont('Arial','B',8);           
                $pdf->Cell(70,4,"Freight",0,0,'L');                                                                          
                $pdf->Cell(25,4,"Packing-Freight",0,0,'L');                      
                $pdf->Cell(25,4,"Freight (Dif.)",0,1,'L');        
                
            $pdf->SetFont('Arial','',8);           
                $pdf->Cell(70,4,$priceSteamR,0,0,'L');                                                                          
                $pdf->Cell(25,4,number_format($pacFreight, 2, '.', ','),0,0,'L');                
                $pdf->Cell(35,4,($priceSteamR-$pacFreight)*$totGrowerDiff,0,1,'L'); 
                
                
                // Total Parcial ///////////////////////////////////////////////////////////////
                $pdf->SetFont('Arial','B',10);
                   $totDiffPartial = (($priceSintransporte-$row['price'])+($priceSteamR-$pacFreight)) * $totGrowerDiff;
                   $pdf->Cell(70,4,"                                              ",0,1,'L');                   
                   $pdf->Cell(70,4,"                                                                     Total Diff...:    ".$totDiffPartial,0,1,'L');                                                                                      
                
            $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');                     
            
                    $totDiffPr = $totDiffPr + (($priceSintransporte-$row['price']) * $totGrowerDiff);
                    $totDiffFr = $totDiffFr + (($priceSteamR-$pacFreight) * $totGrowerDiff);
                    $totDiff = $totDiff + (( ($priceSintransporte-$row['price'])+($priceSteamR-$pacFreight)) * $totGrowerDiff);
            }
                        $pdf->Ln(4);
                 $pdf->SetFont('Arial','B',14);  
                 
      $pdf->Cell(70,4,"Total Difference Price....:    ".$totDiffPr,0,1,'L');                                                                                      
      $pdf->Ln(5);          
      $pdf->Cell(70,4,"Total Difference Freight.:    ".$totDiffFr,0,1,'L');   
      $pdf->Ln(5);    
      $pdf->Cell(70,4,"Total Difference.............:    ".$totDiff,0,1,'L');                                                                                      
                   
  $pdf->Output();
  ?>