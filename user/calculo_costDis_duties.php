<?php
  
include("../config/config_gcp.php");

$id_order     = $_GET['id_fact'];
$buyer_ofer   = $_GET['id_buy'];
$delNow       = date('Y-m-d');
  

// CABECERA ORDERS
$query_cab = "select id_fact         , buyer_id         , order_number    , order_date  , shipping_method , 
                     del_date        , date_range       , is_pending      , order_serial, seen            , 
                     delivery_dates  , lfd_grower       , quick_desc      , bill_number , gross_weight    , 
                     volume_weight   , freight_value    , guide_number    , total_boxes , sub_total_amount, 
                     tax_rate        , shipping_charge  , handling        , grand_total , bill_state      , 
                     date_added      , user_added       ,
                     air_waybill     , charges_due_agent, credit_card_fees, per_kg      ,
                     handling_lax    , brokerage_lax    , tot_shiping_imp_cost          , cad             ,
                     duties          , total_cost       , price_cad                     ,
                     porce_duties    , porce_handling   , box_packing                   , truck
                from invoice_orders  
               where id_fact  = '" . $id_order   . "'  
                 and buyer_id = '" . $buyer_ofer . "'  ";

    $fact_ord    = mysqli_query($con, $query_cab);
    $factura_cab = mysqli_fetch_assoc($fact_ord);
    

// Box Cab Totales
$sqlBoxcabTot = "select sum(Peso) as peso
                   from invoice_packing_cab  
                  where id_fact = '" . $id_order   . "'  
                    and buyer   = '" . $buyer_ofer . "'  ";

$result_cabTot   = mysqli_query($con, $sqlBoxcabTot); 
$TotalesBox = mysqli_fetch_assoc($result_cabTot);
$pesoTotal = $TotalesBox['peso'];

//////////////////////////////////////////////////////////////////////////////

$precioKilo = $factura_cab['tot_shiping_imp_cost']/$factura_cab['gross_weight'] ;
$cotizacion = $factura_cab['cad'];
$duties_por = $factura_cab['porce_duties'];
$handli_por = $factura_cab['porce_handling'];

$pack_box      = $factura_cab['box_packing'];
$freight_truck = $factura_cab['truck'];


///////////////////////////////////////////////////////////////
// CABECERA GOWERS
// CABECERA

$sqlDetalis = "select id            , id_fact       , id_order   , order_serial , buyer      , 
                      grower_id     , box_qty_pack  , box_type   , comment      , date_added , 
                      order_number  , Piezas        , Peso       , box_weight   , Dist       , 
                      Cost          , price_per_box , Stems      , price_st_bun , gyp        ,
                      category
                 from invoice_packing_cab  
                where id_fact = '" . $id_order   . "'  
                  and buyer   = '" . $buyer_ofer . "'  
                order by comment ";        

$resultDet   = mysqli_query($con, $sqlDetalis); 

        
while($rowpeso = mysqli_fetch_array($resultDet))  {
    
    $totalPrecio = $rowpeso['Peso']*$precioKilo;
    $porCaja     = $totalPrecio/$rowpeso['Piezas'];
    $shipping    = $porCaja/$rowpeso['box_weight'];  
               
                        $set_dist3 = "UPDATE invoice_packing_cab 
                                        SET Dist          ='" . $totalPrecio   . "' ,
                                            price_per_box ='" . $porCaja   . "'     ,
                                            Cost          ='" . $shipping   . "'     
                                      WHERE id ='" . $rowpeso['id'] . "' ";

                        mysqli_query($con,$set_dist3);   
                        
                        
                        $set_dist4 = "UPDATE invoice_packing_box 
                                        SET ship_cost = '" . $shipping   . "' 
                                      WHERE id_refer  = '" . $rowpeso['id'] . "' 
                                        AND grower_id = '" . $rowpeso['grower_id'] . "'  ";

                        mysqli_query($con,$set_dist4);                                                                          
}


// Total Tallos //////////////////////////////////

   $sqlStems = "select g.growers_name , 
                     gr.id as grid  , gr.bunchqty    , 
                     gr.steams      , gr.offer_id    ,                      
                     gr.price       , gr.product     ,
                     gr.request_id  , gr.cliente_id  ,                     
                     gr.product_subcategory as subs  ,                            
                     (ipx.steams*ipx.qty_pack) as totstems , 
                     f.name as features ,                      
                     scl.name as client_name,
                     p.box_type
                from grower_offer_reply gr 
               inner join buyer_requests br  on gr.offer_id     = br.id 
               inner join product p          on gr.product      = p.name 
                left join subcategory s      on p.subcategoryid = s.id
               inner join growers g          on gr.grower_id    = g.id 
               inner join invoice_packing ip on (gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  on ip.id = ipx.id_order               
                left join sub_client scl           on ipx.cliente_id = scl.id
                left join sub_client_branch bra    on ipx.branch = bra.id
                left join features f               on br.feature = f.id                
               where gr.buyer_id='" . $buyer_ofer . "'
                 and gr.offer_id >= '6731'
                 and ip.id_fact = '" . $id_order . "'                 
                 and reject in (0) 
                 and g.id is not NULL 
               group by ipx.id    ";

$resultStems   = mysqli_query($con, $sqlStems); 

        
while($rowstems = mysqli_fetch_array($resultStems))  {   
 
                        $tot_tems = $tot_tems + ($rowstems['totstems'] ) ;                                                                                    
}

 

// DETALLE //////////////////////////////////

$Subtotal = 0;

   $sqlReq = "select g.growers_name , 
                     gr.id as grid  , gr.bunchqty    , 
                     gr.steams      , gr.offer_id    ,                      
                     gr.price       , gr.product     ,
                     gr.request_id  , gr.cliente_id  ,                     
                     gr.product_subcategory as subs  ,                            
                     (ipx.steams*ipx.qty_pack) as stemsvic , 
                     f.name as features ,                      
                     scl.name as client_name ,
                     p.image_path as img_url , 
                     s.name as subs1,                     
                     bra.name as branch_name,                     
                     br.id_order , 
                     br.feature  , 
                     ipx.grower_id    , ipx.size         ,
                     ipx.box_name     , ipx.qty_pack     ,
                     ipx.id as idbox  , ipx.price_cad    ,
                     ipx.id as ipxid  , ipx.duties       , 
                     ipx.handling_pro , ipx.total_duties ,
                     ipx.ship_cost as ship_stem,                     
                     ip.price as gorPrice ,
                     ip.weight , 
                     ip.product as prod_pack
                from grower_offer_reply gr 
               inner join buyer_requests br  on gr.offer_id     = br.id 
               inner join product p          on gr.product      = p.name 
                left join subcategory s      on p.subcategoryid = s.id
               inner join growers g          on gr.grower_id    = g.id 
               inner join invoice_packing ip on (ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  on ip.id = ipx.id_order               
                left join sub_client scl           on ipx.cliente_id = scl.id
                left join sub_client_branch bra    on ipx.branch = bra.id
                left join features f               on br.feature = f.id                
               where gr.buyer_id='" . $buyer_ofer . "'
                 and gr.offer_id >= '6731'
                 and ip.id_fact = '" . $id_order . "'                                  
                 and reject in (0) 
                 and g.id is not NULL 
               group by ipx.id   ";

$resultPrice   = mysqli_query($con, $sqlReq); 
        
while($rowprice = mysqli_fetch_array($resultPrice))  {              
    
    
        $FactorPack  = $pack_box/$tot_tems;        
        $FactorTruck = $freight_truck/$tot_tems;
                
        $pricepack  = $FactorPack  ;
        $pricetruck = $FactorTruck ;    
    
    
        ////////////////////////////////////
        
        $duties_cal  = $rowprice['gorPrice'] * $duties_por/100;
                        
        $Subtot_ship = $rowprice['ship_stem'];
        
        $Subtot_hand = ($rowprice['gorPrice'] + $duties_cal + $Subtot_ship)* $handli_por/100;
        
        $Total_final = $rowprice['gorPrice']  + $duties_cal + $Subtot_ship + $Subtot_hand + $pricepack + $pricetruck;
        
        $Total_cad   = $Total_final/$cotizacion;
        

        
                                            
                            $set_cad_box  = "UPDATE invoice_packing_box 
                                               SET weight       ='" . $rowprice['weight']. "' ,   
                                                   gorPrice     ='" . $rowprice['gorPrice']. "' ,   
                                                   duties       ='" . $duties_cal   . "' ,   
                                                   handling_pro ='" . $Subtot_hand  . "' ,                                                          
                                                   total_duties ='" . $Total_final  . "' ,                                                         
                                                   price_cad    ='" . $Total_cad    . "' ,
                                                   box_packing  ='" . $pricepack  . "'   ,                                                         
                                                   truck        ='" . $pricetruck . "' 
                                             WHERE id   ='" . $rowprice['ipxid'] . "'    ";

                            mysqli_query($con,$set_cad_box);                          
         
} 

         header("location:invoice_mgmt_duties.php");       
?> 