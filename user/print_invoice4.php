<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name 
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests

   $sqlDetalis="select rg.id,rg.growers_name,
                       sum(ir.steams * ir.bunchqty ) as steams,
                       sum(boxqty) as bultos,
                       count(*) as reg
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 INNER JOIN product p      ON ir.product = p.id                  
                 INNER JOIN category c     ON c.id = p.categoryid                  
                 INNER JOIN subcategory sc ON sc.id = p.subcategoryid                  
                 where ir.buyer    = '" . $buyer_cab . "'
                   and ir.id_fact  = '" . $id_fact_cab . "'
                 group by rg.growers_name                       
                 order by rg.growers_name";   
        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    
    $pdf->AddPage();
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'PRE-ALERT ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'Consignee...: '.$buy['first_name']." ".$buy['last_name'],0,0,'L');
    $pdf->Cell(0,6,'P.O.NUMBER: '.$buyerOrderCab['order_number'],0,1,'R');  
       
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');        
    
    $pdf->Cell(70,6,'Date: '.date("m-d-Y"),0,1,'L');  
    
    $pdf->Ln(10);

    $pdf->Cell(25,6,'Id',0,0,'L');
    $pdf->Cell(25,6,'Grower',0,0,'C');
    $pdf->Cell(50,6,'Steams',0,0,'C');
    $pdf->Cell(25,6,'Bultos',0,0,'C');
    $pdf->Cell(50,6,'Reg.',0,1,'C'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
        
    while($row = mysqli_fetch_assoc($result))  {
         
                $pdf->Cell(25,4,$row['id'],0,0,'L');            
                $pdf->Cell(25,6,$row['growers_name'],0,0,'L');                              
                $pdf->Cell(50,6,$row['steams'],0,0,'C');                              
                $pdf->Cell(25,6,$row['bultos'],0,0,'C');                                       
                $pdf->Cell(50,6,$row['reg'],0,1,'C');                                 
                
                     $totalCal = $totalCal + $row['bultos'] ;
                     $totalStems = $totalStems + $row['steams'] ;                     
         
    }

    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',10);            
    $pdf->Cell(115,6,'Total   :    '.number_format($totalStems, 0, '.', ',')."                              ".number_format($totalCal, 0, '.', ','),0,1,'R');
    
  $pdf->Output();
  ?>