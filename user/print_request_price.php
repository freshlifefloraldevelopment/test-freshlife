<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $idbuy = $_GET['id_buy'];    
    
    $id_order = $_GET['b'];    


    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id = b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy   = mysqli_fetch_array($buyer);
      
    $sqlbo = "select id , qucik_desc,join_order,del_date
                      from buyer_orders 
                     where id = '" . $id_order . "'         " ;
     
    $buyerbo = mysqli_query($con, $sqlbo);
    $buybo   = mysqli_fetch_array($buyerbo);    
    
    ///////////////////////////////// MISING ////////////////////////////////////////////////////////
       $sqlmissing="select unseen,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order ,                        
                       br.product,
                       p.name , 
                       p.box_type ,                        
                       br.qty ,  
                       br.comment,
                       br.noofstems,
                       br.lfd ,
                       sizeid ,
                       sz.name as size_name,
                       g.growers_name      ,
                       c.name as colorname ,
                       sc.name as subcate  ,
                       IFNULL((select sum(gor.bunchqty * gor.steams) from grower_offer_reply gor where request_id = br.id and buyer_id = buyer),0) as ofertas
                  from buyer_requests br
                  LEFT JOIN product p       ON br.product      = p.id
                  LEFT JOIN subcategory sc  ON p.subcategoryid = sc.id
                  LEFT JOIN buyers b        ON br.buyer        = b.id
                  LEFT JOIN buyer_orders bo ON br.id_order     = bo.id
                  LEFT JOIN sizes sz        ON br.sizeid       = sz.id  
                  LEFT JOIN colors c        ON p.color_id      = c.id
                  left JOIN growers g       ON br.id_grower    = g.id                                                  
                 where br.buyer    = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
                 order by sc.name,c.name,(br.qty*br.noofstems) desc ";

        $missing   = mysqli_query($con, $sqlmissing);   
        
    $pdf = new PDF();   
    $pdf->AddPage();
    
        $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'REQUEST VS PRICE ',0,0,'L'); 
    
    $pdf->Ln(10);    
                
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,$buy['company'],0,1,'L');  
    $pdf->Cell(20,6,'Order: '.$buybo['id'],0,1,'L');  
    $pdf->Cell(40,6,'Delivery date: '.$buybo['del_date'],0,1,'L');  
    
    $pdf->Ln(10);
    
    $pdf->SetFont('Arial','B',30);    
    $pdf->Cell(70,10,'MISSING ITEMS',0,0,'L'); 
    
    $pdf->Ln(20);
    $pdf->SetFont('Arial','B',8);    
    $pdf->Cell(70,6,' ',0,0,'L');         
    $pdf->Cell(15,6,'S.ORDER ',0,0,'R');                 
    $pdf->Cell(15,6,'OFFER ',0,0,'R');             
    $pdf->Cell(15,6,'MISSING ',0,1,'R');     
    $pdf->Cell(70,6,'___________________________________________________________________________________________',0,1,'L');      
    
    $pdf->Ln(5);
    
    $totalMisqty  = 0;
    $totalMisofer = 0;
    
    while($mis = mysqli_fetch_assoc($missing))  {
               
         $pdf->SetFont('Arial','B',8);
         
         
                              if ($mis['unseen'] == '0') {                                                                                                          
                                        if ($mis['box_type'] == 0) {
                                            $stemsReq1 = $mis['qty']*$mis['noofstems'];                                    
                                        } else {
                                            $stemsReq1 = $mis['qty'];
                                        }                                                                         
                              }else{
                                    $stemsReq1 = $mis['qty'];
                              }          
  
                    $pdf->Cell(70,6,$mis['subcate']." ".$mis['name']." ".$mis['colorname']." ".$mis['size_name']." cm. ",0,0,'L');            
                    
                    $pdf->Cell(15,6,number_format($stemsReq1, 0, '.', ','),0,0,'R');                       
                    $pdf->Cell(15,6,number_format($mis['ofertas'], 0, '.', ','),0,0,'R');                                           
                    $pdf->Cell(15,6,number_format($stemsReq1-$mis['ofertas'], 0, '.', ','),0,1,'R');   
                          
                    $totalMisqty  = $totalMisqty + $stemsReq1;              
                    $totalMisofer = $totalMisofer + $mis['ofertas'];              
    }    
    
    $pdf->Cell(70,6,'TOTAL...:',0,0,'L');     
    $pdf->Cell(15,6,''.number_format($totalMisqty, 0, '.', ','),0,0,'R');      
    $pdf->Cell(15,6,''.number_format($totalMisofer, 0, '.', ','),0,0,'R');          
    $pdf->Cell(15,6,''.number_format($totalMisqty-$totalMisofer, 0, '.', ','),0,1,'R');  
    
    
    ///////////////////////////////// END MISING ////////////////////////////////////////////////////
   
   // Datos de la Orden  DOH
   
   $sqlDetalis="select unseen,
                br.id as idreq,
                bo.qucik_desc,
                b.first_name ,   
                CAST(br.id_order AS UNSIGNED) idorder,
                br.product,
                p.name , 
                p.box_type ,                        
                br.qty , 
                br.comment,
                br.noofstems,                       
                br.lfd ,
                br.sizeid ,
                sz.name as size_name,
                c.name as colorname ,
                sc.name as subcate  ,
                br.type, br.feature ,
                p.subcategoryid , br.price_front,
                br.cost_transp
            from buyer_requests br
            INNER JOIN product p           ON br.product = p.id
            INNER JOIN subcategory sc      ON p.subcategoryid = sc.id
            INNER JOIN buyers b            ON br.buyer = b.id
            INNER JOIN buyer_orders bo     ON br.id_order = bo.id
            INNER JOIN sizes sz            ON br.sizeid = sz.id  
            LEFT JOIN colors c             ON p.color_id = c.id
            left JOIN growers g            ON br.id_grower = g.id                                                  
            where br.buyer  = '" . $idbuy . "' 
            and br.id_order = '" . $id_order . "' 
            order by p.name ";

        $result   = mysqli_query($con, $sqlDetalis);    
        

    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'REQUEST VS PRICE ',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,$buy['company'],0,1,'L');  
    $pdf->Cell(20,6,'Order: '.$buybo['id'],0,1,'L');  
    
    $pdf->Cell(40,6,'Delivery date: '.$buybo['del_date'],0,1,'L');  
    
    $pdf->Ln(10);
    $cabCount = 1;  
    
// Cabecera
    
    $pdf->Cell(15,6,'',0,0,'L');
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Price',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'R');    
    $pdf->Cell(8,6,' ',0,1,'L');            
   // $pdf->Cell(25,6,'Lfd',0,1,'L');        
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    $totrequest = 0;
    $totoffer   = 0;
    $poceValgen = 0;
    $poceflfgen = 0;
    
    $pocegrogen = 0;
    
    $totCosto=0;
    
    while($row = mysqli_fetch_assoc($result))  {
        
                              if ($row['unseen'] == '0') {                                                                                                          
                                        if ($row['box_type'] == 0) {
                                            $stemsReq = $row['qty']*$row['noofstems'];                                    
                                        } else {
                                            $stemsReq = $row['qty'];
                                        }                                                                         
                              }else{
                                    $stemsReq = $row['qty'];
                              } 
                              
            ///////////////////////////////////////////////////////////////////                  
        
                              if ($row["type"] == "0") {
                                    $market = "S.Order";
                              }else{
                                    $market = "O.Market";
                              }
 
       
           // Datos de la Oferta
   
   $sqlOffer="select gor.marks          ,
                     gor.id             ,  
                     gor.offer_id       , 
                     gor.offer_id_index , 
                     gor.grower_id      , 
                     gor.buyer_id       , 
                     gor.status         , 
                     gor.product        , 
                     gor.price          , 
                     gor.size           , 
                     gor.boxtype        , 
                     gor.bunchsize      , 
                     gor.boxqty         , 
                     gor.bunchqty       , 
                     gor.steams         ,
                     gor.req_group      ,
                     gor.request_id     ,
                     g.growers_name     ,
                     gor.product_subcategory,
                     gor.type_market    ,
                     c.name as colorname                     
                from grower_offer_reply gor
                left JOIN product p ON  gor.product = p.name and gor.product_subcategory = p.subcate_name 
                left JOIN colors c ON p.color_id = c.id
                left JOIN growers g ON gor.grower_id = g.id                                                                  
               where request_id = '" . $row['idreq'] . "'
                 and buyer_id   = '" . $idbuy . "'      " ;         
   
                  

        $result_off   = mysqli_query($con, $sqlOffer);
        
        //$xxprice = 0.0;
        
                if($row['feature']!=0){
                        $filterCat = "and gp.feature = '" . $row['feature'] . "' ";
                }else{ 
                        $filterCat ='';                    
                }

                $sql_price = "select gp.id,gp.price_adm,
                                     s.name as sizename , gp.feature as feature, gp.factor,
                                     gp.stem_bunch , b.name as stems,
                                     gp.unit
                                from grower_parameter gp
                               inner JOIN sizes s ON gp.size = s.id
                               inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                                left JOIN features f ON gp.feature = f.id
                               where gp.idsc = '" . $row['subcategoryid'] . "'
                                 and gp.size = '" . $row['sizeid'] . "'
                                    $filterCat";
                
                        $rs_price = mysqli_query($con,$sql_price);
                        $row_price = mysqli_fetch_array($rs_price);                
        
              //$xxprice = $row_price['price_adm'];
                        
                        $xxprice = $row['price_front'];
              
         $pdf->SetFont('Arial','B',8);
  
         $pdf->Cell(15,6,$market,0,0,'L');                                                                     
         $pdf->Cell(72,6,$row['name']." ".$row['subcate']." ".$row['size_name']." cm  ".$row['colorname']."",0,0,'L');            
         $pdf->Cell(25,6,number_format($xxprice, 2, '.', ','),0,0,'L');    
         $pdf->Cell(23,6," Req. Qty: ".$stemsReq,0,0,'R');                              
         $pdf->Cell(8,6,' ',0,1,'L');                 
        // $pdf->Cell(25,6,$row['lfd'],0,1,'L');                                                                     

         
         $totrequest = $totrequest + $stemsReq;
                  
         
                  $pdf->SetFont('Arial','',8);
                                     $partialStem = 0;  
                                     $totalPrice  = 0;
                                     $totalPriceFlf = 0;
                                     $utilTotal1   = 0;
                                     $averagePrice = 0; 
                                     $countPrice = 0;
                                     $totalMafer = 0;
                                     
     while($rowOff = mysqli_fetch_assoc($result_off))  {    
                            
                            
	  $sel_info="select gr.id,
                           g.growers_name as name_grower, 
                            gr.product_subcategory ,			 
                            gr.product as prod_name , 					 
                            ipx.size ,					 
                            ipx.steams ,					 
                            f.name as features , 
                            ipx.qty_pack as bunchqty,
                            gr.price as gorPrice , 
                            ipx.duties , 					 
                            ipx.ship_cost,					 
                            ipx.handling_pro,  
                            ipx.price_cad   ,
                            ipx.box_packing,  
                            ipx.truck   ,                            
                            p.box_type  ,
                            ipx.total_duties
                from grower_offer_reply gr 
               inner join buyer_requests br  on gr.offer_id     = br.id 
               inner join product p          on gr.product      = p.name and gr.product_subcategory = p.subcate_name
                left join subcategory s      on p.subcategoryid = s.id
               inner join growers g          on gr.grower_id    = g.id 
               inner join invoice_packing ip on (gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
                left join invoice_packing_box ipx  on ip.id = ipx.id_order               
                left join sub_client scl           on ipx.cliente_id = scl.id
                left join sub_client_branch bra    on ipx.branch = bra.id
                left join features f               on br.feature = f.id                
               where gr.buyer_id  = '" . $idbuy . "'
                 and gr.offer_id >= '6731'
                 and ip.id_fact   = '" . $id_order . "'
                 and reject in (0)   
                 and gr.id = '" . $rowOff['id'] . "' ";        
                    
	$rs_info=mysqli_query($con,$sel_info);
        
        $totalMaferTruck=0;
        $totalMafer=0;
        
        while($mafer=mysqli_fetch_array($rs_info)) {
            
            $totalMafer      = $mafer['total_duties'];

            $totalMaferTruck = $mafer['truck'];                        
        }
        
        
          /////////////// Precio Maife Fin
        
                                $invoiceEntity = "select sum(price_Flf) as  price_Flf
                                                    from invoice_requests
                                                   where id_order  = '" . $rowOff['request_id'] . "'
                                                     and id_fact   in ('" . $id_order . "','" . $buybo['join_order'] . "')
                                                     and grower_id = '" . $rowOff['grower_id'] . "'
                                                     and offer_id  = '" . $rowOff['id'] . "' ";
     
                                $invoiceID = mysqli_query($con, $invoiceEntity);
                                $flfprice  = mysqli_fetch_array($invoiceID);
                                
                                
                            
                                if ($row['box_type'] == 0) {
                                    $stems = $rowOff['bunchqty']*$rowOff['steams'];
                                } else {
                                    $stems = $rowOff['bunchqty'];
                                } 
                            
                                if ($rowOff["type_market"] == "0") {
                                    $market = "S.Order";
                                }else{
                                    $market = "O.Market";
                                }
                                
                                $util = (($flfprice['price_Flf']-$rowOff['price'])/$flfprice['price_Flf'])*100;
                                
                                $xx = ($flfprice['price_Flf']*$stems)-($rowOff['price']*$stems);
                                
                                 
                                $pdf->Cell(15,6,$market,0,0,'L');                                
                                $pdf->Cell(60,6,$rowOff['product']." ".$rowOff['product_subcategory']." ".$rowOff['steams']."st/bu ".$rowOff['size']."cm ".$rowOff['colorname'],0,0,'L');    
                                
                                $pdf->Cell(20,6,number_format($rowOff['price'], 2, '.', ','),0,0,'R');    
                                $pdf->Cell(20,6,number_format($util, 2, '.', ',')."% ",0,0,'R');    
                                
                                $pdf->Cell(20,6,$stems,0,0,'R');    
                                $pdf->Cell(8,6,'',0,0,'L');    
                                $pdf->Cell(25,6,substr($rowOff['growers_name'],0,12),0,0,'L'); 
                                $pdf->Cell(15,6,number_format($xx, 2, '.', ','),0,1,'L');    
                                
                                $totoffer = $totoffer + $stems;
                                
                                $cabCount = 0;  
                                $partialStem  = $partialStem + $stems;  
                                

                                $totalPriceFlf= $totalPriceFlf + ($flfprice['price_Flf']*$stems) ;  
                                $totalPrice   = $totalPrice + ($rowOff['price']*$stems) ;
                                
                                
                                
                                $averagePrice  = $averagePrice + $rowOff['price'];
                                
                                $countPrice  = $countPrice + 1;
                                
                                /////////// TOTALES
                                $totCosto= $totCosto + ($rowOff['price']*$stems);
                                
                                $totalPriceFlfGen= $totalPriceFlfGen + ($flfprice['price_Flf']*$stems) ;  
                                $totalPriceGen   = $totalPriceGen + ($rowOff['price']*$stems) ;                                
                                $xxprofit=$flfprice['price_Flf']-$rowOff['price'];
                        }  
                             $totVenta=$totVenta + ($partialStem*$xxprice);
                             
                             $resAverage = $averagePrice/$countPrice;
                             
                           if(is_nan($resAverage)){  
                              $resAverage = $xxprice;
                           }                             
                             
                          $differ = $stemsReq - $partialStem;
                          
                          $porceStems = ($partialStem/$stemsReq)*100;
                          
                          $utilTotal1 = $totalPriceFlf-$totalPrice;
                          $porceVal   = ( ($totalPriceFlf-$totalPrice) / $totalPriceFlf)*100;
                                                    
                          
                          $poceValtot    = $porceVal ;
                          $poceValtotFlf = $totalPriceFlf ;
                          $poceValtotGro = $totalPrice ;
                          
                          if(is_nan($utilTotal1)){  
                              $utilTotal1 = 0;
                          }
                          
                $pdf->Ln(5);
                $pdf->SetFont('Arial','B',9);
                
                $xxpricemafer = $totalMafer;
                
                $xxprice = $xxprice-$row['cost_transp'];
                
                $pdf->Cell(112,6,' ',0,0,'L');                     
                $pdf->Cell(24,6,'Budget',0,0,'L');                     
                $pdf->Cell(24,6,'Real',0,0,'L');                     
                $pdf->Cell(24,6,'Dif',0,1,'L');                                     
                ///////////////////////////////////////////////////////////////////////////////
                $pdf->Cell(34,6,'Requested Qty: ',0,0,'L');     
                $pdf->Cell(14,6,number_format($stemsReq, 0, '.', ','),0,0,'R'); 
$pdf->Cell(35,6,'',0,0,'L');                     
                $pdf->Cell(24,6,'Flower price: ',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxprice, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxpricemafer, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxprice-$xxpricemafer, 2, '.', ','),0,1,'R');                 
                
                $xxfreight=$row['cost_transp'];
                
                 if ($xxpricemafer >0) {
                                     $xxfreightmafer=$totalMaferTruck;
                 }else{
                                     $xxfreightmafer=0;
                 }


                
                $pdf->Cell(34,6,'Confirmed Qty: ',0,0,'L');     
                $pdf->Cell(14,6,number_format($partialStem, 0, '.', ','),0,0,'R');      
$pdf->Cell(35,6,'',0,0,'L');  
                $pdf->Cell(24,6,'Freight: ',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxfreight, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxfreightmafer, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxfreight-$xxfreightmafer, 2, '.', ','),0,1,'R');                 
                
                $$xxprofitcero = 0;
                
                
                $pdf->Cell(34,6,'Remaining: ',0,0,'L');                   
                $pdf->Cell(14,6,$differ,0,0,'R');     
$pdf->Cell(35,6,'',0,0,'L');  
                $pdf->Cell(24,6,'Profit: ',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxprofit, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($$xxprofitcero, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxprofit-$$xxprofitcero, 2, '.', ','),0,1,'R');                 
                
                $xxtotalbudget = $xxprice      + $xxfreight + $xxprofit;
                $xxtotalreal   = $xxpricemafer + $xxfreightmafer + $$xxprofitcero;  
                
                $xxtotaldif    = (xxprice-$xxpricemafer)+($xxfreight-$xxfreightmafer)+($xxprofit-$$xxprofitcero);                                
                
                $pdf->Cell(48,6,'',0,0,'L');                   

$pdf->Cell(35,6,'',0,0,'L');  
                $pdf->Cell(24,6,'Total: ',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxtotalbudget, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxtotalreal, 2, '.', ','),0,0,'R');                 

                $pdf->Cell(10,6,'',0,0,'L');                     
                $pdf->Cell(14,6,number_format($xxtotalbudget-$xxtotalreal, 2, '.', ','),0,1,'R');                 
                
                
                
                $pdf->Ln(5);                

                $pdf->Cell(34,6,'Quantity confirmed: ',0,0,'L');     
                $pdf->Cell(14,6,number_format($partialStem, 0, '.', ','),0,1,'R');                                                    
                $pdf->Cell(34,6,'Average price: ',0,0,'L');     
                $pdf->Cell(14,6,"$".number_format($resAverage, 2, '.', ','),0,1,'R');                                                                    
                $pdf->Cell(34,6,'Total: ',0,0,'L');     
                $pdf->Cell(14,6,"$".number_format($resAverage*$partialStem, 2, '.', ','),0,1,'R');   
                
                
                $pdf->Ln(5);                                                
                
                $pdf->Cell(34,6,'Budget: ',0,0,'L');     
                $pdf->Cell(14,6,"$".number_format($xxprice, 2, '.', ','),0,1,'R');                                                                    
                $pdf->Cell(34,6,'Total: ',0,0,'L');     
                $pdf->Cell(14,6,"$".number_format($xxprice*$partialStem, 2, '.', ','),0,1,'R');                                                                    
                
                $pdf->Ln(5);                                
                
                $pdf->Cell(34,6,'Profit/loss : ',0,0,'L');                  
                if ($utilTotal1 < 0) {
                    $pdf->Cell(14,6,"($".number_format($utilTotal1*-1, 2, '.', ',').")",0,1,'R');   
                }else{
                    $pdf->Cell(14,6,"$".number_format($utilTotal1, 2, '.', ','),0,1,'R');   
                }

                
                $pdf->Cell(34,6,'Profit/loss : ',0,0,'L');   
                                if ($poceValtot < 0) {
                                       $pdf->Cell(14,6,"(".number_format($poceValtot*-1, 2, '.', ',')." %)",0,1,'R');                                                                      
                                }else{
                                       $pdf->Cell(14,6,number_format($poceValtot, 2, '.', ',')." %",0,1,'R');                                  
                                }

                
                $pdf->Ln(5);                
                
                $pdf->Cell(34,6,'Fullfill : ',0,0,'L');                     
                $pdf->Cell(14,6,number_format($porceStems, 2, '.', ',')." %",0,1,'R');   
                
                $totProfit=$totProfit+$utilTotal1;
                                                
                ///////////////////////////////////////////////////////////////////////////////
                
                
                $pdf->Cell(70,6,'            _____________________________________________________________________________________',0,1,'L');  

                
                
                                $pdf->Ln(8);
                
                                  if ($cabCount == 0) {
                                        $pdf->Ln(4);    
                                      $cabCount = 1;  
                                  }

         
         $totalStems    = $totalStems + $subtotalStems;                     
         $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;  
         
         $poceValgen    = $poceValgen + $poceValtot;
         $poceflfgen    = $poceflfgen + $poceValtotFlf;
         $pocegrogen    = $pocegrogen + $poceValtotGro;
         

    }
    
     $totPorce   = ( ($totalPriceFlfGen-$totalPriceGen) / $totalPriceFlfGen)*100;
     
    $pdf->Ln(2);
    $pdf->Cell(70,6,'_________________________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','B',10);

    //$pdf->Cell(30,10,'TOTALS :     ',0,0,'L');    
    //$pdf->Cell(25,10,'Req.  : '.number_format($totrequest, 0, '.', ','),0,0,'L');    
    //$pdf->Cell(25,10,'Offer : '.number_format($totoffer, 0, '.', ','),0,0,'L');    
    //$pdf->Cell(60,10,'To complete : '.number_format($totrequest-$totoffer, 0, '.', ','),0,1,'L');      
    
    $pdf->Cell(25,10,'Total Sales : '.number_format($totVenta, 2, '.', ','),0,1,'L');      
    $pdf->Cell(25,10,'Total Costs : '.number_format($totCosto, 2, '.', ','),0,1,'L');      
    $pdf->Cell(25,10,'Profit/loss : '.number_format($totProfit, 2, '.', ','),0,1,'L');               
    $pdf->Cell(25,10,'Profit/loss : '.number_format($totPorce, 2, '.', ',')." %",0,1,'L');               
    
    
  $pdf->Output();
  ?>