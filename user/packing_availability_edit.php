<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

  $off_id = $_GET['idor'];  
  $fact_num = $_GET['id_fact'];  
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

      
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
     
    $ins = "update buyer_orders 
               set availability   = '" . $_POST["availability"]   . "' 
             where id = '" . $fact_num . "' ";    
       
   if(mysqli_query($con, $ins))
    
    header("location:packing_mgmt.php");   
}


  $sel_offers = "select id             , buyer_id   , order_number , order_date   , shipping_method , 
                        del_date       , date_range , is_pending   , order_serial , seen            , 
                        delivery_dates , lfd_grower , qucik_desc   , type_market  , delivery_day    , 
                        availability
                   from buyer_orders 
                  where id = '" . $fact_num . "' ";

$rs_offers = mysqli_query($con, $sel_offers);
$row = mysqli_fetch_array($rs_offers);   


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                
                arrTmp[0] = checkavailability();
                
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkavailability()       {
                
                if (trim(document.frmcat.availability.value) == "")  {
                    document.getElementById("lblava").innerHTML = "Please enter Availability ";
                    return false;
                }else {
                    document.getElementById("lblava").innerHTML = "";
                    return true;
                }                
                                                
            }
            

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Availability </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="packing_mgmt.php" onclick="this.blur();"><span> Availability</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="packing_availability_edit.php?id_fact=<?php echo $row["id"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>



                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Availability </td>
                                                                                    
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                    <select name="availability" id="availability" class="listmenu">
                                                                                        <option value="0" <?php  if($row["availability"] == 0) echo "selected";?>>Not Ready</option>
                                                                                        <option value="1" <?php  if($row["availability"] == 1) echo "selected";?>>Published</option>
                                                                                        <option value="2" <?php  if($row["availability"] == 2) echo "selected";?>>Packing Load (Ready)</option>
                                                                                        <option value="4" <?php  if($row["availability"] == 4) echo "selected";?>>Completed</option>
                                                                                        <option value="5" <?php  if($row["availability"] == 5) echo "selected";?>>Quick-Books</option>                                                                                        
                                                                                    </select>                                                                                        
                                                                                        
                                                                                        <br>
                                                                                            <span class="error" id="lblava"></span>
                                                                                    </td>
                                                                                </tr>    
                                                                                                                                                              
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>
                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>