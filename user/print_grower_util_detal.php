<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['id_fact'];

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg           ,
                         handling_lax     , brokerage_lax
                    from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];      
            $totalCalGro = 0;
            $totalCalCli = 0;
            $totalCalFre = 0;   
   
   // Datos del Requests
   
   $sqlDetalis="select id_fact            , id_order       , order_serial  , 
                       cod_order          , product        , sizeid        , 
                       qty                , buyer          , date_added    , 
                       bunches            , box_name       , lfd           , 
                       comment            , box_id         , shpping_method, 
                       mreject            , bunch_size     , unseen        , 
                       inventary          , offer_id       , prod_name     , 
                       product_subcategory, size           , boxtype       , 
                       bunchsize          , boxqty         , bunchqty      , 
                       steams             , gorPrice       , box_weight    , 
                       box_volumn         , grower_box_name, reject        , 
                       reason             , coordination   , cargo         , 
                       color_id           , gprice         , tax           , 
                       cost_ship          , round(handling,0) as handling       , grower_id     , offer_id_index,
                       substr(rg.growers_name,1,19) as name_grower , salesPrice ,salesPriceCli   , price_Flf    
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 where buyer    = '" . $buyer_cab . "'
                   and id_fact  = '" . $id_fact_cab . "' 
                 order by ir.prod_name , ir.size ";         

        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();      
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'INVOICE',0,0,'L'); 
    
    $pdf->Image('logo.png',12,5,22); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');         
    
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,0,'L');        
    $pdf->Cell(0,6,'Gross Weight: '.$buyerOrderCab['gross_weight'],0,1,'R');        
    
    $pdf->Ln(10);

    $pdf->Cell(70,6,' ',0,0,'L');
    $pdf->Cell(25,6,'Grower',0,0,'C');    
    $pdf->Cell(25,6,'Client',0,0,'C');    
    $pdf->Cell(25,6,'FreshLife',0,0,'C');        
    $pdf->Cell(25,6,'Utility',0,0,'C');
    $pdf->Cell(25,6,'%',0,1,'C');
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
        
    while($row = mysqli_fetch_assoc($result))  {      
        
         // Verificacion Stems/Bunch
        
        $sel_bu_st = "select box_type , subcategoryid from product where name = '" . $row['prod_name'] . "' "; 
        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $SubtotalGro = $row['steams'] * $row['bunchqty'] * $row['gorPrice'];
                    $SubtotalCli = $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $SubtotalFre = $row['steams'] * $row['bunchqty'] * $row['price_Flf'];
                    
                    $qtyFac = $row['bunchqty']*$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $SubtotalGro =  $row['bunchqty'] * $row['gorPrice'];
                    $SubtotalCli =  $row['bunchqty'] * $row['salesPriceCli'];
                    $SubtotalFre =  $row['bunchqty'] * $row['price_Flf'];
                    
                    $qtyFac  = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }        
        

         
        $subtotalStems= $row['steams'] *$row['bunchqty'] ;         
         
         if ($row['prod_name']." ".$row['size']."cm ".$row['steams']."st/bu" != $tmp_idorder) {
               $pdf->SetFont('Arial','B',8);                   
               if ($sw == 1) { 
                   
                      $sel_tmp = "select growers_name from growers where id ='" . $tmp_idorder . "' ";
                      $rs_tmp = mysqli_query($con,$sel_tmp);       
                      $tmp_grow = mysqli_fetch_array($rs_tmp);
        
                      $pdf->Cell(70,6,$tmp_idorder,0,0,'L');  
                      
                      $pdf->Cell(25,6,'$'.number_format($subTotalGrower, 2, '.', ','),0,0,'C');                       
                      $pdf->Cell(25,6,'$'.number_format($subTotalClient, 2, '.', ','),0,0,'C');                       
                      $pdf->Cell(25,6,'$'.number_format($subTotalFLF, 2, '.', ','),0,0,'C');                       
                      $pdf->Cell(25,6,' '.number_format(($subTotalFLF-$subTotalGrower), 2, '.', ','),0,0,'C');                       
                      $pdf->Cell(25,6,number_format((($subTotalFLF-$subTotalGrower)/$subTotalFLF*100), 2, '.', ','),0,1,'C');                       
                              
               }
                      $sw = 1;
                      
                     // $pdf->Cell(70,6,"  ",0,1,'L'); 
                      
                      $subStemsGrower = 0;   
                      
                      $subTotalGrower = 0;                       
                      $subTotalClient = 0;
                      $subTotalFLF    = 0;
         }

  $pdf->SetFont('Arial','',8);
  
        // $pdf->Cell(70,4,$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu",0,0,'L');         
        // $pdf->Cell(25,6,'$'.number_format($SubtotalGro, 2, '.', ','),0,0,'C');          
        // $pdf->Cell(25,6,'$'.number_format($SubtotalCli, 2, '.', ','),0,0,'C');  
        // $pdf->Cell(25,6,'$'.number_format($SubtotalFre, 2, '.', ','),0,0,'C');  
        // $pdf->Cell(25,6,' '.number_format(($SubtotalFre-$SubtotalGro), 2, '.', ','),0,0,'C');  
            
        if ($bunch_stem['box_type'] == 0) {         
                             
                 $subTotalGrower= $subTotalGrower + ($row['steams']*$row['bunchqty']*$row['gorPrice']) ;             
                 $subTotalClient= $subTotalClient + ($row['steams']*$row['bunchqty']*$row['salesPriceCli']) ;             
                 $subTotalFLF   = $subTotalFLF    + ($row['steams']*$row['bunchqty']*$row['price_Flf']) ;             
        }else{
                 
                 $subTotalGrower= $subTotalGrower + ($row['bunchqty']*$row['gorPrice']) ;                                                                
                 $subTotalClient= $subTotalClient + ($row['bunchqty']*$row['salesPriceCli']) ;                                                               
                 $subTotalFLF   = $subTotalFLF    + ($row['bunchqty']*$row['price_Flf']) ;                                                               
        }                  
         
         
        //$pdf->Cell(25,6,number_format((($SubtotalFre-$SubtotalGro)/$SubtotalFre*100), 2, '.', ','),0,1,'C');  
         
                 
            $totalCalGro = $totalCalGro + $SubtotalGro;
            $totalCalCli = $totalCalCli + $SubtotalCli;
            $totalCalFre = $totalCalFre + $SubtotalFre;                        
            
            $totalStems = $totalStems + $subtotalStems;                                                
            
            $subStemsGrower= $subStemsGrower + ($row['steams'] *$row['bunchqty']) ; 
            
            $nombreGul =$row['name_grower'];
            
            $tmp_idorder = $row['prod_name']." ".$row['size']."cm ".$row['steams']."st/bu";
                        
    }
    
    
   
    $pdf->SetFont('Arial','B',8);                   
    //$pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,$tmp_idorder,0,0,'L');  
                      $pdf->Cell(25,6,'$'.number_format($subTotalGrower, 2, '.', ','),0,0,'C');                       
                      $pdf->Cell(25,6,'$'.number_format($subTotalClient, 2, '.', ','),0,0,'C');                       
                      $pdf->Cell(25,6,'$'.number_format($subTotalFLF, 2, '.', ','),0,0,'C');   
                      $pdf->Cell(25,6,' '.number_format(($subTotalFLF-$subTotalGrower), 2, '.', ','),0,0,'C');  
                      $pdf->Cell(25,6,number_format((($subTotalFLF-$subTotalGrower)/$subTotalFLF*100), 2, '.', ','),0,1,'C');  
                      
     $pdf->Ln(2);
    $pdf->SetFont('Arial','B',8);
    
$pdf->Cell(70,6,'________________________________________________________________________________________________________________________',0,1,'L');  
                         
    $pdf->SetFont('Arial','B',8);            
    $pdf->Ln(2);
    $pdf->Cell(70,6,'TOTALES',0,0,'R');
    $pdf->Cell(25,6,'$'.number_format($totalCalGro, 2, '.', ','),0,0,'C');
    $pdf->Cell(25,6,'$'.number_format($totalCalCli, 2, '.', ','),0,0,'C');
    $pdf->Cell(25,6,'$'.number_format($totalCalFre, 2, '.', ','),0,0,'C');
    $pdf->Cell(25,6,' '.number_format(($totalCalFre-$totalCalGro), 2, '.', ','),0,0,'C');
    $pdf->Cell(25,6,number_format((($totalCalFre-$totalCalGro)/$totalCalFre*100), 2, '.', ','),0,1,'C');
    $pdf->Ln(2);
    
    $pdf->SetFont('Arial','B',10);            
    $pdf->Cell(30,6,'COMISION..: '.number_format(($totalCalCli-$totalCalFre), 2, '.', ','),0,1,'L');
    $pdf->Ln(2);
    $pdf->Cell(30,6,'Stems......:  '.number_format($totalStems, 0, '.', ','),0,1,'L');        
    
  $pdf->Output();
  ?>