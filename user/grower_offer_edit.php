<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../buyer/PHPMailer-master/src/Exception.php';
require '../buyer/PHPMailer-master/src/PHPMailer.php';
require '../buyer/PHPMailer-master/src/SMTP.php';

include "../config/config_gcp.php";

  $off_id     = $_GET['idor'];
  $bck_o      = $_GET['bck'];
  $id_buyerID = $_GET['id_buyerID'];
  
  $qtyv       = $_GET['qty'];
  $namev      = $_GET['varietyname'];
  


	$selectEmailB      = "SELECT * FROM buyers WHERE id='$id_buyerID'";
	$rs_selectEmailB   = @mysqli_query($con,$selectEmailB);
	$orderSelectEmailB = @mysqli_fetch_array($rs_selectEmailB);

		$emailBID = $orderSelectEmailB['email'];
		$fnameBID = $orderSelectEmailB['first_name'];
		$lnameBID = $orderSelectEmailB['last_name'];
		$phoneBID = $orderSelectEmailB['phone'];

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}


if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {

    $ins = "update grower_offer_reply
               set bunchqty ='" . $_POST["bunchqty"] . "',
                   price    ='" . $_POST["price"]    . "',
                   size     ='" . $_POST["size"]     . "',
                   steams   ='" . $_POST["steams"]   . "',
                   product  ='" . $_POST["product"]  . "',
                   type_market     ='" . $_POST["type_market"] . "'
             where id = '" . $off_id . "' ";

    // If stem_bunch product
    //$ins_tot = "update invoice_orders io
    //           join (select id_fact,sum(steams*bunchqty*salesPriceCli) as subt from invoice_requests where id_fact = '" . $fact_number . "') ir on io.id_fact = ir.id_fact
    //           set sub_total_amount = subt
    //           where io.id_fact = '" . $fact_number . "' ";

   // (mysqli_query($con, $ins));

   if(mysqli_query($con, $ins))

	 /*****************************************/
	 /*****  */
	 /***** Build email & send  */
	 /*****************************************/

	 $name  = $_POST["fnameBID"]." ".$_POST["lnameBID"];
	 $email = $_POST["emailBID"];
	 $phone = $_POST["phoneBID"];
         
         $namev1 = $_POST["namevBID"];
         $qtyv1  = $_POST["qtyvBID"];
         $newqty = $_POST["bunchqty"];

	 $message = "<strong>**IMPORTANT**</strong> <br> Offer id: $off_id have changed!. <br><br><strong>**DETAILS**</strong><br> Variety: $namev1<br> Qty: $qtyv1 <br><br><strong>**NEW QUANTITY**</strong><br>Qty: $newqty";
	 $buyerName = "Fresh Life Floral";

	 //Instantiation and passing `true` enables exceptions
	 $mail = new PHPMailer(true);

	 try {
			 //Server settings
			 $mail->SMTPDebug = 0;                      //Enable verbose debug output
			 $mail->Mailer     = 'smtp';                                          //Send using SMTP
			 $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
			 $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
			 $mail->Username   = 'admin@freshlifefloral.com';                     //SMTP username
			 $mail->Password   = 'F39C$2m@';                               //SMTP password
			 $mail->SMTPSecure = 'tls';         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			 $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			 //Recipients
			 $mail->setFrom('admin@freshlifefloral.com', "Fresh Life Floral");
			 $mail->addAddress($email, $name);     //Add a recipient
			// $mail->addReplyTo('admin@victoriasblossom.ca', '**IMPORTANT** Offer have changed!');
			 $mail->addCC('portega@freshlifefloral.com');
			 $mail->addCC('evalencia@freshlifefloral.com');
			 //$mail->addBCC('annie@victoriasblossom.ca');

			 //Attachments
			 //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
			 //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

			 //Content
			 $mail->isHTML(true);
			// $mail->AddEmbeddedImage('../images/logo.jpeg', 'Freshlifefloral', '../images/logo.jpeg');            //Set email format to HTML
			 $mail->Subject = "**IMPORTANT** Offer id: ".$off_id." have changed!.";
			 $mail->Body    = buildEmailBody($name,$email,$phone,$buyerName,$message);


			 //$mail->AddEmbeddedImage('../images/logo.jpeg', 'logoimg', '../images/logo.jpeg'); // attach file logo.jpg, and later link to it using identfier logoimg

			 $mail->send();
				 //die('{:success:}');
				 header("location:grower_offer_mgmt.php?id_offer=$bck_o");
	 } catch (Exception $e) {
	 echo "<div class='alert alert-danger mb-3'>Message could not be sent. {$mail->ErrorInfo}</div>";
	 }



	 /*****************************************/
	 /*****  */
	 /***** Build email & send  */
	 /*****************************************/



}


/**
 *
 *	Creating a template
 *	Looks ugly in here, but the final email
 *	is better than plain text!
 *
 *	:: More email template layouts in a future update!
 */
function buildEmailBody($name,$email,$phone,$buyerName,$message) {


	// HEADER
	$tpl = '
	<html>
		<head>
			<title>"**IMPORTANT** Offer have changed!";</title>
		</head>

		<body style="color:#000000;background-color:#e9ecf3;font-family: Helvetica, Arial, sans-serif;line-height:20px;font-size: 16px;font-weight:normal;">
			<div style="background-color:#ffffff;max-width:600px; margin-left:auto; margin-right:auto; margin-top:60px; margin-bottom:60px; box-shadow: 0 1px 15px 1px rgba(113, 106, 202, 0.08); border-radius:4px; -webkit-border-radius:4px; padding:30px;">


				<h3 style="font-size: 23px; margin-bottom:0px;padding-bottom:30px; border-bottom: #cccccc 1px solid;">
					' . $buyerName . '													<br>
					<span style="color:#999999;font-weight:300; font-size:15px;">
						' . date('d F, Y / H:i') . '
					</span>
				</h3> 																					<br>';

	// BODY
	$tpl .= '
				<div style="font-size:18px;font-weight:normal;">
					' . $message . '
				</div>
																										<br>
				<hr style="border:0; border-top:#cccccc 1px solid;">
																										<br>
				<div style="font-size:14px">
					<strong>Buyer name:</strong> 					' . $name . ' 				<br>
					<strong>Buyer email:</strong> 				' . $email . ' 			<br>
					<strong>Buyer phone:</strong> 				' . $phone . ' 			<br>
				</div> <br>

				<img src="cid:logoimg" width="30%">';



	// FOOTER
	$tpl .= '
			</div>
		</body>
	</html>';


	return $tpl;

}


$sel_offers = "select id             , reject2    , reason2       , offer_id         , offer_id_index     ,
                       grower_id      , buyer_id   , status        , product          , product_subcategory,
                       size           , boxtype    , bunchsize     , boxqty           , bunchqty           ,
                       steams         , total_stems, price         , total_price_st_bu, date_added         ,
                       type           , accept_date, box_weight    , box_volumn       , grower_box_name    ,
                       shipping_method, reject     , reason        , shipping         , handling           ,
                       tax            , totalprice , totalprice_box, totalstems_box   , atprice_box        ,
                       atqty_box      , finalprice , paid          , billing_add      , coordination       ,
                       marks          , cargo      , invoice       , inventary        , unit               ,
                       req_group      , request_id,type_market
                  from grower_offer_reply
                 where id = '" . $off_id . "' ";

$rs_offers = mysqli_query($con, $sel_offers);
$row = mysqli_fetch_array($rs_offers);


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()       {

                if (trim(document.frmcat.price.value) == "")  {
                    document.getElementById("lblfrom").innerHTML = "Please enter price ";
                    return false;
                }else {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }

                if (trim(document.frmcat.bunchqty.value) == "")  {
                    document.getElementById("lblbunch").innerHTML = "Please enter bunches ";
                    return false;
                }else {
                    document.getElementById("lblbunch").innerHTML = "";
                    return true;
                }


                if (trim(document.frmcat.size.value) == "")  {
                    document.getElementById("lblsize").innerHTML = "Please enter size ";
                    return false;
                }else {
                    document.getElementById("lblsize").innerHTML = "";
                    return true;
                }

                if (trim(document.frmcat.steams.value) == "")  {
                    document.getElementById("lblsteams").innerHTML = "Please enter stems ";
                    return false;
                }else {
                    document.getElementById("lblsteams").innerHTML = "";
                    return true;
                }

                if (trim(document.frmcat.product.value) == "")  {
                    document.getElementById("lblprod").innerHTML = "Please enter product ";
                    return false;
                }else {
                    document.getElementById("lblprod").innerHTML = "";
                    return true;
                }
                if (trim(document.frmcat.type_market.value) == "")  {
                    document.getElementById("lblmrk").innerHTML = "Please enter type market ";
                    return false;
                }else {
                    document.getElementById("lblmrk").innerHTML = "";
                    return true;
                }
            }

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Offers</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="grower_offer_mgmt.php" onclick="this.blur();"><span> Offers</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="grower_offer_edit.php?idor=<?php echo $row["id"]."&bck=".$row["offer_id"] ?>">

																															<input type="hidden" id='emailBID' name="emailBID" value='<?php echo $emailBID;?>' />
																															<input type="hidden" id='fnameBID' name="fnameBID" value='<?php echo $fnameBID;?>' />
																															<input type="hidden" id='lnameBID' name="lnameBID" value='<?php echo $lnameBID;?>' />
																															<input type="hidden" id='phoneBID' name="phoneBID" value='<?php echo $phoneBID;?>' />
                                                              <input type="hidden" id='namevBID' name="namevBID" value='<?php echo $namev;?>' />
                                                              <input type="hidden" id='qtyvBID' name="qtyvBID" value='<?php echo $qtyv;?>' />

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Price </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price" id="price" value="<?php echo $row["price"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblfrom"></span></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Bunches </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="bunchqty" id="bunchqty" value="<?php echo $row["bunchqty"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblbunch"></span></td>
                                                                                </tr>



                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Size </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="size" id="size" value="<?php echo $row["size"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblsize"></span></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Stems </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="steams" id="steams" value="<?php echo $row["steams"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblsteams"></span></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Variety </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="product" id="product" value="<?php echo $row["product"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblprod"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Type Market </td>

                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                    <select name="type_market" id="type" class="listmenu">
                                                                                        <option value="0" <?php  if($row["type_market"] == 0) echo "selected";?>>Standing Order</option>
                                                                                        <option value="1" <?php  if($row["type_market"] == 1) echo "selected";?>>Open Market</option>
                                                                                    </select>

                                                                                        <br>
                                                                                            <span class="error" id="lblmrk"></span>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>

                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>

                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
