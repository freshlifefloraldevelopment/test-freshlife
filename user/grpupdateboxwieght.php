<?php	
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";


session_start();
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

$grower_name = $_POST['grower_name'];

if (isset($_POST["Submit"])) {

    $grower_id = $_POST['grower_id'];

    for ($i = 1; $i <= $_POST["total"] - 1; $i++) {

        $sel_price = "select id from grower_product_box_weight

        where growerid='" .  $grower_id . "' and 
        prodcutid='" . $_POST["prodcutid" . $i] . "' and 
        sizeid='" . $_POST["sizeid" . $i] . "' and box_id='" . $_POST["boxid" . $i] . "' and
        feature='" . $_POST["feature" . $i] . "' and bunch_size_id='" . $_POST["bunchsizeid" . $i] . "'";

        $rs_price = mysqli_query($con,$sel_price);
        $check_price = mysqli_num_rows($rs_price);
        if ($check_price >= 1) {
            $price = mysqli_fetch_array($rs_price);
            $update = "update grower_product_box_weight set weight='" . $_POST["qty"] . "' where id='" . $price["id"] . "'";
            mysqli_query($con,$update);

        } else {
            $insert = "insert into grower_product_box_weight set 
                    weight='" . $_POST["qty"] . "',
					growerid='" .  $grower_id . "',
					prodcutid='" . $_POST["prodcutid" . $i] . "',   
					box_id='" . $_POST["boxid" . $i] . "',
                    bunch_size_id='" . $_POST["bunchsizeid" . $i] . "',
                    feature='" . $_POST["feature" . $i] . "',
                    bunch_qty='" . $_POST["bunch_qty" . $i] . "',
                    sizeid='" . $_POST["sizeid" . $i] . "'";

            //echo $insert ;
            mysqli_query($con,$insert);
        }
    }
    header("location:box_weight_mgmt.php?id=" . $grower_id);

} else {
    $qsel = "select b.name as boxname,gpf.features as fid,ff.name as featurename,b.id as boxid,bs.id as bunchsizeid,b.width,b.length,b.height,bs.name as bunchname,

    p.id as pid,p.name as productname,    p.color_id as colorid,p.categoryid as productcategory,p.subcategoryid as productsubcategoryid,gs.sizes as sizeid,

    pc.name productcateogryname,sh.name as sizename,c.name as colorname,gq.quality       as qualitygradeid,s.name as productsubcategoryname,q.name as qualitygradename,gs.* 

    from  grower_product_bunch_sizes gs 

	left join grower_product_features gpf on gs.product_id=gpf.product_id

	left join features ff on gpf.features=ff.id  

	right join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id

	left join product p on gs.product_id=p.id 

	left join category pc on p.categoryid=pc.id 

	left join subcategory s on p.subcategoryid=s.id  

	left join colors c on p.color_id=c.id 

	left join boxes b on gpb.boxes=b.id

	left join bunch_sizes bs on gs.bunch_sizes=bs.id

	left join sizes sh on gs.sizes=sh.id 

	left join (select product_id ,quality from grower_product_quality) gq on p.id=gq.product_id 

	left join quality q on gq.quality=q.id  

	where gs.grower_id='" . $_POST["grower_id"] . "'";

    if ($_POST["pcategory"] != "") {
        $qsel .= " AND p.categoryid='" . $_POST["pcategory"] . "'";
    }
    if ($_POST["grp_box"] != "") {
        $qsel .= " AND gpb.boxes='" . $_POST["grp_box"] . "'";
    }
    if ($_POST["subcategory"] != "") {
        $qsel .= " AND p.subcategoryid='" . $_POST["subcategory"] . "'";

    }
    if ($_POST["products"] != "") {
        $qsel .= " AND p.id='" . $_POST["products"] . "'";
    }
    if ($_POST["grp_special"] != "") {

        $qsel .= " AND gpf.features='" . $_POST["grp_special"] . "'";

    }
    if ($_POST["grp_color"] != "") {
        $qsel .= " AND p.color_id='" . $_POST["grp_color"] . "'";

    }
    if ($_POST["grp_quality"] != "") {
        $qsel .= " AND gq.quality='" . $_POST["grp_quality"] . "'";

    }
    if ($_POST["grp_size"] != "") {
        $qsel .= " AND gs.sizes='" . $_POST["grp_size"] . "'";
    }
    if ($_POST["grp_bunch_size"] != "") {
        $qsel .= " AND gs.bunch_sizes='" . $_POST["grp_bunch_size"] . "'";
    }
    $qsel .= " group by gs.product_id,gs.sizes,gs.grower_id,gs.bunch_sizes,gpb.boxes,gpf.features order by p.name";
    $rs = mysqli_query($con,$qsel);
    $totalrow = mysqli_num_rows($rs);

    if ($totalrow < 1) {
        header("location:box_weight_mgmt.php?error=1&id=" . $_POST["grower_id"] . "");
    }

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

    <title>Admin Area</title>

    <link href="css/style.css" rel="stylesheet" type="text/css"/>

    <link href="css/demo_page.css" rel="stylesheet" type="text/css"/>

    <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css"/>

    <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf-8">

        $(document).ready(function () {
            oTable = $('#example').dataTable({
                //"sScrollXInner": "130%",
                "bJQueryUI": true,
                "bFilter": false,
                "iDisplayLength": 50000,
                "sPaginationType": "full_numbers"
            });
        });



    </script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php include("includes/header_inner.php"); ?>

    <tr>

        <td height="5"></td>

    </tr>

    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <?php include("includes/grower-left.php"); ?>
                    <td width="5">&nbsp;</td>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10" valign="top" background="images/middle-leftline.gif"><img
                                            src="images/middle-topleft.gif" width="10" height="80"/></td>
                                <td valign="top" background="images/middle-topshade.gif"
                                    style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="pagetitle">Update Box Weight By
                                                            Group(<?php echo $grower_name ?>)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <a class="pagetitle1"

                                                                           href="box_weight_mgmt.php?id=<?php echo $_POST["grower_id"] ?>"

                                                                           onclick="this.blur();"><span> Manage Box <span

                                                                                    class="pagetitle">Weight</span></span></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <form action="" id="form1" method="post">
                                                                <div id="box">
                                                                    <div id="container">
                                                                        <div class="demo_jui">
                                                                            <?php
                                                                            $sr = 1;
                                                                            if ($totalrow >= 1) {
                                                                                ?>
                                                                                <div style="color:red; padding-bottom:20px;"
                                                                                    class="pagetitle"> Following
                                                                                    Product(s) Will Be Affected
                                                                                    . <input
                                                                                            name="Submit" type="Submit"
                                                                                        class="buttongrey"
                                                                                        value="Click Here to Update Box Weight(s)"/>
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <table cellpadding="0" cellspacing="0"

                                                                                   border="1" class="display"

                                                                                   id="example" bordercolor="#e4e4e4">
                                                                                <thead>
                                                                                <tr>
                                                                                    <!--Box-->
                                                                                    <th width="20%" align="left">Box
                                                                                    </th>
                                                                                    <!--Prodcut-->
                                                                                    <th width="20%" align="left">Product
                                                                                    </th>
                                                                                    <!--Subcategory-->
                                                                                    <th width="10%" align="left">

                                                                                        SubCategory
                                                                                    </th>
                                                                                    <!--Size-->
                                                                                    <th width="10%" align="left">Size
                                                                                    </th>
                                                                                    <!--Bunch Size-->
                                                                                    <th width="10%" align="left">Bunch
                                                                                        Size
                                                                                    </th>
                                                                                    <!--No of  Stems-->
                                                                                    <th width="10%" align="left"> No of

                                                                                        Stems

                                                                                    </th>

                                                                                    <!--Weight in  KG-->

                                                                                    <th width="10%" align="left"> Weight

                                                                                        ( In KG )

                                                                                    </th>

                                                                                </tr>

                                                                                </thead>

                                                                                <tbody>

                                                                                <?php
                                                                                $sr = 1;
                                                                                while ($product = mysqli_fetch_array($rs)) {
                                                                                    $sel_last_price = "select qty from grower_product_box_packing where 

                                                                                            growerid='" . $_POST["grower_id"] . "' and prodcutid='" . $product["pid"] . "' and 

								                                                            sizeid='" . $product["sizeid"] . "' and box_id='" . $product["boxid"] . "' and 

								                                                            feature='" . $product["fid"] . "' and bunch_size_id='" . $product["bunchsizeid"] . "' 

								                                                             limit 0,1";
                                                                                    $rs_last_price = mysqli_query($con,$sel_last_price);
                                                                                    $last_price = mysqli_fetch_array($rs_last_price);



                                                                                    // echo $last_price["qty"]."<br/>" ;

                                                                                    if ($last_price["qty"] != "") {

                                                                                        ?>

                                                                                        <tr class="gradeU">

                                                                                            <td class="text"

                                                                                                align="left"><?php echo $product["boxname"] ?>

                                                                                                (<?php echo $product["width"] ?>

                                                                                                *<?php echo $product["length"] ?>

                                                                                                *<?php echo $product["height"] ?>

                                                                                                )

                                                                                            </td>

                                                                                            <td class="text"

                                                                                                align="left"><?= $product["productname"] ?> <?php if ($product["featurename"] != "") { ?>

                                                                                                    <br/> (<?php echo $product["featurename"] ?>) <?php } ?>

                                                                                            </td>
                                                                                            <td class="text"
                                                                                                align="left"><?php echo $product["productsubcategoryname"] ?></td>
                                                                                            <td class="text"
                                                                                                align="left"><?php echo $product["sizename"] ?>CM
                                                                                            </td>



                                                                                            <td class="text"

                                                                                                align="center"><?php echo $product["bunchname"] ?>

                                                                                                Stems

                                                                                                <input type="hidden"

                                                                                                       name="prodcutid<?php echo $sr ?>"

                                                                                                       value="<?php echo $product["pid"] ?>"/>

                                                                                                <input type="hidden"

                                                                                                       name="prodcutname<?php echo $sr ?>"

                                                                                                       value="<?php echo $product["productname"] ?>"/>

                                                                                                <input type="hidden"

                                                                                                       name="sizeid<?php echo $sr ?>"

                                                                                                       value="<?php echo $product["sizeid"] ?>"/>

                                                                                                <input type="hidden"

                                                                                                       name="sizename<?php echo $sr ?>"

                                                                                                       value="<?php echo $product["sizename"] ?>"/>

                                                                                                <input type="hidden"

                                                                                                       name="categoryid<?php echo $sr ?>"

                                                                                                       value="<?php echo $product["productcategory"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="subcategoryid<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["sizename"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="boxid<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["boxid"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="bunchsizeid<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["bunchsizeid"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="bunch_qty<?php echo $sr ?>"
                                                                                                       value="<?php echo $last_price["qty"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="feature<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["fid"] ?>"/>
                                                                                                <input type="hidden"
                                                                                                       name="qualityid<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["qualitygradeid"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="qualityname<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["qualitygradename"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="colorid<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["colorid"] ?>"/>

                                                                                                <input type="hidden"
                                                                                                       name="colorname<?php echo $sr ?>"
                                                                                                       value="<?php echo $product["colorname"] ?>"/>
                                                                                            </td>
                                                                                            <td><?php echo ($product["bunchname"]) * ($last_price["qty"]) ?>
                                                                                                Stems
                                                                                            </td>
                                                                                            <td><?php echo $_POST["txtprice"] ?></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        $sr++;
                                                                                    }
                                                                                }
                                                                                ?>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                    </div>

                                                                    <div style="float:right; margin-top:10px;">

                                                                        <input type="hidden" name="total"
                                                                               value="<?php echo $sr ?>"/>

                                                                        <input type="hidden" name="grower_id"
                                                                               id="grower_id"
                                                                               value="<?php echo $_POST["grower_id"] ?>"/>
                                                                        <input type="hidden" name="qty"
                                                                               value="<?php echo $_POST["txtprice"] ?>"/>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="10">&nbsp;</td>
                                        </tr>

                                    </table>

                                </td>

                                <td width="10" valign="top" background="images/middle-rightline.gif"><img

                                        src="images/middle-topright.gif" width="10" height="80"/></td>

                            </tr>

                            <tr>

                                <td background="images/middle-leftline.gif"></td>

                                <td>&nbsp;</td>

                                <td background="images/middle-rightline.gif"></td>

                            </tr>

                            <tr>

                                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10"/></td>

                                <td background="images/middle-bottomline.gif"></td>

                                <td><img src="images/middle-bottomright.gif" width="10" height="10"/></td>

                            </tr>

                        </table>

                    </td>

                </tr>

            </table>

        </td>

    </tr>

    <tr>

        <td height="10"></td>

    </tr>

    <?php include("includes/footer-inner.php"); ?>

    <tr>

        <td>&nbsp;</td>

    </tr>

</table>

</body>

</html>
