<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $idbuy = $_GET['id_buy'];    
    
    $id_order = $_GET['b'];    


    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);

    $OrderCab = "select * from buyer_orders
                  where id = '" . $id_order . "'  " ;
     
    $buyOrder = mysqli_query($con, $OrderCab);
    $cabOrder = mysqli_fetch_array($buyOrder);
    
   
   // Datos de la Orden 
   
   $sqlDetalis="select unseen,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       p.name , 
                       br.qty ,  
                       br.lfd ,
                       sizeid ,
                       sz.name as size_name,
                       g.growers_name , br.id_client,  br.id_reser , sc.name as namecli , s.name as subcate                     
                  from buyer_requests br
                 INNER JOIN product p     ON br.product = p.id
                 INNER JOIN buyers b     ON br.buyer = b.id
                 INNER JOIN buyer_orders bo ON br.id_order = bo.id
                 INNER JOIN sizes sz ON br.sizeid = sz.id  
                 INNER join sub_client sc ON br.id_client = sc.id   
                 INNER JOIN subcategory s ON s.id = p.subcategoryid                                   
                 left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer    = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
                   and br.id_client != 0
                 order by sc.name,p.name ";

    $result   = mysqli_query($con, $sqlDetalis);    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // Resumen de la Orden
    
   $sqlresumen="select p.name    , s.name as subcate  ,  sz.name as size_name ,sum(br.qty) qty ,
                       br.sizeid , s.id as sid , co.name as colname,br.product , br.boxtype
                    from buyer_requests br
                   INNER JOIN product p     ON br.product = p.id
                   INNER JOIN buyers b     ON br.buyer = b.id
                   INNER JOIN buyer_orders bo ON br.id_order = bo.id
                   INNER JOIN sizes sz ON br.sizeid = sz.id  
                   INNER join sub_client sc ON br.id_client = sc.id   
                   INNER JOIN subcategory s ON s.id = p.subcategoryid                                   
                   INNER JOIN colors co ON co.id = p.color_id
                    left JOIN growers g     ON br.id_grower = g.id                                                  
                   where br.buyer    = '" . $idbuy . "' 
                     and br.id_order = '" . $id_order . "' 
                     and br.id_client != 0    
                   group by   s.name   ,p.name ,  sz.name , br.boxtype
                   order by   s.name   ,p.name ,  sz.name , br.boxtype ";

    $resultRes   = mysqli_query($con, $sqlresumen);        

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  CUSTOMER ORDERS ',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,'-'.$cabOrder['qucik_desc'],0,1,'L');  
    
    $pdf->Ln(10);
    $cabCount = 1;  
    
// Cabecera
    
    $pdf->Cell(40,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'R');
    $pdf->Cell(5,6,' ',0,0,'L');        
    $pdf->Cell(25,6,'Order',0,0,'L');
    $pdf->Cell(5,6,' ',0,0,'L');            
    $pdf->Cell(25,6,'Lfd',0,0,'L');    
    $pdf->Cell(15,6,'Size',0,0,'L'); 
    $pdf->Cell(25,6,'Client',0,1,'L');     
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {         
       
           // Datos de la Oferta
   
   $sqlOffer="select gor.marks          ,
                     gor.id             ,  
                     gor.offer_id       , 
                     gor.offer_id_index , 
                     gor.grower_id      , 
                     gor.buyer_id       , 
                     gor.status         , 
                     gor.product        , 
                     gor.price          , 
                     gor.size           , 
                     gor.boxtype        , 
                     gor.bunchsize      , 
                     gor.boxqty         , 
                     gor.bunchqty       , 
                     gor.steams         ,
                     gor.req_group      ,
                     gor.request_id     ,
                     g.growers_name                                            
                from grower_offer_reply gor
                left JOIN growers g ON gor.grower_id = g.id                                                                  
               where request_id     = '" . $row['idreq'] . "'
                 and buyer_id       = '" . $idbuy . "' 
                 and gor.cliente_id = '" . $row['id_client'] . "'     " ;      
      
                  

        $result_off   = mysqli_query($con, $sqlOffer);            
              
         $pdf->SetFont('Arial','B',8);
  
         $pdf->Cell(40,4,$row['name']." ".$row['subcate'],0,0,'L');            
         $pdf->Cell(25,6,$row['qty'],0,0,'R');                              
         $pdf->Cell(5,6,' ',0,0,'L');                      
         $pdf->Cell(25,6,$row['cod_order'],0,0,'L'); 
         $pdf->Cell(5,6,' ',0,0,'L');                 
         $pdf->Cell(25,6,$row['lfd'],0,0,'L');                                                            
         $pdf->Cell(15,6,$row['size_name']." cm. ",0,0,'L');                                                                     
         $pdf->Cell(25,6,$row['namecli'],0,1,'L'); 
         
                  $pdf->SetFont('Arial','',8);
                  
                        while($rowOff = mysqli_fetch_assoc($result_off))  {
                                 $stems = $rowOff['bunchqty'];
                                 
                                $pdf->Cell(34,6,'                 ',0,0,'L');                                
                                $pdf->Cell(40,6,$rowOff['product'],0,0,'L');    
                                $pdf->Cell(25,6,$stems,0,0,'L');    
                                $pdf->Cell(25,6,$rowOff['size'],0,0,'L');                                    
                                $pdf->Cell(25,6,$rowOff['growers_name'],0,1,'L'); 
                                
                                $cabCount = 0;  
                            }         
                                  if ($cabCount == 0) {
                                        $pdf->Ln(4);    
                                      $cabCount = 1;  
                                  }

         
         $totalStems = $totalStems + $subtotalStems;                     
         $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;  

    }
 
                $pdf->Ln(10);
                
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  TOTAL SUMMARY ',0,0,'L'); 
    
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);    
    
    $pdf->Cell(40,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Subcategory',0,0,'R');
    $pdf->Cell(25,6,'Size',0,0,'L');
    $pdf->Cell(20,6,'Bunch',0,0,'L'); 
    $pdf->Cell(20,6,'Stems',0,0,'L'); 
    $pdf->Cell(20,6,'Factor',0,0,'L'); 
    $pdf->Cell(20,6,'Weight',0,1,'L');     
    
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);  
    $totweight = 0;
    
    while($totRes = mysqli_fetch_assoc($resultRes))  {
        
         // Verificacion Stems/Bunch
        
        $sel_bu_st = "select box_type , subcategoryid 
                        from product 
                       where id = '" . $totRes['product'] . "'           "; 
        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
        
                         $sel_box_type = "select substr(code,1,2) as name from units where id='" . $totRes["boxtype"] . "'";
                                $rs_box_type  = mysqli_query($con, $sel_box_type);
                                $box_type     = mysqli_fetch_array($rs_box_type);                                
                                $unit = $box_type["name"];
            

        //////////////////////////////////////////////////////////////////////////////////////////
                        $sqlbunch = "select product_id,sizes  , size_value , sid , bunch_sizes , bunch_value 
                                        from growcard_prod_bunch_sizes
                                       where product_id = '" . $totRes['product'] . "'
                                         and sizes      = '" . $totRes['sizeid'] . "'
                                        group by product_id   " ;
     
                        $rs_bunch = mysqli_query($con, $sqlbunch);
                        $facBunch    = mysqli_fetch_array($rs_bunch);
        /////////////////////////////////////////////////////////////////////////
                        $sqlfactor = "select subcategory , size , factor
                                        from grower_parameter
                                       where idsc = '" . $totRes['sid'] . "'
                                         and size = '" . $totRes['sizeid'] . "'  " ;
     
                        $rs_factor = mysqli_query($con, $sqlfactor);
                        $factor    = mysqli_fetch_array($rs_factor);
                        
              if ($bunch_stem['box_type'] == 0) {
                   // $weight = $factor['factor'] * ($totRes['qty']/$facBunch['bunch_value']);
                   // $cantidad = ($totRes['qty']/$facBunch['bunch_value']);
                  if ($unit == 'ST'){ 
                        $weight = $factor['factor'] * ($totRes['qty']/$facBunch['bunch_value']);
                        $unitFac = "STEMS";   
                        $cantidad = ($totRes['qty']);
                  }else{
                        $weight = $factor['factor'] * ($totRes['qty']);
                        $unitFac = "STEMS";   
                        $cantidad = ($totRes['qty']);
                   }
              }else{
                    $weight = $factor['factor'] * ($totRes['qty']);
                    $unitFac = "BUNCHES";                   
                    $cantidad = ($totRes['qty']);                    
              }                                        
                        
                    
                        
                               
                $pdf->Cell(40,6,$totRes['name']." ".$totRes['colname'],0,0,'L');    
                $pdf->Cell(25,6,$totRes['subcate'],0,0,'L');    
                $pdf->Cell(25,6,$totRes['size_name'],0,0,'L');                                    
                $pdf->Cell(20,6,$cantidad,0,0,'L');                                
                $pdf->Cell(20,6,$facBunch['bunch_value']." ".$unitFac,0,0,'L');                                                
                $pdf->Cell(20,6,$factor['factor'],0,0,'L');                                
                $pdf->Cell(20,6,$weight,0,1,'L');   
                
                $totweight = $totweight + $weight;
    }    
                    $pdf->Ln(5);
        $pdf->Cell(70,6,'_________________________________________________________________________________________________________',0,1,'L');      
        $pdf->Cell(140,6,'Total Weight : ',0,0,'L');
        $pdf->Cell(25,6,$totweight,0,1,'R');
    
  $pdf->Output();
  ?>