<?php	
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        
                session_start();   
                
$growerid   = $_GET['id'];                

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}	
	

                $qsel="SELECT gpb.id AS cartid,id_order,order_serial,cod_order,gpb.product,sizeid,feature,noofstems,qty,buyer,
                 gpb.boxtype,gpb.date_added,gpb.type,gpb.bunches,gpb.box_name,lfd,comment, box_id,shpping_method,isy,
                 mreject,bunch_size,unseen,req_qty,bunches2 ,gpb.date_added ,p.name,
                 p.color_id,p.image_path,s.name AS subs,sh.name AS sizename,
                 ff.name AS featurename,rg.id AS rgid,rg.req_grow,
                 gpb.inventary,
                 gpb.id_client, country ,g.growers_name
            FROM buyer_requests gpb
           INNER JOIN request_growers rg     ON gpb.id          = rg.rid
           inner JOIN product p              ON gpb.product     = p.id
           inner JOIN growers g              ON rg.gid          = g.id
            LEFT JOIN buyer_shipping_methods bsm  ON gpb.buyer  = bsm.buyer_id and gpb.shpping_method = bsm.shipping_method_id            
            LEFT JOIN subcategory s          ON p.subcategoryid = s.id  
            LEFT JOIN features ff            ON gpb.feature     = ff.id
            LEFT JOIN sizes sh               ON gpb.sizeid      = sh.id 
           WHERE rg.gid='" . $growerid . "' 
             AND gpb.lfd>='" . date("Y-m-d") . "'  
           ORDER BY gpb.id DESC";
                
                $rs=mysqli_query($con,$qsel);


	if(isset($_POST["Submit"]))	{
            
	          for($i=1;$i<=$_POST["total"]-1;$i++)	  {
			  
            $update = "update request_growers 
                          set req_grow = '".$_POST[$i."-price"]."'
                        where id       = '".$_POST[$i."-prodcutid"]."'  ";                               
    
            mysqli_query($con, $update);            
                                 
                                 
	           header("location:growers_orders_mgmt.php");                                 
                                 
                                 
				 	     
			  }
	}

	


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

$.fn.dataTableExt.oApi.fnFilterClear  = function ( oSettings )
{
    /* Remove global filter */
    oSettings.oPreviousSearch.sSearch = "";
      
    /* Remove the text of the global filter in the input boxes */
    if ( typeof oSettings.aanFeatures.f != 'undefined' )
    {
        var n = oSettings.aanFeatures.f;
        for ( var i=0, iLen=n.length ; i<iLen ; i++ )
        {
            $('input', n[i]).val( '' );
        }
    }
      
    /* Remove the search text for the column filters - NOTE - if you have input boxes for these
     * filters, these will need to be reset
     */
    for ( var i=0, iLen=oSettings.aoPreSearchCols.length ; i<iLen ; i++ )
    {
        oSettings.aoPreSearchCols[i].sSearch = "";
    }
      
    /* Redraw */
    oSettings.oApi._fnReDraw( oSettings );
};

var oTable;
$(document).ready(function() {

   oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});
   
    $('#form1').submit( function () {
         var oSettings = oTable.fnSettings();
		oTable.fnFilterClear();
        oSettings._iDisplayLength = -1;
		oSettings._bFilter = false;
		oTable.fnDraw();
    });
     
    
});
</script>


<script>

function selectscategory()

	{

   removeAllOptions(document.frmproduct.subcategory);

	addOption(document.frmproduct.subcategory,"","-- All --");

	  <?php

		$sel_subcategory="select gp.sid , 
                                         gp.categoryid , 
                                         s.name as subcategoryname 
                                    from grower_product_sizes gp 
                                    left join subcategory s on gp.sid=s.id 
                                   where gp.grower_id='".$_GET["id"]."' 
                                   group by gp.sid , gp.categoryid ,  s.name
                                   order by s.name";

		$res_subcategory=mysqli_query($con,$sel_subcategory);

		while($rw_subcategory=mysqli_fetch_array($res_subcategory))

		{

	  ?>

		if(document.frmproduct.pcategory.value=="<?php  echo $rw_subcategory["categoryid"]?>" || document.frmproduct.pcategory.value=="")

		{

			addOption(document.frmproduct.subcategory,"<?php  echo $rw_subcategory["sid"];?>","<?php  echo $rw_subcategory["subcategoryname"];?>");

			 $('#subcategory').val('<?php  echo $_POST["subcategory"]?>');

		}

	  <?php

		} 

	  ?>	

	}	


function selectsubcategory()

	{

   removeAllOptions(document.frmproduct.products);

	addOption(document.frmproduct.products,"","-- All --");

	  <?php

		$sel_product="select gp.product_id as pid ,
                                     gp.sid , 
                                     gp.categoryid ,
                                     s.name as productname 
                                from grower_product_sizes gp 
                                left join product s on gp.product_id=s.id 
                               where gp.grower_id='".$_GET["id"]."' 
                               group by gp.product_id gp.sid , 
                                        gp.categoryid ,
                                        s.name
                               order by s.name";
		
		$res_product=mysqli_query($con,$sel_product);

		while($rw_product=mysqli_fetch_array($res_product))

		{	 
			 
	  ?>
	  
	  
	  if(document.frmproduct.pcategory.value=="")
	  
	  {
	  
		if(document.frmproduct.subcategory.value=="<?php  echo $rw_product["sid"]?>" || document.frmproduct.subcategory.value=="" )
		{

			addOption(document.frmproduct.products,"<?php  echo $rw_product["pid"];?>","<?php  echo $rw_product["productname"];?>");

			 $('#grp_products').val('<?php  echo $_POST["grp_products"]?>');

		}
		
	 }	
	 
	 else
	 
	 {
	    if(document.frmproduct.subcategory.value!="")
		
		{
		
		 if(document.frmproduct.pcategory.value=="<?php  echo $rw_product["categoryid"]?>" &&  document.frmproduct.subcategory.value=="<?php  echo $rw_product["sid"]?>"  )
		{

			addOption(document.frmproduct.products,"<?php  echo $rw_product["pid"];?>","<?php  echo $rw_product["productname"];?>");

			 $('#grp_products').val('<?php  echo $_POST["grp_products"]?>');

		}
		
		}
		
		else
		{
		   
		    if(document.frmproduct.pcategory.value=="<?php  echo $rw_product["categoryid"]?>" )
		    {

			  addOption(document.frmproduct.products,"<?php  echo $rw_product["pid"];?>","<?php  echo $rw_product["productname"];?>");

			   $('#grp_products').val('<?php  echo $_POST["grp_products"]?>');

		    }
		
		}
	 }

	  <?php

		} 

	  ?>	
	  
	}	
	
	
	function selectsproduct()

	{

   removeAllOptions(document.frmproduct.products);

	addOption(document.frmproduct.products,"","-- All --");

	  <?php

		$sel_product="select gp.product_id as pid ,
                                     gp.sid , 
                                     gp.categoryid , 
                                     s.name as productname 
                                from grower_product_sizes gp 
                                 left join product s on gp.product_id=s.id 
                                where gp.grower_id='".$_GET["id"]."' 
                                group by gp.product_id ,
                                         gp.sid , 
                                         gp.categoryid , 
                                         s.name
                                order by s.name";
		
		$res_product=mysqli_query($con,$sel_product);

		while($rw_product=mysqli_fetch_array($res_product))

		{	 
			 
	  ?>
	  
	  
	  if(document.frmproduct.pcategory.value=="")
	  
	  {
	  		
			addOption(document.frmproduct.products,"<?php  echo $rw_product["pid"];?>","<?php  echo $rw_product["productname"];?>");

			 $('#grp_products').val('<?php  echo $_POST["grp_products"]?>');

		
	 }	
	 
	 else
	 
	 {
	    
		 if(document.frmproduct.pcategory.value=="<?php  echo $rw_product["categoryid"]?>" )
		 {

			addOption(document.frmproduct.products,"<?php  echo $rw_product["pid"];?>","<?php  echo $rw_product["productname"];?>");

			 $('#grp_products').val('<?php  echo $_POST["grp_products"]?>');

		}
		
		
	 }

	  <?php

		} 

	  ?>	
	  
	}	
	
	
	function removeAllOptions(selectbox)

	{

		var i;

		for(i=selectbox.options.length-1;i>=0;i--)

		{

			selectbox.remove(i);

		}

	}



	function addOption(selectbox,value,text)

	{

		var optn=document.createElement("OPTION");

		optn.text=text;

		optn.value=value;

		selectbox.options.add(optn);

	}

</script>


<script type="text/javascript">

	function verify()

	{ 

		var arrTmp=new Array();

		arrTmp[0]=checkcname();
        //arrTmp[1]=checksp_st_dt();
		//arrTmp[2]=checksp_ed_dt();
	
		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)

		{

			if(arrTmp[i]==false)

			{

			   _blk=false;

			}

		}

		if(_blk==true)

		{

			return true;

		}

		else

		{

			return false;

		}

	

 	}	


	function trim(str) 

	{    

		if (str != null) 

		{        

			var i;        

			for (i=0; i<str.length; i++) 

			{           

				if (str.charAt(i)!=" ") 

				{               

					str=str.substring(i,str.length);                 

					break;            

				}        

			}            

			for (i=str.length-1; i>=0; i--)

			{            

				if (str.charAt(i)!=" ") 

				{                

					str=str.substring(0,i+1);                

					break;            

				}         

			}                 

			if (str.charAt(0)==" ") 

			{            

				return "";         

			} 

			else 

			{            

				return str;         

			}    

		}

	}

	

	function checkcname()

	{

		if(trim(document.frmproduct.txtprice.value) == "")

		{	 

			document.getElementById("lbltxtprice").innerHTML="Please enter price";

			return false;

		}

		else 

		{

			document.getElementById("lbltxtprice").innerHTML="";

			return true;

		}

	}
	
	function checksp_st_dt()

	{

		if(trim(document.frmproduct.sp_st_dt.value) == "")

		{	 

			document.getElementById("lblsp_st_dt").innerHTML="Please enter start date";

			return false;

		}

		else 

		{
			 var today = new Date(); 	    
             var dateString = document.frmproduct.sp_st_dt.value;
			 var monthfield=document.frmproduct.sp_st_dt.value.split("-")[0]
             var dayfield=document.frmproduct.sp_st_dt.value.split("-")[1]
             var yearfield=document.frmproduct.sp_st_dt.value.split("-")[2]
             var dayobj = new Date(yearfield, monthfield-1, dayfield)
			 
			  var newDate = new Date(dayobj.getFullYear(), dayobj.getMonth(), dayobj.getDate()+1); 
			 
             
			 
			 
			 if(newDate<today)
			 {
			 
			   
			    document.getElementById("lblsp_st_dt").innerHTML="Please enter present or future date";

			    return false;
			 
			 }
	
	         else
			 {
			 
			      document.getElementById("lblsp_st_dt").innerHTML="";

			      return true;
				
			 }
			 

		}

	}
	
	
	function checksp_ed_dt()

	{

		if(trim(document.frmproduct.sp_ed_dt.value) == "")

		{	 

			document.getElementById("lblsp_ed_dt").innerHTML="Please enter end date";

			return false;

		}

		else 

		{
		     var today = new Date(); 
			 var monthfield=document.frmproduct.sp_ed_dt.value.split("-")[0]
             var dayfield=document.frmproduct.sp_ed_dt.value.split("-")[1]
             var yearfield=document.frmproduct.sp_ed_dt.value.split("-")[2]
             var dayobj = new Date(yearfield, monthfield-1, dayfield)
			 var newDate = new Date(dayobj.getFullYear(), dayobj.getMonth(), dayobj.getDate()+1); 
            
			 
			  
			 
			 if(newDate<today)
			 {
			 
			    			  
				  document.getElementById("lblsp_ed_dt").innerHTML="Please enter present or future date";

			      return false;
			 
			 }
	
	         else
			 {
			 	
				  
				   document.getElementById("lblsp_ed_dt").innerHTML="";

			      return true;
				
			 }
			 

		}

	}

</script>


</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>

                

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>
                  
                 
                  
                  <tr>

                    <td class="pagetitle">Shopping Cart <?php  echo $product_info["name"]?>  (<?php  echo $info["growers_name"]?>) </td>

                  </tr>


                   <tr><td>&nbsp;</td></tr>
                            <tr>

                    <td>

					<table width="100%">

					<tr>

					<td>

					<a class="pagetitle1" href="growers_orders_mgmt.php" onclick="this.blur();"><span> Growers Orders</span></a>

					</td>

					</tr>

					</table>

					</td>

                  </tr>
                            <tr><td>&nbsp;</td></tr>
                  
                  
                                   
                  <tr>

                    <td>
                    <form action="" id="form1" method="post"><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

	<tr>
			
	   <th width="30%" align="left">Product</th>
            
           <th width="30%" align="left">Destiny</th>
            
           <th width="13%" align="left">LFD</th>
                                    
           <th width="13%" align="left"> Qty Ordered</th>
            
	</tr>
	</thead>

	<tbody>

		<?php

						  	$sr=1;

							 $temp=explode(",",$info["products"]); 
							 
							                           												  
						     while($product=mysqli_fetch_array($rs))		  {
                                                         
                                                         
                                                            $getCountry = "SELECT * FROM country WHERE id ='" . $product["country"] . "'";
                                                            $countryRes = mysqli_query($con, $getCountry);
                                                            $country = mysqli_fetch_assoc($countryRes);  
                                                         
						  
						      $sel_quality_grade = "select qg.name as quality_grade,q.id as quality_grade_id from grower_product_quality q left join quality qg on q.quality=qg.id where q.grower_id='".$_GET["id"]."' and q.product_id='".$product["id"]."'";
							  
							  $sel_color="select name from colors where id='".$product["color_id"]."'";
							  $rs_color=mysqli_query($con,$sel_color);
							  $color=mysqli_fetch_array($rs_color);
							  
							  $sel_category="select name from category where id='".$product["categoryid"]."'";
							  $rs_category=mysqli_query($con,$sel_category);
							  $category=mysqli_fetch_array($rs_category);
							  
							  $sel_subcategory="select name from subcategory where id='".$product["subcategoryid"]."'";
							  $rs_subcategory=mysqli_query($con,$sel_subcategory);
							  $subcategory=mysqli_fetch_array($rs_subcategory);
							  

						  ?>

                    <tr class="gradeU">                          

                          <td class="text" align="left"><?php  echo $product["name"]?></td>
                                                    
                          <td class="text" align="left"><?php  echo $country["name"]?> </td>
                           
                          <td class="text" align="left"><?php  echo $product["lfd"]?> </td>                                                                                                       
                                                                             
                          
                          <td class="text" align="center">
                            <input type="text" name="<?php  echo $sr?>-price" value="<?php  echo $product["req_grow"]?>" size="10" style="width:70px;"  />
                            <input type="hidden" name="<?php  echo $sr?>-prodcutid" value="<?php  echo $product["rgid"]?>"  />
                          </td>                                                
                         
                    </tr>

						 <?php
						 
						      $sr++;
							                                							  												  
									 		
			}
													
			?> 
                         	

	</tbody>

</table>



			</div>

			</div>

<div style="float:right;margin-top:20px;">
    <input type="hidden" name="total" value="<?php  echo $sr?>"  />
<input name="Submit" type="Submit" class="buttongrey" value="Update Cart" />
</div>

			</div></form>
		            </td>

                  </tr>

                </table>
                         </td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
