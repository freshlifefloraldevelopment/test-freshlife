<?php

// PO 2018-09-21 Cambio Eduardo

require_once("../config/config_gcp.php");
session_start();

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  
			
if(isset($_GET['delete'])) {
	  $query = 'DELETE FROM growers WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);	
}

	$qsel="SELECT io.id_fact         , io.buyer_id         , io.order_number   , io.order_date     , io.shipping_method , io.del_date   , io.date_range , 
                      io.is_pending      , io.order_serial     , io.seen           , io.delivery_dates , io.lfd_grower      , io.quick_desc , io.bill_number, 
                      io.gross_weight    , io.volume_weight    , io.freight_value  , io.guide_number   , io.total_boxes     , 
                      io.sub_total_amount, io.tax_rate         , io.shipping_charge, io.handling       , io.grand_total     ,
                      io.bill_state      , io.date_added       , io.user_added     , bo.availability   ,
                      io.air_waybill     , io.charges_due_agent,
                      io.credit_card_fees, io.per_kg           ,
                      b.first_name       , b.last_name
                 FROM invoice_orders io
                 LEFT JOIN buyers b  ON io.buyer_id = b.id  
                 LEFT JOIN buyer_orders bo  ON io.id_fact = bo.id  
                where io.date_added > date_add(curdate(), INTERVAL -30 DAY) ";	

	 $rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Manage Packing List</td>
                  </tr>
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>
                    <th width="5%" align="left">Num</th>             
                    <th width="22%" align="left">Customer</th>             
                    <th width="15%" align="left">Order Num</th>             
		</tr>

</thead>

	<tbody>
		<?php

		  	$sr=1;
        		  while($product=mysqli_fetch_array($rs))  {	
                              
                            if ($product["availability"] == "1") {
                                    $market = "Published";
                            }elseif ($product["availability"] == 2) {
                                    $market = "Packing Load (Ready)";                                                                            
                            }elseif ($product["availability"] == 0) {
                                     $market = "Not Ready)";                                                                                                                    
                            }elseif ($product["availability"] == 4) {
                                     $market = "Completed)";                                                                                                                                                            
                            }elseif ($product["availability"] == 5) {
                                     $market = "Quick-Books)";                                                                                                                                                            
                                     
                            }else{
                                    $market = "Not-Availab.";
                              }
		?>
                          <tr class="gradeU">   
                                <td class="text" align="left"><?php echo $product["id_fact"]?>  </td>                                                                                                                
                                <td class="text" align="left"><?php echo $product["first_name"]?> <?php echo $product["last_name"]?> </td>                                                  
                                <td class="text" align="left"><?php echo $product["order_number"]?>  </td>                                                                                  
                                <td class="text" align="left"><?php echo $market?>  </td>                                                                                                                  
                                <td class="text" align="center"><a href="upload_invoice_grow.php?id_fact=<?php echo $product["id_fact"]?>" >Generate</a> </td>	                                                                
                                <td class="text" align="center"><a href="manage_packing_list.php?id_fact=<?php echo $product["id_fact"]?>" >Packing</a> </td>	                                
                                <td class="text" align="center"><a href="print_packing.php?b=<?php echo $product["id_fact"]."&id_buy=".$product["buyer_id"]?> "  >Report</a> </td>                                
                                <td class="text" align="center"><a href="print_packing4.php?b=<?php echo $product["id_fact"]."&id_buy=".$product["buyer_id"]?> "  >Grower</a> </td>                                                                                                
                                <td class="text" align="center"><a href="print_packing5.php?b=<?php echo $product["id_fact"]."&id_buy=".$product["buyer_id"]?> "  >SubCli</a> </td>                                                                
                                <!--td class="text" align="center"><a href="print_packing6.php?b=<?php echo $product["id_fact"]."&id_buy=".$product["buyer_id"]?> "  >Not Asig Grow</a> </td-->                                                                                                
                                <td class="text" align="center"><a href="print_packing7.php?b=<?php echo $product["id_fact"]."&id_buy=".$product["buyer_id"]?> "  >Not Asigned</a> </td>  
                                
                                <td class="text" align="center"><a href="print_packing_customer_total.php?b=<?php echo $product["id_fact"]."&id_buy=".$product["buyer_id"]?> "  >N.A.Tot</a> </td>                                  
                                
                                
                                <td align="center" ><a href="packing_availability_edit.php?id_fact=<?php echo $product["id_fact"] ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>                                
                                
                          </tr>
				 <?php
			     $sr++;
			   } ?> 	
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>