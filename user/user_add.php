<?php
/**
Developer educristo@gmail.com
Start 15 Sep 2020
Analyze Frond-end, Bac-End
Add Protection SQL INY, XSS
**/
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";


    session_start();

    if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
    {
    	header("location: index.php");
    }
    if(isset($_POST["Submit"]) && $_POST["Submit"]=="Add")
    {
        for($i=1;$i<=24;$i++)
	{
            if($_POST["destination".$i.""]!="")
            {
		$destinationid.=$_POST["destination".$i.""];
		$destinationid.=",";
            }
	}

        $destinationid=trim($destinationid, ",");
	$ins="insert into admin set uname='".$_POST["uname"]."',pass='".$_POST["pass"]."',rights='".$destinationid."',type='User',isadmin='".$_POST["isadmin"]."'";
	mysqli_query($con,$ins);
	header('location:users_mgmt.php');
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript">

	function verify()

	{

		var arrTmp=new Array();

		arrTmp[0]=checkuname();

		arrTmp[1]=checkpass();



		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)

		{

			if(arrTmp[i]==false)

			{

			   _blk=false;

			}

		}

		if(_blk==true)

		{

			return true;

		}

		else

		{

			return false;

		}



 	}





	function validImageFile(strfile)

	{

		var str = strfile;

		var pathLenth = strfile.length;

		var start = (str.lastIndexOf("."));

		var fileType = str.slice(start,pathLenth);

		fileType = fileType.toLowerCase();

		if (strfile.length > 0)

		{

		   if((fileType == ".gif") || (fileType == ".jpg") || (fileType == ".jpeg") || (fileType == ".png") || (fileType == ".bmp") || (fileType == ".GIF") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".PNG") || (fileType == ".BMP"))

		   {

				return true;

		   }

		   else

		   {

				return false;

		   }

		}

	}



	function trim(str)

	{

		if (str != null)

		{

			var i;

			for (i=0; i<str.length; i++)

			{

				if (str.charAt(i)!=" ")

				{

					str=str.substring(i,str.length);

					break;

				}

			}

			for (i=str.length-1; i>=0; i--)

			{

				if (str.charAt(i)!=" ")

				{

					str=str.substring(0,i+1);

					break;

				}

			}

			if (str.charAt(0)==" ")

			{

				return "";

			}

			else

			{

				return str;

			}

		}

	}



	function checkuname()

	{

		if(trim(document.frmcat.uname.value) == "")

		{

			document.getElementById("lbluname").innerHTML="Please enter username";

			return false;

		}

		else

		{

			document.getElementById("lbluname").innerHTML="";

			return true;

		}

	}



	function checkpass()



	{

		if(trim(document.frmcat.pass.value) == "")

		{

			document.getElementById("lblpass").innerHTML="Please enter password";

			return false;

		}

		else

		{

			document.getElementById("lblpass").innerHTML="";

			return true;

		}

	}



	function checkimage()

	{

		if(trim(document.frmcat.image.value) == "")

		{

			document.getElementById("lblimage").innerHTML="Please upload image";

			return false;

		}

		else

		{

			if(!validImageFile(document.frmcat.image.value))

			{

				document.getElementById("lblimage").innerHTML="Please select valid image file";

				return false;

			}

			else

			{

				document.getElementById("lblimage").innerHTML="";

				return true;

			}

		}

	}

</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Add New User</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td><a class="pagetitle1" href="users_mgmt.php" onclick="this.blur();"><span> Manage Users</span></a> </td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                            <tr>
                              <td><div id="box">
                                  <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                    <tr>
                                      <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Username</td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="uname" id="uname" value="<?php echo $_POST["uname"]?>" />
                                        <br/>
                                        &nbsp;&nbsp;<b>(Please Enter First Name Only . Dont Use Space)</b> <br>
                                        <span class="error" id="lbluname"></span> </td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Password</td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="pass" id="pass" value="<?php echo $_POST["pass"]?>" />
                                        <br>
                                        <span class="error" id="lblpass"></span> </td>
                                    </tr>

                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Is Admin</td>
                                      <td width="66%" bgcolor="#f2f2f2"><select name="isadmin" id="isadmin">
                                      <option value="0">No</option>
                                      <option value="1">Yes</option>
                                      </select>
                                        <br>
                                        <span class="error" id="lblisadmin"></span> </td>
                                    </tr>

                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Permission</td>
                                      <td width="66%" bgcolor="#f2f2f2"><div>
                                          <input type="checkbox" name="destination1" value="Manage CMS Pages"  />
                                          <b> Manage CMS Pages </b> <br/>
                                          <input type="checkbox" name="destination2" value="Manage Slideshow"  />
                                          <b> Manage Slideshow </b> <br/>
                                          <input type="checkbox" name="destination3" value="Manage Home Boxes"  />
                                          <b> Manage Home Boxes </b> <br/>
                                          <input type="checkbox" name="destination4" value="Manage Testimonials "  />
                                          <b> Manage Testimonials </b> <br/>
                                          <input type="checkbox" name="destination5" value="Manage Product Categories"  />
                                          <b> Manage Product Categories </b> <br/>
                                          <input type="checkbox" name="destination6" value="Manage Sub Categories"  />
                                          <b> Manage Sub Categories </b> <br/>
                                          <input type="checkbox" name="destination7" value="Manage Colors"  />
                                          <b> Manage Colors </b> <br/>
                                          <input type="checkbox" name="destination8" value="Manage Sizes"  />
                                          <b> Manage Sizes </b> <br/>
                                           <input type="checkbox" name="destination18" value="Manage Coutries"  />
                                          <b> Manage Coutries </b> <br/>

                                          <input type="checkbox" name="destination9" value="Manage Products"  />
                                          <b> Manage Products </b> <br/>

                                          <input type="checkbox" name="destination24" value="Manage Box Types"  />
                                          <b> Manage Box Types </b> <br/>

                                          <input type="checkbox" name="destination14" value="Manage Boxes"  />
                                          <b> Manage Boxes </b> <br/>

                                           <input type="checkbox" name="destination15" value="Manage Bunch Sizes"  />
                                          <b> Manage Bunch Sizes </b> <br/>
                                          <input type="checkbox" name="destination10" value="Manage Growers"  />
                                          <b> Manage Growers </b> <br/>
                                          <input type="checkbox" name="destination16" value="Manage Grower Boxes"  />
                                          <b> Manage Grower Boxes </b> <br/>

                                            <input type="checkbox" name="destination17" value="Manage Farms List"  />
                                          <b> Manage Farms List </b> <br/>

                                             <input type="checkbox" name="destination19" value="Manage Special Features"  />
                                          <b> Manage Special Features </b> <br/>

                                            <input type="checkbox" name="destination20" value="Product Special Features"  />
                                          <b> Product Special Features </b> <br/>

                                           <input type="checkbox" name="destination21" value="Manage Certificates"  />
                                          <b> Manage Certificates </b> <br/>

                                           <input type="checkbox" name="destination22" value="Grower Certificates"  />
                                          <b> Grower Certificates </b> <br/>

                                          <input type="checkbox" name="destination11" value="Manage Users"  />
                                          <b> Manage Users</b> <br/>
                                          <input type="checkbox" name="destination12" value="Manage Quality Grade"  />
                                          <b> Manage Quality Grade</b> <br/>
                                          <input type="checkbox" name="destination13" value="Manage Default Prices"  />
                                          <b> Manage Default Prices</b> <br/>

                                           <input type="checkbox" name="destination23" value="Manage Buyers"  />
                                          <b> Manage Buyers</b> <br/>
                                          <input type="checkbox" name="destination24" value="Manage Agents"  />
                                          <b> Manage Agents </b> <br/>
                                        </div></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input name="Submit" type="Submit" class="buttongrey" value="Add" />
                                      </td>
                                    </tr>
                                  </table>
                                </div></td>
                            </tr>
                          </form>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
