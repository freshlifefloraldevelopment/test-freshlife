
<?php

// PSR-4 Autoloader,QuickBooksOnline\Data;
spl_autoload_register(function ($class) {
	  $prefix = 'QuickBooksOnline\\API\\';
		$base_dir = __DIR__ . DIRECTORY_SEPARATOR;
          //$base_dir = '/home/portega/';
		$len = strlen($prefix);
                
  if (strncmp($prefix, $class, $len) !== 0) {
        return;
  }
		$relative_class = substr($class, $len);
		$filewithoutExtension = $base_dir . str_replace('\\', '/', $relative_class);
		$file =  $filewithoutExtension. '.php';

		//Below str_replace is for local testing. Remove it before putting on Composer.
	  if (file_exists($file) ) {
	        require ($file);
	   }
	});


/*
return array(
    'authorizationRequestUrl' => 'https://appcenter.intuit.com/connect/oauth2',
    'tokenEndPointUrl' => 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
    'client_id' => 'ABYeMIS4swFuYmDzT22VU009aZDFnSpGYizp57KfPIP294Y7BX',
    'client_secret' => 'C7vcbZEfnkAwdPDiTSJUReegLzAR7OgjBg8uBOIM',
    'oauth_scope' => 'com.intuit.quickbooks.accounting openid profile email phone address',
    'oauth_redirect_uri' => 'https://app.freshlifefloral.com/user/quickbooks/callback.php',
)
 
 */
?>
