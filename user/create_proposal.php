<?	
    include "../config/config.php";
    session_start();
    if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
    {
    	header("location: index.php");
    }

    if($_SESSION['grower_id']!=0)
    {
        header("location: growers.php?id=".$_SESSION['grower_id']);
    }  
	
    if(isset($_GET['delete'])) 
    {
      $query = 'DELETE FROM grower_offer_reply  WHERE  id= '.(int)$_GET['delete'];
      mysql_query($query);

    }
    
    $qsel="select id, first_name, last_name from buyers order by level";
    
    $rs=mysql_query($qsel);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Admin Area</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
    <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#example').dataTable({
                //"sScrollXInner": "130%",
                "bJQueryUI": true,
                //"sScrollY": "536",
                "sPaginationType": "full_numbers"
            });
        } );
    </script>
</head>
<body>
    <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
        <? include("includes/header_inner.php");?>
        <tr>
            <td height="5"></td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <? include("includes/left.php");?>
                        <td width="5">&nbsp;</td>
                        <td valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                    <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="10">&nbsp;</td>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td height="5"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="pagetitle">Create Proposal</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <a href="agent_mgmt.php" class="pagetitle" style="text-decoration: underline;" ><span> Manage Agents</span></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-transform: capitalize; text-align: center; padding-top: 20px;">list of the customers</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="box">
                                                                    <div id="container">			
                                                                        <div class="demo_jui">
                                                                            <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th width="10%" align="left">Customer</th> 
                                                                                        <th width="22%" align="left">View</th> 
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody><?
                                                                                    while($product=mysql_fetch_array($rs))
                                                                                    {
                                                                                        
                                                                                        ?><tr class="gradeU">
                                                                                            <td align="left"><?=$product["first_name"]?> <?=$product["last_name"]?> </td>
                                                                                            <td align="center" ><a href="purchase_order.php?id=<?=$product["id"]?>">View</a></td>
                                                                                        </tr><?
                                                                                    }
                                                                                ?></tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="margin-top:20px;">
                                                                    <form method="GET" action="invoice_make.php"> <input type="hidden" name="ids" id="ids" value="<?=$ids?>"  />
                                                                        <input type="hidden" name="b" id="b" value="<?=$_GET["b"]?>"  />
                                                                        <input type="hidden" name="lfd" id="lfd" value="<?=$_GET["lfd"]?>"  />
                                                                        <input type="submit" value="Generate Invoice"  /> 
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                </tr>
                                <tr>
                                    <td background="images/middle-leftline.gif"></td>
                                    <td>&nbsp;</td>
                                    <td background="images/middle-rightline.gif"></td>
                                </tr>
                                <tr>
                                    <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                    <td background="images/middle-bottomline.gif"></td>
                                    <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="10"></td>
        </tr>
        <? include("includes/footer-inner.php"); ?>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</body>
</html>
