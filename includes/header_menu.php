<?php
$sqlCat = "SELECT gp.categoryid, c.name,c.id
                FROM grower_product gp 
                LEFT JOIN growers g ON gp.grower_id=g.id
                LEFT JOIN category c ON gp.categoryid=c.id
                WHERE g.active='active' AND gp.categoryid!=23 
                GROUP BY gp.categoryid ORDER BY c.name";

$categoryData = mysqli_query($con, $sqlCat);
?>
<div id="header" class="sticky clearfix">

    <!-- SEARCH HEADER -->
    <div class="search-box over-header">
        <a id="closeSearch" href="#" class="glyphicon glyphicon-remove"></a>

        <form action="" method="get">
            <input type="text" class="form-control" placeholder="SEARCH" id="searchid3"/>
        </form>
        <div id="result3" class="sky-form"></div>
    </div> 
    <!-- /SEARCH HEADER -->

    <!-- TOP NAV -->
    <header id="topNav">
        <div class="container">

            <!-- Mobile Menu Button -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>

            <!-- BUTTONS -->
            <ul class="pull-right nav nav-pills nav-second-main">

                <!-- SEARCH -->
                <li class="search">
                    <a href="javascript:;">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <!-- /SEARCH -->
                <li class="quick-cart">
					<a href="javascript:;">
					<?php
						if($_SESSION['cart_products'] != null){
							$total_items = 0;
							foreach($_SESSION['cart_products'] as $k => $v){
								$total_items += $v['bunch_size'];
							}
						}
					?>
						<span class="badge badge-aqua btn-xs badge-corner"><?php echo $total_items; ?></span>
						<i class="fa fa-shopping-cart"></i> 
					</a>
					<div class="quick-cart-box">
						<h4>Shopping Cart</h4>

						<div class="quick-cart-wrapper">
							<?php
								if($_SESSION['cart_products'] != null){
									$tempArr = array();
									$total_price = 0;
									foreach($_SESSION['cart_products'] as $k => $v){
										$total_price += $v['bunch_size']*$v['product_price'];												
							?>
							<a href="#">
								<img src="<?php echo SITE_URL.$v['product_image']; ?>" width="45" height="45" alt="Product">
								<h6><span><?php echo $v['bunch_size']; echo ($v['is_bunch'] == 1)? ' bunches' : ' stems'; ?> x </span> <?php echo $v['product_name']; ?></h6>
								<small>$<?php echo $v['product_price']; ?></small>
							</a>
							
						<?php
							}
						?>

						</div>

						<!-- quick cart footer -->
						<div class="quick-cart-footer clearfix">
							<div>
								<a href="<?php echo SITE_URL; ?>cart/cart.php" class="btn btn-primary btn-xs pull-right">VIEW CART</a>
								<span class="pull-left"><strong>TOTAL:</strong> $<?php echo $total_price; ?></span>
							</div>
							
						</div>
						<div style="margin-top:10px; padding:0 50px">
							<a href="<?php echo SITE_URL; ?>cart/process_cart.php?empty_cart=1" class="btn btn-primary btn-xs center">EMPTY CART</a>
						</div>
						<!-- /quick cart footer -->
						<?php
							}
							else{
								echo '<p style="margin-top:10px; color: #CD0000; padding:2px 7px;">Please add some items in your cart.</p>';
							}
						?>

					</div>
				</li>
            </ul>
            <!-- /BUTTONS -->


            <!-- Logo -->
            <a class="logo pull-left" href="http://www.freshlifefloral.com/">
                <img src="<?php echo SITE_URL; ?>assets/images/logo/logo.png" alt="" class="fresh_logo"/>
            </a>

            <!-- 
                    Top Nav 
                    
                    AVAILABLE CLASSES:
                    submenu-dark = dark sub menu
            include("file/dynamic_menu.php")
            -->
        </div>
    </header>
    <!-- /Top Nav -->

</div>
