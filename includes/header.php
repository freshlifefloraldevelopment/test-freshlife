<?php
if ($_SERVER['SERVER_ADDR'] != '50.62.68.1') {
    @session_start();
    if ($_SESSION['client_session'] != 1) {

        if ($_SERVER['SERVER_ADDR'] == '50.62.68.1') {

            header("Location:https://freshlifefloral.com"); // Uncomment when completed
        } else if ($_SERVER['SERVER_ADDR'] == '35.227.110.85') {
            header("Location:https://app.freshlifefloral.com");

        }
    }
}
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>
    <title><?php echo ($pageData["meta_title"] != '') ? $pageData["meta_title"] : $pageData["page_title"]; ?></title>
    <meta name="keywords" content="<?php echo $pageData["meta_keyword"]; ?>"/>
    <meta name="description" content="<?php echo $pageData["meta_desc"]; ?>">
    <meta name="robots" content="noindex,nofollow">
    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <link rel="shortcut icon" type="image/ico" href="<?php echo SITE_URL; ?>images/favicon.ico"/>
    <!-- WEB FONTS : use %7C instead of | (pipe) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css"/>

    <!-- CORE CSS -->
        <link href="<?php echo SITE_URL; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>


    <?php
// www
    ####PRINT CSS LINKS#####################
    foreach ($cssHeadArray as $cssSrc) {
        echo "\t" . "\t" . '<link href="' . $cssSrc . '" rel="stylesheet" type="text/css" />' . "\n";
    }

    ####PRINT JS LINKS#####################
    foreach ($jsHeadArray as $jsSrc) {
        echo "\t" . "\t" . '<script type="text/javascript" src="' . $jsSrc . '" ></script>' . "\n";
    }
    ?>

    <style>
        .dropdown-menu {
            display: important;
        }

    </style>
</head>
<body class="smoothscroll enable-animation">
<!-- SLIDE TOP -->

<?php
if (0) {
    ?>
    <div id="slidetop">

        <div class="container">

            <div class="row">

                <div class="col-md-4">
                    <h6><i class="icon-heart"></i> WHY SMARTY?</h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. </p>
                </div>

                <div class="col-md-4">
                    <h6><i class="icon-attachment"></i> RECENTLY VISITED</h6>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> This is a very long text, very very very very very very very very very very very very </a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Lorem ipsum dolor sit amet</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Dolor sit amet,consectetur adipiscing elit amet</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet,consectetur adipiscing elit</a></li>
                    </ul>
                </div>

                <div class="col-md-4">
                    <h6><i class="icon-envelope"></i> CONTACT INFO</h6>
                    <ul class="list-unstyled">
                        <li><b>Address:</b> PO Box 21132, Here Weare St, <br/> Melbourne, Vivas 2355 Australia</li>
                        <li><b>Phone:</b> 1-800-565-2390</li>
                        <li><b>Email:</b> <a href="mailto:support@yourname.com">support@yourname.com</a></li>
                    </ul>
                </div>

            </div>

        </div>

        <a class="slidetop-toggle" href="#"><!-- toggle button --></a>

    </div>
    <?php
}
?>
<!-- /SLIDE TOP -->

<!-- wrapper -->
<div id="wrapper">

    <!-- Top Bar -->
    <div id="topBar">
        <div class="container">

            <!-- right -->
            <!--Added the login condition fo task BR-121-->
            <div class="pull-right size-14">
                <ul class="top-links list-inline pull-right">
                    <?php if ($_SESSION["login"] != 1) { ?>
                        <li><a href="<?= SITE_URL ?>login.php">LOGIN</a></li>
                        <li><a href="https://content.freshlifefloral.com/register-form">REGISTER</a></li>
                    <?php } else { ?>
                        <li><a href="<?= SITE_URL ?>sign-out.php">LOGOUT</a></li>
                        <li><a href="<?= SITE_URL ?>buyer/buyers-account.php">MY ACCOUNT</a></li>
                    <?php } ?>
                </ul>
            </div>

            <!-- left -->
            <ul class="top-links list-inline">
                <li>
                    <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><img class="flag-lang" src="<?php echo SITE_URL; ?>assets/images/flags/us.png" width="16" height="11" alt="lang"/> ENGLISH</a>
                    <ul class="dropdown-langs dropdown-menu pull-right">
                        <li><a tabindex="-1" href="#"><img class="flag-lang" src="<?php echo SITE_URL; ?>assets/images/flags/us.png" width="16" height="11" alt="lang"/> ENGLISH</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="#"><img class="flag-lang" src="<?php echo SITE_URL; ?>assets/images/flags/de.png" width="16" height="11" alt="lang"/> GERMAN</a></li>
                        <li><a tabindex="-1" href="#"><img class="flag-lang" src="<?php echo SITE_URL; ?>assets/images/flags/ru.png" width="16" height="11" alt="lang"/> RUSSIAN</a></li>
                        <li><a tabindex="-1" href="#"><img class="flag-lang" src="<?php echo SITE_URL; ?>assets/images/flags/it.png" width="16" height="11" alt="lang"/> ITALIAN</a></li>
                    </ul>

                </li>
            </ul>

        </div>
    </div>
    <!-- /Top Bar -->
    <script type="text/javascript" src="<?php echo SITE_URL; ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
       <script src="<?php echo SITE_URL ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <?php include("includes/header_menu.php"); ?>

    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/295496.js"></script>
    <!-- End of HubSpot Embed Code -->
