<aside id="aside">
    <nav id="sideNav"><!-- MAIN MENU -->
        <ul class="nav nav-list">
            <li <?php if($page_request=='dashboard'){ ?> class="active" <?php } ?>><!-- dashboard -->
                <a class="dashboard" href="<?php echo SITE_URL; ?>file/vendor-account.php"><!-- warning - url used by default by ajax (if eneabled) -->
                    <i class="main-icon fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <!--<li>-->

            <li <?php if($page_request=='received_offers'){ ?> class="active" <?php } ?>>
                <a href="<?php echo SITE_URL; ?>growers/growers-recieved-offers.php">
                    <i class="main-icon fa fa-cart-plus"></i> <span>Buyer Requests</span>
                        </a>
            </li>
            <li <?php if($page_request=='sent_offers'){ ?> class="active" <?php } ?>>
                <a href="<?php echo SITE_URL; ?>growers/sent-offers.php">
                    <i class="main-icon fa fa-check-square-o"></i> <span>Offers Status</span>
                        </a>
            </li>
            <li <?php if($page_request=='update_stock'){ ?> class="active" <?php } ?>>
                <a href="<?php echo SITE_URL; ?>file/update-stock.php">
                    <i class="main-icon fa fa-retweet"></i> <span>Update Stock</span>
                        </a>
            </li>
        </ul>
        <!-- SECOND MAIN LIST -->
    </nav>
    <span id="asidebg"><!-- aside fixed background --></span>
</aside>
<!-- HEADER -->
    <header id="header">
      <!-- Mobile Button -->
      <button id="mobileMenuBtn"></button>
      <!-- Logo -->
      <span class="logo pull-left"><a href="<?= SITE_URL ?>"> <img src="/includes/assets/images/logo.png" alt="admin panel" height="35" /></a> </span>
      <form method="get" action="page-search.html" class="search pull-left hidden-xs">
        <input type="text" class="form-control" name="k" placeholder="Search for something..." />
      </form>
      <nav>
        <!-- OPTIONS LIST -->
        <ul class="nav pull-right">
          <!-- USER OPTIONS -->
          <li class="dropdown pull-left"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <img class="user-avatar" alt="" src="<?php echo "/".$img_url;?>" height="34" /> <span class="user-name"> <span class="hidden-xs"> 
		  <?php echo $growers_name; ?><i class="fa fa-angle-down"></i> </span> </span> </a>
            <ul class="dropdown-menu hold-on-click">
              <li>
                <!-- logout -->
                <a href="<?= SITE_URL ?>file/sign-out.php"><i class="fa fa-power-off"></i> Log Out</a> </li>
            </ul>
          </li>
          <!-- /USER OPTIONS -->
        </ul>
        <!-- /OPTIONS LIST -->
      </nav>
    </header>
    <!-- /HEADER -->
