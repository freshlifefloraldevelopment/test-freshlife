<?php
$sel_default_meta = "select * from page_mgmt where page_id=12";//footer page id
$rs_default_meta = mysqli_query($con, $sel_default_meta);
$default_meta = mysqli_fetch_assoc($rs_default_meta);
?>
<!-- FOOTER -->
<footer id="footer">
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <!-- Footer Logo -->
                <img id="logo-footer" class="footer-logo" src="<?php echo SITE_URL; ?>includes/assets/images/logo_new.png"/>

                <!-- Small Description -->
                <p><?php echo $default_meta['para_1']; ?></p>
                <p><?php echo $default_meta['para_2']; ?></p>

                <!-- Contact Address -->
                <address>
                    <ul class="list-unstyled">
                        <li class="footer-sprite address">
                            <!--PO Box 21132<br>-->
                            <?php echo $default_meta['add1']; ?><br>
                            <?php echo $default_meta['add2']; ?><br>
                        </li>
                        <li class="footer-sprite phone">
                            Phone: <?php echo $default_meta['phone']; ?>
                        </li>
                        <li class="footer-sprite email">
                            <a href="mailto:<?php echo $default_meta['email']; ?>"><?php echo $default_meta['email']; ?></a>
                        </li>
                    </ul>
                </address>
                <!-- /Contact Address -->

            </div>

            <div class="col-md-3">

                <!-- Latest Blog Post (falta codigo) -->

                <!-- /Latest Blog Post -->

            </div>

            <div class="col-md-2">

                <!-- Links -->
                <h4 class="letter-spacing-1">EXPLORE SMARTY</h4>

                <ul class="footer-links list-unstyled">
                    <li><a href="http://www.freshlifefloral.com/" target="_blank">HOME</a></li>
                    <li><a href="http://www.freshlifefloral.com/about-us">ABOUT US</a></li>
                    <li><a href="http://www.freshlifefloral.com/contact-us">CONTACT US</a></li>
                    <li><a href="http://www.freshlifefloral.com/faqs">FAQ's</a></li>
                </ul>
                <!-- /Links -->

            </div>

            <div class="col-md-4">

                <!-- Newsletter Form -->
                <h4 class="letter-spacing-1">KEEP IN TOUCH</h4>
                <p>Subscribe to Our Newsletter to get Important News &amp; Offers</p>

                <form class="validate" action="/php/newsletter.php" method="post" data-success="Subscribed! Thank you!" data-toastr-position="bottom-right" novalidate="novalidate">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="email" name="email" class="form-control required" placeholder="Enter your Email">
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Subscribe</button>
                        </span>
                    </div>
                    <input type="hidden" name="is_ajax" value="true">
                </form>
                <!-- /Newsletter Form -->

                <!-- Social Icons -->
                <?php
                $sel_social_data = "select * from page_mgmt where page_id=22";//footer page id
                $rs_default_meta = mysqli_query($con, $sel_social_data);
                $social_url = mysqli_fetch_assoc($rs_default_meta);
                ?>
                <div class="margin-top-20">
                    <a href="<?php echo $social_url['facebook_link']; ?>" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook" target="_blank">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>

                    <a href="<?php echo $social_url['twitter_link']; ?>" class="social-icon social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter" target="_blank">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>

                    <a href="<?php echo $social_url['googleplus_link']; ?>" class="social-icon social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google plus" target="_blank">
                        <i class="icon-gplus"></i>
                        <i class="icon-gplus"></i>
                    </a>

                    <a href="<?php echo $social_url['linkedin_link']; ?>" class="social-icon social-icon-border social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin" target="_blank">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>

                    <a href="<?php echo $social_url['rss_link']; ?>" class="social-icon social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Rss" target="_blank">
                        <i class="icon-rss"></i>
                        <i class="icon-rss"></i>
                    </a>


                </div>

                <div class="margin-top">
                    <br>
                    <br>
                    <br>

                    <!--<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=qf2O19EmgRyK7NNLUJt9Kd2ed9NqNkVWKjlwhadkJx7Ty6Y9Sy2Ltu9Lsqy2"></script></span>-->


                </div>

                <!-- /Social Icons -->

            </div>

        </div>

    </div>

    <div class="copyright">
        <div class="container">
            <ul class="pull-right nomargin list-inline mobile-block">
                <li><a href="<?php echo SITE_URL; ?>terms-and-condition.php">Terms &amp; Conditions</a></li>
                <li>•</li>
                <li><a href="<?php echo SITE_URL; ?>privacy.php">Privacy</a></li>
            </ul>
            © All Rights Reserved, Company LTD
        </div>
    </div>
</footer>
<!-- /FOOTER -->

</div>
<!-- /wrapper -->

<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>

<!-- PRELOADER -->
<div id="preloader">
    <div class="inner">
        <span class="loader"></span>
    </div>
</div><!-- /PRELOADER -->


<!-- JAVASCRIPT FILES -->



<script type="text/javascript">var plugin_path = '<?php echo SITE_URL; ?>includes/assets/plugins/';</script>
<script type="text/javascript" src="<?php echo SITE_URL; ?>includes/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL; ?>includes/assets/js/scripts.js"></script>








</body>
</html>
