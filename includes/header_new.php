<!doctype html>
<html lang="en-US">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Smarty Admin</title>
        <meta name="description" content="" />
        <meta name="robots" content="noindex,nofollow">
        <meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />
        <!-- mobile settings -->
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
        <!-- WEB FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />
        <!-- CORE CSS -->
        <link href="<?php echo SITE_URL; ?>includes/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- THEME CSS -->
        <link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_requestpage.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITE_URL; ?>includes/assets/css/layout_requestpage.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo SITE_URL; ?>includes/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
        <link href="<?php echo SITE_URL; ?>includes/assets/css/layout-shop.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script type="text/javascript" src="<?php echo SITE_URL; ?>includes/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
    </head>
    <body>
        <!-- WRAPPER -->
        <div id="wrapper">
