<?php
require_once("../config/config_gcp.php");

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	
    if (isset($_GET['idGrower']) && isset($_GET['idRequest']) )
    {      

// ver recues por finca 
	/*  $sel_user = "
	  
	  SELECT gpb.id ,
       s.name AS subs,
       p.name,
       sh.name AS sizename,
       gpb.comment, 
       gpb.lfd,
       gpb.qty,  
       un.code as Unit,
       IF(gpb.unseen>0,'OFFER','Pending') State   ,
       gpb.shpping_method    ,
(select c.name as country  from buyer_shipping_methods bms inner JOIN country c ON bms.country = c.id  where bms.shipping_method_id = gpb.shpping_method) country 
  FROM buyer_requests gpb
 INNER JOIN request_growers rg     ON gpb.id          = rg.rid
 INNER JOIN product p              ON gpb.product     = p.id
 INNER JOIN subcategory s          ON p.subcategoryid = s.id  
  LEFT JOIN features ff            ON gpb.feature     = ff.id
  LEFT JOIN grower_offer_reply gor ON rg.rid          = gor.offer_id AND rg.gid=gor.grower_id
  LEFT JOIN sizes sh               ON gpb.sizeid      = sh.id 
  LEFT JOIN units un               ON gpb.boxtype     = un.id 
 WHERE rg.gid='". $_GET['idGrower']. "'
   AND gpb.lfd>=curdate()
   AND IFNULL(gor.reject,'-1')<>'1'  
 GROUP BY gpb.id 
 ORDER BY gpb.id DESC";*/
 
 $sel_user = "
 SELECT gpb.id ,
       s.name AS subs,
       gor.product,
       sh.name AS sizename,
       gor.bunchqty,  
       gor.steams,
       gor.price,
       un.code as Unit,
       IF(gpb.unseen>0,'OFFER','Pending') State   ,
       gpb.shpping_method    ,
       g.growers_name 
  FROM buyer_requests gpb
 INNER JOIN request_growers rg     ON gpb.id          = rg.rid
 INNER JOIN product p              ON gpb.product     = p.id
 INNER JOIN subcategory s          ON p.subcategoryid = s.id  
  LEFT JOIN features ff            ON gpb.feature     = ff.id
  LEFT JOIN grower_offer_reply gor ON rg.rid          = gor.offer_id AND rg.gid=gor.grower_id
  LEFT JOIN growers g              ON gor.grower_id   = g.id
  LEFT JOIN sizes sh               ON gpb.sizeid      = sh.id 
  LEFT JOIN units un               ON gpb.boxtype     = un.id 
  WHERE rg.gid='". $_GET['idGrower']. "'
  AND gor.request_id = '". $_GET['idRequest']. "'
    AND gpb.lfd>=curdate()
 ORDER BY gor.id DESC
  ";
	  
	

  $rs_user = mysqli_query($con, $sel_user);
        $total_user = mysqli_num_rows($rs_user);
        if ($total_user >= 1) {
			while ($row_progress = $rs_user->fetch_array(MYSQLI_ASSOC)) {
			$myArray[] = $row_progress; 
			}


			$array = array("grower_offers"=>
			$myArray);




			header("HTTP/1.1 200 OK");
			echo json_encode( $array);
			exit();

	     
        }



		}
		}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	 $json = file_get_contents('php://input');
    $data = json_decode($json,true);
	
	 try {
	$offerid = "select max(id)+1 as id from grower_offer_reply";
        $rs_id = mysqli_query($con, $offerid);
     $oferId = mysqli_fetch_assoc($rs_id);

	  $sproducto = "
	 SELECT
 CONCAT( bs.name,'') as steams,
   p.subcategoryid as product_subcategory,
   s.name as code_size,
   gs.product_id  as idVarety,
   (select s.name from subcategory s  where  p.subcategoryid   = s.id )  as categoria,
p.name as producto
 FROM grower_product_bunch_sizes gs
 LEFT JOIN product AS p ON gs.product_id=p.id
 LEFT JOIN grower_product_box_packing gr ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes
AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
 LEFT JOIN sizes AS s ON gs.sizes=s.id
 LEFT JOIN boxes AS b ON b.id=gr.box_id
 LEFT JOIN boxtype AS bt ON b.type=bt.id
 LEFT JOIN features AS fe ON fe.id=gr.feature
 LEFT JOIN bunch_sizes AS bs ON gr.bunch_size_id=bs.id
 
WHERE gs.grower_id='".  $data["grower_id"]. "'
 AND gs.product_id= '".  $data["idVarety"]. "'";
 
   $rs_producto = mysqli_query($con, $sproducto);
     $producto = mysqli_fetch_assoc($rs_producto);

	// echo 'sd'.$producto["idVarety"];
	 
   
	$conn = new PDO('mysql:host=localhost;dbname=freshlifefloral', "freshms", "floral2018");
	$sql1="

					insert into grower_offer_reply set
					id ='" . $oferId['id'] . "',
					reject2 = 0,
					reason2 ='',
					reason ='',
					offer_id ='" . $data["idItem"] . "',
					offer_id_index ='1',
					grower_id ='" . $data["grower_id"] . "',
					buyer_id ='" . $data["buyer_id"] . "',
					boxtype= '" . $data["unit_code"] . "',
					grower_box_name =' ',
					box_weight ='0',
					box_volumn =0,
					shipping =0,
					handling ='0',
					tax ='0',
					totalprice =0,
					status = 0,
					type = 0,
					size ='" . $producto["code_size"] . "',
					product ='" . $producto["producto"] . "',
					price ='" . $data["precio"] . "',
					steams ='" . $producto["steams"] . "',
					totalprice_box = 0,
					bunchsize ='0',
					bunchqty ='" . $data["qty"] . "',
					boxqty =1,
					total_stems =0,
					totalstems_box = 0,
					total_price_st_bu ='0',
					atprice_box = 0,
					atqty_box = 0,
					finalprice = 0,
					billing_add = 0,
					coordination ='B',
					marks ='" . $data["user"] . "',
					cargo =' ',
					shipping_method ='" . $data["shipping_method"] . "',
					product_subcategory ='" . $producto["categoria"] . "',
					date_added ='" . date("Y-m-d") . "',
					accept_date ='" . date("Y-m-d") . "',
					invoice = 0 ,
					req_group = 0 ,
					unit = '" . $data["unit_code"] . "',
					cliente_id =0,
					request_id ='" . $data["idItem"] . "'";
  echo $sql1;
    $sql = $conn->prepare(

	$sql1);
							 
							 
						if($sql->execute()) {
					       	echo "Se ha creado el nuevo registro!";
						}
						
		
						
							
                            header("HTTP/1.1 200 OK");
                            exit();
     
		 } catch (PDOException  $e) {
	  echo "ddddd".$e->getMessage();
	}
	
  }
//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{

		

	 
  header("HTTP/1.1 200 OK");
  exit();
}
//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    
    header("HTTP/1.1 200 OK");
    exit();
}
//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");
?>
    
	 
	
