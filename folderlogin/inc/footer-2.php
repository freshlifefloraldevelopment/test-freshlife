
			<!-- INFO BOX -->
			<section class="py-0 border-bottom-xs">
				<div class="container py-2">

					<div class="row">

						<div class="col-12 col-lg-4 p--15 d-flex d-block-xs text-center-xs">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">

								<i class="fas fa-fan"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Location
								</h2>

								<p class="m-0">
									Av. Interoceanica OE6-73 y Gonzalez Suarez
 Quito, Ecuador
								</p>

							</div>

						</div>

						<div class="col-12 col-lg-4 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bt-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="/folderlogin/demo.files/svg/ecommerce/phone.svg" alt="Phone">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Phone
								</h2>

								<p class="m-0">
									Phone: +593 602 2630
								</p>

							</div>

						</div>

						<div class="col-12 col-lg-4 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bl-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="/folderlogin/demo.files/svg/ecommerce/undraw_confirmation_2uy0.svg" alt="Email">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Email address
								</h2>

								<p class="m-0">
								 info@freshlifefloral.com
								</p>

							</div>

						</div>



					</div>

				</div>
			</section>
			<!-- /INFO BOX -->




			<!-- Footer -->
			<footer id="footer" class="shadow-xs">


				<div class="border-top">
					<div class="container text-center py-5">

						<!-- logo -->
						<span class="h--70 d-inline-flex align-items-center">
							<img src="/includes/assets/images/logo_new.png" width="" height="" alt="Fresh Life Floral">
						</span>

						<p class="m-0 text-gray-500 fs--14">

							We ship daily a wide range of fresh flowers to wholesalers and importers across the USA, Australia, South Africa, Russia and many other parts of the world.
 <br>
We give our customers service and tailor made platforms, in order to boost mutual sales. <br>Contact us today and find out what we can do for you.



							<br>	<br>

							All Rights Reserved.

						</p>




					</div>
				</div>

			</footer>
			<!-- /Footer -->


		</div><!-- /#wrapper -->



		<script src="/folderlogin/assets/js/core.min.js"></script>
		 <script src="https://kit.fontawesome.com/768ac4feb3.js" crossorigin="anonymous"></script>

  </body>
</html>
