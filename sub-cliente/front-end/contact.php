<?php
session_start();

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/
/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO REAL *******/
require_once("../config/config_gcp.php");
/**********************************************************/
/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO local *******/
//require_once("../../config/config_gcp.php");
/**********************************************************/
include('GlobalFSyn.php');

if($_SESSION["login-type"]){
  //  header("Location: subclient-market-place.php");
	//	exit();
}
include('inc/header-2.php');


?>




			<!-- CONTACT FORM -->
			<section>
				<div class="container">

					<h2 class="font-weight-light mb-5">
						Contact us
					</h2>


					<div class="row">

						<div class="col-12 col-lg-8 mb-4">


							<!--
								CONTACT FORM : AJAX [TESTED|WORKING AS IT IS]

									Plugin required: SOW Ajax Forms

									In order to work as ajax form, SOW Ajax Forms should be available/enabled
									Else, SOW Form Validation plugin is used.
									If none of them are available, normal submit is used and you can remove:
										.js-ajax
										.bs-validate
										novalidate
										any data-ajax-*
										any data-error-*

									** Remove data-error-toast-* for no error toast notifications




								Ajax will control success/fail alerts according to server response:

									1. unexpected error: 		if server response is this string: {:err:unexpected:}
									2. mising mandatory:		if server response is this string: {:err:required:}
									3. success:					if server response is this string: {:success:}

									data-ajax-control-alerts="true"
									data-ajax-control-alert-succes="#contactSuccess"
									data-ajax-control-alert-unexpected="#contactErrorUnexpected"
									data-ajax-control-alert-mandaroty="#contactErrorMandatory"

								+++++++++++++++++++++++++++++++++++++++++++++++++++++++
									WORKING CONTACT! Edit your php/config.inc.php
								+++++++++++++++++++++++++++++++++++++++++++++++++++++++
							-->
							<form 	novalidate
									action="php/contact_form.php"
									method="POST"

									data-ajax-container="#ajax_dd_contact_response_container"
									data-ajax-update-url="false"
									data-ajax-show-loading-icon="true"
									data-ajax-callback-function=""
									data-error-scroll-up="false"

									data-ajax-control-alerts="true"
									data-ajax-control-alert-succes="#contactSuccess"
									data-ajax-control-alert-unexpected="#contactErrorUnexpected"
									data-ajax-control-alert-mandaroty="#contactErrorMandatory"

									data-error-toast-text="<i class='fi fi-circle-spin fi-spin float-start'></i> Please, complete all required fields!"
									data-error-toast-delay="2000"
									data-error-toast-position="bottom-center"

									class="bs-validate js-ajax">


								<!-- 1.
									optional, hidden action for your backend

									PHP Basic Example
									if($_POST['action'] == 'contact_form_submit') {
										... send message
									}
								-->
								<input type="hidden" name="action" value="contact_form_submit" tabindex="-1">
								<!-- -->


								<!-- 2.
									A very small optional trick (using .hide class instead of type="hidden") for some low spam robots.
									If this is not empty, the process should stop. A normal user/visitor should not be able to see this field.

									PHP Basic Example
									if($_POST['norobot'] != '') {
										exit;
									}
								-->
								<input type="text" name="norobot" value="" class="hide" tabindex="-1">
								<!-- -->

								<div class="form-label-group mb-3">
									<input required placeholder="Name" id="contact_name" name="contact_name" type="text" class="form-control">
									<label for="contact_name">Name</label>
								</div>

								<div class="form-label-group mb-3">
									<input required placeholder="Email" id="contact_email" name="contact_email" type="email" class="form-control">
									<label for="contact_email">Email</label>
								</div>

								<div class="form-label-group mb-3">
									<input required placeholder="Phone" id="contact_phone" name="contact_phone" type="text" class="form-control">
									<label for="contact_phone">Phone</label>
								</div>

								<div class="form-label-group mb-4">
									<textarea required placeholder="Message" id="contact_message" name="contact_message" class="form-control" rows="3"></textarea>
									<label for="contact_message">Message</label>
								</div>





								<!--
									Server detailed error
									!ONLY! If debug is enabled!
									Else, shown ony "Server Error!"
								-->
								<div id="ajax_dd_contact_response_container"></div>

								<!-- {:err:unexpected:} internal server error -->
								<div id="contactErrorUnexpected" class="hide alert alert-danger p-3">
									Unexpected Error!
								</div>

								<!-- {:err:required:} mandatory fields -->
								<div id="contactErrorMandatory" class="hide alert alert-danger p-3">
									Please, review your data and try again!
								</div>

								<!-- {:success:} message sent -->
								<div id="contactSuccess" class="hide alert alert-success p-3">
									Thank you for your message!
								</div>




								<button type="submit" class="btn btn-primary btn-block">
									Send Message
								</button>

							</form>
							<!-- /CONTACT FORM : AJAX -->


						</div>


						<div class="col-12 col-lg-4 mb-4">

							<div class="d-flex">

								<div class="w--40">
									<i class="fi fi-shape-abstract-dots text-gray-500 float-start fs--20"></i>
								</div>

								<div>
									<h2 class="fs--25 font-weight-light">
										Victorias Blossom Imports Inc.
									</h2>
									<ul class="list-unstyled m-0 fs--15">
										<li class="list-item text-muted">1420 172 St</li>
										<li class="list-item text-muted">Surrey, BC V3Z 9M6</li>

								</div>

							</div>



							<div class="d-flex mt-4">

								<div class="w--40">
									<i class="fi fi-time text-gray-500 float-start fs--20"></i>
								</div>

								<div>
									<h3 class="h4 font-weight-normal">
										Working Hours
									</h3>
									<ul class="list-unstyled m-0 fs--15">
										<li class="list-item text-muted">Monday - Friday: 09:00 to 18:00</li>
										<li class="list-item text-muted">Saturday: 09:00 to 12:00</li>
									</ul>
								</div>

							</div>


							<div class="d-flex mt-4">

								<div class="w--40">
									<i class="fi fi-phone text-gray-500 float-start fs--20"></i>
								</div>

								<div>
									<h3 class="h4 font-weight-normal">
										Phone Number
									</h3>
									<ul class="list-unstyled m-0">
										<li class="list-item mb-2 text-gray-500">
											<a class="link-muted" href="tel:7853889450">778-922-0136</a>
										</li>
										<li class="list-item mb-2 text-gray-500">
											<a class="link-muted" href="tel:3162881850">778-922-0136</a>
										</li>
									</ul>
								</div>

							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /CONTACT FORM -->


<?php include('inc/footer-3.php'); ?>
