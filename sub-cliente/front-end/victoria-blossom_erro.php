<?php
session_start();
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/
require_once("../../config/config_gcp.php");
include('GlobalFSyn.php');
include('inc/header-2.php'); ?>




			<!--
				SWIPER
			-->
			<section class="p-0 position-relative overflow-hidden">

				<!--
					Height
						.h-50vh
						.h-75vh
						.h-100vh
				-->
				<div class="swiper-container swiper-btn-group swiper-btn-group-end text-white h-75vh overflow-hidden"
					data-swiper='{
						"slidesPerView": 1,
						"spaceBetween": 0,
						"autoplay": { "delay" : 6500, "disableOnInteraction": true },
						"loop": true,
						"effect": "fade",
						"pagination": { "type": "bullets" }
					}'>

					<div class="swiper-wrapper h-100">


						<!-- slide 2 -->
						<a href="#!" class="h-100 swiper-slide d-middle overlay-dark overlay-opacity-5 bg-cover text-decoration-none text-white lazy" style="background-image:url('images/truck.jpeg')">
							<div class="position-relative container z-index-10 text-white text-center" data-aos="fade-in" data-aos-delay="150" data-aos-offset="0">

								<h2 class="display-3 h1-xs mb-4 font-weight-medium" data-swiper-parallax="-300">
									Victoria's Blossom Imports
								</h2>

								<div data-swiper-parallax="-100"><!-- parallax directly on button will cancel hover fade -->
									<span class="btn btn-lg btn-outline-light shadow-none transition-hover-top">
										Click to start
									</span>
								</div>

							</div>
						</a>
						<!-- /slide 2 -->

					</div>

					<!-- Add Arrows -->
					<div class="swiper-button-next swiper-button-black"></div>
					<div class="swiper-button-prev swiper-button-black"></div>

					<!-- Add Pagination -->
					<div class="swiper-pagination top-0 h--30 mt-4"></div>

					<!-- v shape : .bg-light, .shape-xs (remove .shape-xs for .h-100vh container) -->
					<div class="shape-v shape-xs bottom-0"></div>

				</div>

			</section>
			<!-- /SWIPER -->





			<!-- INFO BOX -->
			<section class="py-0 border-bottom-xs">
				<div class="container py-3">

					<div class="row">

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">

								<i class="fas fa-fan"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Fresh Flowers
								</h2>

								<p class="m-0">
									Wide variety of products
								</p>

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bt-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="demo.files/svg/ecommerce/free-delivery-truck.svg" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Free Shipping
								</h2>

								<p class="m-0">
									Shipping is on us
								</p>

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bl-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="demo.files/svg/ecommerce/24-hours-phone-service.svg" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Professional team
								</h2>

								<p class="m-0">
									24/24 available
								</p>

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bb-0 br-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<i class="fas fa-globe"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Wide Selection
								</h2>

								<p class="m-0">
									Local and Imported Flowers
								</p>

							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /INFO BOX -->




			<!-- OFFER BLOCK -->
			<section>
				<div class="container">


					<div class="mb-7 text-center px-3">
						<h2 class="h3-xs text-center-xs font-weight-normal text-danger">
							Buy by the box and save 10%!
						</h2>

						<p class="lead max-w-600 mx-auto">
							Explore our fresh and new arrivals
						</p>
					</div>



					<!-- product list -->
					<div class="row gutters-xs--xs">


						<!--

							MAIN/FEATURED
							please pay attention to how order-* is set!
							See on mobile how looks like! Play to set as desired!

							You can also insert this "featured" anywhere
							between them as long .order-2 class is present!

						-->
						<div class="order-2 col-12 col-lg-6 mb-4 mb-2-xs">


						</div>
						<!-- /SPECIAL PRODUCT -->


						<!-- item -->
						<div class="order-1 col-6 col-lg-3 mb-4 mb-2-xs">

							<div class="bg-white shadow-xs shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">

								<a href="" class="d-block text-decoration-none">

									<!--

										3 ways to set the image

									-->

									<!-- 1. without .bg-suprime - use image as it is -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden">
										<img class="img-fluid" src="image.jpg" alt="...">
									</figure>
									-->


									<!-- 2. squared, as background -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden bg-cover" style="background-image:url('image.jpg')">
										<img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
									</figure>
									-->

									<!-- 3. with .bg-suprime (remove white bg and add a gray bg) -->
									<figure class="m-0 text-center bg-gradient-radial-light rounded-top overflow-hidden">
										<img class="img-fluid bg-suprime opacity-9" src="images/1.jpeg" alt="...">
									</figure>

									<span class="d-block text-center-xs text-gray-600 py-3">

										<!--
											.max-height-50  = limited to 2 rows of text
											-or-
											.text-truncate
										-->
										<span class="d-block fs--16 max-h-50 overflow-hidden">
											Lorem ipsum dolor sit amet elit
										</span>





									</span>

								</a>

							</div>

						</div>
						<!-- /item -->


						<!-- item -->
						<div class="order-lg-2 col-6 col-lg-3 mb-4 mb-2-xs">

							<div class="bg-white shadow-xs shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">

								<a href="" class="d-block text-decoration-none">

									<!--

										3 ways to set the image

									-->

									<!-- 1. without .bg-suprime - use image as it is -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden">
										<img class="img-fluid" src="image.jpg" alt="...">
									</figure>
									-->


									<!-- 2. squared, as background -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden bg-cover" style="background-image:url('image.jpg')">
										<img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
									</figure>
									-->

									<!-- 3. with .bg-suprime (remove white bg and add a gray bg) -->
									<figure class="m-0 text-center bg-gradient-radial-light rounded-top overflow-hidden">
										<img class="img-fluid bg-suprime opacity-9" src="images/2.png" alt="...">
									</figure>

									<span class="d-block text-center-xs text-gray-600 py-3">

										<!--
											.max-height-50  = limited to 2 rows of text
											-or-
											.text-truncate
										-->
										<span class="d-block fs--16 max-h-50 overflow-hidden">
											Lorem ipsum dolor sit amet elit
										</span>




									</span>

								</a>

							</div>

						</div>
						<!-- /item -->















						<!-- item -->
						<div class="order-2 col-6 col-lg-3 mb-4 mb-2-xs">

							<div class="bg-white shadow-xs shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">

								<a href="" class="d-block text-decoration-none">

									<!--

										3 ways to set the image

									-->

									<!-- 1. without .bg-suprime - use image as it is -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden">
										<img class="img-fluid" src="image.jpg" alt="...">
									</figure>
									-->


									<!-- 2. squared, as background -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden bg-cover" style="background-image:url('image.jpg')">
										<img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
									</figure>
									-->

									<!-- 3. with .bg-suprime (remove white bg and add a gray bg) -->
									<figure class="m-0 text-center bg-gradient-radial-light rounded-top overflow-hidden">
										<img class="img-fluid bg-suprime opacity-9" src="images/3.png" alt="...">
									</figure>

									<span class="d-block text-center-xs text-gray-600 py-3">

										<!--
											.max-height-50  = limited to 2 rows of text
											-or-
											.text-truncate
										-->
										<span class="d-block fs--16 max-h-50 overflow-hidden">
											Lorem ipsum dolor sit amet elit
										</span>





									</span>

								</a>

							</div>

						</div>
						<!-- /item -->


						<!-- item -->
						<div class="order-2 col-6 col-lg-3 mb-4 mb-2-xs">

							<div class="bg-white shadow-xs shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">

								<a href="" class="d-block text-decoration-none">

									<!--

										3 ways to set the image

									-->

									<!-- 1. without .bg-suprime - use image as it is -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden">
										<img class="img-fluid" src="image.jpg" alt="...">
									</figure>
									-->


									<!-- 2. squared, as background -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden bg-cover" style="background-image:url('image.jpg')">
										<img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
									</figure>
									-->

									<!-- 3. with .bg-suprime (remove white bg and add a gray bg) -->
									<figure class="m-0 text-center bg-gradient-radial-light rounded-top overflow-hidden">
										<img class="img-fluid bg-suprime opacity-9" src="images/4.png" alt="...">
									</figure>

									<span class="d-block text-center-xs text-gray-600 py-3">

										<!--
											.max-height-50  = limited to 2 rows of text
											-or-
											.text-truncate
										-->
										<span class="d-block fs--16 max-h-50 overflow-hidden">
											Lorem ipsum dolor sit amet elit
										</span>





									</span>

								</a>

							</div>

						</div>
						<!-- /item -->


						<!-- item -->
						<div class="order-2 col-6 col-lg-3 mb-4 mb-2-xs">

							<div class="bg-white shadow-xs shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">

								<a href="" class="d-block text-decoration-none">

									<!--

										3 ways to set the image

									-->

									<!-- 1. without .bg-suprime - use image as it is -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden">
										<img class="img-fluid" src="image.jpg" alt="...">
									</figure>
									-->


									<!-- 2. squared, as background -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden bg-cover" style="background-image:url('image.jpg')">
										<img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
									</figure>
									-->

									<!-- 3. with .bg-suprime (remove white bg and add a gray bg) -->
									<figure class="m-0 text-center bg-gradient-radial-light rounded-top overflow-hidden">
										<img class="img-fluid bg-suprime opacity-9" src="images/8.png" alt="...">
									</figure>

									<span class="d-block text-center-xs text-gray-600 py-3">

										<!--
											.max-height-50  = limited to 2 rows of text
											-or-
											.text-truncate
										-->
										<span class="d-block fs--16 max-h-50 overflow-hidden">
											Lorem ipsum dolor sit amet elit
										</span>



									</span>

								</a>

							</div>

						</div>
						<!-- /item -->


						<!-- item -->
						<div class="order-2 col-6 col-lg-3 mb-4 mb-2-xs">

							<div class="bg-white shadow-xs shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">

								<a href="" class="d-block text-decoration-none">

									<!--

										3 ways to set the image

									-->

									<!-- 1. without .bg-suprime - use image as it is -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden">
										<img class="img-fluid" src="image.jpg" alt="...">
									</figure>
									-->


									<!-- 2. squared, as background -->
									<!--
									<figure class="m-0 text-center rounded-top overflow-hidden bg-cover" style="background-image:url('image.jpg')">
										<img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
									</figure>
									-->

									<!-- 3. with .bg-suprime (remove white bg and add a gray bg) -->
									<figure class="m-0 text-center bg-gradient-radial-light rounded-top overflow-hidden">
										<img class="img-fluid bg-suprime opacity-9" src="images/7.png" alt="...">
									</figure>

									<span class="d-block text-center-xs text-gray-600 py-3">

										<!--
											.max-height-50  = limited to 2 rows of text
											-or-
											.text-truncate
										-->
										<span class="d-block fs--16 max-h-50 overflow-hidden">
											Lorem ipsum dolor sit amet elit
										</span>




									</span>

								</a>

							</div>

						</div>
						<!-- /item -->

					</div>
					<!-- /product list -->


				</div>
			</section>
			<!-- /OFFER BLOCK -->








			<!-- BRANDS -->
			<section>
				<div class="container">


					<div class="mb-7 text-center px-3">
						<h2 class="h3 text-center-xs font-weight-normal text-danger">
							We select our growers carefully
						</h2>

						<p class="lead max-w-600 mx-auto">
							You can ask for your favorite farm!
						</p>
					</div>

					<!-- BRANDS : GRID : LAZYLOAD -->
					<div class="clearfix mb-3 mt-5 p-3 bg-white shadow-md rounded overflow-hidden">
						<div class="bg-white overflow-hidden"><!-- negative classes - hide bordered edges -->

							<div class="row row-grid mt--n1 mb--n1">

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/112316061629-SISAMPBA.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/050317111031-Albra-Roses-web-final.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/041719132720-Alexandra-farms-web.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/012220002402-ANNIROSES_web.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/030921133141-LOGO-MAGIC-FLOWERS_Web.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/041417095739-Bella-flor_web.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/050819202129-Azaya_web.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/122214085657-arbusta_Final_correction%20updated.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/012220142151-LOGO-GREENROSE_web.png" alt="...">
									</a>
								</div>

								<div class="h--150 col-6 col-md-5th d-flex align-items-center text-center">
									<a href="#!" class="w-100">
										<img class="img-fluid mx-3 max-w-180 lazy" src="https://app.freshlifefloral.com/user/logo/041719132720-Alexandra-farms-web.png" alt="...">
									</a>
								</div>

							</div>

						</div>
					</div>
					<!-- BRANDS : GRID : LAZYLOAD -->


					<div class="text-center pt-5">

						<!-- view all button -->
						<a href="#!" class="btn btn-sm btn-secondary btn-soft btn-soft">Your favorite farms</a>

					</div>


				</div>
			</section>
			<!-- /BRANDS -->




<?php include('inc/footer-2.php') ?>
