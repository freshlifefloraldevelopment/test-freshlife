<?php
header('Location: victoria-blossom.php');
exit;
include('inc/header.php');?>






			<!-- COVER -->
			<section class="p-0">

				<div class="container min-h-75vh d-middle pt-5">

					<div class="row text-center-xs">

						<div class="col-12 col-md-6 order-2 order-md-1 pb-5" data-aos="fade-in" data-aos-delay="0">

							<div class="mb-5">
								<h1 class="font-weight-light mb-4">
									Welcome to <span class="text-primary font-weight-medium">smarty</span>,
									<span class="font-weight-medium">multipurpose template</span>!
								</h1>

								<p class="lead">
									No other template on any market is working the way Smarty does!
									Lightweight, fast, loading plugins <b>only</b> when needed, <B>where</B> are needed in a dynamic way!
								</p>

								<div class="mt-4">

									<a href="overview.html" class="btn btn-primary transition-hover-top mb-3 d-block-xs">
										<i class="fi fi-arrow-right"></i>
										Overview
									</a>

									<a target="_blank" href="../html_admin/layout_1/" class="btn btn-primary transition-hover-top btn-soft mb-3 d-block-xs">
										Admin Panel
									</a>

								</div>
							</div>

						</div>

						<div class="col-12 col-md-6 order-1 order-md-2 pb-5" data-aos="fade-in" data-aos-delay="200">

							<!-- svg image -->
							<img width="600" height="400" class="img-fluid lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/artworks/girl_headphones.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">

						</div>

					</div>

				</div>
			</section>
			<!-- /COVER -->






			<!-- COUNTER -->
			<section class="pt-5 pb-0">
				<div class="container">

					<div class="row col-border text-center text-muted">

						<div class="col-6 col-md-3 mb-5">

							<div class="h1">
								<span data-toggle="count"
									data-count-from="0"
									data-count-to="7000"
									data-count-duration="2500"
									data-count-decimals="0">0</span><span class="font-weight-light">+</span>
							</div>

							<h3 class="h6 m-0">TRUSTED CLIENTS</h3>

						</div>


						<div class="col-6 col-md-3 mb-5">

							<div class="h1">
								<span data-toggle="count"
									data-count-from="0"
									data-count-to="100"
									data-count-duration="2500"
									data-count-decimals="0">0</span><span class="font-weight-light">%</span>
							</div>

							<h3 class="h6 m-0">FREE UPDATES</h3>

						</div>


						<div class="col-6 col-md-3 mb-5">

							<div class="h1">
								<span data-toggle="count"
									data-count-from="0"
									data-count-to="100"
									data-count-duration="2500"
									data-count-decimals="0">0</span><span class="font-weight-light">%</span>
							</div>

							<h3 class="h6 m-0">UPCOMING FEATURES</h3>

						</div>


						<div class="col-6 col-md-3 mb-5">

							<div class="h1">
								<span data-toggle="count"
									data-count-from="0"
									data-count-to="5"
									data-count-duration="1500"
									data-count-decimals="0">0</span><span class="font-weight-light">/5</span>
							</div>

							<h3 class="h6 m-0">RATED</h3>

						</div>

					</div>

				</div>
			</section>
			<!-- /COUNTER -->










			<!-- block -->
			<section class="bg-theme-color-light mb-1">
				<div class="container">

					<div class="row">

						<div class="col-12 col-md-7 col-lg-6" data-aos="fade-up" data-aos-delay="0">

							<span class="badge badge-pill badge-success badge-soft font-weight-light pl-2 pr-2 pt--6 pb--6 mb-2">
								START NOW! FINISH IN MINUTES!
							</span>

							<h2 class="mb-4 font-weight-normal">
								What would you do if
								<span class="text-success">time</span> really matter?
							</h2>

							<p class="fs--18 mb-4">
								Frontend? Admin? What if you don't need anymore to learn and work with two different templates on your projects?
							</p>


							<div class="d-flex mb-4">

								<!-- icon -->
								<div class="text-primary">
									<img width="40" height="40" src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/icons/content_arrows_1.svg" alt="...">
								</div>

								<div class="ml-4 mr-4">

									<!-- heading -->
									<h3 class="h5 mb-1">
										Modular &amp; Reusable
									</h3>

									<!-- text -->
									<p>
										All elements are reusable and adjustable. Just copy/paste an element and that's it! No more wasting time with custom css classes!
									</p>

								</div>

							</div>


							<div class="d-flex mb-4">

								<!-- icon -->
								<div class="text-primary">
									<img width="40" height="40" src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/icons/content_arrows_2.svg" alt="...">
								</div>

								<div class="ml-4 mr-4">

									<!-- heading -->
									<h3 class="h5 mb-1">
										High quality oriented
									</h3>

									<!-- text -->
									<p>
										All our templates are built from scratch on top of bootstrap framework, high quality and speed being our primary focus.
									</p>

								</div>

							</div>

						</div>

						<div class="col-12 col-md-5 col-lg-6" data-aos="fade-up" data-aos-delay="150">

							<div class="d-table">
								<div class="d-table-cell align-middle">

									<div class="transform-3d-right">
										<img class="shadow-xlg img-fluid rounded-xl z-index-1" src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/images/ps_smarty.jpg" width="570" height="357" alt="...">
									</div>

								</div>
							</div>


						</div>

					</div>

				</div>
			</section>
			<!-- /block -->




			<!-- block : icons : 3 -->
			<section class="bg-light mb-1">
				<div class="container">

					<div class="row col-border text-center">


						<div class="col-6 col-md-4 py-5" data-aos="fade-up" data-aos-delay="50" data-aos-offset="0">

							<a href="#!" class="d-block text-decoration-none text-dark transition-hover-top transition-all-ease-250">

								<div class="bg-white w--120 h--120 mb-4 border border-light rounded-circle shadow-md d-inline-flex justify-content-center align-items-center">
									<i class="fi fi-brain fs--35"></i>
								</div>

								<h3 class="h4">Fully Modular</h3>

								<p class="max-w-250 mx-auto fs--16">
									No need to create dedicated classes! Most blocks are created using helper classes!
								</p>

							</a>

						</div>


						<div class="col-6 col-md-4 py-5" data-aos="fade-up" data-aos-delay="250" data-aos-offset="0">

							<a href="#!" class="d-block text-decoration-none text-dark transition-hover-top transition-all-ease-250">

								<div class="bg-white w--120 h--120 mb-4 border border-light rounded-circle shadow-md d-inline-flex justify-content-center align-items-center">
									<i class="fi fi-heart-empty fs--35"></i>
								</div>

								<h3 class="h4">Admin Included</h3>

								<p class="max-w-250 mx-auto fs--16">
									Admin panel included! All frontend elements are available to admin. And vice-versa!
								</p>

							</a>

						</div>


						<div class="col-12 col-md-4 py-5 border-top-xs" data-aos="fade-up" data-aos-delay="450" data-aos-offset="0">

							<a href="#!" class="d-block text-decoration-none text-dark transition-hover-top transition-all-ease-250">

								<div class="bg-white w--120 h--120 mb-4 border border-light rounded-circle shadow-md d-inline-flex justify-content-center align-items-center">
									<i class="fi fi-layers fs--35"></i>
								</div>

								<h3 class="h4">No Separations</h3>

								<p class="max-w-250 mx-auto fs--16">
									There is no separation between Frontend &amp; Admin! Both of them are using the exactly same core!
								</p>

							</a>

						</div>

					</div>

				</div>
			</section>
			<!-- /block : icons : 3 -->






			<!-- block : image -->
			<section class="pb-0 bg-theme-color-light mb-1">
				<div class="container">

					<div class="mb-7 text-center mx-auto max-w-800">
						<h2 class="h1 h2-xs font-weight-normal">
							Unlimited &amp; Modular
						</h2>
						<p>Smarty has powerful options &amp; tools, unlimited designs, responsive framework and amazing support. We are dedicated to providing you with the best experience possible. Read below to find out why the sky's the limit when using Smarty.</p>
					</div>


					<img class="img-fluid lazy" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxNiA5IiAvPg==" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/images/ps_ipad.png" alt="..." data-aos="fade-up" data-aos-delay="300" data-aos-offset="0">
				</div>
			</section>
			<!-- /block : image -->






			<!-- block : icons -->
			<section class="bg-light">
				<div class="container">

					<div class="row">

						<div class="col-6 col-md-4 py-4 my-3 m-0-xs d-lg-flex" data-aos="fade-down" data-aos-delay="50" data-aos-offset="0">

							<div class="flex-none w--80">
								<i class="fi fi-lightbulb fs--45"></i>
							</div>

							<div class="flex-lg-fill">
								<h2 class="h5">New Ideas</h2>
								<p>
									Answering those questions and more is a tall order, but one you should approach with enthusiasm.
								</p>

								<a href="#!">Learn more</a>
							</div>

						</div>


						<div class="col-6 col-md-4 py-4 my-3 m-0-xs d-lg-flex" data-aos="fade-down" data-aos-delay="150" data-aos-offset="0">

							<div class="flex-none w--80">
								<i class="fi fi-graph fs--45"></i>
							</div>

							<div class="flex-lg-fill">
								<h2 class="h5">Organic Growth</h2>
								<p>
									Answering those questions and more is a tall order, but one you should approach with enthusiasm.
								</p>

								<a href="#!">Learn more</a>
							</div>

						</div>


						<div class="col-6 col-md-4 py-4 my-3 m-0-xs d-lg-flex" data-aos="fade-down" data-aos-delay="250" data-aos-offset="0">

							<div class="flex-none w--80">
								<i class="fi fi-brain fs--45"></i>
							</div>

							<div class="flex-lg-fill">
								<h2 class="h5">Brainstorming</h2>
								<p>
									Answering those questions and more is a tall order, but one you should approach with enthusiasm.
								</p>

								<a href="#!">Learn more</a>
							</div>

						</div>


						<div class="col-6 col-md-4 py-4 my-3 m-0-xs d-lg-flex" data-aos="fade-up" data-aos-delay="50" data-aos-offset="0">

							<div class="flex-none w--80">
								<i class="fi fi-truck-speed fs--45"></i>
							</div>

							<div class="flex-lg-fill">
								<h2 class="h5">Delivery Guaranteed</h2>
								<p>
									Answering those questions and more is a tall order, but one you should approach with enthusiasm.
								</p>

								<a href="#!">Learn more</a>
							</div>

						</div>


						<div class="col-6 col-md-4 py-4 my-3 m-0-xs d-lg-flex" data-aos="fade-up" data-aos-delay="150" data-aos-offset="0">

							<div class="flex-none w--80">
								<i class="fi fi-umbrella fs--45"></i>
							</div>

							<div class="flex-lg-fill">
								<h2 class="h5">You got covered</h2>
								<p>
									Answering those questions and more is a tall order, but one you should approach with enthusiasm.
								</p>

								<a href="#!">Learn more</a>
							</div>

						</div>


						<div class="col-6 col-md-4 py-4 my-3 m-0-xs d-lg-flex" data-aos="fade-up" data-aos-delay="250" data-aos-offset="0">

							<div class="flex-none w--80">
								<i class="fi fi-gps fs--45"></i>
							</div>

							<div class="flex-lg-fill">
								<h2 class="h5">To a new destination</h2>
								<p>
									Answering those questions and more is a tall order, but one you should approach with enthusiasm.
								</p>

								<a href="#!">Learn more</a>
							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /block : icons -->




			<!-- BRANDS -->
			<section class="p-0">
				<div class="container">

					<div class="row">

						<div class="col-4 col-md-2 py-5" data-aos="fade-in" data-aos-delay="150" data-aos-offset="0">
							<img width="200" height="71" class="img-fluid opacity-4 lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/vendors/vendor_airbnb.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
						</div>

						<div class="col-4 col-md-2 py-5" data-aos="fade-in" data-aos-delay="200" data-aos-offset="0">
							<img width="200" height="71" class="img-fluid opacity-4 lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/vendors/vendor_netflix.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
						</div>

						<div class="col-4 col-md-2 py-5" data-aos="fade-in" data-aos-delay="250" data-aos-offset="0">
							<img width="200" height="71" class="img-fluid opacity-4 lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/vendors/vendor_coinbase.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
						</div>

						<div class="col-4 col-md-2 py-5" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
							<img width="200" height="71" class="img-fluid opacity-4 lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/vendors/vendor_instagram.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
						</div>

						<div class="col-4 col-md-2 py-5" data-aos="fade-in" data-aos-delay="350" data-aos-offset="0">
							<img width="200" height="71" class="img-fluid opacity-4 lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/vendors/vendor_pinterest.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
						</div>

						<div class="col-4 col-md-2 py-5" data-aos="fade-in" data-aos-delay="400" data-aos-offset="0">
							<img width="200" height="71" class="img-fluid opacity-4 lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/vendors/vendor_dribble.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
						</div>

					</div>

				</div>
			</section>
			<!-- /BRANDS -->







			<!-- PARALLAX -->
			<section class="position-relative jarallax overlay-dark overlay-opacity-7 text-white p-0 bg-cover" style="background-image:url('https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/images/unsplash/covers/perry-grone-lbLgFFlADrY-unsplash.jpg')">
				<div class="container pt--250 pb--250 text-center position-relative z-index-2">

					<img width="250" height="72" src="https://smarty.stepofweb.com/3.0.7/html_frontend/assets/images/logo/logo_light.svg" alt="...">

					<h3 class="font-weight-normal fs--16 mt-3 mb-5">
						FIGHTING TOGETHER <span class="text-warning">FOR THE BETTER</span>
					</h3>

					<a href="#!" class="btn btn-pill btn-light shadow-none transition-hover-top">
						<i class="fi fi-layers-middle"></i>
						Join Smarty Comunity
					</a>

				</div>

				<!-- lines, looks like through a glass -->
				<div class="absolute-full w-100 overflow-hidden opacity-5">
					<img class="img-fluid" width="2000" height="2000" src="https://smarty.stepofweb.com/3.0.7/html_frontend/assets/images/masks/shape-line-lense.svg" alt="...">
				</div>

			</section>
			<!-- /PARALLAX -->







			<!-- QUICK FAQ -->
			<section class="bg-gradient-dark">
				<div class="container">


					<div class="text-center mb-7">
						<span class="badge badge-secondary badge-soft bg-gray-600 text-white badge-pill font-weight-light pl-2 pr-2 pt--6 pb--6">
							HOW DO I START?
						</span>
						<h2 class="h1 mb-5 mt-2 font-weight-normal">Frequently Asked Questions</h2>
					</div>


					<div class="row">

						<div class="col-12 col-md-5 offset-md-1">

							<div class="d-flex mb-3">

								<!-- icon -->
								<div class="w--50 fi mdi-filter_1 fs--25 mt--n6 text-muted"></div>

								<div class="ml-4 mr-4">

									<h3 class="h5 text-white">
										Do I get free updates?
									</h3>

									<p class="mb-6 mb-md-8">
										Yes, all updates are free. Each new version is available for download from your account.
									</p>

								</div>

							</div>


							<div class="d-flex mb-3">

								<!-- icon -->
								<div class="w--50 fi mdi-filter_2 fs--25 mt--n6 text-muted"></div>

								<div class="ml-4 mr-4">

									<h3 class="h5 text-white">
										Is compatible with my framework?
									</h3>

									<p class="mb-6 mb-md-8">
										Yes, because this is a HTML/CSS/JS template, should be compatible with any framework.
									</p>

								</div>

							</div>

						</div>

						<div class="col-12 col-md-5">

							<div class="d-flex mb-3">

								<!-- icon -->
								<div class="w--50 fi mdi-filter_3 fs--25 mt--n6 text-muted"></div>

								<div class="ml-4 mr-4">

									<h3 class="h5 text-white">
										Who can use it?
									</h3>

									<p class="mb-6 mb-md-8">
										You can use it for your own project or for a project for your client. Also, money back is guaranteed!
									</p>

								</div>

							</div>

							<div class="d-flex mb-3">

								<!-- icon -->
								<div class="w--50 fi mdi-filter_4 fs--25 mt--n6 text-muted"></div>

								<div class="ml-4 mr-4">

									<h3 class="h5 text-white">
										Do I get free updates?
									</h3>

									<p class="mb-6 mb-md-8">
										Yes, all updates are free. Each new version is available for download from your account.
									</p>

								</div>

							</div>

						</div>

					</div>



					<div class="text-center mt-5">


						<a href="https://wrapbootstrap.com/theme/smarty-website-admin-rtl-WB02DSN1B" class="btn btn-lg btn-primary shadow-none transition-hover-top d-block-xs">
							Get Smarty Now
						</a>

					</div>

				</div>
			</section>
			<!-- /QUICK FAQ -->







			<!-- INFO BOX -->
			<section class="p-0 bg-warning">
				<div class="container py-3">

					<div class="row">

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs" data-aos="fade-in" data-aos-delay="150">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" class="lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/various/trendy.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Trendy &amp; Modern
								</h2>

								<p class="m-0">
									Love it! Use it! Reuse it!
								</p>

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bt-0 br-0 bb-0 b-0-lg" data-aos="fade-in" data-aos-delay="250">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" class="lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/various/hot_deal.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Free Updates
								</h2>

								<p class="m-0">
									Lifetime <b>free</b> updates!
								</p>

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bl-0 br-0 bb-0 b-0-lg" data-aos="fade-in" data-aos-delay="350">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" class="lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/various/new.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Frontend &amp; Admin
								</h2>

								<p class="m-0">
									Sharing the same core!
								</p>

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bb-0 br-0 b--0-lg" data-aos="fade-in" data-aos-delay="450">

							<!-- link example -->
							<a href="#!" class="text-dark text-decoration-none d-flex d-block-xs text-center-xs">

								<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
									<img width="60" height="60" class="lazy" data-src="https://smarty.stepofweb.com/3.0.7/html_frontend/demo.files/svg/various/ok_like.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="...">
								</div>

								<div class="my-2">

									<h2 class="font-weight-medium fs--20 mb-0">
										You're Covered
									</h2>

									<p class="m-0">
										Frontend + Admin
									</p>

								</div>

							</a>

						</div>

					</div>

				</div>
			</section>
			<!-- /INFO BOX -->





<?php include('inc/footer.php'); ?>
