<?php include('inc/header.php'); ?>
<style type="text/css">
	.custom_select button.btn.dropdown-toggle.select-form-control.border {
    padding-top: 0px;
}
</style>

	

<!-- -->
			<section>
				<div class="container">

					<div class="row">

						<!-- sidebar -->
						<div class="col-12 col-sm-12 col-md-12 col-lg-3 mb--60">


							<!-- CATEGORIES -->
							<nav class="nav-deep nav-deep-light mb-4 shadow-xs shadow-none-md shadow-none-xs px-4 pb-3 p-0-md p-0-xs rounded">

								<!-- mobile trigger : categories -->
								<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#nav_responsive" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">
									<span class="group-icon px-2 py-2 float-start">
										<i class="fi fi-bars-2"></i>
										<i class="fi fi-close"></i>
									</span>

									<span class="h5 py-2 m-0 float-start">
										Categories
									</span>
								</button>

								<!-- desktop only -->
								<h5 class="h6 pt-3 pb-3 m-0 d-none d-lg-block">
									Categories
								</h5>


								<!-- navigation -->
								<ul id="nav_responsive" class="nav flex-column d-none d-lg-block">

									<li class="nav-item">
										<a class="nav-link px-0" href="#!">

											<span class="badge badge-warning float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1">
												New in stock
											</span>

											<i class="fi fi-arrow-end m-0 fs--12"></i> 
											<span class="px-2 d-inline-block">
												Asters
											</span>
										</a>
									</li>

									<li class="nav-item">
										<a class="nav-link px-0" href="#!">
											<i class="fi fi-arrow-end m-0 fs--12"></i> 
											<span class="px-2 d-inline-block">
												Calla Lily
											</span>
										</a>
									</li>

									<li class="nav-item">
										<a class="nav-link px-0" href="#!">
											<i class="fi fi-arrow-end m-0 fs--12"></i> 
											<span class="px-2 d-inline-block">
												Carnation
											</span>
										</a>
									</li>

									<li class="nav-item">
										<a class="nav-link px-0" href="#!">
											<i class="fi fi-arrow-end m-0 fs--12"></i> 
											<span class="px-2 d-inline-block">
												Flowers and Fillers
											</span>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link px-0" href="#!">
											<i class="fi fi-arrow-end m-0 fs--12"></i> 
											<span class="px-2 d-inline-block">
												Roses
											</span>
										</a>
									</li>

								</ul>

							</nav>
							<!-- /CATEGORIES -->



							<!-- mobile trigger : filters -->
							<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#sidebar_filters" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" data-toggle-body-class="overflow-hidden">
								<i class="px-2 py-2 fs--15 float-start fi fi-eq-horizontal"></i>
								<span class="h5 py-2 m-0 float-start">
									Filters
								</span>
							</button>


							<form method="get" id="sidebar_filters" class="d-none d-lg-block">

								<!-- MOBILE ONLY -->
								<div class="bg-white pb-3 mb-4 d-block d-lg-none border-bottom">
									
									
									<i class="fi fi-eq-horizontal float-start"></i>
									<span class="h5 m-0 d-inline-block">
										Filters
									</span>

									<!-- mobile : exit fullscreen -->
									<a href="#" class="float-end btn-toggle text-dark mx-1" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" data-toggle-body-class="overflow-hidden" data-target="#sidebar_filters">
										<i class="fi fi-close"></i>
									</a>

								</div>
								<!-- /MOBILE ONLY -->





								<!-- Reset Filters -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">
									<a href="#" class="text-danger float-end w--20 d-inline">
										<i class="fi fi-close"></i>
									</a>
									Reset Filters
								</div>
								<!-- /Reset Filters -->


								

								<!-- Color -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">
									<h3 class="fs--15 mb-4">

										<a href="#" data-target-reset="#filter_color_list" class="form-advanced-reset hide-force text-danger float-end w--20 d-inline">
											<i class="fi fi-close"></i>
										</a>

										Color
									</h3>

									<div id="filter_color_list">

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #377dff"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #6c757d"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #dc3545"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #fad776"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #e83e8c"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #6610f2"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #4c2c92"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #ffffff"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #000000"></i>
										</label>

										<label class="form-selector">
											<input type="checkbox" name="color[]">
											<i style="background: #ff0000"></i>
										</label>

									</div>

								</div>


								<!-- Size -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">
									<h3 class="fs--15 mb-4">

										<a href="#" data-target-reset="#filter_size_list" class="form-advanced-reset hide-force text-danger float-end w--20 d-inline">
											<i class="fi fi-close"></i>
										</a>

										Size
									</h3>

									<div id="filter_size_list">
										<label class="form-selector">
											<input type="radio" name="size[]">
											<span>50cm 3-5 Blooms</span>
										</label>

										<label class="form-selector">
											<input type="radio" name="size[]">
											<span>60cm 3-5 Blooms</span>
										</label>

										<label class="form-selector">
											<input type="radio" name="size[]">
											<span>70cm 3-5 Blooms</span>
										</label>

										<label class="form-selector">
											<input type="radio" name="size[]">
											<span>50cm 2-3 Blooms</span>
										</label>

										<label class="form-selector">
											<input type="radio" name="size[]">
											<span>60cm 2-3 Blooms</span>
										</label>
									</div>

								</div>





								<!-- Brands -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">

									<div class="input-group-over">
										<input type="text" class="form-control form-control-sm iqs-input" data-container=".iqs-container" value="" placeholder="Select Growers">
										<span class="fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
									</div>

									<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="1">
												<i></i> Farm Name 1 <span class="text-muted fs--12 d-inline-block">(11)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="2">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(45)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="3">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(45)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="4">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(13)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="5">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(21)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="6">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(21)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="7">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(21)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="8">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(21)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="9">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(15)</span>
											</label>

										</div>

										<div class="iqs-item">

											<label class="form-checkbox form-checkbox-primary">
												<input type="checkbox" name="brand[]" value="10">
												<i></i> Farm Name <span class="text-muted fs--12 d-inline-block">(21)</span>
											</label>

										</div>

									</div>

								</div>
								<!-- /Brands -->


								<!-- optional button -->
								<button type="sub	mit" class="btn btn-primary btn-soft btn-sm btn-block">
									Apply Filters
								</button>

							</form>


						</div>
						<!-- /sidebar -->



						<!-- products -->
						<div class="col-12 col-sm-12 col-md-12 col-lg-9">


							<!-- additional filters -->
							<div class="shadow-xs bg-white mb-5 p-3 rounded clearfix">

								<div class="clearfix border-bottom pb-3 mb-3">
								
									<div class="float-start fs--14 position-relative mt-1">
										<a href="#" class="text-primary text-decoration-none btn p-0" data-toggle="dropdown" aria-expanded="false">
											<i class="fi fi-arrow-down-slim fs--12"></i> 12 / page
										</a>

										<ul class="dropdown-menu b-0 mt-3 rounded fs--15">
											<li class="dropdown-item active"><a href="#!" class="text-muted py-2 d-block">12 / page</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">36 / page</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">72 / page</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">100 / page</a></li>
										</ul>
									</div>

									<div class="float-end fs--14 position-relative mt-1">
										Sort by: &nbsp; <a href="#" class="text-primary text-decoration-none" data-toggle="dropdown" aria-expanded="false">
											Popular First &nbsp; 
											<i class="fi fi-arrow-down-slim fs--12"></i>
										</a>

										<ul class="dropdown-menu b-0 mt-3 rounded fs--15">
											<li class="dropdown-item active"><a href="#!" class="text-muted py-2 d-block">Popular First</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Newest First</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Avg. Customer Review</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Price: Low to High</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Price: High to Low</a></li>
										</ul>
									</div>

								</div>


								<h2 class="h6 mb-0">
									1,381 total items
								</h2>

							</div>
							<!-- /additional filters -->



							<!-- product list -->
							<div class="row gutters-xs--xs">


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								
								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->


								<!-- item -->
								<div class="col-12 col-sm-6 col-lg-4 mb-4 mb-2-xs">

									<div class="bg-white show-hover-container p-2 h-100">

										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">
											
											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in -->
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2" 
												data-toggle="tooltip" 
												data-original-title="add to favourite" 
												data-placement="left" 

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>

											<a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a>

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to cart">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->


										<a href="shop-page-product-2.html" class="d-block text-decoration-none text-center">


											<!-- image as background ; img SVG - is a resizable element so we can have a responsive rounded-circle -->
											<figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy">
												<img class="w-100" src="https://app.freshlifefloral.com/images/product-image/big/062013204146_crop.jpg" alt="..."> 
											</figure>


											<span class="d-block text-center text-gray-600 py-3">
												
												<!-- 
													.max-height-50  = limited to 2 rows of text 
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
													Standard Roses Freedom
												</span>



											</span>

										</a>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">Florana</option>
		<option value="2">Agrivaldani</option>
		<option value="3">Sisapamba</option>
		<option value="3">EQR</option>
		<option value="3">Others</option>
	</select>
	<label for="select_options">Select Farm</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select">
		<option value="1">50cm 250gr @ $0.82</option>
		<option value="2">50cm 500gr @ $0.92</option>
	</select>
	<label for="select_options">Select Size</label>
</div>
<div class="form-label-group mb-3">
	<select id="select_options" class="form-control bs-select custom_select">
		<option value="">Quantity</option>
		<?php 
		for($i = 100; $i<=10000;$i+=100)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
	</select>
</div>
									</div>

								</div>
								<!-- /item -->




							</div>
							<!-- /product list -->


							<!-- pagination -->
							<nav aria-label="pagination" class="mt-5">
								<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">

									<li class="page-item disabled">
										<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
									</li>
									
									<li class="page-item active">
										<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
									</li>
									
									<li class="page-item" aria-current="page">
										<a class="page-link" href="#">2</a>
									</li>
									
									<li class="page-item">
										<a class="page-link" href="#">3</a>
									</li>
									
									<li class="page-item">
										<a class="page-link" href="#">Next</a>
									</li>

								</ul>
							</nav>
							<!-- pagination -->
									


						</div>
						<!-- /products -->

					</div>

				</div>
			</section>
			<!-- / -->

<?php include('inc/footer.php'); ?>