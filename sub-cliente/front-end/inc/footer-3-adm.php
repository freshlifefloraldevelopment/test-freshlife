<!-- INFO BOX -->
			<section class="py-0 border-bottom-xs">
				<div class="container py-3">
					<div class="row">

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">

								<i class="fas fa-fan"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Fresh Flowers
								</h2>
								<h3 class="font-weight-medium fs--20 mb-0">
									Wide variety of products
								</h3>
								<!--p class="m-0">
									Wide variety of products
								</p-->
							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bt-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="demo.files/svg/ecommerce/free-delivery-truck.svg" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Free Shipping
								</h2>
								<h3 class="font-weight-medium fs--20 mb-0">
									Shipping is on us
								</h3>


								<!--p class="m-0">
									Shipping is on us
								</p-->

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bl-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="demo.files/svg/ecommerce/24-hours-phone-service.svg" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Professional team
								</h2>

								<h3 class="font-weight-medium fs--20 mb-0">
									24/7 available
								</h3>

								<!--p class="m-0">
									24/7 available
								</p-->

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bb-0 br-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<i class="fas fa-globe"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Wide Selection
								</h2>

								<h3 class="font-weight-medium fs--20 mb-0">
									Local and Imported Flowers
								</h3>

								<!--p class="m-0">
									Local and Imported Flowers
								</p-->

							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /INFO BOX -->




			<!-- Footer -->
			<footer id="footer" class="shadow-xs">

			


				<div class="border-top">
					<div class="container text-center py-5">

						<!-- logo -->
						<span class="h--70 d-inline-flex align-items-center">
							<img src="assets/images/logo/singlelinelogo.svg" width="440" height="150" alt="Victoria's Blossom Imports Inc.">
						</span>

						<p class="text-gray-500 fs--14">

							&copy; Victoria's Blossom Imports Inc. 2015 &copy; 2019.

							<br>

							All Rights Reserved.

						</p>

					</div>
				</div>

			</footer>
			<!-- /Footer -->


		</div><!-- /#wrapper -->


  </body>
</html>
