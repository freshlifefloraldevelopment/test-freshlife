<!-- INFO BOX -->
			<section class="py-0 border-bottom-xs">
				<div class="container py-3">
					<div class="row">

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">

								<i class="fas fa-fan"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Fresh Flowers
								</h2>
								<h3 class="font-weight-medium fs--20 mb-0">
									Wide variety of products
								</h3>
								<!--p class="m-0">
									Wide variety of products
								</p-->
							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bt-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="demo.files/svg/ecommerce/free-delivery-truck.svg" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Free Shipping
								</h2>
								<h3 class="font-weight-medium fs--20 mb-0">
									Shipping is on us
								</h3>


								<!--p class="m-0">
									Shipping is on us
								</p-->

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bl-0 br-0 bb-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<img width="60" height="60" src="demo.files/svg/ecommerce/24-hours-phone-service.svg" alt="...">
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Professional team
								</h2>

								<h3 class="font-weight-medium fs--20 mb-0">
									24/7 available
								</h3>

								<!--p class="m-0">
									24/7 available
								</p-->

							</div>

						</div>

						<div class="col-6 col-lg-3 p--15 d-flex d-block-xs text-center-xs border-dashed border-light bw--1 bb-0 br-0 b--0-lg">

							<div class="pl--10 pr--20 fs--50 line-height-1 pt--6">
								<i class="fas fa-globe"></i>
							</div>

							<div class="my-2">

								<h2 class="font-weight-medium fs--20 mb-0">
									Wide Selection
								</h2>

								<h3 class="font-weight-medium fs--20 mb-0">
									Local and Imported Flowers
								</h3>

								<!--p class="m-0">
									Local and Imported Flowers
								</p-->

							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /INFO BOX -->




			<!-- Footer -->
			<footer id="footer" class="shadow-xs">

				<div class="container py-5">

					<div class="row">

						<div class="col-12 col-lg-8">

							<div class="row mb-3">

								<!-- Col 1 -->
								<div class="col-12 col-lg-4 mb-1">

									<!-- mobile only : SOW Toggle Button -->
									<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none"
											data-target="#footer_c1"
											data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">

										<span class="group-icon p-2 float-start">
											<i class="fi fi-bars-2"></i>
											<i class="fi fi-close"></i>
										</span>

										<span class="h5 py-2 m-0 float-start">
											Orders
										</span>
									</button>

									<!-- desktop only -->
									<h3 class="h5 py-3 m-0 d-none d-lg-block">
										Orders
									</h3>


									<!-- navigation -->
									<ul id="footer_c1" class="nav flex-column d-none d-lg-block">
										<li class="list-item">
											<a class="d-block py-1" href="my-orders.php">My Account</a>
										</li>
					</ul>

								</div>

								<!-- Col 2 -->
								<div class="col-12 col-lg-4 mb-1">

									<!-- mobile only : SOW Toggle Button -->
									<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none"
											data-target="#footer_c2"
											data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">

										<span class="group-icon p-2 float-start">
											<i class="fi fi-bars-2"></i>
											<i class="fi fi-close"></i>
										</span>

										<span class="h5 py-2 m-0 float-start">
											MARKET PLACE
										</span>
									</button>

									<!-- desktop only -->
									<h3 class="h5 py-3 m-0 d-none d-lg-block">
										MARKET PLACE
									</h3>


									<!-- navigation -->
									<ul id="footer_c2" class="nav flex-column d-none d-lg-block">
										<li class="list-item">
											<a class="d-block py-1" href="subclient-market-place.php">Varities</a>
										</li>

									</ul>


								</div>

								<!-- Col 3 -->
								<div class="col-12 col-lg-4 mb-1">

									<!-- mobile only : SOW Toggle Button-->
									<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none"
											data-target="#footer_c3"
											data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">

										<span class="group-icon p-2 float-start">
											<i class="fi fi-bars-2"></i>
											<i class="fi fi-close"></i>
										</span>

										<span class="h5 py-2 m-0 float-start">
											NEW FEATURES
										</span>

									</button>

									<!-- desktop only -->
									<h3 class="h5 py-3 m-0 d-none d-lg-block">
										NEW FEATURES
									</h3>


									<!-- navigation -->
									<ul id="footer_c3" class="nav flex-column d-none d-lg-block">
										<li class="list-item">
											<a class="d-block py-1" href="subclient-market-place-view.php">New Features</a>
										</li>


									</ul>

								</div>

							</div>

						</div>

					<div class="col">

									<!-- Begin Constant Contact Inline Form Code -->
			<div class="ctct-inline-form" data-form-id="93c4c7ef-fafc-4e69-a806-95729bccff96"></div>
			<!-- End Constant Contact Inline Form Code -->

						</div>

					</div>

				</div>


				<div class="border-top">
					<div class="container text-center py-5">

						<!-- logo -->
						<span class="h--70 d-inline-flex align-items-center">
							<img src="assets/images/logo/singlelinelogo.svg" width="440" height="150" alt="Victoria's Blossom Imports Inc.">
						</span>

						<p class="text-gray-500 fs--14">

							&copy; Victoria's Blossom Imports Inc. 2015 &copy; 2019.

							<br>

							All Rights Reserved.

						</p>

					</div>
				</div>

			</footer>
			<!-- /Footer -->


		</div><!-- /#wrapper -->

	
  </body>
</html>
