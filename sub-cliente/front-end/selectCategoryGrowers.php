<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

require_once("../config/config_gcp.php");

$htmlLoadData="";
if(isset($_POST["idBuyer"]) && $_POST['idBuyer']!=''){
$idBuyer = $_POST['idBuyer'];
$idCat = $_POST['idCateg'];

		$sql_categories = "select s.id as id , s.name as name ,count(*) as num_reg
from buyer_requests br
inner join grower_offer_reply gor on gor.offer_id = br.id
inner join buyer_orders bo  on br.id_order = bo.id
inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name
inner join subcategory s ON p.subcategoryid = s.id
inner join colors cl ON p.color_id = cl.id
inner join growers g on gor.grower_id = g.id
left join features f on br.feature = f.id
left JOIN buyer_requests res ON gor.request_id = res.id and res.comment = 'SubClient-Reques'
where br.buyer   = '$idBuyer'
 and bo.availability = 1
 and g.active     = 'active'
 and s.id!='130'
 and p.status_image = '0'
 and (gor.bunchqty-gor.reserve) > 0
group by s.id,s.name
order by s.name
";

       $rs_categories = mysqli_query($con,$sql_categories);

           while ($row_categories = mysqli_fetch_array($rs_categories))
           {
						 $Subcat = '';
						 if($idCat==$row_categories['id']){
							 $Subcat = '<span class="badge badge-success float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1">Selected</span>';
						 }
           $htmlLoadData .='<li class="nav-item"><a class="nav-link px-0" href="javascript:onclick=funSearchPageCat('.$row_categories['id'].');">'.$Subcat.'<i class="fi fi-arrow-end m-0 fs--12"></i><span class="px-2 d-inline-block">'.$row_categories['name'].'</span></a></li>
';
           }

 echo $htmlLoadData;
}
?>
