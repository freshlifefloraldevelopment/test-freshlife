<?php
session_start();

if(!$_SESSION["subclient"]){
    header("Location: login.php");
		exit();
}

$clientSessionID = $_SESSION["subclient"];
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/

/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO REAL *******/
require_once("../config/config_gcp.php");
/**********************************************************/
/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO local *******/
//require_once("../../config/config_gcp.php");
/**********************************************************/

include('GlobalFSyn.php');

$totalOrder =0;



include('inc/header-2.php');
 ?>
<style type="text/css">
	.custom_select button.btn.dropdown-toggle.select-form-control.border {
    padding-top: 0px;
}
</style>

	<section class="bg-light p-0">
				<div class="container py-5">

					<h1 class="h3">
						My Orders
					</h1>

					<nav aria-label="breadcrumb">
						<ol class="breadcrumb fs--14">
							<li class="breadcrumb-item"><a href="subclient-market-place.php">Home</a></li>

							<li class="breadcrumb-item active" aria-current="page">Orders</li>
						</ol>
					</nav>

				</div>
			</section>

<!-- -->
			<section>
				<div class="container">

					<div class="row">

						<div class="col-12 col-sm-12 col-md-12 col-lg-3 mb--60">

							<nav class="sticky-kit nav-deep nav-deep-light js-ajaxified js-stickified" style="">

								<!-- mobile only -->
								<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none js-togglified" data-target="#nav_responsive" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">
									<span class="group-icon px-2 py-2 float-start">
										<i class="fi fi-bars-2"></i>
										<i class="fi fi-close"></i>
									</span>

									<span class="h5 py-2 m-0 float-start">
										Account Menu
									</span>
								</button>

								<!-- desktop only -->
								<h5 class="pt-3 pb-3 m-0 d-none d-lg-block">
									Account Menu
								</h5>


								<!-- navigation -->
								<ul id="nav_responsive" class="nav flex-column d-none d-lg-block">

									<li class="nav-item active">
										<a class="nav-link px-0" href="my-orders.php">
											<i class="fi fi-arrow-end m-0 fs--12"></i>
											<span class="px-2 d-inline-block">
												My Orders
											</span>
										</a>
									</li>

								</ul>

							</nav>

						</div>


						<div class="col-12 col-sm-12 col-md-12 col-lg-9">

              <?php

               $query = query_orders($con,$_SESSION["subclient"]);
              $sql_orders = mysqli_query($con, $query);
              while($row_orders = mysqli_fetch_array($sql_orders))
              {
            if($row_orders["market"]=="0"){


                $Syni = $Syni+1;
                $id = $row_orders["id"];
                $id_order   = $row_orders["order_cli"];
                $id_cliente = $row_orders["client_id"];
                $order_comment = $row_orders["comment1"];
                $order_date = $row_orders["date_add"];
                $status = $row_orders["status_ord"];

                if($status=="Pending / Active"){
                  $color = "text-success";
                }else{
                  $color = "text-warning";
                }

                $totalOrder=0;

                $queryOrders = @mysqli_query($con,getOrdersTotal($id_order,$_SESSION["subclient"],$_SESSION["buyer"]));
                while($row_products = mysqli_fetch_array($queryOrders))
                {

                $price = $row_products["price"];
                $stems = $row_products["noofstems"];


                $bunches = $row_products["qty"];

                $totalOrder = $totalOrder + $bunches*$price;


                }

              ?>

							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="my-orders-details.php?codOrderDetail=<?php echo $id_order; ?>" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="my-orders-details.php?codOrderDetail=<?php echo $id_order; ?>" class="text-dark">
										Order #<?php echo $id_order; ?>
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: <?php echo $order_date; ?> | Total: $<?php echo round($totalOrder+(($totalOrder*5)/100),2)?>
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="<?php echo $color; ?> font-weight-normal"><?php echo $status; ?></span>
								</p>

							</div>
							<!-- /order -->

            <?php
                }
            }
               ?>



							<!-- order -->
              <!--
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #1123
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-info font-weight-normal">Refunded</span>
								</p>

							</div>
            -->
							<!-- /order -->



							<!-- order -->
              <!--
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #1009
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-danger font-weight-normal">Canceled</span>
								</p>

							</div>
							<!-- /order -->




							<!-- order -->
              <!--
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #987
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-success font-weight-normal">Completed</span>
								</p>

							</div>
            -->
							<!-- /order -->



							<!-- pagination -->
              <!--
							<nav aria-label="pagination" class="mt-5">
								<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">

									<li class="page-item disabled">
										<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
									</li>

									<li class="page-item active">
										<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
									</li>

									<li class="page-item" aria-current="page">
										<a class="page-link" href="#">2</a>
									</li>

									<li class="page-item">
										<a class="page-link" href="#">3</a>
									</li>

									<li class="page-item">
										<a class="page-link" href="#">Next</a>
									</li>

								</ul>
							</nav>
            -->
							<!-- pagination -->
						</div>

					</div>

				</div>
			</section>
			<!-- / -->

<?php include('inc/footer-3.php'); ?>
