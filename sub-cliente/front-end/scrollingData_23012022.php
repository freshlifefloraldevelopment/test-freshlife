<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 13 Dec 2021
scrollingData
**/

/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO REAL *******/
require_once("../config/config_gcp.php");
/**********************************************************/
/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO local *******/
//require_once("../../config/config_gcp.php");
/**********************************************************/
include('GlobalFSyn.php');

$htmlLoadData="";
if(isset($_POST["limit"],$_POST['start'],$_POST['query']))
{
$limit = $_POST['limit'];
$start = $_POST['start'];
$query2 = $_POST['query'];
$subClientT = $_SESSION['subclient'];
$buyerT = $_SESSION['buyer'];

$query2 = query_main($start, $limit,$_SESSION["buyer"],$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],'',$con,$_SESSION["tagidF"]);
$sql_products = mysqli_query($con, $query2);


while($row_products = mysqli_fetch_array($sql_products))
{
	$Syni = $Syni+1;
	$category_id_product  = $row_products["categoryid"];
	$id_product           = $row_products["id"];
	$name_product         = $row_products["Produtcname"];
	$product_size         = $row_products["size"];
	$bunchname            = $row_products["steams"];
	$img_product          = $row_products["img"];
	$subcategory_product  = $row_products["subcatename"];
	$subcategoryid        = $row_products["subcategoryid"];
	$featureName          = $row_products["featurename"];
	$featureId            = $row_products["fid"];
	$bunchqty             = $row_products["bunchqty"];
	$idgor                = $row_products['idgor'];
	$product_stems        = $row_products['gorsteams'];
	$inven_type           = $row_products['tab_title1'];
	$fact_id              = $row_products['id_fact'];

	if($featureId==''){$featureId= 0;}

	$getIdSize = mysqli_query($con,"select id from sizes where name ='$product_size'");
	$getNamesArray = mysqli_fetch_array($getIdSize);
	$sizeID = $getNamesArray['id'];

	$getIdStems = mysqli_query($con,"select id from bunch_sizes where name ='$product_stems'");
	$getNamesArrayStems = mysqli_fetch_array($getIdStems);
	$stemsID = $getNamesArrayStems['id'];

	//if($featureId==''){$aaaa = $product_stems. " st/bu";}else{$aaaa = "";}
	if($featureId==''){$aaaa = $product_stems. " st/bu";}else{$aaaa = $product_stems. " st/bu";}

	if($subcategoryid=='130'){$bbbb = ""; $aaaa = $product_stems. " Unit/Box";}else{$bbbb = "Size: ".$product_size.  " [cm] ";}

	if($inven_type == "HOLANDA"){
	$figure ="<figure class='m-0 text-center bg-light-radial rounded-top overflow-hidden'><img class='img-fluid bg-suprime opacity-9 img' alt='$name_product'  src='$img_product' width='300px' height='300px'></figure>";} else {
  $figure ="<figure class='m-0 text-center bg-light-radial rounded-top overflow-hidden'><img class='img-fluid bg-suprime opacity-9 img' alt='$name_product'  src='https://app.freshlifefloral.com/$img_product' width='300px' height='300px'></figure>";}
	if($featureName!=''){$featureNameB = "<button type='button' class='float-end btn btn-sm btn-block  btn-success text-white fs--13 b-0 w--120 mt-1'><i class='fa fa-certificate fa-spin text-warning' aria-hidden='true'></i> $featureName</button>";}else{$featureNameB = "";}

	$i=0;
	$selectComboMedio='';
	$selectCombo1 =  "<select id='select_options$Syni' class='form-control'>";
	for($i = 1; $i<=$bunchqty;$i++){$selectComboMedio .= "<option value='$i'>$i</option>";}
	$selectCombo2 = "</select>";
	$selectComboTotal = $selectCombo1.$selectComboMedio.$selectCombo2;
	$valorPrecio = precioProductSelected($con,$subcategoryid,$sizeID,$id_product,$featureId,$stemsID);
	$namT = str_replace(' ', '-', $name_product);

		$htmlLoadData .="<div class='col-6 col-md-4 mb-4 mb-2-xs'><div class='bg-white shadow-md shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100'><div class='position-absolute top-0 start-0 m-3 m-1-xs z-index-1'><div class='bg-danger text-white fs--13 opacity-9 px-2 py-1 mb-1 rounded timer-countdown timer-countdown-inline' data-timer-countdown-from='11/21/2030 16:00:00' data-timer-countdown-end-hide-self='true' data-timer-countdown-end-hide-target=''><b>Available: </b> <b id='bunchqty$Syni'>$bunchqty</b></div></div><div class='position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 '><a href='#' onclick=showmodal('$img_product','$subcategory_product','$namT','$product_size');  class='btn btn-danger shadow-lg btn-sm rounded-circle mb-2' title='View Product' data-target='#farm_modal_1' data-toggle='modal' data-placement='left' data-original-title='View'><i class='fa fa-search' aria-hidden='true'></i></a></div>
			<style>.img {float: left; width:  250px; height: 250px; object-fit: cover; } p {text-align: center; font-size: 60px; margin-top: 0px;}</style>
			<div class='d-block text-decoration-none'>$figure
			<span class='d-block text-center-xs text-gray-600 py-3'><span class='d-block fs--16 max-h-150 overflow-hidden'>
					$subcategory_product $name_product <br> $bbbb$aaaa
					$featureNameB
			</span><span class='d-block text-danger font-weight-medium fs--16 mt-2'>$ $valorPrecio <input type='hidden' id='priceSelected$Syni' value='$valorPrecio' /></span><div class='form-label-group mb-3 '>$selectComboTotal <label for='select_options'>Select quantity</label></div><div class='form-label-group mb-3'><center><button onclick=requestProduct('$subClientT','$fact_id','$buyerT','$id_product','$Syni','$sizeID','$featureId','$idgor','bunchqty$Syni') class='btn btn-sm rounded-circle-xs btn-danger btn-pill'><i class='fi fi-plus'></i><span>Add to Order</span></button></center></div></span></div></div></div>";

}

 echo "<div class='row gutters-xs--xs'>".$htmlLoadData."</div>";
}
?>
