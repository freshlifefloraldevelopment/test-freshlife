<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

require_once("../../config/config_gcp.php");

$htmlLoadData="";
if(isset($_POST["idBuyer"]) && $_POST['idBuyer']!=''){
$idBuyer = $_POST['idBuyer'];
$idSizes = str_replace(',',' ',$_POST['idSize']);

		$sql_sizes = "select sz.id as size_id, gor.size as size_cm , sz.name as size_name
from buyer_requests br
inner join grower_offer_reply gor on gor.offer_id = br.id
inner join buyer_orders bo  on br.id_order = bo.id
inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name
inner join subcategory s ON p.subcategoryid = s.id
inner join colors cl ON p.color_id = cl.id
inner join sizes sz on gor.size = sz.name
inner join growers g on gor.grower_id = g.id
left join features f on br.feature = f.id
left JOIN buyer_requests res ON gor.request_id = res.id and res.comment = 'SubClient-Reques'
where br.buyer   = '$idBuyer'
 and bo.availability = 1
 and g.active     = 'active'
 and (gor.bunchqty-gor.reserve) > 0
group by sz.name
order by sz.name
";

       $rs_sizes = mysqli_query($con,$sql_sizes);

           while ($row_sizes= mysqli_fetch_array($rs_sizes))
           {

						 $Sizesselect = '';
						 if (strpos($idSizes, $row_sizes['size_id']) !== false)
						 {
						 	 $Sizesselect = 'checked';
						 }

           $htmlLoadData .='<label class="form-selector"><input onclick="get_size_data('.$row_sizes['size_id'].')" name="size['.$row_sizes['size_id'].']" type="checkbox" '.$Sizesselect.'><span>'.$row_sizes['size_name']." cm".'</span></label>';
           }

 echo $htmlLoadData;
}
?>
