<?
	include "../config/config.php";
	session_start();
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}
	if(isset($_POST["Submit"]) && $_POST["Submit"]=="Add")
	{	
	   $temp = "";
	   for($i=0;$i<=6;$i++)
	   {		     
		   if($_POST["chk".$i]!="")
		   {
			  $temp.= $_POST["chk".$i].",";
		   }
	   }
	   	   
	   if($_POST["type"]==1)
	   {
		   $rate=round($_POST["box_weight_arranged"]/$_POST["price_per_box"],2);
		   $addtional_rate=round($_POST["price_extra_kilo"],2);
		   $price_per_box="0.00";
	   }
	   else if($_POST["type"]==2)
	   {
		   $rate_temp=round($_POST["rate"]+$_POST["fuel_surcharge"]+$_POST["security"],2);
		   $extra=round(($_POST["awp"]+$_POST["fee"]+$_POST["fitosnary_fee"]+$_POST["due_agnet"])/$_POST["kilograms"],2);
		   $rate=round($rate_temp+$extra,2);
		   $addtional_rate="0.00";
		   $price_per_box="0.00";
	   }
	    else if($_POST["type"]==4)
		{
			$rate=round($_POST["price_cubic_feet"],2);
			$addtional_rate="0.00";
			$price_per_box="0.00";
		}
		else if($_POST["type"]==3)
		{
			$price_per_box=round($_POST["price_per_box2"],2);
			$rate="0.00";
			$addtional_rate="0.00";
		}
	   
	   
	    $temp=trim($temp,","); 
	    $ins="insert into connections set
		shipping_id='".$_GET["id"]."',
		from1='".$_POST["from"]."',
		to1='".$_POST["to"]."',
		cargo_agency='".$_POST["cargo_agency"]."',
		shipping_company='".$_POST["shipping_company"]."',
		type='".$_POST["type"]."',
		unit='".$_POST["unit"]."',
		days='".$temp."',
        trasit_time='".$_POST["trasit_time"]."',
		mode_of_transport='".$_POST["mode_of_transport"]."',
		box_weight_arranged='".$_POST["box_weight_arranged"]."',
		price_per_box='".$_POST["price_per_box"]."',
		price_extra_kilo='".$_POST["price_extra_kilo"]."',
		kilograms='".$_POST["kilograms"]."',
		rate='".$_POST["rate"]."',
		fuel_surcharge='".$_POST["fuel_surcharge"]."',
		security='".$_POST["security"]."',
		awb='".$_POST["awb"]."',
		fee='".$_POST["fee"]."',
		fitosnary_fee='".$_POST["fitosnary_fee"]."',
		due_agnet='".$_POST["due_agnet"]."',
        minimum_cubic_feet='".$_POST["minimum_cubic_feet"]."',
		price_cubic_feet='".$_POST["price_cubic_feet"]."',
		shipping_rate='".$rate."',	
		addtional_rate='".$addtional_rate."',
		box_price='".$price_per_box."',
		price_per_box2='".$_POST["price_per_box2"]."'";
		mysql_query($ins);
		header('location:connections.php?id='.$_GET["id"]);
	}

    $sel_info="select * from shipping_method where id='".$_GET["id"]."'";
	$rs_info=mysql_query($sel_info);
	$info=mysql_fetch_array($rs_info);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript">
	function verify()
	{ 
		var arrTmp=new Array();
		arrTmp[0]=checkfrom();
		arrTmp[1]=checkto();
		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)
		{

			if(arrTmp[i]==false)
			{
			   _blk=false;

			}

		}

		if(_blk==true)
		{
			return true;
		}

		else
		{
			return false;
		}

 	}	

	function trim(str) 
	{    
		if (str != null) 
		{        
			var i;        
			for (i=0; i<str.length; i++) 
			{           
				if (str.charAt(i)!=" ") 
				{               
					str=str.substring(i,str.length);                 
					break;            

				}        
			}            

			for (i=str.length-1; i>=0; i--)
			{            
				if (str.charAt(i)!=" ") 
				{                
					str=str.substring(0,i+1);                
					break;            
				}         

			}                 

			if (str.charAt(0)==" ") 
			{            
				return "";         
			} 

			else 
			{            
				return str;         
			}    

		}

	}

	function checkfrom()
	{
		if(trim(document.frmcat.from.value) == "")
		{	 
			document.getElementById("lblfrom").innerHTML="Please enter Source City ";
			return false;
		}
		else 
		{
			document.getElementById("lblfrom").innerHTML="";
			return true;
		}

	}
	
	function checkto()
	{
		if(trim(document.frmcat.to.value) == "")
		{	 
			document.getElementById("lblto").innerHTML="Please enter Destination City ";
			return false;
		}
		else 
		{
			document.getElementById("lblto").innerHTML="";
			return true;
		}

	}
	
	function doshowtype()
	{
		$('#spdiv').hide();
	    $('#airdiv2').hide();
		
		 if($('#mode_of_transport').val()==1)
		 {
			  $('#typediv').show();
			  $('#cargodiv').show();
			  $('#type1div').show();
			   $('#type2div').hide();
			  
		 }
		 else
		 {
			   $('#typediv').show();
			   $('#cargodiv').hide();
			    $('#type2div').show();
			   $('#type1div').hide()
		 }
	}
	
	
	function perbox()
	{
		$('#type1').hide();
		$('#type2').show();
	}
	
	function percubicfeet()
	{
		$('#type2').hide();
		$('#type1').show();
	}
	
	function doshowother()
	{
	  $('#airdiv2').show();	
	  $('#spdiv').hide();
	}
	
	function dohideother()
	{
	  $('#spdiv').show();
	  $('#airdiv2').hide();
	}

	

</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <? include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <? include("includes/shipping-left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Add New Connection ( <?=$info["name"]?> ) </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td><a class="pagetitle1" href="connections.php?id=<?=$_GET["id"]?>" onclick="this.blur();"><span> Manage Connections ( <?=$info["name"]?> ) </span></a></td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          
                            <tr>
                              <td><div id="box">
                              <form name="frmcat" action="connection_add.php?id=<?=$_GET["id"]?>" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                  <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                    <tr>
                                      <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>From </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="from" id="from" value="<?=$_POST["from"]?>" />
                                        <br>
                                        <span class="error" id="lblfrom"></span></td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>To </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="to" id="to" value="<?=$_POST["to"]?>" />
                                        <br>
                                        <span class="error" id="lblto"></span></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Transit Days </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <select class="listmenu" name="trasit_time" id="trasit_time" style="width:150px;" >
                                      <option value="">--Select--</option>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                      <option value="6">6</option>
                                      <option value="7">7</option>
                                      </select>
                                        <br>
                                        <span class="error" id="lbltrasit_time"></span></td>
                                    </tr>
                                    
                                    <tr>
                                     <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Days Avialable</td>
                                    <td><div>
                                    <input name="chk1" id="chk1" type="checkbox" value="1" />&nbsp;&nbsp;Monday<br/>
                                    <input name="chk2" id="chk2" type="checkbox" value="2" />&nbsp;&nbsp;Tuesday<br/>
                                    <input name="chk3" id="chk3" type="checkbox" value="3" />&nbsp;&nbsp;Wednesday<br/>
                                    <input name="chk4" id="chk4" type="checkbox" value="4" />&nbsp;&nbsp;thursday<br/>
                                    <input name="chk5" id="chk5" type="checkbox" value="5" />&nbsp;&nbsp;Friday<br/>
                                    <input name="chk6" id="chk6" type="checkbox" value="6" />&nbsp;&nbsp;Saturday<br/>
                                    <input name="chk0" id="chk0" type="checkbox" value="0" />&nbsp;&nbsp;Sunday<br/> 
                                    </div></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Mode of Transport </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <select class="listmenu" name="mode_of_transport" id="mode_of_transport" style="width:150px;" onchange="doshowtype();">
                                      <option value="">--Select--</option>
                                      <option value="1">Air</option>
                                      <option value="2">Truck</option>
                                      </select>
                                     
                                        <br>
                                        <span class="error" id="lblmode_of_transport"></span></td>
                                    </tr>
                                    
                                    <tr style="display:none;" id="cargodiv">
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Cargo Agency </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <select class="listmenu" name="cargo_agency" id="cargo_agency" style="width:150px;">
                                      <option value="">--Select--</option>
                                      <?
									     $sel_cargo_agency="select * from cargo_agency order by name";
										 $rs_carg_agency=mysql_query($sel_cargo_agency);
										 while($cargo_agency=mysql_fetch_array($rs_carg_agency))
										 {
									  ?>
                                      
                                           <option value="<?=$cargo_agency["id"]?>"><?=$cargo_agency["name"]?></option>
                                      
                                      <?
										 }
									  ?>
                                      </select>
                                     
                                        <br>
                                        <span class="error" id="lblcargo_agency"></span></td>
                                    </tr>
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Airline or Transport Company</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <select class="listmenu" name="shipping_company" id="shipping_company" style="width:150px;">
                                      <option value="">--Select--</option>
                                      <?
									     $sel_cargo_agency="select * from shipping_company order by name";
										 $rs_carg_agency=mysql_query($sel_cargo_agency);
										 while($cargo_agency=mysql_fetch_array($rs_carg_agency))
										 {
									  ?>
                                      
                                           <option value="<?=$cargo_agency["id"]?>"><?=$cargo_agency["name"]?></option>
                                      
                                      <?
										 }
									  ?>
                                      </select>
                                     
                                        <br>
                                        <span class="error" id="lblshipping_company"></span></td>
                                    </tr>
                                    
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Unit</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <select class="listmenu" name="unit" id="unit" style="width:150px;">
                                      <option value="">--Select--</option>
                                      <option value="1">Weight</option>
                                      <option value="2">Cubic Feet</option>
                                      </select>
                                     
                                        <br>
                                        <span class="error" id="lblunit"></span></td>
                                    </tr>
                                    
                                    <tr id="typediv" style="display:none;">
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Type</td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      
                                      <div id="type1div">
                                      <input type="radio" name="type" id="type" value="1" onClick="dohideother();"  />&nbsp; Special Condition ( Consolidation ) <br/>
                                      <input type="radio" name="type" id="type" value="2" onClick="doshowother();"  />&nbsp; Single Air Way Bill <br/>
                                      </div>
                                      
                                      <div id="type2div">
                                      <input type="radio" name="type" id="type" value="3" onClick="perbox();"  />&nbsp; Per Box <br/>
                                      <input type="radio" name="type" id="type" value="4" onClick="percubicfeet();"  />&nbsp; Per Cubic Feet <br/>
                                      </div>
                                      
                                        <br>
                                        <span class="error" id="lbltype"></span></td>
                                    </tr>
                                    
                                     <tr style="display:none;" id="spdiv">
                                   <td colspan="2" style="padding-left:20px;">
                                   <br/> <b> Special Consolidation :</b> <br/>
                                   <table style="width:100%">
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Box Weight Arranged </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="box_weight_arranged" id="box_weight_arranged" value="<?=$_POST["box_weight_arranged"]?>" />
                                        <br>
                                        <span class="error" id="lblbox_weight_arranged"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Price Per Box </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_per_box" id="price_per_box" value="<?=$_POST["price_per_box"]?>" />
                                        <br>
                                        <span class="error" id="lblprice_per_box"></span></td>
                                    </tr>
                                    
                                    <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Price Extra Kilo. </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_extra_kilo" id="price_extra_kilo" value="<?=$_POST["price_extra_kilo"]?>" />
                                      
                                        <span class="error" id="lblprice_extra_kilo"></span></td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    
                                    <tr style="display:none;"  id="airdiv2" ><td colspan="2" style="padding-left:20px;">
                                    <br/> <b> Single Air Way Bill :</b> <br/>
                                    <table style="width:100%">
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Kilograms </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <input type="text" class="textfieldbig" name="kilograms" id="kilograms" value="<?=$_POST["kilograms"]?>" />
                                       <br>
                                        <span class="error" id="lblkilograms"></span>
                                        </td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Rate(KG) </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="rate" id="rate" value="<?=$_POST["rate"]?>" />
                                        <br>
                                        <span class="error" id="lblrate"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Fuel Surcharge. </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="fuel_surcharge" id="fuel_surcharge" value="<?=$_POST["fuel_surcharge"]?>" />
                                        <span class="error" id="lblfuel_surcharge"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Sec(Security). </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="security" id="security" value="<?=$_POST["security"]?>" />
                                        <span class="error" id="lblsecurity"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>AWB </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="awb" id="awb" value="<?=$_POST["awb"]?>" />
                                        <span class="error" id="lblawb"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>From a Fee(Each Form) </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="fee" id="fee" value="<?=$_POST["fee"]?>" />                                     <span class="error" id="lblfee"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Fitosnary Free </td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="fitosnary_fee" id="fitosnary_fee" value="<?=$_POST["fitosnary_fee"]?>" />                                     <span class="error" id="lblfitosnary_fee"></span></td>
                                    </tr>
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Due Agent</td>
                                      <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="due_agnet" id="due_agnet" value="<?=$_POST["due_agnet"]?>" />                                     <span class="error" id="lbldue_agnet"></span></td>
                                    </tr>
                                   
                                    
                                    </table> 
                                    </td>
                                    </tr>
                                    
                                    <tr style="display:none;" id="type1">
                                    <td colspan="2">
                                    <b>Price per Cubic Feet </b> <br/>
                                    <table style="width:100%">
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Minimum Cubic Feet </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <input type="text" class="textfieldbig" name="minimum_cubic_feet" id="minimum_cubic_feet" value="<?=$_POST["minimum_cubic_feet"]?>" />
                                       <br>
                                        <span class="error" id="lblminimum_cubic_feet"></span>
                                        </td>
                                    </tr>
                                    
                                    
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Price Per Cubic Feet </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <input type="text" class="textfieldbig" name="price_cubic_feet" id="price_cubic_feet" value="<?=$_POST["price_cubic_feet"]?>" />
                                       <br>
                                        <span class="error" id="lblprice_cubic_feet"></span>
                                        </td>
                                    </tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    <tr style="display:none;" id="type2">
                                    <td colspan="2">
                                    <b> Price per Box </b> <br/>
                                    <table style="width:100%">
                                     <tr>
                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Price per Box </td>
                                      <td width="66%" bgcolor="#f2f2f2">
                                      <input type="text" class="textfieldbig" name="price_per_box2" id="price_per_box2" value="<?=$_POST["price_per_box2"]?>" />                                       <br>
                                        <span class="error" id="lblprice_per_box2"></span>
                                        </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                    </tr>
                                  </table>
                              </form>    
                                </div></td>
                            </tr>
                          
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <? include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
