<?	
	include "../config/config.php";
	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}

		$sel="select * from admin where uname='admin'";
		$rs_sel=mysql_query($sel);
		$v=mysql_fetch_array($rs_sel);
		if(isset($_POST['Submit'])&& $_POST["Submit"]=="Change")
	{		
		$change = "UPDATE `admin` SET  pass='".$_POST["txtnewpass"]."' WHERE uname='admin'";
		mysql_query($change);
		header("Location: home.php");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript">
	
	function trim(str) 
		{    
			 if (str != null) 
			 {        
			 	var i;        
			    for (i=0; i<str.length; i++) 
				{           
					 if (str.charAt(i)!=" ") 
					 {               
					   str=str.substring(i,str.length);                 
					   break;            
					 }        
				}            
					  
					 for (i=str.length-1; i>=0; i--)
					 {            
					 	if (str.charAt(i)!=" ") 
						{                
							str=str.substring(0,i+1);                
						     break;            
						}         
					 }                 
					if (str.charAt(0)==" ") 
					{            
						return "";         
					} 
					else 
					{            
					    return str;         
					}    
				}
			}

	function verify()
	{ 
		var arrTmp=new Array();
		arrTmp[0]=checkoldpass();
		arrTmp[1]=checknewpass();
		arrTmp[2]=checkconpass();
	
		var i;
		_blk=true;
		for(i=0;i<arrTmp.length;i++)
		{
			if(arrTmp[i]==false)
			{
			   _blk=false;
			}
		}
		if(_blk==true)
		{
			return true;
		}
		else
		{
			return false;
		}
	
 	}	
	
	function checkoldpass()
	{
		
		if(trim(document.frmcpass.txtoldpass.value) == "")
		{	 
			document.getElementById("lbloldpass").innerHTML="Please enter old password ";
			return false;
		}
		else
		{
			if(document.frmcpass.txtold.value != document.frmcpass.txtoldpass.value)
			{
				document.getElementById("lbloldpass").innerHTML="Old password is not correct ";
				return false;
			}
			else
			{
				document.getElementById("lbloldpass").innerHTML="";
				return true;
			}
		}
	}
	function checknewpass()
	{
		
		if(trim(document.frmcpass.txtnewpass.value) == "")
		{	 
			document.getElementById("lblnewpass").innerHTML="Please enter new password ";
			return false;
		}
		else
		{
			document.getElementById("lblnewpass").innerHTML="";
			return true;
		}
	}
	function checkconpass()
	{
		
		if(trim(document.frmcpass.txtconpass.value) == "")
		{	 
			document.getElementById("lblconpass").innerHTML="Please enter confirm password ";
			return false;
		}
		else
		{
			if(document.frmcpass.txtnewpass.value != document.frmcpass.txtconpass.value)
			{
				document.getElementById("lblconpass").innerHTML="New password & confirm password must be same ";
				return false;
			}
			else
			{
				document.getElementById("lblconpass").innerHTML="";
				return true;
			}
		}
	}
	
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <? include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <? include("includes/left.php");?>
        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10">&nbsp;</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5"></td>
                  </tr>
                  <tr>
                    <td class="pagetitle">Change Password </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><div id="box">
                      <span class="error">* </span><span class="text">indicates mandatory field required </span><br /><br />
              <table style="border-collapse: collapse;" width="100%" border="1" cellpadding="2"
                                            cellspacing="0" bordercolor="#e4e4e4">
                <form action="change_pass.php" method="post" name="frmcpass" id="frmcpass" onsubmit="return verify()">
                  <tr height="24">
                    <td width="17%"  align="right" class="text">&nbsp;&nbsp;&nbsp;User Name : </td>
                    <td width="83%" bgcolor="#f2f2f2" class="text"><?=$v["uname"]?></td>
                  </tr>
                  <tr>
                    <td align="right" valign="middle" class="text"><span class="error">* </span>Old Password : </td>
                    <td bgcolor="#f2f2f2"><input name="txtoldpass" type="password" class="textfieldbig" value="<?=$_POST["txtoldpass"]?>" />
                        <input name="txtold" type="hidden" class="redtext" id="txtold" value="<?=$v["pass"]?>" />
                        <br />
                        <span id="lbloldpass" class="error"></span></td>
                  </tr>
                  <tr>
                    <td align="right" class="text" valign="middle"><span class="error">* </span>New Password : </td>
                    <td bgcolor="#f2f2f2"><input name="txtnewpass" type="password" class="textfieldbig"  value="<?=$_POST["txtnewpass"]?>"/>
                        <br />
                        <span  id="lblnewpass" class="error"></span> </td>
                  </tr>
                  <tr>
                    <td align="right" class="text" valign="middle"	><span class="error">* </span>Confirm Password : </td>
                    <td bgcolor="#f2f2f2"><input name="txtconpass" type="password" class="textfieldbig"  value="<?=$_POST["txtconpass"]?>"/>
                        <br />
                        <span id="lblconpass" class="error"></span> </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td><input name="Submit" type="submit" class="buttongrey" value="Change" />                    </td>
                  </tr>
                </form>
              </table>
            </div></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr></tr>
                  <tr></tr>
                </table></td>
                <td width="10">&nbsp;</td>
              </tr>
            </table></td>
            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>
          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>
          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <? include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
