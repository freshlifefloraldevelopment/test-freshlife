<?

	ini_set("memory_limit","100M");
	include "../config/config.php";
	include 'resize.image.class.php';
	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}
	
	
	if(isset($_POST['Submit'])&& $_POST["Submit"]=="Add")
	{
		
		
		if($_FILES['image1']['name'] != "")
		{
		    $today = date('mdyHis');
			$tmp1 = $_FILES['image1']['name'];
			$ext1 = explode('.',$tmp1);
			$image=0;
					
			$uploaddir = '../gallery-image/';
							
			if(($ext1[1]=="gif"||$ext1[1]=="jpeg" || $ext1[1]=="jpg" || $ext1[1]=="png" || $ext1[1]=="GIF"||$ext1[1]=="JPEG" || $ext1[1]=="JPG" || $ext1[1]=="PNG" ))
			{
			    $ext1[1] = strtolower($ext1[1]);
				$uploadfile1 = $uploaddir.$today."big.".$ext1[1];
				$filepath1 = 'gallery-image/'.$today."big.".$ext1[1];
				$filepath2 = 'gallery-image/small/'.$today."_crop.".$ext1[1];
				$filepath3 = 'gallery-image/big/'.$today."_crop.".$ext1[1];
								
				move_uploaded_file($_FILES['image1']['tmp_name'],$uploadfile1);				
				list($width, $height) = getimagesize("../$filepath1");
				
				if($width > 316 || $height > 288 ) 
				{
				
					    if($width > $height )
					
						{
										$image = new Resize_Image;
										$image->new_width = 316;
										$image->new_height = 288;
										
										$simage = new Resize_Image;
										$simage->new_width = 79;
										$simage->new_height = 72;
										
						}
						
						if ($height >$width)
					
						{
										$image = new Resize_Image;
										$image->new_width = 288;
										$image->new_height = 316;
										
										$simage = new Resize_Image;
										$simage->new_width = 75;
										$simage->new_height = 72;
										
						}
								
						if ($height ==$width)
					
						{
										$image = new Resize_Image;
										$image->new_width = 316;
										$image->new_height = 316;
										
										$simage = new Resize_Image;
										$simage->new_width = 79;
										$simage->new_height = 72;
										
						}		
										
						$image->image_to_resize = "../$filepath1"; // Full Path to the file
						
										$image->ratio = true; // Keep Aspect Ratio?
										
										// Name of the new image (optional) - If it's not set a new will be added automatically
						
										$image->new_image_name =$today.'_crop';
						
										/* Path where the new image should be saved. If it's not set the script will output the image without saving it */
						
										$image->save_folder = '../gallery-image/big/';
						
										$process = $image->resize();
										
										$simage->image_to_resize = "../$filepath1"; // Full Path to the file
				
										$simage->ratio = false; // Keep Aspect Ratio?
				
										// Name of the new image (optional) - If it's not set a new will be added automatically
				
										$simage->new_image_name =$today.'_crop';
				
									 	/* Path where the new image should be saved. If it's not set the script will output the image without saving it */
				
										$simage->save_folder = '../gallery-image/small/';
				
									    $process = $simage->resize();
										
										$ins_image="insert into gallery_mgmt (image_path,thumbnail,title) values ('".$filepath3."','".$filepath2."','".$_POST["title1"]."')";
										mysql_query($ins_image);
														
										unlink("../$filepath1");

			      
			    }
				  
			}
	 }				
	 
	 
	    if($_FILES['image2']['name'] != "")
		{
		    $today = date('mdyHis');
			$tmp1 = $_FILES['image2']['name'];
			$ext1 = explode('.',$tmp1);
			$image=0;
					
			$uploaddir = '../gallery-image/';
							
			if(($ext1[1]=="gif"||$ext1[1]=="jpeg" || $ext1[1]=="jpg" || $ext1[1]=="png" || $ext1[1]=="GIF"||$ext1[1]=="JPEG" || $ext1[1]=="JPG" || $ext1[1]=="PNG" ))
			{
			    $ext1[1] = strtolower($ext1[1]);
				$uploadfile2 = $uploaddir.$today."big2.".$ext1[1];
				$filepath4 = 'gallery-image/'.$today."big2.".$ext1[1];
				$filepath5 = 'gallery-image/small/'.$today."_crop2.".$ext1[1];
				$filepath6 = 'gallery-image/big/'.$today."_crop2.".$ext1[1];
								
				move_uploaded_file($_FILES['image2']['tmp_name'],$uploadfile2);				
				list($width, $height) = getimagesize("../$filepath4");
				
				if($width > 316 || $height > 288 ) 
				{
				
					    if($width > $height )
					
						{
										$image2 = new Resize_Image;
										$image2->new_width = 316;
										$image2->new_height = 288;
										
										$simage2 = new Resize_Image;
										$simage2->new_width = 79;
										$simage2->new_height = 72;
										
						}
						
						if ($height >$width)
					
						{
										$image2 = new Resize_Image;
										$image2->new_width = 288;
										$image2->new_height = 316;
										
										$simage2 = new Resize_Image;
										$simage2->new_width = 75;
										$simage2->new_height = 72;
										
						}
								
						if ($height ==$width)
					
						{
										$image2 = new Resize_Image;
										$image2->new_width = 316;
										$image2->new_height = 316;
										
										$simage2 = new Resize_Image;
										$simage2->new_width = 79;
										$simage2->new_height = 72;
										
						}		
										
						$image2->image_to_resize = "../$filepath4"; // Full Path to the file
						
										$image2->ratio = true; // Keep Aspect Ratio?
										
										// Name of the new image (optional) - If it's not set a new will be added automatically
						
										$image2->new_image_name =$today.'_crop2';
						
										/* Path where the new image should be saved. If it's not set the script will output the image without saving it */
						
										$image2->save_folder = '../gallery-image/big/';
						
										$process = $image2->resize();
										
										$simage2->image_to_resize = "../$filepath4"; // Full Path to the file
				
										$simage2->ratio = false; // Keep Aspect Ratio?
				
										// Name of the new image (optional) - If it's not set a new will be added automatically
				
										$simage2->new_image_name =$today.'_crop2';
				
									 	/* Path where the new image should be saved. If it's not set the script will output the image without saving it */
				
										$simage2->save_folder = '../gallery-image/small/';
				
									    $process = $simage2->resize();
										
				$ins_image="insert into gallery_mgmt (image_path,thumbnail,title) values ('".$filepath6."','".$filepath5."','".$_POST["title2"]."')";
				mysql_query($ins_image);
								
				unlink("../$filepath4");
			
			    }
				  
			}
	 }					
	 
	     if($_FILES['image3']['name'] != "")
		{
		    $today = date('mdyHis');
			$tmp1 = $_FILES['image3']['name'];
			$ext1 = explode('.',$tmp1);
			$image=0;
					
			$uploaddir = '../gallery-image/';
							
			if(($ext1[1]=="gif"||$ext1[1]=="jpeg" || $ext1[1]=="jpg" || $ext1[1]=="png" || $ext1[1]=="GIF"||$ext1[1]=="JPEG" || $ext1[1]=="JPG" || $ext1[1]=="PNG" ))
			{
			    $ext1[1] = strtolower($ext1[1]);
				$uploadfile3 = $uploaddir.$today."big3.".$ext1[1];
				$filepath7 = 'gallery-image/'.$today."big3.".$ext1[1];
				$filepath8 = 'gallery-image/small/'.$today."_crop3.".$ext1[1];
				$filepath9 = 'gallery-image/big/'.$today."_crop3.".$ext1[1];
								
				move_uploaded_file($_FILES['image3']['tmp_name'],$uploadfile3);				
				list($width, $height) = getimagesize("../$filepath7");
				
				if($width > 316 || $height > 288 ) 
				{
				
					    if($width > $height )
					
						{
										$image3 = new Resize_Image;
										$image3->new_width = 316;
										$image3->new_height = 288;
										
										$simage3 = new Resize_Image;
										$simage3->new_width = 79;
										$simage3->new_height = 72;
										
						}
						
						if ($height >$width)
					
						{
										$image3 = new Resize_Image;
										$image3->new_width = 288;
										$image3->new_height = 316;
										
										$simage3 = new Resize_Image;
										$simage3->new_width = 75;
										$simage3->new_height = 72;
										
						}
								
						if ($height ==$width)
					

						{
										$image3 = new Resize_Image;
										$image3->new_width = 316;
										$image3->new_height = 316;
										
										$simage3 = new Resize_Image;
										$simage3->new_width = 79;
										$simage3->new_height = 72;
										
						}		
										
						$image3->image_to_resize = "../$filepath7"; // Full Path to the file
						
										$image3->ratio = true; // Keep Aspect Ratio?
										
										// Name of the new image (optional) - If it's not set a new will be added automatically
						
										$image3->new_image_name =$today.'_crop3';
						
										/* Path where the new image should be saved. If it's not set the script will output the image without saving it */
						
										$image3->save_folder = '../gallery-image/big/';
						
										$process = $image3->resize();
										
										$simage3->image_to_resize = "../$filepath7"; // Full Path to the file
				
										$simage3->ratio = false; // Keep Aspect Ratio?
				
										// Name of the new image (optional) - If it's not set a new will be added automatically
				
										$simage3->new_image_name =$today.'_crop3';
				
									 	/* Path where the new image should be saved. If it's not set the script will output the image without saving it */
				
										$simage3->save_folder = '../gallery-image/small/';
				
									    $process = $simage3->resize();
										
				$ins_image="insert into gallery_mgmt (image_path,thumbnail,title) values ('".$filepath9."','".$filepath8."','".$_POST["title3"]."')";
				mysql_query($ins_image);
								
				unlink("../$filepath7");
			
			    }
				  
			}
	 }			
	 header("Location:gallery_mgmt.php");

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/common.js"></script>
<script language="javascript" type="text/javascript">

function validImageFile(strfile)
	{
			
			var str = strfile;
			var pathLenth = strfile.length;
			var start = (str.lastIndexOf("."));
			var fileType = str.slice(start,pathLenth);
			fileType = fileType.toLowerCase();
			if (strfile.length > 0)
			{
				if((fileType == ".gif") || (fileType == ".jpg") || (fileType == ".jpeg") || (fileType == ".png") || (fileType == ".bmp") || (fileType == ".GIF") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".PNG") || (fileType == ".BMP")) 
				{
					return true;
				}
				else 
				{
					return false;
				} 
			}
	}
	
	
function verify()
	{
		var arrTmp=new Array();
		arrTmp[0]=checkimage1();
		arrTmp[1]=checkimage2();
		arrTmp[2]=checkimage3();
		//arrTmp[3]=checkimage4();
		//arrTmp[4]=checkimage5();

		var i;
        for(i=0;i<arrTmp.length;i++)
        {
            if(arrTmp[i]==false)
            {
                return false;
            }
        }
        return true;
	}
	function checkimage1()
	{
		if(trim(document.frmimage.image1.value) == "")
		{	 
			document.getElementById("lblimage1").innerHTML="";
			return true;
		}
		else
		{
			if(!validImageFile(document.frmimage.image1.value))
			{
				document.getElementById("lblimage1").innerHTML="Please select valid image file";
				return false;
			}
			else
			{
				document.getElementById("lblimage1").innerHTML="";
				return true;
			}
		}
	}
	function checkimage2()
	{
		if(trim(document.frmimage.image2.value) == "")
		{	 
			document.getElementById("lblimage2").innerHTML="";
			return true;
		}
		else
		{
			if(!validImageFile(document.frmimage.image2.value))
			{
				document.getElementById("lblimage2").innerHTML="Please select valid image file";
				return false;
			}
			else
			{
				document.getElementById("lblimage2").innerHTML="";
				return true;
			}
		}
	}
	function checkimage3()
	{
		if(trim(document.frmimage.image3.value) == "")
		{	 
			document.getElementById("lblimage3").innerHTML="";
			return true;
		}
		else
		{
			if(!validImageFile(document.frmimage.image3.value))
			{
				document.getElementById("lblimage3").innerHTML="Please select valid image file";
				return false;
			}
			else
			{
				document.getElementById("lblimage3").innerHTML="";
				return true;
			}
		}
	}
</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<? include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <? include("includes/left.php");?>
        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10">&nbsp;</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5"></td>
                  </tr>
                  <tr>
                    <td class="pagetitle">Add images</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
				  <tr>
                    <td>
					<table width="100%">
					<tr>
					<td>
					<a class="pagetitle1" href="gallery_mgmt.php" onclick="this.blur();"><span> Manage Gallery</span></a>
					</td>
					</tr>
					</table>
					</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                    <form name="frmimage" action="gallery_add.php" method="post" onsubmit="return verify();" enctype="multipart/form-data" >
                  <tr>
                    <td><div id="box">
                      <table style="border-collapse: collapse;" width="100%" border="1" cellpadding="2"
                                            cellspacing="0" bordercolor="#e4e4e4">
                        <tr>
                          <td width="2%" align="center" class="text">&nbsp;</td>
                          <td colspan="3" align="left" class="text">Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                          </tr>
						  <tr><td colspan="3" height="25"><span class="error"><b>Note : Image should be larger than 316/288 px</b></span></td></tr>
						<tr>
                          <td align="center" valign="middle" class="text">&nbsp;</td>
                          <td width="39%" align="left" valign="middle" class="text">Image 1:</td>
							  <td width="59%" bgcolor="#f2f2f2"><input type="file" name="image1" size="32">
						<br>
						<?
						 if($image1==1)
						 {
						   
						       echo'<span class="error" id="lblimage1">Please upload valid image file [jpeg,gif,png].</span>';
						
						}
						else
						{
								echo'<span class="error" id="lblimage1"></span>';
						}
						?>						  </td>
                        </tr>
						<tr>
                          <td align="center" valign="middle" class="text">&nbsp;</td>
                          <td width="39%" align="left" valign="middle" class="text">Title 1:</td>
							  <td width="59%" bgcolor="#f2f2f2"><input type="text" name="title1" class="textfieldbig">
							  <br>
						<span class="error" id="lbltitle"></span>						</td>
						</tr>
						<tr><td colspan="3" height="25"></td></tr>
                        
                        <tr>
                          <td align="center" valign="middle" class="text">&nbsp;</td>
                          <td width="39%" align="left" valign="middle" class="text">Image 2:</td>
							  <td width="59%" bgcolor="#f2f2f2"><input type="file" name="image2" size="32">
						<br>
						<?
						 if($image1==1)
						 {
						   
						       echo'<span class="error" id="lblimage2">Please upload valid image file [jpeg,gif,png].</span>';
						
						}
						else
						{
								echo'<span class="error" id="lblimage2"></span>';
						}
						?>						  </td>
                        </tr>
						<tr>
                          <td align="center" valign="middle" class="text">&nbsp;</td>
                          <td width="39%" align="left" valign="middle" class="text">Title 2:</td>
							  <td width="59%" bgcolor="#f2f2f2"><input type="text" name="title2" class="textfieldbig">
							  <br>
						<span class="error" id="lbltitle"></span>						</td>
                        </tr>
						 <tr><td colspan="3" height="25"></td></tr>
                        <tr>
                          <td align="center" valign="middle" class="text">&nbsp;</td>
                          <td width="39%" align="left" valign="middle" class="text">Image 3:</td>
							  <td width="59%" bgcolor="#f2f2f2"><input type="file" name="image3" size="32">
						<br>
						<?
						 if($image1==1)
						 {
						   
						       echo'<span class="error" id="lblimage3">Please upload valid image file [jpeg,gif,png].</span>';
						
						}
						else
						{
								echo'<span class="error" id="lblimage3"></span>';
						}
						?>						  </td>
                        </tr>
						<tr>
                          <td align="center" valign="middle" class="text">&nbsp;</td>
                          <td width="39%" align="left" valign="middle" class="text">Title 3:</td>
							  <td width="59%" bgcolor="#f2f2f2"><input type="text" name="title3" class="textfieldbig">
							  <br>
						<span class="error" id="lbltitle"></span>						</td>
                        </tr>
						 <tr><td colspan="3" height="25"></td></tr>
                        
                       
                        <tr>
                          <td colspan="2">&nbsp;</td>
                          <td><input name="Submit" type="Submit" class="buttongrey" value="Add" />                          </td>
                        </tr>
                      </table>
                    </div></td>
                  </tr>
				  </form>
                </table></td>
                <td width="10">&nbsp;</td>
              </tr>
            </table></td>
            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>
          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>
          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <? include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
