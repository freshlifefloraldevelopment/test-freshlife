<?

    $page_id=3;  

    include("config/config.php");

	$sel_page="select * from page_mgmt where page_id=2";

	$rs_page=mysql_query($sel_page);

	$page=mysql_fetch_array($rs_page);

	$pageid=2;

?>

<!DOCTYPE html>

<html lang="en">

<?php include("include/head.php"); ?>

<body>

<!-- main wrapper starts -->

<div class="wrapper">

  <!-- header starts -->

 <?php include("include/header.php"); ?>

  <!-- header ends -->

  <div class="cl"></div>

  <!-- About us banner starts -->

  <div class="aboutus-banner">

    <div class="banner-about">

      <div class="ban-image"><img src="<?=$page["image_path"]?>jpg" width="485" height="267" alt=""> </div>

      <div class="ban-content">

        <h3><?=$page["banner_title"]?></h3>

        <p><?=$page["banner_description"]?></p>

      </div>

    </div>

  </div>

  <!-- body starts -->

  <div class="cl"></div>

  <!-- inner-content starts -->

  <div class="content-container inner-content">

    <div class="brad"><a href="index.php" class="no-active">Home</a> / <a href="our-flowers.php">Our Flowers</a></div>

    <div class="cl"></div>

    <!-- Left Starts -->

    <div class="left-container">

      <div class="top"></div>

      <div class="left-mid">

        <div class="content_area">

          <h2><?=$page["page_title"]?></h2>

          <?=$page["page_desc"]?>

          <?php

		     $i=1;

		      $sel_cat="select * from category order by id";

			  $rs_cat=mysql_query($sel_cat);

			  while($cat=mysql_fetch_array($rs_cat))

			  {
                    $pname = preg_replace("![^a-z0-9]+!i", "-", $cat["name"]);
		 ?>	  

          <div class="flower_details">

            <div class="flower-pic <?php if($i%2==0) { echo'fr'; } else { echo'fl'; } ?>"><a href="<?=$siteurl?><?=trim(strtolower($pname))?>/<?=$cat["id"]?>"><img src="<?=$cat["cat_image"]?>" alt="" width="147" height="75" border="0"></a></div>

            <p style="margin-bottom:5px;"><strong><a href="<?=$siteurl?><?=trim(strtolower($pname))?>/<?=$cat["id"]?>"><?=$cat["name"]?></a></strong></p>

            <p <?php if($i%2==0) { echo 'style="width:443px;margin:0px;"'; } else {  echo 'style="margin:0px;"'; }?> ><?=$cat["cat_desc"]?></p>

          </div>

          <div class="cl"></div>

		  <?

		     $i=$i+1;

		     }

		  ?>

           <p><strong>Yes, we have more!</strong><br>

           We have other Ecuadorian blooms that you may find interesting.  Fresh, stunning, and absolutely beautiful, we also have other rare and hard to find flowers which are not mentioned here.  For a more complete list of our products, please don’t hesitate to <a href="contact-us.php">contact us</a>. </p>

      </div>

        </div>

        

       

      <div class="bot"></div>

      <div class="cl"></div>

    </div>

    <!-- Left ends -->

    <!-- Right Starts -->

    <?php include("include/right.php"); ?>

    <!-- Right Ends -->

    <div class="cl"></div>

  </div>

  <!-- inner-content ends -->

  <!-- footer starts -->

  <div class="footerwraper">

  <?php include("include/footer.php"); ?>

  </div>

  <!-- footer ends -->

   <?php include("include/copyright.php"); ?>

  <div class="cl"></div>

  <!-- body ends -->

</div>

<!-- main wrapper -->

<!-- Fixed-div starts -->

<?php include("include/fixeddiv.php"); ?>

<!-- Fixed-div ends -->

</body>

</html>
