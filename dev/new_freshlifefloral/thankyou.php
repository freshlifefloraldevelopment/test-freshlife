<?
    include("config/config.php");
?>
<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
<body>
<!-- main wrapper starts -->
<div class="wrapper">
  <!-- header starts -->
 <?php include("include/header.php"); ?>
  <!-- header ends -->
  <div class="cl"></div>
  <!-- About us banner starts -->
  <div class="aboutus-banner">
    <div class="banner-about">
      <div class="ban-image"><img src="images/abt-ban-img.jpg" width="485" height="267" alt=""> </div>
      <div class="ban-content">
        <h3>Exporting high quality Ecuadorian flowers</h3>
        <p>Ecuador is also home to some of the world’s finest and talented floral growers, creating the finest cut flowers and lush Ecuadorian plants. </p>
      </div>
    </div>
  </div>
  <!-- body starts -->
  <div class="cl"></div>
  <!-- inner-content starts -->
  <div class="content-container inner-content">
    <div class="brad"><a href="index.php" class="no-active">Home</a> / <a href="thankyou.php">Thank You</a></div>
    <div class="cl"></div>
    <!-- Left Starts -->
    <div class="left-container">
      <div class="top"></div>
      <div class="left-mid">
        <div class="content_area">
          <h2>Thank You</h2>
         <?php if($_GET["newsletter"]==1)
         {
         ?>
          <div class="success">Your Form successfully Submitted. Please check for confirmation email. </div>

          <?php
            }
            else
            {
          ?>
 
               <div class="success">Your Form successfully Submitted.</div>

          <?php
             }
          ?>
          
			<div class="cont-mail">          
           Email us at <a href="mailto:info@freshlifefloral.com">info@freshlifefloral.com</a>
       		</div>
        </div>
        <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <!-- Left ends -->
    <!-- Right Starts -->
    <?php include("include/right.php"); ?>
    <!-- Right Ends -->
    <div class="cl"></div>
  </div>
  <!-- inner-content ends -->
  <!-- footer starts -->
  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <!-- footer ends -->
   <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
  <!-- body ends -->
</div>
<!-- main wrapper -->
<!-- Fixed-div starts -->
<?php include("include/fixeddiv.php"); ?>
<!-- Fixed-div ends -->
</body>
</html>
