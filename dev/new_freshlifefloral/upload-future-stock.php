<?php
    $menuoff=1;
	$page_id=475;  
    include("config/config.php");	
	
	if($_SESSION["login"]!=1 && $_SESSION["grower"]=="" )
	{
	   header("location:".$siteurl);
	}
	
	$today=date("Y-m-d");
	//$today=date("2014-01-13");
		
	function getWeekday($date) {
    return date('w', strtotime($date));
    }
	
    $day_of_week=getWeekday($today); // returns 4
	
	if($day_of_week>=0 && $day_of_week<=3 )
	{
		if($day_of_week==0)
		{
			$starting_date=date('Y-m-d', strtotime($today. ' +15 day'));
		}
		else if($day_of_week==1)
		{
			$starting_date=date('Y-m-d', strtotime($today. '+14 day'));
		}
		else if($day_of_week==2)
		{
			$starting_date=date('Y-m-d', strtotime($today. '+13 day'));
		}
		else if($day_of_week==3)
		{
			$starting_date=date('Y-m-d', strtotime($today. '+12 day'));
		}
		
		$end_date=date('Y-m-d', strtotime($starting_date. '+5 day'));
	}
	
	if($starting_date!="" && $end_date!="")
	{
		   function week_number($date) 
           { 
                return ceil( date( 'j', strtotime( $date ) ) / 7 ); 
           } 
			
			$week_no = week_number($starting_date);
			
			if($week_no==1)
			{
				$option_update=1;
			}
			else if($week_no==2)
			{
				$option_update=2;
			}
			else if($week_no==3)
			{
				$option_update=1;
			}
			else if($week_no==4)
			{
				$option_update=4;
			}
			else if($week_no==5)
			{
				$option_update=5;
			}
			
			$temp_starting_date=explode("-",$starting_date);
			$orginal_starting_date=$temp_starting_date[1]."-".$temp_starting_date[2]."-".$temp_starting_date[0];
			
			$temp_end_date=explode("-",$end_date);
			$orginal_end_date=$temp_end_date[1]."-".$temp_end_date[2]."-".$temp_end_date[0];
			
    }
	
	if(isset($_POST["submit"]))
	{
           
		     $today = date('mdyHis');
			 $tmp1 = $_FILES['stockfile']['name'];
			 $ext1 = explode('.',$tmp1);
			 $image=0;
			 $uploaddir = 'csvfiles/';
			 $uploadfile1 = $uploaddir.$today."big.".$ext1[1];
			 move_uploaded_file($_FILES['stockfile']['tmp_name'],$uploadfile1);
			
			 $csv_file=$uploadfile1;
			 
			 if (($getfile = fopen($csv_file, "r")) !== FALSE) 
			 { 
				 $data = fgetcsv($getfile, 5000000, ",");
				 while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) 
				 {
					 $num = count($data); 
					 for ($c=0; $c < $num; $c++) 
					 {
						 $result = $data; 
						 $str = implode('"', $result); 
						 $slice = explode('"', $str);
						
					 }
					    
						if($slice[0]>1)
						{
							
							if($option_update==1)
							{
							$update="update grower_product_box_packing set future_stock='".$slice[8]."',future_price='".$slice[7]."',future_comment='".$slice[6]."' where id='".$slice[0]."'";			    
						  
							}
							
							else if($option_update==2)
							{
							$update="update grower_product_box_packing set future_stock2='".$slice[8]."',future_price2='".$slice[7]."',future_comment2='".$slice[6]."' where id='".$slice[0]."'";			    
						  
							}
							
							else if($option_update==3)
							{
							$update="update grower_product_box_packing set future_stock3='".$slice[8]."',future_price3='".$slice[7]."',future_comment3='".$slice[6]."' where id='".$slice[0]."'";			    
						    
							}
							
							else if($option_update==4)
							{
							$update="update grower_product_box_packing set future_stock4='".$slice[8]."',future_price4='".$slice[7]."',future_comment4='".$slice[6]."' where id='".$slice[0]."'";			    
						   
							}
							
							else if($option_update==5)
							{
							$update="update grower_product_box_packing set future_stock5='".$slice[8]."',future_price5='".$slice[7]."',future_comment5='".$slice[6]."' where id='".$slice[0]."'";			    
						    
							}
							
							mysql_query($update);
							
						}
				 }
				 
				 header("location:http://freshlifefloral.com/vendor/update-future-stock?msg=1");
		     }
	}
		
?>

<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
 <style>
 .inner-content
 {
    margin-top:30px !important;
 }
 .brad
 {
	margin-bottom:0px;
	text-align:right;
	margin-right:269px;
  
 }
 .left-container .left-mid
 {
   margin-top:-25px;
 }
 </style>

<body>
<div class="wrapper">
<?php include("include/header.php"); ?>
<script type="text/javascript">
 function verify()
	{ 
		var arrTmp=new Array();
		arrTmp[5]=checkimage();
		var i;
		_blk=true;
		for(i=0;i<arrTmp.length;i++)
		{
			if(arrTmp[i]==false)
			{
			   _blk=false;
			}
		}

		if(_blk==true)
		{
			return true;
		}

		else
		{
			return false;
		}
 	}	

	function trim(str) 
	{    

		if (str != null) 

		{        

			var i;        

			for (i=0; i<str.length; i++) 

			{           

				if (str.charAt(i)!=" ") 

				{               

					str=str.substring(i,str.length);                 

					break;            

				}        

			}            

			for (i=str.length-1; i>=0; i--)

			{            

				if (str.charAt(i)!=" ") 

				{                

					str=str.substring(0,i+1);                

					break;            

				}         

			}                 

			if (str.charAt(0)==" ") 

			{            

				return "";         

			} 

			else 

			{            

				return str;         

			}    

		}

	}
	
	function checkimage()
	{

		if(trim(document.frmupload.stockfile.value) == "")
		{	 
			document.getElementById("lblstockfile").innerHTML="Please upload .csv file";
			return false;
		}
		else 
		{

			if(!validImageFile(document.frmupload.stockfile.value))
			{
				document.getElementById("lblstockfile").innerHTML="Please select valid file (.csv)";
				return false;
			}

			else
			{
				document.getElementById("lblstockfile").innerHTML="";
				return true;
			}
		}
	}
	
	function validImageFile(strfile)
	{

		var str = strfile;

		var pathLenth = strfile.length;

		var start = (str.lastIndexOf("."));

		var fileType = str.slice(start,pathLenth);

		fileType = fileType.toLowerCase();

		if (strfile.length > 0)
		{

		   if((fileType == ".csv")) 
		   {
				return true;

		   }

		   else 
		   {
				return false;
		   } 
		}
	}	
</script>	
  <div class="cl"></div>
  <div class="cl"></div>
  <div class="content-container inner-content">
       <div class="left-container" style="width:990px;">
      <div class="left-mid" style="width:990px; background:none; ">
        <div class="content_area" style="width:990px;">
          <h2>Update future stock for <?=$orginal_starting_date?> to <?=$orginal_end_date?> </h2>
          <div style="clear:both"></div>
          <div class="contact-form">
          <form action="" method="post" name="frmupload" id="frmupload" onsubmit="return verify();" enctype="multipart/form-data" >
             <div class="form-row">
        <label style="width:400px;">Upload your future inventory (.csv file) from <?=$orginal_starting_date?> to <?=$orginal_end_date?> : <span>*</span></label>
        <div class="right-form"><div class="">
        <input type="file" name="stockfile" id="stockfile" />
        <br/><span id="lblstockfile" style="color:red; font-size:12px; font-weight:bold;"></span>
        
        
        </div></div>
        <div class="cl"></div>
        </div>
        
        <div class="form-row">
        <label>&nbsp; </label><div class="cl"></div><input type="submit" name="submit" class="submit" value="" /><div class="cl"></div></div>
        </form>
          </div>
      </div>
       <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <div class="cl"></div>
  </div>
  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
</div>
<?php include("include/fixeddiv.php"); ?>
</body>
</html>
<script tpe="text/javascript">
	function funPage(pageno)
	{
		document.frmfprd.startrow.value=pageno;
		document.frmfprd.submit();
	}
	function docolorchange()
	{
	   window.location.href='<?=$siteurl?>buy.php?id=<?=$_GET["id"]?>&categories=<?=$strdelete?>&l=<?=$_GET["l"]?>&c='+$('#color').val();
	}
	
	function frmsubmite()
	{
	  document.frmfilter.submit();
    }
	
	function doadd(id)
	{
	   var check = 0 ;
	   if($('#qty-'+id).val()=="" )
	   {
	       $('#ermsg-'+id).html("please enter box qty.")
		   check = 1 ;
	   }
	   if(check==0)
	   {
	     $('#ermsg-'+id).html("");
		 $('#gid6').val(id);
		 $('#frmrequest').submit();
	   }	 
	}
</script>
