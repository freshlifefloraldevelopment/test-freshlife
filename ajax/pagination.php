<?php
require_once("../config/config_gcp.php");

class pagination
{
    var $limit = 10;
    var $adjacentLinks = 2;
    
    function paginationVarietySubPageGrowers($currentPage, $limit, $p_id)
    {
        global $con;
        
        if(empty($limit) )
            $limit = $this->limit;
//        echo "<font color=red>LIMIT:$limit :::\$currentPage:$currentPage</font><br>";
        $page = $currentPage;
        if ($page == 1) {
            $start = 0;
        } else {
            $start = ($page - 1) * $limit;
        } 
        
        /*$growersSql = "select g.id,g.growers_name,g.page_desc,g.certificates,g.file_path5,g.short_desc,g.active as growers_status,g.country_id 
              from growers as g where g.active IN ('active','advertising') ORDER BY g.growers_name";# WHERE active='active'*/
        
        $growersSql = "select grower_product.product_id, growers.* 
                         from grower_product 
                         left join growers on growers.id = grower_product.grower_id 
                        where growers.active IN ('active','advertising') 
                          and grower_product.product_id = ".$p_id." 
                        ORDER BY growers.growers_name";
        
        
        ## WHERE active='active'
        //echo $growersSql;
        
        
        $query = mysqli_query($con, $growersSql);
        $totalResultsCount = $query->num_rows;
        
        
        //echo $totalResultsCount;
        
        /*$growersSql = "select g.id,g.growers_name,g.page_desc,g.certificates,g.file_path5,g.short_desc,g.active as growers_status,g.country_id 
              from growers as g where g.active IN ('active','advertising') ORDER BY g.growers_name LIMIT $start,$limit";# WHERE active='active'*/
        
        
        $growersSql = "select grower_product.product_id, growers.* 
                         from grower_product left join growers on growers.id = grower_product.grower_id 
                        where growers.active IN ('active','advertising') 
                          and grower_product.product_id = ".$p_id." 
                        ORDER BY growers.growers_name LIMIT $start,$limit";     
        
        ## WHERE active='active' 
        #
        #
            //echo $growersSql;
        
        
        $growersResult = mysqli_query($con, $growersSql);
        
        while ($growersRow = mysqli_fetch_assoc($growersResult)) {
            $growers_status=$growersRow['growers_status'];
                                
            $sel_country="SELECT * from country where id='".$growersRow['country_id']."'";
            //echo $sel_country;
            $rs_country=mysqli_query($con,$sel_country);
            $row_Country=mysqli_fetch_array($rs_country);
        ?>
            <div class="row"><!-- item -->
                <div class="col-md-2"><!-- company logo -->
                    <img src="<?php echo SITE_URL . "user/" . $growersRow['file_path5']; ?>" class="img-responsive" alt="company logo">
                </div>
                <div class="col-md-10"><!-- company detail -->
                    <h4 class="margin-bottom-10"><?php echo $growersRow["growers_name"]; ?></h4>
                    <ul class="list-inline">
                     <?php 
                        if($row_Country['name'] != "")
                        {
                            ?>
                            <li><!-- i class="fa fa-map-marker color-green"></i> -->
                                <img src="<?php echo SITE_URL;?>includes/assets/images/flags/<?php echo $row_Country['flag']?>" />
                                <?php 
                            echo $row_Country['name'];
                            ?>
                            </li>
                            <?php 
                        }
                        
                        ?>
                    
                        <?php 
                        if($growers_status != "")
                        {?>
                            <li>
                           <?php 
                            if($growers_status == "active")
                            { 
                                echo '<i class="fa fa-info-circle color-green"></i>';
                                echo "Premium Member";
                            }
                            /*else if($growers_status == "advertising")
                            {
                                echo '<i class="fa fa-info-circle color-green"></i>';
                                echo "Regular Member";
                            }*/
                            $review_p="select * from review_rating where grower_id='".$growersRow['id']."'";
                            //echo $review_p;
                            $rs_review_p=mysqli_query($con,$review_p);
                            $total_rating=0;
                            $cnt_row=mysqli_num_rows($rs_review_p);
                            if($cnt_row > 0)
                            {
                                while($review_detail=mysqli_fetch_array($rs_review_p))
                                {
                                    $total_rating+=$review_detail['final_rating'];
                                }   
                            }
                            $f_rating=0;
                            if($total_rating != 0)
                            {
                                $f_rating=($total_rating*5/($cnt_row*5));
                            }   

                            ?>  
                                &nbsp;&nbsp;&nbsp;
                                <span class="size-14 text-muted"><!-- stars -->
                                    <?php
                                    for($i=1;$i<=5;$i++)
                                    {
                                        if($i <= round($f_rating))
                                        { ?>
                                            <i class="fa fa-star"></i>
                                        <?php }
                                        else{
                                            ?>
                                            <i class="fa fa-star-o"></i>
                                        <?php }   
                                    }
                                    ?>
                                 </span>    
                            </li>
                            <?php 
                        }
                        ?>
                   
                </ul>
                    <p><?php echo $growersRow["short_desc"]; ?></p>
                </div>
            </div><!-- /item -->
            <hr>
        <?php 
        }
        
        $this->generatePaginationLinks($limit, $this->adjacentLinks, $totalResultsCount, $page);
    }
    
    /**
     * GENERATES PAGINATION LINKS
     * @param type $limit
     * @param type $adjacentLinks number of links to be displayed
     * @param type $totalResults TOTAL RESULT COUNT
     * @param int $page CURRENT PAGE
     */ 
    private function generatePaginationLinks($limit, $adjacentLinks, $totalResultsCount, $page) 
    {        
        #echo "<font color=red>LIMIT:$limit :::\$adjacentLinks:$adjacentLinks :::\$totalResultsCount:$totalResultsCount :::\$page:$page</font><br>";
        $pagination = '';
        if ($page == 0)
            $page = 1;     //if no page var is given, default to 1.
        $prev = $page - 1;       //previous page is page - 1
        $next = $page + 1;       //next page is page + 1
        $prev_ = '';
        $first = '';
        $lastpage = ceil($totalResultsCount / $limit);
        $next_ = '';
        $last = '';

        if ($lastpage > 1) {

            //previous button
            if ($page > 1)
                $prev_.= "<li><a class='page-numbers' href='?page=$prev' >prev</a></li>";
            else {
                //$pagination.= "<span class=\"disabled\">previous</span>";	
            }

            //pages	
            if ($lastpage < 5 + ($adjacentLinks * 2)) { //not enough pages to bother breaking it up
                $first = '';
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><span class='page-numbers' >$counter</span></li>";
                    else
                        $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                }
                $last = '';
            }
            elseif ($lastpage > 3 + ($adjacentLinks * 2)) { //enough pages to hide some
                //close to beginning; only hide later pages
                $first = '';
                if ($page < 1 + ($adjacentLinks * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacentLinks * 2); $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span>$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last.= "<li><a class='page-numbers' href='?page=$lastpage' >Last</a></li>";
                }

                //in middle; hide some front and some back
                elseif ($lastpage - ($adjacentLinks * 2) > $page && $page > ($adjacentLinks * 2)) {
                    $first.= "<li><a class='page-numbers' href='?page=1' >First</a></li>";
                    for ($counter = $page - $adjacentLinks; $counter <= $page + $adjacentLinks; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span>$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counte' >$counter</a></li>";
                    }
                    $last.= "<li><a class='page-numbers' href='?page=$lastpage' >Last</a></li>";
                }
                //close to end; only hide early pages
                else {
                    $first.= "<li><a class='page-numbers' href='?page=1' >First</a></li>";
                    for ($counter = $lastpage - (2 + ($adjacentLinks * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span class='' >$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last = '';
                }
            }
            if ($page < $counter - 1)
                $next_.= "<li><a class='page-numbers' href='?page=$next'>next</a></li>";
            else {
                //$pagination.= "<span class=\"disabled\">next</span>";
            }
            $pagination = "<ul class='pagination pagination-simple pull-right'>" . $first . $prev_ . $pagination . $next_ . $last . "</ul>";
        }   

        echo $pagination;
    }
    
}

if($_POST['action'] == 'varietySubPageGrowersPagination')
{
    $currentPage = $_POST['page'];
    $product_id = $_POST['pid'];
    $limit = 5;

    $pagination = new pagination();
    $pagination->paginationVarietySubPageGrowers($currentPage, $limit, $product_id);
}

?>
