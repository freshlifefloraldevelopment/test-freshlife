<!DOCTYPE html>
<html lang="en">
<head>
<meta name="description" content=" Learn the septic tank do's and don'ts to have a healthy septic system and protect your family from diseases.">        
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">Septic Tank Do's and Don'ts</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-2.gif);"></div>        
  </div>
</header>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h2><a aria-hidden="true"><span class="icon icon-link"></span></a>Septic Tank Do's and Don'ts</h2>          
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Septic Tank Do's</h3>                
            <p class="text-justify">1.	Educate yourself on your septic system, its design, its components, how do septic tanks work, and the proper septic tank maintenance that will prevent problems in the future. </p>        
            <p class="text-justify">2.	Educate yourself on recognizing the signs of any problems with your septic system and how to troubleshoot those problems. Then you will know when to call for assistance and what to expect from your septic tank service provider. </p>                       
            <p class="text-justify">3.	Make sure you arrange for septic tank pumping approximately every 3-5 years, depending on the usage, size of the tank, number of people in the home, also taking into consideration the age and type of septic system you have. </p>            
            <p class="text-justify">4.	Keep records concerning your septic system. </p>             
            <p class="text-justify">5.	Educate yourself on your city or county's requirements. Keep a record of septic tank repairs made to your septic system so that you will have documentation regarding the operation and the septic system maintenance. </p>                    
            <p class="text-justify">6.	Fix and repair all leaking faucets, plumbing fixtures, or running toilets immediately no matter how small the leak. </p>        
            <p class="text-justify">7.	Use washing machines, dishwashers and other water-consuming devices sparingly and evenly over 7 days. Try to limit the number of washloads to 2 or less per day, one in the morning and one in the evening. </p>                    
            
            <!-- Pagina 2  --->
            <p class="text-justify">8.	Place a septic tank filter inside your septic tank. </p>        
            <p class="text-justify">9.	Divert all gutter drains and rainwater away from your septic tanks and leach fields. </p>                       
            <p class="text-justify">10.	Use drier sheets instead of liquid softeners. Liquid softeners add grease and soap residue to your septic system. </p>            
            <p class="text-justify">11.	Use liquid detergents instead of powder detergents. Powder detergents residue can clog the pours of your leach field.</p>             
            <p class="text-justify">12.	Only put septic-friendly material down your drains. Materials that are marked "Septic Safe" do not mean that they should go into the septic system, some things are very hard to break down and for your system to digest and are better suited for the garbage can. </p>                    
            <p class="text-justify">13.	Mow the grass over the leaching field regularly and keep all growth from over or on the field. </p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Septic Tank Pumping is required if:</h3>                
            <p class="text-justify">1.	The top of the sludge deposit is within 12 inches of the septic tank outlet baffle.</p>        
            <p class="text-justify">2.	The bottom of the floating scum mat is within three inches of the bottom of the septic tank outlet baffle.</p>                       
            <!-- Pagina 3  --->                        
            <p class="text-justify">3.	The top of the floating scum mat is within one inch of the top of the outlet baffle.</p>        
            <p class="text-justify">4.	The floating scum mat is more than 10-12 inches thick.</p>                       
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Septic Tank Don'ts</h3>                
            <p class="text-justify">1.	Allow anyone to park or drive over your septic system.</p>        
            <p class="text-justify">2.	Dig into your leach field or build anything on top of it. </p>        
            <p class="text-justify">3.	Make or allow septic tank maintenance without first contacting your local health department and a licensed technician. </p>        
            <p class="text-justify">4.	Use septic tank additives or enzymes of any kind. You are wasting your money on additives, enzymes or other items that they try to sell you on TV, the Internet or some unscrupulous service providers - they are not necessary, they do not necessarily hurt your septic system, but they are not needed for your system to function correctly - if you are having issues all the "product" in the world is not going to fix it!</p>        
            <p class="text-justify">5.	We cannot stress this enough! Use garbage disposal. Do not put table scraps down the kitchen sink - the use of the garbage disposal is like adding 1 or more persons to the household - raw garbage is very hard to dispose of! </p>        
            <p class="text-justify">6.	Use those very nice, plush, ever-so-soft, toilet tissues - the thicker the toilet tissue the harder it is to dispose of - they also tend to collect in the baffles and tees and cause sewage backups in the main inlet line. </p>        
            <!-- Pagina 4  --->                                    
            <p class="text-justify">7.	Dump non-biodegradable items such as grease, disposable diapers, sanitary pads, condoms, plastics, cigarette butts, Kleenex, feminine products, baby wipes, cleaner cloths, toilet cleaner heads, paper towels, etc. down the toilet - these items DO NOT BREAK DOWN EASILY and can cause congestion in your septic system. </p>        
            <p class="text-justify">8.	Avoid chemicals such as paint thinner, pesticides, prescription drugs, an excessive number of cleaners, detergents or other hazardous substances into your septic system.</p>        
            <p class="text-justify">9.	Build a swimming pool near your septic system.</p>        
            <p class="text-justify">10.	Plant trees, large shrubs, gardens, etc. near or over your septic system.</p>        

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>HARMFUL MATERIALS</h3>                
            <p class="text-justify">1.	Fats </p>                    
            <p class="text-justify">2.	Solvents </p>                    
            <p class="text-justify">3.	Oils</p>                    
            <p class="text-justify">4.	Disinfectants </p>  
            
            <!-- Pagina 5  --->                                                
            <p class="text-justify">5.	Paints</p>                    
            <p class="text-justify">6.	Chemicals</p>                    
            <p class="text-justify">7.	Pesticides</p>                    
            <p class="text-justify">8.	Poisons</p>  
            <p class="text-justify">9.	Coffee grounds</p>                    
            <p class="text-justify">10.	Paper towels</p>                    
            <p class="text-justify">11.	Disposable Diapers</p>                    
            <p class="text-justify">12.	Sanitary napkins</p>  
            <p class="text-justify">13.	Tampons</p>              
            
     
     
      </div>
    </div>
  </div>


<?php 
include "footer.php";
?>
</body>

</html>


