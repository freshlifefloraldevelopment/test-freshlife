<!DOCTYPE html>
<html lang="en">
<head>
<meta name="description" content=" What is a septic tank is not only about the septic tank, rather than the whole septic system including the leach field.">    
<?php
$title="Service - Septic Works LLC.";
include "head.php";
head($title);
?>

</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">What is a Septic Tank?</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-2.gif);"></div>        
  </div>
</header>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What is a Septic Tank?</h3>    
        
        
      </div>
    </div>
  </div>            
 
<div class="block-1">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">        
            <div class="col-lg-8 mb-4 mbl-md-0">
                  <div class="block-12-card-1__content w-100">
                    <div><img class="img-fluid w-100" src="../page-septic/img/dostank-2.jpg" alt="Grafico" ></div>
                    <p class="text-justify">What is a septic tank?</p>
                  </div>
            </div>               
    </div>
  </div>
</div>    
        
        
 <div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        

        
            <p class="text-justify">What is a septic tank? It is a container that is buried underground. It is water-tight and made of concrete, fiberglass, or polyethylene.</p>        
            <p class="text-justify">The job of a septic tank is to hold the wastewater that exits your home and separate the waste from the water by making use of natural processes and proven technology.</p>                

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What is a septic tank: A bit of history</h3>                
            <p class="text-justify">Way back when tanks were homemade, they came in a variety of sizes and materials. In the 40s & 50s, they started building the concrete box, a single compartment, but it did standardize the tank a bit. In the late 70s, they came to adopt the standard size of 1000 gallons and put in a wall and divided the tank for better settling. </p>        

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What is a septic tank: Size requirements</h3>                
            <p class="text-justify">Most counties in Alabama determine the needed size of the septic tank by the number of bedrooms (supposed to equate back to the average number of people in the home). </p>        
            <p class="text-justify">Today's standard is 3 bedrooms require a minimum tank size of 1000, 4 bedrooms a 1250 and 5 or more would require a 1500-gallon capacity tank. Most builders will install a 1250 tank even if the home is only 3 bedrooms.</p>                

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What is a septic tank: The treatment area</h3>                
            <p class="text-justify">This gives the owner a little wiggle room for expansion and the addition of a bedroom in the basement. But, the true size of the septic system is not the size of the septic tank, but the size of the soil treatment area, or leach field. </p>        
            <p class="text-justify">Exceptions occur when there is a specific need for a larger tank, more compartments, more treatment is needed or a larger soil treatment area. This is usually determined by the engineer after soil tests are done and percolation rates are calculated, lot size is evaluated, groundwater location, etc. </p>                
            
            <p class="text-justify">If anything falls into the exception category then the engineer designs a septic system that will function within the prohibiting conditions. These systems usually require more attention to maintenance than the standard gravity-fed system.</p>        
            <p class="text-justify">The outlet lines also need to be clear and properly connected. Again, today new homes use PVC pipes. If your home is older, then you may consider replacing any of the old types of pipes, or you may have yourself many headaches. Now onto the absorption field.</p>                
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What is a septic tank: The leach field</h3>                
            <p class="text-justify">What is a septic tank is not only about the septic tank rather than the whole septic system. With a basic gravity-fed septic system, there are primarily two types of fields:</p>        
            
            <ul><li>A gravel bed, which consists of an area that has been excavated or dugout, gravel laid down, perforated pipes laid on top of the gravel and recovered with soil. The effluent water enters the bed at one end and gravity allows it to flow to the other end seeping out of the holes and into the ground. </li></ul>
            <ul><li>There is also another type of system that consists of trenches and dome-shaped plastic chambers. This system works in much the same way as a gravity gravel bed but is much easier to install and less costly. If the terrain, size of the lot or percolation rates dictate an absorption field, and possibly the whole system, may have to be designed by an engineer.</li></ul>                        

            <p class="text-justify">These would be the exception systems discussed above and could have pumps, sprayers, filtration beds, jets, dousing systems, etc. Again, if any of these components are used, it is even more critical that the homeowner maintain the septic system and arrange for regular septic tank pumping and inspection.</p>                    
            <p class="text-justify">So, what is a septic tank is not only about the septic tank, it is the whole septic system including the soil and the leach field as well.</p>                    
      </div>
    </div>
  </div>            
            

            


<?php 
include "footer.php";
?>
</body>
</html>


