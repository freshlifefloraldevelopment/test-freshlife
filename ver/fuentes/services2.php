<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>

</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">Septic Tank Pumping</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/blog-header-2.gif);"></div>        
  </div>
</header>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Septic Tank Pumping</h3>                
            <p class="text-justify">Septic tank pumping is the process of removing the solid waste (sludge) from the bottom of the septic tank. This can only be done by a certified and licensed septic tank cleaner, and before the sludge level rises too much and starts to leak into the leach field or back up into the house.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Who Pumps Septic Tanks Near Me?</h3>                
            <p class="text-justify">At Septic Works LLC, we're the ones who pump septic tanks near you (me) anywhere in the US. Our septic tank services include septic tank inspection, septic tank repair, septic tank pumping, septic tank installation, engineered septic system, alternative septic systems, and porta potti rentals. We serve residential, municipal and commercial clients across the United States.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How to Pump a Septic Tank?</h3>                
            <p class="text-justify">How to pump a septic tank is not a complicated job. You schedule an appointment with our septic tank pumping company and we send a septic tank cleaner to do the job.</p>        
            <p class="text-justify">He/she comes with a septic tanker truck, vacuum equipment and suction gear. The septic tank cleaner will then open the cover of the manhole and insert a large hose into the septic tank.</p>        
            <p class="text-justify">After that is done, the suction gear will start to suck out the content of the septic tank while the septic cleaner will stir the sludge layer (solids that have settled to the bottom of the tank) with a rake to break it up.</p>                    
          
        <!-- Page 2-->
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>When to Pump a Septic Tank</h3>                
            <p class="text-justify">According to the United States Environmental Protection Agency EPA, septic tank pumping should typically happen every three to five years.</p>        
            <p class="text-justify">A septic tank cleaner will normally start by emptying the septic tank � removing the solid waste that has settled to the bottom. He/she will use the suction gear to collect the waste from the tank and dump it in the septic tanker truck.</p>        
            <p class="text-justify">That being said, the best time to carry out a septic tank pumping is in the summer, the spring, or the warmer months. That is because the soil around the drain line might be frozen and hard in the cold months, so it is not easy to find the way to the septic tank cover.</p>                    
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How Do I Prepare My Septic Tank for Pumping?</h3>                
            <p class="text-justify">It is recommended that you arrange for a septic tank pumping once every three years. However, this is not a concrete rule. It depends on the amount of waste your house generates, the size of the septic tank and how old your septic system is. </p>        
            <p class="text-justify">It is important to ensure that it is the due time to pump out the septic tank. You don't want to waste your money when the waste layer is less than one-third of the septic tank capacity.</p>        
            <p class="text-justify">You can prepare for your septic tank pumping by:</p>   
                <ul><li>Having the maintenance record in handy;</li></ul>
                <ul><li>Clean the area around the septic tank;</li></ul>
                <ul><li>Having the septic permit in handy</li></ul>                        
            <p class="text-justify">The septic permit has a drawing of the septic system of the property. This will provide you with a detailed description of the septic system components (e.g., size of the septic tank or drain field) and how many bedrooms was the septic system designed for � the design capacity.</p>           
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What to Do After a Septic Tank is Pumped?</h3>                
            <p class="text-justify">What to do after a septic tank is pumped is completely your responsibility. Here is a list of what to do after a septic tank is pumped: </p>        
                <ul><li>Arrange for a septic tank inspection once every three years;</li></ul>
                <ul><li>Arrange for a septic tank pumping every three to five years; </li></ul>
                <ul><li>Keep maintenance records; </li></ul>                                    
                
                <ul><li>Keep track of the scum and sludge levels (According to the United States Environmental Protection Agency EPA, you need to pump out the septic tank if the bottom of the scum layer is within six inches of the bottom of the outlet, or if the top of the sludge layer is within 12 inches of the outlet);</ul>
                <ul><li>Only flush things that are septic-friendly;</li></ul>
                <ul><li>Never flush down coffee grounds, diapers, cooking oil, cigarettes, feminine products, cat litter, or chemicals. They could clog the septic system;</li></ul>                                    
                
                <ul><li>Avoid using additives. They kill the bacteria that break down the wastewater;</li></ul>
                <ul><li>Use low-water consuming toilets and showerheads. That is because too much water flushes out the tank quickly;</li></ul>
                <ul><li>Don't overload your septic tank (expanding the house and overwhelming the septic system);</li></ul>                                    

                <ul><li>Don't park or drive over the drain line. This can damage the pipes;</li></ul>
                <ul><li>Grow plants or trees far from the leach field. Their roots can do damage;</li></ul>
                <ul><li>Hire a septic inspector to come to your house and check for any problems or defects that need correction. According to the EPA, average household septic systems should be inspected at least once every three years by a professional septic inspector.</li></ul>                                    

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How Long Does It Take to Do a Septic Tank Pumping?</h3>                
            <p class="text-justify">A septic tank pumping duration depends on the size of the septic tank and the pump capacity. It can take as little as 20-30 minutes for a tank size of 1000-1250 gallons. Or one hour or more for a septic tank size of 1500-2000 gallons.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Should a Septic Tank be Pumped Before Inspection?</h3>                
            <p class="text-justify">During our septic inspection, we will learn whether or not your septic tank needs to be pumped. However, pumping a septic tank before a septic inspection means that the drain field cannot be tested. This action may hide any issues the septic system may have.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What Happens if You Never Pump Your Septic Tank?</h3>                
            <p class="text-justify">What happens if you never pump your septic tank is that the solid waste or sludge keeps building up until it starts to exit the outlet baffle into the drain field. Or the water leaves the septic tank lid.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How Much Does It Cost to Pump a Septic Tank? </h3>                
            <p class="text-justify">The cost of septic tank pumping starts at USD350 depending on the size of your septic tank and the location of the landfill. A landfill just outside town costs less to drive to than one that is 50 miles away.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How Often to Pump a Septic Tank?</h3>                
            <p class="text-justify">How often to pump a septic tank depends on the size of the septic tank or the number of people living in a certain house. According to the United States Environmental Protection Agency, septic tanks are typically pumped every three to five years. </p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How Often Does a 1000-Gallon Septic Tank Need to be Pumped?</h3>                
            <p class="text-justify">It boils down to the number of people occupying the house. If you have a 1000-gallon septic tank, you would need to arrange for a septic tank pumping:</p>        
                <ul><li>Once every three years if it is a family of two persons;</li></ul>
                <ul><li>Once every year if it is eight people;</li></ul>
                <ul><li>Once every two years if it is a family of five.</li></ul>                                    
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What Are the Signs That Your Septic Tank is Full?</h3>                
            <p class="text-justify">There are many signs that your septic tank is full including: </p>        
                <ul><li>Slow draining or flushing problems;</li></ul>
                <ul><li>Unpleasant odors;</li></ul>
                <ul><li>Greener and wetter lawn over the septic system;</li></ul>                                    
                
                <ul><li>Pools of water around your yard;</li></ul>
                <ul><li>Sewage backing up into your home;</li></ul>
                <ul><li>Pipes producing gurgling sounds</li></ul>                                    
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How Much Sludge Should a Septic Tank Have?</h3>                
            <p class="text-justify">The sludge in your septic tank should be more than 18 inches from the tank outlet. Said differently, you can only have a septic tank pumping when the sludge is more than one-third the size of the septic tank.</p>        
                
      </div>
    </div>
  </div>


<?php 
include "footer.php";
?>
</body>

</html>


