<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>

</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">Septic Tank Inspection</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/blog-header-2.gif);"></div>        
  </div>
</header>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Septic Tank Inspection</h3>                
            <p class="text-justify">Although septic tank inspection is not a legal requirement, it is recommended that you arrange for regular checks. A septic system is an important fixture to consider when buying a house with a septic tank. A septic tank inspection will benefit you for the following reasons:</p>        
            <p class="text-justify"><u>If you are a buyer, you want to: </u></p>  
            <p class="text-justify">1.	Know the condition of the new home septic system</p>
            <p class="text-justify">2.	Know the location of the septic tank and drain field</p>
            <p class="text-justify">3.	Renegotiate the property price if the septic tank inspection fails</p>
            
            <p class="text-justify"><u>If you are a homeowner, you want to:</u></p>  
            <p class="text-justify">1.	Ensure the septic system is fully functional and suitable for expansion plans</p>
            <p class="text-justify">2.	Avoid any potential issues of liability in the future from a malfunctioning septic system in case you sell your house</p>
            <p class="text-justify">3.	Prevent unpleasant odors, flushing problems, or system malfunctions</p>
            
            <p class="text-justify">At Septic Works LLC, our septic tank inspection services have three levels. They are designed to serve commercial, residential and municipal clients.</p>
            <p class="text-justify">1.	Septic tank inspection level 1: This is a visual inspection that involves running water from various drains and flushing toilets in the home. The septic tank inspector is looking at how well everything is draining. The visual inspection also includes looking for septic leakage to the top of the drain field area. We also check for sewage odors in the area of the septic tank and the leach field. This inspection can help identify problems but a level two can give our septic inspectors a better idea on issues with your septic system.</p>        
            <p class="text-justify">2.	Septic tank inspection level 2: We do a septic tank pumping. We remove the cover of the septic tank. This allows our septic tank inspectors to check the water level. Water levels determine whether water is properly draining. To make sure the water is properly flowing, our septic inspectors will run water in the home. This is done to determine if the water level rises when more water is introduced. Next, we will check out if your septic system has backflow from the absorption area. Backflow will let us know if there is a problem with the leach field.</p>                    
            <p class="text-justify">3.	Septic tank inspection level 3: We highly recommend you choose this option when you are purchasing a new home. The level three inspection includes level one and level two inspections. The difference with level three is that baffles are installed. In many states this is now mandated by the Department of Environmental Health. Baffles are installed in two separate locations. There is an inlet baffle and an outlet baffle. The inlet baffle allows water to flow into your septic system without disturbing the scum layer. This baffle also keeps the wastewater from flowing straight across the septic tank. It guides it to flow down, across and then up. The outlet baffle serves as a filter to retain solids from traveling to the leach field. Having this baffle is essential to avoid pipe clogs that could destroy your drain line.</p>            
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What is a septic certification?</h3>                
            <p class="text-justify">A septic certification is a written document that affirms the condition of the on-site septic system in your property to be either functioning, needs repairs or replacements, or just not operating altogether.</p>        
            <p class="text-justify">When you hire the services of Septic Works LLC, a septic inspector comes to your house and checks for any problems or defects in your system that need correction. The septic inspector tests each part of your system including the septic tank, drainage field, and soil. This process can also help identify any leaks or issues.</p>                    
            <p class="text-justify">Remember that a timely septic tank inspection is fundamental to save yourself from any costly repairs a few years down the line and give yourself peace of mind.</p>                    
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Why do you need a septic certification?</h3>                
            <p class="text-justify">In most states, it is recommended to present a septic certification when selling a house to avoid any issues of liability and accountability that might arise from a malfunctioning system.</p>        
            <p class="text-justify">A septic certification becomes more important for buyers. It is in the interest of a prospective buyer to insist that the septic system be investigated before they purchase the home if it has not been done recently.</p>                    
            <p class="text-justify">Nobody wants to buy the house of their dreams to only discover later that there are sewage backups or pipe clogs in the septic system or the septic tank needs to be pumped out at a short notice.</p>                    
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Who pays for septic tank inspection?</h3>                
            <p class="text-justify">If you are buying a house with a septic tank, you are probably wondering who pays for the septic tank inspection? Buyer or seller? It is typically the responsibility of the seller to pay for the septic tank inspection.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Can you inspect a septic tank without pumping it?</h3>                
            <p class="text-justify">During our septic tank inspection, we will learn whether or not your septic tank needs to be pumped. However, pumping a septic tank before a septic inspection means that the leach field cannot be tested. This action may hide any issues the septic system may have.</p>        
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How often should septic tanks be inspected?</h3>                
            <p class="text-justify">According to the <a href="">United States Environmental Protection Agency,</a> average household septic systems should be inspected at least once every three years by a professional septic inspector. However, septic tanks are typically pumped every three to five years.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Who does septic tank inspection?</h3>                
            <p class="text-justify">Not anybody can do a septic tank inspection. Only certified and licensed septic inspectors can carry it out. On top of this, septic tank inspection requirements vary by state, so make sure you hire a septic inspector who has all the experience and the know-how.</p>        
            <p class="text-justify">At Septic Works LLC, we do a septic inspection for any type of septic tank you have and we are licensed and certified. Our septic tank services include as well septic tank repair, septic tank pumping, septic tank installation, engineered septic system, alternative septic systems, and porta potty.</p>                    
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>How much does a septic tank inspection cost?</h3>                
            <p class="text-justify">A septic tank inspection cost for a home transaction starts at $350 and can go up to $1,000 or more depending on the inspection type.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What happens if the septic tank inspection fails?</h3>                
            <p class="text-justify">Many prospective buyers might hesitate to buy a property if the septic tank inspection fails. To avoid this, the property seller has to do quick fixes to the septic system. Once this is done, there can be another inspection. Some common scenarios are:</p>   

            <p class="text-justify">1.	The seller replaces the leach field or the septic tank</p>            
            <p class="text-justify">2.	The seller replaces the whole system</p>
            <p class="text-justify">3.	The seller does the necessary repairs </p>
            <p class="text-justify">4.	The seller credits the buyer the money to do repairs after the transaction has been completed</p>
            <p class="text-justify">5.	The buyer can renegotiate the price of the property</p>      
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>What to ask a septic inspector?</h3>                
            <p class="text-justify">Buying a house is everybody�s dream, but this lifetime investment involves many things to consider. If you are buying a house with a septic system, you want to ensure that it is in excellent condition. A healthy septic system protects your family, the environment, the greenery, and the drinking water. It also avoids any unwelcome surprises or costly repairs. Try to know the following information:</p>   
            
            <p class="text-justify">1.	How old is the septic system?</p>            
            <p class="text-justify">2.	How old is the property?</p>
            <p class="text-justify">3.	How long has the seller owned the property?</p>
            <p class="text-justify">4.	Where is the septic system located?</p>
            <p class="text-justify">5.	What is the size of the septic tank?</p>      
            <p class="text-justify">6.	Where is the original septic permit?</p>
            <p class="text-justify">7.	Ask for the septic tank maintenance records</p>
            <p class="text-justify">8.	Ask when the last septic tank inspection was conducted</p>
            <p class="text-justify">9.	Ask about other septic systems in the neighborhood</p>      
            
            
      </div>
    </div>
  </div>


<?php 
include "footer.php";
?>
</body>

</html>


