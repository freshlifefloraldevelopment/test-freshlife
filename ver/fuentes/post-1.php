<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>

</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">SC Septic Tank Regulations: 8 Things You Need to Know</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/blog-header-2.gif);"></div>        
  </div>
</header>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h2><a aria-hidden="true"><span class="icon icon-link"></span></a>Want to buy a house but it has a Georgia septic tank?</h2>          
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Want to buy a house but it has a septic tank? Don't worry! Through this article, we'll lead you through all that you need to know about SC septic tank regulations and septic tank inspection</h3>                
            <p class="text-justify">Understanding sc septic tank regulations is important to a healthy septic system. A good-performing septic system not only saves you money but protects your family from contamination and maintains the environment.</p>        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>1. SC Septic Tank Regulations</h3>                          
            <p class="text-justify">Here are a few points that you need to know about sc septic tank regulations:</p>   
                <ul><li>The number of bedrooms decides the capacity of the septic tank.</li></ul>            
                <ul><li>Each bedroom needs a septic tank capacity of 250 gallons.</li></ul>            
                <ul><li>Septic tanks have to be watertight and built against corrosion.</li></ul>  
                
                <ul><li>Newly built septic tanks have to have two compartments.</li></ul>            
                <ul><li>Septic tank shall be at least 50 feet away from the water supply.</li></ul>                            
                <ul><li>It is the home owner's responsibility to implement a septic tank maintenance.</li></ul>                                            
            <p class="text-justify">Getting a septic tank inspection has to be aligned with sc septic tank regulations. If you're getting your house ready to sell, there are some items you should inspect before you put it up for sale. Likewise, if you are looking to buy a property, you will have to do more than just checking out how the home looks like on the inside.</p>   
                
    <!-- Page 2 -->        
            <p class="text-justify">At Septic Works LLC, we do septic tank inspection, septic tank pumping, septic tank repair, septic tank installation, engineered septic system, alternative septic systems, and porta potty. We are also licensed and certified by the State of South Carolina to provide a septic certification before buying a house - our septic tank inspectors will take care of all your needs.</p>   
            <p class="text-justify">Our operations cover Charleston sc, Columbia sc, Hilton Head, Beaufort sc, Summerville sc, Bluffton sc, Isle of Palms and all over the state.</p>   
            <p class="text-justify">Since your groundwater comes from the septic system installed at your property after it has been treated, it is important to ensure that your septic tank is properly designed, installed, maintained, and inspected as per sc septic tank regulations. That's to protect your public health and the environment.</p>   
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>2.	Is a SC Septic Inspection Really Necessary?</h3>                          
            <p class="text-justify">Sadly, many sellers and buyers neglect the importance of running a septic tank inspection for their sc septic. It is recommended that a septic tank inspector checks your septic system at least once every three years. </p>   
            <p class="text-justify">And it becomes highly important when you want to sell your house. Presenting a septic certification that verifies the status of your septic tank following sc septic tank regulations to a prospective buyer raises the value of your house and avoids any potential issues of liability in the future from a malfunctioning septic system. </p>   
            <p class="text-justify">On the other hand, if you are a buyer, you certainly don't want any unwelcome surprises after you close your property purchase. You don't want to buy the house of your dreams to only find out later there is sewage backup or clogs in the septic system or you need to arrange for a septic tank pumping at a short notice.</p>               

    <!-- Page 3 -->  
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>3.	Hire the Best Septic Tank Inspector</h3>                          
            <p class="text-justify">So, what should you do to give yourself peace of mind? Whether a buyer or seller, talk to a professional septic inspector who knows the ins and outs of the sc septic. Schedule an appointment with a septic inspector to come to your house and check for any problems or defects that need correction.</p>   
            <p class="text-justify">The septic inspector will test each part of your septic system, including the septic tank, leach field, and soil. This way the septic tank inspection will help you to identify any leaks or issues and thus save you time and money spent on costly repairs.</p>   
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>4.	SC Septic Tank Regulations: Types of Septic Inspection </h3>                          
            <p class="text-justify">At Septic Works LLC, our experienced septic tank inspectors perform all sorts of checks for any type of septic tank and in compliance with all sc septic tank regulations for any residential, commercial, or municipal clients: </p>   
            <p class="text-justify">1.	Septic tank inspection level 1: This is a visual inspection that involves running water from various drains and flushing toilets in the home. The septic tank inspector is looking at how well everything is draining. </p>   
            <p class="text-justify">The visual inspection also includes looking for septic leakage to the top of the drain field area. We also check for sewage odors in the area of the septic tank and the leach field. This inspection can help identify problems but a level two can give our septic inspectors a better idea on issues with your septic system.</p>   
            
     <!-- Page 4 -->  
            <p class="text-justify">2.	Septic tank inspection level 2: We do a septic tank pumping. We remove the cover of the septic tank. This allows our septic tank inspectors to check the water level. Water levels determine whether water is properly draining. To make sure the water is properly flowing, our septic inspectors will run water in the home. This is done to determine if the water level rises when more water is introduced. Next, we will check out if your septic system has backflow from the absorption area. Backflow will let us know if there is a problem with the leach field.</p>        
            <p class="text-justify">3.	Septic tank inspection level 3: We highly recommend you choose this option when you are purchasing a new home. The level three inspection includes level one and level two inspections. The difference with level three is that septic tank baffles are installed. This is now mandated by SC septic tank regulations. Baffles are installed in two separate locations. There is an inlet baffle and an outlet baffle. </p>   
            <p class="text-justify">The inlet baffle allows water to flow into your septic system without disturbing the scum layer. This baffle also keeps the wastewater from flowing straight across your sc septic tank. It guides it to flow down, across and then up. The outlet baffle serves as a filter to retain solids from traveling to the drain field. Having this baffle is essential to avoid clogs that could destroy your drain line.</p>   
            <p class="text-justify">After completing our septic tank service, you will get to know all the following details: </p>   
                <ul><li>The type of septic system you have </li></ul>                
                <ul><li>The condition of the septic tank </li></ul>                
                <ul><li>The condition of the leach field </li></ul>                
                <ul><li>The location of the septic tank and drain field if previously unknown</li></ul>                                

     <!-- Page 5 -->     
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>5.	SC Septic Tank Regulations: Who Pays for Septic Certification?</h3>                          
            <p class="text-justify">SC septic tank regulations don't stipulate whose responsibility is. But it is typically the responsibility of the seller to pay for the septic inspection.</p>   
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>6.	Can You Inspect a SC Septic Tank Without Pumping It?</h3>                          
            <p class="text-justify">We will decide about this during our septic tank inspection. On occasions, the realtor or the house owner will arrange for a septic tank to be pumped out just before a septic inspection. Be careful! Pumping a septic tank before a septic inspection means that the leach field cannot be tested. This can hide issues the septic system may have.</p>   
            <p class="text-justify">You can also find answers for frequently-asked questions at the <a href="">S.C. Department of Health and Environmental Control.</a></p>   
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>7.	How Often Should Septic Tanks Be Inspected?</h3>                          
            <p class="text-justify">SC septic tank regulations recommend a septic inspection at least once every three years for average household septic systems. However, septic tanks are typically pumped every three to five years.</p>   
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>8.	SC Septic Tank Regulations: Septic System Components</h3>                          
            <p class="text-justify">Remember that your septic system is buried underground and not visible to your naked eye. It is a structure that relies on wastewater technology and natural resources to treat sewerage that goes out of your household toilets and drains. Then it releases it to the underground water under your property.</p>   
         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Any Septic System Mainly Has Four Units:</h3>                          
            <p class="text-justify">1.	A pipe that connects your house with the septic tank. </p>   
     
     <!-- Page 6 -->                 
            <p class="text-justify">2.	A septic tank or a buried container. As per sc septic tank regulations, septic tanks are made of concrete, fiberglass, or polyethylene. The job of a septic tank is important to the whole functionality of your septic system. All the wastewater and solids that leave your house end up in the septic tank via the connecting pipe. The septic tank ensures that the wastewater inside stays in place until solids have settled in the bottom of the tank to form something known as sludge. Oil and grease float to form a top layer known as scum. Once settled, it releases only the wastewater through the septic tank outlet baffle to the drain field.</p>   
            <p class="text-justify">3.	A drain field/leach field receives only the wastewater from the septic tank via the effluent filters. Further treatment in a series of perforated (with holes) pipes before it filters through the soil.</p>   
            <p class="text-justify">4.	Soil (natural resource) offers the last stage of treatment. It removes any remaining bacteria or solids before wastewater enters the groundwater.</p>   
     
                                        <!-- GRAFICO -->                 
     
     <!-- Page 7 -->                      
            <p class="text-justify">Not anybody can do a septic tank inspection. It is only carried out by certified and licensed septic inspectors.</p>   
            <p class="text-justify">On top of this, septic inspection requirements vary by state, so you want to make sure you hire a septic inspector who has all the technical know-how. </p>   
            <p class="text-justify">You can find further guidelines about SC septic tank regulations at the <a href="">S.C. Department of Health and Environmental Control.</a></p>   

         <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Work with the Best Septic Inspector in South Carolina</h3>                          
            <p class="text-justify">At Septic Works LLC, we specialize in all things that are septic. We have been serving clients across the US for 20 years including South Carolina. We are fully licensed and insured.</p>   
            <p class="text-justify">Our prices are competitive and our specialized services are sought by residential, commercial and municipal clients. Our septic inspectors provide an easy and frictionless experience.</p>   
            <p class="text-justify">Our customer service agents are available anytime you need any help. They are friendly, knowledgeable, and quick to respond to any questions or comments you have. If you have any doubts about SC septic tank regulations, call us today at 843-300-0318 </p>   
            
     
     
      </div>
    </div>
  </div>


<?php 
include "footer.php";
?>
</body>

</html>


