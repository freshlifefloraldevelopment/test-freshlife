<!DOCTYPE html>
<html lang="en">
<head>
<meta name="description" content="Timely septic tank repair is important to prevent small issues from getting worse. Watch out for warning signs that you need a septic tank repair.">    
<?php
$title="Service - Septic Works LLC.";
include "head.php";
head($title);
?>

</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">Porta-Potty Rentals</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-2.gif);"></div>        
  </div>
</header>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Porta-Potty Rentals</h3>                
            <p class="text-justify">Porta-potty rentals probably aren't at the top of your mind when planning your event. However, porta-potty rentals are the ideal solution for your home construction or the party you're throwing on the weekend.</p>        
            <p class="text-justify">At Septic Works LLC, we have been renting a porta potty for 20 years across the United States. We are highly competent and we are ready to accommodate the largest of events in a professional manner.</p>                
            <p class="text-justify">We can assess your need no matter if it is a weekend party or a long-term porta-potty rental at your warehouse or business. We will help you determine the right number of units to fit your budget and the event's unique needs.</p>        
            <p class="text-justify">Our porta-potty rentals don't require any upkeep or maintenance from you. Our services include delivery, placement, porta-potty cleaning, and removal. As well as portable toilet chemicals and camping toilets.</p>                
            <p class="text-justify">You can call us at 678-326-3591 or email us at info@septicworksllc.com</p>                            

      </div>
    </div>
  </div>            
            

<div class="block-1">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">        
            <div class="col-lg-8 mb-4 mbl-md-0">
                  <div class="block-12-card-1__content w-100">
                        <div><img class="img-fluid w-100" src="../page-septic/img/portable.jpg" alt="Portable" ></div>
                        <!--p class="text-justify">A house for sale with a Georgia septic tank.</p-->
                  </div>
            </div>               
    </div>
  </div>
</div>
    
            
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">            
        <h2><a aria-hidden="true"><span class="icon icon-link"></span></a>F.A.Q</h2>                            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>1.	What is a portable toilet?</h3>                
            <p class="text-justify">A portable toilet refers to any type of toilet that you can move around to serve people at particular events including weddings, birthday parties, or construction sites. Porta potties are not connected to a septic tank or a municipal system and are completely self-contained.</p>        

        <!--Page 2  -->    
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>2.	How does a portable toilet work?</h3>                
            <p class="text-justify">How a portable toilet works is different from a normal house toilet. Portable toilets are not connected to a sewer. When you flush the toilet, the waste gets carried to a holding tank that sits beneath the toilet seat.</p>        
            <p class="text-justify">Inside the holding tank, there is a blue additive to sanitize and prevent unpleasant odors as well as chemicals that break down the solid waste.</p>        
            <p class="text-justify">The toilet flush uses freshwater that comes from a water tank inside the toilet. Porta-potty rental units don't need to be connected to a water supply.</p>        
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>3.	How much does it cost to rent and clean a porta-potty?</h3>                
            <p class="text-justify">There is no definite answer to how much it costs to rent and clean a porta-potty. The cost varies depending on the geographic location, the frequency of porta potty cleaning, the total number of porta-potty rentals, porta-potty rental length, and site accessibility. </p>        
            <p class="text-justify">For example, if you are having a multi-day event and have rented more portable toilets, you would need frequent porta potty cleaning services and thus higher costs.</p>        
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>4.	How do you maintain a porta-potty rental unit?</h3>                
            <p class="text-justify">At Septic Works LLC, your portable toilet is worry-free. We will maintain and service your portable toilet for you. If you should notice a high amount of use, all you have to do is give us a call. We will be glad to come to assist with any issues that may arise.</p>        
            <p class="text-justify">We do recommend leaving the door open for about a half-hour when people are not likely to use to the porta potty.</p>        

        <!--Page 3  -->      
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>5.	How many porta potties do you need?</h3>                
            <p class="text-justify">The main factors to consider for how many porta potties you need are the number of people who will use the portable restrooms and the number of hours for which you'll need them. The consensus is that every 50 people need one porta-potty.</p>        
            <p class="text-justify">If you are planning a wedding or birthday party that lasts up to four hours, you will need at least two porta-potty rental units for every 100 people.</p>        
            <p class="text-justify">But you will have to add more porta-potties if people are staying more than four hours or if alcohol will be served.</p>                    
        
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>6.	How many porta-potty rental units do I need for my construction site?</h3>                
            <p class="text-justify"><a href="">The United States Department of Labor Occupational Safety and Health Administration</a> requires that 20 employees or less have at least one porta-potty.</p>        
            <p class="text-justify">Toilets shall be provided for employees according to the following table:</p>
                <ul><li>1 porta potty if you have 20 employees or less</li></ul>
                <ul><li>1 toilet seat for 20 employees or more and 1 urinal per 40 workers</li></ul>
                <ul><li>1 toilet seat for 200 employees or more and 1 urinal per 50 workers</li></ul>                        

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>7.	How to clean a portable toilet?</h3>                
            <p class="text-justify">How to clean a portable toilet includes four steps:</p>     
            
        <!--Page 4  -->                  
                <li>Remove the waste</li>
            <p class="text-justify">Unlike the toilets most of us have in our homes, porta-potties aren't connected to any sort of sewage system. Instead, the waste goes to a holding tank and stays there until it is serviced.</p>                        
            <p class="text-justify">Our sanitation workers start by removing the waste from the holding tank. To do this, we insert a vacuum hose into the opening of the toilet and empty the waste tank.  Then we transport the waste by a sanitation truck to the nearest wastewater treatment facility where it is properly treated.</p>                                    
                   <li>Prepare the porta-potty rental unit for use</li>
            <p class="text-justify">After cleaning the porta-potty, we prepare the unit for use. We fill it with fresh water and add blue additives to the holding tank to disinfect and prevent foul smells. You'll know it's time to change the additive when it takes on a greenish tint.</p>                        
                   <li>Eliminate bacteria</li>
            <p class="text-justify">We clean and wash the interior walls and major points of contact inside the portable restroom, the floors, urinals, toilet seats, and any fixtures. This eliminates germs and bacteria and keeps the porta-potty rental unit clean and fresh.</p>                        
                   <li>Restock the supplies</li>
            <p class="text-justify">We refill any hand sanitizers, soap, and paper towel dispensers. Your guests will never run out of toilet paper or hand sanitizer at any moment.</p>                        
         
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>8.	How to empty a portable toilet?</h3>                
            <p class="text-justify">How to empty a portable toilet is a bit similar to emptying a septic tank. Unlike the toilets most of us have in our homes, porta-potties aren't connected to any sort of sewage system. Instead, the waste goes to a holding tank and stays there until it is serviced.</p>     
            
        <!--Page 5  -->                          
            <p class="text-justify">Our sanitation workers start by removing the waste from the holding tank. To do this, we insert a vacuum hose into the opening of the toilet and empty the waste tank.  Then we transport the waste by a sanitation truck to the nearest wastewater treatment facility where it is properly treated.</p>     
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>9.	Where to empty the portable toilet?</h3>                
            <p class="text-justify">At Septic Works LLC, we transport the waste of a porta-potty rental unit by a sanitation truck to the nearest wastewater treatment facility where it is properly treated.</p>     
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>10.	How often do you need to empty a portable toilet?</h3>                
            <p class="text-justify">How often you need to empty, clean or pump a porta-potty depends on the amount of use and the amount of people using the portable toilet.</p>     
            <p class="text-justify">A Septic Works LLC representative can assist you with determining how often your portable toilet will need service.</p>                 
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>11.	How do I stop my portable toilet from smelling?</h3>                
            <p class="text-justify">It is a good idea to use an air freshener to emit fragrance but you will still need to air out the toilet regularly. Leave the door open for at least half an hour when people are not likely to use the porta-potty and this will make a huge difference.</p>     
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>12.	What is the blue liquid in a porta-potty rental unit?</h3>                
            <p class="text-justify">The blue liquid is an additive that we add to combat the smells that a portable toilet produces. It is a deodorizer that emits a pleasant fragrance and keeps the portable toilet smelling fresh.</p>     

        <!--Page 6  -->                                      
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>13.	Is the blue liquid in porta-potties safe?</h3>                
            <p class="text-justify">Yes, it is safe. It is not toxic, nor threatening to the environment. It serves as a deodorizer that emits fragrance and keeps the porta-potty rental piece smelling fresh.</p>     
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>14.	What chemicals do I need for a portable toilet?</h3>                
            <p class="text-justify">There are a few portable toilet chemicals that are used for different purposes. The blue dye masks the waste when you flush the toilet and when the holding tank is full, it changes color from blue to green.</p>     
            <p class="text-justify">Another portable toilet chemical is the biocides that we use to prevent the growth of the bacteria found in human waste. Biocides help to maintain a clean and odor-free environment for several people to use the portable toilets.</p>     
            <p class="text-justify">We use fragrances inside our porta-potty rental units to hide odors and provide a good experience for the occupants, similar to the one they normally have at home.</p>     
            <p class="text-justify">Portable toilet chemicals come in liquid and tablet form. They are all eco-friendly and not difficult to use. We only use chemicals that are made for portable toilets.</p>     
        
      </div>
    </div>
  </div>


<?php 
include "footer.php";
?>
</body>
</html>


