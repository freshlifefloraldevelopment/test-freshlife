<nav class="hero-nav position-relative container mx-auto px-0">
        <ul class="nav w-100 list-unstyled align-items-center p-0">
          <li class="hero-nav__item"><a href="/"><img class="hero-nav__logo" src="../page-septic/img/septiclogo.png" change-src-onscroll="../page-septic/img/septiclogo.png" alt="our logo"></a><!-- Don't remove this empty span --><span class="mx-2"></span></li>
          
          <li id="hero-menu" class="flex-grow-1 hero__nav-list hero__nav-list--mobile-menu ft-menu">
            <ul class="hero__menu-content nav flex-column flex-lg-row ft-menu__slider animated list-unstyled p-2 p-lg-0">
              <li class="flex-grow-1">
                <ul class="nav nav--lg-side flex-column-reverse flex-lg-row-reverse list-unstyled align-items-center p-0">
                  <li class="flex-grow-1">
                    <ul class="nav nav--lg-side flex-column-reverse flex-lg-row-reverse list-unstyled align-items-center p-0">
                      <!--li class="hero-nav__item"><a href="../front-end/login.php" target="_blank" class="btn btn-primary">Login</a></li-->
                    </ul>
                  </li>
                    <li class="hero-nav__item"><a href="../page-septic/blog.php" class="hero-nav__link">Blog</a></li>                    
                    <li class="hero-nav__item"><a href="../page-septic/faq.php" class="hero-nav__link">F.A.Q.</a></li>
                    <li class="hero-nav__item"><a href="../page-septic/contact-us.php" class="hero-nav__link">Contact Us</a></li>

                        <li class="hero-nav__item main-sub"><a href="../page-septic/services.php" class="hero-nav__link">Services</a>
                                      <ul class="sub-container bg-secondary">
                                          <li class="item_sub"><a href="../porta-potty-rentals" class="hero-nav__link">Porta-potty rentals</a>                                                                                                                                                                
                                          <li class="item_sub"><a href="../septic-tank-repair" class="hero-nav__link">Septic tank repair</a>
                                          <li class="item_sub"><a href="../septic-tank-pumping" class="hero-nav__link">Septic tank pumping</a>
                                          <li class="item_sub"><a href="../septic-tank-inspection" class="hero-nav__link">Septic tank inspection</a>
                                          <li class="item_sub"><a href="../septic-tank-installation-near-me" class="hero-nav__link">Septic tank installation</a>
                                          <li class="item_sub"><a href="#" class="hero-nav__link">Engineered septic system</a>
                                          <li class="item_sub"><a href="#" class="hero-nav__link">Alternative septic systems</a>                      
                                          </li>
                                      </ul>
                        </li>                               
                    <li class="hero-nav__item"><a href="../page-septic/about-us.php" class="hero-nav__link">About</a></li>
                    <li class="hero-nav__item"><a href="../page-septic/home.php" class="hero-nav__link">Home</a></li>
                </ul>
              </li>
            </ul><button close-nav-menu="" class="ft-menu__close-btn animated"><svg class="bi bi-x" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="https://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z" clip-rule="evenodd"></path>
                <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z" clip-rule="evenodd"></path>
              </svg></button>
          </li>
          <li class="hero-nav__item d-lg-none d-flex flex-row-reverse"><button open-nav-menu="" class="text-center px-2"><i class="fas fa-bars"></i></button></li>
        </ul>
</nav>