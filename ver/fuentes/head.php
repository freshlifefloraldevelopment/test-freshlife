<?php 
function head($title){
?>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&amp;display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <title><?php echo $title; ?></title>
  <link href="img/septiclogo.png" rel="shortcut icon">
  <script>
    window.HELP_IMPROVE_VIDEOJS = !1
  </script>
  
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-4EMBNXN710"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-4EMBNXN710');
        </script>    
    <?php
}