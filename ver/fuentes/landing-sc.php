<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="Septic Works LLC - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->          
<script src="js/jquery-3.6.0.js"></script>
<script src="js/jquery-3.6.0.min.js"></script>

  
  
  <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  
  <style type="text/css">
        #myModal{
            width: 50%;
            height: 700px;
            color: black;
            padding: 30px;
            box-sizing: border-box;            
        position: fixed;
            left: 50%;
            top: 50%; 
            margin-left: -400px;
            margin-top: -250px;
        }      
        
        #graciasModal{
            width: 50%;
            height: 700px;
            color: black;
            padding: 30px;
            box-sizing: border-box;            
        position: fixed;
            left: 50%;
            top: 50%; 
            margin-left: -400px;
            margin-top: -250px;
        }   
        
        .modal-backdrop
        {
            opacity:0.7 !important;
        }  
        
  .heroa__body {
      max-width: 640px;
      margin: 0;
      padding: 0;
      z-index: 1;
  }    
  .estilo-x { 
        font-size: calc(1rem + 1.4vh)!important;;
        line-height: 1rem;
        padding: 7rem;
        margin: 1rem;
  }
  
  </style>
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden p-0" >    
    <div class="hero__content h75 container " style="background-color: #000000;" >

        <!--div class="container "-->
          <div class="row">
              
                <div class="heroa__body col-md-3">
                    <br><br><br><br><br>
                    <br><br><br><br><br>
                    <br><br><br><br><br>
                    <br><br><br><br><br>                    
                    <br>
                    <a href="javascript:void(0);" class="btn-secondary btn-margin-auto mb-2 mb-3"  data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal" data-backdrop="false">Get Started <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;                                                          
                </div>
              
              
              
          </div>
        <!--/div-->
        
    </div>
    
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/black.svg);">
            <br>
            <h4><span class="estilo-x">SCHEDULE <font color="#b62226">SEPTIC TANK INSPECTION</font></span></h4>                    
            <h4 align="center"><span class="estilo-x" >IN SOUTH CAROLINA AT $300</span></h4>
            <h4><span class="estilo-x"><font color="#b62226">SEPTIC TANK DRAIN LINE REPAIR</font> AT</span></h4>                    
            <h4 align="center"><span class="estilo-x">$4,299</span></h4>
            <br>
            <h4 align="center"><span class="estilo-x">SOME RESTRICTIONS APPLY</span></h4>
            <h4 align="center"><span class="estilo-x">CALL <font color="#b62226">(843) 300-0318</font> FOR MORE</span></h4>  
            <h4 align="center"><span class="estilo-x">INFORMATION</span></h4>   
            <br>                    
            <h4 align="center"><span class="estilo-x"><font color="#b62226">AVOID</font> COSTLY REPAIRS AND</span></h4>  
            <h4 align="center"><span class="estilo-x">PROTECT THE HEALTH OF YOUR</span></h4>   
            <h4 align="center"><span class="estilo-x">FAMILY AND THE ENVIROMENT</span></h4>  
            <h4 align="center"><span class="estilo-x">AROUND</span></h4>                                                               
        </div>
        
        <div class="block-31__image-column container col-lg-6" >
          <video id="my-video" class="video-js" autoplay muted loop preload="auto" width="100%"  >
              <source src="img/half-home.mp4" type="video/webm">
          </video>      
        </div>
  </div> 
    
</header>
<div class="block-1 mt-5" id='cab'>
  <div class="container">
      <div class="col-lg-2 col-xl-2 mx-auto text-center mb-5">
           <a id="bbblink" class="ruhzbum" href="https://www.bbb.org/us/ga/bluffton/profile/septic-tank-contractors/septic-works-llc-0743-108004#bbbseal" 
              accesskey=""title="Septic Works, LLC., Septic Tank Contractors, Bluffton, GA" 
	 style="display: block;position: relative;overflow: hidden; width: 150px; height: 68px; margin: 0px; padding: 0px;">
	 <img style="padding: 0px; border: none;" id="bbblinkimg" 
	 src="https://seal-centralgeorgia.bbb.org/logo/ruhzbum/septic-works-108004.png" width="300" height="68" alt="Septic Works, LLC., Septic Tank Contractors, Bluffton, GA" /></a>
	 <script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); (function(){var s=document.createElement('script');s.src=bbbprotocol + 'seal-centralgeorgia.bbb.org' + unescape('%2Flogo%2Fseptic-works-108004.js');s.type='text/javascript';s.async=true;var st=document.getElementsByTagName('script');st=st[st.length-1];var pt=st.parentNode;pt.insertBefore(s,pt.nextSibling);})();</script>          
      </div>                        
    <div class="row mb-5">
      <div class="col-12 col-md-2 text-center ">
        <span class="highlight"><b>CUSTOMER SATISFACTION</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>BEST PRICE</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>PERSONAL ATTENTION</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>CONVENIENT TIMES</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>EXCELLENT CUSTOMER SERVICE</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>FAST RESPONSE</b></span>
      </div>
    </div>
    
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-12 px-lg-5 mb-2 mbl-lg-0">
            <p>A septic tank inspection should be on top of your mind when buying or selling a home with a septic system. It is important as a homeowner to ensure that your septic system is fully functional and with no unpleasant odors or flushing problems. As a buyer, the purchase of a home might be the largest investment you will ever make. A septic inspection will confirm that your dream home is in excellent condition and avoid you any unwelcome surprises.</p>
            <p>At Septic Works LLC, we do a septic inspection and we are licensed and certified– whether you’re making a purchase or sale, our septic tank inspectors will take care of all your needs.</p>
            
        </div>
    </div>
  </div>
</div>


<div class="block-1 pt-5" id='test'>
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-lg-4 col-xl-6 px-lg-5 mb-4 mbl-lg-0">
            <h2 class="block__title mb-3 highlight">AVOID COSTLY REPAIRS AND PROTECT THE HEALTH OF YOUR FAMILY AND THE ENVIRONMENT AROUND</h2>
                    <a href="javascript:void(0);" class="btn-secondary btn-margin-auto mb-2 mb-3"  onclick="imagen_req()" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal" data-backdrop="false">Contact us <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
            
       
            <h3 class=" mb-3">WHAT DOES A SEPTIC TANK INSPECTION INVOLVE?</h3>
            <p class="text-justify">At Septic Works LLC, our professional septic tank inspectors are ready and willing to serve all residential, commercial and municipal clients. Our septic tank services include:</p>
            <p class="text-justify"><b>Septic inspection level 1:</b>This is a visual inspection that involves running water from various drains and flushing toilets in the home. The septic tank inspector is looking at how well everything is draining. The visual inspection also includes looking for septic leakage to the top of the drain field area. We also check for sewage odors in the area of the septic tank and the leach field. This inspection can help identify problems but a level two can give our septic inspectors a better idea on issues with your septic system.</p>
                    <a href="javascript:void(0);" class="btn-secondary btn-margin-auto mb-2 mb-3"  onclick="imagen_req()" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal" data-backdrop="false">Contact us <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
            <p class="text-justify"><b>Septic inspection level 2:</b> We do a septic tank pumping. We remove the cover of the septic tank. This allows our septic tank inspectors to check the water level. Water levels determine whether water is properly draining. To make sure the water is properly flowing, our septic inspectors will run water in the home. This is done to determine if the water level rises when more water is introduced. Next, we will check out if your septic system has backflow from the absorption area. Backflow will let us know if there is a problem with the leach field.</p>
                    <a href="javascript:void(0);" class="btn-secondary btn-margin-auto mb-2 mb-3"  onclick="imagen_req()" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal" data-backdrop="false">Contact us <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
            <p class="text-justify"><b>Septic inspection level 3:</b> We highly recommend you choose this option when you are purchasing a new home. The level three septic tank service includes both levels one and two. The difference with level three is that baffles are installed. In many states this is now mandated by the Department of Environmental Health. Baffles are installed in two separate locations: an inlet baffle and an outlet baffle. The inlet baffle allows water to flow into your septic system without disturbing the scum layer. This baffle guides wastewater to flow down, across the septic tank and then up. The outlet baffle serves as a filter to retain solids from traveling to the drain field.</p>
                                    
                    <a href="javascript:void(0);" class="btn-secondary btn-margin-auto mb-2 mb-3 " onclick="imagen_req()" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal" data-backdrop="false">Contact us <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
            
            <p class="text-justify">Unless requested by the customer to carry out the septic inspection level 3 or 2, our septic tank inspectors will assess your septic system using the first level of inspection.

</p>
        </div>
        <div class="col-lg-6 mb-4 mbl-md-0">
            <div class="block-12-card-1__content w-100">
                <div><img class="img-fluid w-100 mb-4" src="img/services1.jpg" alt="about us" >
                </div>
                <div><img class="img-fluid w-100 mb-4" src="img/home-services.jpg" alt="about us" >
                </div>
                <div><img class="img-fluid w-100 mb-4" src="img/dreamstime_s_25485823.jpg" alt="about us" >
                </div>
                <div><img class="img-fluid w-100" src="img/dreamstime_s_173449759.jpg" alt="about us" >
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="block-1 space-between-blocks pt-3" >
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2" id='faq'>
        <div class="col-lg-12 px-lg-5 mb-4 mbl-lg-0"id="accordion">
        <h2 class="block__title mb-3 highlight text-center">SEPTIC TANK INSPECTION FAQS</h2>
        
        <?php 
                include "landing-faq.php";
         ?>
        </div>
        <div class="col-12 mb-4">
        <p>The septic permit has a drawing of the septic system of the property. This paper lets you know how many bedrooms the septic system is designed for. Also, it lets you know where the septic tank and leach field are located so you don’t waste time looking for them and can better protect your septic system.</p>
        
        
        <!--a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a-->
        
                <div class="to_right">
                    <br>
                    <div id="erMsg" style="display:none;color:red;">Please select</div>
                    <a href="" class="btn-secondary btn-margin-auto mb-2 mb-3" style="display:none;color:red;" data-dismiss="modal" aria-label="Close">Cancel</a>
                    <a href="javascript:void(0);" style="display:none;color:red;" data-toggle="modal" data-target="#buying_method_next" data-dismiss="modal">After </a>
                    <a href="javascript:void(0);" class="btn-secondary btn-margin-auto mb-2 mb-3"  onclick="imagen_req()" data-toggle="modal" data-target=".buying_method_next" data-dismiss="modal" data-backdrop="false">Contact us <i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp;
                </div>
        
        </div>
    </div>
      
      <!--Order create modal open-->
<div class="modal buying_method_next " id="myModal"   tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md" >
        <div class="modal-content" >
          <!-- header modal -->
          <div class="modal-header">
            <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Contact us</h3>  
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="icarga()">
                    <span class="fi fi-close fs--18" aria-hidden="true">X</span>
                </button>
          </div>
          <!-- body modal 3-->
          <form name="frmcat" action="insert-landing1.php" class="form-horizontal" id="profile_form" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <h5><a aria-hidden="true"><span class="icon icon-link"></span></a>We will help you with your septic needs</h5>  
                        <div class="mb-3 ">
                          <div style="position: absolute; display: flex; width: 0px; height: 0px; border: none; padding: 0px; margin: 0px; background: no-repeat; visibility: visible; user-select: none; pointer-events: none; z-index: auto; opacity: 1;"><img title="Required" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjQ4cHgiIGhlaWdodD0iNDhweCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDxkZWZzPjwvZGVmcz4NCiAgICA8ZyBpZD0iSWNvbnMtKy1sb2dvIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHlsZT0ib3BhY2l0eTowLjU0Ij4NCiAgICAgICAgPGcgaWQ9IlNQLUxvZ28iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02MS4wMDAwMDAsIC02MS4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJzeW1ib2wiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYxLjAwMDAwMCwgNjEuMDAwMDAwKSI+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM0LjA4MTAyNzcsMjMuMjYyODk5MiBMMjUuMDYyMDU4NSwxNC4yNzg2ODY0IEMyNC42MDQyODYxLDEzLjgyNDMyMDMgMjMuODYzODg5LDEzLjgyNDMyMDMgMjMuNDA0NDEwNywxNC4yNzg2ODY0IEwxNC4zODc3MTYxLDIzLjI2Mjg5OTIgQzEzLjkyNzY2OSwyMy43MTc4MzE5IDEzLjkyNzY2OSwyNC40NTYwMzUzIDE0LjM4NzcxNjEsMjQuOTEyNjY3NiBMMjMuNDA0NDEwNywzMy44OTYzMTM5IEMyMy42MzE4NzUyLDM0LjEyNDA2MzUgMjMuOTMxNTU5NywzNC4yMzc5MzgzIDI0LjIzMjk1MDMsMzQuMjM3OTM4MyBDMjQuNTMyMDY2MSwzNC4yMzc5MzgzIDI0LjgzNTE2MjYsMzQuMTI0MDYzNSAyNS4wNjIwNTg1LDMzLjg5NjMxMzkgTDM0LjA4MTAyNzcsMjQuOTEyNjY3NiBDMzQuMjk5MzkzNywyNC42OTA1ODM0IDM0LjQyMjc5MzIsMjQuMzk4MjQ4MSAzNC40MjI3OTMyLDI0LjA4ODM1IEMzNC40MjI3OTMyLDIzLjc3NjE4NTcgMzQuMjk5MzkzNywyMy40ODA0NTExIDM0LjA4MTAyNzcsMjMuMjYyODk5MiBaIiBpZD0iRmlsbC0xIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzODc3OTUsMjcuNzQ3ODY3MiBDMTEuMDgxMDA3MSwyNy4yOTQ2MzQxIDEwLjM0MDYxLDI3LjI5NDYzNDEgOS44ODI4Mzc2MywyNy43NDc4NjcyIEwwLjg2Nzg0OTA0OSwzNi43MzI2NDY1IEMwLjQwODM3MDY2OCwzNy4xODgxNDU4IDAuNDA4MzcwNjY4LDM3LjkyNTIxNjEgMC44Njc4NDkwNDksMzguMzgzNTQ4IEw5Ljg4MjgzNzYzLDQ3LjM2ODMyNzMgQzEwLjExMDg3MDgsNDcuNTkzODEwOCAxMC40MTExMjQsNDcuNzA5OTUxOCAxMC43MTI1MTQ2LDQ3LjcwOTk1MTggQzExLjAwNzA4MTEsNDcuNzA5OTUxOCAxMS4zMTE4ODM2LDQ3LjU5MzgxMDggMTEuNTM4Nzc5NSw0Ny4zNjgzMjczIEwyMC41NjE3Mjk0LDM4LjM4MzU0OCBDMjAuNzc3ODIwNywzOC4xNjA4OTczIDIwLjkwNTIwMDgsMzcuODcwODI4MSAyMC45MDUyMDA4LDM3LjU1NjM5NzYgQzIwLjkwNTIwMDgsMzcuMjQ1OTMzIDIwLjc3NzgyMDcsMzYuOTUxODk4IDIwLjU2MTcyOTQsMzYuNzMyNjQ2NSBMMTEuNTM4Nzc5NSwyNy43NDc4NjcyIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMwMEE5RTAiPjwvcGF0aD4NCiAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzguNTY2MTczNiwwLjgyMzM4NTk0IEMzOC4xMDg5Njk5LDAuMzY3ODg2NjkyIDM3LjM2NDU5MjIsMC4zNjUwNTM5ODUgMzYuOTA5NjYzMSwwLjgyMzM4NTk0IEwyNy44OTI5Njg1LDkuODA5Mjk4MzUgQzI3LjQzNTE5NjEsMTAuMjYxOTY0OSAyNy40MzUxOTYxLDExLjAwMTg2NzkgMjcuODkyOTY4NSwxMS40NTg1MDAyIEwzNi45MDk2NjMxLDIwLjQ0MzI3OTUgQzM3LjEzNTk5MDMsMjAuNjY5ODk2MSAzNy40MzUxMDYyLDIwLjc4MjYzNzggMzcuNzM2NDk2NywyMC43ODI2Mzc4IEMzOC4wMzU2MTI2LDIwLjc4MjYzNzggMzguMzM1Mjk3MSwyMC42Njk4OTYxIDM4LjU2NjE3MzYsMjAuNDQzMjc5NSBMNDcuNTgyODY4MiwxMS40NTg1MDAyIEM0Ny44MDE4MDI4LDExLjIzNDcxNjQgNDcuOTI1MjAyNCwxMC45NDQ2NDcyIDQ3LjkyNTIwMjQsMTAuNjMxOTE2NCBDNDcuOTI1MjAyNCwxMC4zMTk3NTIxIDQ3LjgwMTgwMjgsMTAuMDI5MTE2NCA0Ny41ODI4NjgyLDkuODA1MzMyNTYgTDM4LjU2NjE3MzYsMC44MjMzODU5NCBaIiBpZD0iRmlsbC01IiBmaWxsPSIjRDYxODE4Ij48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzMDA3OSwwLjc5NjgxNTE1IEMxMS4wNzQ1ODEzLDAuMzQzMDE1NTI2IDEwLjMyNjc5MTYsMC4zNDMwMTU1MjYgOS44NzU4NDMxLDAuNzk2ODE1MTUgTDAuODU3NDQyNTQ2LDkuNzgxMDI3OTMgQzAuMzk3Mzk1NTAzLDEwLjIzNjUyNzIgMC4zOTczOTU1MDMsMTAuOTc2NDMwMiAwLjg1NzQ0MjU0NiwxMS40MzMwNjI1IEw5Ljg3NTg0MzEsMjAuNDE2MTQyMiBDMTAuMTAyMTcwMywyMC42NDcyOTExIDEwLjQwMTI4NjIsMjAuNzU3MjAwMSAxMC43MDI2NzY3LDIwLjc1NzIwMDEgQzExLjAwNDA2NzIsMjAuNzU3MjAwMSAxMS4zMDMxODMxLDIwLjY0NzI5MTEgMTEuNTMwMDc5LDIwLjQxNjE0MjIgTDIwLjU0NjIwNDksMTEuNDMzMDYyNSBDMjAuNzY3NDE0MiwxMS4yMTI2Nzc5IDIwLjg5NDIyNTcsMTAuOTIxNDc1NyAyMC44OTQyMjU3LDEwLjYwNDc3OTEgQzIwLjg5NDIyNTcsMTAuMjk0MzE0NCAyMC43Njc0MTQyLDEwLjAwMjU0NTYgMjAuNTQ2MjA0OSw5Ljc4MTAyNzkzIEwxMS41MzAwNzksMC43OTY4MTUxNSBaIiBpZD0iRmlsbC03IiBmaWxsPSIjN0FCODAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM4LjYzMTY4MzQsMjcuNzk4ODU1OSBDMzguMTc0NDc5NywyNy4zNDIyMjM2IDM3LjQyOTUzMzMsMjcuMzQyMjIzNiAzNi45NzM0NjY5LDI3Ljc5ODg1NTkgTDI3Ljk1OTYxNTYsMzYuNzgwMjM2IEMyNy41MDE4NDMyLDM3LjIzODAwMTQgMjcuNTAxODQzMiwzNy45NzUwNzE3IDI3Ljk1OTYxNTYsMzguNDMxMTM3NSBMMzYuOTczNDY2OSw0Ny40MTgxODMgQzM3LjIwMDM2MjgsNDcuNjQ0Nzk5NSAzNy40OTg5MSw0Ny43NTk4MDc0IDM3LjgwMDg2OTIsNDcuNzU5ODA3NCBDMzguMTAyMjU5Nyw0Ny43NTk4MDc0IDM4LjM5ODUzMjMsNDcuNjQ0Nzk5NSAzOC42MzE2ODM0LDQ3LjQxODE4MyBMNDcuNjQ4Mzc4LDM4LjQzMTEzNzUgQzQ3Ljg2Njc0NCwzOC4yMTE4ODYgNDcuOTkxODQ5NSwzNy45MTg5ODQxIDQ3Ljk5MTg0OTUsMzcuNjA5MDg2IEM0Ny45OTE4NDk1LDM3LjI5NTc4ODYgNDcuODY2NzQ0LDM3LjAwNDU4NjQgNDcuNjQ4Mzc4LDM2Ljc4MDIzNiBMMzguNjMxNjgzNCwyNy43OTg4NTU5IFoiIGlkPSJGaWxsLTkiIGZpbGw9IiMwMDQ2QUQiPjwvcGF0aD4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg" style="position: relative; border: none; display: inline; cursor: default; padding: 0px; margin: 0px; pointer-events: auto; left: 315px; top: 11px; width: 16px; height: 16px; min-width: 16px; max-width: 16px; min-height: 16px; max-height: 16px;"></div><input type="text" class="form-control confondo" id="name" name="name" placeholder="your name" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                        </div>

                        <div class="mb-3">
                          <div style="position: absolute; display: flex; width: 0px; height: 0px; border: none; padding: 0px; margin: 0px; background: no-repeat; visibility: visible; user-select: none; pointer-events: none; z-index: auto; opacity: 1;"><img title="Required" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjQ4cHgiIGhlaWdodD0iNDhweCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDxkZWZzPjwvZGVmcz4NCiAgICA8ZyBpZD0iSWNvbnMtKy1sb2dvIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHlsZT0ib3BhY2l0eTowLjU0Ij4NCiAgICAgICAgPGcgaWQ9IlNQLUxvZ28iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02MS4wMDAwMDAsIC02MS4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJzeW1ib2wiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYxLjAwMDAwMCwgNjEuMDAwMDAwKSI+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM0LjA4MTAyNzcsMjMuMjYyODk5MiBMMjUuMDYyMDU4NSwxNC4yNzg2ODY0IEMyNC42MDQyODYxLDEzLjgyNDMyMDMgMjMuODYzODg5LDEzLjgyNDMyMDMgMjMuNDA0NDEwNywxNC4yNzg2ODY0IEwxNC4zODc3MTYxLDIzLjI2Mjg5OTIgQzEzLjkyNzY2OSwyMy43MTc4MzE5IDEzLjkyNzY2OSwyNC40NTYwMzUzIDE0LjM4NzcxNjEsMjQuOTEyNjY3NiBMMjMuNDA0NDEwNywzMy44OTYzMTM5IEMyMy42MzE4NzUyLDM0LjEyNDA2MzUgMjMuOTMxNTU5NywzNC4yMzc5MzgzIDI0LjIzMjk1MDMsMzQuMjM3OTM4MyBDMjQuNTMyMDY2MSwzNC4yMzc5MzgzIDI0LjgzNTE2MjYsMzQuMTI0MDYzNSAyNS4wNjIwNTg1LDMzLjg5NjMxMzkgTDM0LjA4MTAyNzcsMjQuOTEyNjY3NiBDMzQuMjk5MzkzNywyNC42OTA1ODM0IDM0LjQyMjc5MzIsMjQuMzk4MjQ4MSAzNC40MjI3OTMyLDI0LjA4ODM1IEMzNC40MjI3OTMyLDIzLjc3NjE4NTcgMzQuMjk5MzkzNywyMy40ODA0NTExIDM0LjA4MTAyNzcsMjMuMjYyODk5MiBaIiBpZD0iRmlsbC0xIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzODc3OTUsMjcuNzQ3ODY3MiBDMTEuMDgxMDA3MSwyNy4yOTQ2MzQxIDEwLjM0MDYxLDI3LjI5NDYzNDEgOS44ODI4Mzc2MywyNy43NDc4NjcyIEwwLjg2Nzg0OTA0OSwzNi43MzI2NDY1IEMwLjQwODM3MDY2OCwzNy4xODgxNDU4IDAuNDA4MzcwNjY4LDM3LjkyNTIxNjEgMC44Njc4NDkwNDksMzguMzgzNTQ4IEw5Ljg4MjgzNzYzLDQ3LjM2ODMyNzMgQzEwLjExMDg3MDgsNDcuNTkzODEwOCAxMC40MTExMjQsNDcuNzA5OTUxOCAxMC43MTI1MTQ2LDQ3LjcwOTk1MTggQzExLjAwNzA4MTEsNDcuNzA5OTUxOCAxMS4zMTE4ODM2LDQ3LjU5MzgxMDggMTEuNTM4Nzc5NSw0Ny4zNjgzMjczIEwyMC41NjE3Mjk0LDM4LjM4MzU0OCBDMjAuNzc3ODIwNywzOC4xNjA4OTczIDIwLjkwNTIwMDgsMzcuODcwODI4MSAyMC45MDUyMDA4LDM3LjU1NjM5NzYgQzIwLjkwNTIwMDgsMzcuMjQ1OTMzIDIwLjc3NzgyMDcsMzYuOTUxODk4IDIwLjU2MTcyOTQsMzYuNzMyNjQ2NSBMMTEuNTM4Nzc5NSwyNy43NDc4NjcyIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMwMEE5RTAiPjwvcGF0aD4NCiAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzguNTY2MTczNiwwLjgyMzM4NTk0IEMzOC4xMDg5Njk5LDAuMzY3ODg2NjkyIDM3LjM2NDU5MjIsMC4zNjUwNTM5ODUgMzYuOTA5NjYzMSwwLjgyMzM4NTk0IEwyNy44OTI5Njg1LDkuODA5Mjk4MzUgQzI3LjQzNTE5NjEsMTAuMjYxOTY0OSAyNy40MzUxOTYxLDExLjAwMTg2NzkgMjcuODkyOTY4NSwxMS40NTg1MDAyIEwzNi45MDk2NjMxLDIwLjQ0MzI3OTUgQzM3LjEzNTk5MDMsMjAuNjY5ODk2MSAzNy40MzUxMDYyLDIwLjc4MjYzNzggMzcuNzM2NDk2NywyMC43ODI2Mzc4IEMzOC4wMzU2MTI2LDIwLjc4MjYzNzggMzguMzM1Mjk3MSwyMC42Njk4OTYxIDM4LjU2NjE3MzYsMjAuNDQzMjc5NSBMNDcuNTgyODY4MiwxMS40NTg1MDAyIEM0Ny44MDE4MDI4LDExLjIzNDcxNjQgNDcuOTI1MjAyNCwxMC45NDQ2NDcyIDQ3LjkyNTIwMjQsMTAuNjMxOTE2NCBDNDcuOTI1MjAyNCwxMC4zMTk3NTIxIDQ3LjgwMTgwMjgsMTAuMDI5MTE2NCA0Ny41ODI4NjgyLDkuODA1MzMyNTYgTDM4LjU2NjE3MzYsMC44MjMzODU5NCBaIiBpZD0iRmlsbC01IiBmaWxsPSIjRDYxODE4Ij48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzMDA3OSwwLjc5NjgxNTE1IEMxMS4wNzQ1ODEzLDAuMzQzMDE1NTI2IDEwLjMyNjc5MTYsMC4zNDMwMTU1MjYgOS44NzU4NDMxLDAuNzk2ODE1MTUgTDAuODU3NDQyNTQ2LDkuNzgxMDI3OTMgQzAuMzk3Mzk1NTAzLDEwLjIzNjUyNzIgMC4zOTczOTU1MDMsMTAuOTc2NDMwMiAwLjg1NzQ0MjU0NiwxMS40MzMwNjI1IEw5Ljg3NTg0MzEsMjAuNDE2MTQyMiBDMTAuMTAyMTcwMywyMC42NDcyOTExIDEwLjQwMTI4NjIsMjAuNzU3MjAwMSAxMC43MDI2NzY3LDIwLjc1NzIwMDEgQzExLjAwNDA2NzIsMjAuNzU3MjAwMSAxMS4zMDMxODMxLDIwLjY0NzI5MTEgMTEuNTMwMDc5LDIwLjQxNjE0MjIgTDIwLjU0NjIwNDksMTEuNDMzMDYyNSBDMjAuNzY3NDE0MiwxMS4yMTI2Nzc5IDIwLjg5NDIyNTcsMTAuOTIxNDc1NyAyMC44OTQyMjU3LDEwLjYwNDc3OTEgQzIwLjg5NDIyNTcsMTAuMjk0MzE0NCAyMC43Njc0MTQyLDEwLjAwMjU0NTYgMjAuNTQ2MjA0OSw5Ljc4MTAyNzkzIEwxMS41MzAwNzksMC43OTY4MTUxNSBaIiBpZD0iRmlsbC03IiBmaWxsPSIjN0FCODAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM4LjYzMTY4MzQsMjcuNzk4ODU1OSBDMzguMTc0NDc5NywyNy4zNDIyMjM2IDM3LjQyOTUzMzMsMjcuMzQyMjIzNiAzNi45NzM0NjY5LDI3Ljc5ODg1NTkgTDI3Ljk1OTYxNTYsMzYuNzgwMjM2IEMyNy41MDE4NDMyLDM3LjIzODAwMTQgMjcuNTAxODQzMiwzNy45NzUwNzE3IDI3Ljk1OTYxNTYsMzguNDMxMTM3NSBMMzYuOTczNDY2OSw0Ny40MTgxODMgQzM3LjIwMDM2MjgsNDcuNjQ0Nzk5NSAzNy40OTg5MSw0Ny43NTk4MDc0IDM3LjgwMDg2OTIsNDcuNzU5ODA3NCBDMzguMTAyMjU5Nyw0Ny43NTk4MDc0IDM4LjM5ODUzMjMsNDcuNjQ0Nzk5NSAzOC42MzE2ODM0LDQ3LjQxODE4MyBMNDcuNjQ4Mzc4LDM4LjQzMTEzNzUgQzQ3Ljg2Njc0NCwzOC4yMTE4ODYgNDcuOTkxODQ5NSwzNy45MTg5ODQxIDQ3Ljk5MTg0OTUsMzcuNjA5MDg2IEM0Ny45OTE4NDk1LDM3LjI5NTc4ODYgNDcuODY2NzQ0LDM3LjAwNDU4NjQgNDcuNjQ4Mzc4LDM2Ljc4MDIzNiBMMzguNjMxNjgzNCwyNy43OTg4NTU5IFoiIGlkPSJGaWxsLTkiIGZpbGw9IiMwMDQ2QUQiPjwvcGF0aD4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg" style="position: relative; border: none; display: inline; cursor: default; padding: 0px; margin: 0px; pointer-events: auto; left: 333px; top: 11px; width: 16px; height: 16px; min-width: 16px; max-width: 16px; min-height: 16px; max-height: 16px;"></div><input type="email" class="form-control confondo" id="email" name="email" placeholder="name@example.com">
                        </div>  

                        <div class="mb-3 ">
                          <div style="position: absolute; display: flex; width: 0px; height: 0px; border: none; padding: 0px; margin: 0px; background: no-repeat; visibility: visible; user-select: none; pointer-events: none; z-index: auto; opacity: 1;"><img title="Required" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjQ4cHgiIGhlaWdodD0iNDhweCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDxkZWZzPjwvZGVmcz4NCiAgICA8ZyBpZD0iSWNvbnMtKy1sb2dvIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHlsZT0ib3BhY2l0eTowLjU0Ij4NCiAgICAgICAgPGcgaWQ9IlNQLUxvZ28iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02MS4wMDAwMDAsIC02MS4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJzeW1ib2wiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYxLjAwMDAwMCwgNjEuMDAwMDAwKSI+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM0LjA4MTAyNzcsMjMuMjYyODk5MiBMMjUuMDYyMDU4NSwxNC4yNzg2ODY0IEMyNC42MDQyODYxLDEzLjgyNDMyMDMgMjMuODYzODg5LDEzLjgyNDMyMDMgMjMuNDA0NDEwNywxNC4yNzg2ODY0IEwxNC4zODc3MTYxLDIzLjI2Mjg5OTIgQzEzLjkyNzY2OSwyMy43MTc4MzE5IDEzLjkyNzY2OSwyNC40NTYwMzUzIDE0LjM4NzcxNjEsMjQuOTEyNjY3NiBMMjMuNDA0NDEwNywzMy44OTYzMTM5IEMyMy42MzE4NzUyLDM0LjEyNDA2MzUgMjMuOTMxNTU5NywzNC4yMzc5MzgzIDI0LjIzMjk1MDMsMzQuMjM3OTM4MyBDMjQuNTMyMDY2MSwzNC4yMzc5MzgzIDI0LjgzNTE2MjYsMzQuMTI0MDYzNSAyNS4wNjIwNTg1LDMzLjg5NjMxMzkgTDM0LjA4MTAyNzcsMjQuOTEyNjY3NiBDMzQuMjk5MzkzNywyNC42OTA1ODM0IDM0LjQyMjc5MzIsMjQuMzk4MjQ4MSAzNC40MjI3OTMyLDI0LjA4ODM1IEMzNC40MjI3OTMyLDIzLjc3NjE4NTcgMzQuMjk5MzkzNywyMy40ODA0NTExIDM0LjA4MTAyNzcsMjMuMjYyODk5MiBaIiBpZD0iRmlsbC0xIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzODc3OTUsMjcuNzQ3ODY3MiBDMTEuMDgxMDA3MSwyNy4yOTQ2MzQxIDEwLjM0MDYxLDI3LjI5NDYzNDEgOS44ODI4Mzc2MywyNy43NDc4NjcyIEwwLjg2Nzg0OTA0OSwzNi43MzI2NDY1IEMwLjQwODM3MDY2OCwzNy4xODgxNDU4IDAuNDA4MzcwNjY4LDM3LjkyNTIxNjEgMC44Njc4NDkwNDksMzguMzgzNTQ4IEw5Ljg4MjgzNzYzLDQ3LjM2ODMyNzMgQzEwLjExMDg3MDgsNDcuNTkzODEwOCAxMC40MTExMjQsNDcuNzA5OTUxOCAxMC43MTI1MTQ2LDQ3LjcwOTk1MTggQzExLjAwNzA4MTEsNDcuNzA5OTUxOCAxMS4zMTE4ODM2LDQ3LjU5MzgxMDggMTEuNTM4Nzc5NSw0Ny4zNjgzMjczIEwyMC41NjE3Mjk0LDM4LjM4MzU0OCBDMjAuNzc3ODIwNywzOC4xNjA4OTczIDIwLjkwNTIwMDgsMzcuODcwODI4MSAyMC45MDUyMDA4LDM3LjU1NjM5NzYgQzIwLjkwNTIwMDgsMzcuMjQ1OTMzIDIwLjc3NzgyMDcsMzYuOTUxODk4IDIwLjU2MTcyOTQsMzYuNzMyNjQ2NSBMMTEuNTM4Nzc5NSwyNy43NDc4NjcyIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMwMEE5RTAiPjwvcGF0aD4NCiAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzguNTY2MTczNiwwLjgyMzM4NTk0IEMzOC4xMDg5Njk5LDAuMzY3ODg2NjkyIDM3LjM2NDU5MjIsMC4zNjUwNTM5ODUgMzYuOTA5NjYzMSwwLjgyMzM4NTk0IEwyNy44OTI5Njg1LDkuODA5Mjk4MzUgQzI3LjQzNTE5NjEsMTAuMjYxOTY0OSAyNy40MzUxOTYxLDExLjAwMTg2NzkgMjcuODkyOTY4NSwxMS40NTg1MDAyIEwzNi45MDk2NjMxLDIwLjQ0MzI3OTUgQzM3LjEzNTk5MDMsMjAuNjY5ODk2MSAzNy40MzUxMDYyLDIwLjc4MjYzNzggMzcuNzM2NDk2NywyMC43ODI2Mzc4IEMzOC4wMzU2MTI2LDIwLjc4MjYzNzggMzguMzM1Mjk3MSwyMC42Njk4OTYxIDM4LjU2NjE3MzYsMjAuNDQzMjc5NSBMNDcuNTgyODY4MiwxMS40NTg1MDAyIEM0Ny44MDE4MDI4LDExLjIzNDcxNjQgNDcuOTI1MjAyNCwxMC45NDQ2NDcyIDQ3LjkyNTIwMjQsMTAuNjMxOTE2NCBDNDcuOTI1MjAyNCwxMC4zMTk3NTIxIDQ3LjgwMTgwMjgsMTAuMDI5MTE2NCA0Ny41ODI4NjgyLDkuODA1MzMyNTYgTDM4LjU2NjE3MzYsMC44MjMzODU5NCBaIiBpZD0iRmlsbC01IiBmaWxsPSIjRDYxODE4Ij48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzMDA3OSwwLjc5NjgxNTE1IEMxMS4wNzQ1ODEzLDAuMzQzMDE1NTI2IDEwLjMyNjc5MTYsMC4zNDMwMTU1MjYgOS44NzU4NDMxLDAuNzk2ODE1MTUgTDAuODU3NDQyNTQ2LDkuNzgxMDI3OTMgQzAuMzk3Mzk1NTAzLDEwLjIzNjUyNzIgMC4zOTczOTU1MDMsMTAuOTc2NDMwMiAwLjg1NzQ0MjU0NiwxMS40MzMwNjI1IEw5Ljg3NTg0MzEsMjAuNDE2MTQyMiBDMTAuMTAyMTcwMywyMC42NDcyOTExIDEwLjQwMTI4NjIsMjAuNzU3MjAwMSAxMC43MDI2NzY3LDIwLjc1NzIwMDEgQzExLjAwNDA2NzIsMjAuNzU3MjAwMSAxMS4zMDMxODMxLDIwLjY0NzI5MTEgMTEuNTMwMDc5LDIwLjQxNjE0MjIgTDIwLjU0NjIwNDksMTEuNDMzMDYyNSBDMjAuNzY3NDE0MiwxMS4yMTI2Nzc5IDIwLjg5NDIyNTcsMTAuOTIxNDc1NyAyMC44OTQyMjU3LDEwLjYwNDc3OTEgQzIwLjg5NDIyNTcsMTAuMjk0MzE0NCAyMC43Njc0MTQyLDEwLjAwMjU0NTYgMjAuNTQ2MjA0OSw5Ljc4MTAyNzkzIEwxMS41MzAwNzksMC43OTY4MTUxNSBaIiBpZD0iRmlsbC03IiBmaWxsPSIjN0FCODAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM4LjYzMTY4MzQsMjcuNzk4ODU1OSBDMzguMTc0NDc5NywyNy4zNDIyMjM2IDM3LjQyOTUzMzMsMjcuMzQyMjIzNiAzNi45NzM0NjY5LDI3Ljc5ODg1NTkgTDI3Ljk1OTYxNTYsMzYuNzgwMjM2IEMyNy41MDE4NDMyLDM3LjIzODAwMTQgMjcuNTAxODQzMiwzNy45NzUwNzE3IDI3Ljk1OTYxNTYsMzguNDMxMTM3NSBMMzYuOTczNDY2OSw0Ny40MTgxODMgQzM3LjIwMDM2MjgsNDcuNjQ0Nzk5NSAzNy40OTg5MSw0Ny43NTk4MDc0IDM3LjgwMDg2OTIsNDcuNzU5ODA3NCBDMzguMTAyMjU5Nyw0Ny43NTk4MDc0IDM4LjM5ODUzMjMsNDcuNjQ0Nzk5NSAzOC42MzE2ODM0LDQ3LjQxODE4MyBMNDcuNjQ4Mzc4LDM4LjQzMTEzNzUgQzQ3Ljg2Njc0NCwzOC4yMTE4ODYgNDcuOTkxODQ5NSwzNy45MTg5ODQxIDQ3Ljk5MTg0OTUsMzcuNjA5MDg2IEM0Ny45OTE4NDk1LDM3LjI5NTc4ODYgNDcuODY2NzQ0LDM3LjAwNDU4NjQgNDcuNjQ4Mzc4LDM2Ljc4MDIzNiBMMzguNjMxNjgzNCwyNy43OTg4NTU5IFoiIGlkPSJGaWxsLTkiIGZpbGw9IiMwMDQ2QUQiPjwvcGF0aD4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg" style="position: relative; border: none; display: inline; cursor: default; padding: 0px; margin: 0px; pointer-events: auto; left: 333px; top: 11px; width: 16px; height: 16px; min-width: 16px; max-width: 16px; min-height: 16px; max-height: 16px;"></div><input type="text" class="form-control confondo" id="phone" name="phone" placeholder="Phone number">
                        </div>
                        <div class="mb-3 ">
                          <input type="text" class="form-control confondo" id="comment1" name="comment1" placeholder="State">
                        </div>   
                    <h5><a aria-hidden="true"><span class="icon icon-link"></span></a>Write a note for us, and we will find the best service for you</h5>                  
                        <div class="mb-3">
                          <textarea class="form-control confondo" id="message" name="message" placeholder="Message" rows="3"></textarea>
                        </div>     

                        <div class="mb-3">
                          <button type="submit" class="btn-secondary btn-block"><span class="text-white">Submit</span></button>
                        </div>                                                                                                                            
                    
                <!--div class="to_right">
                    <a href="" class="btn-secondary btn-margin-auto mb-2 mb-3">Submit</a>                                        
                </div-->
                    
            </div>
          </form>
        </div>
    </div>
</div>
<!--Order create modal End-->

      <!-- modal gracias-->
<div class="modal gracias_next " id="graciasModal"   tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-md" >
        <div class="modal-content" >

          <!-- body modal-->
            <div class="modal-body">
                <h2><a aria-hidden="true"><span class="icon icon-link"></span></a>Thank you!</h2>  

                <h4><a aria-hidden="true"><span class="icon icon-link"></span></a>An agent will be contacting you soon!</h4>                  

                <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/home.php">Go to Septic Works LLC</a>
                    
            </div>
        </div>
    </div>
</div>
<!--Order create modal End-->

      
  
</div>
</div>
    

<?php 
include "footer.php";
?>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    
<script>
    function imagen_req(){       

        $("#test").css("opacity", 0.2);
        $("#faq").css("opacity", 0.2);        
        $("#cab").css("opacity", 0.2);                
    }                          
</script>

<script>
    function icarga(){       

        location.reload();
    }                          
</script>

  </body>
</html>