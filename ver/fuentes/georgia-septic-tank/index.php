<!DOCTYPE html>
<html lang="en">
<head>
<meta name="description" content="Got Georgia septic tank? Buying a new house with a Georgia septic tank shouldn't be a worry. Read on to know all you need to know.">    
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3">Georgia Septic Tank: 8 Things to Know Before Buying a House</h1>
      <p class="article-info">
            <span class="mb-0 text-center"></span>
            <span class="mb-0 text-center">10 min read</span>
          </p>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-1.gif);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(../page-septic/img/blog-header-2.gif);"></div>        
  </div>
</header>
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h2><a aria-hidden="true"><span class="icon icon-link"></span></a>Want to buy a house but it has a Georgia septic tank?</h2>
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Don't worry! Through this article, we'll inform you about all that you need to know</h3>        
            <p class="text-justify">Buying a house with a Georgia septic tank should not be a worry. If you're getting your house ready to sell, there are some items you should inspect before you put it up for sale. Likewise, if you are looking to buy a property, you will have to do more than just checking out how the home looks like on the inside.</p>        
            <p class="text-justify">At Septic Works LLC, we do septic tank inspection before buying a house. We also offer a range of septic tank services including septic tank pumping, septic tank repair, septic tank cleaning, septic tank installation, engineered septic system, alternative septic systems, and porta potti (portable toilets).</p>                
            <p class="text-justify">We are licensed and certified by the State of Georgia to provide a septic certification for any real estate septic system inspection - our septic tank inspectors will take care of all your needs.</p>        
            <p class="text-justify">Our operations cover Atlanta, Savannah, Augusta, Macon, Warner Robins, Columbus, Dalton and all over Georgia.</p>                
      </div>
    </div>
  </div>            
            

<div class="block-1">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">        
            <div class="col-lg-8 mb-4 mbl-md-0">
                  <div class="block-12-card-1__content w-100">
                        <div><img class="img-fluid w-100" src="../page-septic/img/for-sale.jpg" alt="For Sale" ></div>
                        <p class="text-justify">A house for sale with a Georgia septic tank.</p>
                  </div>
            </div>               
    </div>
  </div>
</div>
    
            
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>1. Should I Get My Georgia Septic Tank Inspected?</h3>           
            <p class="text-justify">One thing many Georgian sellers and buyers overlook is completing a septic tank inspection for their Georgia septic tank. If you are a seller, you surely want to present a septic certification to the prospective buyer to avoid any potential issues of liability in the future from a malfunctioning septic system.</p>                        
        
        <!-- Page 2 -->
            <p class="text-justify">On the other hand, if you are a buyer, you certainly don't want any unwelcome surprises after you close your property purchase. You don't want to buy the house of your dreams to only find out later there are sewage backups or clogged pipes in the septic system or you need to arrange for a septic tank pumping at a short notice.</p>

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>2. Hire the Best Septic Tank Inspector</h3>                   
            <p class="text-justify">So, what should you do to give yourself peace of mind? Whether a buyer or seller, talk to a professional septic tank inspector who knows the ins and outs of the septic system. Schedule an appointment with a septic inspector to come to your house and check for any problems or defects that need correction.</p>        
            <p class="text-justify">The septic tank inspector will test each part of your septic system, including the septic tank, leach field, and soil. This way the septic tank inspection process will help you to identify any leaks or issues and thus save you time and money spent on costly repairs.</p>        
            <p class="text-justify">Not anybody can do a septic inspection. It is only carried out by certified and licensed septic inspectors. On top of this, septic inspection requirements vary by state, so you want to make sure you hire a septic inspector who is experienced with a vast knowledge.</p>                
            <p class="text-justify">You can find further details about Georgia septic tank laws on the website of the <a href="https://dph.georgia.gov/search?search=onsite+sewage&sm_site_name=dph" target="_blank">Georgia Department of Public Health.</a></p>                            
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>3. Georgia Septic Tank: Types of Septic Tank Inspection</h3>                               
            <p class="text-justify">At Septic Works LLC, our experienced septic tank inspectors perform all sorts of checks for any residential, commercial, or municipal clients and any type of Georgia septic tank:</p>                
            
        <!-- Page 3 -->
        
        <p class="text-justify"><strong>1. Georgia septic tank inspection level 1:</strong> This is a visual inspection that involves running water from various drains and flushing toilets in the home. The septic tank inspector is looking at how well everything is draining. The visual inspection also includes looking for septic leakage to the top of the drain field area. We also check for sewage odors in the area of the septic tank and the leach field. This inspection can help identify problems but a level two can give our septic inspectors a better idea on issues with your septic system.</p>        
        <p class="text-justify"><strong>2. Georgia septic tank inspection level 2:</strong> We do a septic tank pumping. We remove the cover of the septic tank. This allows our septic tank inspectors to check the water level. Water levels determine whether water is properly draining. To make sure the water is properly flowing, our septic inspectors will run water in the home. This is done to determine if the water level rises when more water is introduced. Next, we will check out if your septic system has backflow from the absorption area. Backflow will let us know if there is a problem with the leach field.</p>            
        <p class="text-justify"><strong>3. Georgia septic tank inspection level 3:</strong> We highly recommend you choose this option when you are purchasing a new home. The level three inspection includes level one and level two inspections. The difference with level three is that baffles are installed. In many states, this is now mandated by the Department of Environmental Health. Septic tank baffles are installed in two separate locations. There is an inlet baffle and an outlet baffle. The inlet baffle allows water to flow into your septic system without disturbing the scum layer. This baffle also keeps the wastewater from flowing straight across your Georgia septic tank. It guides it to flow down, across and then up. The outlet baffle serves as a filter to retain solids from traveling to the leach field. Having this baffle is essential to avoid clogs that could destroy your drain line.</p>            

        <!-- Page 4 -->
            <p class="text-justify">After completing our septic system inspection, you will get to know all the following details: </p>                        
            <ul><li>The type of septic system you have</li></ul>
            <ul><li>The condition of the septic tank</li></ul>
            <ul><li>The condition of the leach field</li></ul>
            <ul><li>The location of the septic tank and drain field if previously unknown</li></ul>
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>4. Georgia Septic Tank: Who Pays for Septic Certification?</h3>                               
            <p class="text-justify">If you are buying a house with a Georgia septic tank, you are probably wondering who pays for the septic inspection? Buyer or seller? It is typically the responsibility of the seller to pay for the septic inspection. Or they both sit down and discuss who takes charge of it and it could be both or just one of them.</p>                
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>5. Can You Inspect a Georgia Septic Tank Without Pumping It?</h3>                               
            <p class="text-justify">We'll decide about this during our septic tank inspection. On a few occasions, the realtor or the house owner will arrange for a septic tank pumping just before a septic inspection. Be careful! Pumping a septic tank before a septic inspection means that the leach field cannot be tested. This can hide issues the septic system may have.</p>                
            
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>6. How Often Should Septic Tanks Be Inspected?</h3>                               
        <p class="text-justify">According to the <a href="https://www.epa.gov/septic/how-care-your-septic-system" target="_blank">United States Environmental Protection Agency</a>, average household septic systems should be inspected at least once every three years by a professional septic inspector. However, septic tank pumping is typically every three to five years.</p>                

        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>7. Georgia Septic Tank Components</h3> 
        
        <!-- Page 5 -->        
        <p class="text-justify">Remember that your septic system is buried underground and not visible to your naked eye. It is a structure that relies on wastewater technology and natural resources to treat sewerage that goes out of your household toilets and drains. Then it releases it to the underground water under your property.</p>                
        

      </div>
    </div>
  </div>            
 
<div class="block-1">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">        
            <div class="col-lg-8 mb-4 mbl-md-0">
                  <div class="block-12-card-1__content w-100">
                    <div><img class="img-fluid w-100" src="../page-septic/img/grafico-post.jpg" alt="Grafico" ></div>
                    <p class="text-justify">A house with a Georgia septic tank and leach field.</p>
                  </div>
            </div>               
    </div>
  </div>
</div>    
    
    
        
 <div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">
               
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Any septic system mainly has four units:</h3>                  
            <p class="text-justify">1. A pipe that connects your house with the septic tank.</p>        
            <p class="text-justify">2. A septic tank or a buried container. For Georgia septic tanks, the material is concrete, fiberglass, or polyethylene. The job of a septic tank is important to the whole functionality of your septic system. All the wastewater and solids that leave your house end up in the septic tank via the connecting pipe. The septic tank ensures that the wastewater inside stays in place until solids have settled in the bottom of the tank to form something known as sludge. Oil and grease float to form a top layer known as scum. Once settled, it releases only the wastewater through the septic tank outlet baffle to the drain field.</p>        
            <p class="text-justify">3. A drain field/leach field receives only the wastewater from the septic tank via the effluent filters. Further treatment in a series of perforated (with holes) pipes takes place before it filters through the soil.</p>        
            <p class="text-justify">4. Soil (natural resource) offers the last stage of treatment. It removes any remaining bacteria or solids before wastewater enters the groundwater.</p>                
            
        <!-- Page 6 -->               
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>8. Georgia Septic Tank Laws</h3>                               
            <ul><li>According to the <a href="https://dph.georgia.gov/search?search=Guide+to+Septic+Tanks+-+Georgia+Department+of+Public+Health&sm_site_name=dph" target="_blank">Georgia Department of Public Health</a>, the number of bedrooms determines the size of the Georgia septic tank.</li></ul>
            <ul><li>Any septic tank must have a capacity of at least 1000 gallons to serve 3 or 4 bedrooms.</li></ul>            
            <ul><li>All septic tanks must have an outlet baffle. It should be serviced on a timely basis</li></ul>
            <ul><li>You are not allowed to discharge the effluent or the content of your septic tank in into a road, street, gutter, ditch, water course, or the surface of the ground.</li></ul>                        
            <ul><li>Septic tanks should be inspected by a licensed septic inspector / professional.</li></ul>
            <ul><li>Georgia septic tanks can only be accessed for cleaning or inspection through the cover or the manhole.</li></ul>
            <ul><li>Septic tanks have to be watertight and built against corrosion.</li></ul>
            <ul><li>It is the home owner's responsibility to implement a septic system maintenance.</li></ul> 
            
            
            
      </div>
    </div>
  </div>            
 
<div class="block-1">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">        
            <div class="col-lg-8 mb-4 mbl-md-0">
                  <div class="block-12-card-1__content w-100">
                    <div><img class="img-fluid w-100" src="../page-septic/img/edificio-post.jpg" alt="Grafico" ></div>
                    <p class="text-justify">Georgia septic tank laws.</p>
                  </div>
            </div>               
    </div>
  </div>
</div>    
    
    
        
 <div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">                                                
        <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>Work with the Best Georgia Septic Tank Inspector</h3>                               
        
        <!-- Page 7 -->               
            <p class="text-justify">At Septic Works LLC, we specialize in all things that are septic. We have been serving clients across the US for 20 years including Georgia. We are fully licensed and insured.</p>                
            <p class="text-justify">Our prices are competitive and our specialized services are sought by residential, commercial and municipal clients. Our septic inspectors provide an easy and frictionless experience.</p>                            
            <p class="text-justify">Our customer service agents are available anytime you need any help. We are friendly, knowledgeable, and quick to respond to any questions or comments you have. If you have any doubts about our septic tank services, call us at (678) 326-3591.</p>            
        
      </div>
    </div>
  </div>



<?php 
include "footer.php";
?>
</body>

</html>


