<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="About us - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->    
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3"><span class="highlight">About.</span> Our Company</h1>
      <div class="hero__btns-container"><a class="hero__btn btn btn-primary mb-2 mb-lg-0 mx-1 mx-lg-2" href="services.php">View our services</a>
      </div>
    </div>
  </div>
    
    <div class="row hero__video-container">
            <div class="block-31__image-column container col-lg-6" style="background-image: url(img/about-header-2.jpg);"></div>
            <div class="block-31__image-column container col-lg-6" style="background-image: url(img/about-header-1.jpg);"></div>
    </div>
    
</header>
<div class="block-12 space-between-blocks">
  <div class="container">
    <div class="px-2 px-lg-0">
      <div class="block-12-card-1 row justify-content-center px-2">
          
        <div class="col-lg-6 mb-6mbl-md-0">
          <div class="block-12-card-1__content w-100">
            <h2 class="block__title mb-3">Is a Septic Tank Inspection Necessary?</h2>
            <p class="text-justify">We are Septic Works LLC and we have been providing customers with septic tank inspection, septic tank repair, septic tank pumping, septic tank installation, engineered septic system, alternative septic systems and porta potti for 20 years. </p>
            <p class="text-justify">The high-quality job our professional septic inspectors do protect public health and the environment. Early septic tank inspections mean less probability of contaminated water, avoiding your children and family from any diseases, as well as protecting the greenery around your house.</p>
          </div>
        </div>
          
        <div class="col-lg-6 mb-6 mbl-md-0">
          <div class="block-12-card-1__content w-100">
          <div> <img class="img-fluid w-100" src="img/about-us.jpg" alt="about us" ></div>
          </div>
        </div>
          
      </div>
    </div>
  </div>
  <div class="block-12__shape-for-background"></div>
</div>
<div class="block-1 space-between-blocks pt-3">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">
    <div class="col-lg-6 mb-4 mbl-md-0">
                  <br>
                  <br>
          <div class="block-12-card-1__content w-100">
            <div><img class="img-fluid w-100" src="img/septic-works-02.jpg" alt="about us" >
            </div>
          </div>
    </div>
      <div class="col-lg-4 col-xl-6 px-lg-5 mb-4 mbl-lg-0">
        <h2 class="block__title mb-3">Septic Inspectors with Long History</h2>
        <p class="text-justify">Our licensed and insured septic tank inspectors have served thousands of clients including homeowners, prospective buyers, realtors and home inspectors. At Septic Works LLC, we base our septic inspection services around the quality, respect, honesty, and professionalism you deserve. </p>
        <p class="text-justify">We are a local company that started in Georgia and then moved on to operate in Florida, South Carolina and Tennessee as well. We're expanding our septic inspection operations to serve clients in North Carolina, Alabama, Kentucky, Texas and Mississippi as well as other states across the country. Our future goal is to be the first and best national septic inspector across the United States.</p>
      </div>
    </div>
  </div>
</div>

<div class="block-1 space-between-blocks pt-3">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-lg-4 col-xl-6 px-lg-5 mb-4 mbl-lg-0">
            <h2 class="block__title mb-3">Hire the Best Septic Inspection Service</h2>
            <p class="text-justify">We know that buying a home is an expensive decision and a septic system plays a vital role in the overall performance of your plumbing system. Since the system is buried underground, you need a professional septic tank inspection to thoroughly assess its condition.</p>
            <p class="text-justify">Our septic inspectors make this stressful and complicated process easy for you. We examine your house, the yard, the septic tank, and the leach field. After that is done, we inform you about any problems the system has along with the possible solutions and cost estimates.</p>
            <p class="text-justify font-weight-bold">Need help with selling or buying a house with a septic system? Call us at ((912) 666-2210.</p>
        </div>
        <div class="col-lg-6 mb-4 mbl-md-0">
          <div class="block-12-card-1__content w-100">
            <div><img class="img-fluid w-100" src="img/septic-works-01.jpg" alt="about us" >
            </div>
          </div>
    </div>
    </div>
  </div>
</div>


<?php 
include "footer.php";
?>
</body>

</html>