<div class="block-41 py-3 footer-style">
  <div class="container">
    <div class="row px-2">
      <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0"><img class="footer_logo mb-3" src="img/septiclogo.png">
        <p class="block-41__brand-story mb-0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Quality, Honesty, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Professionalism.</p>
      </div>
      <div class="col-md-3 col-lg-3 mb-sm-4 mb-lg-0">
        <ul class="block-41__links-list list-unstyled p-0">
          <li class="block-41__li-heading">Navigation</li>
          <li class="block-41__li"><a href="home.php" class="block-41__li-link">Home</a></li>
          <li class="block-41__li"><a href="about-us.php" class="block-41__li-link">About us</a></li>
          <li class="block-41__li"><a href="faq.php" class="block-41__li-link">F.A.Q.</a></li>
          <li class="block-41__li"><a href="blog.php" class="block-41__li-link">Blog</a></li>          
          <li class="block-41__li"><a href="policy.php" class="block-41__li-link">Privacy Policy</a></li>                    
        </ul>
      </div>
      <div class="col-md-3 col-lg-3 mb-sm-4 mb-lg-0">
        <ul class="block-41__links-list list-unstyled p-0">
          <li class="block-41__li-heading">Services</li>
          <li class="block-41__li"><a href="services.php#services" class="block-41__li-link">For Buyer</a></li>
          <li class="block-41__li"><a href="services.php#services" class="block-41__li-link">For Homeowner</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-lg-3 mb-sm-4 mb-lg-0">
        <ul class="block-41__links-list list-unstyled p-0">
          <li class="block-41__li-heading">Contact Information</li>
          <li class="block-41__li"><a href="mailto:info@septicworksllc.com" class="block-41__li-link">info@septicworksllc.com</a></li>
          <li class="block-41__li"><a href="tel:(912) 666-2210 Georgia" class="block-41__li-link">(912) 666-2210 GA</a></li>                    
          <li class="block-41__li"><a href="tel:(843) 300-0318 SC" class="block-41__li-link">(843) 300-0318 S.C</a></li>
          <li class="block-41__li"><a href="tel:(843) 300-0318 SC" class="block-41__li-link">(505) 318 0739 N.M</a></li>          
          <li class="block-41__li"><a href="https://goo.gl/maps/sd87eDCQ4vc2Ssbu8" class="block-41__li-link">699 Trask Parkway Seabrook, S.C. 29940</a></li>
        </ul>
      </div>
    </div>
  </div>
  <hr class="block-41__divider">
  <div class="container">
    <div class="row px-2">
      <div class="flex-grow-1">
        <ul class="block-41__extra-links d-flex list-unstyled p-0">
          <li class="mx-2"><a href="https://www.facebook.com/SepticTankInspection" class="block-41__extra-link m-0"><i class="fab fa-facebook"></i></a></li>
          <li class="mx-2"><a href="https://instagram.com/septicworksllcofficial?utm_medium=copy_link" class="block-41__extra-link m-0"><i class="fab fa-instagram"></i></a></li>          
          <li class="mx-2"><a href="mailto:info@septicworksllc.com" class="block-41__extra-link m-0"><i class="fas fa-envelope"></i></a></li>
        </ul>
      </div>
      <p class="block-41__copyrights">© 2021 Septic Works. All Rights Reserved.</p>
    </div>
  </div>
</div>
<script src="https://unpkg.com/tua-body-scroll-lock"></script>
<script src="js/script.js"></script>
