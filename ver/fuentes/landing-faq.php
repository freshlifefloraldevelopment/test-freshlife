<div id="tab-1" class="tab">
        <h3>1) Is a septic inspection really necessary?</h3>
</div>
<div class="tab-content">
    <p>A septic system is an important component of your home. Although septic tank inspection is not a legal requirement, it is highly recommended that you arrange for regular checks. An inspection will benefit you for the following reasons:</p>
    <p><strong>If you are a buyer, you want to:</strong></p>
    <p>1) Know the condition of the new home septic system.</p>
    <p>2) Know the location of the septic tank and drainage field.</p>
    <p>3) Ensure your next home is in excellent condition.</p>
    <p>4) Renegotiate the property price if the septic system needs repairs or replacements.</p>
    <p><strong>If you are a homeowner, you want to:</strong></p>
    <p>1) Ensure your septic system is fully functional and suitable for expansion if you are remodeling your house.</p>
    <p>2) Avoid any potential issues of liability in the future from a malfunctioning septic system in case you sell your house.</p>
    <p>3) Prevent unpleasant odors, flushing problems, or system malfunctions, etc.</p>
</div>

<div id="tab-1" class="tab">
        <h3>2) Who does septic tank inspection?</h3>
</div>
<div class="tab-content">
    <p>Not anybody can do a septic inspection. Only certified and licensed septic inspectors can carry it out. On top of this, septic tank inspection requirements vary by state, so make sure you hire a septic inspector who has all the experience and the know-how.</p>
</div>

<div id="tab-1" class="tab">
        <h3>3) Can you inspect a septic tank without pumping it?</h3>
</div>
<div class="tab-content">
    <p>During our septic tank inspection, we will learn whether or not your septic tank needs to be pumped</p>
</div>

<div id="tab-1" class="tab">
        <h3>4) How often should I have my septic tank inspected?</h3>
</div>
<div class="tab-content">
    <p>According to the&nbsp;<a href="https://www.epa.gov/septic/how-care-your-septic-system">United States Environmental Protection Agency</a>, average household septic systems should be inspected at least once every three years by a professional septic inspector. However, septic tanks are typically pumped every three to five years.</p>
</div>

<div id="tab-1" class="tab">
        <h3>5) What are the signs of a full septic tank?</h3>
</div>
<div class="tab-content">
    <p>-Slow draining or flushing problems</p>
    <p>-Unpleasant odors</p>
    <p>-Surprisingly lush and green lawn over the septic system</p>
    <p>-Pools of water around your yard</p>
    <p>-Sewage backing up into your home</p>
    <p>-Pipes producing gurgling sounds</p>
</div>

<div id="tab-1" class="tab">
        <h3>6) How long does it take to perform a septic inspection?</h3>
</div>
<div class="tab-content">
    <p>Every septic inspection is different from the other. Time varies depending on the complexity and on-the-spot findings. Usually, septic inspections can take anywhere from forty-five minutes to three hours.</p>
</div>

<div id="tab-1" class="tab">
        <h3>7) How long do septic tanks last?</h3>
</div>
<div class="tab-content">
    <p>The life expectancy of a septic system varies largely from 15 to 40 years. It depends on how well you maintain your septic system.</p>
</div>

<div id="tab-1" class="tab">
        <h3>8) How do you get a septic system to pass inspection?</h3>
</div>
<div class="tab-content">
    <p>-Pump out your septic tank every 2-3 years</p>
    <p>-Use low-water consuming toilets and showerheads</p>
    <p>-Only flush things that are septic-friendly</p>
    <p>-Grow trees far from your septic system</p>
    <p>-Don&rsquo;t drive or park over the septic system</p>
    <p>-Don&rsquo;t overload your septic tank by expanding your house</p>
</div>

<div id="tab-1" class="tab">
        <h3>9) Why would a septic inspection fail?</h3>
</div>
<div class="tab-content">
    <p>Many reasons will fail a septic inspection:</p>
    <p>-The baffle needs repairing</p>
    <p>-Tree roots are damaging the soil around the drain field</p>
    <p>-House drains are not emptying slowly</p>
    <p>-Sewage is backing up into the house</p>
    <p>-The grass near the septic tank is greener and wetter than the rest of your lawn</p>
</div>

<div id="tab-1" class="tab">
        <h3>10) What happens if the septic inspection fails?</h3>
</div>
<div class="tab-content">
    <p>Many prospective buyers might hesitate to buy a property if it fails the septic inspection. To avoid this, the property seller has to do quick fixes to the septic system. Once this is done, there can be another inspection. Some common scenarios are:</p>
    <p>-The seller replaces the leach field or the septic tank</p>
    <p>-The seller replaces the whole system</p>
    <p>-The seller does the necessary repairs</p>
    <p>-The seller credits the buyer the money to do repairs after the transaction has been completed</p>
    <p>-The buyer can renegotiate the price of the property</p>
</div>

<div id="tab-1" class="tab">
        <h3>11) How do I get my septic to pass inspection?</h3>
</div>
<div class="tab-content">
    <p>Your septic will pass the inspection if the sludge only occupies a third or less of the septic tank, and the water flows naturally inside the septic tank.</p>
    <p><strong>Sludge:</strong> It is the solid waste that settles to the bottom of the tank. A routine pump-out will not allow it to pile up and cause clogging or flow into the drain field.</p>
    <p><strong>Scum:</strong> It is the oil, grease and fats that float to the top of the water. If the scum is sticking around in the tank, addressing this issue is important.</p>
    <p><strong>Flow:</strong> The flow within the septic tank should be normal without any disruptions. Disruptions can lead to backups and clogs in the system.</p>
    <p>Other tips to ensure your septic passes inspection:</p>
    <p>-Pump out your septic tank every 2-3 years</p>
    <p>-Use low-water consuming toilets and showerheads</p>
    <p>-Only flush things that are septic-friendly</p>
    <p>-Grow trees far from your septic system</p>
    <p>-Don&rsquo;t drive or park over the septic system</p>
    <p>-Don&rsquo;t overload your septic tank (expanding the house)</p>
</div>

<div id="tab-1" class="tab">
        <h3>12) What to ask a septic inspector?</h3>
</div>
<div class="tab-content">
    <p>Buying a house is everybody&rsquo;s dream, but this lifetime investment involves many things to consider. If you are buying a house with a septic system, you want to ensure that it is in excellent condition. A healthy septic system protects your family, the environment, the greenery, and the drinking water. It also avoids any unwelcome surprises or costly repairs. Try to know the following information from the septic inspector and the homeowner:</p>
    <p>1. How old is the septic system?</p>
    <p>2. How old is the property?</p>
    <p>3. How long has the seller owned the property?</p>
    <p>4. Where is the septic system located?</p>
    <p>5. What is the size of the septic tank?</p>
    <p>6. Where is the original septic permit?</p>
    <p>7. Ask for the maintenance records</p>
    <p>8. Ask about other septic systems in the neighborhood</p>
</div>