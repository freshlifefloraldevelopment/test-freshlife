<!DOCTYPE html>
<html lang="en">
<head>
<meta name="description" content="Understanding SC septic tank regulations is critical to the health of your septic system. Read this blog to know all about SC septic.">        
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
    
    
    
<div class="article-block space-between-blocks">
    <div class="container">
      <div class="article col-xl-10 mx-auto">

        <h2><a aria-hidden="true"><span class="icon icon-link"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tank you!</h2>                

      </div>
    </div>
  </div>            
 
<div class="block-1">
  <div class="container">
    <div class="row justify-content-center ">        

            <div class="col-lg-8 mb-4 mbl-md-0">
                  <div class="content w-100">
                    <div><img class="img-fluid w-50" src="../page-septic/img/gracias-landing.jpg" alt="Grafico" ></div>
                    <br>
                    <h3><a aria-hidden="true"><span class="icon icon-link"></span></a>An agent will be contacting you soon!</h3>                                                                                               
         <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/home.php">Go to Septic Works LLC</a>                         
                  </div>
            </div>               
    </div>
  </div>
</div>    
        
        

<?php 
include "footer.php";
?>
</body>

</html>


