<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="Septic Works LLC - Septic Works LLC.";
include "head.php";
head($title);
?>
    
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->      
<script src="js/jquery-3.6.0.js"></script>
<script src="js/jquery-3.6.0.min.js"></script>
  <script>
    $(document).ready(function(){
        $(".tab").click(function() {
          var selectorID= ($(this).attr("id"));
          $(this).next(".tab-content").slideToggle("slow");
        });
    });
  </script>
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden p-0" >    
    <div class="pt-5 pb-5" style="background-color: #0B448C96;">


        <div class="container ">
          <div class="row mb-5">
                <div class="hero__body col-12 col-md-6 header-landing text-center text-md-left">
                  <h1>CALL 912-666-2210</h1>
                </div>
                <div class="hero__body col-12 col-md-6 text-center text-md-right header-landing">
                    <a class="btn-secondary mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a>
                </div>
            <div class="hero__body col-12 pt-5 text-center banner-landing">
                    <h1>SCHEDULE SEPTIC TANK INSPECTION IN GEORGIA STARTING AT $300</h1>
                    <h2>SEPTIC TANK DRAIN LINE REPAIR AT $4,299.</h2>
                    <p>SOME RESTRICTIONS APPLY.</p>
                    <p>CALL 912-666-2210 FOR MORE INFORMATION</p>
                    <p>AVOID COSTLY REPAIRS AND PROTECT THE HEALTH OF YOUR FAMILY AND THE ENVIRONMENT AROUND</p>
            </div>
          </div>
        </div>
    </div>
    
  <div class="hero__video-container">
    <video id="my-video" class="video-js" autoplay muted loop preload="auto" width="100%" poster="img/home-services.jpg" >
      <source src="img/videolp-4.mp4" type="video/webm">
      <!--source src="img/robotank.mp4" type="video/webm"-->
    </video>
  </div> 
    
</header>
<div class="block-1 mt-5">
  <div class="container">
      <div class="col-lg-2 col-xl-2 mx-auto text-center mb-5">
           <a id="bbblink" class="ruhzbum" href="https://www.bbb.org/us/ga/bluffton/profile/septic-tank-contractors/septic-works-llc-0743-108004#bbbseal" 
              accesskey=""title="Septic Works, LLC., Septic Tank Contractors, Bluffton, GA" 
	 style="display: block;position: relative;overflow: hidden; width: 150px; height: 68px; margin: 0px; padding: 0px;">
	 <img style="padding: 0px; border: none;" id="bbblinkimg" 
	 src="https://seal-centralgeorgia.bbb.org/logo/ruhzbum/septic-works-108004.png" width="300" height="68" alt="Septic Works, LLC., Septic Tank Contractors, Bluffton, GA" /></a>
	 <script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); (function(){var s=document.createElement('script');s.src=bbbprotocol + 'seal-centralgeorgia.bbb.org' + unescape('%2Flogo%2Fseptic-works-108004.js');s.type='text/javascript';s.async=true;var st=document.getElementsByTagName('script');st=st[st.length-1];var pt=st.parentNode;pt.insertBefore(s,pt.nextSibling);})();</script>          
      </div>                  
    <div class="row mb-5">
      <div class="col-12 col-md-2 text-center ">
        <span class="highlight"><b>CUSTOMER SATISFACTION</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>BEST PRICE</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>PERSONAL ATTENTION</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>CONVENIENT TIMES</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>EXCELLENT CUSTOMER SERVICE</b></span>
      </div>
      <div class="col-12 col-md-2 text-center">
        <span class="highlight"><b>FAST RESPONSE</b></span>
      </div>
    </div>
    
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-12 px-lg-5 mb-2 mbl-lg-0">
            <p>A septic tank inspection should be on top of your mind when buying or selling a home with a septic system. It is important as a homeowner to ensure that your septic system is fully functional and with no unpleasant odors or flushing problems. As a buyer, the purchase of a home might be the largest investment you will ever make. A septic inspection will confirm that your dream home is in excellent condition and avoid you any unwelcome surprises.</p>
            <p>At Septic Works LLC, we do a septic inspection and we are licensed and certified– whether you’re making a purchase or sale, our septic tank inspectors will take care of all your needs.</p>
            
        </div>
    </div>
  </div>
</div>


<div class="block-1 pt-5 ">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-lg-4 col-xl-6 px-lg-5 mb-4 mbl-lg-0">
            <h2 class="block__title mb-3 highlight">AVOID COSTLY REPAIRS AND PROTECT THE HEALTH OF YOUR FAMILY AND THE ENVIRONMENT AROUND</h2>
            <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a>
            
       
            <h3 class=" mb-3">WHAT DOES A SEPTIC TANK INSPECTION INVOLVE?</h3>
            <p class="text-justify">At Septic Works LLC, our professional septic tank inspectors are ready and willing to serve all residential, commercial and municipal clients. Our septic tank services include:</p>
            <p class="text-justify"><b>Septic inspection level 1:</b>This is a visual inspection that involves running water from various drains and flushing toilets in the home. The septic tank inspector is looking at how well everything is draining. The visual inspection also includes looking for septic leakage to the top of the drain field area. We also check for sewage odors in the area of the septic tank and the leach field. This inspection can help identify problems but a level two can give our septic inspectors a better idea on issues with your septic system.</p>
            <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a>
            <p class="text-justify"><b>Septic inspection level 2:</b> We do a septic tank pumping. We remove the cover of the septic tank. This allows our septic tank inspectors to check the water level. Water levels determine whether water is properly draining. To make sure the water is properly flowing, our septic inspectors will run water in the home. This is done to determine if the water level rises when more water is introduced. Next, we will check out if your septic system has backflow from the absorption area. Backflow will let us know if there is a problem with the leach field.</p>
            <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a>
            <p class="text-justify"><b>Septic inspection level 3:</b> We highly recommend you choose this option when you are purchasing a new home. The level three septic tank service includes both levels one and two. The difference with level three is that baffles are installed. In many states this is now mandated by the Department of Environmental Health. Baffles are installed in two separate locations: an inlet baffle and an outlet baffle. The inlet baffle allows water to flow into your septic system without disturbing the scum layer. This baffle guides wastewater to flow down, across the septic tank and then up. The outlet baffle serves as a filter to retain solids from traveling to the drain field.</p>
            <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a>
            <p class="text-justify">Unless requested by the customer to carry out the septic inspection level 3 or 2, our septic tank inspectors will assess your septic system using the first level of inspection.

</p>
        </div>
        <div class="col-lg-6 mb-4 mbl-md-0">
            <div class="block-12-card-1__content w-100">
                <div><img class="img-fluid w-100 mb-4" src="img/services1.jpg" alt="about us" >
                </div>
                <div><img class="img-fluid w-100 mb-4" src="img/home-services.jpg" alt="about us" >
                </div>
                <div><img class="img-fluid w-100 mb-4" src="img/dreamstime_s_25485823.jpg" alt="about us" >
                </div>
                <div><img class="img-fluid w-100" src="img/dreamstime_s_173449759.jpg" alt="about us" >
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="block-1 space-between-blocks pt-3">
  <div class="container">
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-lg-12 px-lg-5 mb-4 mbl-lg-0"id="accordion">
        <h2 class="block__title mb-3 highlight text-center">SEPTIC TANK INSPECTION FAQS</h2>
        
        <?php 
                include "landing-faq.php";
         ?>
        </div>
        <div class="col-12 mb-4">
        <p>The septic permit has a drawing of the septic system of the property. This paper lets you know how many bedrooms the septic system is designed for. Also, it lets you know where the septic tank and leach field are located so you don’t waste time looking for them and can better protect your septic system.</p>
        <a class="btn-secondary btn-margin-auto mb-2 mb-3" href="https://septicworksllc.com/page-septic/contact-us.php">Schedule an appointment</a>
        </div>
    </div>
  </div>
</div>


<?php 
include "footer.php";
?>
</body>

</html>