<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="F.A.Q - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->    
<script src="js/jquery-3.6.0.js"></script>
<script src="js/jquery-3.6.0.min.js"></script>
  <script>
    $(document).ready(function(){
        $(".tab").click(function() {
          var selectorID= ($(this).attr("id"));
          $(this).next(".tab-content").slideToggle("slow");
        });
    });
  </script>
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3"><span class="highlight">Frequently</span> Asked Questions</h1>
      <div class="hero__btns-container"><a class="hero__btn btn btn-primary mb-2 mb-lg-0 mx-1 mx-lg-2" href="services.php">View our services</a>
      </div>
    </div>
  </div>
  <div class="row hero__video-container">
  <div class="block-31__image-column container col-lg-6" style="background-image: url(img/faq-header-1.jpg);"></div>
  <div class="block-31__image-column container col-lg-6" style="background-image: url(img/faq-header-2.jpg);"></div>  
  </div>
</header>
<div class="block-12 space-between-blocks">
  <div class="container">
    <div class="px-2 px-lg-0">
      <div class="block-12-card-1 row justify-content-center px-2">
        <div class="col-lg-12 mb-6mbl-md-0">
          <div class="block-12-card-1__content w-100">
            <h2 class="block__title mb-3">FAQ</h2>
            <div id="accordion">

                <?php 
                include "accordion.php";
                ?>

            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
  <div class="block-12__shape-for-background"></div>
</div>


<div class="block-1 space-between-blocks pt-3">
  <div class="container">
   <div class="row">
        <div class="col-lg-8 offset-md-2 pb-5">
            <p class="mb-0 text-justify">Got any inquiries about our services or need help with selling or buying a house with a septic system? <strong>Call us at (912) 666-2210.</strong></p>
        </div>
    </div>      
    <div class="row justify-content-center flex-lg-row px-2">
        <div class="col-lg-12 col-xl-12 px-lg-12 mb-4 mbl-lg-0">
        <form name="frmcat" action="insert-customer3.php" class="form-horizontal" id="profile_form" method="post" enctype="multipart/form-data">  
                <div class="container" id="formulario-sw">

                    <h3 class="section-title">Contact SEPTIC WORKS LLC.</h3>
                    <div class="input-group two-fields">       
                        <div class="mb-3 col-6 pr-3">
                          <div style="position: absolute; display: flex; width: 0px; height: 0px; border: none; padding: 0px; margin: 0px; background: no-repeat; visibility: visible; user-select: none; pointer-events: none; z-index: auto; opacity: 1;"><img title="Required" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjQ4cHgiIGhlaWdodD0iNDhweCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDxkZWZzPjwvZGVmcz4NCiAgICA8ZyBpZD0iSWNvbnMtKy1sb2dvIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHlsZT0ib3BhY2l0eTowLjU0Ij4NCiAgICAgICAgPGcgaWQ9IlNQLUxvZ28iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02MS4wMDAwMDAsIC02MS4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJzeW1ib2wiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYxLjAwMDAwMCwgNjEuMDAwMDAwKSI+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM0LjA4MTAyNzcsMjMuMjYyODk5MiBMMjUuMDYyMDU4NSwxNC4yNzg2ODY0IEMyNC42MDQyODYxLDEzLjgyNDMyMDMgMjMuODYzODg5LDEzLjgyNDMyMDMgMjMuNDA0NDEwNywxNC4yNzg2ODY0IEwxNC4zODc3MTYxLDIzLjI2Mjg5OTIgQzEzLjkyNzY2OSwyMy43MTc4MzE5IDEzLjkyNzY2OSwyNC40NTYwMzUzIDE0LjM4NzcxNjEsMjQuOTEyNjY3NiBMMjMuNDA0NDEwNywzMy44OTYzMTM5IEMyMy42MzE4NzUyLDM0LjEyNDA2MzUgMjMuOTMxNTU5NywzNC4yMzc5MzgzIDI0LjIzMjk1MDMsMzQuMjM3OTM4MyBDMjQuNTMyMDY2MSwzNC4yMzc5MzgzIDI0LjgzNTE2MjYsMzQuMTI0MDYzNSAyNS4wNjIwNTg1LDMzLjg5NjMxMzkgTDM0LjA4MTAyNzcsMjQuOTEyNjY3NiBDMzQuMjk5MzkzNywyNC42OTA1ODM0IDM0LjQyMjc5MzIsMjQuMzk4MjQ4MSAzNC40MjI3OTMyLDI0LjA4ODM1IEMzNC40MjI3OTMyLDIzLjc3NjE4NTcgMzQuMjk5MzkzNywyMy40ODA0NTExIDM0LjA4MTAyNzcsMjMuMjYyODk5MiBaIiBpZD0iRmlsbC0xIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzODc3OTUsMjcuNzQ3ODY3MiBDMTEuMDgxMDA3MSwyNy4yOTQ2MzQxIDEwLjM0MDYxLDI3LjI5NDYzNDEgOS44ODI4Mzc2MywyNy43NDc4NjcyIEwwLjg2Nzg0OTA0OSwzNi43MzI2NDY1IEMwLjQwODM3MDY2OCwzNy4xODgxNDU4IDAuNDA4MzcwNjY4LDM3LjkyNTIxNjEgMC44Njc4NDkwNDksMzguMzgzNTQ4IEw5Ljg4MjgzNzYzLDQ3LjM2ODMyNzMgQzEwLjExMDg3MDgsNDcuNTkzODEwOCAxMC40MTExMjQsNDcuNzA5OTUxOCAxMC43MTI1MTQ2LDQ3LjcwOTk1MTggQzExLjAwNzA4MTEsNDcuNzA5OTUxOCAxMS4zMTE4ODM2LDQ3LjU5MzgxMDggMTEuNTM4Nzc5NSw0Ny4zNjgzMjczIEwyMC41NjE3Mjk0LDM4LjM4MzU0OCBDMjAuNzc3ODIwNywzOC4xNjA4OTczIDIwLjkwNTIwMDgsMzcuODcwODI4MSAyMC45MDUyMDA4LDM3LjU1NjM5NzYgQzIwLjkwNTIwMDgsMzcuMjQ1OTMzIDIwLjc3NzgyMDcsMzYuOTUxODk4IDIwLjU2MTcyOTQsMzYuNzMyNjQ2NSBMMTEuNTM4Nzc5NSwyNy43NDc4NjcyIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMwMEE5RTAiPjwvcGF0aD4NCiAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzguNTY2MTczNiwwLjgyMzM4NTk0IEMzOC4xMDg5Njk5LDAuMzY3ODg2NjkyIDM3LjM2NDU5MjIsMC4zNjUwNTM5ODUgMzYuOTA5NjYzMSwwLjgyMzM4NTk0IEwyNy44OTI5Njg1LDkuODA5Mjk4MzUgQzI3LjQzNTE5NjEsMTAuMjYxOTY0OSAyNy40MzUxOTYxLDExLjAwMTg2NzkgMjcuODkyOTY4NSwxMS40NTg1MDAyIEwzNi45MDk2NjMxLDIwLjQ0MzI3OTUgQzM3LjEzNTk5MDMsMjAuNjY5ODk2MSAzNy40MzUxMDYyLDIwLjc4MjYzNzggMzcuNzM2NDk2NywyMC43ODI2Mzc4IEMzOC4wMzU2MTI2LDIwLjc4MjYzNzggMzguMzM1Mjk3MSwyMC42Njk4OTYxIDM4LjU2NjE3MzYsMjAuNDQzMjc5NSBMNDcuNTgyODY4MiwxMS40NTg1MDAyIEM0Ny44MDE4MDI4LDExLjIzNDcxNjQgNDcuOTI1MjAyNCwxMC45NDQ2NDcyIDQ3LjkyNTIwMjQsMTAuNjMxOTE2NCBDNDcuOTI1MjAyNCwxMC4zMTk3NTIxIDQ3LjgwMTgwMjgsMTAuMDI5MTE2NCA0Ny41ODI4NjgyLDkuODA1MzMyNTYgTDM4LjU2NjE3MzYsMC44MjMzODU5NCBaIiBpZD0iRmlsbC01IiBmaWxsPSIjRDYxODE4Ij48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzMDA3OSwwLjc5NjgxNTE1IEMxMS4wNzQ1ODEzLDAuMzQzMDE1NTI2IDEwLjMyNjc5MTYsMC4zNDMwMTU1MjYgOS44NzU4NDMxLDAuNzk2ODE1MTUgTDAuODU3NDQyNTQ2LDkuNzgxMDI3OTMgQzAuMzk3Mzk1NTAzLDEwLjIzNjUyNzIgMC4zOTczOTU1MDMsMTAuOTc2NDMwMiAwLjg1NzQ0MjU0NiwxMS40MzMwNjI1IEw5Ljg3NTg0MzEsMjAuNDE2MTQyMiBDMTAuMTAyMTcwMywyMC42NDcyOTExIDEwLjQwMTI4NjIsMjAuNzU3MjAwMSAxMC43MDI2NzY3LDIwLjc1NzIwMDEgQzExLjAwNDA2NzIsMjAuNzU3MjAwMSAxMS4zMDMxODMxLDIwLjY0NzI5MTEgMTEuNTMwMDc5LDIwLjQxNjE0MjIgTDIwLjU0NjIwNDksMTEuNDMzMDYyNSBDMjAuNzY3NDE0MiwxMS4yMTI2Nzc5IDIwLjg5NDIyNTcsMTAuOTIxNDc1NyAyMC44OTQyMjU3LDEwLjYwNDc3OTEgQzIwLjg5NDIyNTcsMTAuMjk0MzE0NCAyMC43Njc0MTQyLDEwLjAwMjU0NTYgMjAuNTQ2MjA0OSw5Ljc4MTAyNzkzIEwxMS41MzAwNzksMC43OTY4MTUxNSBaIiBpZD0iRmlsbC03IiBmaWxsPSIjN0FCODAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM4LjYzMTY4MzQsMjcuNzk4ODU1OSBDMzguMTc0NDc5NywyNy4zNDIyMjM2IDM3LjQyOTUzMzMsMjcuMzQyMjIzNiAzNi45NzM0NjY5LDI3Ljc5ODg1NTkgTDI3Ljk1OTYxNTYsMzYuNzgwMjM2IEMyNy41MDE4NDMyLDM3LjIzODAwMTQgMjcuNTAxODQzMiwzNy45NzUwNzE3IDI3Ljk1OTYxNTYsMzguNDMxMTM3NSBMMzYuOTczNDY2OSw0Ny40MTgxODMgQzM3LjIwMDM2MjgsNDcuNjQ0Nzk5NSAzNy40OTg5MSw0Ny43NTk4MDc0IDM3LjgwMDg2OTIsNDcuNzU5ODA3NCBDMzguMTAyMjU5Nyw0Ny43NTk4MDc0IDM4LjM5ODUzMjMsNDcuNjQ0Nzk5NSAzOC42MzE2ODM0LDQ3LjQxODE4MyBMNDcuNjQ4Mzc4LDM4LjQzMTEzNzUgQzQ3Ljg2Njc0NCwzOC4yMTE4ODYgNDcuOTkxODQ5NSwzNy45MTg5ODQxIDQ3Ljk5MTg0OTUsMzcuNjA5MDg2IEM0Ny45OTE4NDk1LDM3LjI5NTc4ODYgNDcuODY2NzQ0LDM3LjAwNDU4NjQgNDcuNjQ4Mzc4LDM2Ljc4MDIzNiBMMzguNjMxNjgzNCwyNy43OTg4NTU5IFoiIGlkPSJGaWxsLTkiIGZpbGw9IiMwMDQ2QUQiPjwvcGF0aD4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg" style="position: relative; border: none; display: inline; cursor: default; padding: 0px; margin: 0px; pointer-events: auto; left: 287px; top: 11px; width: 16px; height: 16px; min-width: 16px; max-width: 16px; min-height: 16px; max-height: 16px;"></div><input type="text" class="form-control confondo" id="name" name="name" placeholder="Name" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                        </div>

                        <div class="mb-3 col-6  ">
                          <div style="position: absolute; display: flex; width: 0px; height: 0px; border: none; padding: 0px; margin: 0px; background: no-repeat; visibility: visible; user-select: none; pointer-events: none; z-index: auto; opacity: 1;"><img title="Required" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjQ4cHgiIGhlaWdodD0iNDhweCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDxkZWZzPjwvZGVmcz4NCiAgICA8ZyBpZD0iSWNvbnMtKy1sb2dvIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHlsZT0ib3BhY2l0eTowLjU0Ij4NCiAgICAgICAgPGcgaWQ9IlNQLUxvZ28iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02MS4wMDAwMDAsIC02MS4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJzeW1ib2wiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYxLjAwMDAwMCwgNjEuMDAwMDAwKSI+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM0LjA4MTAyNzcsMjMuMjYyODk5MiBMMjUuMDYyMDU4NSwxNC4yNzg2ODY0IEMyNC42MDQyODYxLDEzLjgyNDMyMDMgMjMuODYzODg5LDEzLjgyNDMyMDMgMjMuNDA0NDEwNywxNC4yNzg2ODY0IEwxNC4zODc3MTYxLDIzLjI2Mjg5OTIgQzEzLjkyNzY2OSwyMy43MTc4MzE5IDEzLjkyNzY2OSwyNC40NTYwMzUzIDE0LjM4NzcxNjEsMjQuOTEyNjY3NiBMMjMuNDA0NDEwNywzMy44OTYzMTM5IEMyMy42MzE4NzUyLDM0LjEyNDA2MzUgMjMuOTMxNTU5NywzNC4yMzc5MzgzIDI0LjIzMjk1MDMsMzQuMjM3OTM4MyBDMjQuNTMyMDY2MSwzNC4yMzc5MzgzIDI0LjgzNTE2MjYsMzQuMTI0MDYzNSAyNS4wNjIwNTg1LDMzLjg5NjMxMzkgTDM0LjA4MTAyNzcsMjQuOTEyNjY3NiBDMzQuMjk5MzkzNywyNC42OTA1ODM0IDM0LjQyMjc5MzIsMjQuMzk4MjQ4MSAzNC40MjI3OTMyLDI0LjA4ODM1IEMzNC40MjI3OTMyLDIzLjc3NjE4NTcgMzQuMjk5MzkzNywyMy40ODA0NTExIDM0LjA4MTAyNzcsMjMuMjYyODk5MiBaIiBpZD0iRmlsbC0xIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzODc3OTUsMjcuNzQ3ODY3MiBDMTEuMDgxMDA3MSwyNy4yOTQ2MzQxIDEwLjM0MDYxLDI3LjI5NDYzNDEgOS44ODI4Mzc2MywyNy43NDc4NjcyIEwwLjg2Nzg0OTA0OSwzNi43MzI2NDY1IEMwLjQwODM3MDY2OCwzNy4xODgxNDU4IDAuNDA4MzcwNjY4LDM3LjkyNTIxNjEgMC44Njc4NDkwNDksMzguMzgzNTQ4IEw5Ljg4MjgzNzYzLDQ3LjM2ODMyNzMgQzEwLjExMDg3MDgsNDcuNTkzODEwOCAxMC40MTExMjQsNDcuNzA5OTUxOCAxMC43MTI1MTQ2LDQ3LjcwOTk1MTggQzExLjAwNzA4MTEsNDcuNzA5OTUxOCAxMS4zMTE4ODM2LDQ3LjU5MzgxMDggMTEuNTM4Nzc5NSw0Ny4zNjgzMjczIEwyMC41NjE3Mjk0LDM4LjM4MzU0OCBDMjAuNzc3ODIwNywzOC4xNjA4OTczIDIwLjkwNTIwMDgsMzcuODcwODI4MSAyMC45MDUyMDA4LDM3LjU1NjM5NzYgQzIwLjkwNTIwMDgsMzcuMjQ1OTMzIDIwLjc3NzgyMDcsMzYuOTUxODk4IDIwLjU2MTcyOTQsMzYuNzMyNjQ2NSBMMTEuNTM4Nzc5NSwyNy43NDc4NjcyIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMwMEE5RTAiPjwvcGF0aD4NCiAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzguNTY2MTczNiwwLjgyMzM4NTk0IEMzOC4xMDg5Njk5LDAuMzY3ODg2NjkyIDM3LjM2NDU5MjIsMC4zNjUwNTM5ODUgMzYuOTA5NjYzMSwwLjgyMzM4NTk0IEwyNy44OTI5Njg1LDkuODA5Mjk4MzUgQzI3LjQzNTE5NjEsMTAuMjYxOTY0OSAyNy40MzUxOTYxLDExLjAwMTg2NzkgMjcuODkyOTY4NSwxMS40NTg1MDAyIEwzNi45MDk2NjMxLDIwLjQ0MzI3OTUgQzM3LjEzNTk5MDMsMjAuNjY5ODk2MSAzNy40MzUxMDYyLDIwLjc4MjYzNzggMzcuNzM2NDk2NywyMC43ODI2Mzc4IEMzOC4wMzU2MTI2LDIwLjc4MjYzNzggMzguMzM1Mjk3MSwyMC42Njk4OTYxIDM4LjU2NjE3MzYsMjAuNDQzMjc5NSBMNDcuNTgyODY4MiwxMS40NTg1MDAyIEM0Ny44MDE4MDI4LDExLjIzNDcxNjQgNDcuOTI1MjAyNCwxMC45NDQ2NDcyIDQ3LjkyNTIwMjQsMTAuNjMxOTE2NCBDNDcuOTI1MjAyNCwxMC4zMTk3NTIxIDQ3LjgwMTgwMjgsMTAuMDI5MTE2NCA0Ny41ODI4NjgyLDkuODA1MzMyNTYgTDM4LjU2NjE3MzYsMC44MjMzODU5NCBaIiBpZD0iRmlsbC01IiBmaWxsPSIjRDYxODE4Ij48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzMDA3OSwwLjc5NjgxNTE1IEMxMS4wNzQ1ODEzLDAuMzQzMDE1NTI2IDEwLjMyNjc5MTYsMC4zNDMwMTU1MjYgOS44NzU4NDMxLDAuNzk2ODE1MTUgTDAuODU3NDQyNTQ2LDkuNzgxMDI3OTMgQzAuMzk3Mzk1NTAzLDEwLjIzNjUyNzIgMC4zOTczOTU1MDMsMTAuOTc2NDMwMiAwLjg1NzQ0MjU0NiwxMS40MzMwNjI1IEw5Ljg3NTg0MzEsMjAuNDE2MTQyMiBDMTAuMTAyMTcwMywyMC42NDcyOTExIDEwLjQwMTI4NjIsMjAuNzU3MjAwMSAxMC43MDI2NzY3LDIwLjc1NzIwMDEgQzExLjAwNDA2NzIsMjAuNzU3MjAwMSAxMS4zMDMxODMxLDIwLjY0NzI5MTEgMTEuNTMwMDc5LDIwLjQxNjE0MjIgTDIwLjU0NjIwNDksMTEuNDMzMDYyNSBDMjAuNzY3NDE0MiwxMS4yMTI2Nzc5IDIwLjg5NDIyNTcsMTAuOTIxNDc1NyAyMC44OTQyMjU3LDEwLjYwNDc3OTEgQzIwLjg5NDIyNTcsMTAuMjk0MzE0NCAyMC43Njc0MTQyLDEwLjAwMjU0NTYgMjAuNTQ2MjA0OSw5Ljc4MTAyNzkzIEwxMS41MzAwNzksMC43OTY4MTUxNSBaIiBpZD0iRmlsbC03IiBmaWxsPSIjN0FCODAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM4LjYzMTY4MzQsMjcuNzk4ODU1OSBDMzguMTc0NDc5NywyNy4zNDIyMjM2IDM3LjQyOTUzMzMsMjcuMzQyMjIzNiAzNi45NzM0NjY5LDI3Ljc5ODg1NTkgTDI3Ljk1OTYxNTYsMzYuNzgwMjM2IEMyNy41MDE4NDMyLDM3LjIzODAwMTQgMjcuNTAxODQzMiwzNy45NzUwNzE3IDI3Ljk1OTYxNTYsMzguNDMxMTM3NSBMMzYuOTczNDY2OSw0Ny40MTgxODMgQzM3LjIwMDM2MjgsNDcuNjQ0Nzk5NSAzNy40OTg5MSw0Ny43NTk4MDc0IDM3LjgwMDg2OTIsNDcuNzU5ODA3NCBDMzguMTAyMjU5Nyw0Ny43NTk4MDc0IDM4LjM5ODUzMjMsNDcuNjQ0Nzk5NSAzOC42MzE2ODM0LDQ3LjQxODE4MyBMNDcuNjQ4Mzc4LDM4LjQzMTEzNzUgQzQ3Ljg2Njc0NCwzOC4yMTE4ODYgNDcuOTkxODQ5NSwzNy45MTg5ODQxIDQ3Ljk5MTg0OTUsMzcuNjA5MDg2IEM0Ny45OTE4NDk1LDM3LjI5NTc4ODYgNDcuODY2NzQ0LDM3LjAwNDU4NjQgNDcuNjQ4Mzc4LDM2Ljc4MDIzNiBMMzguNjMxNjgzNCwyNy43OTg4NTU5IFoiIGlkPSJGaWxsLTkiIGZpbGw9IiMwMDQ2QUQiPjwvcGF0aD4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg" style="position: relative; border: none; display: inline; cursor: default; padding: 0px; margin: 0px; pointer-events: auto; left: 305px; top: 11px; width: 16px; height: 16px; min-width: 16px; max-width: 16px; min-height: 16px; max-height: 16px;"></div><input type="text" class="form-control confondo" id="phone" name="phone" placeholder="Phone number">
                        </div>                                    
                    </div>   

                    <div class="input-group two-fields">                                       
                        <div class="mb-3  col-6  pr-3">
                          <div style="position: absolute; display: flex; width: 0px; height: 0px; border: none; padding: 0px; margin: 0px; background: no-repeat; visibility: visible; user-select: none; pointer-events: none; z-index: auto; opacity: 1;"><img title="Required" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjQ4cHgiIGhlaWdodD0iNDhweCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPg0KICAgIDxkZWZzPjwvZGVmcz4NCiAgICA8ZyBpZD0iSWNvbnMtKy1sb2dvIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHlsZT0ib3BhY2l0eTowLjU0Ij4NCiAgICAgICAgPGcgaWQ9IlNQLUxvZ28iIHRyYW5zZm9ybT0idHJhbnNsYXRlKC02MS4wMDAwMDAsIC02MS4wMDAwMDApIj4NCiAgICAgICAgICAgIDxnIGlkPSJzeW1ib2wiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYxLjAwMDAwMCwgNjEuMDAwMDAwKSI+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM0LjA4MTAyNzcsMjMuMjYyODk5MiBMMjUuMDYyMDU4NSwxNC4yNzg2ODY0IEMyNC42MDQyODYxLDEzLjgyNDMyMDMgMjMuODYzODg5LDEzLjgyNDMyMDMgMjMuNDA0NDEwNywxNC4yNzg2ODY0IEwxNC4zODc3MTYxLDIzLjI2Mjg5OTIgQzEzLjkyNzY2OSwyMy43MTc4MzE5IDEzLjkyNzY2OSwyNC40NTYwMzUzIDE0LjM4NzcxNjEsMjQuOTEyNjY3NiBMMjMuNDA0NDEwNywzMy44OTYzMTM5IEMyMy42MzE4NzUyLDM0LjEyNDA2MzUgMjMuOTMxNTU5NywzNC4yMzc5MzgzIDI0LjIzMjk1MDMsMzQuMjM3OTM4MyBDMjQuNTMyMDY2MSwzNC4yMzc5MzgzIDI0LjgzNTE2MjYsMzQuMTI0MDYzNSAyNS4wNjIwNTg1LDMzLjg5NjMxMzkgTDM0LjA4MTAyNzcsMjQuOTEyNjY3NiBDMzQuMjk5MzkzNywyNC42OTA1ODM0IDM0LjQyMjc5MzIsMjQuMzk4MjQ4MSAzNC40MjI3OTMyLDI0LjA4ODM1IEMzNC40MjI3OTMyLDIzLjc3NjE4NTcgMzQuMjk5MzkzNywyMy40ODA0NTExIDM0LjA4MTAyNzcsMjMuMjYyODk5MiBaIiBpZD0iRmlsbC0xIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzODc3OTUsMjcuNzQ3ODY3MiBDMTEuMDgxMDA3MSwyNy4yOTQ2MzQxIDEwLjM0MDYxLDI3LjI5NDYzNDEgOS44ODI4Mzc2MywyNy43NDc4NjcyIEwwLjg2Nzg0OTA0OSwzNi43MzI2NDY1IEMwLjQwODM3MDY2OCwzNy4xODgxNDU4IDAuNDA4MzcwNjY4LDM3LjkyNTIxNjEgMC44Njc4NDkwNDksMzguMzgzNTQ4IEw5Ljg4MjgzNzYzLDQ3LjM2ODMyNzMgQzEwLjExMDg3MDgsNDcuNTkzODEwOCAxMC40MTExMjQsNDcuNzA5OTUxOCAxMC43MTI1MTQ2LDQ3LjcwOTk1MTggQzExLjAwNzA4MTEsNDcuNzA5OTUxOCAxMS4zMTE4ODM2LDQ3LjU5MzgxMDggMTEuNTM4Nzc5NSw0Ny4zNjgzMjczIEwyMC41NjE3Mjk0LDM4LjM4MzU0OCBDMjAuNzc3ODIwNywzOC4xNjA4OTczIDIwLjkwNTIwMDgsMzcuODcwODI4MSAyMC45MDUyMDA4LDM3LjU1NjM5NzYgQzIwLjkwNTIwMDgsMzcuMjQ1OTMzIDIwLjc3NzgyMDcsMzYuOTUxODk4IDIwLjU2MTcyOTQsMzYuNzMyNjQ2NSBMMTEuNTM4Nzc5NSwyNy43NDc4NjcyIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMwMEE5RTAiPjwvcGF0aD4NCiAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzguNTY2MTczNiwwLjgyMzM4NTk0IEMzOC4xMDg5Njk5LDAuMzY3ODg2NjkyIDM3LjM2NDU5MjIsMC4zNjUwNTM5ODUgMzYuOTA5NjYzMSwwLjgyMzM4NTk0IEwyNy44OTI5Njg1LDkuODA5Mjk4MzUgQzI3LjQzNTE5NjEsMTAuMjYxOTY0OSAyNy40MzUxOTYxLDExLjAwMTg2NzkgMjcuODkyOTY4NSwxMS40NTg1MDAyIEwzNi45MDk2NjMxLDIwLjQ0MzI3OTUgQzM3LjEzNTk5MDMsMjAuNjY5ODk2MSAzNy40MzUxMDYyLDIwLjc4MjYzNzggMzcuNzM2NDk2NywyMC43ODI2Mzc4IEMzOC4wMzU2MTI2LDIwLjc4MjYzNzggMzguMzM1Mjk3MSwyMC42Njk4OTYxIDM4LjU2NjE3MzYsMjAuNDQzMjc5NSBMNDcuNTgyODY4MiwxMS40NTg1MDAyIEM0Ny44MDE4MDI4LDExLjIzNDcxNjQgNDcuOTI1MjAyNCwxMC45NDQ2NDcyIDQ3LjkyNTIwMjQsMTAuNjMxOTE2NCBDNDcuOTI1MjAyNCwxMC4zMTk3NTIxIDQ3LjgwMTgwMjgsMTAuMDI5MTE2NCA0Ny41ODI4NjgyLDkuODA1MzMyNTYgTDM4LjU2NjE3MzYsMC44MjMzODU5NCBaIiBpZD0iRmlsbC01IiBmaWxsPSIjRDYxODE4Ij48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTExLjUzMDA3OSwwLjc5NjgxNTE1IEMxMS4wNzQ1ODEzLDAuMzQzMDE1NTI2IDEwLjMyNjc5MTYsMC4zNDMwMTU1MjYgOS44NzU4NDMxLDAuNzk2ODE1MTUgTDAuODU3NDQyNTQ2LDkuNzgxMDI3OTMgQzAuMzk3Mzk1NTAzLDEwLjIzNjUyNzIgMC4zOTczOTU1MDMsMTAuOTc2NDMwMiAwLjg1NzQ0MjU0NiwxMS40MzMwNjI1IEw5Ljg3NTg0MzEsMjAuNDE2MTQyMiBDMTAuMTAyMTcwMywyMC42NDcyOTExIDEwLjQwMTI4NjIsMjAuNzU3MjAwMSAxMC43MDI2NzY3LDIwLjc1NzIwMDEgQzExLjAwNDA2NzIsMjAuNzU3MjAwMSAxMS4zMDMxODMxLDIwLjY0NzI5MTEgMTEuNTMwMDc5LDIwLjQxNjE0MjIgTDIwLjU0NjIwNDksMTEuNDMzMDYyNSBDMjAuNzY3NDE0MiwxMS4yMTI2Nzc5IDIwLjg5NDIyNTcsMTAuOTIxNDc1NyAyMC44OTQyMjU3LDEwLjYwNDc3OTEgQzIwLjg5NDIyNTcsMTAuMjk0MzE0NCAyMC43Njc0MTQyLDEwLjAwMjU0NTYgMjAuNTQ2MjA0OSw5Ljc4MTAyNzkzIEwxMS41MzAwNzksMC43OTY4MTUxNSBaIiBpZD0iRmlsbC03IiBmaWxsPSIjN0FCODAwIj48L3BhdGg+DQogICAgICAgICAgICAgICAgPHBhdGggZD0iTTM4LjYzMTY4MzQsMjcuNzk4ODU1OSBDMzguMTc0NDc5NywyNy4zNDIyMjM2IDM3LjQyOTUzMzMsMjcuMzQyMjIzNiAzNi45NzM0NjY5LDI3Ljc5ODg1NTkgTDI3Ljk1OTYxNTYsMzYuNzgwMjM2IEMyNy41MDE4NDMyLDM3LjIzODAwMTQgMjcuNTAxODQzMiwzNy45NzUwNzE3IDI3Ljk1OTYxNTYsMzguNDMxMTM3NSBMMzYuOTczNDY2OSw0Ny40MTgxODMgQzM3LjIwMDM2MjgsNDcuNjQ0Nzk5NSAzNy40OTg5MSw0Ny43NTk4MDc0IDM3LjgwMDg2OTIsNDcuNzU5ODA3NCBDMzguMTAyMjU5Nyw0Ny43NTk4MDc0IDM4LjM5ODUzMjMsNDcuNjQ0Nzk5NSAzOC42MzE2ODM0LDQ3LjQxODE4MyBMNDcuNjQ4Mzc4LDM4LjQzMTEzNzUgQzQ3Ljg2Njc0NCwzOC4yMTE4ODYgNDcuOTkxODQ5NSwzNy45MTg5ODQxIDQ3Ljk5MTg0OTUsMzcuNjA5MDg2IEM0Ny45OTE4NDk1LDM3LjI5NTc4ODYgNDcuODY2NzQ0LDM3LjAwNDU4NjQgNDcuNjQ4Mzc4LDM2Ljc4MDIzNiBMMzguNjMxNjgzNCwyNy43OTg4NTU5IFoiIGlkPSJGaWxsLTkiIGZpbGw9IiMwMDQ2QUQiPjwvcGF0aD4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgPC9nPg0KICAgIDwvZz4NCjwvc3ZnPg" style="position: relative; border: none; display: inline; cursor: default; padding: 0px; margin: 0px; pointer-events: auto; left: 305px; top: 11px; width: 16px; height: 16px; min-width: 16px; max-width: 16px; min-height: 16px; max-height: 16px;"></div><input type="email" class="form-control confondo" id="email" name="email" placeholder="name@example.com">
                        </div>                                            
                        <div class="mb-3 col-6 ">
                          <input type="text" class="form-control confondo" id="comment2" name="comment2" placeholder="Interested in">
                        </div>                                                                                
                    </div>

                    <div class="mb-3">
                      <textarea class="form-control confondo" id="message" name="message" placeholder="Message" rows="3"></textarea>
                    </div>  


                    <div class="col-auto">
                        <button type="submit" class="btn btn-danger mb-3">Submit</button>
                    </div>                                    
                </div>                                
          </form>
        </div>
        
    </div>
  </div>
</div>


<?php 
include "footer.php";
?>
</body>

</html>