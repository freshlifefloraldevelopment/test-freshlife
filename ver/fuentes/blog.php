<!DOCTYPE html>
<html lang="en">
<head>
<?php
$title="Blog - Septic Works LLC.";
include "head.php";
head($title);
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '861192277779833');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=861192277779833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->    
<script src="js/jquery-3.6.0.js"></script>
<script src="js/jquery-3.6.0.min.js"></script>
  <script>
    $(document).ready(function(){
        $(".tab").click(function() {
          var selectorID= ($(this).attr("id"));
          $(this).next(".tab-content").slideToggle("slow");
        });
    });
  </script>
</head>

<body>
<header class="hero container-fluid position-relative overflow-hidden">
  <div class="hero__content h50 container">
    <div class="fixed-nav-container">
    <?php 
    include "menu.php";
    ?>
    </div>
    <div class="hero__body text-center col-lg-8 px-0 mx-auto">
      <h1 class="hero__title mb-3"><span class="highlight">Our</span> Blogs</h1>
      <div class="hero__btns-container"><a class="hero__btn btn btn-primary mb-2 mb-lg-0 mx-1 mx-lg-2" href="services.php">View our services</a>
      </div>
    </div>
  </div>
  <div class=" row hero__video-container">
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/post-header-2.jpg);"></div>
        <div class="block-31__image-column container col-lg-6" style="background-image: url(img/post-header-1.jpg);"></div>        
  </div>
</header>
<div class="space-between-blocks">
  <div class="container">
    <div class="block__header col-lg-8 col-xl-7 mx-auto text-center">
      <h1 class="block__title">Blog</h1>
      <p class="text-justify">Ask a septic tank expert who can take care of all your septic needs. </p> 
      <p class="text-justify"> Our blogs answer all the questions you may have about septic inspection, septic tank installation prices, septic tank repair, septic tank pumping, drain fields, and porta potty. We provide you with all the tips and tricks you need to know on how do septic tanks work to avoid expensive repairs.</p>      
    </div>
    <div class="row px-2 justify-content-center position-relative">
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="post">
            <div class="post__img-container pb-1"><a href="/georgia-septic-tank"><img src="img/casa-blog.jpg" class="post__img"></a></div>
          <div class="post__content px-4 mt-4">
            
            <h3 class="post__title mt-2 mb-3">Georgia Septic Tank: 8 Things to Know Before Buying a House</h3><a href="/georgia-septic-tank" class="post__link"><span>Read Article</span></a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="post">
            <div class="post__img-container pb-1"><a href="/sc-septic-tank-regulations"><img src="img/casa-blog.jpg" class="post__img"></a></div>
          <div class="post__content px-4 mt-4">
            
            <h3 class="post__title mt-2 mb-3">SC Septic Tank Regulations: 8 Things You Need to Know</h3><a href="/sc-septic-tank-regulations" class="post__link"><span>Read Article</span></a>
          </div>
        </div>
      </div>
        
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="post">
            <div class="post__img-container pb-1"><a href="/septic-tank-dos-and-donts"><img src="img/home-services.jpg" class="post__img"></a></div>
          <div class="post__content px-4 mt-4">
            
            <h3 class="post__title mt-2 mb-3">Septic Tank Do's and Don'ts</h3><a href="/septic-tank-dos-and-donts" class="post__link"><span>Read Article</span></a>
          </div>
        </div>
      </div>        
        
    </div>
      
      <!--  Nuevos Blocks-->

    <div class="row px-2 justify-content-center position-relative">
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="post">
            <div class="post__img-container pb-1"><a href="/what-is-a-septic-tank"><img src="img/casa-blog.jpg" class="post__img"></a></div>
          <div class="post__content px-4 mt-4">
            
            <h3 class="post__title mt-2 mb-3">What is a Septic Tank?</h3><a href="/what-is-a-septic-tank" class="post__link"><span>Read Article</span></a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="post">
            <div class="post__img-container pb-1"><a href=""><img src="img/casa-blog.jpg" class="post__img"></a></div>
          <div class="post__content px-4 mt-4">
            
            <h3 class="post__title mt-2 mb-3">Coming Soon</h3><a href="" class="post__link"><span>Read Article</span></a>
          </div>
        </div>
      </div>
        
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="post">
            <div class="post__img-container pb-1"><a href=""><img src="img/home-services.jpg" class="post__img"></a></div>
          <div class="post__content px-4 mt-4">
            
            <h3 class="post__title mt-2 mb-3">Coming Soon</h3><a href="" class="post__link"><span>Read Article</span></a>
          </div>
        </div>
      </div>        
        
    </div>  
      
  </div>
</div>

<?php 
include "footer.php";
?>
</body>

</html>