<?php
require_once("../config/config_gcp.php");

function pagination($query, $per_page,$page,$con)
{
    $query_product = "select g.id from growers as g where g.active IN ('active','advertising') ORDER BY g.growers_name";
    $query = $query_product;
    $row_cnt=mysqli_num_rows(mysqli_query($con,$query));
    //$total=ceil($rows/$limit);


    //$total = $row['num'];
    $total=$row_cnt;
    $adjacents = "2";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $counter=1;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total/$per_page);
    $lpm1 = $lastpage - 1;

    $pagination = "";
    //echo $page."<".$counter." - 1";
    if($lastpage > 1)
    {
        $pagination .= "<ul class='pagination pagination-pill justify-content-end justify-content-center justify-content-md-end'>";
                //$pagination .= "<li class='details'>Page $page of $lastpage</li>";

        if ($page > $counter){
            $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=1'>First</a></li>";
            $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$prev'>Pervious</a></li>";
        }else{
            $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);' >First</a></li>";
            $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);' >Pervious</a></li>";
        }

        if ($lastpage < 7 + ($adjacents * 2))
        {
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);' >$counter</a></li>";
                else
                    $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?&id=$counter'>$counter</a></li>";
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))
        {
            if($page < 1 + ($adjacents * 2))
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='page-item active'><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$lastpage'>$lastpage</a></li>";
            }
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=1'>1</a></li>";
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='page-item active'><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$lastpage'>$lastpage</a></li>";
            }
            else
            {
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=1'>1</a></li>";
                $pagination.= "<li class='page-item '><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='page-item active'><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$counter'>$counter</a></li>";
                }
            }
        }

        if ($page < $counter - 1){
            $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$next'>Next</a></li>";
            $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='?id=$lastpage'>Last</a></li>";
        }else{
            $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);'>Next</a></li>";
            $pagination.= "<li class='page-item'><a class='page-link' tabindex='-1' aria-disabled='true' href='javascript:void(0);'>Last</a></li>";
        }
        $pagination.= "</ul>\n";
    }


    return $pagination;
}
$limit=12;

$start=0;

if(isset($_GET['id'])) {
    $id=$_GET['id'];
    $start=($id-1)*$limit;
    $prueba= 1;
}



 $sel_catinfo="select g.id,g.growers_name,g.page_desc,g.certificates,g.file_path5,g.short_desc,g.active as growers_status,g.country_id
 from growers as g
 where g.active IN ('active','advertising')
 and g.market_place = '1'
 ORDER BY g.growers_name LIMIT $start,$limit";

 $rs_catinfo=mysqli_query($con,$sel_catinfo);



include('../back-end/inc/header_ini_browse.php');

?>


			<!-- -->
			<section>
				<div class="container">


					<!--
						In order for lazyload to work, a height is required, else all images are in viewport.
						By default, an empty png is set with 300px height:
						http://png-pixel.com/
					-->
					<div class="row">

						<?php
						while($catinfo=mysqli_fetch_array($rs_catinfo))
						{
								//echo "<pre>";print_r($catinfo);echo "</pre>";
								$growers_status=$catinfo['growers_status'];

								 $sel_country="SELECT * from country where id='".$catinfo['country_id']."'";
								//echo $sel_country;
								$rs_country=mysqli_query($con,$sel_country);
								$row_Country=mysqli_fetch_array($rs_country);
						?>

						<div class="col-6 col-lg-3 mb-5">

							<div class="bg-white p-2 shadow-primary-xs transition-hover-top transition-all-ease-250">
								<a href="<?php echo "../en/single-growers.php?id=".$catinfo['id'];?>" class="d-block overflow-hidden overlay-dark-hover overlay-opacity-2 text-decoration-none text-dark">
									<?php
									if($catinfo['file_path5'] != "")
									{
									?>
											<img class="img-fluid lazy rounded" src="<?php echo SITE_URL."user/".$catinfo['file_path5'];?>" class="img-responsive growers_logo" alt="<?php echo $catinfo['growers_name'];?>">
									<?php }
									else
									{ ?>
											<img class="img-fluid lazy rounded" src="../assets/images/demo/people/300x300/no_user.png" class="img-responsive" alt="<?php echo $catinfo['growers_name'];?>">
									<?php } ?>
				</a>

								<div class="p-3">

									<h5 class="m-0">
										<a href="<?php echo "../en/single-growers.php?id=".$catinfo['id'];?>">
										<?php echo $catinfo['growers_name'];?>
									</a>
									</h5>

									<ul class="list-inline fs--13 m-0">
										<li class="list-inline-item">

											<?php
												 if($row_Country['name'] != "")
												 {
														 ?>
														 <li><!-- i class="fa fa-map-marker color-green"></i> -->
																 <img src="<?php echo SITE_URL;?>../includes/assets/images/flags/<?php echo $row_Country['flag']?>" />
																 <a href="#!" class="text-gray-500">
																 <?php
														 echo $row_Country['name']." ".$start;
														 ?>
														 </a>
														 </li>
														 <?php
												 }

												 ?>

											<z class="text-purple-500">
												<?php
												if($growers_status != "")
												{?>
														<li>
													 <?php
														if($growers_status == "active")
														{
																echo '<i class="fi fi-round-info-full"></i>';
																echo " Premium Member";
														}
														/*else if($growers_status == "advertising")
														{
																echo '<i class="fa fa-info-circle color-green"></i>';
																echo "Regular Member";
														}*/
														$review_p="select * from review_rating where grower_id='".$catinfo['id']."'";
														//echo $review_p;
														$rs_review_p=mysqli_query($con,$review_p);
														$total_rating=0;
														$cnt_row=mysqli_num_rows($rs_review_p);
														if($cnt_row > 0)
														{
																while($review_detail=mysqli_fetch_array($rs_review_p))
																{
																		$total_rating+=$review_detail['final_rating'];
																}
														}
														$f_rating=0;
														if($total_rating != 0)
														{
																$f_rating=($total_rating*5/($cnt_row*5));
														}

														?>
																&nbsp;&nbsp;&nbsp;
																<span class="size-14 text-muted"><!-- stars -->
																		<?php
																		for($i=1;$i<=5;$i++)
																		{
																				if($i <= round($f_rating))
																				{ ?>
																						<i class="fi fi-star"></i>
																				<?php }
																				else{
																						?>
																						<i class="fi fi-star-full"></i>
																				<?php }
																		}
																		?>
																 </span>
														</li>
														<?php
												}
												?>
											</z>
										</li>
										<br>
										<li class="list-inline-item">

												<?php echo $catinfo['short_desc'];?>

										</li>
									</ul>

								</div>
							</div>

						</div>

						<?php } ?>

					</div>




					<!-- pagination -->
					<nav aria-label="pagination" class="mt-5">

						<?php
								$page = (int) (!isset($_GET["id"]) ? 1 : $_GET["id"]);
								echo pagination("growers",$limit,$page,$con);
						?>


					</nav>
					<!-- pagination -->

				</div>
			</section>
			<!-- / -->

<?php include('../back-end/inc/footer_browse.php'); ?>


		</div><!-- /#wrapper -->

		<script src="assets/js/core.min.js"></script>

	</body>
</html>
