
<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/

    require_once("../config/config_gcp.php");
    $htmlLoadData="";
    $htmlLoadDataPricesItems="";
    $PreviousProductKey = $_POST['PreviousProductKey'];
    $order_id_prev = $_POST['ord_id'];

    $loadDataItemsPrevious= "select sct.id_buyer_temp_item as keyItem, sct.id_product ,
p.name as prodname, p.image_path as prodimage, sct.price as prodprice, sct.quantity as prodquantity,
sct.size_steam as sizesteam, sct.steams as steams, sct.feature_id as features,
p.subcate_name as subcatname,
sct.offer
    from buyer_requests_shoping_cart_temp_items sct
    inner join product  p   ON sct.id_product=p.id
    where
  id_order_prev='$order_id_prev'
    and state_item='0'
    order by sct.id_buyer_temp_item";

    $result_loadDataItemsPrevious = mysqli_query($con,$loadDataItemsPrevious);
    while ($row_result = mysqli_fetch_array($result_loadDataItemsPrevious))
    {

      $id_feature = $row_result['features'];
      $selectFeatures= mysqli_query($con,"select name from features where id='$id_feature'");
       $fila = mysqli_fetch_array($selectFeatures);
        $name_features = $fila['name'];

    $subTotalItems += $row_result['prodprice'];
    $subTotalItemsT = $row_result['prodprice'] * $row_result['steams'] * $row_result['prodquantity'];


    $subTotalItemsS += $row_result['prodprice'];
    $subTotalItemsTS += $row_result['prodprice'] * $row_result['steams'] * $row_result['prodquantity'];

    $id_offerSale = $row_result['offer'];

    if($id_offerSale==1){
    $offer = "<span class='badge badge-purple' style='left: 0%;
      top:10%;
      position: relative;
      margin-left: 15px;
      height:20px;
      transform: rotate(330deg);
      transform-origin: 0% 0%;' fs--10'>Offer</span>";
    }else{
    $offer ='';
    }


    $htmlLoadData .= "
<div class='d-flex justify-content-center align-items-center border-bottom py-3'>
    <div class='w--100'>
      <img class='img-fluid max-h-80' src='https://app.freshlifefloral.com/".$row_result['prodimage']."' alt='".$row_result['prodname']."'>
    </div>

    <div class='w-100 pl-3 pr-3'>

      <div class='float-end'>
        <p class='fs--13 text-weight-muted mb-0'>
          <del>".number_format($subTotalItemsT,2)."</del>
        </p>
        <p class='fs--16 font-weight-medium mb-0'>
          ".number_format($subTotalItemsT,2)."
        </p>
      </div>

      <h6>".$row_result['steams']." x ".$row_result['prodquantity']." ".$row_result['prodname']."</h6>
      <small class='text-muted'><strong>".$row_result['sizesteam']."</strong></small>
</div>
    </div>";

  //  $htmlLoadData .= "<li class='list-group-item d-flex justify-content-between lh-condensed'><div><h6 class='my-0'>".$row_result['prodname']."</h6><small class='text-muted'><strong> - ".$row_result['subcatname']."</strong> - $offer<br>".$row_result['sizesteam']."</small></div><span class='text-muted'> $".$subTotalItemsT."</span><br><font color='#800080'>".$name_features."</font><div style='position: absolute; top: 30px; right: 10px'; onclick='deleteItemPrevious(".$row_result['keyItem'].")'><i class='fa fa-trash fa-1x' aria-hidden='true' title='Delete product'></i></div>";

  $subTT = number_format($subTotalItemsTS,2);
  $Transport = "0.0";
  $valueTransport = number_format((float)$Transport,2);


  $totalPrice = number_format($subTotalItemsTS + $valueTransport, 2);

    }





    //$htmlLoadDataPricesItems = "<li class='list-group-item d-flex justify-content-between'><font color='#000000'><span> Total (USD) </span><strong>".$totalPrice."</strong></font></li>";


    $htmlLoadDataPricesItems = "<div class='border-bottom pb-3 mb-3 mt-3 px-3'>

      <div class='clearfix'>
        Subtotal:
        <span class='float-end font-weight-medium'>
            $totalPrice
        </span>
      </div>

      <div class='clearfix'>
        Transport:
        <span class='float-end'>
         $0.00
        </span>
      </div>

    </div>

    <div class='clearfix mb-3'>
      <h3 class='h4-xs float-start'>Total:</h3>
      <h3 class='h4-xs float-end'>
          $totalPrice
      </h3>
    </div>";



    echo $htmlLoadData.$htmlLoadDataPricesItems;
  ?>
