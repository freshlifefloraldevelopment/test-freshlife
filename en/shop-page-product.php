<?php
session_start();
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 20 Dec 2021
Modificado 27 Dec 2021
**/
// start a session

$productId = $_GET['id'];
$id_product = $productId;
$subcategoryid = $_GET['subId'];
$orderSelected = $_GET['prevId'];

if ($_GET['id']=="" OR $_GET['subId']=="" OR $_GET['prevId']=="") {
  header("Location: ../en/florMP.php");
  exit();
}



function show_sizes_proces($subcategoryid,$userSessionID,$con){

  echo $subcategoryid;

               // se eliminó  después de and gpb.prodcutid = '".$products['id']."' "and gpb.price > 0"
              $tasaKilo = calculateKilo($userSessionID,$con);

               $sql_price = "select gp.id,gp.price_adm as price,   s.name as sizename   ,
               f.name as featurename, gp.feature as feature, s.id as size_id,
               gp.factor , gp.stem_bunch , b.name as stems, gp.price_card,
               gp.unit, gp.variation_price, gp.date_ini,  gp.date_end
               from grower_parameter gp
               inner JOIN sizes s ON gp.size = s.id
               inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
               left JOIN features f  ON gp.feature = f.id
               where gp.idsc = '$subcategoryid' " ;

               $rs_price = mysqli_query($con,$sql_price);

               while ($row_price = mysqli_fetch_array($rs_price))
               {

                 $priceTotal = 0;

                 $siz = $row_price['size_id'];
                 $idSub = $row_price['id'];



                 $sql_priceP = "select gp.id, gp.price_card as price,
                 gp.variation_price as vp, gp.date_ini,  gp.date_end
                 from param_product_price gp
                 where gp.idsc = '$subcategoryid'
                 and gp.product_id = '$id_product'
                 and gp.size = '$siz'" ;

                 $rs_priceP  = mysqli_query($con,$sql_priceP);
                 $row_priceP = mysqli_fetch_array($rs_priceP);


                   $sql_priceG = "select gp.id, gp.price as price,
                 gp.variation_price as vp, gp.date_ini,  gp.date_end
                 from growcard_product_price gp
                 where
                  gp.prodcutid = '$id_product'
                 and gp.sizeid    = '$siz'
                 and gp.growerid  = '$idGrower'";

                 $rs_priceG  = mysqli_query($con,$sql_priceG);
                 $row_priceG = mysqli_fetch_array($rs_priceG);

                 $date_ini = $row_price['date_ini'];
                 $date_end = $row_price['date_end'];

                 if($date_ini =="" || $date_end=="" || $date_ini =="no-date" || $date_end=="no-date"){
                   $row_priceS = $row_price['price'];
                   $row_priceSS= $row_price['price_card'];
                 }else{

                    if($date_ini <= date('Y-m-d'))
                    {
                      if($date_end >= date('Y-m-d'))
                      {
                        $row_priceS = $row_price['price']+ $row_price['variation_price'];
                        $row_priceSS= $row_price['price_card']+ $row_price['variation_price'];
                      }else{
                        //Fechas y vriation a cero a nivel de Card

                        $update_resS = "UPDATE  grower_parameter
                                        SET
                                        variation_price = '0.00',
                                        date_ini = 'no-date',
                                        date_end = 'no-date'
                                        WHERE
                                        id      = '$idSub'
                                        ";

                                    @mysqli_query($con, $update_resS);

                          $row_priceS = $row_price['price'];
                          $row_priceSS= $row_price['price_card'];
                      }
                    }else{
                      $row_priceS = $row_price['price'];
                      $row_priceSS= $row_price['price_card'];
                    }

                 }

                  $date_ini_P = $row_priceP['date_ini'];
                  $date_end_P = $row_priceP['date_end'];
                  $idProd = $row_priceP['id'];

                  if($date_ini_P =="" || $date_end_P=="" || $date_ini_P =="no-date" || $date_end_P=="no-date"){
                     $row_priceP = $row_priceP['price'];
                  }else{



                     if($date_ini_P <= date('Y-m-d'))
                     {

                       if($date_end_P >= date('Y-m-d'))
                       {

                       $row_priceP = $row_priceP['price'] + $row_priceP['vp'];
                       }else{
                         //Fechas y vriation a cero a nivel de Product

                         $update_resP = "UPDATE  param_product_price
                                         SET
                                         variation_price = '0.00',
                                         date_ini = 'no-date',
                                         date_end = 'no-date'
                                         WHERE
                                         id      = '$idProd'
                                         ";

                                     @mysqli_query($con, $update_resP);
                          $row_priceP = $row_priceP['price'];
                       }
                     }else{

                        $row_priceP = $row_priceP['price'];
                     }

                  }


                  $date_ini_G = $row_priceG['date_ini'];
                  $date_end_G = $row_priceG['date_end'];
                  $idGro = $row_priceG['id'];

                  if($date_ini_G =="" || $date_end_G=="" || $date_ini_G =="no-date" || $date_end_G=="no-date"){
                     $row_priceG = $row_priceG['price'];
                  }else{

                     if($date_ini_G <= date('Y-m-d'))
                     {
                       if($date_end_G >= date('Y-m-d'))
                       {
                       $row_priceG = $row_priceG['price'] + $row_priceG['vp'];
                       }else{
                         //Fechas y vriation a cero a nivel de Grower

                         $update_resG = "UPDATE  growcard_product_price
                                         SET
                                         variation_price = '0.00',
                                         date_ini = 'no-date',
                                         date_end = 'no-date'
                                         WHERE
                                         id      = '$idGro'
                                         ";

                                     @mysqli_query($con, $update_resG);

                         $row_priceG = $row_priceG['price'];
                       }
                     }else{
                       $row_priceG = $row_priceG['price'];
                     }

                  }



                 if($row_price["unit"]== 39)
                 {

                   if($facx=='100000'){
                     $priceTotal = $row_priceS;
                   }else{



                     if($row_priceG != "0.00" && $row_priceG != ""){
                         $priceTotal = $row_priceG;
                     }
                     else
                     {
                       if($row_priceP !="0.00" && $row_priceP != ""){
                         $priceTotal = $row_priceP;
                       }else{
                         $priceTotal = $row_priceS;
                       }
                     }
                   }




                  $unid = "Stems";
                  $unid_id = $row_price["unit"];
                  $priceBunch = $tasaKilo * $row_price['factor'];
                  $priceSteam = $priceBunch / $row_price['stems'];
                  $priceCalculado = sprintf('%.2f',round($priceSteam + $priceTotal,2));


                }else{

                   if($row_price["unit"]== 40){

                     if($facx=='100000'){
                       $priceTotal = $row_priceSS;
                     }else{



                     if($row_priceG != "0.00" && $row_priceG != ""){
                         $priceTotal = $row_priceG;
                     }else{
                       if($row_priceP !="0.00" && $row_priceP != ""){
                         $priceTotal = $row_priceP;
                       }else{
                         $priceTotal = $row_priceSS;
                       }
                     }
                   }

                     $unid = "Bunches";
                     $unid_id = $row_price["unit"];
                      $priceBunch = $tasaKilo * $row_price['factor'];
                      $priceCalculado = sprintf('%.2f',round($priceBunch + $priceTotal,2));


                   }else{
                     $unid = "Unit";
                     $unid_id = $row_price["unit"];
                   }

                 }

                  $sizenameL =  $row_price['sizename'];
                  $featureL = $row_price['feature'];
                  $size_idL = $row_price['size_id'];
                  $unitL = $row_price['unit'];
                  $featurenameL = $row_price['featurename'];
                  $priceCalculado1 = $priceCalculado +1;


                $prices.= "<div><label class='form-radio form-radio-primary'>
                <input value='$priceCalculado.'/'.$sizenameL.'/'.$featureL.'/'.$size_idL.'/'.$unitL' type='radio' name='checkboxT' id='checkboxT' onclick='flagCost('$priceCalculado','$size_idL','$featureL','$unitL','$priceCalculado')'
                data-count-target='.cart-example'
                data-count-math='$priceCalculado1'
                data-count-last-math='$priceCalculado'
                >";
                $prices.= "<i></i> + $ $priceCalculado.' / '.$sizenameL.' [cm] '.$featurenameL";
                $prices.= "</label></div>";


  }

return $prices;
}

// initialize session variables
// $order_prevID = $_SESSION['orderSelected'];
$unid = "Unit";
$display = 20;
$XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');

if($_SESSION["buyer"])
{

  $userSessionID = $_SESSION["buyer"];

  if($_SESSION['orderVery_id']==0)
  {

    $_varibleOrder = $_POST['valueOrderId_MP'];
    if(!$_varibleOrder)
    {
      header("Location: ../buyer/buyers-account.php?menu=1");
      exit();
    }
    else
    {

      $_SESSION['orderSelected'] = $_POST['valueOrderId_MP'];
      $_SESSION['orderVery_id'] = 1;
    }
  }
  else
  {

        if($_POST['valueOrderId_MP']){
          $_SESSION['orderSelected']=$_POST['valueOrderId_MP'];
        }
      }

  }

  else{

    header("Location: ../login.php");
    exit();
  }

  $sel_product = "SELECT p.id , p.tab_desc1 , p.tab_desc2 , substr(p.tab_desc2,1,78) short_desc2,substr(p.tab_desc2,79,80) short_desc3, p.name , p.image_path , s.description ,c.name name_cat
                    FROM product p
                    JOIN subcategory s ON p.subcategoryid = s.id
                    JOIN category c ON p.categoryid = c.id
                   WHERE p.id = $productId";

  $rs_products = mysqli_query($con, $sel_product);

  $product = mysqli_fetch_assoc($rs_products);

  $name = preg_replace("![^a-z0-9]+!i", "-", trim($product["name"]));
  $name_cat = preg_replace("![^a-z0-9]+!i", "-", trim($product["name_cat"]));
  $tab2 = $product["tab_desc2"];
  $img = $product['image_path'];

include('../back-end/inc/header_ini.php');

 ?>

<style type="text/css">
.form-control-sm {
    padding: 0px!important;
}



</style>

<style type="text/css">


.plus-image {
    left: 10%;
    top: 90%;
    position: absolute;
    margin-top: -25px;
    margin-left: -25px;
}

.plus-image-sale{
    right: 0%;
    top:5%;
    position: absolute;
    margin-top: 5px;
    margin-left: -25px;
}


.plus-image-sale-logo{
  left: 5%;
  top:30%;
  position: absolute;
  margin-top: 5px;
  margin-left: -25px;


  transform: rotate(330deg);
  transform-origin: 0% 0%;
}

.product-holder {
    position: relative;
    display: block;
}

.css-typing p {
  border-right: .15em;
  height: 20px;
  font-size: 15px;
  white-space: nowrap;
  overflow: hidden;
}
.css-typing p:nth-child(1) {
  width: 22em;
    height: 20px;
  -webkit-animation: type 4s steps(40, end);
  animation: type 4s steps(40, end);
  -webkit-animation-fill-mode: forwards;
  animation-fill-mode: forwards;
}





@keyframes type {
  0% {
    width: 0;
  }
  99.9% {
    border-right: .15em solid orange;
  }
  100% {
    border: none;
  }
}

@-webkit-keyframes type {
  0% {
    width: 0;
  }
  99.9% {
    border-right: .15em solid orange;
  }
  100% {
    border: none;
  }
}

@keyframes type2 {
  0% {
    width: 0;
  }
  1% {
    opacity: 1;
  }
  99.9% {
    border-right: .15em solid orange;
  }
  100% {
    opacity: 1;
    border: none;
  }
}

@-webkit-keyframes type2 {
  0% {
    width: 0;
  }
  1% {
    opacity: 1;
  }
  99.9% {
    border-right: .15em solid orange;
  }
  100% {
    opacity: 1;
    border: none;
  }
}

@keyframes type3 {
  0% {
    width: 0;
  }
  1% {
    opacity: 1;
  }
  100% {
    opacity: 1;
  }
}

@-webkit-keyframes type3 {
  0% {
    width: 0;
  }
  1% {
    opacity: 1;
  }
  100% {
    opacity: 1;
  }
}

@keyframes blink {
  50% {
    border-color: transparent;
  }
}
@-webkit-keyframes blink {
  50% {
    border-color: tranparent;
  }
}

.full_modal-dialog {
  width: 98% !important;
  height: 92% !important;
  min-width: 98% !important;
  min-height: 92% !important;
  max-width: 98% !important;
  max-height: 92% !important;
  padding: 0 !important;
}

.full_modal-content {
  height: 99% !important;
  min-height: 99% !important;
  max-height: 99% !important;
}

</style>


			<div class="d-flex flex-fill" id="wrapper_content">
<?php

include('../back-end/inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
          <section class="bg-light p-0">
            <div class="container py-4">

              <h1 class="h3">
                 <input type="hidden" id="valueOrderId_MP" value="<?php echo $_SESSION['orderSelected']; ?>" />
                Product Detail

                <div class="btn-group" style="float:right; margin:0 auto;">
                  <a style="left:-50px;" role="button" id="price_modal" class="btn btn-default btn-xs" href="shop-page-cart-1.php" >
                                 <img src="../includes/assets/images/icon-header-02.png" alt="ICON">
                                       <span class="bg-danger circle rounded" style="
                     width: 20px;
                     height: 20px;
                     padding: 4px;
                     background: #fff;
                     left: -10px;
                     top: -50%;
                     position: relative;
                     border: 1px solid #666;
                     color: #fff;
                     text-align: center;

                     font: 14px Arial, sans-serif;" id="countProductosPrev">0</span>


                   </a>
                </div>
              </h1>

              <!-- Breadcrumbs -->
              <div class="mb-0">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb fs--14">
                    <li class="breadcrumb-item"><a href="florMP.php">Flower Market</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Product Detail</li>
                  </ol>
                </nav>


              </div>
              <!-- /Breadcrumbs -->

            </div>
          </section>


          <input type="hidden" id="sessionStorage" name="sessionStorage" />
          <input type="hidden" id="productID" value="<?php echo $productId; ?>" />
          <input type="hidden" id="ordenPrevId" value="<?php echo $orderSelected; ?>" />
          <input type="hidden" id="subID" value="<?php echo $subcategoryid; ?>" />



          			<!-- PRODUCT -->
          			<section class="pt-4">
          				<div class="container">

                    <div class="fs--14 text-muted mt-1 mb-1">
                      Last updated on <time datetime="2021-12-20">Dec 20th, 2021</time>

                    </div>

          					<div class="row">

          						<div class="col-lg-7 col-md-6 order-1 mb-5">

          							<!--

          								SWIPER SLIDER
          								w-75 w-100-xs 		= 70% width on desktop, 100% mobile
          								swiper-white 		= white buttons. (swiper-primary, swiper-danger, etc)

          								By default, Smarty controller will reconfigure swiper if -only- one image detected:
          									- remove arrows
          									- remove progress/bullets
          									- disable loop
          								Add .js-ignore class to skip, if for some reason is needed!

          							-->
          							<div class="swiper-container swiper-preloader swiper-white mx-auto"
          								data-swiper='{
          									"slidesPerView": 1,
          									"spaceBetween": 0,
          									"autoplay": false,
          									"loop": true,
          									"zoom": true,
          									"effect": "slide",
          									"pagination": { "type": "progressbar" }
          								}'>

          								<!--

          									NOTE: only the first image is NOT using lazy loading (to avoid 'jumping')
          									lazy is optional but recommended: ~80% of visitors don't slide through images!

          									Images are using srcset for responsiveness!

          								-->
                          <div class="swiper-wrapper text-center">

                            <?php
                            $sel_product = "SELECT p.id , p.tab_desc1 , p.tab_desc2 , substr(p.tab_desc2,1,78) short_desc2,substr(p.tab_desc2,79,80) short_desc3, p.name , p.image_path , s.description ,c.name name_cat
                            								 FROM product p
                            								 JOIN subcategory s ON p.subcategoryid = s.id
                            								 JOIN category c ON p.categoryid = c.id
                            								WHERE p.id = $productId";

                            $rs_products = mysqli_query($con, $sel_product);
                            $product = mysqli_fetch_assoc($rs_products);
                            $img = $product['image_path'];

                             $htmlLoadData1 = "<div class='swiper-slide'>
                             <div class='swiper-zoom-container'>
                            	 <img class='bg-suprime img-fluid rounded max-h-600'
                            		 sizes='(max-width: 768px) 100vw'
                            		 srcset='https://app.freshlifefloral.com/$img 1200w, ttps://app.freshlifefloral.com/$img 768w'
                            		 src='https://app.freshlifefloral.com/$img'>
                             </div>
                            </div>";


                            $sel_img = "SELECT gp.id , gp.product_id , gp.grower_id , gp.categoryid , gp.subcaegoryid , gp.colorid , gp.image_path
                            							FROM grower_product gp
                            						 WHERE image_path IS NOT NULL
                            							AND gp.product_id = $productId
                            						 order by image_path";

                            $rs_img = mysqli_query($con, $sel_img);


                                       while ($row_img = mysqli_fetch_array($rs_img))
                            					 {

                            						 $img_S = $row_img['image_path'];


                            						$htmlLoadData .= "<div class='swiper-slide'>
                            							<div class='swiper-zoom-container'>
                            								<img class='bg-suprime img-fluid rounded max-h-600'

                            									sizes='(max-width: 768px) 100vw'
                            									srcset='https://app.freshlifefloral.com/$img_S 1200w, https://app.freshlifefloral.com/$img_S 768w'
                            									src='https://app.freshlifefloral.com/$img_S'>
                            							</div>
                            						</div>";


                            						$i++;
                                       }

                             echo $htmlLoadData1.' '.$htmlLoadData;


                            ?>


                          	</div>

          								<!-- Left|Right Arrows -->
          								<div class="swiper-button-next rounded-circle shadow-xs d-none d-md-block"></div>
          								<div class="swiper-button-prev rounded-circle shadow-xs d-none d-md-block"></div>

          								<!-- Progress Bar -->
          								<div class="swiper-pagination position-relative mt-4 h--1"></div>

          							</div>
          							<!-- /SWIPER SLIDER -->


          							<div class="clearfix mt-4">
          								<div class="font-weight-medium mb-1 text-success">
          									<i class="fi fi-box-checked"></i>
          									<span class="d-inline-block px-2">Details</span>
          								</div>

          								<img height="50" class="float-start" src="https://app.freshlifefloral.com/<?php echo $img; ?> " alt="<?php echo $name." ".$name_cat; ?> ">

          								<a class="text-dark" href="#!">
          									<?php echo $name; ?>
          								</a>

          								<small class="text-muted d-block">
          									<?php echo $name_cat; ?>
          								</small>

          							</div>



          						</div>


          						<div class="col-lg-5 col-md-6 order-2 mb-5">

          							<div class="clearfix"><!-- sticky-kit -->

          								<!-- TITLE -->
          								<h1 class="h2 h3-xs font-weight-medium mb-3">
                            <?php echo $name; ?>
          									<span class="d-block text-muted fs--14"><?php  echo $name_cat; ?></span>
          								</h1>



          								<!-- Form -->
          								<form novalidate class="bs-validate" method="post" action="#" data-error-scroll-up="true">


          									<!-- PRICE -->
          									<div class="clearfix mb-4">

          										<p style="display:none" class="text-muted m-0">
          											<del id="valorAdd"  class="cart-example" data-toggle="count"
                               data-count-decimals="2"
                               data-count-from="0.00"
                               data-count-to="0.00"
                               data-count-base-to="0.00"
                               data-count-duration="1500">$0.00</del>
          											<span class="text-success font-light fs--17">(– 49%) /
          												<span class="font-weight-medium">you save $1.00</span>
          											</span>
          										</p>

          										<p class="fs--25 m-0 font-weight-medium text-danger">

          											<!--
          												counter used because of configurator to do the math.
          												If configurator not used, just add the price instead ($149.99)
          											-->

          											$<span class="cart-example1" id="cart-example1"
					                           >0.00</span>
          										</p>

          									</div>


                            <!-- CONFIGURATOR -->
          									<div class="clearfix mb-3">

          										<h6 class="font-weight-medium">
          											Grower
          										</h6>

                              <?php
                              $growers_sql1 = "select g.id , g.growers_name,
                                                     (select TIMESTAMPDIFF(DAY, max(date_added), now()) from invoice_requests where grower_id = gp.grower_id ) as dias_transcurridos
                                                 from grower_product gp
                                                 inner join growers  g on gp.grower_id  = g.id
                                                 where gp.product_id = $productId
                                                   and g.active='active'
                                                   and g.market_place = '1'
                                                 order by g.growers_name";

                             $rs_growers1 = mysqli_query($con,$growers_sql1);
                             while ($row_grower1 = mysqli_fetch_array($rs_growers1)){
                              ?>
          										<div>
          											<label class="form-radio form-radio-primary">
          												<input type="radio" name="checkbox"
          													value="<?php echo $row_grower1['id']; ?>" onclick="VerGrower('<?php  echo $row_grower1['id']; ?>');">
          												<i></i> + <?php echo $row_grower1['growers_name']; ?>
          											</label>
          										</div>

                              <?php
                            }
                              ?>

                              <input type="hidden" id="growerIDD" value="" />



                                <span class="hr mb-3"></span>

                                <h6 class="font-weight-medium">
                                  Sizes
                                </h6>

                                <div id='pricesG'>
                                </div>


                                 <span class="hr mb-3"></span>
                                 <h6 class="font-weight-medium">
             											Boxes
             										</h6>


                    							<select class="form-control" id="sub_cat_change3" disabled="true" onchange="removeDisabledC()">
                                    <option value='0'>Select Boxes</option>
                    							</select>


                                  <span class="hr mb-3"></span>
                                  <h6 class="font-weight-medium">
                                   Qty
                                  </h6>

                                  <select class="form-control" id="sub_cat_change4" disabled="true">
                    							  <option value='0'>Select Quantity</option>
                                  <?php

                                      for($k=1;$k<=50;$k++) {
                                  ?>
                                  <option data-icon="fi fi-boxfloat-start" value="<?php echo $k; ?>"><?php echo $k." boxes"; ?></option>
                                  <?php
                                    }
                                  ?>
                                  	</select>


          									</div>
          									<!-- /CONFIGURATOR -->


                            <!-- ADD TO CART -->
                            <div class="clearfix d-flex d-block-xs">

                              <!-- ADD TO CART BUTTON -->
                              <div class="d-flex flex-fill ml-0 mr-0 mt-2">




                                <div class="pl-2 pr-2 w-100">
                                  <button  type="button" onclick="msnCart()" class="btn btn-block btn-danger bg-gradient-danger text-white px-4 b-0">

                                    <span class="px-4 p-0-xs">
                                      <i>
                                        <svg width="22px" height="22px" x="0px" y="0px" viewBox="0 10 459.529 500.529">
                                          <path fill="#ffffff" d="M17,55.231h48.733l69.417,251.033c1.983,7.367,8.783,12.467,16.433,12.467h213.35c6.8,0,12.75-3.967,15.583-10.2    l77.633-178.5c2.267-5.383,1.7-11.333-1.417-16.15c-3.117-4.817-8.5-7.65-14.167-7.65H206.833c-9.35,0-17,7.65-17,17    s7.65,17,17,17H416.5l-62.9,144.5H164.333L94.917,33.698c-1.983-7.367-8.783-12.467-16.433-12.467H17c-9.35,0-17,7.65-17,17    S7.65,55.231,17,55.231z"></path>
                                          <path fill="#ffffff" d="M135.433,438.298c21.25,0,38.533-17.283,38.533-38.533s-17.283-38.533-38.533-38.533S96.9,378.514,96.9,399.764    S114.183,438.298,135.433,438.298z"></path>
                                          <path fill="#ffffff" d="M376.267,438.298c0.85,0,1.983,0,2.833,0c10.2-0.85,19.55-5.383,26.35-13.317c6.8-7.65,9.917-17.567,9.35-28.05    c-1.417-20.967-19.833-37.117-41.083-35.7c-21.25,1.417-37.117,20.117-35.7,41.083    C339.433,422.431,356.15,438.298,376.267,438.298z"></path>
                                        </svg>
                                      </i>

                                      <span class="fs--18">Add to cart</span>
          													</span>

                                    <!-- free shipping : optional : good for conversions -->
                                    <span class="d-block pt-2 pb-1">
                                      <span class="hr"></span>
                                      <span class="font-light fs--13 opacity-7 d-block mt-2">
                                      This product is awesome
                                      </span>
                                    </span>
                                    <!-- /free shipping : optional : good for conversions -->

                                  </button>
                                    <input type="hidden" id="priceC" value="" />
                                    <input type="hidden" id="sizeL" value="" />
                                    <input type="hidden" id="featureL" value="" />
                                    <input type="hidden" id="size_idL" value="" />
                                    <input type="hidden" id="unitL" value="" />

                                </div>

                              </div>

                            </div>
                            <!-- /ADD TO CART -->

                            <!-- stock info -->
                            <div class="pt-4 ">
                                                    <a href="florMP.php" class="fs--15 text-decoration-none">
                                                      <i class="fi fi-arrow-start-slim"></i>
                                                      return to Flower Market
                                                    </a>
                                                  </div>
                            <!-- /ADD TO CART -->

          								</form>
          								<!-- /Form -->

          							</div>

          						</div>

          					</div>


          				</div>
          			</section>
          			<!-- /PRODUCT -->



                			<!-- SUGGESTIONS -->
                			<section class="bg-theme-color-light pt-4 pb-0">
                				<div class="container">

                					<h2 class="h4 mb-0">
                						Growers
                					</h2>



                					<!--

                						SWIPER SLIDER

                					-->
                					<div class="swiper-container swiper-preloader"
                						data-swiper='{
                							"slidesPerView": 5,
                							"spaceBetween": 10,
                							"autoplay": false,
                							"loop": true,
                							"pagination": { "type": "bullets" },
                							"breakpoints": {
                								"1200": { "slidesPerView": "4" },
                								"1024": { "slidesPerView": "3" },
                								"0": 	{ "slidesPerView": "2" }
                							}
                						}'>

                						<!--

                							NOTE: do not use lazy when loop is true!

                						-->
                						<div class="swiper-wrapper">

                              <?php

                              $growersSql = "select grower_product.product_id, growers.*
                                               from grower_product left join growers on growers.id = grower_product.grower_id
                                              where growers.active IN ('active','advertising')
                                                and grower_product.product_id = $productId
                                              ORDER BY growers.growers_name";

                                              $growersResult = mysqli_query($con, $growersSql);

                                              while ($growersRow = mysqli_fetch_assoc($growersResult)) {
                                                  $growers_status=$growersRow['growers_status'];

                                                  $sel_country="SELECT * from country where id='".$growersRow['country_id']."'";
                                                  //echo $sel_country;
                                                  $rs_country=mysqli_query($con,$sel_country);
                                                  $row_Country=mysqli_fetch_array($rs_country);





                               ?>

                							<!-- slider 1 -->
                							<div class="swiper-slide">

                								<div class="bg-white shadow-xs shadow-md-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 my-5">

                									<a href="#!" class="text-decoration-none">

                										<figure class="pt--30 pb--30 pl--6 pr--6 m-0 text-center bg-gradient-radial-light rounded-top">
                											<img src="https://app.freshlifefloral.com/user/<?php echo $growersRow['file_path5']; ?>" alt="..." class="img-fluid maxx-h-150 bg-suprime opacity--9">
                										</figure>

                										<div class="text-center-xs text-gray-600 py-3">

                											<!-- .max-height-80  = limited to 2 rows of text -->
                											<span class="d-block fs--16 max-h-50 overflow-hidden">
                												<?php echo $growersRow["growers_name"]; ?>
                											</span>

                											<span class="d-block text-danger font-weight-medium fs--1 mt-2">

                                        <?php if($row_Country['name'] != "")
                                        {
                                            ?>
                                            <li><!-- i class="fa fa-map-marker color-green"></i> -->
                                                <img src="<?php echo SITE_URL;?>includes/assets/images/flags/<?php echo $row_Country['flag']?>" />
                                                <?php
                                            echo $row_Country['name'];
                                            ?>
                                            </li>
                                            <?php
                                        }

                                        ?>


                											</span>
                                      <br>

                                      <p><?php echo $growersRow["short_desc"]; ?></p>

                										</div>

                									</a>

                								</div>

                							</div>

                            <?php } ?>

                						</div>


                						<!-- Bullets -->
                						<div class="swiper-pagination"></div>

                					</div>
                					<!-- /SWIPER SLIDER -->


                				</div>
                			</section>
                			<!-- /SUGGESTIONS -->


				</div><!-- /MIDDLE -->
			</div><!-- FOOTER -->
<!-- Modal -->
<div class="modal fade" id="buy_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog" role="document">
				<div class="modal-content">

						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">Confirmation</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->
						<div class="modal-body">
								Your order has been sent
						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
										<i class="fi fi-close"></i>
										Close
								</button>
						</div>

				</div>
		</div>
</div>
<!-- /Modal -->
<?php include('../back-end/inc/footer.php'); ?>
<script src="../back-end/assets/js/blockui.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript" src="appMkPlace.js"></script>


<!--Price Modal Continue Checkout--------------------------------------------------------->

<div id="myModalCheckOut" class="modal fade checkout_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none; overflow-y:scroll;">


    <div class="full_modal-dialog modal-dialog">

        <div class="modal-content" style="background:#fcfcfc;">

            <div class="modal-body request_product_modal_hide">

                <div class="row">
                  <div class="col-md-5 order-md-2 mb-4" style="background:#fcfcfc;">
                    <h4 class="d-flex justify-content-between align-items-center mb-2">
                      Your cart
                      <span style="background:#8A2984; color:white; size:8px;" class="badge" id="countProductosPrev1">0</span>
                    </h4>

                    <ul style="background:#fffffc;" class="list-group mb-3" id="itemPreviousCheckOut" style="height: 400px; overflow-y:scroll;">
                    </ul>
                  <ul class="list-group mb-3" id="itemPreviousSumCheckOut">

                    </ul>


                  </div>
                  <div class="col-md-7 order-md-1">

                    <form id="frm-box-mix" method="post" action="/back-end/mix-box.php">

                    </form>

                    <form action="CreateCharge.php" method="post" id="payment-form">

                      <div id="requiereDelivery">
                        <h5 class="mb-3">Delivery Date </h5>
                      <hr class="mb-4">


                      <!-- date picker -->
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a style="background-color:#DFE0E1; border-style:1px solid #ccc;" class="nav-link" id="home-delivery" data-toggle="tab" href="#home" onclick="payCredCard();" role="tab" aria-controls="home" aria-selected="true">
                          <strong><font size="3" color="black">Delivery date </font></strong> <img src="../images/delivery.png" width="30px" height="20px"/>
                        </a>

                      </li>

                    <?php
                      $getBuyerShippingMethod = "select shipping_code , shipping_method_id
                                                from buyer_shipping_methods
                                                where buyer_id = '".$userSessionID."'  ";
                                    $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
                                    $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);


                      ?>
                      <input type="hidden" value="<?php echo $buyerShippingMethod['shipping_method_id'];?>" id="shipping_method_id" />

                      <label class="field cls_date_start_date" id="cls_date">
                        <input value="" class="form-control required start_date" placeholder="Select Date" type="text">

                      </label>

                    <br>

                    </div>

                      <h4 class="mb-3">Payment Method</h4>
                      <input type="hidden" id="tipo-payment" value="1"/>

                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li id="myTabCredit" class="nav-item">
                        <a style="background-color:#DFE0E1; border-style:1px solid #ccc;" class="nav-link" id="home-tab" data-toggle="tab" href="#home" onclick="payCredCard();" role="tab" aria-controls="home" aria-selected="true">
                          <strong><font size="3">Credit card</font></strong> <img src="../images/creditcard.png" width="50px" height="50px"/>
                        </a>

                      </li>
                      <li id="myTabCode" class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" onclick="payCode();" role="tab" aria-controls="profile" aria-selected="false">
                          <strong><font size="3">Code</font></strong> <img src="../images/paycode.png" width="50px" height="50px"/>
                        </a>
                      </li>
                    </ul>

                    <div id="myTabContent">

                      <div class="tab-pane" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div class="form-advanced-list p-4 shadow-xs border rounded mb-5">
                        <div class="payCredCardF" id="payCredCardF" style="display: block;">
                          <label class="form-radio form-radio-primary d-block py-3 border-bottom">

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                <label for="cc-name">Name on card</label>
                                <input type="text" id="cc-card-name"><br>
                                <small class="text-muted">Full name as displayed on card</small>
                              </div>

                              <div class="col-md-6 mb-3">
                                <label for="cc-name">Phone number</label>
                                <input type="text" id="cc-phone-number"><br>
                                <small class="text-muted">Include area code</small>
                              </div>
                            </div>


                            <div class="row">
                              <div class="col-md-8 mb-3">
                                <label for="cc-name">Credit card</label>
                                 <fieldset>
                                <div id="card-element" class='inputs'></div>
                              </fieldset>
                              </div>
                            </div>
                        </div>
                      </div>

                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <label class="form-radio form-radio-primary d-block py-3 border-bottom">
                        <div id="payCodeF" style="display: none;">

                            <div class="row">
                                <div class="col-md-6 mb-3">

                                <label for="cc-name">Code</label>

                                <input type="password" id="cc-card-code"><br>
                                  <fieldset>
                                <small class="text-muted">Please, use your code here.</small>
                                  </fieldset>
                              </div>
                            </div>

                        </div>

                      </div>
                      <div id="payCodeFA" style="display: none;">
                        <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link"  data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                            <strong><font size="3">Code</font></strong> <img src="../images/paycode.png" width="50px" height="50px"/>
                          </a>
                        </li></ul>

                          <div class="row">
                              <div class="col-md-6 mb-3">

                              <label for="cc-name">Code</label>

                              <input type="password" id="cc-card-code1"><br>
                                <fieldset>
                              <small class="text-muted">Please, use your code here.</small>
                                </fieldset>
                            </div>
                          </div>

                      </div>
                    </div>
                    <br><br>
                      <button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>

                      <button onclick="revForm()" type="submit" id="buyItNow" class="btn btn-primary">
                        <i class="fa fa-cart-plus"></i>
                        Buy it now</button><br>
                    </form>
                    <br>
                    <script src="https://js.stripe.com/v3/"></script>
                  </div>

                </div>

                <table>
                      <tr>
                      <td>
                        <div class="row">
                          <div class="col-md-8 order-md-2 mb-2">
                    <ul class="fa-ul" style="display: inline;">
                        <font size="5" color="#1d9d74">Secure Payments &nbsp;&nbsp;<i class="fa fa-shield" aria-hidden="true"></i></font><br><br>
                    </ul>
                        <font size="1">
                        <ul class="fa-ul" style="display: inline;">
                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Data protected</li>
                          <li style="display: inline;">&nbsp;&nbsp;</li>
                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Consumer rights</li>
                          <li style="display: inline;">&nbsp;&nbsp;</li>
                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Refund politics</li>
                          <li style="display: inline;">&nbsp;&nbsp;</li>
                        </ul>

                        <ul class="fa-ul" style="display: inline;">

                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Secure Payments</li>
                          <li style="display: inline;">&nbsp;&nbsp;</li>
                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Guaranteed safe</li>
                        </ul>
                      </font>
                    </div>
                    </div>

                      </td>
                      <td align="left"><img src="../images/payments.png" width="190px" height="50px"/></td>
                      </tr>

                </table>


                <footer class="my-2 pt-5 text-muted text-center text-small">
                <p class="mb-1">© 2021 Fresh Life Floral</p>
                </footer>



              </div>

        </div>
    </div>

</div>
<!--Price Modal End------------------------------------------------------------>



            <!--Select Orders Modal Open-->
            <div class="modal fade orders_method_modal______test" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
                <div class="modal-dialog modal-md modal-md" role="document">
                    <div class="modal-content">

                        <!-- header modal -->
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabelMd">Select Previous Order</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span class="fi fi-close fs--18" aria-hidden="true"></span>
                          </button>

                        </div>
                        <!-- body modal 3-->
                        <form action="../en/florMP.php" method="post" id="payment-form">
                        <div class="modal-body">
                            <div class="table-responsive">

                              <font color="#000">Please, before to continue select an order.</font><br><br>

                              <div class="form-label-group mb-3">
                              <select class="form-control"  id="selectPreviousOrder_1" onchange="checkOrderPrevious_1()" name="order_id_1">
                                          <option value='0'>Select Previous Order</option>
                                          <?php
                                                  $sel_order="select id , order_number ,del_date , qucik_desc
                                                                from buyer_orders
                                                               where del_date >= '" . date("Y-m-d") . "'
                                                                 and is_pending=0 and buyer_id = '".$userSessionID."' order by id desc ";

                                                  $rs_order=mysqli_query($con,$sel_order);

                                              while($orderCab=mysqli_fetch_array($rs_order))  {
                                          ?>
                                                  <option value="<?php echo $orderCab["id"]?>"><?php echo $orderCab["id"]." ".$orderCab["order_number"]." ".$orderCab["del_date"]." ".$orderCab["qucik_desc"] ?></option>
                                          <?php
                                              }
                                             ?>
                              </select>

                              <label for="select_options">Select Previous Order</label>
                               <input type="hidden" name="valueOrderId_MP" id="valueOrderId_MP" value="0" />
                            </div>



                                                    <br>
                                                   <span class="badge badge-warning" style="color: #000; background-color: #ffc107; border-color: #28a745;"><strong>Attention:</strong></span> <em><font color="#000">Creating a new order should be the first step.</font></em>

                            </div>



                        </div>

                        <div class="modal-footer request_product_modal_hide_footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                            <button style="background:#8a2b83!important;" type="submit" disabled class="btn btn-primary" id="orders_modal" class="btn btn-default btn-xs" data-toggle="modal" data-target=".checkout_modal">Go to Market Place</button>

                        </div>
                        </form>
                    </div>
                </div>
            </div>



<script type="text/javascript">



window.onload=function() {



  var productID = document.getElementById('productID').value;
  var datos = "id_product="+productID;


  $.ajax({
       type: "POST",
       url: "../back-end/selectSliderProductsSmarty.php",
       data: datos,
       cache: false,
       success: function(r){
       document.getElementById('imgSlider').innerHTML = r;
         $.unblockUI();
       }
   });


var key = localStorage.getItem("idSessionCompras");
countProductPrevious(key);
}

function countProductPrevious(PreviousProductKey){
document.getElementById('sessionStorage').value = PreviousProductKey;

var order_id_prev = document.getElementById('ordenPrevId').value;
var datos = "ord_id="+order_id_prev+"&PreviousProductKey="+PreviousProductKey;
//alert(datos);

 $.ajax({
      type: "POST",
      url: "count_temp_cart.php",
      data: datos,
      cache: false,
      success: function(r){
       //alert(r);
       // return false;

        document.getElementById('countProductosPrev').innerHTML = r;
      //  document.getElementById('countProductosPrev1').innerHTML = r;
      }
  });

}


var myVar;


function flagCost(a,q,w,e,r,g,i) {

//$priceCalculado','$sizenameL','$featureL','$size_idL','$unitL','precio','$i',

var totalGr = document.getElementById('totalGrower').value;

for(var z=1; z<=totalGr; z++){
  //var eee = z+1;//alert(z+1);

    if(z!=i){
      document.getElementById('checkboxT'+z).checked = false;
    }else{
      document.getElementById('checkboxT'+i).checked = true;
    }
}

//alert(i);
  //

myVar = setTimeout(alertFunc, 300,a);

var cantidad = parseFloat(a) +1;



document.getElementById('priceC').value = a;
document.getElementById('sizeL').value = q;
document.getElementById('featureL').value = w;
document.getElementById('size_idL').value = e;
document.getElementById('unitL').value = r;

document.getElementById('valorAdd').innerHTML  = cantidad.toFixed(2);
  var y = document.getElementById('growerIDD').value;
  var b = document.getElementById('productID').value;


  var size_id  = e;
  var feature_id_add  = w;
  var unit_send  = r;
  var b = document.getElementById('productID').value;

  datos = "idGrower="+y+"&product_id="+b+"&size_id="+size_id+"&feature_id_add="+feature_id_add+"&unit_id_add="+unit_send;
   //alert(datos);
  $.ajax({
       type: "POST",
       url: "selectBoxesProduct.php",
       data: datos,
       cache: false,
       success: function(r){

         document.getElementById('sub_cat_change3').innerHTML = r;
         document.getElementById('sub_cat_change3').disabled=false;
       }
   });
}

function alertFunc(a) {
  document.getElementById('cart-example1').innerHTML = a;
}


function VerGrower(a){
  document.getElementById('growerIDD').value = a;
  //document.getElementById('checkboxT').checked=false;

var sib = document.getElementById('subID').value;
var pib = document.getElementById('productID').value;


    var datos = "idGrower="+a+"&idsc="+sib+"&prod="+pib+"&xfac="+0;

  $.ajax({
       type: "POST",
       url: "selectSizesSmarty.php",
       data: datos,
       cache: false,
       success: function(r){
       document.getElementById('pricesG').innerHTML = r;
       }
   });

  document.getElementById('sub_cat_change3').value=0;
  document.getElementById('sub_cat_change3').disabled=true;
  document.getElementById('sub_cat_change4').value=0;
  document.getElementById('sub_cat_change4').disabled=true;

}

function VerSizes(a){
  document.getElementById('sizeIDD').value = a;
}

function removeDisabledC(){
  document.getElementById('sub_cat_change4').value=0;
  document.getElementById('sub_cat_change4').disabled=false;
}


function msnCart()
{
//$priceCalculado','$sizenameL','$featureL','$size_idL','$unitL',$i,

var w = document.getElementById('priceC').value;
var e =document.getElementById('sizeL').value;
var r =document.getElementById('featureL').value;
var t =document.getElementById('size_idL').value;
var y =document.getElementById('unitL').value;



  var prodCart = document.getElementById('productID').value;
  var prodBoxes = document.getElementById('sub_cat_change3').value;
  var prodQty = document.getElementById('sub_cat_change4').value;


  if(prodCart!='' && prodBoxes!=0 && prodQty!=0){
    newProduct(prodCart,prodBoxes,prodQty,w,e,r,t,y);
  }else{
    swal("Sorry!", "Please, fill all required fields!", "error");
  }

}

function newProduct(prodCart,prodBoxes,prodQty,w,e,r,t,y)
{


 if(localStorage.getItem("idSessionCompras") == null)
 {
   var newPreviousProductKey = RandomNum(1000000001, 2147483647);
   localStorage.setItem("idSessionCompras", newPreviousProductKey);
   var idSessionCompras = localStorage.getItem("idSessionCompras");

   var price = w;
   var size  = e+" [cm]";
   var feature_id  = r;
   var size_id  = t;
   var unit = y;
   var offerId = 0;
    var steamsT = document.getElementById('sub_cat_change3').value;
    var steamsArray = steamsT.split("/");
    var steams = steamsArray[1];
    var quantity = document.getElementById('sub_cat_change4').value;
    var bestOp = 0;
    var order_id_prev = document.getElementById('ordenPrevId').value;
    var prodId = prodCart;
    var growerIDP = document.getElementById('growerIDD').value;
    var subcategoryId = document.getElementById('subID').value;


  var datosCart = "unit_id_add="+unit+"&ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+growerIDP+"&price_id="+price+"&size="+size+"&box_id="+document.getElementById('sub_cat_change3').value+"&quantity_id="+quantity+"&steams="+steams+"&categoriaId="+subcategoryId+"&offer_Id="+offerId;


      $.ajax({
           type: "POST",
           url: "save_temp_cart.php",
           data: datosCart,
           cache: false,
           success: function(r){
            // alert(r);
            // return false;
             if(r==1){
               countProductPrevious(idSessionCompras);
               document.getElementById('sub_cat_change3').value = "0";
               document.getElementById('sub_cat_change4').value = "0";
               document.getElementById('sub_cat_change3').disabled=true;
               document.getElementById('sub_cat_change4').disabled=true;
               swal("Good job!", "Product add to cart!", "success");
             }else{
               swal("Sorry!", "Please, try again!", "error");
             }

           }
       });



 }
   else
     {
     addNoFirstTime(localStorage.getItem("idSessionCompras"),prodCart,prodBoxes,prodQty,w,e,r,t,y);
     }
}

function addNoFirstTime(newPreviousProductKey,prodCart,prodBoxes,prodQty,w,e,r,t,y)
{

  var price = w;
  var size  = e+" [cm]";
  var feature_id  = r;
  var size_id  = t;
  var unit = y;
  var offerId = 0;
   var steamsT = document.getElementById('sub_cat_change3').value;
   var steamsArray = steamsT.split("/");
   var steams = steamsArray[1];
   var quantity = document.getElementById('sub_cat_change4').value;
   var bestOp = 0;
   var order_id_prev = document.getElementById('ordenPrevId').value;
   var prodId = prodCart;
   var growerIDP = document.getElementById('growerIDD').value;
   var subcategoryId = document.getElementById('subID').value;


     var datosCart = "unit_id_add="+unit+"&ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+growerIDP+"&price_id="+price+"&size="+size+"&box_id="+document.getElementById('sub_cat_change3').value+"&quantity_id="+quantity+"&steams="+steams+"&categoriaId="+subcategoryId+"&offer_Id="+offerId;

  //   alert(datosCart);
    $.ajax({
         type: "POST",
         url: "save_temp_cart_items.php",
         data: datosCart,
         cache: false,
         success: function(r){

            // alert(r);
            // return false;
           if(r==1){

             countProductPrevious(newPreviousProductKey);

             document.getElementById('sub_cat_change3').value = "0";
             document.getElementById('sub_cat_change4').value = "0";
             document.getElementById('sub_cat_change3').disabled=true;
             document.getElementById('sub_cat_change4').disabled=true;


             swal("Good job!", "Product add to cart!", "success");
           }else{
             swal("Sorry!", "Please, try again!", "error");
           }
         }
     });
}
</script>
