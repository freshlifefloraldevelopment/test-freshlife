<?php
session_start();
$userSessionID = $_SESSION["buyer"];
/**
#Save temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/


require_once("../config/config_gcp.php");

  $product_id = $_POST['product_id'];
  $grower_id = $_POST['grower_id'];
  $box_id = $_POST['box_id'];
  $price_id = $_POST['price_id'];
  $size = $_POST['size'];
  $quantity_id = $_POST['quantity_id'];
  $steams = $_POST['steams'];
  $feature_id = $_POST['feature_id'];
  $size_id = $_POST['size_id'];
  $best_id = $_POST['bestOp_id'];
  $order_id_prev = $_POST['ord_id'];
  $categoriaId = $_POST['categoriaId'];

  if($categoriaId==''){
    $selectCatQ = mysqli_query($con,"Select subcategoryid from product where id='$product_id'");
    $selectCat  = mysqli_fetch_array($selectCatQ);
    $categoriaId = $selectCat['subcategoryid'];
  }

  $newPreviousProductKey = $_POST['newPreviousProductKey'];
  $timestamp = date_timestamp_get(date_create());
  $datePrevious = date("Y/m/d");
  $timePrevious = date("G:i:s");

    $price_cost_transp = calculateCost_Transp($grower_id,$product_id,$size_id,$categoriaId,$con,$userSessionID);

$insertPreviousProductHeadKey="INSERT into buyer_requests_shoping_cart_temp (id_buyer_temp,timestamp_seg,date_prev,time_prev,id_order_prev) values ('$newPreviousProductKey','$timestamp','$datePrevious','$timePrevious','$order_id_prev')";
mysqli_query($con,$insertPreviousProductHeadKey);

$insertPreviousProductItemsKey="INSERT into buyer_requests_shoping_cart_temp_items (id_grower,id_product,id_boxes,price,quantity,steams,feature_id,size_id,timestamp_seg,id_buyer_temp, size_steam, id_best_op,id_order_prev,box_mix_id,id_category,cost_transp,bunchs)
                                                                            values ('$grower_id','$product_id','$box_id','$price_id',$quantity_id,'$steams','$feature_id','$size_id','$timestamp','$newPreviousProductKey','$size','$best_id','$order_id_prev',0,'$categoriaId','$price_cost_transp',0)";
echo mysqli_query($con,$insertPreviousProductItemsKey);

function calculateCost_Transp($growerid,$productid,$sizeid,$idsc,$con,$userSessionID){

	$tasaKilo = calculateKilo($userSessionID,$con);

   $getGrowerPriceMeth1 = "select gp.price as price
															from growcard_prod_price gp
															INNER JOIN growers g ON gp.growerid = g.id
															INNER JOIN product p ON gp.productid = p.id
															INNER JOIN subcategory s ON p.subcategoryid = s.id
															INNER JOIN colors c on p.color_id = c.id
															where g.active = 'active'
															and gp.growerid = '$growerid'
															and gp.productid = '$productid'
															and gp.sizeid = '$sizeid'";
  $getGrowerPriceMeth1_array = mysqli_query($con, $getGrowerPriceMeth1);
  $GrowerPriceMeth1 = mysqli_fetch_assoc($getGrowerPriceMeth1_array);
  $priceGrower = $GrowerPriceMeth1['price'];
		if($priceGrower<=0){
			$getGrowerPriceMeth2 = "select gp.id,gp.price_adm as price,
			s.name as sizename , gp.feature as feature, gp.factor,
			gp.stem_bunch , b.name as stems
			from grower_parameter gp
			inner JOIN sizes s ON gp.size = s.id
			inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
			left JOIN features f ON gp.feature = f.id
			where gp.idsc = '$idsc'
			and gp.size = '$sizeid'";
			$getGrowerPriceMeth2_array = mysqli_query($con, $getGrowerPriceMeth2);
			$GrowerPriceMeth2 = mysqli_fetch_assoc($getGrowerPriceMeth2_array);

			$priceBunch = $tasaKilo * $GrowerPriceMeth2['factor'];
		 	$priceSteam = $priceBunch / $GrowerPriceMeth2['stems'];
			//$a = $GrowerPriceMeth2['price'];
			$priceGrower = sprintf('%.2f',round($priceSteam,2));
		}

  	return $priceGrower;
}

function calculateKilo($userSessionID,$con){
   $getBuyerShippingMethod = "select shipping_method_id
                              from buyer_shipping_methods
                             where buyer_id ='" . $userSessionID . "'";

  $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
  $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
   $shipping_method_id = $buyerShippingMethod['shipping_method_id'];

     $getShippingMethod = "select connect_group
                             from shipping_method
                            where id='" . $shipping_method_id . "'";

      $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
      $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

       $temp_conn = explode(',', $shippingMethodDetail['connect_group']);

      $id_conn = $temp_conn[1];  // Default

      $getConnect = "select charges_per_kilo
                       from connections
                      where id='" . $id_conn . "'";

      $rs_connect = mysqli_query($con, $getConnect);

      $charges = mysqli_fetch_assoc($rs_connect);

       /////////////////////////////////////////////////////////////

      $cost = $charges['charges_per_kilo'];
      $cost_un = unserialize($cost);

      $cost_sum = 0;

      foreach ($cost_un as $key => $value) {
          $cost_sum = $cost_sum + $value;
      }

      return $charges_per_kilo_trans =  $cost_sum;
}

?>
