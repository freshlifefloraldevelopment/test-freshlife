<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Date Dec 02 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/
require_once("../config/config_gcp.php");
$htmlLoadData="";
$PreviousProductKey = $_POST['PreviousProductKey'];
$idCategory = $_POST['idCategory'];
$order_id_prev = $_POST['ord_id'];

$offer='';
$t=0;
$h=1;
$w=0;
$c=1;
$x=0;

 $loadDataItemsPrevious= "select p.subcategoryid as subCat, sct.id_buyer_temp_item as keyItem, sct.id_product, p.name as prodname, p.image_path as prodimage, sct.box_mix_id as box, sct.price as prodprice, sct.quantity as prodquantity, sct.size_steam as sizesteam, sct.steams as steams, sct.feature_id as features, sct.id_grower, sct.unit as unit, sct.offer from buyer_requests_shoping_cart_temp_items sct inner join product p ON sct.id_product=p.id where id_order_prev='$order_id_prev' and state_item='0' order by sct.box_mix_id";

$result_loadDataItemsPrevious = mysqli_query($con,$loadDataItemsPrevious);
while ($row_result = mysqli_fetch_array($result_loadDataItemsPrevious))
{
  $idUnit = $row_result['unit'];
  $describUnit_array = mysqli_fetch_array(mysqli_query($con,"select descrip FROM units where id='$idUnit'"));
  $descripbUnit = $describUnit_array['descrip'];

  $id_feature = $row_result['features'];
  $selectFeatures= mysqli_query($con,"select name from features where id='$id_feature'");
  $fila = mysqli_fetch_array($selectFeatures);

    $name_features = $fila['name'];
    if($name_features!=""){$name_features = " / ".$fila['name'];} else {$name_features = $fila['name'];}
    $gggg = $row_result['id_grower'];

    if($row_result['box'] !=0)
    {
      $selectGNameMy = mysqli_query($con,"Select growers_name from growers where id ='$gggg'");
      $selectGName = mysqli_fetch_array($selectGNameMy);
      $name_grower = $selectGName['growers_name'];
      $numBox = $row_result['box'];
      $delete = '';

    if($h==1)
    {$_SESSION['groParsing'] = $row_result['id_grower'];
    $_SESSION['numBM']      = $numBox;
    }$h=$h+1;
      if($t<1)
        {if($_SESSION['groParsing'] == $row_result['id_grower'] && $_SESSION['numBM'] == $numBox)
            {if($c==1){$gro = $row_result['id_grower'];
              $TBoxMix = "<div class='row'><div class='col-10'><div class='float-start'>	<h3 class='h4 text-left pt-2 mb-2'>Box Mix / $name_grower</h3></div></div><p class='text-dark float-right font-weight-light mb-0 pl--12 pr--12'><button  type='button' onclick='deleteItemPreviousMixBox(".$gro.",".$numBox.")' class='btn btn-sm btn-danger'><i class='fi fi-close'></i>Delete</button></p></div>";
              $t = 0; $w = 0; $c = 2;
              }else{$TBoxMix='';}
          }else{if($c==2){$gro = $row_result['id_grower'];
            $_SESSION['groParsing'] = $row_result['id_grower'];
            $_SESSION['numBM'] = $numBox;
            $TBoxMix = "<div class='row'><div class='col-10'><div class='float-start'>	<h3 class='h4 text-left pt-2 mb-2'>Box Mix / $name_grower</h3></div></div><p class='text-dark float-right font-weight-light mb-0 pl--12 pr--12'><button  type='button' onclick='deleteItemPreviousMixBox(".$gro.",".$numBox.")' class='btn btn-sm btn-danger'><i class='fi fi-close'></i>Delete</button></p></div>";
            $t = 0; $w = 0; $c = 2;
          }else{$gro = $row_result['id_grower'];
            $_SESSION['groParsing'] = $row_result['id_grower'];
            $_SESSION['numBM'] = $numBox;
            $TBoxMix = "<div class='row'><div class='col-10'><div class='float-start'>	<h3 class='h4 text-left pt-2 mb-2'>Box Mix / $name_grower</h3></div></div><p class='text-dark float-right font-weight-light mb-0 pl--12 pr--12'><button  type='button' onclick='deleteItemPreviousMixBox(".$gro.",".$numBox.")' class='btn btn-sm btn-danger'><i class='fi fi-close'></i>Delete</button></p></div>";
            $t = 0; $w = 0; $c = 2;
          }}}
    }else{
        $t = 0; $c = 1; $h = 1;
      $numBox = '';
      $delete = "<a class='js-ajax-confirm fs--13' href='#!' onclick='deleteItemPrevious(".$row_result['keyItem'].")'>Delete</a>";
      if($x==0){$TBoxMix = "<h3 class='h4 text-left pt-2 mb-2'>Classical Box</h3>";
      $x=1;}else{$TBoxMix ='';}
    }

    $id_offerSale = $row_result['offer'];
    if($id_offerSale==1){ $offer = "<span class='badge badge-purple' style='left: 0%; top:50%; position: relative; margin-left: -60px; transform: rotate(330deg); transform-origin: 0% 0%;' fs--12'>Offer</span>";
    }else{ $offer =''; }

$steamsTotal = $row_result['prodquantity']*$row_result['steams'];
$priceOld    = $row_result['prodprice']+ 0.34;
$saveValue   = $priceOld - $row_result['prodprice'];

$htmlLoadData .= "$TBoxMix <div class='cart-item shadow-xs p-3 mb-4 rounded border'>
  <div class='row'>
    <div class='col-4 col-sm-4 col-md-2 col-lg-2 text-center'><a href='shop-page-product.php?id=".$row_result['id_product']."&&subId=".$row_result['subCat']."&&prevId=$order_id_prev'><img class='img-fluid max-h-80' src='https://app.freshlifefloral.com/".$row_result['prodimage']."' alt='".$row_result['prodname']."'></a></div>
    <div class='col-8 col-sm-8 col-md-10 col-lg-10'><div class='row'><div class='col-12 col-sm-12 col-md-6 col-lg-6'><div class='clearfix my-2 d-block'><a class='fs--18 text-dark font-weight-medium' href='shop-page-product.php?id=".$row_result['id_product']."&&subId=".$row_result['subCat']."&&prevId=$order_id_prev'>".$idCategory." ".$row_result['prodname']." / ".$row_result['sizesteam']."</a><span class='d-block text-muted fs--12'>Boxes: ".$row_result['prodquantity']."  $name_features</span><ul class='list-inline'><li class='list-inline-item'>$delete</li></ul></div></div><div class='col-6 col-sm-6 col-md-3 col-lg-3 text-center'><div class='position-relative'><span class='js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top start-0 m-1'>please, order between 1 and 99.</span><input type='number' name='qty[5][]' class='cart-qty form-control form-control-sm text-center js-form-advanced-limit' min='1' max='2000' readonly value='$steamsTotal'></div><span class='d-block text-muted fs--11 mt-1'>$descripbUnit</span></div>

    <div class='col-6 col-sm-6 col-md-3 col-lg-3 text-align-end'><p class='fs--13 text-weight-muted mb-0'><del>".number_format($priceOld,2)."</del></p><p class='fs--16 font-weight-medium mb-0'>".$row_result['prodprice']."</p><span class='text-success d-block fs--12 mb-2'>You save:<br>$saveValue</span></div></div></div></div>







</div>";
}
echo $htmlLoadData;

$gift = "<div class='row'>
  <div class='col-12 col-sm-12 col-md-2 col-lg-2'></div>
  <div class='col-12 col-sm-12 col-md-10 col-lg-10'>
    <div class='font-weight-medium mb-1 border-top pt-3 mt-2'>
      <i class='fi fi-gift text-muted'></i>
      <span class='d-inline-block px-2'>Your Gift</span>
    </div>
    <span class='float-end text-align-end text-muted fs--12'>
      <span class='d-block'>Gift value:</span>
      $200
    </span>
    <img height='30' class='float-start' src='/smarty/html_frontend/demo.files/images/unsplash/products/thumb_330/gift.jpg' alt='...'>
    <a class='text-dark fs--13 d-block pt--4' href='#!'>
      1 &times; Product gift title
    </a>
  </div>
</div>
";


?>
