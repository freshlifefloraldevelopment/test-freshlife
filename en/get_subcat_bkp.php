<?php
require_once("../config/config_gcp.php");

if ($_REQUEST['action'] == "getsubcat") {
    
    if ($_REQUEST['cat_main_id'] == "" || $_REQUEST['cat_main_id'] == "undefined") {
        
        $sel_products_sub = "select s.* 
                               from subcategory s 
		               left join grower_product gp on s.id=gp.subcaegoryid
		               left join growers g on gp.grower_id=g.id
		              where g.active='active' 
                                and g.market_place = '1'
                              group by s.id order by s.name";

    } else {
        $sel_products_sub = "select s.* 
                               from subcategory s 
		               left join grower_product gp on s.id=gp.subcaegoryid
		               left join growers g on gp.grower_id=g.id
		              where g.active='active' 
                                and g.market_place = '1'
                                and gp.categoryid='" . $_REQUEST['cat_main_id'] . "' 
                              group by s.id order by s.name";

    }

//echo $sel_products_sub;
    $rs_products_sub = mysqli_query($con, $sel_products_sub);
    ?>
    <option value="">All</option>
    <?php
    while ($sub_cat = mysqli_fetch_array($rs_products_sub)) {

        ?>
        <option value="<?php echo $sub_cat['id'] ?>"><?php echo $sub_cat['name'] ?></option>
    <?php } ?>
    <?php

} elseif ($_REQUEST['action'] == "subcatdata") {

    if ($_REQUEST['cat_main_id'] == "" || $_REQUEST['cat_main_id'] == "undefined") {
        
        $sel_products = "select p.* 
                           from product p 
		           left join grower_product gp on p.id=gp.product_id
		           left join growers g on gp.grower_id=g.id
		           left JOIN colors as clr ON clr.id=p.color_id
		          where g.active='active' 
                            and g.market_place = '1'
                          group by p.id limit 0,18";
    } else {
                
        $sel_products = "select p.* 
                           from product p 
		           left join grower_product gp on p.id=gp.product_id
		           left join growers g on gp.grower_id=g.id
		           LEFT JOIN colors as clr ON clr.id=p.color_id
		          where g.active='active' 
                            and g.market_place = '1'
                            AND p.categoryid='" . $_REQUEST['cat_main_id'] . "' 
                          GROUP BY p.id limit 0,12";

    }
     
    
    //echo $sel_products;
    $rs_products = mysqli_query($con, $sel_products);
    $number_row = mysqli_num_rows($rs_products);
    
    $selected_color_array = array();
    
    if ($number_row > 0) {
        while ($products = mysqli_fetch_array($rs_products)) {
            //echo "<pre>";print_r($products);echo "</pre>";exit();
            array_push($selected_color_array, $products['color_id']);
            $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

            $rs_catinfo = mysqli_query($con, $sel_catinfo);

            $catinfo = mysqli_fetch_array($rs_catinfo);

            $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));
            $absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/" . $products['image_path'];
            //if(file_exists($absolute_path))
            //{

            ?>
            <div class="col-md-4 col-sm-4 mix <?php echo $products['categoryid']; ?>"><!-- item -->

                <div class="item-box">
                    <figure>
					<span class="item-hover">
						<span class="overlay dark-5"></span>
						<span class="inner">

							<!-- lightbox -->
							<a class="ico-rounded lightbox" href="<?php echo SITE_URL . "/" . $products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
								<span class="fa fa-plus size-20"></span>
							</a>

                            <!-- details -->
							<a class="ico-rounded" href="<?php echo SITE_URL . '/en/variety-sub-page.php?id=' . $products['id']; ?>">
								<span class="glyphicon glyphicon-option-horizontal size-20"></span>
							</a>

						</span>
					</span>
                        <div class="item-box-overlay-title">
                            <h3><?php echo $products["name"]; ?></h3>
                            <ul class="list-inline categories nomargin">
                                <li><a href="#"><?php echo $catinfo['name'] ?></a></li>
                            </ul>
                        </div>

                        <!-- carousel get_subcat.php-->
                        <div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                            <div>
                                <img class="img-responsive" src="<?php echo SITE_URL . "/" . $products['image_path'] ?>" width="600" height="399" alt="">
                            </div>

                        </div>
                        <!-- /carousel -->

                    </figure>
                </div>

            </div><!-- /item -->
            <?php
            //}
        }
    } else { ?>
        <div class="notfound" style="margin-left: 20px;">No Item Found !</div>
    <?php }

    $imp_color = implode(",", $selected_color_array);

   // $sel_colors = "select * from colors where id IN($imp_color) GROUP BY id order by name";
    $sel_colors = "select * from colors  order by name";    
    $rs_colors = mysqli_query($con, $sel_colors);
    ?>
    #$#$#$#$#$
    <!-- <li data-filter="all" class="filter active"><a href="javascript:void(0);" onclick="get_subcat()">All</a></li> -->

    <h4>COLOR.</h4>
    <?php
    while ($colors = mysqli_fetch_array($rs_colors)) {

        if ($colors["name"] == "Black") { ?>
            <a style="background-color:#000000" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } elseif ($colors["name"] == "Bicolor") { ?>
            <a style="background: linear-gradient(to right, red,orange,yellow,green,blue,indigo,violet);" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>            
        <?php } elseif ($colors["name"] == "Blue") { ?>
            <a style="background-color:#0000CC" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>                        
        <?php } elseif ($colors["name"] == "Burgundy") { ?>
            <a style="background-color:#45001C" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>                        
        <?php } elseif ($colors["name"] == "Earth Tones") { ?>
            <a style="background-color:#D19C4C" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>           
        <?php } elseif ($colors["name"] == "Green") { ?>
            <a style="background-color:#009900" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Hot Pink") { ?>
            <a style="background-color:#7A313D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>            
            
        <?php } elseif ($colors["name"] == "Lavander") { ?>
            <a style="background-color:#7A313D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Light Pink") { ?>
            <a style="background-color:#CEB5B0" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Orange") { ?>
            <a style="background-color:#FF9900" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Peach") { ?>
            <a style="background-color:#D8D97B" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Pink") { ?>
            <a style="background-color:#FFCCCC" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Purple") { ?>
            <a style="background-color:#990099" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Red") { ?>
            <a style="background-color:#FF0000" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "White") { ?>
            <a style="background-color:#FFFFFF" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "White") { ?>
            <a style="background-color:#C8A2C8" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
            
        <?php } elseif ($colors["name"] == "Yellow") { ?>            
            <a style="background-color:#FFFF00" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>                        
        <?php } else {
            ?>
            <a style="background-color:<?php echo $colors["name"]; ?>" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php }
        ?>

        <?php

    }

}


?>
