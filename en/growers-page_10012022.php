<?php


require_once("../config/config_gcp.php");

function pagination($query, $per_page,$page,$con) {        
    $query_product = "select g.id from growers as g 
                       where g.active IN ('active','advertising') 
                         and g.logo = '1'
                       order by g.growers_name";
    
    $query = $query_product;
    $row_cnt=mysqli_num_rows(mysqli_query($con,$query));
    //$total=ceil($rows/$limit);
    
    //$total = $row['num'];
    $total=$row_cnt;
    $adjacents = "2"; 

    $page = ($page == 0 ? 1 : $page);  
    $start = ($page - 1) * $per_page;                               
    
    $counter=1;
    $prev = $page - 1;                          
    $next = $page + 1;
    $lastpage = ceil($total/$per_page);
    $lpm1 = $lastpage - 1;
    
    $pagination = "";
    //echo $page."<".$counter." - 1";
    if($lastpage > 1)
    {   
        $pagination .= "<ul class='pagination'>";
                //$pagination .= "<li class='details'>Page $page of $lastpage</li>";
        
        if ($page > $counter){ 
            $pagination.= "<li><a href='?id=1'>First</a></li>";
            $pagination.= "<li><a href='?id=$prev'>Pervious</a></li>";
        }else{
            $pagination.= "<li class=''><a href='javascript:void(0);' >First</a></li>";
            $pagination.= "<li class=''><a href='javascript:void(0);' >Pervious</a></li>";
        }
                
        if ($lastpage < 7 + ($adjacents * 2))
        {   
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                else
                    $pagination.= "<li><a href='?&id=$counter'>$counter</a></li>";                    
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))
        {
            if($page < 1 + ($adjacents * 2))        
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?id=$counter'>$counter</a></li>";                    
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='?id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='?id=$lastpage'>$lastpage</a></li>";      
            }
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<li><a href='?id=1'>1</a></li>";
                $pagination.= "<li><a href='?id=2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?id=$counter'>$counter</a></li>";                    
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='?id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='?id=$lastpage'>$lastpage</a></li>";      
            }
            else
            {
                $pagination.= "<li><a href='?id=1'>1</a></li>";
                $pagination.= "<li><a href='?id=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?id=$counter'>$counter</a></li>";                    
                }
            }
        }
        
        if ($page < $counter - 1){ 
            $pagination.= "<li><a href='?id=$next'>Next</a></li>";
            $pagination.= "<li><a href='?id=$lastpage'>Last</a></li>";
        }else{
            $pagination.= "<li class=''><a href='javascript:void(0);'>Next</a></li>";
            $pagination.= "<li class=''><a href='javascript:void(0);'>Last</a></li>";
        }
        $pagination.= "</ul>\n";        
    }


    return $pagination;
} 
$limit=10;

$start=0;

if(isset($_GET['id'])) {
    $id=$_GET['id'];
    $start=($id-1)*$limit;
    $prueba= 1;
}


$sel_catinfo="select g.id         , g.growers_name , g.page_desc , g.certificates , g.file_path5 ,
                     g.short_desc , g.active as growers_status   , g.country_id   ,
                     (select TIMESTAMPDIFF(DAY, max(date_added), now()) from invoice_requests where grower_id = g.id ) as dias_transcurridos
                from growers as g 
               where g.active IN ('active','advertising') 
                 and g.logo = '1'
               order by g.growers_name LIMIT $start,$limit  ";

 $rs_catinfo=mysqli_query($con,$sel_catinfo);
 
 
 
 
    $page_id=11;
    $sel_default_meta="select * from page_mgmt where page_id=11";
    $rs_default_meta=mysqli_query($con,$sel_default_meta);
    $default_meta=mysqli_fetch_array($rs_default_meta);
    
     if(isset($_GET["lang"]) && $_GET["lang"]!="")
     {
        $_SESSION["lang"]=$_GET["lang"];
     }
     if(!isset($_SESSION["lang"]))
     {
        $_SESSION["lang"]="en";
     }

#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 11; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . '../includes/assets/css/essentials-flfv3.css', SITE_URL . 'includes/assets/css/layout-flfv3.css',
    SITE_URL . '../includes/assets/css/header-4.css', SITE_URL . '../includes/assets/css/color_scheme/blue.css');
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON############### #################ENDS################################################################

require_once '../includes/header.php';
?>

<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Growers</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->
<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <span class="logo pull-left">
            <a href="https://app.freshlifefloral.com/"> 
                <img src="https://app.freshlifefloral.com/user/logo.png" alt="admin panel" height="60" width="200"> </a>
            <a>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</a>  
            <a href="<?php echo SITE_URL; ?>"><FONT SIZE=4>Home</font></a>            
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>                        
            <a href="<?php echo SITE_URL."en/variety-page.php"; ?>"><FONT SIZE=4>Browse</font></a>  
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>  
            <a href="<?php echo SITE_URL."en/growers-page.php"; ?>"><FONT SIZE=4>Growers</font></a>            
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>                        
            <a href="<?php echo "https://www.freshlifefloral.com/blog/how-it-works-buyers"; ?>"><FONT SIZE=4>How it Works</font></a> 
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>              
            <a href="<?php echo "https://www.freshlifefloral.com/blog"; ?>"><FONT SIZE=4>Blog</font></a>            
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>                        
            <a href="<?php echo "https://www.freshlifefloral.com/blog/contact-us"; ?>"><FONT SIZE=4>Contacts</font></a>                                    
        </span>  

        <!-- /breadcrumbs -->

        <?php
                $sqlGrow = "select id,growers_name 
                              from growers 
                             where active='active'
                               and logo = '1'
                             order by growers_name ";
                
                    $rs_grow = mysqli_query($con,$sqlGrow);
        
        ?>
        <!-- Consulta Grower-->
        
        <div class="text-center">            
                <select class="form-control select2" id="vari_change" name="vari_change">	
                                    <option value="">Growers</option>
                    <?php
                            while ($row_grow = mysqli_fetch_array($rs_grow)) {
                     ?>
                            <option value="<?php echo $row_grow['id'] ?>"><?php echo $row_grow['growers_name'] ?></option>
                                        
                    <?php } ?>
                </select>        
        </div>
        
        <br>
        
       
    </div>
    
    
</section>
<!-- -->
<section>
                <div class="container">
                    
                    <div class="row">

                        <!-- LEFT COLUMNS -->
                        <div class="col-md-9 col-sm-9 col-md-push-3 col-sm-push-3" id="result_data">
                            <?php while($catinfo=mysqli_fetch_array($rs_catinfo))
                            { 
                                //echo "<pre>";print_r($catinfo);echo "</pre>";
                                $growers_status=$catinfo['growers_status'];
                                
                                $sel_country="SELECT * from country where id='".$catinfo['country_id']."'";
                                //echo $sel_country;
                                $rs_country=mysqli_query($con,$sel_country);
                                $row_Country=mysqli_fetch_array($rs_country);
                                ?>
                                <div class="row"><!-- item -->
                                    <div class="col-md-2"><!-- company logo -->
                                        <?php
                                        if($catinfo['file_path5'] != "")
                                        { 
                                        ?>
                                            <img src="<?php echo SITE_URL."user/".$catinfo['file_path5'];?>" class="img-responsive growers_logo" alt="company logo88888">
                                        <?php }
                                        else
                                        { ?>
                                            <img src="../assets/images/demo/people/300x300/no_user.png" class="img-responsive" alt="company logo">
                                        <?php } ?>  
                                    </div>

                                    <div class="col-md-10"><!-- company detail -->
                                        <h4 class="margin-bottom-10"><a href="<?php echo SITE_URL."en/single-growers.php?id=".$catinfo['id'];?>"><?php echo $catinfo['growers_name']." (".$catinfo['dias_transcurridos'].")";?></a></h4>
                                        <ul class="list-inline">
                                             <?php 
                                                if($row_Country['name'] != "")
                                                {
                                                    ?>
                                                    <li><!-- i class="fa fa-map-marker color-green"></i> -->
                                                        <img src="<?php echo SITE_URL;?>../includes/assets/images/flags/<?php echo $row_Country['flag']?>" />
                                                        <?php 
                                                    echo $row_Country['name']." ";
                                                    ?>
                                                    </li>
                                                    <?php 
                                                }
                                                
                                                ?>
                                            
                                                <?php 
                                                if($growers_status != "")
                                                {?>
                                                    <li>
                                                   <?php 
                                                    if($growers_status == "active")
                                                    { 
                                                        echo '<i class="fa fa-info-circle color-green"></i>';
                                                        echo "Premium Member";
                                                    }
                                                    /*else if($growers_status == "advertising")
                                                    {
                                                        echo '<i class="fa fa-info-circle color-green"></i>';
                                                        echo "Regular Member";
                                                    }*/
                                                    $review_p="select * from review_rating where grower_id='".$catinfo['id']."'";
                                                    //echo $review_p;
                                                    $rs_review_p=mysqli_query($con,$review_p);
                                                    $total_rating=0;
                                                    $cnt_row=mysqli_num_rows($rs_review_p);
                                                    if($cnt_row > 0)
                                                    {
                                                        while($review_detail=mysqli_fetch_array($rs_review_p))
                                                        {
                                                            $total_rating+=$review_detail['final_rating'];
                                                        }   
                                                    }
                                                    $f_rating=0;
                                                    if($total_rating != 0)
                                                    {
                                                        $f_rating=($total_rating*5/($cnt_row*5));
                                                    }   

                                                    ?>  
                                                        &nbsp;&nbsp;&nbsp;
                                                        <span class="size-14 text-muted"><!-- stars -->
                                                            <?php
                                                            for($i=1;$i<=5;$i++)
                                                            {
                                                                if($i <= round($f_rating))
                                                                { ?>
                                                                    <i class="fa fa-star"></i>
                                                                <?php }
                                                                else{
                                                                    ?>
                                                                    <i class="fa fa-star-o"></i>
                                                                <?php }   
                                                            }
                                                            ?>
                                                         </span>    
                                                    </li>
                                                    <?php 
                                                }
                                                ?>
                                           
                                        </ul>
                                        <p><?php echo $catinfo['short_desc'];?></p>
                                    </div>
                                </div><!-- /item -->
                                <hr>
                            <?php } ?>

                            <?php 
                                $page = (int) (!isset($_GET["id"]) ? 1 : $_GET["id"]);
                                echo pagination("growers",$limit,$page,$con);
                            ?>

                        </div>
                        <!-- /LEFT COLUMNS -->

                        <!-- RIGHT COLUMNS -->
                        <div class="col-md-3 col-sm-3 col-md-pull-9 col-sm-pull-9">

                            <h4><strong><?php echo $default_meta["heading1"]; ?></strong> </h4>
                            <p><em><?php echo $default_meta["para_1"]; ?></em></p>

                            <hr>

                            <ul class="list-unstyled"><!-- block 1 -->
                            <?php 
                            $para2=explode(":",$default_meta["para_2"]);
                            for($i=0;$i<count($para2);$i++){
                            if($para2[$i] != ''){
                            ?>
                                <li><a href="#"><?php echo $para2[$i];?></a></li>
                                
                                <?php }}?>
                            </ul><!-- /block 1 -->

                            <hr>

                            <h4><strong>Contact</strong> Us</h4>
                            <!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
<style type="text/css">
	.hbspt-form input[type="submit"] {
    margin-top: 90px;
    margin-left: 95px;
}
</style>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
<script>
  hbspt.forms.create({ 
    css: '',
    portalId: '295496',
    formId: 'cb0eb96a-aeaa-4f0a-a1d5-41544d8480a1'
  });
</script>


                        </div>
                        <!-- /RIGHT COLUMNS -->

                    </div>
                    
                </div>
            </section>
            <!-- / -->
<?php
require_once '../includes/footer.php';
?>
            
<script type="text/javascript">
    
    
    jQuery(function () {
        
                        jQuery("#vari_change").change(function () {

                            var vari_id = jQuery("#vari_change").val();
                            var dataString = 'varieties_checked=' + vari_id;                            

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>en/search_grower.php",
                                data: dataString,
                                success: function (html)   {
                                    var split_html = html.split("#$#$#$#$#$");
                                    jQuery("#result_data").html(split_html[0]);

                                }
                            });
        
            });
      });
   
   
   
                    if (jQuery().dataTable) {

                        function initTable1() {
                            var table = jQuery('#sample_1');

                            /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

                            /* Set tabletools buttons and button container */

                            $.extend(true, $.fn.DataTable.TableTools.classes, {
                                "container": "btn-group pull-right tabletools-topbar",
                                "buttons": {
                                    "normal": "btn btn-sm btn-default",
                                    "disabled": "btn btn-sm btn-default disabled"
                                },
                                "collection": {
                                    "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
                                }
                            });

                            var oTable = table.dataTable({
                                "order": [
                                    [0, 'asc']
                                ],
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                // set the initial value
                                "pageLength": 10,
                                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

                                "tableTools": {
                                    "sSwfPath": "assets/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                                    "aButtons": [{
                                            "sExtends": "pdf",
                                            "sButtonText": "PDF"
                                        }, {
                                            "sExtends": "csv",
                                            "sButtonText": "CSV"
                                        }, {
                                            "sExtends": "xls",
                                            "sButtonText": "Excel"
                                        }, {
                                            "sExtends": "print",
                                            "sButtonText": "Print",
                                            "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                                            "sMessage": "Generated by DataTables"
                                        }]
                                }
                            });

                            var tableWrapper = jQuery('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

                            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
                        }

                        function initTable2() {
                            var table = jQuery('#sample_2');

                            /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

                            /* Set tabletools buttons and button container */

                            $.extend(true, $.fn.DataTable.TableTools.classes, {
                                "container": "btn-group tabletools-btn-group pull-right",
                                "buttons": {
                                    "normal": "btn btn-sm btn-default",
                                    "disabled": "btn btn-sm btn-default disabled"
                                }
                            });

                            var oTable = table.dataTable({
                                "order": [
                                    [0, 'asc']
                                ],
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                // set the initial value
                                "pageLength": 10,
                                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

                                "tableTools": {
                                    "sSwfPath": "assets/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                                    "aButtons": [{
                                            "sExtends": "pdf",
                                            "sButtonText": "PDF"
                                        }, {
                                            "sExtends": "csv",
                                            "sButtonText": "CSV"
                                        }, {
                                            "sExtends": "xls",
                                            "sButtonText": "Excel"
                                        }, {
                                            "sExtends": "print",
                                            "sButtonText": "Print",
                                            "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
                                            "sMessage": "Generated by DataTables"
                                        }, {
                                            "sExtends": "copy",
                                            "sButtonText": "Copy"
                                        }]
                                }
                            });

                            var tableWrapper = jQuery('#sample_2_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
                            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
                        }

                        function initTable3() {
                            var table = jQuery('#sample_3');

                            /* Formatting function for row expanded details */
                            function fnFormatDetails(oTable, nTr) {
                                var aData = oTable.fnGetData(nTr);
                                var sOut = '<table>';
                                sOut += '<tr><td>Platform(s):</td><td>' + aData[2] + '</td></tr>';
                                sOut += '<tr><td>Engine version:</td><td>' + aData[3] + '</td></tr>';
                                sOut += '<tr><td>CSS grade:</td><td>' + aData[4] + '</td></tr>';
                                sOut += '<tr><td>Others:</td><td>Could provide a link here</td></tr>';
                                sOut += '</table>';

                                return sOut;
                            }

                            /*
                             * Insert a 'details' column to the table
                             */
                            var nCloneTh = document.createElement('th');
                            nCloneTh.className = "table-checkbox";

                            var nCloneTd = document.createElement('td');
                            nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';

                            table.find('thead tr').each(function () {
                                this.insertBefore(nCloneTh, this.childNodes[0]);
                            });

                            table.find('tbody tr').each(function () {
                                this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
                            });

                            var oTable = table.dataTable({
                                "columnDefs": [{
                                        "orderable": false,
                                        "targets": [0]
                                    }],
                                "order": [
                                    [1, 'asc']
                                ],
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                // set the initial value
                                "pageLength": 10,
                            });

                            var tableWrapper = jQuery('#sample_3_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
                            var tableColumnToggler = jQuery('#sample_3_column_toggler');

                            /* modify datatable control inputs */
                            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

                            /* Add event listener for opening and closing details
                             * Note that the indicator for showing which row is open is not controlled by DataTables,
                             * rather it is done here
                             */
                            table.on('click', ' tbody td .row-details', function () {
                                var nTr = jQuery(this).parents('tr')[0];
                                if (oTable.fnIsOpen(nTr)) {
                                    /* This row is already open - close it */
                                    jQuery(this).addClass("row-details-close").removeClass("row-details-open");
                                    oTable.fnClose(nTr);
                                } else {
                                    /* Open this row */
                                    jQuery(this).addClass("row-details-open").removeClass("row-details-close");
                                    oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
                                }
                            });

                            /* handle show/hide columns*/
                            jQuery('input[type="checkbox"]', tableColumnToggler).change(function () {
                                /* Get the DataTables object again - this is not a recreation, just a get of the object */
                                var iCol = parseInt(jQuery(this).attr("data-column"));
                                var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
                                oTable.fnSetColumnVis(iCol, (bVis ? false : true));
                            });
                        }

                        function initTable4() {

                            var table = jQuery('#sample_4');

                            /* Fixed header extension: http://datatables.net/extensions/scroller/ */

                            var oTable = table.dataTable({
                                "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // datatable layout without  horizobtal scroll
                                "scrollY": "300",
                                "deferRender": true,
                                "order": [
                                    [0, 'asc']
                                ],
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                "pageLength": 10 // set the initial value            
                            });


                            var tableWrapper = jQuery('#sample_4_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
                            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
                        }

                        function initTable5() {

                            var table = jQuery('#sample_5');

                            /* Fixed header extension: http://datatables.net/extensions/keytable/ */

                            var oTable = table.dataTable({
                                "order": [
                                    [0, 'asc']
                                ],
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                "pageLength": 10, // set the initial value,
                                "columnDefs": [{// set default column settings
                                        'orderable': false,
                                        'targets': [0]
                                    }, {
                                        "searchable": false,
                                        "targets": [0]
                                    }],
                                "order": [
                                    [1, "asc"]
                                ]
                            });

                            var oTableColReorder = new $.fn.dataTable.ColReorder(oTable);

                            var tableWrapper = jQuery('#sample_5_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
                            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
                        }

                        function initTable6() {
                            var table = jQuery('#datatable_sample');

                            table.dataTable({
                                "columns": [{
                                        "orderable": false
                                    }, {
                                        "orderable": true
                                    }, {
                                        "orderable": false
                                    }, {
                                        "orderable": false
                                    }, {
                                        "orderable": true
                                    }, {
                                        "orderable": false
                                    }],
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                // set the initial value
                                "pageLength": 5,
                                "pagingType": "bootstrap_full_number",
                                "language": {
                                    "lengthMenu": "  _MENU_ records",
                                    "paginate": {
                                        "previous": "Prev",
                                        "next": "Next",
                                        "last": "Last",
                                        "first": "First"
                                    }
                                },
                                "columnDefs": [{// set default column settings
                                        'orderable': false,
                                        'targets': [0]
                                    }, {
                                        "searchable": false,
                                        "targets": [0]
                                    }],
                                "order": [
                                    [1, "asc"]
                                ] // set first column as a default sort by asc
                            });

                            var tableWrapper = jQuery('#datatable_sample_wrapper');

                            table.find('.group-checkable').change(function () {
                                var set = jQuery(this).attr("data-set");
                                var checked = jQuery(this).is(":checked");
                                jQuery(set).each(function () {
                                    if (checked) {
                                        jQuery(this).attr("checked", true);
                                        jQuery(this).parents('tr').addClass("active");
                                    } else {
                                        jQuery(this).attr("checked", false);
                                        jQuery(this).parents('tr').removeClass("active");
                                    }
                                });
                                jQuery.uniform.update(set);
                            });

                            table.on('change', 'tbody tr .checkboxes', function () {
                                jQuery(this).parents('tr').toggleClass("active");
                            });

                            tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown

                        }

                        function initTable7() {

                            function restoreRow(oTable, nRow) {
                                var aData = oTable.fnGetData(nRow);
                                var jqTds = $('>td', nRow);

                                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                                    oTable.fnUpdate(aData[i], nRow, i, false);
                                }

                                oTable.fnDraw();
                            }

                            function editRow(oTable, nRow) {
                                var aData = oTable.fnGetData(nRow);
                                var jqTds = $('>td', nRow);
                                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
                                jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
                            }

                            function saveRow(oTable, nRow) {
                                var jqInputs = $('input', nRow);
                                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
                                oTable.fnDraw();
                            }

                            function cancelEditRow(oTable, nRow) {
                                var jqInputs = $('input', nRow);
                                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                                oTable.fnDraw();
                            }

                            var table = $('#sample_editable_1');

                            var oTable = table.dataTable({
                                "lengthMenu": [
                                    [5, 15, 20, -1],
                                    [5, 15, 20, "All"] // change per page values here
                                ],
                                // set the initial value
                                "pageLength": 10,
                                "language": {
                                    "lengthMenu": " _MENU_ records"
                                },
                                "columnDefs": [{// set default column settings
                                        'orderable': true,
                                        'targets': [0]
                                    }, {
                                        "searchable": true,
                                        "targets": [0]
                                    }],
                                "order": [
                                    [0, "asc"]
                                ] // set first column as a default sort by asc
                            });

                            var tableWrapper = $("#sample_editable_1_wrapper");

                            tableWrapper.find(".dataTables_length select").select2({
                                showSearchInput: false //hide search box with special css class
                            }); // initialize select2 dropdown

                            var nEditing = null;
                            var nNew = false;

                            $('#sample_editable_1_new').click(function (e) {
                                e.preventDefault();

                                if (nNew && nEditing) {
                                    if (confirm("Previose row not saved. Do you want to save it ?")) {
                                        saveRow(oTable, nEditing); // save
                                        $(nEditing).find("td:first").html("Untitled");
                                        nEditing = null;
                                        nNew = false;

                                    } else {
                                        oTable.fnDeleteRow(nEditing); // cancel
                                        nEditing = null;
                                        nNew = false;

                                        return;
                                    }
                                }

                                var aiNew = oTable.fnAddData(['', '', '', '', '', '']);
                                var nRow = oTable.fnGetNodes(aiNew[0]);
                                editRow(oTable, nRow);
                                nEditing = nRow;
                                nNew = true;
                            });

                            table.on('click', '.delete', function (e) {
                                e.preventDefault();

                                if (confirm("Are you sure to delete this row ?") == false) {
                                    return;
                                }

                                var nRow = $(this).parents('tr')[0];
                                oTable.fnDeleteRow(nRow);
                                alert("Deleted! Do not forget to do some ajax to sync with backend :)");
                            });

                            table.on('click', '.cancel', function (e) {
                                e.preventDefault();

                                if (nNew) {
                                    oTable.fnDeleteRow(nEditing);
                                    nNew = false;
                                } else {
                                    restoreRow(oTable, nEditing);
                                    nEditing = null;
                                }
                            });

                            table.on('click', '.edit', function (e) {
                                e.preventDefault();

                                /* Get the row as a parent of the link that was clicked on */
                                var nRow = $(this).parents('tr')[0];

                                if (nEditing !== null && nEditing != nRow) {
                                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                                    restoreRow(oTable, nEditing);
                                    editRow(oTable, nRow);
                                    nEditing = nRow;
                                } else if (nEditing == nRow && this.innerHTML == "Save") {
                                    /* Editing this row and want to save it */
                                    saveRow(oTable, nEditing);
                                    nEditing = null;
                                    alert("Updated! Do not forget to do some ajax to sync with backend :)");
                                } else {
                                    /* No edit in progress - let's start one */
                                    editRow(oTable, nRow);
                                    nEditing = nRow;
                                }
                            });

                        }


                        initTable1();
                        initTable2();
                        initTable3();
                        initTable4();
                        initTable5();
                        initTable6();
                        initTable7();

                    }

</script>

</body>
</html>
