<?php
require_once("../config/config_gcp.php");
//require_once("../functions/functions.php");
$growerId = $_GET['id_grow'];
$sel_catinfo = "select g.id,g.growers_name,g.page_desc,g.certificates,g.file_path5,g.short_desc,g.country_id,con.name as country_name 
                from growers as g
                LEFT JOIN country as con ON con.id=g.country_id 
                where g.id=$growerId";

   $grow = $_REQUEST['id'];
   
$rs_catinfo = mysqli_query($con, $sel_catinfo);
$catinfo = mysqli_fetch_array($rs_catinfo);
/* * **** Code To show Buyer and User Ratings in % ****** */
if (isset($_GET['id']) && $_GET['id'] > 0) {

    $sql = "SELECT AVG(ur_quality_rating) AS buyer_quality_rating, AVG(ur_freshness_rating) AS buyer_freshness_rating, AVG(ur_trustworthiness_rating) AS buyer_trustworthiness_rating, AVG(ur_pricing_rating) AS buyer_pricing_rating, AVG(ur_packing_rating) AS buyer_packing_rating, AVG(ur_final_rating) AS buyer_final_rating FROM user_ratings WHERE ur_status = 1 AND ur_user_type = 'buyer' AND ur_grower_idFk = $growerId"; // Buyer average rating
    $result = mysqli_query($con, $sql);
    $buyer_rating = mysqli_fetch_array($result);
    //var_dump($buyer_rating);
    $buyer_quality_percentage = round((100 / 5) * $buyer_rating['buyer_quality_rating']);
    $buyer_freshness_percentage = round((100 / 5) * $buyer_rating['buyer_freshness_rating']);
    $buyer_trustworthiness_percentage = round((100 / 5) * $buyer_rating['buyer_trustworthiness_rating']);
    $buyer_pricing_percentage = round((100 / 5) * $buyer_rating['buyer_pricing_rating']);
    $buyer_packing_percentage = round((100 / 5) * $buyer_rating['buyer_packing_rating']);
    $buyer_rating_percentage = round((100 / 5) * $buyer_rating['buyer_final_rating']);
    $sql1 = "SELECT AVG(ur_quality_rating) AS user_quality_rating, AVG(ur_freshness_rating) AS user_freshness_rating, AVG(ur_trustworthiness_rating) AS user_trustworthiness_rating, AVG(ur_pricing_rating) AS user_pricing_rating, AVG(ur_packing_rating) AS user_packing_rating, AVG(ur_final_rating) AS user_final_rating FROM user_ratings WHERE ur_status = 1 AND ur_user_type = 'user' AND ur_grower_idFk = $growerId"; // End User average rating
    $result1 = mysqli_query($con, $sql1);
    $user_rating = mysqli_fetch_array($result1);
    $user_quality_percentage = round((100 / 5) * $user_rating['user_quality_rating']);
    $user_freshness_percentage = round((100 / 5) * $user_rating['user_freshness_rating']);
    $user_trustworthiness_percentage = round((100 / 5) * $user_rating['user_trustworthiness_rating']);
    $user_pricing_percentage = round((100 / 5) * $user_rating['user_pricing_rating']);
    $user_packing_percentage = round((100 / 5) * $user_rating['user_packing_rating']);
    $user_rating_percentage = round((100 / 5) * $user_rating['user_final_rating']);
    
    $sql1 = "SELECT AVG(ur_quality_rating) AS admin_quality_rating, AVG(ur_freshness_rating) AS admin_freshness_rating, AVG(ur_trustworthiness_rating) AS admin_trustworthiness_rating, AVG(ur_pricing_rating) AS admin_pricing_rating, AVG(ur_packing_rating) AS admin_packing_rating, AVG(ur_final_rating) AS admin_final_rating FROM user_ratings WHERE ur_status = 1 AND ur_user_type = 'admin' AND ur_grower_idFk = $growerId"; // Admin average rating
    $result1 = mysqli_query($con, $sql1);
    $admin_rating = mysqli_fetch_array($result1);
    $admin_quality_percentage = round((100 / 5) * $admin_rating['admin_quality_rating']);
    $admin_freshness_percentage = round((100 / 5) * $admin_rating['admin_freshness_rating']);
    $admin_trustworthiness_percentage = round((100 / 5) * $admin_rating['admin_trustworthiness_rating']);
    $admin_pricing_percentage = round((100 / 5) * $admin_rating['admin_pricing_rating']);
    $admin_packing_percentage = round((100 / 5) * $admin_rating['admin_packing_rating']);
    $admin_rating_percentage = round((100 / 5) * $admin_rating['admin_final_rating']);
    
    $all_quality_percentage = round(($buyer_quality_percentage + $user_quality_percentage + $admin_quality_percentage)/3);
    $all_freshness_percentage = round(($buyer_freshness_percentage + $user_freshness_percentage + $admin_freshness_percentage)/3);
    $all_trustworthiness_percentage = round(($buyer_trustworthiness_percentage + $user_trustworthiness_percentage + $admin_trustworthiness_percentage)/3);
    $all_pricing_percentage = round(($buyer_pricing_percentage + $user_pricing_percentage + $admin_pricing_percentage)/3);
    $all_packing_percentage = round(($buyer_packing_percentage + $user_packing_percentage + $admin_packing_percentage)/3);
    
}
/* * **** End Code To show Buyer and User Ratings in % ****** */
/* * **** Fetch and Show user ratings ****** */

if ($_GET['id']) {
    $sql_user_rating = "SELECT DISTINCT(user_ratings.ur_user_idFk), user_ratings.ur_comment, user_ratings.ur_final_rating, ur_create_datetime, users.u_first_name, users.u_last_name, users.u_gender, users.u_picture FROM user_ratings LEFT JOIN users ON user_ratings.ur_user_idFk = users.u_id WHERE ur_grower_idFk = " . $growerId;
    $result_user_rating = mysqli_query($con, $sql_user_rating);
}


/* * **** End Fetch and Show user ratings ****** */

function pagination_p($query, $per_page, $page, $con, $growers_id, $subcate_id = "", $url) {
    $sel_catinfo_p = "select gp.prodcutname,gp.prodcutid,gp.price,gp.sizename,feat.name as features_name,sub_cat.name as sub_cat_name
                            from grower_product_price as gp 
                            LEFT JOIN features as feat ON feat.id=gp.feature 
                            INNER JOIN product as p ON p.id=gp.prodcutid
                            INNER JOIN  subcategory sub_cat ON sub_cat.id=p.subcategoryid
                            where gp.growerid='" . $growers_id . "' GROUP BY gp.prodcutid,gp.feature ORDER BY gp.prodcutid";
    $row_cnt = mysqli_num_rows(mysqli_query($con, $sel_catinfo_p));
    $total = $row_cnt;
    $adjacents = "2";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $counter = 1;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total / $per_page);
    $lpm1 = $lastpage - 1;
    $pagination = "";

    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination'>";
        //$pagination .= "<li class='details'>Page $page of $lastpage</li>";

        if ($page > $counter) {
            $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=1'>First</a></li>";
            $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$prev'>Pervious</a></li>";
        } else {
            $pagination.= "<li class=''><a href='javascript:void(0);' >First</a></li>";
            $pagination.= "<li class=''><a href='javascript:void(0);' >Pervious</a></li>";
        }

        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                else
                    $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$counter'>$counter</a></li>";
            }
        }
        elseif ($lastpage > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>...</li>";
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$lastpage'>$lastpage</a></li>";
            }
            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=1'>1</a></li>";
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=2'>2</a></li>";
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>..</li>";
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$lastpage'>$lastpage</a></li>";
            }
            else {
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=1'>1</a></li>";
                $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=2'>2</a></li>";
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$counter'>$counter</a></li>";
                }
            }
        }

        if ($page < $counter - 1) {
            $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$next'>Next</a></li>";
            $pagination.= "<li><a href='?{$url}&p_page=yes&v_id=$lastpage'>Last</a></li>";
        } else {
            $pagination.= "<li class=''><a href='javascript:void(0);'>Next</a></li>";
            $pagination.= "<li class=''><a href='javascript:void(0);'>Last</a></li>";
        }
        $pagination.= "</ul>\n";
    }


    return $pagination;
}

function pagination($query, $per_page, $page, $con, $growers_id, $subcate_id = "", $url) {
    if ($subcate_id != "") {
        $wh = " AND gp.subcaegoryid='" . $_REQUEST['subcate_id'] . "'";
    }
    $sel_catinfo_v = "select p.image_path,p.name from grower_product as gp 
                            INNER JOIN product as p ON p.id=gp.product_id
                            where gp.grower_id='" . $growers_id . "' $wh ORDER BY p.name";
    $row_cnt = mysqli_num_rows(mysqli_query($con, $sel_catinfo_v));
    //$total=ceil($rows/$limit);
    //$row = mysql_fetch_array(mysql_query($query));
    //$total = $row['num'];
    $total = $row_cnt;
    $adjacents = "2";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $counter = 1;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total / $per_page);
    $lpm1 = $lastpage - 1;

    $pagination = "";
    //echo $page."<".$counter." - 1";
    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination'>";
        //$pagination .= "<li class='details'>Page $page of $lastpage</li>";

        if ($page > $counter) {
            $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=1'>First</a></li>";
            $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$prev'>Pervious</a></li>";
        } else {
            $pagination.= "<li class=''><a href='javascript:void(0);' >First</a></li>";
            $pagination.= "<li class=''><a href='javascript:void(0);' >Pervious</a></li>";
        }

        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                else
                    $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$counter'>$counter</a></li>";
            }
        }
        elseif ($lastpage > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>...</li>";
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$lastpage'>$lastpage</a></li>";
            }
            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=1'>1</a></li>";
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=2'>2</a></li>";
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$counter'>$counter</a></li>";
                }
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>..</li>";
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$lastpage'>$lastpage</a></li>";
            }
            else {
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=1'>1</a></li>";
                $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=2'>2</a></li>";
                $pagination.= "<li class='dot' style='float:left;margin-right:5px;'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$counter'>$counter</a></li>";
                }
            }
        }

        if ($page < $counter - 1) {
            $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$next'>Next</a></li>";
            $pagination.= "<li><a href='?{$url}&v_page=yes&v_id=$lastpage'>Last</a></li>";
        } else {
            $pagination.= "<li class=''><a href='javascript:void(0);'>Next</a></li>";
            $pagination.= "<li class=''><a href='javascript:void(0);'>Last</a></li>";
        }
        $pagination.= "</ul>\n";
    }


    return $pagination;
}

error_reporting(0);
$cssHeadArray = array(SITE_URL . '../includes/assets/css/essentials-flfv3.css', SITE_URL . 'includes/assets/css/layout-flfv3.css',
    SITE_URL . '../includes/assets/css/header-4.css', SITE_URL . '../includes/assets/css/color_scheme/blue.css', SITE_URL . '../includes/assets/css/custom.css', SITE_URL . '/assets/plugins/slider.revolution.v5/fonts/font-awesome/css/font-awesome.min.css');
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################
$pageId = 21; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
require_once '../includes/header.php';



/* * *** Code for posting comment and rating  ***** */
if (isset($_POST['post-comment'])) {
    $user_id = $_SESSION['user_id'];
    $user_type = 'user';
    $quality = $_POST["quality_rating"];
    $freshness = $_POST["freshness_rating"];
    $trustworthiness = $_POST["trustworthiness_rating"];
    $pricing = $_POST["pricing_rating"];
    $packing = $_POST["packing_rating"];
    $comment = $_POST["comment"];
    $create_date = date("Y-m-d H:i:s");
    $final_rating = (($quality + $freshness + $trustworthiness + $pricing + $packing) * 5) / 25;
    $sql1 = "SELECT ur_id FROM user_ratings WHERE ur_user_idFk = '" . $user_id . "' AND ur_grower_idFk = '" . $growerId . "'";
    $result1 = mysqli_query($con, $sql1);
    if (mysqli_num_rows($result1) > 0) {
        ?>
        <script>window.onload = function () {
                $('#danger-alert1 > span').html(' You have already rated this grower.');
                $('#danger-alert1').show();
                $('#c_reviews').click();
            };
            </script>
        <?php
    } else {
        $sql = "INSERT INTO user_ratings (ur_quality_rating, ur_freshness_rating, ur_trustworthiness_rating, ur_pricing_rating, ur_packing_rating, ur_final_rating, ur_comment, ur_user_idFk, ur_user_type, ur_grower_idFk, ur_create_datetime) VALUES(" . $quality . "," . $freshness . "," . $trustworthiness . "," . $pricing . "," . $packing . "," . $final_rating . ",'" . $comment . "'," . $user_id . ", '" . $user_type . "', " . $growerId . ", '" . $create_date . "')";
        $res = mysqli_query($con, $sql);
        if (mysqli_insert_id($con) > 0) {
            $suer_data = "SELECT u_first_name, u_last_name FROM users WHERE u_id= " . $user_id;
            $user_res = mysqli_query($con, $suer_data);
            $user = mysqli_fetch_array($user_res);
            
            $grower_data = "SELECT growers_name FROM growers WHERE id= " . $growerId;
            $grower_res = mysqli_query($con, $grower_data);
            $grower_details = mysqli_fetch_array($grower_res);

            $email_subject = "User Rating For Grower"; // The Subject of the email
            $email_message = "Hi FreshLifeFloral Admin, <br><br> One User commented/reviewed on a grower. Please Click 'Approve Comment' link if you want to display this review under Grower Section. <br><br> User Full Name: " . $user['u_first_name'] . " " . $user['u_last_name'] . "<br><br> Comment: " . $comment . " <br><br> User Ratings: ". $final_rating ." <br><br> Grower Full Name: ".$grower_details['growers_name']." <br><br> Grower's Profile Link: <a href='".SITE_URL."single-growers.php?id=".$growerId."'> Grower Profile </a> <br><br> Link: <a href='" . SITE_URL . "approve_comment.php?id=" . mysqli_insert_id($con) . "'>Approve Comment</a> <br><br>  Thank You"; // Message that the email has in it

            date_default_timezone_set('Asia/Karachi');
            /*             * *** Exists in functins.php  ***** */
            send_mail($email_subject, $email_message);
            ?>
            <script>$(function () {
                    $('#success-alert1 > span').html(' You have successfully rated this grower and your comment is pending for approval.');
                    $('#success-alert1').show();
                    $('#c_reviews').click();
                });</script>
            <?php
        }
    }
}

/* * *** End Code for posting comment and rating  ***** */
?>
<section class="page-header page-header-xs">
    <div class="container">
        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Single Grower</li>
        </ol><!-- /breadcrumbs -->
    </div>
</section>
<!-- /PAGE HEADER -->

<style type="text/css">
    .list-unstyled > li{
        font-size: 13px;
    }

</style>
<!-- -->
                      <?php      require_once '../en/portfolio-detail-1.php';?>

<section>
    <div class="container">
          
        <!-- LEFT -->
        <div class="col-lg-3 col-md-3 col-sm-4">
            
            <div class="thumbnail text-center">
                <?php

                    if($catinfo['file_path5'] == ''){
                        $pic_path = SITE_URL."../includes/assets/images/demo/people/300x300/no_user.png";
                    }
                    else{
                        $pic_path = SITE_URL . "user/" . $catinfo['file_path5'];
                    }
                    
                ?>
                <img src="<?php echo $pic_path; ?>" alt="Grower Picture" width="100%" height="200"/>
                <h2 class="size-18 margin-top-10 margin-bottom-0"><?php echo $catinfo['growers_name']; ?></h2>
                <h3 class="size-11 margin-top-0 margin-bottom-10 text-muted"><?php echo $catinfo['country_name']; ?></h3>
            </div>

            <!-- SIDE NAV -->

            <ul class="side-nav list-group margin-bottom-60" id="sidebar-nav">
                <?php
                if ($_REQUEST['v_page'] == "yes") {
                    ?>
                    <li class="list-group-item active"><a href="#v2" data-toggle="tab"><i class="fa fa-list"></i> VARITIES</a></li>
                    <?php
                } else {
                    ?>
                    <li class="list-group-item"><a href="#v2" data-toggle="tab"><i class="fa fa-list"></i> VARITIES</a></li>
<?php }
?>


                <li class="list-group-item"><a href="#v3" data-toggle="tab"><i class="fa fa-money"></i> PRICES</a></li>
                <li class="list-group-item"><a href="#v4" data-toggle="tab"><i class="fa fa-cubes"></i> AVAILABILITY</a></li>
                <li class="list-group-item"><a href="#v7" data-toggle="tab" id="c_reviews"><i class="fa fa-star-half-o"></i> CUSTOMER REVIEWS</a></li>



            </ul>
            <!-- /SIDE NAV -->

            <!-- Small progress bars here   --->
            <h5 class="nomargin">
                <span class="pull-right"><?php echo ($all_quality_percentage != '')? $all_quality_percentage: '0'; ?>%</span>Quality</h5>
            <div class="progress progress-xs"><!-- progress bar -->
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo ($all_quality_percentage != '')? $all_quality_percentage: '0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($all_quality_percentage != '')? $all_quality_percentage: '0'; ?>%;"></div>
            </div><!-- /progress bar -->

            <h5 class="nomargin">
                <span class="pull-right"><?php echo ($all_freshness_percentage != '')? $all_freshness_percentage: '0'; ?>%</span>Freshness</h5>
            <div class="progress progress-xs"><!-- progress bar -->
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo ($all_freshness_percentage != '')? $all_freshness_percentage: '0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($all_freshness_percentage != '')? $all_freshness_percentage: '0'; ?>%;"></div>
            </div><!-- /progress bar -->

            <h5 class="nomargin">
                <span class="pull-right"><?php echo ($all_trustworthiness_percentage != '')? $all_trustworthiness_percentage: '0'; ?>%</span>Trustworthiness  </h5>
            <div class="progress progress-xs"><!-- progress bar -->
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo ($all_trustworthiness_percentage != '')? $all_trustworthiness_percentage: '0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($all_trustworthiness_percentage != '')? $all_trustworthiness_percentage: '0'; ?>%;"></div>
            </div><!-- /progress bar -->

            <h5 class="nomargin"><span class="pull-right"><?php echo ($all_pricing_percentage != '')? $all_pricing_percentage: '0'; ?>%</span>Pricing </h5>
            <div class="progress progress-xs"><!-- progress bar -->
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo ($all_pricing_percentage != '')? $all_pricing_percentage: '0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($all_pricing_percentage != '')? $all_pricing_percentage: '0'; ?>%;"></div>
            </div><!-- /progress bar -->
            <h5 class="nomargin">
                <span class="pull-right"><?php echo ($all_packing_percentage != '')? $all_packing_percentage: '0'; ?>%</span>Packing </h5>
            <div class="progress progress-xs"><!-- progress bar -->
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo ($all_packing_percentage != '')? $all_packing_percentage: '0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($all_packing_percentage != '')? $all_packing_percentage: '0'; ?>%;"></div>
            </div><!-- /progress bar -->
            <!-- Small progress bars end here --->
            <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center" style="min-height: 264px;">
                <div class="front">
                    <div style="background-color: rgb(115, 185, 220); min-height: 264px;" class="box1">
                        <div class="box-icon-title">
                            <i class="fa fa-info"></i>
                            <h2><?php echo $pageData["box1_title"]; ?></h2>
                        </div>
                        <p><?php echo $pageData["box1_desc"]; ?></p>
                    </div>
                </div>

                <div class="back">
                    <div style="background-color: rgb(115, 185, 220); min-height: 264px;" class="box2">
                        <h4><?php echo $pageData["box2_title"]; ?></h4>
                        <hr>
                        <p><?php echo $pageData["box2_desc"]; ?></p>
                        <a class="btn btn-translucid btn-lg btn-block" data-target=".packing_info" data-toggle="modal"><?php echo $pageData["box2_designation"]; ?></a>
                    </div>
                </div>
            </div>            
        </div>


        <!-- RIGHT -->
        <div class="col-lg-9 col-md-9 col-sm-8">
            <div class="alert alert-success" id="success-alert1">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <span></span>
            </div>

            <div class="alert alert-danger" id="danger-alert1">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> <span></span> 
            </div>  
             
            <?php
          /*  $tab_active_a = "";
            if ($_REQUEST['subcate_id'] == "") {
                if ($_REQUEST['v_page'] == "yes") {
                    $tab_active_a = "";
                } else {
                    $tab_active_a = "active";
                }
            }*/
            ?>  
            
            <div class="tab-content">

                <?php
                $tab_active = "";
                if ($_REQUEST['subcate_id'] != "") {
                    $tab_active = "active";
                } elseif ($_REQUEST['v_page'] == "yes") {
                    $tab_active = "active";
                }
                ?>               
                
                <!-- Second tab Code -->
                <div id="v2" class="tab-pane fade in <?php echo $tab_active; ?>">

                    <div class="table-responsive">
                        <div class="">
                            <div id="portfolio" class="portfolio-gutter">
                                <ul class="nav nav-pills mix-filter margin-bottom-60">
                                    <li data-filter="all" class="filter active"><a href="#">All</a></li>

                                    <?php

                                    if (isset($_GET['id'])) {
                                        $cat_sql = "SELECT DISTINCT grower_product.subcaegoryid, subcategory.name as sub_category_name FROM grower_product LEFT JOIN subcategory ON subcategory.id = grower_product.subcaegoryid WHERE grower_id = $growerId";
                                        $cat_res = mysqli_query($con, $cat_sql);
                                        while ($sub_category = mysqli_fetch_array($cat_res)) {
                                            $sub_category_name = str_replace(" ", "_", $sub_category['sub_category_name']);
                                            $sub_category_name = str_replace("'", "_", $sub_category_name);
                                        ?>        
                                            <li data-filter="<?php echo $sub_category_name; ?>" class="filter"><a href="#"><?php echo $sub_category['sub_category_name']; ?></a></li>
                                    <?php 
                                        }
                                    }
                                    ?>

                                </ul>
                                <div class="row mix-grid">
                                    <?php
                                    if (isset($_GET['id'])) {
                                        $product_sql = "SELECT grower_product.product_id, product.categoryid, product.subcategoryid, product.name as product_name, product.image_path, category.name as category_name, subcategory.name as sub_category_name FROM grower_product 
                                            LEFT JOIN product ON product.id = grower_product.product_id 
                                            LEFT JOIN category ON category.id = product.categoryid 
                                            LEFT JOIN subcategory ON subcategory.id = product.subcategoryid 
                                            WHERE grower_id = $growerId";
                                        $product_res = mysqli_query($con, $product_sql);
                                        while ($product_data = mysqli_fetch_array($product_res)) {
                                            $product_sub_category_name = str_replace(" ", "_", $product_data['sub_category_name']);
                                            $product_sub_category_name = str_replace("'", "_", $product_sub_category_name);
                                            ?>
                                            <div style="display: block;  opacity: 1;" class="col-md-5th col-sm-4 mix design mix_all <?php echo $product_sub_category_name; ?>"><!-- item -->
                                                <div class="item-box">
                                                    <figure>
                                                        <span class="item-hover">
                                                            <span class="overlay dark-5"></span>
                                                            <span class="inner">

                                                                <!-- lightbox -->
                                                                <a class="ico-rounded" href="<?php echo SITE_URL."../en/single-growers-detail.php?id=".$product_data['product_id']."&id_grow=".$growerId ?>" >
								     <span class="fa fa-eye fs-20"></span>
                                                                </a>

                                                                <!-- details -->
                                                                <a class="ico-rounded" href="<?php echo SITE_URL."../en/variety-sub-page.php?id=".$product_data['product_id']?>">
                                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                                </a>

                                                            </span>
                                                        </span>

                                                        <!-- carousel -->
                                                        <div style="opacity: 1; display: block;" class="owl-carousel buttons-autohide controlls-over nomargin owl-theme owl-carousel-init" data-plugin-options="{&quot;singleItem&quot;: true, &quot;autoPlay&quot;: false, &quot;navigation&quot;: false, &quot;pagination&quot;: true, &quot;transitionStyle&quot;:&quot;none&quot;}">
                                                            <div class="owl-wrapper-outer">
                                                                <div style="width: 1284px; left: 0px; display: block; transition: all 0ms ease 0s; transform: translate3d(-214px, 0px, 0px); perspective-origin: 321px 50%;" class="owl-wrapper">
                                                                    <div style="width: 214px;" class="owl-item">
                                                                        <div>
                                                                            <img class="img-responsive" src="<?php echo SITE_URL . $product_data['image_path']; ?>" alt="" height="399" width="600">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /carousel -->

                                                    </figure>

                                                    <div style="" class="item-box-desc">
                                                        <h3><?php echo $product_data['product_name'] ?></h3>
                                                        <ul class="list-inline categories nomargin">
                                                            <li><a href="#"><?php echo $product_data['category_name'] ?></a></li>
                                                            <li><a href="#"><?php echo $product_data['sub_category_name'] ?></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- /item -->
                                        <?php
                                        }
                                    }
                                    ?>

                                </div>

                            </div>

                        </div>

                        <!-- atif added end -->
                        <?php
                        $page = (int) (!isset($_GET["v_id"]) ? 1 : $_GET["v_id"]);
                        $growers_id = $_REQUEST['id'];
                        $url = "id=" . $_REQUEST['id'];
                        echo pagination("growers", $limit, $page, $con, $growers_id, $_REQUEST['subcate_id'], $url);
                        ?>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade in" id="r1">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>
                        <div class="tab-pane fade in" id="r2">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>
                        <div class="tab-pane fade in" id="r3">
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>
                        <div class="tab-pane fade" id="tb">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
                        </div>
                        <div class="tab-pane fade" id="fnf">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
                        </div>

                    </div>
                    </div>
                <!-- End Second Tab Code -->
                
                
                <!-- Third Tab Code -->
                <div id="v3" class="tab-pane fade in">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Market Price (FOB Quito)</th>
                                    <th>Request for Product</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $limit = 10;
                                $start = 0;
                                if (isset($_GET['p_id'])) {
                                    $id = $_GET['p_id'];
                                    $start = ($id - 1) * $limit;
                                }
                                $sel_catinfo_p = "select gp.prodcutname,gp.prodcutid,gp.price,gp.sizename,feat.name as features_name,sub_cat.name as sub_cat_name
                            from grower_product_price as gp 
                            LEFT JOIN features as feat ON feat.id=gp.feature 
                            INNER JOIN product as p ON p.id=gp.prodcutid
                            INNER JOIN  subcategory sub_cat ON sub_cat.id=p.subcategoryid
                            where gp.growerid='" . $_REQUEST['id'] . "' GROUP BY gp.prodcutid,gp.feature ORDER BY gp.prodcutid LIMIT $start, $limit";
                            
                                $rs_catinfo_p = mysqli_query($con, $sel_catinfo_p);
                                while ($catinfo_p = mysqli_fetch_array($rs_catinfo_p)) {

                                    ?>  
                                    <tr>
                                        <td style="text-align: left;">
                                            <?php
                                            //echo $catinfo_p['prodcutid'];
                                            $features_name = "";
                                            if ($catinfo_p['features_name'] != "") {
                                                $features_name = $catinfo_p['features_name'];
                                            }

                                            echo $catinfo_p['sub_cat_name'] . " " . $catinfo_p['prodcutname'] . " " . $catinfo_p['sizename'] . " CM" . " " . $features_name;
                                            ?>
                                        </td>
                                        <td>$<?php echo $catinfo_p['price']; ?></td>
                                        <td><span class="label label-success">Request</span></td>
                                    </tr>

                            <?php } ?>
                            </tbody>
                        </table>
                        <?php
                        $page = (int) (!isset($_GET["p_id"]) ? 1 : $_GET["p_id"]);
                        $growers_id = $_REQUEST['id'];
                        $url = "id=" . $_REQUEST['id'];
                        echo pagination_p("growers", $limit, $page, $con, $growers_id, $_REQUEST['subcate_id'], $url);
                        ?>
                    </div>
                </div>
                <!-- End Third Tab Code -->
                <!-- Fourth Tab Code -->
                <div id="v4" class="tab-pane fade in">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Product Description</th>
                                    <th>Bunch or Stem</th>
                                    <th>Pack</th>
                                    <th>Unit Price</th>
                                    <th>Boxes for Sale</th>
                                    <th>Buy Now</th>
                                    <th>Counter Bid</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sel_ava_p = "select gpb.prodcutid,gpb.id as gid,GROUP_CONCAT(gpb.price) as price,GROUP_CONCAT(gpb.qty) as qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,
                                                     gpb.growerid,p.id,p.name as productname,p.box_type as p_box_type,s.name as sub_cat_name,
                                                     g.growers_name,sh.name as sizename,ff.name as features_name,b.name as boxname,GROUP_CONCAT(gpb.stock) as stock,
                                                     GROUP_CONCAT(bs.name) as bunch_stemp_value,GROUP_CONCAT(bt.name) as boxtype 
                                                from grower_product_box_packing gpb
                                                left join product p on gpb.prodcutid = p.id
                                                left join subcategory s on p.subcategoryid=s.id  
                                                left join colors c on p.color_id=c.id 
                                                left join features ff on gpb.feature=ff.id
                                                left join sizes sh on gpb.sizeid=sh.id 
                                                left join boxes b on gpb.box_id=b.id
                                                left join boxtype bt on b.type=bt.id
                                                left join growers g on gpb.growerid=g.id
                                                left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                                               where g.active='active' and gpb.growerid='" . $_REQUEST['id'] . "' and gpb.type!=2  and p.name is not null  and gpb.stock > 0
                                                 and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y') 
                                               GROUP BY sh.name";

                                $rs_ava_p = mysqli_query($con, $sel_ava_p);

                                while ($ava_p = mysqli_fetch_array($rs_ava_p)) {
                                    $features_name = "";
                                    if ($ava_p['features_name'] != "") {
                                        $features_name = $ava_p['features_name'];
                                    }
                                    $sel_bunch_size = "Select gpbs.bunch_sizes,gpbs.is_bunch,gpbs.is_bunch_value from grower_product_bunch_sizes as gpbs 
                                 where gpbs.grower_id='" . $_REQUEST['id'] . "' AND gpbs.product_id='" . $ava_p['prodcutid'] . "' AND gpbs.sizes='" . $ava_p['sizeid'] . "'";

                                    $rs_bunch_p = mysqli_query($con, $sel_bunch_size);
                                    $row_bunch = mysqli_fetch_array($rs_bunch_p);
                                    $exp_boxtype = explode(",", $ava_p['boxtype']);
                                    
                                    for ($i = 0; $i < count($exp_boxtype); $i++) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                $box_type_s = "";
                                                if($ava_p['p_box_type'] == "0")
                                                {
                                                    $box_type_s = "Stems";
                                                }   
                                                else if($ava_p['p_box_type'] == "1")
                                                {
                                                    $box_type_s = "Bunch";
                                                }
                                                if ($row_bunch['is_bunch'] == 0) {
                                                    $exp_bunch = explode(",", $ava_p['bunch_stemp_value']);
                                                    echo $ava_p['sub_cat_name'] . " " . $ava_p['productname'] . " " . $ava_p['sizename'] . "cm" . " " . $features_name . " " . $exp_bunch[0] . $box_type_s;
                                                } else {
                                                    echo $ava_p['sub_cat_name'] . " " . $ava_p['productname'] . " " . $ava_p['sizename'] . "cm" . " " . $features_name . " " . $row_bunch['is_bunch_value'] . $box_type_s;
                                                }

                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($row_bunch['is_bunch'] == 0) {
                                                    echo "Stem";
                                                } else {
                                                    echo "Bunch";
                                                }
                                                ?>     
                                            </td>        
                                            <td>
                                                <?php
                                                $exp_bunch_stemp_value = explode(",", $ava_p['bunch_stemp_value']);
                                                $exp_qty = explode(",", $ava_p["qty"]);
                                                if ($row_bunch['is_bunch'] == 0) {
                                                    echo $exp_bunch_stemp_value[$i] * $exp_qty[$i];
                                                } else {
                                                    echo $exp_qty[$i];
                                                }
                                                ?></td>

                                            <?php
                                            $exp_price = explode(",", $ava_p['price']);
                                            if ($exp_price[$i] == "") {
                                                $f_price = "0.00";
                                            } else {
                                                $f_price = $exp_price[$i];
                                            }
                                            ?>
                                            <td>$<?php echo $f_price; ?></td>

                                            <td><?php
                                                $exp_stock = explode(",", $ava_p['stock']);
                                                if ($exp_boxtype[$i] == "QB") {
                                                    echo $exp_stock[$i] . " Quarter box";
                                                } elseif ($exp_boxtype[$i] == "HB") {
                                                    echo $exp_stock[$i] . " Half Box";
                                                } elseif ($exp_boxtype[$i] == "EB") {
                                                    echo $exp_stock[$i] . " Eight Box";
                                                } elseif ($exp_boxtype[$i] == "JB") {
                                                    echo $exp_stock[$i] . " Jumbo Box";
                                                }
                                                ?></td>
                                            <td><span class="label label-success">Buy Now</span></td>
                                            <td><button class="btn btn-success btn-xs"><i class="fa fa-gavel"></i> 100</button></td>
                                        </tr><?php
                                    }
                                    ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Fourth Tab Code -->
                <div id="v6" class="tab-pane fade in">
                </div>
                <!-- End Fifth Tab Code -->
                <div id="v7" class="tab-pane fade in <?php echo $tab_active_a; ?>">
                    <div class="row box-static box-transparent user-rating-box box-bordered padding-20" >
                        <div class=" margin-bottom-30">
                            <div class="col-md-6 border-right">
                                <h2 class="size-20">Quality Assurance's <span id="textlogin">Rating</span></h2>
                                <div class="col-md-4 col-sm-4"><span class="purple bold size-40"><?php echo $admin_rating_percentage; ?>%</span></div>
                                <div class="col-md-8 col-sm-8">
                                    <div class="progress">
                                        <div class="progress-bar puple-background" role="progressbar" aria-valuenow="<?php echo $admin_rating_percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $admin_rating_percentage; ?>%">
                                            <span class="sr-only"><?php echo $admin_rating_percentage; ?>% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h2 class="size-20">Buyer's <span id="textlogin">Rating</span></h2>
                                <div class="col-md-4 col-sm-4"><span class="purple bold size-40"><?php echo $buyer_rating_percentage; ?>%</span></div>
                                <div class="col-md-8 col-sm-8"> 
                                    <div class="progress">
                                        <div class="progress-bar puple-background" role="progressbar" aria-valuenow="<?php echo $buyer_rating_percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $buyer_rating_percentage; ?>%">
                                            <span class="sr-only"><?php echo $buyer_rating_percentage; ?>% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($_SESSION['login-type'] != 'buyer' && $_SESSION['login-type'] != 'grower') { ?>
                        <div class="row box-static box-transparent user-rating-box box-bordered padding-20">
                            <form action="" method="post" id="user-rating-form">
                                <div class=" margin-bottom-30">
                                    <div class="col-md-6 border-right rating-star-row">

                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-5 col-sm-6 control-label" for="profileFirstName">Quality Rating</label>
                                                <div class="col-md-7 col-sm-6">
                                                    <input id="quality_rating" name="quality_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa"> 

                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-5 col-sm-6 control-label" for="profileFirstName">Freshness&nbsp;Rating</label>
                                                <div class="col-md-7 col-sm-6">
                                                    <input id="freshness_rating" name="freshness_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">         

                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-5 col-sm-6 control-label" for="profileFirstName">Trustworthiness Rating</label>
                                                <div class="col-md-7 col-sm-6">
                                                    <input id="trustworthiness_rating" name="trustworthiness_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">         

                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-5 col-sm-6 control-label" for="profileFirstName">Pricing Rating</label>
                                                <div class="col-md-7 col-sm-6">
                                                    <input id="pricing_rating" name="pricing_rating" type="hidden" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">         

                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-md-5 col-sm-6 control-label" for="profileFirstName">Packing Rating</label>
                                                <div class="col-md-7 col-sm-6">
                                                    <input id="packing_rating" name="packing_rating" type="hidden" value="0" class="rating" min="0" max="5" step="0.5" data-size="sm"
                                                           data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">     

                                                </div>
                                            </div>
                                        </fieldset>
                                                                                
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <!--<input type="button" class="btn btn-success btn-sm margin-top-20" value="Rate Grower" onclick="rate_grower()"/>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="5" name="comment" id="comment" placeholder="Add your comment here..."></textarea>
                                        <input type="hidden" name="post-comment" value="1" />
                                        <input type="button" class="btn btn-success btn-sm pull-right margin-top-10" value="Rate & Comment" onclick="post_comment();" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php } 
                          else if($_SESSION['login-type'] == 'buyer') { ?>
                          <div class="row">
                            <a href="<?php echo SITE_URL ?>buyers-account.php" class="btn btn-success pull-right margin-top-10">Click here to Rate & Comment</a>
                          </div>
                    <?php
                        }
                    ?>
                    <div class="row box-static box-transparent user-rating-box box-bordered padding-20" id="all_comments">
                        <h2 class="size-20">Users <span id="textlogin">Reviews</span></h2>
                        <!-- COMMENTS -->
                        <div id="comments" class="comments">
                            <div class="comment-list">
                                <div class="row" id="posts_content">
                                <?php
                                if ($_GET['id']) {
                                    $_SESSION['grower_id'] = $growerId;
                                    include('../pagination/Pagination.php');
                                    //Include database configuration file

                                    $limit = 6;

                                    //get number of rows
                                    $queryNum = mysqli_query($con, "SELECT DISTINCT(user_ratings.ur_user_idFk), COUNT(user_ratings.ur_id) as postNum FROM user_ratings WHERE ur_status =1 AND ur_grower_idFk = " . $_SESSION['grower_id']);

                                    $resultNum = mysqli_fetch_assoc($queryNum);

                                    $rowCount = $resultNum['postNum'];

                                    //initialize pagination class
                                    
                                    $pagConfig = array('baseURL' => '/pagination/getUserComments.php', 'totalRows' => $rowCount, 'perPage' => $limit, 'contentDiv' => 'posts_content');
                                  
                                    $pagination = new Pagination($pagConfig);

                                    $query = mysqli_query($con, "SELECT DISTINCT(ur_user_idFk), ur_id, ur_user_type, ur_comment, ur_final_rating, ur_create_datetime FROM user_ratings WHERE ur_status =1 AND ur_grower_idFk = $growerId ORDER BY ur_id DESC LIMIT $limit");

                                    if (mysqli_num_rows($query) > 0) {
                                        ?>
                                        <div class="posts_list col-md-12">
                                            <?php
                                            while ($row_user_rating = mysqli_fetch_assoc($query)) {
                                                //var_dump($row_user_rating);
                                                if ($row_user_rating['ur_user_type'] == 'admin') {

                                                    $sql_data = "SELECT first_name, last_name FROM review_rating WHERE user_rating_idFk = " . $row_user_rating['ur_id'];
                                                    $data_res = mysqli_query($con, $sql_data);
                                                    $row = mysqli_fetch_assoc($data_res);

                                                    $row_user_rating['u_first_name'] = $row['first_name'];
                                                    $row_user_rating['u_last_name'] = $row['last_name'];
                                                    $row_user_rating['u_picture'] = '';
                                                    $row_user_rating['u_gender'] = 'male';
                                                    
                                                    $sql_data = "SELECT uname, picture FROM admin WHERE id= 1 AND isadmin = 1";
                                                    $data_res = mysqli_query($con, $sql_data);
                                                    $row = mysqli_fetch_assoc($data_res);
                                                    
                                                    $pic_path = $pic_path = SITE_URL . '../includes/assets/profile_pictures/'.$row['picture'];
                                                } else if ($row_user_rating['ur_user_type'] == 'buyer') {
                                                    //echo 'it is buyer';
                                                    $sql_data = "SELECT first_name, last_name, profile_image FROM buyers WHERE id= " . $row_user_rating['ur_user_idFk'];
                                                    $data_res = mysqli_query($con, $sql_data);
                                                    $row = mysqli_fetch_assoc($data_res);

                                                    $row_user_rating['u_first_name'] = $row['first_name'];
                                                    $row_user_rating['u_last_name'] = $row['last_name'];
                                                    $row_user_rating['u_picture'] = $row['profile_image'];
                                                    $row_user_rating['u_gender'] = 'male';
                                                    if ($row['profile_image'] != '') {
                                                        $pic_path = SITE_URL . 'profile_images/' . $row_user_rating['u_picture'];
                                                    } else {
                                                        $pic_path = SITE_URL . '../includes/assets/profile_pictures/default_male.jpg';
                                                    }
                                                } else if ($row_user_rating['ur_user_type'] == 'user') {
                                                    $sql_data = "SELECT u_first_name, u_last_name, u_picture FROM users WHERE u_id= " . $row_user_rating['ur_user_idFk'];
                                                    $data_res = mysqli_query($con, $sql_data);
                                                    $row = mysqli_fetch_array($data_res);

                                                    $row_user_rating['u_first_name'] = $row['u_first_name'];
                                                    $row_user_rating['u_last_name'] = $row['u_last_name'];
                                                    $row_user_rating['u_picture'] = $row['u_picture'];
                                                    $row_user_rating['u_gender'] = $row['u_gender'];

                                                    if (strpos($row_user_rating['u_picture'], 'https://') !== false) {
                                                        $pic_path = $row_user_rating['u_picture'];
                                                    } else {
                                                        $pic_path = SITE_URL . '../includes/assets/profile_pictures/default_male.jpg';
                                                        if ($row_user_rating['u_gender'] == 'female') {
                                                            $pic_path = SITE_URL . '../includes/assets/profile_pictures/default_female.jpg';
                                                        }
                                                    }
                                                }
                                                ?>
                                    
                                    
                                    
                                    <!-- First Comment -->
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 hidden-xs">
                                            <figure>
                                            <img class="img-responsive rounded review_pic" src="<?php echo $pic_path; ?>" alt="User Pic">
                                             </figure>
                                        </div>
                                        <div class="col-md-10 col-sm-10">
                                            <div class="panel panel-default arrow left">
                                                <div class="panel-body">
                                                    <header class="text-left">
                                                        <div class="comment-user"><i class="fa fa-user"></i> <?php echo $row_user_rating['u_first_name'] . ' ' . $row_user_rating['u_last_name']; ?></div>
                                                        <div>
                                                        <?php
                                                        for ($i = 1; $i <= $row_user_rating['ur_final_rating']; $i++) {
                                                        ?>
                                                            <img alt="1" src="<?php echo SITE_URL; ?>images/star-on.png">
                                                        <?php
                                                        }
                                                        ?>
                                                        </div>
                                                        <span class="comment-date"><i class="fa fa-clock-o"></i> <?php echo date('F d, Y', strtotime($row_user_rating['ur_create_datetime'])); ?></span>
                                                        
                                                    </header>
                                                    <div class="comment-post">
                                                        <p>
                                                            <?php echo $row_user_rating['ur_comment']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?php echo $pagination->createLinks(); ?>
                                    </div>
                                <?php
                                }
                                if ($rowCount == 0) {
                                    echo '<div class="col-md-12">No comments found</div>';
                                }
                            }
                            ?>
                                </div>
                            </div>
                        </div>   
                        <div class="row"></div>
                    </div>
                </div>
                <!-- End Fifth Tab Code -->
            </div>
        </div>
    </div>
</section>
<!-- / -->








<!-- Modal -->

<div id="loginModal" class="modal fade modal-md" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Login</h4>
            </div>
            <div class="modal-body">
                <div class="row">  
                    <div class="col-md-6 col-sm-6">
                        <a class="btn btn-primary-fb btn-lg fullWidth" id="fbLoginButton" onclick="loginFB()">
                            <i class="fa fa-facebook"></i>
                            Log in with Facebook
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <a class="btn btn-primary-fb btn-lg fullWidth" id="googleLoginButton" onclick="loginGoogle()">
                            <i class="fa fa-google-plus"></i>
                            Log in with Gmail
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 col-md-offset-3 col-xs-offset-3 size-21 margin-top-10 margin-bottom-10">
                        -----------------   OR  -----------------
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form id="user-login-form" class="form" action="login-process.php" method="post">
                            <div class="form-group">
                                <label for="ul_email">Email address <span class="mandatory">*</span></label>
                                <input type="email" name="ul_email" class="form-control" id="ul_email" placeholder="Email">
                                <span id="ul_email_error"></span>
                            </div>
                            <div class="form-group">
                                <label for="ul_password">Password <span class="mandatory">*</span></label>
                                <input type="password" name="ul_password" class="form-control" id="ul_password" placeholder="Password">
                                <span id="ul_password_error"></span>
                            </div>
                            <div class="form-group">
                                <input type="button" name="btn-login" class="btn btn-success" value="Submit Comment" onclick="return login_process();"/>
                                <span class="margin-left-20">Don't have an account? <a href="javascript:show_user_register_form();">Sign up here</a></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="registerModal" class="modal fade modal-md" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Registration</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="user-registration-form" class="form" action="login-process.php" method="post">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ur_fname">First Name <span class="mandatory">*</span></label>
                                    <input type="text" name="fname" class="form-control" tabindex="1" id="ur_fname" placeholder="First Name">
                                    <span id="ur_fname_error"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ur_lname">Last Name <span class="mandatory">*</span></label>
                                    <input type="text" name="lname" class="form-control" tabindex="2" id="ur_lname" placeholder="Last Name">
                                    <span id="ur_lname_error"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ur_email">Email address <span class="mandatory">*</span></label>
                                    <input type="email" name="email" class="form-control" tabindex="3" id="ur_email" placeholder="Email">
                                    <span id="ur_email_error"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ur_password">Password <span class="mandatory">*</span></label>
                                    <input type="password" name="password" class="form-control" tabindex="4" id="ur_password" placeholder="Password">
                                    <span id="ur_password_error"></span>
                                </div>

                            </div>
                            <div class="col-md-6">    
                                <div class="form-group">
                                    <label for="gender">Gender <span class="mandatory">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <input type="radio" name="gender" class="form-control" tabindex="5" value="male" style="height:10px; margin-bottom: 10px;"> 
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        Male
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <input type="radio" name="gender" class="form-control" value="female"  style="height:10px; margin-bottom: 10px;"> 
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        Female
                                    </div>
                                    <div class="col-md-12">
                                        <span id="ur_gender_error"></span>
                                    </div>
                                </div>
                            </div>        
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="button" name="btn-register" class="btn btn-success" value="Register" onclick="return register_user();"/>
                            <span class="margin-left-20">Already have an account? <a href="javascript:show_user_login_form();">Login here</a></span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<script>
    var is_login = "<?php echo $_SESSION['login'] ?>";
    var is_user = "<?php echo $_SESSION['login-type'] ?>";
</script>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '652316331589506',
            xfbml: true,
            version: 'v2.7'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>

    function loginFB() {
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                console.log('Logged in.');
                fb_user_details();
            } else {
                FB.login(function (response) {
                    if (response.status === 'connected') {
                        console.log('Logged in.');
                        fb_user_details();
                    }
                });
            }
        });

    }
</script>
<script>
    function fb_user_details() { // For FB User
        FB.api('/me', 'GET', {fields: 'first_name, last_name, email, gender, picture'}, function (response) {
            var first_name = response.first_name;
            var last_name = response.last_name;
            var email = response.email;
            var gender = response.gender;
            var picture = response.picture.data.url;
            var reg_type = 'fb';
            var fb_id = response.id;
            //console.log(response.id);
            register_social_user(first_name, last_name, email, picture, gender, reg_type, fb_id);
        });
    }

    function register_user() { // For website Registration

        var reg_response = validate_regiser_user();

        if (reg_response == true) {
            var formData = $('#user-registration-form').serialize();
            var url = "<?php echo SITE_URL; ?>register_user.php";
            $.ajax({
                type: 'post',
                data: formData,
                url: url,
                dataType: 'text',
                success: function (data) {
                    if (data == 1) {
                        $('#user-rating-form').submit();
                    } else if (data == -1) {
                        $('#registerModal').modal('hide');
                        $('#danger-alert1 > span').html(' This email id already exists, Try another email.');
                        $('#danger-alert1').show();
                        window.scrollTo(0, 0);

                    }
                }
            });
            $('#loginModal').modal('hide');
        } else {
            //alert('Validation errorrs');
        }

    }

    function validate_regiser_user() {
        var firstName = $('#ur_fname').val();
        var lastName = $('#ur_lname').val();
        var email = $('#ur_email').val();
        var password = $('#ur_password').val();
        var gender = $('input[name=gender]:checked').val();

        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var isValid = true;

        if (firstName == '') {
            $('#ur_fname_error').html('Please provide your first name');
            $('#ur_fname_error').addClass('error');
            isValid = false;
        }

        if (lastName == '') {
            $('#ur_lname_error').html('Please provide your last name');
            $('#ur_lname_error').addClass('error');
            isValid = false;
        }

        if (email == '') {
            $('#ur_email_error').html('Please provide your email address');
            $('#ur_email_error').addClass('error');
            isValid = false;
        } else if (!regex.test(email)) {
            $('#ur_email_error').html('Please provide your valid email address');
            $('#ur_email_error').addClass('error');
            isValid = false;
        }

        if (gender == '' || gender == undefined) {
            $('#ur_gender_error').html('Please provide your gender');
            $('#ur_gender_error').addClass('error');
            isValid = false;
        }

        if (password == '') {
            $('#ur_password_error').html('Please provide your password');
            $('#ur_password_error').addClass('error');
            isValid = false;
        }
        return isValid;

    }

</script>
<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery("#v1").removeClass("active");
    });

    is_login = "<?php echo $_SESSION['login'] ?>";
    is_user = "<?php echo $_SESSION['login-type'] ?>";
</script>
<script src="<?php echo SITE_URL ?>/includes/assets/js/jquery.raty.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL ?>/includes/assets/js/star-ratings.js" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        $( "div.image-hover img" ).stop( true, true );
        $.fn.raty.defaults.path = 'images';
        $(function () {
            $('#quality_rating1').raty({scoreName: 'quality_rating'});
            $('#freshness_rating1').raty({scoreName: 'freshness_rating'});
            $('#trustworthiness_rating1').raty({scoreName: 'trustworthiness_rating'});
            $('#pricing_rating1').raty({scoreName: 'pricing_rating'});
            $('#packing_rating1').raty({scoreName: 'packing_rating'});
        });

        $('#quality_rating').on('rating.change', function (event, value, caption) {
            $("#quality_rating").val(value);
        });
        $('#freshness_rating').on('rating.change', function (event, value, caption) {
            $("#freshness_rating").val(value);
        });
        $('#trustworthiness_rating').on('rating.change', function (event, value, caption) {
            $("#trustworthiness_rating").val(value);
        });
        $('#pricing_rating').on('rating.change', function (event, value, caption) {
            $("#pricing_rating").val(value);
        });

        $('#packing_rating').on('rating.change', function (event, value, caption) {
            $("#packing_rating").val(value);
        });

    });
    function post_comment() {
        var quality = $("input[name=quality_rating]").val();
        var freshness = $("input[name=freshness_rating]").val();
        var trustworthiness = $("input[name=trustworthiness_rating]").val();
        var pricing = $("input[name=pricing_rating]").val();
        var packing = $("input[name=packing_rating]").val();
        var comment = $("textarea[name=comment]").val();
        if (quality == '' || freshness == '' || trustworthiness == '' || pricing == '' || packing == '' || comment == '' || comment.length < 5) {
            $('#danger-alert1 > span').html(' Please select atleast 1 star from each and minimum 5 characters comment.');
            $('#danger-alert1').show();
            window.scrollTo(0, 0);
            //alert('Please select atleast 1 star from each and minimum 5 characters comment.');
        } else {
            if (is_login == '' || is_user != 'user') {
                $('#loginModal').modal('show');
            } else {
                $('#user-rating-form').submit();
            }
            return false;
        }
    }

    function login_process() {

        var response = validate_login_form();
        if (response == true) {
            var formData = $('#user-login-form').serialize();
            var url = "<?php echo SITE_URL; ?>login-process.php";
            $.ajax({
                type: 'post',
                data: formData,
                url: url,
                dataType: 'text',
                success: function (data) {
                    if (data == 1) {
                        $('#user-rating-form').submit();
                    } else {
                        $('#danger-alert1 > span').html(' Invalid email address or password.');
                        $('#danger-alert1').show();
                        window.scrollTo(0, 0);
                    }
                }
            });
            $('#loginModal').modal('hide');
        } else {
            //alert('Validation Errors');
        }



    }

    function show_user_register_form() {
        $('#user-login-form').trigger('reset');
        $('#user-login-form .error').html('');
        $('#loginModal').modal('hide');
        $('#registerModal').modal('show');

    }

    function show_user_login_form() {
        $('#user-registration-form').trigger('reset');
        $('#user-registration-form .error').html('');
        $('#registerModal').modal('hide');
        $('#loginModal').modal('show');

    }

    function validate_login_form() {
        var email = $('#ul_email').val();
        var password = $('#ul_password').val();
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var isValid = true;

        if (email == '') {
            $('#ul_email_error').html('Please provide your email address');
            $('#ul_email_error').addClass('error');
            isValid = false;
        } else if (!regex.test(email)) {
            $('#ul_email_error').html('Please provide your valid email address');
            $('#ul_email_error').addClass('error');
            isValid = false;
        }

        if (password == '') {
            $('#ul_password_error').html('Please provide your password');
            $('#ul_password_error').addClass('error');
            isValid = false;
        }
        return isValid;
    }
    function register_social_user(first_name, last_name, email, picture, gender, reg_type, fb_id) {
        var url = "<?php echo SITE_URL; ?>register_user.php";
        $.ajax({
            type: 'post',
            data: {fname: first_name, lname: last_name, email: email, picture: picture, gender: gender, reg_type: reg_type, fb_id: fb_id},
            url: url,
            dataType: 'text',
            success: function (data) {
                if (data == 1) {
                    $('#user-rating-form').submit();
                } else {
                    return false;
                }
            }
        });
    }
    // Your Client ID can be retrieved from your project in the Google
    // Developer Console, https://console.developers.google.com
    var CLIENT_ID = '821928589813-1d2o8tlvoc9og2p7o28579lekkpdeg2v.apps.googleusercontent.com';

    var SCOPES = ['https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/plus.login'];

    /**
     * Check if current user has authorized this application.
     */
    function checkAuth() {
        gapi.auth.authorize(
                {
                    'client_id': CLIENT_ID,
                    'scope': SCOPES.join(' '),
                    'immediate': true
                }, handleAuthResult);
    }

    /**
     * Handle response from authorization server.
     *
     * @param {Object} authResult Authorization result.
     */
    function handleAuthResult(authResult) {

        if (authResult && !authResult.error) {
            // Hide auth UI, then load client library.

            //loadGmailApi();
        } else {
            // Show auth UI, allowing the user to initiate authorization by
            // clicking authorize button.

        }
    }

    /**
     * Initiate auth flow in response to user clicking authorize button.
     *
     * @param {Event} event Button click event.
     */
    function loginGoogle(event) {
        gapi.auth.authorize(
                {client_id: CLIENT_ID, scope: SCOPES, immediate: false},
                loadGmailApi);
        return false;
    }

    /**
     * Load Gmail API client library. List labels once client library
     * is loaded.
     */
    function loadGmailApi() {
        gapi.client.load('plus', 'v1', getUserDetails);
    }

    /**
     * Print all Labels in the authorized user's inbox. If no labels
     * are found an appropriate message is printed.
     */
    function getUserDetails() {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });

        request.execute(function (resp) {
            var first_name = resp.name.givenName;
            var last_name = resp.name.familyName;
            var email = resp.emails[0].value;
            var gender = resp.gender;
            var picture = resp.image.url;
            var reg_type = 'gp';
            if (gender == 'undefined') {
                gender = 'unknown';
            }
            var fb_id = '';
            register_social_user(first_name, last_name, email, picture, gender, reg_type, fb_id);
        });
    }
</script>
<script src="https://apis.google.com/js/client.js?onload=checkAuth"></script>
<script>
    $(document).ready(function () {
        $('#ul_email').focus(function () {
            $('#ul_email_error').html('');
            $('#ul_email_error').removeClass('error');
        });
        $('#ul_password').focus(function () {
            $('#ul_password_error').html('');
            $('#ul_password_error').removeClass('error');
        });
        $('#ur_fname').focus(function () {
            $('#ur_fname_error').html('');
            $('#ur_fname_error').removeClass('error');
        });
        $('#ur_lname').focus(function () {
            $('#ur_lname_error').html('');
            $('#ur_lname_error').removeClass('error');
        });
        $('#ur_email').focus(function () {
            $('#ur_email_error').html('');
            $('#ur_email_error').removeClass('error');
        });
        $('#ur_password').focus(function () {
            $('#ur_password_error').html('');
            $('#ur_password_error').removeClass('error');
        });
        $('input[name=gender]').change(function () {
            $('#ur_gender_error').html('');
            $('#ur_gender_error').removeClass('error');
        });

    });
    var url_package = "<?php echo SITE_URL; ?>/file/ajax_package_price.php";
    var id = "<?php echo $_REQUEST['id'];?>";
    $.ajax({
        type: 'post',
        url: url_package,
        dataType : 'html',
        data: 'id='+id+'&start=0&end=50',
        success: function (data) {
            //alert(data);
            $("#main_content_package_price").html(data);
        }
    });
    var page=1;
    
    var url_package = "<?php echo SITE_URL; ?>/file/ajax_package_price.php";
    var id = "<?php echo $_REQUEST['id'];?>";
    for(i=1;i<10;i++)
    {
        var start = i * 50;
        $.ajax({
            type: 'post',
            url: url_package,
            dataType : 'html',
            data: 'id='+id+'&start='+start+'&end=50',
            success: function (data) {
                //alert(data);
                $("#main_content_package_price").append(data);
                page++;
            }
        });
    }
    
    
    
</script>
<?php
require_once '../includes/footer.php'; 
?>
