<?php 
require_once("../config/config_new.php");

#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 10; //HOME PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . '../includes/assets/plugins/slider.revolution.v5/css/pack.css', SITE_URL . 'includes/assets/css/essentials-flfv5.css', 
                        SITE_URL . '../includes/assets/css/layout-flfv5.css', SITE_URL . 'includes/assets/css/thematics-restaurant.css',
                        SITE_URL . '../includes/assets/css/header-1.css', SITE_URL . 'includes/assets/css/color_scheme/blue.css'
                    );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

require_once("../includes/header.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

/************* code for recent post starts *****************/
$blogSql = "SELECT * FROM wpn_posts WHERE post_status='publish' AND post_type='post' ORDER BY post_modified DESC LIMIT 8";
$blogData = mysqli_query($con, $blogSql);
$blog_data = array();

while($blog = mysqli_fetch_assoc($blogData)) {
    $blogSql1 = "SELECT p.guid as pguid FROM wpn_posts p JOIN wpn_postmeta pm ON p.ID=pm.meta_value WHERE pm.meta_key='_thumbnail_id' AND p.post_parent='".$blog['ID']."'";
    $blogData1 = mysqli_query($con, $blogSql1);
    if($blog1 = mysqli_fetch_assoc($blogData1)) {
        $blog['img_path'] = $blog1['pguid'];
    }
    array_push($blog_data,$blog);
}
/************* code for recent post ends *****************/
?>

<style>
@media (min-width:1020px) and (max-width: 2500px)
{
.revtp-searchform{
        margin-top: 17.6%!important;
}
}
#result2                                                     
	{
		position:absolute;
		padding:10px;                                         
		display:none;
		margin-top:-10px;
		border-top:0px;
		overflow:hidden;
		border:1px #CCC solid;
		background-color: white;
		border:solid 1px #999;
		padding:10px;
		font-size:14px;
       
        z-index:9999;
		width: 82.48%;
        padding-bottom: 0px;
margin-top: -38px;
	}
	.show
	{
		padding: 8px;
		font-size: 15px;
		width: 100%;
		margin-left: 20%;
		border-bottom: 0px solid #ccc!important;
		margin-bottom: 11px;
		margin-top: 0px;
	}
.show a{
text-shadow:none!important;
}
.show span{
text-shadow:none!important;
}
	.show:hover
	{
		background:#EBEBED;
		color:#FFF;
		cursor:pointer;
	}
	
	.show a:hover
	{
		color:#FFF;
		cursor:pointer;
	}
	.left_category{
	float: left;
    margin-top: 20px;
    color: #C0C0C0;
    font-size: 14px;
    margin-left: -3px;
text-shadow:none!important;
	}
	#searchid2:focus {
  border: 1px solid #8a2b83;
}
</style>
<div id="rev_slider_98_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="search-form-hero72" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
    <div id="rev_slider_98_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
        <ul>
            <!-- SLIDE  -->
            <li data-index="rs-280" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="../../assets/images/landscape-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Intro" data-description="">
                <!-- MAIN IMAGE -->
                <img src="<?php echo SITE_URL . "user/" . $pageData["welcome_image"]; ?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0" 
                     id="slide-280-layer-10" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                     data-width="full"
                     data-height="full"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"

                     data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                     data-transform_out="s:300;s:300;" 
                     data-start="750" 
                     data-basealign="slide" 
                     data-responsive_offset="on" 
                     data-responsive="off"

                     style="z-index: 5;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0.50);"> 
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" 
                     id="slide-280-layer-8" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['middle','middle','middle','middle']" data-voffset="['-62','-68','-78','-78']" 
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-style_hover="cursor:default;"

                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                     data-start="1000" 
                     data-splitin="none" 
                     data-splitout="none" 
                     data-responsive_offset="on" 


                     style="z-index: 6; white-space: nowrap;"><i class="fa fa-search"></i> 
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
                     id="slide-280-layer-1" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','-22','-29']" 
                     data-fontsize="['50','70','40','30']"
                     data-lineheight="['50','70','40','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"

                     data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                     data-start="1000" 
                     data-splitin="none" 
                     data-splitout="none" 
                     data-responsive_offset="on" 


                     style="z-index: 7; white-space: nowrap; font-size: 50px; line-height: 50px;"><?php echo $pageData["image_title"]; ?> 
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
                     id="slide-280-layer-4" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['middle','middle','middle','middle']" data-voffset="['42','52','18','7']" 
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"

                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                     data-start="1000" 
                     data-splitin="none" 
                     data-splitout="none" 
                     data-responsive_offset="on" 


                     style="z-index: 8; white-space: nowrap;"><?php echo $pageData["image_sub_title"]; ?> 
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption NotGeneric-CallToAction   rs-parallaxlevel-0" 
                     id="slide-280-layer-11" 
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                     data-y="['top','top','top','top']" data-voffset="['376','336','252','191']" 
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-style_hover="cursor:default;"

                     data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;" 
                     data-transform_out="opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                     data-start="1000" 
                     data-splitin="none" 
                     data-splitout="none" 
                     data-responsive_offset="on" 
                     data-responsive="off"

                     style="z-index: 9; white-space: nowrap;padding:0px 0px 0px 0px;border-color:rgba(255, 255, 255, 0);border-style:none;">
                   <form role="search" method="get"  class="revtp-searchform" action="#">
										<input type="text" value=""  placeholder="What are you looking for?" id="searchid2" autocomplete="off"/>
										<input type="submit" id="searchsubmit" value="Find" />
									</form> 
 <div id="result2" class="sky-form"></div>
                </div>
            </li>
        </ul>
        <div class="tp-static-layers"></div>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
    </div>
</div><!-- END REVOLUTION SLIDER -->


<hr class="nomargin" /><!-- 1px line separator -->


<!-- BUTTON CALLOUT -->
<a href="#" class="btn btn-xlg btn-info size-20 fullwidth nomargin noradius padding-40">
    <span class="font-lato size-30">
        Do you have questions? 
        <strong>Contact us &raquo;</strong>
    </span>
</a>
<!-- /BUTTON CALLOUT -->



<!-- Overview -->
<section>
    <div class="container">

        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-12 ">
                <h2><?php echo $pageData["heading1"]; ?></h2>

                <p><?php echo $pageData["heading_desc1"]; ?></p>

                <a class="btn btn-danger btn-lg" href="<?php echo $pageData["heading_link1"]; ?>"><?php echo $pageData["heading_button1"]; ?></a>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-12">
                <h3 class="weight-300"><?php /* Welcome To Fresh Life Floral */echo $pageData["welcome_title"]; ?></h3>

                <p><?php echo $pageData["para_1"]; ?></p>

                <hr />

                <ul class="list-unstyled list-icons">
                    <?php
                    $para2 = explode(":", $pageData["para_2"]);
                    for ($i = 0; $i < count($para2); $i++) {
                        if ($para2[$i] != '') {
                            ?>
                            <li><i class="fa fa-check"></i> 
                                <?php echo $para2[$i]; ?>
                            </li>
                        <?php }
                    }
                    ?>	
                </ul>

            </div>
        </div>

    </div>
</section>
<!-- /Overview -->




<!-- Parallax -->
<section class="parallax parallax-1" style="background-image: url('http://staging.freshlifefloral.com/user/<?php echo $pageData["welcome_image2"]; ?>');">
    <div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>

    <div class="container">

        <div class="text-center">
            <h2 class="size-40 weight-300"><?php echo $pageData["heading2"]; ?></h2>
            <a class="btn btn-danger btn-lg" href="<?php echo $pageData["heading_link2"]; ?>"><?php echo $pageData["bottom_image_button_text"]; ?></a>
        </div>

    </div>
</section>
<!-- /Parallax -->




<!-- Team -->
<section>
    <div class="container">

        <header class="text-center margin-bottom-60 clearfix">
            <h2 class="weight-300 nomargin-bottom"><?php echo $pageData["box_main_heading"]; ?></h2>
        </header>

        <div class="row">

            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img class="img-responsive" src="<?php echo $pageData["box1_img"]; ?>" alt="" />
                    <div class="caption">
                        <h4 class="nomargin"><?php echo $pageData["box1_title"]; ?></h4>
                        <small class="margin-bottom-20 block"><?php echo $pageData["box1_designation"]; ?></small>

                        <p><?php echo $pageData["box1_desc"]; ?></p>
                        <a href="<?php echo SITE_URL; ?>buying-methods.php" class="btn btn-default btn-sm">READ MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img class="img-responsive" src="<?php echo $pageData["box2_img"]; ?>" alt="" />
                    <div class="caption">
                        <h4 class="nomargin"><?php echo $pageData["box2_title"]; ?></h4>
                        <small class="margin-bottom-20 block"><?php echo $pageData["box2_designation"]; ?></small>

                        <p><?php echo $pageData["box2_desc"]; ?></p>
                        <a href="<?php echo SITE_URL; ?>buying-methods.php" class="btn btn-default btn-sm">READ MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img class="img-responsive" src="<?php echo $pageData["box3_img"]; ?>" alt="" />
                    <div class="caption">
                        <h4 class="nomargin"><?php echo $pageData["box3_title"]; ?></h4>
                        <small class="margin-bottom-20 block"><?php echo $pageData["box3_designation"]; ?></small>

                        <p><?php echo $pageData["box3_desc"]; ?></p>
                        <a href="<?php echo SITE_URL; ?>buying-methods.php" class="btn btn-default btn-sm">READ MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img class="img-responsive" src="<?php echo $pageData["box4_img"]; ?>" alt="" />
                    <div class="caption">
                        <h4 class="nomargin"><?php echo $pageData["box4_title"]; ?></h4>
                        <small class="margin-bottom-20 block"><?php echo $pageData["box4_designation"]; ?></small>

                        <p><?php echo $pageData["box4_desc"]; ?></p>
                        <a href="<?php echo SITE_URL; ?>buying-methods.php" class="btn btn-default btn-sm">READ MORE</a>
                    </div>
                </div>
            </div>

        </div>


    </div>
</section>
<!-- /Team -->




<!-- 4 Box -->
<section>
    <div class="container">

        <!-- FEATURED BOXES 3 -->
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-dollar"></i>
                    <h4><?php echo $pageData["box5_title"]; ?></h4>
                    <p><?php echo $pageData["box5_desc"]; ?></p>

                    <a href="<?php echo SITE_URL; ?>services-page.php">
                        Read
                        <!-- /word rotator -->
                        <span class="word-rotator active" data-delay="2000" style="height: 21px;">
                            <span class="items" style="top: -21px;">
                                <span>more</span>
                                <span>now</span>
                                <span>more</span></span>
                        </span><!-- /word rotator -->
                        <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-search"></i>
                    <h4><?php echo $pageData["box6_title"]; ?></h4>
                    <p><?php echo $pageData["box6_desc"]; ?></p>

                    <a href="<?php echo SITE_URL; ?>services-page.php">
                        Read
                        <!-- /word rotator -->
                        <span class="word-rotator active" data-delay="2000" style="height: 21px;">
                            <span class="items" style="top: 0px;">
                                <span>more</span>
                                <span>now</span>
                                <span>more</span></span>
                        </span><!-- /word rotator -->
                        <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-shield"></i>
                    <h4><?php echo $pageData["box7_title"]; ?></h4>
                    <p><?php echo $pageData["box7_desc"]; ?></p>

                    <a href="<?php echo SITE_URL; ?>services-page.php">
                        Read
                        <!-- /word rotator -->
                        <span class="word-rotator active" data-delay="2000" style="height: 21px;">
                            <span class="items" style="top: 0px;">
                                <span>more</span>
                                <span>now</span>
                                <span>more</span></span>
                        </span><!-- /word rotator -->
                        <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="text-center">
                    <i class="ico-light ico-lg ico-rounded ico-hover fa fa-user"></i>
                    <h4><?php echo $pageData["box8_title"]; ?></h4>
                    <p><?php echo $pageData["box8_desc"]; ?></p>

                    <a href="<?php echo SITE_URL; ?>services-page.php">
                        Read
                        <!-- /word rotator -->
                        <span class="word-rotator active" data-delay="2000" style="height: 21px;">
                            <span class="items" style="top: 0px;">
                                <span>more</span>
                                <span>now</span>
                                <span>more</span></span>
                        </span><!-- /word rotator -->
                        <i class="glyphicon glyphicon-menu-right size-12"></i>
                    </a>

                </div>
            </div>
        </div>
        <!-- /FEATURED BOXES 3 -->

    </div>
</section>
<!-- 4 BOx -->





<!-- News -->
<section>
    <div class="container">

        <header class="text-center margin-bottom-60 clearfix">
            <h2 class="weight-300 nomargin-bottom">Recent News</h2>
        </header>

        <!-- 
                controlls-over		= navigation buttons over the image 
                buttons-autohide 	= navigation buttons visible on mouse hover only

                data-plugin-options:
                        "singleItem": true
                        "autoPlay": true (or ms. eg: 4000)
                        "navigation": true
                        "pagination": true
                        "items": "4"

                owl-carousel item paddings
                        .owl-padding-0
                        .owl-padding-3
                        .owl-padding-6
                        .owl-padding-10
                        .owl-padding-15
                        .owl-padding-20
        -->
        <div class="owl-carousel owl-padding-10 buttons-autohide controlls-over" data-plugin-options='{"singleItem": false, "items":"4", "autoPlay": 4000, "navigation": true, "pagination": false}'>

            <?php 
            //echo "<pre>";print_r($blog_data);echo "</pre>";
            foreach($blog_data as $blog){ 
                $img_path_url=$blog['img_path'];
                $path_array=pathinfo($img_path_url);
                //echo "<pre>";print_r($path_array);echo "</pre>";
                $new_file_name=$path_array['dirname']."/".$path_array['filename']."-285x214.".$path_array['extension'];

            ?>
            <div class="img-hover">
				<div style="height:200px;">
					<a href="<?php echo SITE_URL . "blog?p=" . $blog["ID"]; ?>">
						<img class="img-responsive" src="<?php echo $new_file_name;?>" alt="" style="height:198px;">
					</a>
				</div>
                <h4 class="text-left margin-top-20"><a href="<?php echo SITE_URL . "blog?p=" . $blog["ID"]; ?>"><?php echo substr($blog['post_title'],0,25);if(strlen($blog['post_title'])>25){ echo " ...";}?></a></h4>
                <p class="text-left"><?php echo substr($blog['post_content'],0,105);if(strlen($blog['post_content'])>105){ echo " ...";}?></p>
                <ul class="text-left size-12 list-inline list-separator">
                    <li>
                        <i class="fa fa-calendar"></i> 
                        <?php echo date("jS M Y",strtotime($blog['post_date']));?>
                    </li>
                    <li>
                        <a href="<?php echo SITE_URL . "blog?p=" . $blog["ID"]; ?>">
                            <i class="fa fa-comments"></i> 
                            <?php echo $blog['comment_count'];?>
                        </a> 
                    </li>
                </ul>
            </div>
            <?php } ?>

        </div>

    </div>
</section>
<!-- /News -->





<!-- APPOINTMENT -->
<section id="appointment" class="alternate">
    <div class="container">

        <header class="text-center margin-bottom-60 clearfix">
            <h2 class="weight-300 nomargin-bottom"><?php echo $pageData["client_review2"]; ?></h2>
        </header>

        
<!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
<script>
  hbspt.forms.create({ 
    css: '',
    portalId: '295496',
    formId: 'd239f77d-0d7f-473f-8da8-7c36f01070c8'
  });
</script>


    </div>
</section>
<!-- /APPOINTMENT -->
<?php require_once('../includes/footer.php'); ?>

<style>

.btn-info {
    background-color: #8d2d86;
    border-color: #8d2d86;
    color: #fff;
}
.btn-info.focus, .btn-info:focus {
    background-color: #a5bb3a;
    border-color: #1b6d85;
    color: #fff;
}
.btn-info:hover {
    background-color: #a5bb3a;
    border-color: #fff;
    color: #fff;
} 
/*for hubspot form*/
.hs-form fieldset{
    max-width:100%;
}   
.hbspt-form input[type="text"]{
    display: block;
    width: 97%;
    height: 40px!important;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        border: #ddd 2px solid;
    box-shadow: none;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    margin-bottom: 20px;
}
.hbspt-form input[type="submit"]{
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    margin-top: 20px;
}
.hbspt-form input[type="email"]{
    display: block;
    width: 97%;
    height: 40px!important;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        border: #ddd 2px solid;
    box-shadow: none;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    margin-bottom: 20px;
}
.hbspt-form textarea{
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    outline: none !important;
    resize: none;
}
.hbspt-form select{
    display: block;
    width: 97%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    border: 2px solid #E5E7E9;
    height: 40px;
    padding: 5px;
    outline: none;
    line-height: 1 !important;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.hs-error-msgs {
    display: none!important;
}
@media (min-width: 800px) and (max-width: 2700px)
{
    .hbspt-form textarea{
        width: 1113px;
        height: 200px;
    }
}
/*for hubspot form*/   
</style>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="<?php echo SITE_URL . "../includes/assets/plugins/slider.revolution.v5/js/jquery.themepunch.tools.min.js"; ?>" ></script>
<script type="text/javascript" src="<?php echo SITE_URL . "../includes/assets/plugins/slider.revolution.v5/js/jquery.themepunch.revolution.min.js"; ?>" ></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    if (jQuery("#rev_slider_98_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_98_1");
    }
    else {
        revapi98 = jQuery("#rev_slider_98_1").show().revolution({
            sliderType: "hero",
            jsFileLocation: plugin_path + "slider.revolution.v5/js/",
            sliderLayout: "fullwidth",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
            },
            responsiveLevels: [1240, 1024, 778, 480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [814, 500, 400, 300],
            lazyType: "none",
            parallax: {
                type: "mouse",
                origo: "slidercenter",
                speed: 2000,
                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            },
            shadow: 0,
            spinner: "off",
            autoHeight: "off",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                disableFocusListener: false,
            }
        });
    }
});
</script>
<script type="text/javascript">
$(function(){
$("#searchid2").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
var randomnumber=Math.floor(Math.random()*11)

	$.ajax({
	type: "POST",
	url: "http://freshlifefloral.com/search-home.php?new="+randomnumber,
	data: dataString,
	cache: false,
	success: function(html)
	{
	 
    $("#result2").html(html).show();
	}
	});
    
});
$("#searchid3").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
var randomnumber=Math.floor(Math.random()*11)

    $.ajax({
    type: "POST",
    url: "http://development.freshlifefloral.com/search-home.php?new="+randomnumber,
    data: dataString,
    cache: false,
    success: function(html)
    {
     
    $("#result3").html(html).show();
    }
    });
    
});
jQuery("#result2").on("click",function(e){ 
	var $clicked = $(e.target);
	var $name = $clicked.find('.name').html();
	var decoded = $("<div/>").html($name).text();
	$('#searchid2').val(decoded);
});
jQuery("#result3").on("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid3').val(decoded);
});
jQuery(document).on("click", function(e) { 
	var $clicked = $(e.target);
	if (! $clicked.hasClass("search")){
	jQuery("#result2").fadeOut(); 
	}
});
jQuery(document).on("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result3").fadeOut(); 
    }
});
$('#searchid2').click(function(){
	// jQuery("#result").fadeIn();
});
$('#searchid3').click(function(){
    // jQuery("#result").fadeIn();
});
});

function dolangwork()
{
   $('#langform').submit();
}

</script>
	</body>
</html>
