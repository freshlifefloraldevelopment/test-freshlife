<?php 
require_once("../config/config_new.php");

#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 13; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . 'includes/assets/css/essentials-flfv5.css', SITE_URL . 'includes/assets/css/layout-flfv5.css',
    SITE_URL . '../includes/assets/css/header-4.css', SITE_URL . 'includes/assets/css/color_scheme/blue.css');
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

require_once '../includes/header.php';
?>
<section class="page-header page-header-xs">
    <div class="container">
        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">How it Work</li>
        </ol><!-- /breadcrumbs -->
    </div>
</section>
<!-- /PAGE HEADER -->
<section>
    <div class="container">
        <div class="box-inner">
            <div class="timeline"><!-- .timeline-inverse = right position [left on RTL] -->
                <?php
                for ($i = 1; $i <= 7; $i++) {
					$s=	explode(",",$pageData['fb'.$i]);
                    if ($pageData['box' . $i . '_title'] != '') {
                        ?>
                        <!-- ITEM -->
                        <div class="timeline-item timeline-item-bordered">
                            <!-- timeline entry -->
                            <div class="timeline-entry rounded"><!-- .rounded = entry -->
                                <?php echo $s[0]; ?><span> <?php echo $s[1]; ?></span>
                                <div class="timeline-vline"><!-- vertical line --></div>
                            </div>
                            <!-- /timeline entry -->
                            <h2 class="uppercase bold size-17"><?php echo $pageData['box' . $i . '_title'] ?></h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img-responsive" src="<?php echo SITE_URL . 'user/' . $pageData['box' . $i . '_img'] ?>" alt="image">
                                    <div class="md-margin-bottom-20"></div>
                                </div>
                                <div class="col-md-8">    
                                    <p><?php echo $pageData['box' . $i . '_desc'] ?></p> 
                                </div>
                            </div>
                        </div>
                        <!-- /ITEM -->
                    <?php }
                }
                ?>
            </div>
        </div>
    </div>
</section>
<?php
require_once '../includes/footer.php';
?>
