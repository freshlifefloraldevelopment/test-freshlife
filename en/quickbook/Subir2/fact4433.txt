*InvoiceNo	*Customer	*InvoiceDate	*DueDate	Terms	Location	Memo	Item(Product/Service)	ItemDescription	ItemQuantity	ItemRate	*ItemAmount	*ItemTaxCode	ItemTaxAmount	Currency
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Brighton 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Brighton 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Explorer 70 cm 25 st/bu 	100	36.25	3625	GST	181.25	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Explorer 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Hot Explorer 70 cm 25 st/bu 	50	36.25	1812.5	GST	90.62	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Hot Explorer 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 70 cm 25 st/bu 	75	36.25	2718.75	GST	135.94	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Moody Blues 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Moody Blues 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Pink Mondial 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Angela & Gabriel's	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Pink Mondial 70 cm 25 st/bu 	25	36.25	906.25	GST	45.31	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Bupleurum	Bupleurum 70 cm 10 st/bu 	20	9.5	190	GST	9.5	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Frutteto 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Gotcha 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Red Wine 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical White 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Mini Green Dark 60 cm 1 st/bu Mini	30	1.5	45	GST	2.25	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Playa Blanca  60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Rubicon 50 cm 10 st/bu 	10	16.5	165	GST	8.25	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Shimmer 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Fine Floral Designs	23/11/2019	23/12/2019	Net 30		Asters	Solidago	Solidago Golden Glory 80 cm 10 st/bu 250gr	20	9.85	197	GST	9.85	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Coral 40 cm 10 st/bu 	10	14.5	145	GST	7.25	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Orange 40 cm 10 st/bu 	10	14.5	145	GST	7.25	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Purple 40 cm 10 st/bu 	10	14.5	145	GST	7.25	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Anemone	Agrogana White. 40 cm 10 st/bu 	30	13.5	405	GST	20.25	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Yellow 40 cm 10 st/bu 	10	14.5	145	GST	7.25	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Snapdragon	Bazzani Red 50 cm 10 st/bu Perfection	10	9.85	98.5	GST	4.93	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Confidential 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Explorer 60 cm 25 st/bu 	25	37.5	937.5	GST	46.88	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Faith 50 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Gotcha 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James Hot Pink 70 cm 10 st/bu 	10	11.75	117.5	GST	5.88	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James Purple 70 cm 10 st/bu 	10	11.75	117.5	GST	5.88	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Peach 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Pink 50 cm 10 st/bu Perfection	10	9.8	98	GST	4.9	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Playa Blanca  60 cm 25 st/bu 	25	50	1250	GST	62.5	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Sahara Sensation 60 cm 10 st/bu 	10	15.5	155	GST	7.75	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Somerset 50 cm 10 st/bu 	10	15.5	155	GST	7.75	CAD
4003	Foxgloves	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Anemone	White 50 cm 10 st/bu Perfection	10	13.5	135	GST	6.75	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Light Pink 40 cm 10 st/bu 	10	19.5	195	GST	9.75	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Light Pink 40 cm 10 st/bu 	10	19.5	195	GST	9.75	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Purple 40 cm 10 st/bu 	10	19.5	195	GST	9.75	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Purple 40 cm 10 st/bu 	10	19.5	195	GST	9.75	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Red 40 cm 10 st/bu 	40	19.5	780	GST	39	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana White 40 cm 10 st/bu 	20	19.5	390	GST	19.5	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Explorer 60 cm 25 st/bu 	75	33.75	2531.25	GST	126.56	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Free Spirit 50 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Freedom 60 cm 25 st/bu 	75	33.75	2531.25	GST	126.56	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James Peach 70 cm 10 st/bu 	20	11.75	235	GST	11.75	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Kahala  60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Proteas and Leucadendrons	Leucadendron	Leucadendrum 65 cm 10 st/bu 	10	8.75	87.5	GST	4.38	CAD
4003	Garden Party Team	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Shimmer 50 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Andate 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Antique White 60 cm 1 st/bu Premiun	12	2.5	30	GST	1.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Antique White 60 cm 1 st/bu Premiun	12	2.5	30	GST	1.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Antique White 60 cm 1 st/bu Premiun	12	2.5	30	GST	1.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Atomic 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Statice	Bazzani Purple 70 cm 10 st/bu 	60	8.75	525	GST	26.25	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Bodine 60 cm 10 st/bu Fancy	20	5.3	106	GST	5.3	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Bupleurum	Bupleurum 70 cm 10 st/bu 	40	9.5	380	GST	19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Confidential 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Eryngium	Deep Blue Jackpot 50 cm 5 st/bu 	15	8.75	131.25	GST	6.56	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Deep Purple 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Greens and Branches	Green Foliages	Dusty Miller 50 cm 10 st/bu 	10	16.5	165	GST	8.25	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Epsilon 60 cm 10 st/bu Fancy	30	5.3	159	GST	7.95	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Extasis 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Fellini 60 cm 10 st/bu Fancy	20	5.3	106	GST	5.3	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Freedom 60 cm 25 st/bu 	100	33.75	3375	GST	168.75	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Frutteto 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Gotcha 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Gransole 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Hermes 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Hermes Orange 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Asters	Asters	Hot Pink. 50 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Ibis 60 cm 10 st/bu Fancy	30	5.3	159	GST	7.95	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Kahala  60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Proteas and Leucadendrons	Leucadendron	Leucadendrum 65 cm 10 st/bu 	40	8.75	350	GST	17.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Limonium	Hybrid Limonium	Limonium Blue 70 cm 12 st/bu 	36	9.8	352.8	GST	17.64	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Red Wine 70 cm 10 st/bu 	20	12.5	250	GST	12.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Minerva 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Olivino 60 cm 10 st/bu Fancy	20	5.3	106	GST	5.3	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Orange Star 60 cm 10 st/bu 	10	13.5	135	GST	6.75	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Pigeon 60 cm 10 st/bu Fancy	20	5.3	106	GST	5.3	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Prado Mint 60 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Asters	Asters	Purple. 50 cm 10 st/bu 	20	10	200	GST	10	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Mini Carnations	Rony 60 cm 10 st/bu Fancy	30	5.3	159	GST	7.95	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Scarlett Mimi 50 cm 10 st/bu 	20	13.5	270	GST	13.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Asters	Solidago	Solidago Golden Glory 80 cm 10 st/bu 250gr	60	9.85	591	GST	29.55	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Sunflowers	Sunflower 70 cm 5 st/bu 	30	7	210	GST	10.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Sunflowers	Sunflower 70 cm 5 st/bu 	10	7	70	GST	3.5	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Gypsophila	Xcelence 75 cm 10 st/bu 250gr	40	16	640	GST	32	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Gypsophila	Xcelence 75 cm 10 st/bu 250gr	10	16	160	GST	8	CAD
4003	Harry’s Flowers	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Zafiro 50 cm 25 st/bu Standard	25	10.5	262.5	GST	13.12	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Statice	Bazzani Purple 70 cm 10 st/bu 	10	8.75	87.5	GST	4.38	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Betsy 60 cm 25 st/bu Standard	25	11	275	GST	13.75	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Faith 50 cm 25 st/bu 	25	25	625	GST	31.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Asters	Asters	Hot Pink. 50 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Limonium	Hybrid Limonium	Limonium Blue 70 cm 12 st/bu 	12	9.8	117.6	GST	5.88	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Green Power 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Pink 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Red Wine 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Triumph 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Playa Blanca  60 cm 25 st/bu 	25	25	625	GST	31.25	CAD
4003	Jennings Florist	23/11/2019	23/12/2019	Net 30		Asters	Asters	Red. 50 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	John den Haan	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Quicksand 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Maple Ridge Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Confidential 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Maple Ridge Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Free Spirit 50 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	Maple Ridge Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Freedom 60 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	Maple Ridge Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Hot Explorer 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Maple Ridge Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Maple Ridge Florist	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana Orange 40 cm 10 st/bu 	10	12	120	GST	6	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Babe 50 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Brighton 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Explorer 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Carnations	Carnations	Gransole 60 cm 25 st/bu Standard	25	11	275	GST	13.75	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Gypsophila	Million Stars 75 cm 10 st/bu 500 gr	30	16	480	GST	24	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Pink 50 cm 10 st/bu Perfection	10	9.5	95	GST	4.75	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Pink Irischka 60 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Chrysanthemum	Spray (Pompoms)	Radost White 50 cm 7 st/bu 	28	8.8	246.4	GST	12.32	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Shimmer 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Chrysanthemum	Disbuds/Mums	Viking Orange 50 cm 7 st/bu 	7	8.8	61.6	GST	3.08	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Anemone	White 50 cm 10 st/bu Perfection	10	9.8	98	GST	4.9	CAD
4003	Oak Bay Flower Shop	23/11/2019	23/12/2019	Net 30		Roses	Spray Roses	Yellow Babe 50 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Pacific Coast Floral Wholesale Ltd	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana White 40 cm 10 st/bu 	10	19.5	195	GST	9.75	CAD
4003	Pacific Coast Floral Wholesale Ltd	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Ranunculus	Agrogana White 40 cm 10 st/bu 	20	19.5	390	GST	19.5	CAD
4003	Pacific Coast Floral Wholesale Ltd	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Hearts 40 cm 25 st/bu 	125	18.75	2343.75	GST	117.19	CAD
4003	Pacific Coast Floral Wholesale Ltd	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mother of Pearl 40 cm 25 st/bu 	75	18.75	1406.25	GST	70.31	CAD
4003	Pacific Coast Floral Wholesale Ltd	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mother of Pearl 50 cm 25 st/bu 	50	18.75	937.5	GST	46.88	CAD
4003	Pacific Coast Floral Wholesale Ltd	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Spinelli 40 cm 25 st/bu 	125	18.75	2343.75	GST	117.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Brighton 60 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Confidential 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Confidential 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Deep Purple 60 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Freedom 60 cm 25 st/bu 	75	33.75	2531.25	GST	126.56	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Gotcha 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Magic Times 60 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hydrangeas	Mini Green Dark 60 cm 1 st/bu Mini	30	1.5	45	GST	2.25	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Pink Mondial 60 cm 25 st/bu 	50	33.75	1687.5	GST	84.38	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Playa Blanca  60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Vendela 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	West Coast Floral Growers & Distr.	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Wasabi 50 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Free Spirit 50 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Freedom 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James Lavander 70 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James Peach 70 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James Purple 70 cm 10 st/bu 	10	10	100	GST	5	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Stock	James White 70 cm 10 st/bu 	20	10	200	GST	10	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Peach 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Pink 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Red Wine 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical Triumph 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Hypericum	Magical White 70 cm 10 st/bu 	10	12.5	125	GST	6.25	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Mondial 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Roses	Standard Rose	Shimmer 60 cm 25 st/bu 	25	33.75	843.75	GST	42.19	CAD
4003	Windsor Market	23/11/2019	23/12/2019	Net 30		Flowers and Fillers	Sunflowers	Sunflower 70 cm 5 st/bu 	20	5	100	GST	5	CAD
