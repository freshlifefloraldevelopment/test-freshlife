<?php
require_once("../config/config_gcp.php");
include('../back-end/GlobalFSyn.php');
include('../back-end/inc/header_ini_stripe.php');
require_once("../functions/functions.php");
if($_SESSION["buyer"]){
  $userSessionID = $_SESSION["buyer"];
}else{
  header("location: variety-page.php");
}
?>

<head>

  <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- MDB -->


<link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/pricing/">
<link href="includes/assets/css/essentials.css" rel="stylesheet" type="text/css"/>
<link href="includes/assets/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="includes/assets/plugins/slider.revolution.v5/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/includes/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript">

window.onload=function() {

var tokenId = '<?=$_POST['stripeToken']?>';
var delivery = '<?=$_POST['delivery']?>';
var orderIDP = '<?=$_POST['orderIdBuyer']?>';
var orderID_Order = '<?=$_POST['order_id']?>';

if(tokenId=='')
{
  if(delivery=='NO-VALUE')
  {
    var datosPayment = "ord_id="+'<?=$_POST['order_id']?>'+"&orderIdBuyer="+'<?=$_POST['orderIdBuyer']?>'+"&key="+'<?=$_POST['key']?>'+"&delivery="+'<?=$_POST['delivery']?>';

      $.ajax({
           type: "POST",
           url: "paymentCodeStripeWithOutDate.php",
           data: datosPayment,
           cache: false,
           success: function(r){
             //alert(r);
             //return false;
             var result = r.substring(0, 1);
             if(result==2){
               swal("Sorry!", "Please, try again!", "error");
               document.getElementById('payM').innerHTML = r.substring(1);
             }else{
               if(result==1){
                 swal("Good job!", "Order completed Successfully!", "success");
                // localStorage.clear();
                 document.getElementById('payM').innerHTML = r.substring(1);
               }


             }

           }
       });
  }
  else
  {
    var datosPayment = "key="+'<?=$_POST['orderIdBuyer']?>'+"&delivery="+'<?=$_POST['delivery']?>';
      $.ajax({
           type: "POST",
           url: "paymentCodeStripe.php",
           data: datosPayment,
           cache: false,
           success: function(r){
             //alert(r);
            //return false;
             var result = r.substring(0, 1);
             if(result==2){
               swal("Sorry!", "Please, try again!", "error");
               document.getElementById('payM').innerHTML = r.substring(1);
             }else{
               if(result==1){
                 swal("Good job!", "Order completed Successfully!", "success");
                // localStorage.clear();
                 document.getElementById('payM').innerHTML = r.substring(1);
               }


             }

           }
       });
  }
}
else
{
  if(delivery=='NO-VALUE')
  {
    var datosPayment = "ord_id="+'<?=$_POST['order_id']?>'+"&orderIdBuyer="+'<?=$_POST['orderIdBuyer']?>'+"&token="+'<?=$_POST['stripeToken']?>'+"&key="+'<?=$_POST['key']?>'+"&delivery="+'<?=$_POST['delivery']?>';
    $.ajax({
         type: "POST",
         url: "paymentStripeWithOutDate.php",
         data: datosPayment,
         cache: false,
         success: function(r){

           var result = r.substring(0, 1);

           if(result==2){
             swal("Sorry!", "Please, try again!", "error");
             document.getElementById('payM').innerHTML = r.substring(1);
           }else{
             if(result==1){
               swal("Good job!", "Order completed Successfully!", "success");
              // localStorage.clear();
               document.getElementById('payM').innerHTML = r.substring(1);
             }

           }

         }
     });
  }
  else
  {
    var datosPayment = "token="+'<?=$_POST['stripeToken']?>'+"&key="+'<?=$_POST['orderIdBuyer']?>'+"&delivery="+'<?=$_POST['delivery']?>';


    $.ajax({
         type: "POST",
         url: "paymentStripe.php",
         data: datosPayment,
         cache: false,
         success: function(r){
        //  alert(r);
        //  return false;
           var result = r.substring(0, 1);

           if(result==2){
             swal("Sorry!", "Please, try again!", "error");
             document.getElementById('payM').innerHTML = r.substring(1);
           }else{
             if(result==1){
               swal("Good job!", "Order completed Successfully!", "success");
              // localStorage.clear();
               document.getElementById('payM').innerHTML = r.substring(1);
             }


           }

         }
     });
  }
}

}
</script>




    <!-- Favicons -->

<link rel="icon" href="https://app.freshlifefloral.com/images/favicon.ico">
<meta name="theme-color" content="#7952b3">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .spinner-grow {
      position: fixed;
      top: 50%; right: 0;
      bottom: 0; left: 50%;
  }


    </style>


    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/5.0/examples/pricing/pricing.css" rel="stylesheet">
  </head>
<body>

<div class="d-flex justify-content-center">
<div class="spinner-grow text-success" style="width: 3rem; height: 3rem" role="status">
  <span class="visually-hidden">Loading...</span>
</div>
</div>
<div class="d-flex flex-fill" id="wrapper_content">
    <div class="flex-fill" id="middle">
      <div id="payM">

      </div>
    </div>
</div>

</body>
