
<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/

require_once("../config/config_gcp.php");

function calculateKilo($userSessionID,$con){
   $getBuyerShippingMethod = "select shipping_method_id
                              from buyer_shipping_methods
                             where buyer_id ='" . $userSessionID . "'";

  $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
  $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
   $shipping_method_id = $buyerShippingMethod['shipping_method_id'];

     $getShippingMethod = "select connect_group
                             from shipping_method
                            where id='" . $shipping_method_id . "'";

      $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
      $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

       $temp_conn = explode(',', $shippingMethodDetail['connect_group']);

      $id_conn = $temp_conn[1];  // Default

      $getConnect = "select charges_per_kilo
                       from connections
                      where id='" . $id_conn . "'";

      $rs_connect = mysqli_query($con, $getConnect);

      $charges = mysqli_fetch_assoc($rs_connect);

       /////////////////////////////////////////////////////////////

      $cost = $charges['charges_per_kilo'];
      $cost_un = unserialize($cost);

      $cost_sum = 0;

      foreach ($cost_un as $key => $value) {
          $cost_sum = $cost_sum + $value;
      }

      return $charges_per_kilo_trans =  $cost_sum;
}


class pagination{
    var $limit = 10;
    var $adjacentLinks = 2;

  function paginationVarietySubPageGrowers($currentPage, $limit, $p_idcate,$p_act2) {

        global $con;

        if(empty($limit) )
            $limit = $this->limit;
            $page = $currentPage;

        if ($page == 1) {
            $start = 0;
        } else {
            $start = ($page - 1) * $limit;
        }

///////////////////////////////////////////////////////////////////////////////////////////

if ($p_act2 == "getsubcat") {

    ?>

                    <!--option value="">All</option-->
    <?php
            //while ($sub_cat = mysqli_fetch_array($rs_products_sub)) {
    ?>
                    <!--option value="<?php echo $sub_cat['id'] ?>"><?php echo $sub_cat['name'] ?></option-->
    <?php //    ?>


    <?php

} elseif ($p_act2 == "subcatdata") {

    if ($p_idcate == "" || $p_idcate == "undefined") {

        $growersSql = "select p.*
                           from product p
      		          inner join grower_product gp on p.id=gp.product_id
      		          inner join growers g on gp.grower_id=g.id
      		           left JOIN colors as clr ON clr.id=p.color_id
      		          where g.active='active'
                            and g.market_place = '1'
                            and p.status = 0
                          GROUP BY p.id";

        $query = mysqli_query($con, $growersSql);
        $totalResultsCount = $query->num_rows;


        $sel_products = "select p.id             ,p.categoryid     ,p.subcategoryid  ,
                                p.name           ,p.image_path     ,p.color_id       ,
                                g.id as idg,gp.grower_id
                           from product p
      		          inner join grower_product gp on p.id=gp.product_id
      		          inner join growers g on gp.grower_id=g.id
      		           left JOIN colors as clr ON clr.id=p.color_id
      		          where g.active='active'
                            and g.market_place = '1'
                            and p.status = 0
                          GROUP BY p.id ,p.categoryid ,p.subcategoryid  , p.name , p.image_path ,p.color_id , g.id ,gp.grower_id
                          ORDER BY p.subcategoryid LIMIT $start,$limit  ";
    } else {

        $growersSql = "select p.*
                            from product p
		           inner join grower_product gp on p.id=gp.product_id
		           inner join growers g on gp.grower_id=g.id
		            left JOIN colors as clr ON clr.id=p.color_id
		           where g.active='active'
                             and g.market_place = '1'
                             and p.status = 0
                             AND p.categoryid='" . $p_idcate . "'
                           GROUP BY p.id";

        $query = mysqli_query($con, $growersSql);
        $totalResultsCount = $query->num_rows;

          $sel_products = "select p.id             ,p.categoryid     ,p.subcategoryid  ,
                                p.name           ,p.image_path     ,p.color_id       ,
                                g.id as idg,gp.grower_id
                            from product p
		           inner join grower_product gp on p.id=gp.product_id
		           inner join growers g on gp.grower_id=g.id
		            left JOIN colors as clr ON clr.id=p.color_id
		           where g.active='active'
                             and g.market_place = '1'
                             and p.status = 0
                             AND p.categoryid='" . $p_idcate . "'
                           GROUP BY p.id ,p.categoryid ,p.subcategoryid  , p.name , p.image_path ,p.color_id , g.id ,gp.grower_id
                           ORDER BY p.subcategoryid LIMIT $start,$limit ";
    }
     //////////////////////////////////////////////////
    $rs_products = mysqli_query($con, $sel_products);
    $number_row = mysqli_num_rows($rs_products);

    $selected_color_array = array();

    if ($number_row > 0) {

      $i = 0;
      $idGrowerSelect;
        while ($products = mysqli_fetch_array($rs_products))
        {

          $i = $i+1;

            $sel_imagen = "select im.id         , im.product_id , im.grower_id  ,
                                  im.image_path , im.imagen
                             from grower_product_img im
                            inner join product p on im.product_id = p.id
                            where im.product_id = '" . $products['id'] . "'
                              and im.imagen = 1
                              and p.status = 0  ";

                $rs_imagen = mysqli_query($con, $sel_imagen);
                $number_img = mysqli_num_rows($rs_imagen);

                $rowImg = mysqli_fetch_array($rs_imagen);

                    if ($number_img > 0) {
                     $default_img = $rowImg["image_path"];
                    }else {
                     $default_img = $products['image_path'];
                    }


            array_push($selected_color_array, $products['color_id']);
            $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

            $rs_catinfo = mysqli_query($con, $sel_catinfo);

            $catinfo = mysqli_fetch_array($rs_catinfo);

            $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));
            $absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/" . $products['image_path'];

    ?>



            <div class="col-lg-4 col-lg-4 mix <?php echo $products['categoryid']; ?>"><!-- item -->

                <div class="item-box">
                    <figure class="w-100 d-inline-block m-0 overflow-hidden rounded-circle bg-light bg-cover lazy" data-loaded="true">
                      <img src="<?php echo "/" . $products['image_path'] ?>" width="250px" height="250px">
				    <span class="item-hover">
						<span class="overlay dark-5"></span>
						<span class="inner">

							<!-- lightbox -->

							<a class="ico-rounded lightbox" href="<?php echo "/" . $products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
								<span class="fa fa-plus size-20">

                </span>
							</a>

                            <!-- details -->
							<a class="ico-rounded" href="<?php echo '/en/variety-sub-page.php?id=' . $products['id']; ?>">
								<span class="glyphicon glyphicon-option-horizontal size-20"></span>
							</a>

						</span>
                                    </span>

                                    <div class="item-box-overlay-title"  >
                                            <h3><?php echo $products["name"]; ?></h3>
                                            <ul class="list-inline categories nomargin">
                                                <!--li><a href="#"><?php echo $catinfo['name']." ".$start." ".$prueba ?></a></li-->
                                                <li><a href="#"><?php echo $catinfo['name'] ?></a></li>
                                            </ul>
                                    </div>



                    </figure>

                    <?php
                    if($_SESSION["buyer"]){

                      $userSessionID = $_SESSION["buyer"];
                    }
                      else{

                        $userSessionID="N/A";
                      }

                      if($userSessionID != "N/A"){


                     ?>

                    <form name="frmproduct" method="post" onsubmit="" enctype="multipart/form-data">
                      <div id="div_cart">

                        <?php

                        $growers_sql1 = "select g.id , g.growers_name,
                                                (select TIMESTAMPDIFF(DAY, max(date_added), now()) from invoice_requests where grower_id = gp.grower_id ) as dias_transcurridos
                                             from grower_product gp
                                            inner join growers  g on gp.grower_id  = g.id
                                            where gp.product_id = '".$products['id']."'
                                              and g.active='active'
                                              and g.market_place = '1'
                                            order by g.growers_name";

                        $rs_growers1 = mysqli_query($con,$growers_sql1);

                        ?>
                        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                        <div class="field">
                            <select id="sub_cat_change1<?php echo $i; ?>" onchange="removeDisabledP(<?php echo $i; ?>);" class="listmenu"  style="font-family: FontAwesome, Verdana; margin-left:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                <option value="0">Select Grower</option>
                                  <optgroup label="Best Deal">
                                    <option value="100000">Best Deal</option>
                                   </optgroup>
                            <optgroup label="Regular Options">
                                <?php
                                while ($row_grower1 = mysqli_fetch_array($rs_growers1)){
                                ?><option value="<?php echo $row_grower1['id']; ?>"><?php echo $row_grower1['growers_name']." (".$row_grower1['dias_transcurridos'].")" ?></option>
                                <?php } ?>
                            </optgroup>
                            </select>

                        </div>



                        <?php
                      // se eliminó  después de and gpb.prodcutid = '".$products['id']."' "and gpb.price > 0"
                      $tasaKilo = calculateKilo($userSessionID,$con);

                       $sql_price = "select gp.id,gp.price_adm as price,   s.name as sizename   ,
                                           f.name as featurename, gp.feature as feature, s.id size_id,
                                           gp.factor , gp.stem_bunch , b.name as stems
                                      from grower_parameter gp
                                     inner JOIN sizes s ON gp.size = s.id
                                     inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                                      left JOIN features f  ON gp.feature = f.id
                                     where gp.idsc = '".$products['subcategoryid']."' " ;


                                        $rs_price = mysqli_query($con,$sql_price);

                      ?>

                        <div class="field">
                            <select id="sub_cat_change2<?php echo $i; ?>" disabled="true" onchange="removeDisabledB(<?php echo $i; ?>,<?php echo $products['id']; ?>)" class="listmenu"  style="margin-left:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                        <option value="0">Select Price</option>
                                        <?php
                                        while ($row_price = mysqli_fetch_array($rs_price)) {
                                           $priceBunch = $tasaKilo * $row_price['factor'];
                                          $priceSteam = $priceBunch / $row_price['stems'];
                                           $priceCalculado = sprintf('%.2f',round($priceSteam + $row_price['price'],2));
                                        ?>
                                    <option value="<?php echo $priceCalculado."/".$row_price['sizename']."/".$row_price['feature']."/".$row_price['size_id'];  ?>"><?php echo $priceCalculado." ".$row_price['sizename']." cm. ".$row_price['featurename'] ?></option>

                            <?php
                                  } ?>

                            </select>
                        </div>

                        <!-- Drop down Boxes -->

                            <div class="field">
                                <select id="sub_cat_change3<?php echo $i; ?>" disabled="true" onchange="removeDisabledC(<?php echo $i; ?>)" class="listmenu" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                            <option value='0'>Select Boxes</option>
                                </select>
                            </div>


                    <!-- Drop down precios -->


                        <div class="field">
                            <select id="sub_cat_change4<?php echo $i; ?>" disabled="true" onchange="removeDisabledD(<?php echo $i; ?>)" class="listmenu" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                        <option value="0">Select Quantity</option>
                                        <?php

                                            for($k=1;$k<=50;$k++) {
                                        ?>
                                        <option value="<?php echo $k; ?>"><?php echo $k." boxes"; ?></option>
                                        <?php
                                          }
                                        ?>

                            </select>
                        </div>

                        <!-- Drop down precios -->


                            <div class="field">
                                <select id="sub_cat_change6<?php echo $i; ?>" disabled="true" onchange="removeDisabledF(<?php echo $i; ?>)" class="listmenu" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px; display:none">
                                            <option value="0">Select Steams</option>
                                            <?php

                                                for($p=1;$p<=500;$p++) {
                                            ?>
                                            <option value="<?php echo $p; ?>"><?php echo $p." steams"; ?></option>
                                            <?php
                                              }
                                            ?>

                                </select>
                            </div>

                        <div class="field">
                        <button  id="sub_cat_change5<?php echo $i; ?>" type="button" onclick="msnCart(<?php echo $i; ?>,<?php echo $products['id']; ?>,'')" disabled style="margin-left:10px; margin-top:10px; height:35px; width:230px;border-radius: 5px; background-color: #dee1e3; color:white;">
                        <i class="fa fa-cart-plus"></i> Add Cart</button>
                      </div>

                      </div>

                     </form>
                   <?php } ?>
                </div>

            </div><!-- /item -->

            <?php

        }

    } else { ?>
        <div class="notfound" style="margin-left: 20px;"> </div>
    <?php

    }

 }
         $this->generatePaginationLinks($limit, $this->adjacentLinks, $totalResultsCount, $page);
 }
    /**
     * GENERATES PAGINATION LINKS
     * @param type $limit
     * @param type $adjacentLinks number of links to be displayed
     * @param type $totalResults TOTAL RESULT COUNT
     * @param int $page CURRENT PAGE
     */
    private function generatePaginationLinks($limit, $adjacentLinks, $totalResultsCount, $page)   {
        #echo "<font color=red>LIMIT:$limit :::\$adjacentLinks:$adjacentLinks :::\$totalResultsCount:$totalResultsCount :::\$page:$page</font><br>";
        $pagination = '';

        if ($page == 0)
            $page = 1;     //if no page var is given, default to 1.

        $prev     = $page - 1;       //previous page is page - 1
        $next     = $page + 1;       //next page is page + 1
        $prev_    = '';
        $first    = '';
        $lastpage = ceil($totalResultsCount / $limit);
        $next_    = '';
        $last     = '';

        if ($lastpage > 1) {

            //previous button
            if ($page > 1)
                $prev_.= "<li><a class='page-numbers' href='?page=$prev' >prev</a></li>";
            else {
                //$pagination.= "<span class=\"disabled\">previous</span>";
            }

            //pages
            if ($lastpage < 5 + ($adjacentLinks * 2)) { //not enough pages to bother breaking it up
                $first = '';
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><span class='page-numbers' >$counter</span></li>";
                    else
                        $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                }
                $last = '';
            }
            elseif ($lastpage > 3 + ($adjacentLinks * 2)) { //enough pages to hide some
                //close to beginning; only hide later pages
                $first = '';
                if ($page < 1 + ($adjacentLinks * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacentLinks * 2); $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span>$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last.= "<li><a class='page-numbers' href='?page=$lastpage' >Last</a></li>";
                }

                //in middle; hide some front and some back
                elseif ($lastpage - ($adjacentLinks * 2) > $page && $page > ($adjacentLinks * 2)) {
                    $first.= "<li><a class='page-numbers' href='?page=1' >First</a></li>";
                    for ($counter = $page - $adjacentLinks; $counter <= $page + $adjacentLinks; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span>$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last.= "<li><a class='page-numbers' href='?page=$lastpage' >Last</a></li>";
                }
                //close to end; only hide early pages
                else {
                    $first.= "<li><a class='page-numbers' href='?page=1' >First</a></li>";
                    for ($counter = $lastpage - (2 + ($adjacentLinks * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span class='' >$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last = '';
                }
            }
            if ($page < $counter - 1)
                $next_.= "<li><a class='page-numbers' href='?page=$next'>next</a></li>";
            else {
                //$pagination.= "<span class=\"disabled\">next</span>";
            }
            $pagination = "<ul class='pagination pagination-simple pull-right'>" . $first . $prev_ . $pagination . $next_ . $last . "</ul>";
        }

        echo $pagination;
    }

}

if($_POST['action'] == 'varietySubPageGrowersPagination')
{
    $currentPage = $_POST['page'];
    $product_id  = $_POST['pid'];

    $idCategoria = $_REQUEST['cat_main_id'];
    $idAction    = $_POST['action2'];


    $limit = 15;

    $pagination = new pagination();
    $pagination->paginationVarietySubPageGrowers($currentPage, $limit, $idCategoria,$idAction);
}


?>
