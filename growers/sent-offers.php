<?php
require_once("../config/config_gcp.php");
// PO 2018-07-02

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["grower"];
/* * *******get the data of session user*************** */
$sel_info = "select * from growers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$img_url = 'profile_images/noavatar.jpg';
if ($info["file_path5"] != '') {
    $k = explode("/", $info["file_path5"]);
    $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);
    $img_url = SITE_URL . "user/logo/" . $k[1];
}
$page_request = "sent_offers";
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_growers.php";

$query = "select gpb.*,bo.del_date,bo.shipping_method,gor.marks,gor.cargo, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,ff.name as featurename,rg.gprice,gor.boxqty  
                from buyer_requests gpb
           left join product            p   on gpb.product     = p.id
          inner join request_growers    rg  on gpb.id          = rg.rid
          inner join buyer_orders       bo  on gpb.id_order    = bo.id          
           left join subcategory        s   on p.subcategoryid = s.id  
           left join features           ff  on gpb.feature     = ff.id
           left join grower_offer_reply gor on gpb.id          = gor.offer_id 
           left join sizes              sh  on gpb.sizeid      = sh.id 
          where gor.grower_id='" . $userSessionID . "' and gor.reject=0 and gor.status in (0,1) and gor.id is not null group by gpb.id order by gpb.id desc";

$result = mysqli_query($con, $query);
$tp = mysqli_num_rows($result);



?>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Offers Status</h1>
        <ol class="breadcrumb">
            <li><a href="#">Check</a></li>
            <li class="active">Sent Offers</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Sent Offers</strong> <!-- panel title -->
                </span>
                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->
            </div>
            <!-- panel content -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin" style="padding:8px!important;"> 
                        <thead>
                            <tr>
                                <th>Boxes</th>
                                <th>Product</th>
                                <!--<th>Price</th>-->
                                <th>Comment</th>
                                <th>LFD</th>
                                <th>Cargo Agency</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            
                            while ($producs = mysqli_fetch_array($result)) { //echo '<pre>';print_r($producs);die;

                                $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag from grower_offer_reply gr 
                                                                        left join growers g on gr.grower_id=g.id 
                                                                        left join country cr on g.country_id=cr.id
                                                                        where gr.offer_id='" . $producs["cartid"] . "' and gr.buyer_id='" .$producs["buyer"]. "' and g.id is not NULL order by gr.date_added desc";                                                          
    
                                    $rs_growers1 = mysqli_query($con, $sel_check);
                                    $bq = '0';
                                    
                                    while ($growers_1 = mysqli_fetch_assoc($rs_growers1)) {
                                        $bq = $growers_1['boxqty'] + $bq;
                                    }

                                    $query1 = " select b.coordination ,  b.address ,  b.city ,  b.country , b.company box_marks,  b.phone 
                                                 from buyers b
                                                where b.id = '" . $producs["buyer"] . "'  " ;

                                    $result1 = mysqli_query($con, $query1);                                    
                                    
                                    while ($cag = mysqli_fetch_array($result1)) {
                                        // $agency     = $cag["agency"];
                                           $coordi     = $cag["coordination"];
                                        // $invoiceName= $cag["invoiceName"];
                                           $boxMarks   = $cag["box_marks"]; 
                                           $countr     = $cag["country"]; 
                                           $city       = $cag["city"];                                                                                      
                                           $addres     = $cag["address"];
                                           $phone      = $cag["phone"];
                                    }
                                    
                                    $remaning_qty = "";
                                    if (!empty($bq) && ($producs["qty"] < $bq || $producs["qty"] == $bq )) {
                                    } else {
                                        $remaning_qty = $producs["qty"] - $bq;
                                    }
                                           
                                ?>
                                <tr><?php
                                    $temp = explode("-", $producs["boxtype"]);
                                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                    $rs_box_type = mysqli_query($con, $sel_box_type);
                                    $box_type = mysqli_fetch_array($rs_box_type);
                                    ?>
                                    <td><?= $producs["boxqty"] ?> <?= $box_type["name"] ?> Box(es)</td>
                                    <td style="font-size:13px;">
                                        <?php if ($producs["type"] != 3) {
                                            ?> 
                                            <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?>cm 
                                        <?php } else { ?>
                                            Assorted
                                        <?php }
                                        ?>
                                    </td>
                                    <!--<td>$1.5</td>-->
                                    <td><a href="#" data-toggle="modal" data-target=".text_modal_<?php echo $i; ?>">
                                            <?php
                                            if ($producs["comment"] != "") {
                                                echo substr($producs["comment"], 0, 15) . '...';
                                            } else {
                                                echo "No Comment";
                                            }
                                            ?>
                                        </a>
                                        <!-- Text Modal >-->
                                        <div class="modal fade text_modal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">

                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                                                        <h4 class="modal-title" id="mySmallModalLabel">Comment</h4>
                                                    </div>

                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <?php
                                                        if ($producs["comment"] != "") {
                                                            echo $producs["comment"];
                                                        } else {
                                                            echo "No Comment";
                                                        }
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Text Modal >-->
                                    </td>

                                    <td> <?php
                                    
                                    $getShippingMethod = "select connections from shipping_method where id='" . $producs["shipping_method"] . "'";
                                    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
                                    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
                                    $connections = unserialize($shippingMethodDetail['connections']);
    
                                    foreach ($connections as $connection) {
                                            $getDiasTran = mysqli_query($con, "select trasit_time from connections where id='" . $connection . "'");
                                            $conTransito = mysqli_fetch_assoc($getDiasTran);                
                                            $tran_transito = $conTransito['trasit_time'];
                                    }
                                    $delivery = $producs["del_date"];
    
                                    $sqlids = "SELECT date_add('".$delivery."', INTERVAL -".$tran_transito." DAY) fecha";  
                                    $row_sqlids = mysqli_query($con, $sqlids);
                                                            
                                    while ($fila =mysqli_fetch_assoc($row_sqlids)) {
                                            $date_lfd= $fila["fecha"];
                                    }
    
                                    $fecha_tmp = $date_lfd;
                                    $dayofweek = date('D', strtotime($date_lfd));  
                                    if ($dayofweek == "Sun")  {
                                        $date_lfd = strtotime ('-2 day',strtotime ($fecha_tmp) );
                                        $date_lfd = date ( 'Y-m-j' , $date_lfd );
                                    }else {
                                        $date_lfd=$fecha_tmp;                    
                                    }                                                            
    
                                    // echo $date_lfd; po
                                       echo $producs["lfd"]

                                        /*
                                        if ($producs["lfd2"] == '0000-00-00') {
                                            
                                            $lfd_temp = explode("-", $producs["lfd"]);
                                            echo $lfd_temp[1] . "-" . $lfd_temp[2] . "-" . $lfd_temp[0];
                                            
                                        } else {
                                            $lfd_temp = explode("-", $producs["lfd2"]);
                                            echo $lfd_temp[1] . "-" . $lfd_temp[2] . "-" . $lfd_temp[0] . " to <br/>";
                                            
                                            $lfd_temp = explode("-", $producs["lfd"]);
                                            echo $lfd_temp[1] . "-" . $lfd_temp[2] . "-" . $lfd_temp[0] . " ";
                                        }*/
                                        ?></td>
                                    
                                    <td><a href="javascript:void(0);" id="country_model" data-toggle="modal" data-target="#datosBuyer_<?php echo $i; ?>"><?php echo $producs['cargo'] ?></a>
                                        
 <!-- Transinternational Modal >-->
 <!-- (PO) >-->                                          
 <!--Offer Modal Start-->
                                                            <div class="modal fade" id="datosBuyer_<?php echo $i; ?>" role="dialog" aria-labelledby="aria-labelledby">
                                                                <div class="modal-dialog modal-sm">
                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title">Cargo Agency</h4>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                                <?php
                                                                                    $queryMod = "select ca.name agency, b.coordination , b.address , b.city , c.name country,b.company box_marks,  b.phone 
                                                                                    from buyers b
                                                                                    inner join buyer_shipping_methods bs on bs.buyer_id = b.id
                                                                                    inner join cargo_agency ca           on ca.id       = bs.cargo_agency_id
                                                                                    inner join country c                 on c.id        = b.country
                                                                                    where b.id = '" . $producs["buyer"] . "' 
                                                                                      and bs.shipping_method_id = '" . $producs["shipping_method"] . "'
                                                                                    group by ca.name , b.coordination , b.address , b.city , c.name, b.company , b.phone " ;

                                                                                    $resultMod = mysqli_query($con, $queryMod);                                    
                                    
                                                                                     while ($cag = mysqli_fetch_array($resultMod)) {
                                                                                        $agency     = $cag["agency"];
                                                                                        $coordi     = $cag["coordination"];
                                                                                     // $invoiceName= $cag["invoiceName"];
                                                                                        $boxMarks   = $cag["box_marks"]; 
                                                                                        $countr     = $cag["country"]; 
                                                                                        $city       = $cag["city"];                                                                                      
                                                                                        $addres     = $cag["address"];
                                                                                        $phone      = $cag["phone"];
                                                                                    }
                                                                                   ?>  

                                                                                    <p> Agency  : <?php echo $agency ?> </p>
                                                                                    <p> Coord.  : <?php echo $coordi ?> </p>
                                                                                    <p> Invoice Name: </p>  
                                                                                    <p> Box Marks: <?php echo $boxMarks ?> </p>  
                                                                                    <p> Country  : <?php echo $countr ?> </p>    
                                                                                    <p> City     : <?php echo $city ?> </p>                                                        
                                                                                    <p> Address  : <?php echo $addres ?> </p>
                                                                                    <p> Phone    : <?php echo $phone ?> </p>                                                        
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>                                
                                
 <!--Offer Modal End-->
 <!-- (PO) >-->
 <!-- Transinternationa Modal >-->

                                    </td>
                                                                        
                                    <td>
                                        <?php
										//it will give all the data from the table grower_offer_reply.
                                        $sel_check = "select * from  grower_offer_reply where offer_id='" . $producs["cartid"] . "' and grower_id='" . $userSessionID . "' ";
                                        $rs_check = mysqli_query($con, $sel_check);
                                        $check = mysqli_num_rows($rs_check);
                                        $grower_offer = mysqli_fetch_array($rs_check);
                                       
									   
									   //it will give help to compare the current time with the lfd-*date*
											$tdate2 = date("Y-m-d");
                                            $date12 = date_create($grower_offer["date_added"]);
                                            $date22 = date_create($tdate2);
                                            $interval = $date12->diff($date22);
                                            $checka1 = $interval->format('%R%a');
                                            $expired = 0;
											
											
									   if ($grower_offer["status"] == 1 ) {
                                            $cls = 'success';
                                            $orderstatus = "In progres";
                                        } else if ($grower_offer["reject2"] == 1) {
                                            $cls = 'primary';
                                            $orderstatus = "Bid Closed";
                                        } else {
                                            
                                            if ($checka1 < 2) {
                                                $sel_buyer_info = "select live_days from buyers where id='" . $growers["buyer_id"] . "'";
                                                $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                $buyer_info = mysqli_fetch_array($rs_buyer_info);
                                                if ($buyer_info["live_days"] > 0) {
                                                    $tdate = date("Y-m-d");
                                                    $date1 = date_create($producs['lfd']);
                                                    $date2 = date_create($tdate);
                                                    $interval = $date2->diff($date1);
                                                    $checka2 = $interval->format('%R%a');
                                                    if ($checka2 == 0) {
                                                        $expired = 1;
                                                    } else if ($checka2 > $buyer_info["live_days"] ) {
                                                        $expired = 0;
                                                    } else {
                                                        $expired = 1;
                                                    }
                                                }
                                            } else {
                                                $expired = 1;
                                            }
                                            if ($expired == 0) {
                                                if($remaning_qty > 0){
						    $cls = 'warning';
                                                    $orderstatus = "In Process";                                                   												       
                                                }else{
                                                    $cls = 'primary';												
			                            $orderstatus = "Confirm";     
                                                }                                                
                                            } else {
                                                $cls = 'primary';
                                                $orderstatus = "Bid Closed";
                                            }
											
                                        }
                                        ?> 
                                        <a data-toggle="modal" data-target=".open_offer_modal_<?php echo $i; ?>" class="btn btn-<?php echo $cls ?> btn-xs relative">
                                            <?php echo $orderstatus ?>
											
											
                                        </a>
                                        <!--Offer Modal Start-->
                                        <div class="modal fade open_offer_modal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">

                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel">You have sent offer on <?php
                                                            $temp_date = explode("-", $grower_offer["date_added"]);
                                                            echo $temp_date[1] . "-" . $temp_date[2] . "-" . $temp_date[0];
                                                            ?></h4>
                                                        <?php if ($grower_offer["reject2"] == 1) { ?> <span style="color:#F00;"> Rejected <?php if ($grower_offer["reason2"] != "") { ?> ( <?= $grower_offer["reason2"] ?> ) <?php } ?> </span>  <?php } ?>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Product</th>
                                                                        <th>Bunch Size</th>
                                                                        <th>Bunch Quantity</th>
                                                                        <th>Box Qty/Box Type</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if ($grower_offer["type"] == 2) {
                                                                        $sel_boxes = "select boxname,boxqty,boxtype from grower_reply_box where offer_id='" . $producs["cartid"] . "' and reply_id!=0 group by boxname";
                                                                        $rs_boxes = mysqli_query($con, $sel_boxes);
                                                                        $total_boxes = mysqli_num_rows($rs_boxes);
                                                                        while ($boxes = mysqli_fetch_array($rs_boxes)) {
                                                                            $name = explode("-", $boxes["boxname"]);
                                                                            $sel_box_products = "select * from grower_reply_box where offer_id='" . $producs["cartid"] . "' and reply_id!=0 and boxname='" . $boxes["boxname"] . "'";
                                                                            $rs_box_products = mysqli_query($con, $sel_box_products);
                                                                            while ($box_products = mysqli_fetch_array($rs_box_products)) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?= $box_products["product"] ?></td>
                                                                                    <td><?= $box_products["bunchsize"] ?></td>
                                                                                    <td><?= $box_products["bunchqty"] ?> </td>
                                                                                    <td><?= $boxes["boxqty"] ?> <?= $boxes["boxtype"] ?></td>
                                                                                    <td>$<?= $box_products["price"] ?></td>
                                                                                </tr>      
                                                                            <?php
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if ($check > 1) {
                                                                            $sel_check = "select * from  grower_offer_reply where offer_id='" . $producs["cartid"] . "' and grower_id='" . $userSessionID . "' ";
                                                                            $rs_check = mysqli_query($con, $sel_check);
                                                                            $check = mysqli_num_rows($rs_check);
                                                                            while ($grower_offer = mysqli_fetch_array($rs_check)) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?= $grower_offer["product"] ?>  <?= $grower_offer["size"] ?></td>
                                                                                    <td><?= $grower_offer["bunchsize"] ?></td>
                                                                                    <td><?= $grower_offer["bunchqty"] ?></td>
                                                                                    <td><?= $grower_offer["boxqty"] ?> <?= $grower_offer["boxtype"] ?></td>
                                                                                    <td>$<?= sprintf("%.2f", $grower_offer["price"]) ?></td>
                                                                                </tr>    
                                                                            <?php }
                                                                        }
                                                                    } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!--Offer Modal End-->
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!--  /PANEL -->
    </div>
</section>
<?php include("../includes/footer_new.php"); ?>
