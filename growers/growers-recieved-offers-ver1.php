<?php

// PO 2018-05-17

require_once("../config/config_gcp.php");
if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["grower"];
/* * *******get the data of session user*************** */
$sel_info = "SELECT * FROM growers WHERE id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$img_url = '/imagenes/profile_images/noavatar.jpg';
if ($info["file_path5"] != '') {
    $k = explode("/", $info["file_path5"]);
    $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);
    $img_url = SITE_URL . "user/logo/" . $k[1];
}
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_growers_new.php";

$query = "SELECT gpb.id AS cartid,id_order,order_serial,cod_order,gpb.product,sizeid,feature,noofstems,qty,buyer,
                 gpb.boxtype,gpb.date_added,gpb.type,gpb.bunches,gpb.box_name,lfd,comment, box_id,shpping_method,isy,
                 mreject,bunch_size,unseen,req_qty,bunches2 ,gpb.date_added ,p.name,
                 p.color_id,p.image_path,s.name AS subs,sh.name AS sizename,
                 ff.name AS featurename,rg.gprice AS price,rg.tax AS tax ,rg.shipping AS  cost_ship,rg.handling ,gor.offer_id ,gpb.inventary
            FROM buyer_requests gpb
           INNER JOIN request_growers rg     ON gpb.id          = rg.rid
            LEFT JOIN product p              ON gpb.product     = p.id
            LEFT JOIN subcategory s          ON p.subcategoryid = s.id  
            LEFT JOIN features ff            ON gpb.feature     = ff.id
            LEFT JOIN grower_offer_reply gor ON rg.rid          = gor.offer_id AND rg.gid=gor.grower_id
            LEFT JOIN sizes sh               ON gpb.sizeid      = sh.id 
           WHERE rg.gid='" . $userSessionID . "' AND gpb.lfd>='" . date("Y-m-d") . "'  and   IFNULL(gor.reject,'-1')<>'1'  GROUP BY gpb.id ORDER BY gpb.id DESC";

//echo  $query;


$queryBunche="SELECT  gr.id , gr.box_id ,   b.type  , b.name ,gs.sizes         ,
          gs.bunch_value   ,
          gs.is_bunch      ,
          gs.is_bunch_value,
          s.name AS size_name 
     FROM grower_product_bunch_sizes gs
     LEFT JOIN grower_product_box_packing  gr  ON( gr.sizeid= gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
     LEFT JOIN sizes AS s                      ON  gs.sizes = s.id
     LEFT JOIN boxes AS b                      ON  b.id     = gr.box_id
    WHERE  gs.grower_id='" . $userSessionID . "'   
    ORDER BY s.name";
	
//	$resultBunche = mysqli_query($con, $queryBunche);

$result      = mysqli_query($con, $query);
$result_name = mysqli_query($con, $query);
$total_records = mysqli_num_rows($result);
$product_array = array();

if ($total_records > 0) {
    $p = 0;
    while ($products_name = mysqli_fetch_array($result_name)) {
		 $products_n = $products_name["subs"] . " " . $products_name["name"] . " " . $products_name["featurename"] . " " . $products_name["sizename"] . "cm";
        $product_array[$p] = $products_n;
        $p++;
    }
}

echo $userSessionID;
function order_view($cart_id, $user_session)
{
    global $con;
    $update_state = "UPDATE request_growers SET order_view=1 WHERE rid='" . $cart_id . "' AND gid='" . $user_session . "'";
    mysqli_query($con, $update_state);
}

function shipping_method($shipping_method)
{
    global $con;
    $getShippingMethod = "SELECT * FROM buyer_shipping_methods WHERE shipping_method_id='" . $shipping_method . "'";
    $shippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);
    return $shippingMethodData;

}

function country($shipping_country)
{
    global $con;
    $getCountry = "SELECT * FROM country WHERE id ='" . $shipping_country . "'";
    $countryRes = mysqli_query($con, $getCountry);
    $country = mysqli_fetch_assoc($countryRes);
    return $country;

}

function get_buyer_order($buyer_re_id, $buyer_id)
{
    global $con;
    $sql_buyer_order = "SELECT * FROM buyer_orders WHERE buyer_request_id='" . $buyer_re_id . "' AND is_pending = 0 AND buyer_id='" . $buyer_id . "'";
    $getbuyerorder = mysqli_query($con, $sql_buyer_order);
    $buyerorder = mysqli_fetch_assoc($getbuyerorder);
    return $buyerorder;
}

function get_sum_grower($buyer_re_id, $buyer_id)
{
    global $con;
    $sel_sum_qty = "SELECT Sum(boxqty) FROM grower_offer_reply WHERE offer_id=" . $buyer_re_id . " AND grower_id ='" . $buyer_id . "'";
    $rs_sum_qty = mysqli_query($con, $sel_sum_qty);
    $subm_qty = mysqli_fetch_array($rs_sum_qty);
    return $subm_qty;
}

		
function  sumasdiasemana($fecha,$dias)
{
$datestart= strtotime($fecha);

$datesuma = 15 * 86400;
$diasemana = date('N',$datestart);
$totaldias = $diasemana+$dias;
$findesemana = intval( $totaldias/5) *2 ; 
$diasabado = $totaldias % 5 ; 

if ($diasabado==6) $findesemana++;
if ($diasabado==0) $findesemana=$findesemana-2;

$total = (($dias+$findesemana) * 86400)+$datestart ; 
return $twstart=date('Y-m-d', $total);
}

function get_buyer_shipping_met()
{

}

function get_tax_relations()
{


}

function get_bunchs($product_id, $product_size, $buyer_id)
{
    global $con;
    $getbunches = mysqli_query($con, "SELECT gs.sizes,bs.name AS bunchname  
                                        FROM grower_product_bunch_sizes AS gs 
                                        LEFT JOIN bunch_sizes bs ON gs.bunch_sizes=bs.id 
                                       WHERE gs.grower_id = '" . $buyer_id . "' AND gs.product_id = '" . $product_id . "' 
                                         AND gs.sizes = '" . $product_size . "'");
    
    $rowbunches = mysqli_fetch_assoc($getbunches);
    return $rowbunches;
}

function  box_type($box_type){
    global  $con;
    $sel_box_type = "SELECT id , code as name , descrip FROM units WHERE id='" .$box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type;

}

function boxe_grower($userSessionID){
    global $con;
    $sql_boxes = "SELECT  boxes FROM  growers WHERE  id ='" . $userSessionID . "'";
    $rs_boxes = mysqli_query($con, $sql_boxes);
    $boxes = mysqli_fetch_array($rs_boxes);
    return $boxes;
}

function getShippingMethod($shipping_metod,$conCount,$delDate){
    global $con;
    $getShippingMethod = "select  t.shipping_option ,s.connections from  buyer_shipping_methods b 
                                                              left join  tax_relations t on  b.choose_shipping=t.shipping_method_name
                                                              left join  shipping_method  s on   s.id=t.shipping_option
                                                              where  b.id='" . $shipping_metod . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);

    $trasit_time = 0;
    $days = array();
    if ($conCount == 1) {
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "SELECT trasit_time,days FROM connections WHERE id='" . $connection . "'");
            $conDetail = mysqli_fetch_assoc($getConDetail);
            //echo "<pre>";print_r($conDetail);echo "</pre>";
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $days = explode(',', $conDetail['days']);
            $daysOfWeekDisabled = $conDetail['days'];
            $trasit_time_sh = $conDetail['trasit_time'];
        }
    } else {
        $t = array();
        $dDate = $delDate;
        foreach ($connections as $connection) {
            $getConDetail = mysqli_query($con, "SELECT trasit_time,days FROM connections WHERE id='" . $connection . "'");
            $conDetail = mysqli_fetch_assoc($getConDetail);
            //echo "<pre>";print_r($conDetail);echo "</pre>";
            $trasit_time = $trasit_time + $conDetail['trasit_time'];
            $t[] = $conDetail['trasit_time'];
            $days[] = $conDetail['days'];
            $day = explode(',', $conDetail['days']);
        }
        $ttime = 0;

        for ($j = (count($t) - 1); $j >= 0; $j--) {

            if ($j == (count($t) - 1)) {
                $new_date = date("Y-m-d", strtotime('-' . ($t[$j] + 1) . ' day', strtotime($Date)));
            } else if ($j == 0) {

                $new_date = date("Y-m-d", strtotime('-' . $t[$j] . ' day', strtotime($new_date)));
                $timestamp = strtotime($new_date);

                $day_s = date('w', $timestamp);
                for ($d = 0; $d < count($days); $d++) {
                    //echo $days[$d]."<br>";
                    if ($d == 0) {
                        //echo $days[$d]."</br>";
                        $exp_days = explode(",", $days[$d]);
                        for ($c = (count($exp_days) - 1); $c >= 0; $c--) {
                            //echo $exp_days[$c]."<br>";
                            if ($exp_days[$c] != 0) {
                                if ($exp_days[$c] == $day_s) {
                                    $new_date = date("Y-m-d", strtotime('-1 day', strtotime($new_date)));
                                    break;
                                } else if ($exp_days[$c] < $day_s) {
                                    $minus_days = $day_s - $exp_days[$c];
                                    $minus_days = $minus_days + 1;
                                    $new_date = date("Y-m-d", strtotime('-' . $minus_days . ' day', strtotime($new_date)));
                                    break;
                                }
                            }
                        }
                    }
                }

            } else {
                $new_date = date("Y-m-d", strtotime('-' . $t[$j] . ' day', strtotime($new_date)));
            }

            $ttime = $ttime + $t[$j];

        }
    }


    return $connections;
}

?>
<!--<LINK href="estilos.css" rel="stylesheet" type="text/css">-->
<!-- HEADER -->
<!-- /HEADER -->
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Buyer Requests</h1>
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Make your offers</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">

                            <div id="panel-2" class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="title elipsis">
                                        <strong>Received Offers</strong> <!-- panel title -->
                                    </span>
                                    <!-- right options -->
                                    <ul class="options pull-right list-inline">
                                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                                        <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom"
                                               data-original-title="Close"><i class="fa fa-times"></i></a></li>
                                    </ul>
                                    <!-- /right options -->
                                </div>
                                <!-- panel content -->
                                <div class="panel-body">
                                    <div class="">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Comment</th>
                                                <th>Destiny</th>
                                                <th>LFD</th>
                                                <th> </th>                                                
                                                <th>Units</th>
                                                <th>Price</th>
                                                <th>Estado</th>                                                
                                                <th>Type</th>                                                                                                
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <!--Row Start-->
                                            <?php
                                            
                                            if ($total_records > 0) {
                                                $i = 0;
                                                $s = 1;
                                                
                                                while ($products = mysqli_fetch_array($result)) {
                                                    order_view($products['cartid'], $userSessionID);
                                                    $buyerorder = get_buyer_order($products['cartid'], $products['buyer']);
                                                    $subm_qty = get_sum_grower($products['cartid'], $products['buyer']);
                                                    if ($products["comment"] != "") {
                                                        $product_comment = substr($products["comment"], 0, 20) . '.';
                                                    } else {
                                                        $product_comment = "No Comment";
                                                    }
                                                    ?>
                                                    <tr>
                                                        
                                                        <td><?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] . " cm" ?></td>
                                                        <!--Comment-->
                                                        <td width="200">
                                                            <a href="#" data-toggle="modal" data-target="#text_modal_<?= $s; ?>"><?= $product_comment ?></a>
                                                            <div class="modal fade" id="text_modal_<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                                                                 test="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Comment</h4>
                                                                        </div>
                                                                        <div class="modal-body"><?= $products["comment"]; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $shippingMethodData = shipping_method($products['shpping_method']);
                                                            if ($shippingMethodData['country'] != 0) {
                                                                $country = country($shippingMethodData['country']);
                                                                ?>
                                                                <a href="javascript:void(0);" id="country_model" data-toggle="modal" data-target="#myModal_country_<?php echo $products['shpping_method']; ?>"><?php echo $country['name']; ?>
                                                                    <img src="<?php echo SITE_URL; ?>../includes/assets/images/flags/<?php echo $country['flag'] ?>" width="25"/>
                                                                </a>
                                                            <?php } else {
                                                                echo "--";
                                                            }
                                                            ?>
                                                            
                                                            
                                                            <!-- country Modal -->
                                                            <div class="modal fade" id="myModal_country_<?php echo $products['shpping_method']; ?>" role="dialog" aria-labelledby="aria-labelledby">
                                                                <div class="modal-dialog modal-sm">
                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title">Destiny</h4>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                                <?php
                                                                                    $queryMod = "select ca.name agency, b.coordination , b.address , b.city , c.name country,b.company box_marks,  b.phone 
                                                                                                   from buyers b
                                                                                                  inner join buyer_shipping_methods bs on bs.buyer_id = b.id
                                                                                                   left join cargo_agency ca           on ca.id       = bs.cargo_agency_id
                                                                                                   left join country c                 on c.id        = b.country
                                                                                                  where b.id = '" . $products["buyer"] . "' 
                                                                                                  group by ca.name , b.coordination , b.address , b.city , c.name, b.company , b.phone " ;

                                                                                    $resultMod = mysqli_query($con, $queryMod);                                    
                                    
                                                                                     while ($cag = mysqli_fetch_array($resultMod)) {
                                                                                        $agency     = $cag["agency"];
                                                                                        $coordi     = $cag["coordination"];
                                                                                     // $invoiceName= $cag["invoiceName"];
                                                                                        $boxMarks   = $cag["box_marks"]; 
                                                                                        $countr     = $cag["country"]; 
                                                                                        $city       = $cag["city"];                                                                                      
                                                                                        $addres     = $cag["address"];
                                                                                        $phone      = $cag["phone"];
                                                                                    }
                                                                                   ?>  

                                                                                    <p> Agency  : <?php echo $agency ?> </p>
                                                                                    <p> Coord.  : <?php echo $coordi ?> </p>
                                                                                    <p> Invoice Name: </p>  
                                                                                    <p> Box Marks: <?php echo $boxMarks ?> </p>  
                                                                                    <p> Country  : <?php echo $countr ?> </p>    
                                                                                    <p> City     : <?php echo $city ?> </p>                                                        
                                                                                    <p> Address  : <?php echo $addres ?> </p>
                                                                                    <p> Phone    : <?php echo $phone ?> </p>                                                        
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- END Modal -->
                                                            
                                                            
                                                        </td>
                                                        
                                                        
                                                        <!--LFD-->
                                                        <td><?php
                                                            $d = $buyerorder['lfd_grower'];
                                                            $Date = $products["lfd"];																																								
															
                                                            $sqlids = "select connections from shipping_method  where id='" . $products['shpping_method'] . "'";  
                                                            $row_sqlids = mysqli_query($con, $sqlids);
                                                            $row_sql = mysqli_fetch_assoc($row_sqlids);
                                                            $cost = $row_sql['connections'];
                                                            $cost_un = unserialize($cost);

                                                            foreach ($cost_un as $key => $value) {
                                                                    $value;
                                                            }
                                                            $trasit=0;
                                                            $sqlids = "select trasit_time from connections where id='" . $value . "'";  
                                                            $trasit=0;

                                                            $row_sqlids = mysqli_query($con, $sqlids);
                                                            while ($fila =mysqli_fetch_assoc($row_sqlids)) {
                                                                    $trasit= $fila["trasit_time"];
			   
                                                            }
                                                            $fecha=0;
		
                                                            $sqlids = "SELECT del_date FROM buyer_orders WHERE id='" .$products['id_order']. "'";  
                                                            $row_sqlids = mysqli_query($con, $sqlids);
                                                            while ($fila =mysqli_fetch_assoc($row_sqlids)) {
                                                                    $deldate= $fila["del_date"];		   
                                                            }
                                                           // $trasit=$trasit+1;
		
		
                                                            $sqlids = "SELECT date_add('".$deldate."', INTERVAL -".$trasit." DAY) fecha";  
                                                            $row_sqlids = mysqli_query($con, $sqlids);
                                                            
                                                            while ($fila =mysqli_fetch_assoc($row_sqlids)) {
                                                                      $fecha= $fila["fecha"];
                                        		            }
 
                                                            //echo sumasdiasemana($deldate,$trasit);
                                                            $fecha_lfd = $fecha;

                                                            $dayofweek = date('D', strtotime($fecha));  
                                                            if ($dayofweek == "Sun")  {
                                                                    $fecha = strtotime ('-2 day',strtotime ($fecha_lfd) );
                                                                    $fecha = date ( 'Y-m-j' , $fecha );
                                                            }else {
                                                                    $fecha=$fecha_lfd;                    
                                                            }                                                            

                                                            //echo $fecha;
                                                            
                                                            echo $products["lfd"];
                                                              
                                                              echo "<br>";
                                                            ?>
                                                        </td>
                                                        
                                                        
                                                        
                                                        <!--Boxes-->
                                                        <td><?php
                                                            //echo $products["qty"] . '  ';
                                                            $box_type=box_type($products['boxtype']);                                                            
                                                            //echo $box_type["name"];                                                                                                                        
                                                            ?>
                                                            
                                                        <!--Units-->
                                                        <td>
                                                            <a href="#" data-toggle="modal" data-target="#unit_modal_<?= $s; ?>"><?= $products["qty"] . '  '.$box_type["name"]; ?></a>
                                                            <div class="modal fade" id="unit_modal_<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                                                                 test="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Unit Description</h4>
                                                                        </div>
                                                                        <div class="modal-body"><?= $box_type["descrip"]; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>                                                            
								<input type="hidden" name="box_type_x" id="box_type_x" value="<?= $box_type["name"] ?>">
								<input type="hidden" name="qty_x1" id="qty_x1" value="<?= $products["qty_x"] ?>">
															 
															 
										                </td>
                                                                                                        <!--Price-->
                                                                                                <td>
														
													<?php if( $products['price']>0){
														echo $products['price'];
													}else{
															echo "Open bid";
													}
														
														?></td>
                                                                                                        <!--Estado-->
                                                                                                <td>
														
													<?php if( $products['unseen']>0){
														echo  "OFFER";
													}else{
														echo "Pending";
													}
														
														?>
                                                                                                </td>  
                                                                                                
                                                                                                        <!--Type-->
                                                                                                <td>
														
													<?php if( $products['type']>0){
														echo  "OPEN";
													}else{
														echo "Stand";
													}
														
														?>
                                                                                                </td>                                                                                                
													<!--Action-->
													<td>
													
													  <?php 
													//echo $products['inventary'];
													  if ($products['price'] > 0 && $products['inventary'] >0) { ?>
                                                                <button type="button" class=" btn btn-xs btn-success" onclick="modifyOfferxConfir(<?= $products["cartid"] ?>)" >Confirm</button>

                                                            <?php }?>
													
                                                            <?php if ($products['price'] > 0 && $products['inventary'] < 1 ) { ?>
                                                                <button type="button" class=" btn btn-xs btn-success" data-toggle="modal" id="modify_modal_x<?php echo $s; ?>" data-target=".modify_modal_x<?php echo $s; ?>">Send Offer</button>

                                                            <?php } else { if ($products['inventary'] <1) {?>
                                                                <button type="button" class="btn-xs btn btn-success" data-toggle="modal" id="modify_modal_<?php echo $s; ?>" data-target=".modify_modal_<?php echo $s; ?>">Send Offer</button>
                                                            <?php } } ?>

                                                                
                                                            <!-- Modify Modal >-->
                                                            <div class="modal fade modify_modal_<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-medium" style="width: 1000px">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <?php
                                                                        $rowbunches = get_bunchs($products['product'], $products['sizeid'], $userSessionID);
                                                                        ?>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">
                                                                                <?php echo $products["qty"] . " " . $box_type["name"]; ?>
                                                                                <?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] ?> cm <?php echo $rowbunches['bunchname'] . " st/buc"; ?></h4>
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body"><?php //echo '<pre>'; print_r($products);     ?>
                                                                            <div class="row">
                                                                                <ul class="nav nav-tabs nav-clean">
                                                                                    <li class="dropdown active">
                                                                                        <a data-toggle="dropdown" id="offer_dropdown<?php echo $s; ?>" class="dropdown-toggle" href="#" aria-expanded="false">Offers<span class="caret"></span></a>
                                                                                        <ul class="dropdown-menu" id="offer_ul_s<?php echo $s; ?>">
                                                                                            <?php
                                                                                            $cnt_offer = 1;
                                                                                            if ($products['offer_id'] != "") {
                                                                                                $offer_sql = "SELECT * FROM grower_offer_reply WHERE 
                                                                                                offer_id='" . $products['offer_id'] . "' GROUP BY offer_id_index";
                                                                                                $rs_offer = mysqli_query($con, $offer_sql);
                                                                                                $rs_number_of_row = mysqli_num_rows($rs_offer);

                                                                                                if ($rs_number_of_row > 0) {
                                                                                                    while ($offer_info = mysqli_fetch_array($rs_offer)) {
                                                                                                        ?>
                                                                                                        <li>
                                                                                                            <a data-toggle="tab"
                                                                                                               onclick="getOfferDetailsPopup(<?php echo $offer_info['offer_id']; ?>,<?php echo $offer_info['offer_id_index']; ?>,<?php echo $s; ?>,<?php echo $products['product']; ?>,<?php echo $products['feature']; ?>,<?php echo $products['sizename']; ?>,<?php echo $products['qty']; ?>,<?php echo $products['boxtype']; ?>,<?php echo $products['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                               href="#" aria-expanded="false">
                                                                                                                <?php echo "Offer" . $offer_info['offer_id_index']; ?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <?php
                                                                                                        $cnt_offer++;
                                                                                                    }
                                                                                                }
                                                                                            } ?>
                                                                                            <li>
                                                                                                <a data-toggle="tab" tabindex="-1" href="#"
                                                                                                   onclick="defaultOffer(<?php echo $products['cartid']; ?>,<?php echo $cnt_offer; ?>,<?php echo $s; ?>,<?php echo $products['product']; ?>,<?php echo $products['feature']; ?>,<?php echo $products['sizename']; ?>,<?php echo $products['qty']; ?>,<?php echo $products['boxtype']; ?>,<?php echo $products['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                   aria-expanded="false">
                                                                                                    <i class="fa fa-plus"></i> Add Offer
                                                                                                </a>
                                                                                            </li>

                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class=""><a data-toggle="tab" href="#tab_b_<?php echo $s; ?>" aria-expanded="false">Order Details</a></li>

                                                                                </ul>
                                                                                <!-- tabs content -->
                                                                                <div class="col-md-12 col-sm-12 nopadding">
                                                                                    <div class="tab-content tab-stacked">
                                                                                        <div id="tab_a_<?php echo $s; ?>" class="tab-pane active">
                                                                                            <!-- classic select21 -->
                                                                                            <?php
                                                                                            $sql_boxes = "SELECT  boxes FROM  growers WHERE  id ='" . $userSessionID . "'";
                                                                                            $rs_boxes = mysqli_query($con, $sql_boxes);
                                                                                            $boxes = mysqli_fetch_array($rs_boxes);
                                                                                            $a = substr($boxes['boxes'], 0, -1);
                                                                                            $a = substr($a, 1);


                                                                                            $sel_boxes = " SELECT b.id,b.name,b.width,b.length,b.height ,bo.name AS name_box ,b.type,un.code,unt.name as name_unit
                                                                                                             FROM boxes b 
                                                                                                             LEFT JOIN  boxtype bo    ON  b.type=bo.id 
                                                                                                             LEFT JOIN  units un       ON  b.type=un.id 
                                                                                                             LEFT JOIN  units_type unt ON  un.type=unt.id 
                                                                                                            WHERE  b.id IN(" . $a . ") AND bo.name in ('HB','QB')  ";
                                                                                                                                                                                       
                                                                                            $rs_boxes = mysqli_query($con, $sel_boxes);

                                                                                            $sel_typeu = " select un.id , un.code, un.type , unt.id ,unt.name as name_unit from units un
                                                                                                             left join units_type unt on un.type=unt.id 
                                                                                                            where un.id = '" . $products["boxtype"] . "'; ";
                                                                                                                                                                                       
                                                                                            $rs_typeu = mysqli_query($con, $sel_typeu);  
                                                                                            $type_sel = mysqli_fetch_array($rs_typeu)
                                                                                            
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="request_qty_box" id="request_qty_box_<?php echo $s; ?>"
                                                                                                    onchange="boxQuantityChange(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>)" style="width:300px;">
                                                                                                <option value="">Select Box Quantity</option>
                                                                                                <?php
                                                                                                
                                                                                                $request_qty = round($products["qty"]/100);   
                                                                                                     
                                                                                                if ($request_qty <= 3) {
                                                                                                     $request_qty = 10;                                                                                                        
                                                                                                }
                                                                                                
                                                                                                while ($box_sel = mysqli_fetch_array($rs_boxes)) {
                                                                                                    if ($type_sel['name_unit'] == "Box") {
                                                                                                                $request_qty = $products["qty"];
                                                                                                    }                                                                                                                                                                                                        
                                                                                                    for ($i = 1; $i <= $request_qty; $i++) {
                                                                                                        ?>
                                                                                                        <option value="<?php echo $i . "-" . $box_sel['width'] * $box_sel['length'] * $box_sel['height']; ?>">
                                                                                                            <?= $i . " " . $box_sel['name_box'] . " " . $box_sel['width'] . "x" . $box_sel['length'] . "x" . $box_sel['height']; ?>
                                                                                                        </option>
                                                                                                    <?php }
                                                                                                } ?>
                                                                                            </select> 

                                                             
                                                                                            <input type="hidden" class="cls_hidden_selected_qty" name="cls_hidden_selected_qty" value=""/>
                                                                                            <form name="sendoffer" id="sendoffer<?= $products["cartid"] ?>" method="post">
                                                                                                <input type="hidden" name="mainprice" id="mainprice" value=" <?php
                                                                                                if ($products["price"] != "0.00") {
                                                                                                    echo $products["price"];
                                                                                                }

                                                                                                ?> "/>
                                                                                                <!--Inside Table-->

                                                                                                <input type="hidden" name="offer" id="offer" value="<?= $products["cartid"] ?>">
                                                                                                <input type="hidden" name="buyer" id="buyer" value="<?= $products["buyer"] ?>">
                                                                                                <input type="hidden" name="product" id="product" value="<?= $products["name"] ?> <?= $products["featurename"] ?>">                                                                                                                                                                                                
                                                                                                <input type="hidden" name="product_subcategory" id="product_subcategory" value="<?= $products["subs"] ?>">
                                                                                                <input type="hidden" name="product_ship" id="product_ship" value="1">
                                                                                                <input type="hidden" name="shipp" id="shipp" value="<?= $products["shpping_method"] ?>">
                                                                                                <input type="hidden" name="product_su" id="product_su" value="1">
                                                                                                <input type="hidden" name="volume" id="volume" value="1">
                                                                                                <input type="hidden" name="code_order" id="code_order" value="<?= substr($products["cod_order"], 0, 4) ?>">
                                                                                                <input type="hidden" name="size_name" id="size_name" value="<?= $products["sizename"] ?>"> 
                                                                                                <input type="hidden" name="stems" id="stems" value="<?= $rowbunches['bunchname'] ?>">   
                                                                                                <input type="hidden" name="boxcant" id="boxcant" value="1"> 
                                                                                                <input type="hidden" name="boxtype" id="boxtype" value="<?= $box_type["name"] ?>">                                                                                                 
                                                                                                <input type="hidden" name="boxtypen" id="boxtypen" value="HB">

                                                                                                
                                                                                                <?php

                                                                                                $products["boxtype"];
                                                                                                $temp = explode("-", $products["boxtype"]);
                                                                                                $box_type_id = $temp[0];
                                                                                                $sel_box_ids = "SELECT bt.id AS boxtypeid,bt.name AS boxtype,bo.width,bo.length,bo.height,bo.name 
                                                                                                            AS box_value,go.grower_id,go.boxes FROM grower_product_box go 
                                                                                                            LEFT JOIN boxes bo ON bo.id=go.boxes
                                                                                                            LEFT JOIN boxtype bt ON bo.type=bt.id
                                                                                                            WHERE go.grower_id=" . $userSessionID . " AND bt.id ='" . $box_type_id . "' GROUP BY bo.name";
                                                                                                $rs_box_ids = mysqli_query($con, $sel_box_ids);
                                                                                                $totalboxtype = mysqli_num_rows($rs_box_ids);
                                                                                                $i = 1;
                                                                                                while ($box_ids = mysqli_fetch_array($rs_box_ids)) {
                                                                                                    $sel_weight = "SELECT weight FROM grower_product_box_weight WHERE growerid=" . $userSessionID . " AND  box_id='" . $box_ids["boxes"] . "'";
                                                                                                    $rs_weight = mysqli_query($con, $sel_weight);
                                                                                                    $weight = mysqli_fetch_array($rs_weight);
                                                                                                    $optionbb[$i] .= $box_ids["boxtype"] . "-" . $box_ids["box_value"] . "-" . ($box_ids["length"] * $box_ids["height"] * $box_ids["width"]) . "-" . $weight["weight"];
                                                                                                    $i++;
                                                                                                }
                                                                                                                                                                                                
                                                                                                
                                                                                                ?>
                                                                                                <input type="hidden" name="boxtype-<?= $products["cartid"] ?>" id="boxtype-<?= $products["cartid"] ?>" value="<?php echo $optionbb[1]; ?>">
                                                                                                <div class="table-responsive" style="margin-top:20px;">

                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_<?php echo $s; ?>">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Product</th>
                                                                                                            <th>Bunch Quantity</th>
                                                                                                            <th>Price</th>
                                                                                                            <th>Request</th>                                                                                                            
                                                                                                            <th>  </th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <?php $a = 0; ?>
                                                                                                        <tr id="product_tr_0">
                                                                                                            <?php $a = $a + 1; ?>
                                                                                                            <!--Product-->
                                                                                                            <td>
                                                                                                                <?php
                                                                                                                $sql_box_ids3 = "SELECT  p.name as variety,gr.id AS id,gr.box_id ,b.type,b.name,gs.sizes,gs.bunch_value,gs.is_bunch,gs.is_bunch_value,s.name AS size_name,
                                                                                                                                         fe.name as featurename , bt.name as caja
                                                                                                                                   FROM grower_product_bunch_sizes gs
                                                                                                                                   LEFT JOIN product AS p   ON gs.product_id=p.id
                                                                                                                                   LEFT JOIN grower_product_box_packing  gr ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
                                                                                                                                   LEFT JOIN sizes AS s     ON gs.sizes=s.id
                                                                                                                                   LEFT JOIN boxes AS b     ON b.id=gr.box_id
                                                                                                                                   LEFT JOIN boxtype AS bt  ON b.type=bt.id
                                                                                                                                   LEFT JOIN features AS fe ON fe.id=gr.feature
                                                                                                                                  WHERE  gs.grower_id='" . $userSessionID . "' 
                                                                                                                                    AND bt.name in ('HB','QB')
                                                                                                                                  ORDER BY p.name, s.name  ";                                                                                                                                                                                                        
                                                                                                                                                                                                                                
                                                                                                                
                                                                                                                $sel_box_ids3 = mysqli_query($con, $sql_box_ids3);
                                                                                                                ?>
                                                                                                                <select class="form-controll disabled-css offer-select" name="productsize[]" disabled
                                                                                                                        onchange="changeCMSize(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>,<?php echo $products['boxtype']; ?>, <?php echo $userSessionID; ?>,<?php echo $products["product"]; ?>,<?php echo $products["sizeid"]; ?>)"
                                                                                                                        id="productsize-<?= $products["cartid"] ?>-0">
                                                                                                                    <option value="">Select Product</option>
                                                                                                                    <?php

                                                                                                                    while ($box_ids3 = mysqli_fetch_array($sel_box_ids3)) {
                                                                                                                        $bunch_value_s = "";
                                                                                                                         
                                                                                                                        
                                                                                                                        if ($box_ids3['is_bunch'] == "0") {
                                                                                                                            $bunch_value_s = $box_ids3['bunch_value'];
                                                                                                                        } else {
                                                                                                                            $bunch_value_s = $box_ids3['is_bunch_value'];
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        <!--aqui  estoy  enviando  el  id-->
                                                                                                                        <option value="<?= $box_ids3["id"] ?>" size_id="<?php echo $box_ids3["sizes"]; ?>" prod_name="<?php echo $box_ids3["variety"]; ?>" s_name="<?php echo $box_ids3["size_name"]; ?>" b_type="<?php echo $box_ids3["caja"]; ?>" cod_id="<?php echo $box_ids3["id"]; ?>"> 
                                                                                                                                <?= $box_ids3["caja"] ?> <?= $products["subs"] ?> <?= $box_ids3["variety"] ?> <?= $box_ids3["featurename"] ?> <?= $box_ids3["size_name"] ?> cm <?php echo $bunch_value_s . "st/bu"; ?></option>
                                                                                                                        <?php
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                            </td>
                                                                                                            <!--Bunch Quantity-->
                                                                                                            <td>
                                                                                                                <select class="form-controll disabled-css offer-select" disabled
                                                                                                                        onchange="changeBunchQty(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>)" name="bunchqty[]"
                                                                                                                        id="bunchqty-<?= $products["cartid"] ?>-0">
                                                                                                                    <option value="">Select Bunch QTY</option>
                                                                                                                    <?php
                                                                                                                    for ($i = 1; $i <= 500; $i++) {
                                                                                                                        ?>
                                                                                                                        <option value="<?= $i ?>"><?= $i ?></option>
                                                                                                                    <?php } ?>
                                                                                                                </select>

                                                                                                            </td>
                                                                                                            <!--Price-->
                                                                                                            <td>
                                                                                                                <label id="price_label_<?= $products["cartid"] ?>-0" style="display: none;"><?php echo substr($real_product_price, 0, 4); ?></label>
                                                                                                                <select style="display: block;" name="price[]" disabled id="price-<?= $products["cartid"] ?>-0"
                                                                                                                        class="form-controll modal_option disabled-css offer-select">
                                                                                                                    <option value="">Select Price</option>
                                                                                                                    <?php
                                                                                                                    for ($i = 1; $i <= 999; $i++) { ?>
                                                                                                                    
                                                                                                                                <?php
                                                                                                                                $ipre = $i;
                                                                                                                                
                                                                                                                                if ($i < 10) {?>
                                                                                                                                        <option value="0.0<?php echo $ipre; ?>" price_u="0.0<?php echo $ipre; ?>" >$0.0<?php echo $ipre; ?></option>  
                                                                                                                                <?php   } 
                                                                                                                                ?>  
                                                                                                                                        
                                                                                                                                <?php
                                                                                                                                if ($i>= 10 && $i < 100) {?>
                                                                                                                                        <option value="0.<?php echo $ipre; ?>">$0.<?php echo $ipre; ?></option>  
                                                                                                                                <?php   } 
                                                                                                                                ?>
                                                                                                                                                                                                                                                     
                                                                                                                                <?php
                                                                                                                                if ($i>= 100 && $i<200) {?>
                                                                                                                                        <?php  if (($ipre-100)>= 0 && ($ipre-100)<=9) {?>
                                                                                                                                                <option value="1.0<?php echo $ipre-100; ?>">$1.0<?php echo $ipre-100; ?></option>
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="1.<?php echo $ipre-100; ?>">$1.<?php echo $ipre-100; ?></option>
                                                                                                                                        <?php   } ?>
                                                                                                                                          
                                                                                                                                <?php   } 
                                                                                                                                ?>
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 200 && $i<300 ) {?>
                                                                                                                                        <?php  if (($ipre-200)>= 0 && ($ipre-200)<=9) {?>
                                                                                                                                                <option value="2.0<?php echo $ipre-200; ?>">$2.0<?php echo $ipre-200; ?></option>
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="2.<?php echo $ipre-200; ?>">$2.<?php echo $ipre-200; ?></option>
                                                                                                                                        <?php   } ?>        
                                                                                                                                          
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>   
                                                                                                                                                
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 300 && $i<400) {?>
                                                                                                                                        <?php  if (($ipre-300)>= 0 && ($ipre-300)<=9) {?>
                                                                                                                                                <option value="3.0<?php echo $ipre-300; ?>">$3.0<?php echo $ipre-300; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="3.<?php echo $ipre-300; ?>">$3.<?php echo $ipre-300; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>  
                                                                                                                                
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 400 && $i<500) {?>
                                                                                                                                        <?php  if (($ipre-400)>= 0 && ($ipre-400)<=9) {?>
                                                                                                                                                <option value="4.0<?php echo $ipre-400; ?>">$4.0<?php echo $ipre-400; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="4.<?php echo $ipre-400; ?>">$4.<?php echo $ipre-400; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>                
                                                                                                                            
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 500 && $i<600) {?>
                                                                                                                                        <?php  if (($ipre-500)>= 0 && ($ipre-500)<=9) {?>
                                                                                                                                                <option value="5.0<?php echo $ipre-500; ?>">$5.0<?php echo $ipre-500; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="5.<?php echo $ipre-500; ?>">$5.<?php echo $ipre-500; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>                
                                                                                                                                                
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 600 && $i<700) {?>
                                                                                                                                        <?php  if (($ipre-600)>= 0 && ($ipre-600)<=9) {?>
                                                                                                                                                <option value="6.0<?php echo $ipre-600; ?>">$6.0<?php echo $ipre-600; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="6.<?php echo $ipre-600; ?>">$6.<?php echo $ipre-600; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>
                                                                                                                                
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 700 && $i<800) {?>
                                                                                                                                        <?php  if (($ipre-700)>= 0 && ($ipre-700)<=9) {?>
                                                                                                                                                <option value="7.0<?php echo $ipre-700; ?>">$7.0<?php echo $ipre-700; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="7.<?php echo $ipre-700; ?>">$7.<?php echo $ipre-700; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>                
                                                                                                                                 
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 800 && $i<900) {?>
                                                                                                                                        <?php  if (($ipre-800)>= 0 && ($ipre-800)<=9) {?>
                                                                                                                                                <option value="8.0<?php echo $ipre-800; ?>">$8.0<?php echo $ipre-800; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="8.<?php echo $ipre-800; ?>">$8.<?php echo $ipre-800; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>               
                                                                                                                                  
                                                                                                                                <?php                                                                                                                                
                                                                                                                                if ($i>= 900 && $i<1000) {?>
                                                                                                                                        <?php  if (($ipre-900)>= 0 && ($ipre-900)<=9) {?>
                                                                                                                                                <option value="9.0<?php echo $ipre-900; ?>">$9.0<?php echo $ipre-900; ?></option>  
                                                                                                                                        <?php   }else{ ?>
                                                                                                                                                <option value="9.<?php echo $ipre-900; ?>">$9.<?php echo $ipre-900; ?></option>  
                                                                                                                                        <?php   } ?>        
                                                                                                                                        
                                                                                                                                <?php   }                                                                                                                                 
                                                                                                                                ?>
                                                                                                                                                
                                                                                                                                                
                                                                                                                    <?php }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                            </td>
                                                                                                            <!--Request-->
                                                                                                            <td>
                                                                                                                <?php
                                                                                                                
                                                                                                                $sql_request3 = " select ir.id as rid,  rg.name as prod_name, sz.name as sizename , ir.qty
                                                                                                                                    from  buyer_requests ir
                                                                                                                                    INNER JOIN product rg ON ir.product = rg.id
                                                                                                                                    INNER JOIN sizes sz ON ir.sizeid = sz.id
                                                                                                                                    where id_order = '" . $products["id_order"] . "'  order by rg.name ";                                                                                                                                                                                                        
                                                                                                                                                                                                                                
                                                                                                                
                                                                                                                $sel_request3 = mysqli_query($con, $sql_request3);
                                                                                                                ?>                                                                                                                
                                                                                                                <select class="form-controll disabled-css offer-select" disabled
                                                                                                                        onchange="request_buyer(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>)" name="requestb[]"
                                                                                                                        id="requestb-<?= $products["cartid"] ?>-0">
                                                                                                                    <option value="">Select Request</option>
                                                                                                                    
                                                                                                                    
                                                                                                                    <?php
                                                                                                                       while ($req_ids3 = mysqli_fetch_array($sel_request3)) {
                                                                                                                    ?>
                                                                                                                        <option value="<?= $req_ids3["rid"] ?>" > 
                                                                                                                                <?= $req_ids3["prod_name"]."  ".$req_ids3["sizename"]." cm  ".$req_ids3["qty"]." ST " ; ?></option>
                                                                                                                    <?php }
                                                                                                                    ?>
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                </select>

                                                                                                            </td>    
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            <!--Column name-->
                                                                                                            <td>
                                                                                                                <input type="hidden" class="boxqty<?php echo $s; ?>" name="boxqty[]" id="boxqty_0_<?php echo $s; ?>_<?php echo $products["cartid"]; ?>" value=""/>
                                                                                                                <input type="hidden" name="box[]" id="box" value="$i"/>
                                                                                                                <input type="hidden" name="total_qty_bunches[]" id="total_qty_bunches_0_<?php echo $s; ?>_<?php echo $products["cartid"]; ?>" value="0"/>
                                                                                                                <a class="add_new_row" id="add_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $s; ?>"
                                                                                                                   onclick="addNewVariety(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>,<?php echo $products['boxtype']; ?>,<?php echo $userSessionID; ?>,<?php echo $products["product"]; ?>,<?php echo $products["sizeid"]; ?>)"
                                                                                                                   href="javascript:void(0);"><span class="label label-success">Add Variety </span> </a>&nbsp;
                                                                                                                <a href='javascript:void(0)' id='edit_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $s; ?>'
                                                                                                                   onclick='editVariety(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-edit white"></i> Edit </a>
                                                                                                                <a href='javascript:void(0)' id='delete_variety_0_<?php echo $products["cartid"]; ?>_<?php echo $s; ?>'
                                                                                                                   onclick='deleteVariety(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>)' class="btn btn-default btn-xs"><i
                                                                                                                            class="fa fa-times white"></i> Delete </a>


                                                                                                            </td>
                                                                                                        </tr>


                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <input type="hidden" name="total_a" id="total_a" value="1"/>
                                                                                                    <input type="hidden" name="total_availabel_product" id="total_availabel_product_<?php echo $s; ?>" value=""/>
                                                                                                    <div class="col-lg-4">
                                                                                                        <div class="error-box">
                                                                                                            <span id="errormsg-<?= $products["cartid"] ?>" style="color:#FF0000; font-size:12px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                                                        </div>
                                                                                                        <div class="heading-title heading-border-bottom">
                                                                                                            <h3 style="background-color:transparent!important;">Box Competition</h3>
                                                                                                        </div>

                                                                                                        <div class="progress progress-lg"><!-- progress bar -->
                                                                                                            <div class="progress-bar progress-bar-warning progress-bar-striped active text-left" role="progressbar" id="progressbar_id_<?php echo $s; ?>"
                                                                                                                 aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                                                                                <span>Completed 0%</span>
                                                                                                            </div>
                                                                                                        </div><!-- /progress bar -->
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--Inside Table-->
                                                                                                <input type="hidden" name="hdn_offer_id_index" id="hdn_offer_id_index" value="<?php echo $cnt_offer; ?>"/>
                                                                                            </form>
                                                                                        </div>

                                                                                        <div id="tab_b_<?php echo $s; ?>" class="tab-pane">

                                                                                            <!--Inside Table-->
                                                                                            <div class="table-responsive" id="order_details">
                                                                                                <?php
                                                                                                $buyre_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE id='" . $products['shpping_method'] . "'";
                                                                                                $rs_buyre_detail = mysqli_query($con, $buyre_detail_sql);
                                                                                                $row_buyre = mysqli_fetch_array($rs_buyre_detail);

                                                                                                $country_sql = "SELECT * FROM country WHERE id='" . $row_buyre['country'] . "'";
                                                                                                $rs_country_detail = mysqli_query($con, $country_sql);
                                                                                                $row_country = mysqli_fetch_array($rs_country_detail);
                                                                                                //echo "<pre>";print_r($row_buyre);echo "</pre>";
                                                                                                ?>

                                                                                                <ul class="list-unstyled">
                                                                                                    <div style="float:left;width:300px;"><i class="fa fa-map-marker abs"></i>
                                                                                                        <li class="footer-sprite address">
                                                                                                            <?php echo $row_buyre['first_name'] . " " . $row_buyre['last_name'] ?>,<br><?php echo $row_buyre['address']; ?>,
                                                                                                            <?php
                                                                                                            if ($row_buyre['address2'] != "") {
                                                                                                                echo $row_buyre['address2'] . ",";
                                                                                                            }
                                                                                                            ?>
                                                                                                            <?php
                                                                                                            if ($row_buyre['state'] != "") {
                                                                                                                ?>
                                                                                                                <br><?php echo $row_buyre['state']; ?>,
                                                                                                            <?php }
                                                                                                            if ($row_country['name']) {
                                                                                                                echo $row_country['name'] . "<br>";
                                                                                                            } ?>


                                                                                                        </li>
                                                                                                    </div>
                                                                                                    <?php
                                                                                                    $shipping_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE id='" . $products['shpping_method'] . "'";
                                                                                                    $rs_shipping_detail = mysqli_query($con, $shipping_detail_sql);
                                                                                                    $row_shipping = mysqli_fetch_array($rs_shipping_detail);

                                                                                                    $cargo_agency_sql = "SELECT * FROM cargo_agency WHERE id='" . $row_shipping['cargo_agency_id'] . "'";
                                                                                                    $rs_cargo_agency = mysqli_query($con, $cargo_agency_sql);
                                                                                                    $row_cargo_agency = mysqli_fetch_array($rs_cargo_agency);
                                                                                                    ?>
                                                                                                    <div style="float:left;width:1200px;">
                                                                                                        <i class="fa fa-user  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Client: <?php echo $row_buyre['first_name'] . " " . $row_buyre['last_name'] ?>
                                                                                                        </li>
                                                                                                        <i class="fa fa-plane abs"></i>
                                                                                                        <li class="footer-sprite email">
                                                                                                            Cargo Agency: <?php
                                                                                                            if ($row_cargo_agency['name'] != "") {
                                                                                                                echo $row_cargo_agency['name'];
                                                                                                            } else {
                                                                                                                echo "-";
                                                                                                            }
                                                                                                            ?>
                                                                                                        </li>
                                                                                                        <i class="fa fa-phone  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Coordination : Flora and Design
                                                                                                        </li>
                                                                                                        <i class="fa fa-pencil-square-o  abs"></i>
                                                                                                        <li class="footer-sprite phone">
                                                                                                            Marks: Fresh Life Floral - <?= $row_shipping['shipping_code']; ?>
                                                                                                        </li>

                                                                                                    </div>

                                                                                                </ul>
                                                                                                <a href="#" class="btn btn-3d btn-green"><i class="fa fa-comments-o"></i>Comments: <?= $product_comment; ?></a>
                                                                                            </div>
                                                                                            <!--Inside Table-->

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $b=0; ?>
                                                                        <?php $ss=10; ?>                                                                        
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer noborder offer_model_popup">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <a href="javascript:void(0);" onclick="modifyOffer(<?= $products["cartid"] ?>,<?= $ss+1 ?> )" class="btn btn-primary">Send Offer 1</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                            <div class="modal fade modify_modal_x<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-medium" style="width: 1000px">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <?php


                                                                        $query_bon = "SELECT gs.sizes,bp.qty,bs.name AS bunchname  
                                                                                        FROM grower_product_bunch_sizes AS gs 
                                                                                        LEFT JOIN bunch_sizes bs                ON gs.bunch_sizes=bs.id 
                                                                                        LEFT JOIN grower_product_box_packing bp ON bp.growerid=gs.grower_id AND bp.prodcutid=gs.product_id AND bp.sizeid=gs.sizes AND bs.id= bp.bunch_size_id
                                                                                        LEFT JOIN  boxes  bo                    ON bp.box_id =bo.id
                                                                                       WHERE gs.grower_id = '" . $userSessionID . "' AND gs.product_id = '" . $products['product'] . "' 
                                                                                         AND gs.sizes = '" . $products['sizeid'] . "'
                                                                                         AND bo.type='" . $products['boxtype'] . "' ";
                                                                        
                                                                        $getbunches = mysqli_query($con, $query_bon);
                                                                        $rowbunches = mysqli_fetch_assoc($getbunches);

                                                                        ?>
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X(------)</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">
                                                                                <?= $products["qty"] . " " . $box_type["name"]; ?>
                                                                                <?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] . " cm" ?> <?= $rowbunches['qty'] . "Bu x"; ?><?php echo $rowbunches['bunchname'] . "st"; ?></h4>
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body"><?php //echo '<pre>'; print_r($products);     ?>
                                                                            <div class="row">
                                                                                <ul class="nav nav-tabs nav-clean">
                                                                                    <li class="dropdown active">
                                                                                        <a data-toggle="dropdown" id="offer_dropdown<?= $s; ?>" class="dropdown-toggle" href="#" aria-expanded="false">Offers<span class="caret"></span></a>
                                                                                        <ul class="dropdown-menu" id="offer_ul_s<?= $s; ?>">
                                                                                            <?php
                                                                                            $cnt_offer = 1;
                                                                                            if ($products['offer_id'] != "") {
                                                                                                $offer_sql = "SELECT * FROM grower_offer_reply WHERE 
                                                                                                offer_id='" . $products['offer_id'] . "' GROUP BY offer_id_index";
                                                                                                $rs_offer = mysqli_query($con, $offer_sql);
                                                                                                $rs_number_of_row = mysqli_num_rows($rs_offer);
                                                                                                //echo $rs_number_of_row." Test";
                                                                                                if ($rs_number_of_row > 0) {
                                                                                                    while ($offer_info = mysqli_fetch_array($rs_offer)) {
                                                                                                        ?>
                                                                                                        <li>
                                                                                                            <a data-toggle="tab"
                                                                                                               onclick="getOfferDetailsPopup(<?php echo $offer_info['offer_id']; ?>,<?php echo $offer_info['offer_id_index']; ?>,<?php echo $s; ?>,<?php echo $products['product']; ?>,<?php echo $products['feature']; ?>,<?php echo $products['sizename']; ?>,<?php echo $products['qty']; ?>,<?php echo $products['boxtype']; ?>,<?php echo $products['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                               href="#" aria-expanded="false">
                                                                                                                <?php echo "Offer" . $offer_info['offer_id_index']; ?>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <?php
                                                                                                        $cnt_offer++;
                                                                                                    }
                                                                                                }
                                                                                            } ?>
                                                                                            <li>
                                                                                                <a data-toggle="tab" tabindex="-1" href="#"
                                                                                                   onclick="defaultOffer(<?php echo $products['cartid']; ?>,<?php echo $cnt_offer; ?>,<?php echo $s; ?>,<?php echo $products['product']; ?>,<?php echo $products['feature']; ?>,<?php echo $products['sizename']; ?>,<?php echo $products['qty']; ?>,<?php echo $products['boxtype']; ?>,<?php echo $products['sizeid']; ?>,<?php echo "'" . $box_type["name"] . "'"; ?>)"
                                                                                                   aria-expanded="false"><i class="fa fa-plus"></i> Add Offer
                                                                                                </a>
                                                                                            </li>

                                                                                        </ul>
                                                                                    </li>
                                                                                    <li class=""><a data-toggle="tab" href="#tab_b_<?php echo $s; ?>" aria-expanded="false">Order Details</a></li>

                                                                                </ul>
                                                                                <!-- tabs content -->
                                                                                <div class="col-md-12 col-sm-12 nopadding">
                                                                                    <div class="tab-content tab-stacked">
                                                                                        <div id="tab_a_x_<?php echo $s; ?>" class="tab-pane active">
                                                                                            <!-- classic select22 -->
                                                                                            <?php
                                                                                            $sql_boxes = "SELECT  boxes FROM  growers WHERE  id ='" . $userSessionID . "'";                                                                                            
                                                                                            $rs_boxes = mysqli_query($con, $sql_boxes);
                                                                                            $boxes = mysqli_fetch_array($rs_boxes);
                                                                                            $a = substr($boxes['boxes'], 0, -1);
                                                                                            $a = substr($a, 1);
                                                                                            $sel_boxes = " SELECT b.id,b.name,b.width,b.length,b.height ,bo.name AS name_box ,b.type,un.code,unt.name as name_unit
                                                                                                             FROM boxes b 
                                                                                                             LEFT JOIN  boxtype bo    ON  b.type=bo.id 
                                                                                                             LEFT JOIN  units un       ON  b.type=un.id 
                                                                                                             LEFT JOIN  units_type unt ON  un.type=unt.id 
                                                                                                            WHERE  b.id IN(" . $a . ") AND bo.name in ('HB','QB')  ";
                                                                                         
                                                                                            
                                                                                                                                                                                        
                                                                                            $rs_boxes = mysqli_query($con, $sel_boxes);
                                                                                            
                                                                                            $sel_typeu = " select un.id , un.code, un.type , unt.id ,unt.name as name_unit from units un
                                                                                                             left join units_type unt on un.type=unt.id 
                                                                                                            where un.id = '" . $products["boxtype"] . "'; ";
                                                                                                                                                                                       
                                                                                            $rs_typeu = mysqli_query($con, $sel_typeu);  
                                                                                            $type_sel = mysqli_fetch_array($rs_typeu);                                                                                            
                                                                                            
                                                                                            ?>
                                                                                            <select class="form-control select2 " name="request_qty_box_x" id="request_qty_box_x_<?php echo $s; ?>"
                                                                                                    onchange="boxQuantityChanges(0,<?php echo $products["cartid"]; ?>,<?php echo $s; ?>,<?= $rowbunches['qty'] ?>)" style="width:300px;">
                                                                                                <option value="">Select Box Quantity</option>
                                                                                                <?php
                                                                                                $request_qty = round($products["qty"]/100);
                                                                                                if ($request_qty < 1) {
                                                                                                     $request_qty = 1;                                                                                                        
                                                                                                }
                                                                                                
                                                                                                //$request_qty = 1;  
                                                                                                
                                                                                                while ($box_sel = mysqli_fetch_array($rs_boxes)) {
                                                                                                    if ($type_sel['name_unit'] == "Box") {
                                                                                                                $request_qty = $products["qty"];
                                                                                                    }                                                                                                    
                                                                                                    
                                                                                                    for ($i = 1; $i <= $request_qty; $i++) {
                                                                                                        ?>
                                                                                                        <option value="<?php echo $i . "-" . $box_sel['width'] * $box_sel['length'] * $box_sel['height'] . "-" . $box_sel['name']; ?>">
                                                                                                            <?= $i . " " . $box_sel['name_box'] . " " . $box_sel['width'] . "x" . $box_sel['length'] . "x" . $box_sel['height']; ?>
                                                                                                        </option>
                                                                                                    <?php }
                                                                                                } ?>
                                                                                            </select>
                                                                                            <input type="hidden" name="cls_hidden_selected_qty_x" value=""/>
                                                                                            <form name="sendof" id="sendof<?= $products["cartid"] ?>">  
                                                                                                <input type="hidden" name="offer_x" id="offer_x" value="<?= $products["cartid"] ?>">
                                                                                                <input type="hidden" name="buyer_x" id="buyer_x" value="<?= $products["buyer"] ?>">
                                                                                                <input type="hidden" name="product_x" id="product_x" value="<?= $products["name"] ?> <?= $products["featurename"] ?>">
                                                                                                <input type="hidden" name="product_subcategory_x" id="product_subcategory_x" value="<?= $products["subs"] ?>">
                                                                                                <input type="hidden" name="product_ship_x" id="product_ship_x" value="1">
                                                                                                <input type="hidden" name="shipp_x" id="shipp_x" value="<?= $products["shpping_method"] ?>">
                                                                                                <input type="hidden" name="product_su_x" id="product_su_x" value="1">
                                                                                                <input type="hidden" name="price_x" id="price_x" value="<?= $products["price"] ?>">
                                                                                                <input type="hidden" name="volume_x" id="volume_x" value="1">
                                                                                                <input type="hidden" name="tot_bu_x" id="tot_bu_x" value="0">
                                                                                                <input type="hidden" name="stems_x" id="stems_x" value="<?= $rowbunches['bunchname'] ?>">
                                                                                                <input type="hidden" name="tax_x" id="tax_x" value="<?= $products['tax'] ?>">
												<input type="hidden" name="inventary" id="inventary" value="<?= $products['inventary'] ?>">
                                                                                                <input type="hidden" name="ship_cost_x" id="ship_cost_x" value="<?= $products['cost_ship'] ?>">
                                                                                                <input type="hidden" name="handling_x" id="handling_x" value="<?= $products['handling'] ?>">
                                                                                                <input type="hidden" name="box_type_x" id="box_type_x" value="<?= $box_type["name"] ?>">
                                                                                                <input type="hidden" name="sizename_x" id="sizename_x" value="<?= $products["sizename"] ?>">
                                                                                                <input type="hidden" name="box_volumn_x" id="box_volumn_x" value="0">
                                                                                                <input type="hidden" name="box_name" id="box_name" value="">
                                                                                                <input type="hidden" class="boxqty<?=$s;?>" name="boxqty[]" id="boxqty_0_<?=$s;?>_<?=$products["cartid"];?>" value=""/>
                                                                                                <input type="hidden" name="code_x" id="code_x" value="<?= substr($products["cod_order"], 0, 4) ?>">                                                                                                   
                                                                                                <?php
                                                                                                $i = 1;
                                                                                                ?>
                                                                                                <div class="table-responsive" style="margin-top:20px;">
                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_offer_x_<?php echo $s; ?>">
                                                                                                    </table>
                                                                                                    <table class="table table-bordered table-vertical-middle" id="main_table_x_<?php echo $s; ?>">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Product</th>
                                                                                                            <th>St/Bu</th>
                                                                                                            <th>Total Bunch Quantity</th>
                                                                                                            <th>Price</th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <?php $a = 0; ?>
                                                                                                        <tr id="product_tr_0_0">
                                                                                                            <td><?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] . " cm " ?></td>
                                                                                                            <td><?=$rowbunches['bunchname'] . "st/bu"; ?></td>
                                                                                                            <td><input type="text" name="setear" id="setear-<?= $s?>" disabled></td>
                                                                                                            <td><label id="price_label_<?= $products["cartid"] ?>-0" style="display: none;"><?=substr($real_product_price, 0, 4); ?>
                                                                                                                </label><?=$products["price"]; ?></td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                         </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer noborder offer_model_popup">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <a href="javascript:void(0);" onclick="modifyOfferx(<?= $products["cartid"] ?>)" class="btn btn-success">Send Offer</a>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                            <!-- Modify Modal >-->
                                                            <a href="javascript:void(0);" onclick="rejectOffer('<?php echo $products["cartid"]; ?>', '<?php echo $products["buyer"]; ?>','<?php echo $userSessionID; ?>')" class="btn-xs btn btn-danger">Reject</a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                    $s++;
                                                }   
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="4">No Results Found</td>
                                                </tr>
                                            <?php } ?>
                                            <!--Row end-->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /MIDDLE -->

<script>
    var cant = 0;
    var porcentaje = [];
    function rejectOffer(offer_id, buyer_id, grower_id) {
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>/growers/reject_offer_grower.php',
            data: 'offer_id=' + offer_id + '&buyer_id=' + buyer_id + '&grower_id=' + grower_id,
            success: function (data) {
                data = $.trim(data);
			   if (data == 'true') {
                    alert('Offer has been rejected.');
                    location.reload();
                } else {
                    alert('There is some error. Please try again');
                    location.reload();
                }
            }
        });
    }
    function modifyOffer(id,main_tr) {
    
        var check = 0;
        
        <?php
            $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
            $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
            $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>
                
      // Inicio Validacion Campos
           
           console.log("Price " + $('#price-' + id + '-0').val()); 
           
        if ($('#productsize-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please select Product");
            check = 1;
        }
        
        if ($('#price-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please enter your Price");
            check = 1;
        }else if ($('#price-' + id + '-0').val() != "") {
            if ($('#price-' + id + '-0').val() < <?= $minimum_price["mprice"] ?>) {
                $('#errormsg-' + id).html("price should be greater than <?= $minimum_price["mprice"] ?> ")
                check = 1;
            }
        }
        
        if ($('#boxtype-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please select Box type");
            check = 1;
        }else if ($('#bunchsize-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please select Bunch size");
            check = 1;
        }else if ($('#bunchqty-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please enter Bunch qty / box ");
            check = 1;
        }else if ($('#boxqty-' + id + '-0').val() == "") {
            $('#errormsg-' + id).html("please enter Box qty");
            check = 1;
        }
        
    // Fin Validacion Campos
    
        var nombres=[];
        var medidas=[];   
        var cajas=[];
        var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
        
        for (var i=0; i<=main_tr; i++) {   

            var prod_name = $("#productsize-" + id + "-" + i + " option:selected").attr("prod_name");  
            var s_name = $("#productsize-" + id + "-" + i + " option:selected").attr("s_name");  
            var b_type = $("#productsize-" + id + "-" + i + " option:selected").attr("b_type");  
            
            console.log("Prueba " + prod_name); 
            
            nombres[i]=prod_name;
            medidas[i]=s_name;            
            cajas[i]=b_type;  
            
            console.log("Variedad " + nombres + main_tr + total_tr); 
            console.log("prod_name " + prod_name); 
        }
        
       if (check == 0) {
            $('#addanomo').val("1")
            $('#errormsg-' + id).html("");
         
            
            var k = confirm('Are you sure you want send offer 1');
            if (k == true) {
                $(".disabled-css").removeAttr("disabled");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>growers/growers-send-offers-ajax-ver1.php',
                    data: [$('form#sendoffer' + id).serialize()]+ '&prod_name=' + nombres + '&s_name=' + medidas + '&b_type=' + cajas,
                    success: function (data) {
                        var fichas = data.split('######');
						 jQuery(".progress-bar").animate({width: "100%"}, 6000);
                        $('.count').each(function () {
                            $(this).prop('Counter', 0).animate(
                                {Counter: $(this).text()},
                                {
                                    duration: 7000, easing: 'swing', step: function (now) {
                                    $(this).text(Math.ceil(now));
                                }
                                });
                        });
                        console.log("este  es el  mensaje de  cuanto  esta  enviando:" + fichas[0]);
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
           }
        }
    
    }
    function modifyOfferx(id) {


        var check = 0;
        <?php
        $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
        $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
        $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>

        /*if ($('#productsize-' + id).val() == "") {
         $('#errormsg-' + id).html("please select product size");
         check = 1;
         }

         if ($('#price-' + id).val() == "") {
         $('#errormsg-' + id).html("please enter your price");
         check = 1;
         }else if ($('#price-' + id).val() != "") {
            if ($('#price-' + id).val() <?= $minimum_price["mprice"] ?>) {
                $('#errormsg-' + id).html("price should be greater than <?= $minimum_price["mprice"] ?> ")
                check = 1;
            }
         }

         if ($('#boxtype-' + id).val() == "") {
         $('#errormsg-' + id).html("please select box type");
         check = 1;
         }

         else if ($('#bunchsize-' + id).val() == "") {
         $('#errormsg-' + id).html("please select bunch size");
         check = 1;
         }

         else if ($('#bunchqty-' + id).val() == "") {
         $('#errormsg-' + id).html("please enter bunch qty / box ");
         check = 1;
         }

         else if ($('#boxqty-' + id).val() == "") {
         $('#errormsg-' + id).html("please enter box qty");
         check = 1;
         }*/
        var check = 0;

        if (check == 0) {
            var k = confirm('Are you sure you want send offer 2');
            if (k == true) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>/growers/growers-send-offers-price.php',
                    data: $('form#sendof' + id).serialize(),
                    success: function (data) {
						    console.log(data);
                        var fichas = data.split('######');
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
            }
        }

    }
	
	
	   function modifyOfferxConfir(id) {


        var check = 0;
        <?php
        $sel_minimum_price = "SELECT * FROM minimum WHERE id=1";
        $rs_minimum_price = mysqli_query($con, $sel_minimum_price);
        $minimum_price = mysqli_fetch_array($rs_minimum_price);
        ?>

        /*if ($('#productsize-' + id).val() == "") {
         $('#errormsg-' + id).html("please select product size");
         check = 1;
         }

         if ($('#price-' + id).val() == "") {
         $('#errormsg-' + id).html("please enter your price");
         check = 1;
         }

         else if ($('#price-' + id).val() != "") {
         if ($('#price-' + id).val() <?= $minimum_price["mprice"] ?>) {
         $('#errormsg-' + id).html("price should be greater than <?= $minimum_price["mprice"] ?> ")
         check = 1;
         }
         }


         if ($('#boxtype-' + id).val() == "") {
         $('#errormsg-' + id).html("please select box type");
         check = 1;
         }

         else if ($('#bunchsize-' + id).val() == "") {
         $('#errormsg-' + id).html("please select bunch size");
         check = 1;
         }

         else if ($('#bunchqty-' + id).val() == "") {
         $('#errormsg-' + id).html("please enter bunch qty / box ");
         check = 1;
         }

         else if ($('#boxqty-' + id).val() == "") {
         $('#errormsg-' + id).html("please enter box qty");
         check = 1;
         }*/
        var check = 0;

        if (check == 0) {
            var k = confirm('Are you sure you want send offer 3');
            if (k == true) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>/growers/growers-send-offers-price-conffir.php',
                    data: $('form#sendof' + id).serialize(),
                    success: function (data) {
						    console.log(data);
                        var fichas = data.split('######');
                        alert('Offer has been sent.');
                        location.reload();
                    }
                });
            }
        }

    }
    function boxQuantityChange(id, cartid, main_tr, with_a) {
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val = $("#request_qty_box_" + main_tr + " option:selected").val();
        
        	console.log(selected_val);
                console.log($.trim(selected_text));  
                
		var nbox = selected_val.split('-');
                var tbox = $.trim(selected_text).split(" ");
                
		$('input[name=boxcant]').val(nbox[0]);
                
                $('input[name=boxtypen]').val(tbox[1]);
                
          //                      
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);
        
        $("#productsize-" + cartid + "-" + id).removeAttr("disabled");
        $("#bunchqty-" + cartid + "-" + id).removeAttr("disabled");
        $("#price-" + cartid + "-" + id).show();
        $("#requestb-" + cartid + "-" + id).removeAttr("disabled");        
        
        $("#price_label_" + cartid + "-" + id).hide();
        
        $("#price-" + cartid + "-" + id).removeAttr("disabled");
        $("#productsize-" + cartid + "-" + id).removeClass("disabled-css");
        $("#bunchqty-" + cartid + "-" + id).removeClass("disabled-css");
        $("#requestb-" + cartid + "-" + id).removeClass("disabled-css");        
        
        $("#price-" + cartid + "-" + id).removeClass("disabled-css");
                         
    }
    function boxQuantityChanges(id, cartid, main_tr, with_a) {
        var selected_text = $("#request_qty_box_x_" + main_tr + " option:selected").text();
        var selected_val = $("#request_qty_box_x_" + main_tr + " option:selected").val();
        var numberbox = selected_val.split("-");
        $('input[name=product_su_x]').val(numberbox[0]);

        $("#setear-" + main_tr).val(with_a * numberbox[0]);

        $('input[name=tot_bu_x]').val(with_a * numberbox[0]);
        $('input[name=box_volumn_x]').val(numberbox[1] / 6000);
        $('input[name=box_name]').val(numberbox[2]);
        
        			var arr = selected_val.split('-');
		$("#boxcant").val(arr[0]);


    }
    
    function request_buyer(id, cartid, main_tr) {

        var selected_val = $("#request-" + cartid + "-" + id + " option:selected").val();//cantidad selecionada
        total_qty_request = $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cartid).val();//total de bunches q me permite la caja
        
        $("#boxqty_" + id + "_" + main_tr + "_" + cartid).val(selected_val);        		
    }    

    function changeBunchQty(id, cartid, main_tr) {

        var selected_val = $("#bunchqty-" + cartid + "-" + id + " option:selected").val();//cantidad selecionada
        total_qty_bunches = $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cartid).val();//total de bunches q me permite la caja
        $("#boxqty_" + id + "_" + main_tr + "_" + cartid).val(selected_val);
        
        var total_selected_val = 0;
        var this_box = 0;	
        var total = $("#total_a").val();
		
		for (var i = 0; i < total; i++) {
                            this_box= parseInt($("#boxqty_"+i+"_"+main_tr+"_"+cartid).val());
                            total_selected_val = parseInt(total_selected_val) + this_box;
		}
                                               		
        var nfilas = $("#main_table_" + main_tr + " tbody tr").length;
        var total_qty = total_qty_bunches;
        
        if (parseInt(this_box) > parseInt(total_qty)) {
            alert("Your bunch quantity greater than requested bunch qty.");
        }else{

            $("#total_availabel_product_" + main_tr).val(total_selected_val);
            var per_val = parseInt(this_box) * 100 / parseInt(total_qty);
            porcentaje[nfilas - 1] = per_val;

            var porcentajes = 0;
            for (var i = 0; i < nfilas; i++) {
                porcentajes = porcentajes + porcentaje[i];
            }

            $("#progressbar_id_"+main_tr).css("width",parseFloat(per_val).toFixed(2)+"%");
             $("#progressbar_id_"+main_tr).html("<span>Completed "+parseFloat(per_val).toFixed(2)+"%</span>");

            $("#progressbar_id_" + main_tr).css("width", parseFloat(porcentajes).toFixed(2) + "%");
            $("#progressbar_id_" + main_tr).html("<span>Completed " + parseFloat(porcentajes).toFixed(2) + "%</span>");
        }
    }
    function editVariety(id, cat_id, main_tr) {
        var productsize = $("#productsize-" + cat_id + "-" + id).val();
        var bunchqty = $("#bunchqty-" + cat_id + "-" + id).val();
        var price = $("#price-" + cat_id + "-" + id).val();
        var requestb = $("#requestb-" + cat_id + "-" + id).val();        
        
        if (productsize == "") {
            $("#productsize-" + cat_id + "-" + id).css("border", "2px solid red");
        }
        else if (bunchqty == "") {
            $("#bunchqty-" + cat_id + "-" + id).css("border", "2px solid red");
        }
        else if (price == "") {
            $("#price-" + cat_id + "-" + id).css("border", "2px solid red");
        }
        else if (requestb == "") {
            $("#requestb-" + cat_id + "-" + id).css("border", "2px solid red");
        }        
        else {

            $("#productsize-" + cat_id + "-" + id).removeAttr("disabled");
            $("#bunchqty-" + cat_id + "-" + id).removeAttr("disabled");
            $("#price-" + cat_id + "-" + id).removeAttr("disabled");
            $("#requestb-" + cat_id + "-" + id).removeAttr("disabled");            

            $("#productsize-" + cat_id + "-" + id).removeClass("disabled-css");
            $("#bunchqty-" + cat_id + "-" + id).removeClass("disabled-css");
            $("#price-" + cat_id + "-" + id).removeClass("disabled-css");
            $("#requestb-" + cat_id + "-" + id).removeClass("disabled-css");            
            
            $("#edit_variety_" + id + "_" + cat_id + "_" + main_tr).html("<i class='fa fa-edit white'></i>Update");
            $("#edit_variety_" + id + "_" + cat_id + "_" + main_tr).attr("onclick", "updateVariety(" + id + "," + cat_id + "," + main_tr + ")");
            $("#edit_variety_" + id + "_" + cat_id + "_" + main_tr).addClass("btn_update_offer");

            $("#add_variety_" + id + "_" + cat_id + "_" + main_tr).hide();
            $("#delete_variety_" + id + "_" + cat_id + "_" + main_tr).hide();
        }


    }
    function updateVariety(id, cat_id, main_tr) {
        $("#productsize-" + cat_id + "-" + id).attr("disabled", "disable");
        $("#bunchqty-" + cat_id + "-" + id).attr("disabled", "disable");
        $("#price-" + cat_id + "-" + id).attr("disabled", "disable");
        $("#requestb-" + cat_id + "-" + id).attr("disabled", "disable");        

        $("#productsize-" + cat_id + "-" + id).addClass("disabled-css");
        $("#bunchqty-" + cat_id + "-" + id).addClass("disabled-css");
        $("#price-" + cat_id + "-" + id).addClass("disabled-css");
        $("#requestb-" + cat_id + "-" + id).addClass("disabled-css");        
        
        $("#edit_variety_" + id + "_" + cat_id + "_" + main_tr).html("<i class='fa fa-edit white'></i>Edit");
        $("#edit_variety_" + id + "_" + cat_id + "_" + main_tr).attr("onclick", "editVariety(" + id + "," + cat_id + "," + main_tr + ")");
        $("#edit_variety_" + id + "_" + cat_id + "_" + main_tr).removeClass("btn_update_offer");

        $("#add_variety_" + id + "_" + cat_id + "_" + main_tr).show();
        $("#delete_variety_" + id + "_" + cat_id + "_" + main_tr).show();
    }
    function deleteVariety(id, cartid, main_tr) {
        var box_qty_cnt = $("#main_table_" + main_tr + " tbody tr").length;
        if (box_qty_cnt > 1) {
            var total_qty_bunches = $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cartid).val();
            var total_qty = total_qty_bunches;

            $("#product_tr_" + id).remove();
            var total_selected_val = 0;
            $(".boxqty" + main_tr).each(function () {
                total_selected_val = parseInt(total_selected_val) + parseInt($(this).val());
            });

            if (parseInt(total_selected_val) > parseInt(total_qty)) {
                alert("Your bunch quantity greater than requested bunch qty.");
            }
            else {
                $("#total_availabel_product_" + main_tr).val(total_selected_val);
                var per_val = parseInt(total_selected_val) * 100 / parseInt(total_qty);
                $("#progressbar_id_" + main_tr).css("width", per_val + "%");
                $("#progressbar_id_" + main_tr).html("<span>Completed " + per_val + "%</span>");
            }
        }


    }
    function changeCMSize(id, cat_id, main_tr, boxtype, userSessionID, product, sizeid) {

        var qty = $("#request_qty_box_" + main_tr + " option:selected").val();
        if (qty == "") {
            qty = $(".cls_hidden_selected_qty").val();
        }

        if (qty != "") {
            var sizeid = $("#productsize-" + cat_id + "-" + id + " option:selected").attr("size_id");
            var codid = $("#productsize-" + cat_id + "-" + id + " option:selected").attr("cod_id");
            jQuery.ajax({
                url: '/growers/ajax_bunch_standard.php',
                type: 'post',
                data: {'boxtype': boxtype, 'userSessionID': userSessionID, 'product': product, 'sizeid': sizeid, 'codid': codid, 'qty': qty},
                success: function (data) {
					 console.log(data);
                    if (jQuery.trim(data) == "0") {
                        alert("Your quantity level is : 0");
                    }
                    else {
                        $("#total_qty_bunches_" + id + "_" + main_tr + "_" + cat_id).val(data);
                    }

                }
            });
        }else{
            alert("Please select box quantity.");
        }

    }
    function addNewVariety(id, cat_id, main_tr, boxtype, userSessionID, product, sizeid) {
        var total_qty = "<?php echo $total_qty_bunches;?>";
        var total_selected_val = $("#total_availabel_product_" + main_tr).val();
        
        if (parseInt(total_selected_val) > parseInt(total_qty)) {
            alert("Your bunch quantity greater than requested bunch qty.");
        }else{
            var productsize = $("#productsize-" + cat_id + "-" + id).val();
            var bunchqty = $("#bunchqty-" + cat_id + "-" + id).val();            
            var price = $("#price-" + cat_id + "-" + id).val();
            var requestb = $("#requestb-" + cat_id + "-" + id).val();
            
            if (productsize == "") {
                $("#productsize-" + cat_id + "-" + id).css("border", "2px solid red");
            }
            else if (bunchqty == "") {
                $("#bunchqty-" + cat_id + "-" + id).css("border", "2px solid red");
            }
            else if (price == "") {
                $("#price-" + cat_id + "-" + id).css("border", "2px solid red");
            }
            else if (requestb == "") {
                $("#requestb-" + cat_id + "-" + id).css("border", "2px solid red");                            
            }else{
                $("#productsize-" + cat_id + "-" + id).attr("disabled", "disable");
                $("#bunchqty-" + cat_id + "-" + id).attr("disabled", "disable");
                $("#price-" + cat_id + "-" + id).attr("disabled", "disable");
                $("#requestb-" + cat_id + "-" + id).attr("disabled", "disable");                

                $("#productsize-" + cat_id + "-" + id).addClass("disabled-css");
                $("#bunchqty-" + cat_id + "-" + id).addClass("disabled-css");
                $("#price-" + cat_id + "-" + id).addClass("disabled-css");
                $("#requestb-" + cat_id + "-" + id).addClass("disabled-css");                

                var total_tr = $("#main_table_" + main_tr + " tbody tr").length;
                var conta_fil = parseInt($("#total_a").val()) + 1;

                $("#total_a").val(conta_fil);                                

                console.log($("#total_a").val());


                var box_qty_cnt = $("#main_table_" + main_tr + " tbody tr").length;
                box_qty_cnt = parseInt(box_qty_cnt) - 1;
                $("#request_qty_box_" + main_tr).attr("onchange", "boxQuantityChange(" + total_tr + "," + cat_id + "," + main_tr + ")");

                var productsize_html = $("#productsize-" + cat_id + "-0").html();
                var first_td = "<td><select class='form-controll offer-select' name='productsize[]' onchange='changeCMSize(" + total_tr + "," + cat_id + "," + main_tr + "," + boxtype + "," + userSessionID + "," + product + "," + sizeid + ")' id='productsize-" + cat_id + "-" + total_tr + "'>" + productsize_html + "</select></td>";
                
                var bunchqty_html = $("#bunchqty-" + cat_id + "-0").html();
                var second_td = "<td><select class='form-controll offer-select' onchange='changeBunchQty(" + total_tr + "," + cat_id + "," + main_tr + ")' name='bunchqty[]' id='bunchqty-" + cat_id + "-" + total_tr + "'>" + bunchqty_html + "</select></td>";
                
                var price_html = $("#price-" + cat_id + "-0").html();                
                var fourth_td = "<td><select class='form-controll modal_option offer-select' name='price[]' id='price-" + cat_id + "-" + total_tr + "'>" + price_html + "</select></td>";

                var requestb_html = $("#requestb-" + cat_id + "-0").html();
                var sixth_td = "<td><select class='form-controll offer-select' onchange='request_buyer(" + total_tr + "," + cat_id + "," + main_tr + ")' name='requestb[]' id='requestb-" + cat_id + "-" + total_tr + "'>" + requestb_html + "</select></td>";
                
                var fifth_td = "<td><a href='javascript:void(0)' id='add_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='addNewVariety(" + total_tr + "," + cat_id + "," + main_tr + "," + boxtype + "," + userSessionID + "," + product + "," + sizeid + ")' class='add_new_row'><span class='label label-success'>Add Variety </span></a>&nbsp;<a class='btn btn-default btn-xs' href='javascript:void(0)' id='edit_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='editVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-edit white'></i> Edit </a><a class='btn btn-default btn-xs' href='javascript:void(0)' id='delete_variety_" + total_tr + "_" + cat_id + "_" + main_tr + "' onclick='deleteVariety(" + total_tr + "," + cat_id + "," + main_tr + ")'><i class='fa fa-times white'></i> Delete </a><input type='hidden' name='boxqty[]' class='boxqty" + main_tr + "' id='boxqty_" + total_tr + "_" + main_tr + "_" + cat_id + "' value='0' /><input type='hidden' name='total_qty_bunches[]'  id='total_qty_bunches_" + total_tr + "_" + main_tr + "_" + cat_id + "' value='0' /></td>";
                $("#main_table_" + main_tr).append("<tr id='product_tr_" + total_tr + "'>" + first_td + second_td + fourth_td + sixth_td + fifth_td + "</tr>");
            }
        }
    }
    
    
    $(".offer-cls").click(function () {
        var request_number = $(this).attr("request_number");
        var id_full = $(this).attr("id");
        var id_split = id_full.split("_");
        if (request_number == id_split[1]) {

            $("#modify_modal_" + request_number).trigger("click");
            $('.modify_modal_' + request_number).addClass("sameoffer");
            $("#tab_a_" + id_split[1]).addClass("active");
            $("#tab_b_" + id_split[1]).removeClass("active");
            $(".nav-tabs li").removeClass("active");
        }
        else {
            $('.modify_modal_' + request_number).removeClass("sameoffer");
            $(".dropdown-menu li a").removeClass("active");
            $(".offer-cls-" + id_split[1]).addClass("active");
            $("#tab_a_" + id_split[1]).addClass("active");
            $("#tab_b_" + id_split[1]).removeClass("active");
            $(".nav-tabs li").removeClass("active");
            $('.modal').modal('hide');
        }

    });
    
    function send_offer_box() {

        alert(send_offer_box);

    }
    function send_offer(cartid, buyer, product, product_subcategory, sizename, price, qty) {
        console.log("Esta  es la oferta  se  esta  enviando");
        var boxtype = $("#boxtype-" + cartid).val();
        var k = confirm('Are you sure you want send offer');
        if (k == true) {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/growers/growers-send-offers-ajax-direct.php',
                data: 'offer=' + cartid + '&buyer=' + buyer + '&product=' + product + '&product_subcategory=' + product_subcategory + '&boxtype=' + boxtype + '&sizename' + sizename + '&price=' + price + '&qty=' + qty,
                success: function (data) {
                    alert('Offer has been sent.');
                    location.reload();
                }
            });
        }
    }
    function getOfferDetailsPopup(offer_id, offer_index_number, popup_index, product, feature, sizename, qty, boxtype, sizeid, box_type_name) {
        jQuery("#tab_a_" + popup_index).addClass('active');
        jQuery("#tab_b_" + popup_index).removeClass("active");
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>/growers/getOfferDetailsPopup.php',
            data: 'offer_id=' + offer_id + '&offer_index_number=' + offer_index_number + '&action_type=get_details_offer&product=' + product + '&feature=' + feature + '&sizename=' + sizename + '&qty=' + qty + '&boxtype=' + boxtype + '&sizeid=' + sizeid + '&box_type_name=' + box_type_name,
            success: function (data) {
                $("#main_table_offer_" + popup_index).html(data);
                $("#main_table_" + popup_index).hide();
                $(".offer_model_popup").hide();
            }
        });
    }
    function defaultOffer(offer_id, offer_index_number, popup_index, product, feature, sizename, qty, boxtype, sizeid, box_type_name) {
        $("#main_table_offer_" + popup_index).html("");
        $("#main_table_" + popup_index).show();
        $("#tab_a_" + popup_index).addClass("active");
        $("#tab_b_" + popup_index).removeClass("active");
    }
</script>
<?php include("../includes/footer_new.php"); ?>
