<?php
// PO 2018-07-02
require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["grower"];
/* * *******get the data of session user*************** */
if($_POST['action_type'] == "get_details_offer")
{
    $sel_info = "select * from grower_offer_reply where offer_id='" .$_POST['offer_id']. "' AND offer_id_index='".$_POST['offer_index_number']."'";
    // echo $sel_info;
    $rs_info = mysqli_query($con, $sel_info);
    ?>
    <thead>
        <tr>
            <th>Product</th>
            <th>Bunch Quantity</th>
            <th>Price</th>
            <th>Column name</th>
        </tr>
    </thead>
    <?php
    $tr_i = 0; 
    $query = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,ff.name as featurename,rg.gprice,gor.offer_id 
            from buyer_requests gpb
            inner join request_growers rg on gpb.id=rg.rid
            left join product p on gpb.product = p.id
            left join subcategory s on p.subcategoryid=s.id  
            left join features ff on gpb.feature=ff.id
            left join grower_offer_reply gor on rg.rid=gor.offer_id and rg.gid=gor.grower_id
            left join sizes sh on gpb.sizeid=sh.id where rg.gid='" . $userSessionID . "' and gpb.product='".$_POST['product']."' and gpb.lfd>='" . date("Y-m-d") . "'  group by gpb.id order by gpb.id desc";            
    //echo $query;
    $result = mysqli_query($con, $query);
    $products = mysqli_fetch_array($result);
    $s = $_POST['popup_index'];
    while($info = mysqli_fetch_array($rs_info))
    { ?>
        <tbody>
            <tr id="product_tr_<?php echo $tr_i;?>">
                <td>
                    <select class="form-controll disabled-css offer-select" name="productsize[]"  disabled onchange="changeCMSize(<?php echo $tr_i;?>,<?php echo $products["cartid"];?>,<?php echo $_POST['offer_index_number'];?>,<?php echo $products['boxtype'];?>,<?php echo $userSessionID;?>,<?php echo $products["product"];?>,<?php echo $products["sizeid"];?>)" id="productsize-<?= $products["cartid"] ?>-<?php echo $tr_i;?>">
                    <option value="">Select Product</option>
                    <?php
                    $sel_box_ids3 = "select bs.name as sizename,bs.id as sizeid from grower_product_box_packing go 
                                    left join sizes bs on go.sizeid=bs.id
                                   where go.prodcutid='" . $products["product"] . "' and go.feature='" . $products["feature"] . "' and go.growerid=" . $userSessionID . " group by go.sizeid order by CONVERT(SUBSTRING(bs.name,1), SIGNED INTEGER)";
                    
                                  
                    $rs_box_ids3 = mysqli_query($con, $sel_box_ids3);
                    ?>
                    <?php
                    while ($box_ids3 = mysqli_fetch_array($rs_box_ids3)) {
                        ?>
                        <option value="<?= $box_ids3["sizename"] ?>cm" <?php if($box_ids3["sizename"]."cm" == $info["size"]){ echo "selected";}?> size_id="<?php echo $box_ids3["sizeid"];?>"  > <?= $_POST["box_type_name"]?> <?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?><?= $box_ids3["sizename"] ?>cm <?php echo $rowbunches['bunchname']."st/bu";?></option>
                        <?php
                    }
                    ?>
                    </select>
                </td>
                <td>
                    <select class="form-controll disabled-css offer-select" disabled="" onchange="changeBunchQty(0,1904,0)" name="bunchqty[]" id="bunchqty-1904-0">
                        <option value="">Select Bunch QTY</option>
                        <?php
                        for ($i = 1; $i <= 100; $i++) {
                            ?>
                            <option value="<?= $i ?>" <?php if($info["bunchqty"] == $i) echo "SELECTED";?>><?= $i ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <?php 
                    $real_product_price = "";    
                    if ($products['discount'] != '' || $products['discount'] != 0) {
                        if($products["price"] == "0")
                        {
                            //echo "Open Bid";
                        }
                        else
                        {
                            //echo "$".$products['discount'];
                            $real_product_price = $products['discount'];
                        }    
                    } else {
                        if($products["price"] == "0")
                        {
                            //echo "Open Bid";
                        }
                        else
                        {
                            //echo "$".$products["price"];
                            $real_product_price = $products['discount'];
                        }    
                    } ?>
                    <select name="price[]" disabled="" id="price-1904-0" class="form-controll modal_option disabled-css offer-select">
                        <option value="">Select Price</option>
                        <?php 
                        for($i=10;$i<=90;$i++)
                        { ?>
                            <option value="0.<?php echo $i;?>" <?php if($info['price'] == "0.".$i) echo "selected";?>>$0.<?php echo $i;?></option>
                        <?php }
                        ?>
                    </select>
                </td>
                <td>
                    <a class="add_new_row" id="add_variety_<?php echo $tr_i;?>_<?php echo $products["cartid"];?>_<?php echo $s;?>" onclick="addNewVariety($tr_i,<?php echo $products["cartid"];?>,<?php echo $s;?>,<?php echo $products['boxtype'];?>,<?php echo $userSessionID;?>,<?php echo $products["product"];?>,<?php echo $products["sizeid"];?>)" href="javascript:void(0);"><span class="label label-success">Add Variety </span> </a>&nbsp;
                    <a href='javascript:void(0)' id='edit_variety_<?php echo $tr_i;?>_<?php echo $products["cartid"];?>_<?php echo $s;?>' onclick='editVariety($tr_i,<?php echo $products["cartid"];?>,<?php echo $s;?>)' class="btn btn-default btn-xs"><i class="fa fa-edit white"></i> Edit </a>
                    <a href='javascript:void(0)' id='delete_variety_<?php echo $tr_i;?>_<?php echo $products["cartid"];?>_<?php echo $s;?>' onclick='deleteVariety($tr_i,<?php echo $products["cartid"];?>,<?php echo $s;?>)' class="btn btn-default btn-xs"><i class="fa fa-times white"></i> Delete </a>
                    <input type="hidden" class="boxqty<?php echo $s;?>" name="boxqty[]" id="boxqty_<?php echo $tr_i;?>_<?php echo $s;?>_<?php echo $products["cartid"];?>" value="" />
                    <input type="hidden" name="total_qty_bunches[]" id="total_qty_bunches_<?php echo $tr_i;?>_<?php echo $s;?>_<?php echo $products["cartid"];?>" value="0" />
                </td>
            </tr>
        </tbody>
    <?php
        $tr_i++; 
    }
}
elseif ($_POST['action_type'] == "get_default_offer") {
    $tr_i = 0; 
    $query = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,ff.name as featurename,rg.gprice,gor.offer_id 
            from buyer_requests gpb
            inner join request_growers rg on gpb.id=rg.rid
            left join product p on gpb.product = p.id
            left join subcategory s on p.subcategoryid=s.id  
            left join features ff on gpb.feature=ff.id
            left join grower_offer_reply gor on rg.rid=gor.offer_id and rg.gid=gor.grower_id
            left join sizes sh on gpb.sizeid=sh.id where rg.gid='" . $userSessionID . "' and gpb.product='".$_POST['product']."' and gpb.lfd>='" . date("Y-m-d") . "'  group by gpb.id order by gpb.id desc";            
    //echo $query;
    $result = mysqli_query($con, $query);
    $products = mysqli_fetch_array($result);
    $s = $_POST['popup_index'];
    ?>
        <thead>
            <tr>
                <th>Product</th>
                <th>Bunch Quantity</th>
                <th>Price</th>
                <th>Column name</th>
            </tr>
        </thead>
        <tbody>
            <tr id="product_tr_<?php echo $tr_i;?>">
                <td>
                    <select class="form-controll offer-select" name="productsize[]"   onchange="changeCMSize(<?php echo $tr_i;?>,<?php echo $products["cartid"];?>,<?php echo $_POST['offer_index_number'];?>,<?php echo $products['boxtype'];?>,<?php echo $userSessionID;?>,<?php echo $products["product"];?>,<?php echo $products["sizeid"];?>)" id="productsize-<?= $products["cartid"] ?>-<?php echo $tr_i;?>">
                    <option value="">Select Product</option>
                    <?php
                    $sel_box_ids3 = "select bs.name as sizename,bs.id as sizeid from grower_product_box_packing go 
                                    left join sizes bs on go.sizeid=bs.id
                                   where go.prodcutid='" . $products["product"] . "' and go.feature='" . $products["feature"] . "' and go.growerid=" . $userSessionID . " group by go.sizeid order by CONVERT(SUBSTRING(bs.name,1), SIGNED INTEGER)";
                    
                                  
                    $rs_box_ids3 = mysqli_query($con, $sel_box_ids3);
                    ?>
                    <?php
                    while ($box_ids3 = mysqli_fetch_array($rs_box_ids3)) {
                        ?>
                        <option value="<?= $box_ids3["sizename"] ?>cm"  size_id="<?php echo $box_ids3["sizeid"];?>"  > <?= $_POST["box_type_name"]?> <?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?><?= $box_ids3["sizename"] ?>cm <?php echo $rowbunches['bunchname']."st/bu";?></option>
                        <?php
                    }
                    ?>
                    </select>
                </td>
                <td>
                    <select class="form-controll  offer-select"  onchange="changeBunchQty(0,1904,0)" name="bunchqty[]" id="bunchqty-1904-0">
                        <option value="">Select Bunch QTY</option>
                        <?php
                        for ($i = 1; $i <= 100; $i++) {
                            ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <?php 
                    $real_product_price = "";    
                    if ($products['discount'] != '' || $products['discount'] != 0) {
                        if($products["price"] == "0")
                        {
                            //echo "Open Bid";
                        }
                        else
                        {
                            //echo "$".$products['discount'];
                            $real_product_price = $products['discount'];
                        }    
                    } else {
                        if($products["price"] == "0")
                        {
                            //echo "Open Bid";
                        }
                        else
                        {
                            //echo "$".$products["price"];
                            $real_product_price = $products['discount'];
                        }    
                    } ?>
                    <select name="price[]"  id="price-1904-0" class="form-controll modal_option  offer-select">
                        <option value="">Select Price</option>
                        <?php 
                        for($i=10;$i<=90;$i++)
                        { ?>
                            <option value="0.<?php echo $i;?>" <?php if($real_product_price['price'] == "0.".$i) echo "selected";?>>$0.<?php echo $i;?></option>
                        <?php }
                        ?>
                    </select>
                </td>
                <td>
                    <a class="add_new_row" id="add_variety_<?php echo $tr_i;?>_<?php echo $products["cartid"];?>_<?php echo $s;?>" onclick="addNewVariety($tr_i,<?php echo $products["cartid"];?>,<?php echo $s;?>,<?php echo $products['boxtype'];?>,<?php echo $userSessionID;?>,<?php echo $products["product"];?>,<?php echo $products["sizeid"];?>)" href="javascript:void(0);"><span class="label label-success">Add Variety </span> </a>&nbsp;
                    <a href='javascript:void(0)' id='edit_variety_<?php echo $tr_i;?>_<?php echo $products["cartid"];?>_<?php echo $s;?>' onclick='editVariety($tr_i,<?php echo $products["cartid"];?>,<?php echo $s;?>)' class="btn btn-default btn-xs"><i class="fa fa-edit white"></i> Edit </a>
                    <a href='javascript:void(0)' id='delete_variety_<?php echo $tr_i;?>_<?php echo $products["cartid"];?>_<?php echo $s;?>' onclick='deleteVariety($tr_i,<?php echo $products["cartid"];?>,<?php echo $s;?>)' class="btn btn-default btn-xs"><i class="fa fa-times white"></i> Delete </a>
                    <input type="hidden" class="boxqty<?php echo $s;?>" name="boxqty[]" id="boxqty_<?php echo $tr_i;?>_<?php echo $s;?>_<?php echo $products["cartid"];?>" value="" />
                    <input type="hidden" name="total_qty_bunches[]" id="total_qty_bunches_<?php echo $tr_i;?>_<?php echo $s;?>_<?php echo $products["cartid"];?>" value="0" />
                </td>
            </tr>
        </tbody>
    <?php
      
}
?>
