<?php

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/
/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO REAL *******/
require_once("config/config_gcp.php");
/**********************************************************/
/**********************************************************/
/***********CAMBIAR LINEA DE CONEXION EN SITIO local *******/
//require_once("../../config/config_gcp.php");
/**********************************************************/
if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login-type"] == 'buyer') {
    header("location: buyer/buyers-account.php");
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location: vendor-account.php");
    die;
}


            $update_menu = "update control_menu
                                set menu = 2
                              where id = 1 ";

            mysqli_query($con, $update_menu);


#############QUERY TO FETCH PAGE DETAILS testing by  KD Sharma. All working good chetan###################STARTS###########################################################################
$pageId = 17;//VARIETY PAGE ID
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################




require_once "folderlogin/inc/header-2.php";




?>
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>

<style type="text/css">
	.custom_select button.btn.dropdown-toggle.select-form-control.border {
    padding-top: 0px;
}
</style>
<!-- PAGE TITLE -->
			<section>
				<div class="px-0 max-w-700 mx-auto text-center">

					<h1 class="display-4 h2-xs font-weight-bold">
						Login as <span class="text-purple" id="textlogin"> <i class="fa fa-user-circle" aria-hidden="true"></i> Buyers</span>
					</h1>

					<p class="lead m-0">
						Manage your account and data
					</p>

				</div>

			<!-- /PAGE TITLE -->




			<!-- FORM -->

				<div class="container">


					<div class="row">


						<!-- ALERT -->


						<div class="col-12 col-sm-8 col-md-8 col-lg-6 offset-sm-2 offset-md-2 offset-lg-3">

							<div class="alert alert-mini margin-bottom-30" id="checkLoginErr">
											<!--<strong>Oh snap!</strong> Login Incorrect!-->
							</div><!-- /ALERT -->

							<!-- optional class: .form-control-pill -->

              <form class="bs-validate p-5 p-4-xs rounded shadow-xs" id="loginFormID" id="loginFormID"  action="login.php" method="post" autocomplete="off">
								<!--
								<p class="text-danger">
									Ups! Please check again
								</p>
								-->
								<div class="form-label-group mb-3">
									<input required placeholder="Email" name="email"  id="email" type="email" class="form-control">
									<label for="account_email">Email</label>
								</div>

								<div class="input-group-over">
									<div class="form-label-group mb-3">
										<input required placeholder="Password" type="password" name="password" type="password" class="form-control">
										<label for="account_password">Password</label>
									</div>

									<a href="forgot.php" class="btn fs--12">
										FORGOT?
									</a>

								</div>



								<div class="row">

									<div class="col-12 col-md-6 mt-4">
										<button type="submit" class="btn btn-primary btn-block font-weight-medium">
											Sign In
										</button>
									</div>

                  <input type="hidden" name="usrtype" id="usrtypeID" value="buyer">
                  <input type="hidden" name="submit" value="_login">

									<div class="col-12 col-md-6 mt-4 text-align-end text-center-xs">
										<a href="http://content.freshlifefloral.com/register-form?hs_preview=dleYBCdQ-4328719101" class="btn px-0">
											Don't have an account yet?
										</a>
									</div>

								</div>

							</form>

						</div>

					</div>

				</div>
			</section>
			<!-- /FORM -->

<?php include('folderlogin/inc/footer-2.php'); ?>
<script type="text/javascript">
    $("#login_as_growers").click(function (event) {
        $("#textlogin").html('<span class="text-success" id="textlogin"> <i class="fa fa-truck" aria-hidden="true"></i> Growers</span>');
        $("#usrtypeID").val('grower');
        var url_s = "/grower/forgot-grower.php";
        $("#forgot_id").attr("href",url_s);
    });
    $("#login_as_buyers").click(function (event) {
        $("#textlogin").html('<span class="text-purple" id="textlogin"> <i class="fa fa-user-circle" aria-hidden="true"></i> Buyers</span>');
        $("#usrtypeID").val('buyer');
        var url_s = "/grower/forgot-grower.php";
        $("#forgot_id").attr("href",url_s);
    });
    $(function () {
        $("#loginFormID").submit(function (event) {
            $('#checkLoginErr').removeClass("alert-danger");
            $('#checkLoginErr').removeClass("alert-success");
            $('#checkLoginErr').html('');
            event.preventDefault();
            $.ajax({
                url: 'savelogin.php',
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                     if (result == 1) {
                        $('#checkLoginErr').html('Login success!');
                        $('#checkLoginErr').addClass("alert-success");
                        //window.location.href = window.location.href;
                        window.location.href = "buyer/buyers-account.php?menu=1";

                    } else {

                         if (result == 2) {
                        $('#checkLoginErr').html('Login success!');
                        $('#checkLoginErr').addClass("alert-success");
                        //window.location.href = window.location.href;
                        //window.location.href = "<?php echo SITE_URL; ?>growers/growers-account-summery.php";
                        window.location.href = "file/vendor-account.php";
                            }  else{
                        $('#checkLoginErr').addClass("alert-danger");
                        $('#checkLoginErr').html(result);
                    }
                    }
                }
            });
        });
    });
</script>
